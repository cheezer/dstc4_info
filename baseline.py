import argparse, sys, ontology_reader, dataset_walker, time, json, copy
from fuzzywuzzy import fuzz
from nltk.tag import StanfordPOSTagger
from collections import defaultdict
import math, re, os

#tokenizer?

def solve_unk_word(word):
    number_map = {'1':'one','2':'two','3':'three','4':'four','5':'five','6':'six','7':'seven','8':'eight','9':'nine','0':'zero'}
    word = word.strip(",.?!+-@~"+"\'")
    if word == "":
        return []
    else:
        word = re.sub("[,.?!+\-@~]",' ',word)
        tmp_words = word.split(' ')
        new_tmp_words = []
        for tmp_word in tmp_words:
            if tmp_word in number_map:
                new_tmp_words.append(number_map[tmp_word])
            else:
                if tmp_word in ['o\'clock']:
                    new_tmp_words.append(tmp_word)
                elif '\'' in tmp_word:
                    tmp_word = tmp_word.replace('\'',' \'').split(' ')
                    for key in tmp_word:
                        new_tmp_words.append(key)
                else:
                    new_tmp_words.append(tmp_word)
        return new_tmp_words

def preprocess(text):
    tmp_words = text.split(' ')
    words = []
    for word in tmp_words:
        new_sub_words = solve_unk_word(word)
        for sub_word in new_sub_words:
            words.append(sub_word)
    return ' '.join(words)

def addtsv(topic, slot, value, ans):
    if topic not in ans:
        ans[topic] = {}
    if slot not in ans[topic]:
        ans[topic][slot] = {}
    if value not in ans[topic][slot]:
        ans[topic][slot][value] = []
    return ans

class bigram:
    def __init__(self):
        pass

    def calculateDistance(self, topic, description, transcript):
        valueText = preprocess(description).split(" ")
        text = preprocess(transcript).split(" ")
        if len(valueText) < 2:
            return 0
        tot = 0
        for i in range(len(valueText) - 1):
            for j in range(len(text) - 1):
                if text[j] == valueText[i] and text[j + 1] == valueText[i + 1]:
                    tot += 1
        return tot / (len(valueText) - 1)

class bf0:
    def __init__(self):
        pass

    def calculateDistance(self, topic, description, transcript):
        if len(transcript) < 6:
            return 0
        if len(description) < 6 and ' ' not in description and len(transcript) > 10:
            return 0
        ratio = fuzz.partial_ratio(description.lower().replace('-', ' '), transcript)
        if ratio >= 90 or (ratio >= 85 and (len(description) > 8 or ' ' in description)):
            return ratio / 100.0

class FromTos:
    def __init__(self, tagsets):
        self.tagsets = tagsets

    def calculateDistance(self, topic, slot, value, transcript):
        if slot.lower() != "from" and slot.lower() != "to":
            return 0
        if slot.lower() + " " + value.lower() in transcript.lower():
            return 1
        else:
            return 0

class MRTColor:
    def __init__(self, tagsets, knowledge):
        self.tagsets = tagsets
        self.knowledge = knowledge

    def calculateDistance(self, topic, slot, value, transcript):
        if slot.lower() != "line":
            return 0
        if slot.lower() != "line":
            for line in self.knowledge["MRT_LINE"]:
                if line["Name"] == value:
                    if line["COLOR"].lower() in transcript or " " + line["CODE"].lower() + " " in transcript:
                        return 1
        return 0


def getDescription(tagsets, knowledge):
    #value_map = {'Nightclub':'night club','Dresscode':'dress code', 'Historic site':'historical places','Cultural site':'cultural places'}
    value_map = {'Historic site':'historical places','Cultural site':'cultural places',
                 'Nightclub':'night club','Dresscode':'dress code','Thankyou':'thank you',
                 'Parkroyal':'park royal','Northpoint':'north point','Fairprice':'fair price',
                 'Riverview':'river view','Parkview':'park view','Waterpark':'water park','Artscinece':'art scinece',
                 'Fishball':'fish ball','Southbridge':'south bridge','Pricerange':'price range','Westgate':'west gate',
                 }
    ans = {}
    for topic in tagsets:
        for slot in tagsets[topic]:
            if slot == 'LINE':
                for item in knowledge['MRT_LINE']:
                    color = item['COLOR']
                    value = item['NAME']
                    ans = addtsv(topic, slot, value, ans)
                    ans[topic][slot][value].append(color.lower()+' line')
            prefix = ''
            if slot == "FROM":
                prefix = 'from '
            elif slot == "TO":
                prefix = 'to '
            for value in tagsets[topic][slot]:
                value1 = value
                ans = addtsv(topic, slot, value, ans)
                ans[topic][slot][value].append(prefix+value1.lower())
                if value in value_map:
                    value1 = value_map[value]
                    ans = addtsv(topic, slot, value, ans)
                    ans[topic][slot][value].append(prefix+value1.lower())
    return ans

class TfIdf:
    def __init__(self, ontology, knowledge):
        sp = os.sep
        tagPath = "stanford-postagger-2014-08-27"
        self.pos = StanfordPOSTagger(tagPath + sp + "models" + sp + "english-bidirectional-distsim.tagger", tagPath + sp + "stanford-postagger.jar")
        self.nounList = ["NN", "NNS", "NNP", "NNPS", "PRP", "PRP$", "WP", "WP$"]
        self.ontology = ontology
        self.dict = Dictionary()
        self.tf = {}
        self.appear = {}
        self.idf = {}
        self.tfIdf = {}
        self.numDoc = {}
        self.threshold = 0.6
        self.lastQuery = None
        self.lastVec = None
        self.valueDict = {}
        description = getDescription(self.ontology, knowledge)
        #description = {}
        for topic in description:
            self.tf[topic] = {}
            self.idf[topic] = {}
            self.valueDict[topic] = {}
            self.appear[topic] = {}
            self.numDoc[topic] = 0
            for slot in description[topic]:
                self.tf[topic][slot] = {}
                self.valueDict[topic][slot] = {}
                for value in description[topic][slot]:
                    self.tf[topic][slot][value] = defaultdict(int)
                    self.valueDict[topic][slot][value] = Dictionary()
                    self.dict.add(value)
                    self.valueDict[topic][slot][value].add(value)
                    for i in description[topic][slot][value]:
                        self.numDoc[topic] += 1
                        self.dict.add(i)
                        self.valueDict[topic][slot][value].add(i)
                        for word in Dictionary.wordList(i):
                            self.tf[topic][slot][value][word] += 1
                            '''if value == "Cultural site":
                                print value, word'''
                            if word not in self.appear[topic]:
                                self.appear[topic][word] = {}
                            #if word == "singapore" and topic == "FOOD" and (topic, slot, value, self.numDoc[topic]) not in self.appear[topic][word]:
                            #    print i
                            self.appear[topic][word][(topic, slot, value, self.numDoc[topic])] = 1

        for topic in ontology:
            for word in self.appear[topic]:
                appearTime = len(self.appear[topic][word].keys())
                self.idf[topic][word] = math.log(float(self.numDoc[topic]) / appearTime) #??add 1 before math.log?
        self.tfIdf = copy.deepcopy(self.tf)
        for topic in ontology:
            for slot in ontology[topic]:
                for value in ontology[topic][slot]:
                    for word in self.tfIdf[topic][slot][value]:
                        self.tfIdf[topic][slot][value][word] = self.tf[topic][slot][value][word] * self.idf[topic][word]
                    #print topic, slot, value, self.tfIdf[topic][slot][value]
                    #self.tfIdf[topic][slot][value] = normalise(self.tfIdf[topic][slot][value])
        for topic in ontology:
            json.dump(self.tfIdf[topic], open(topic + ".json", "wb"))
        for topic in ontology:
            for slot in ontology[topic]:
                for value in ontology[topic][slot]:
                    self.tfIdf[topic][slot][value] = normalise(self.tfIdf[topic][slot][value])

    '''def documentPreProcess(self, documents):
        self.docAppear = {}
        ci = 0
        self.docIdf = {}
        for st in documents:
            ci += 1
            ss = self.dict.wordList(st)
            #print ss
            #for word, pro in self.pos.tag(ss):
            #    if pro in self.nounList: #is this list reliable?
            for word in ss:
                if 1 == 1:
                    #print word
                    if self.dict.inference(word) is not None:
                        if word not in self.docAppear:
                            self.docAppear[word] = defaultdict(int)
                        self.docAppear[word][ci] = 1
                else:
                    pass
        for word in self.docAppear:
            appearTime = len(self.docAppear[word].keys())
            self.docIdf[word] = math.log(ci / (1 + appearTime)) #??add 1 before math.log?'''

    def calculateDistance(self, topic, slot, value, documents):
        '''if documents == self.lastQuery:
            vec = self.lastVec
        else:
            self.lastQuery = documents'''
        vec = defaultdict(int)
        for st in [documents]:
            ss = Dictionary.wordList(st)
            #for word, pro in self.pos.tag(ss):
                #if pro in self.nounList:
            for word in ss:
                if 1 == 1:
                    #if self.valueDict[topic][slot][value].inference(value) is not None:#and word in self.docIdf:
                    #if word in self.valueDict[topic][slot][value].dict:
                    if word in self.idf[topic]:
                        #vec[word] += self.docIdf[word]
                        #print topic, word, value
                        vec[word] += self.idf[topic][word]
        vec = normalise(vec)
        #self.lastVec = vec
        ans = 0
        #for word in vec:
        for word in self.tfIdf[topic][slot][value]:
            ans += vec[word] * self.tfIdf[topic][slot][value][word]
        #if value == "Universal Studios Singapore":
        #    print documents, vec, self.tfIdf[topic][slot][value]
        return ans

class Dictionary:
    def __init__(self):
        self.dict = defaultdict(int)
        self.tot = 0

    @staticmethod
    def removePunct(st):
        st = st.replace("%", "").replace("-", "").replace("@", "")#.replace(".", " ")
        return st

    @staticmethod
    def wordList(st):
        return preprocess(st).lower().split(" ")
        wl = removePunct(st).lower().split(" ")
        ans = []
        #print wl
        for i in wl:
            if i.endswith("."):
                ans.append(i[:-1])
                ans.append(".")
            elif i.find('\'') != -1:
                k1 = i.find('\'')
                ans.append(i[:k1])
                ans.append(i[k1 + 1:])
            elif i != "":
                ans.append(i)
        return ans

    def inferenceWithAdd(self, word):
        word = Dictionary.removePunct(word)
        if word in self.dict:
            return self.dict[word]
        else:
            self.dict[word] = self.tot
            self.tot += 1

    def add(self, st):
        words = Dictionary.wordList(st)
        for i in words:
            if i.isalpha():
                self.inferenceWithAdd(i)

    def inference(self, word):
        #word = self.removePunct(word)
        if word in self.dict:
            return self.dict[word]
        else:
            return None

def normalise(dct):
    #print dct
    tot = 0
    for word in dct:
        tot += dct[word] * dct[word]
    if math.fabs(tot) < 1e-8:
        return dct
    tot = math.sqrt(tot)
    for word in dct:
        dct[word] = dct[word] * 1.0 / tot
    return dct

class BaselineTracker(object):
    def __init__(self, tagsets, tfIdf):
        self.tagsets = tagsets
        self.tfIdf = tfIdf
        self.frame = {}
        self.memory = {}
        self.reset()

    def addUtter(self, utter):
        output = {'utter_index': utter['utter_index']}
        topic = utter['segment_info']['topic']
        transcript = utter['transcript'].replace('Singapore', '')
        if utter['segment_info']['target_bio'] == 'B':
            self.reset()
        #self.doc = self.doc + transcript + " "
        self.doc = transcript
        if topic in self.tagsets:
            for slot in self.tagsets[topic]:
                for value in self.tagsets[topic][slot]:
                    #ratio = fuzz.partial_ratio(value, transcript)
                    #ratio = self.tfIdf.calculateDistance(topic, slot, value, self.doc)
                    ratio = self.tfIdf.calculateDistance(topic, value, self.doc)
                    #if ratio > self.tfIdf.threshold:
                    if ratio > 1:
                        if slot not in self.frame:
                            self.frame[slot] = []
                        if value not in self.frame[slot]:
                            self.frame[slot].append(value)
                        #self.frame[slot] = [value]
                # if topic == 'ATTRACTION' and 'PLACE' in self.frame and 'NEIGHBOURHOOD' in self.frame and self.frame[
                #     'PLACE'] == self.frame['NEIGHBOURHOOD']:
                #     del self.frame['PLACE']

            output['frame_label'] = copy.deepcopy(self.frame)
        return output

    def reset(self):
        self.frame = {}
        self.doc = ""

def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True,
                        help='The dataset to analyze')
    parser.add_argument('--dataroot', dest='dataroot', action='store', required=True, metavar='PATH',
                        help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--trackfile', dest='trackfile', action='store', required=True, metavar='JSON_FILE',
                        help='File to write with tracker output')
    parser.add_argument('--ontology', dest='ontology', action='store', metavar='JSON_FILE', required=True,
                        help='JSON Ontology file')

    args = parser.parse_args()
    tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
    knowledge = ontology_reader.OntologyReader(args.ontology).get_knowledge()
    tfIdf = TfIdf(tagsets, knowledge)
    '''doc = []
    lastDoc = ""
    for ds in ["dstc4_train", "dstc4_dev"]:
        dataset = dataset_walker.dataset_walker(ds, dataroot=args.dataroot, labels=False)
        for call in dataset:
            for (utter, _) in call:
                #sys.stderr.write('%d:%d\n' % (call.log['session_id'], utter['utter_index']))
                bio = utter['segment_info']['target_bio']
                if bio != "I" and lastDoc != "":
                    doc.append(copy.deepcopy(lastDoc))
                    lastDoc = ""
                if bio == "B":
                    lastDoc = ""
                if bio != "O":
                    lastDoc = lastDoc + utter['transcript'] + " "
            if lastDoc != "":
                doc.append(copy.deepcopy(lastDoc))
                lastDoc = ""
    json.dump(doc, open("abc.txt", "wb"))
    tfIdf.documentPreProcess(doc)'''
    dataset = dataset_walker.dataset_walker(args.dataset, dataroot=args.dataroot, labels=False)
    track_file = open(args.trackfile, "wb")
    track = {"sessions": []}
    track["dataset"] = args.dataset
    start_time = time.time()
    #tracker = BaselineTracker(tagsets, tfIdf)
    tracker = BaselineTracker(tagsets, bigram())
    for call in dataset:
        this_session = {"session_id": call.log["session_id"], "utterances": []}
        tracker.reset()
        for (utter, _) in call:
            sys.stderr.write('%d:%d\n' % (call.log['session_id'], utter['utter_index']))
            tracker_result = tracker.addUtter(utter)
            if tracker_result is not None:
                this_session["utterances"].append(tracker_result)
        track["sessions"].append(this_session)
    end_time = time.time()
    elapsed_time = end_time - start_time
    track['wall_time'] = elapsed_time
    json.dump(track, track_file, indent=4)
    track_file.close()

if __name__ == "__main__":
    main(sys.argv)
