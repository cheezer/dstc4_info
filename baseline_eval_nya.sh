#!/bin/bash
python check_track.py --dataset dstc4_dev --dataroot ../data --ontology config/ontology_dstc4.json --trackfile baseline_nya.json
python score.py --dataset dstc4_dev --dataroot ../data --trackfile baseline_nya.json --scorefile baseline_nya.score.csv --ontology config/ontology_dstc4.json
python report.py --scorefile baseline_nya.score.csv
