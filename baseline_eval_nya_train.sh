#!/bin/bash
ds=dstc4_train1
python check_track.py --dataset $ds --dataroot ../data --ontology config/ontology_dstc4.json --trackfile baseline_nya_train.json
python score.py --dataset $ds --dataroot ../data --trackfile baseline_nya_train.json --scorefile baseline_nya_train.score.csv --ontology config/ontology_dstc4.json
python report.py --scorefile baseline_nya_train.score.csv
