ABC News is the news division of the American Broadcasting Company (ABC), owned by the Disney Media Networks division of the Walt Disney Company. Its flagship program is the daily evening newscast ABC World News Tonight; other programs include morning news-talk show Good Morning America, newsmagazine series Nightline, Primetime and 20/20, and Sunday morning political affairs program This Week with George Stephanopoulos.


== History ==
ABC began news broadcasts early in its independent existence as a radio network after the Federal Communications Commission (FCC) ordered NBC to spin off the former NBC Blue Network into an independent company in 1943. The split (which NBC conducted voluntarily in the event that its appeal to have the ruling overturned was denied) was enforced to expand competition in radio broadcasting in the United States as the industry had only a few companies such as NBC and CBS that dominated the radio market, and in particular, was intended to prevent the limited competition from dominating news and political broadcasting and projecting narrow points-of-view. Television broadcasting was suspended however, during World War II.
Regular television news broadcasts on ABC began soon after the network signed on its initial owned-and-operated television station (WJZ-TV, now WABC-TV) and production center in New York City in August 1948. ABC news broadcasts have continued as the television network expanded nationwide, a process that took many years beginning with its launch in 1948. However, from the 1950s through the early 1970s, ABC News' programs (as was the case with the television network in general during that period) consistently ranked third in viewership behind news programs on CBS and NBC. Until the 1970s, the ABC television network had fewer affiliate stations, as well as a weaker prime-time programming slate to be able to truly support the network's news operations in comparison to the two larger networks, each of which had established their radio news operations during the 1930s.


=== Under Roone Arledge ===
Only after Roone Arledge, the president of ABC Sports at the time, was appointed as president of ABC News in 1977, at a time when the network's prime-time entertainment programs were achieving stronger ratings and drawing in higher advertising revenue and profits to the ABC corporation overall, was ABC able to invest the resources to make it a major source of news content. Arledge, known for experimenting with the broadcast "model", created many of ABC News' most popular and enduring programs, including 20/20, World News Tonight, This Week, Nightline and Primetime Live.
ABC News gained respect in the early 1980s for its coverage of the Iran hostage crisis (which led to the development of the late-night news program that would become Nightline) and, later, for its coverage of the Loma Prieta earthquake that struck the San Francisco Bay Area in October 1989 (which occurred during the network's Game 1 coverage of the 1989 World Series) with live reports.
ABC News' longtime slogan, "More Americans get their news from ABC News than from any other source" (introduced in the late 1980s), was a claim referring to the number of people who watch, listen and read ABC News content on television, radio and (eventually) the Internet, and not necessarily to the telecasts alone.
In June 1998, ABC News (which owned an 80% stake in the service), Nine Network and ITN sold their respective interests in Worldwide Television News to the Associated Press. Additionally, ABC News signed a multi-year content deal with AP for its affiliate video service Associated Press Television News (APTV) while providing material from ABC's news video service ABC News One to APTV.


== Association with ESPN ==
ESPN, a sports-news organization with several cable and satellite television channels – and also majority owned by ABC parent The Walt Disney Company – provides sports bulletins and video footage for some of ABC News' programs, especially the network's overnight news programs; America This Morning features a segment of sports highlights provided by the overnight anchors of ESPN's flagship sports news program SportsCenter.


== Programming ==


=== Current ABC News programs ===
20/20 (June 6, 1978–present)
ABC World News Tonight (July 10, 1978–present)
America This Morning (July 5, 1982–present)
Good Morning America (November 3, 1975–present)
Nightline (March 24, 1980 – Present)
Primetime (August 3, 1989–present)
The View (October 31, 2014–present; taking over responsibilities from ABC Daytime)
This Week (November 15, 1981–present)
World News Now (January 6, 1992–present)


=== Former ABC News programs ===


==== Morning news programs ====
AM America (January 6–October 31, 1975)


==== Newsmagazines ====
20/20 Downtown (October 1999 – 2001)
Day One (March 7, 1993 – 1995)
Primetime Thursday (2000–2002)
Our World (September 25, 1986 – May 28, 1987)
Turning Point (March 9, 1994 – June 17, 1999)


==== Public affairs ====
Issues and Answers (November 1960–November 1981)


=== Daytime news-talk programs ===
Good Afternoon America (July 9–September 7, 2012)


== Other services ==
ABC News Radio, a service syndicated by Cumulus Media Networks, broadcasts hourly news updates, live feeds and specialty news, sports and entertainment programming to approximately 2,000 radio affiliates nationwide. As part of Disney's sale of the ABC Radio division to Citadel Broadcasting in 2007, ABC News entered into an exclusive agreement with Citadel to distribute its radio news service on terrestrial stations (Citadel has since merged with Cumulus Media).
ABC NewsOne is ABC News's affiliate news service. It gathers and feeds regional, national and international news material to ABC affiliates around the country and foreign networks.
A 30-second ABC News Brief is broadcast weekdays at 2:58 p.m. ET following General Hospital (though these newsbriefs are not aired on all ABC stations, those that do not carry the newsbriefs typically opt to begin local commercial breaks immediately following the network program preceding it). A news brief containing information relevant to college students is shown every hour on mtvU, and ABC News segments are packaged or customized for broadcast over Wal-Mart's in-store television network.
Fusion is a digital cable and satellite network operated as a joint venture between ABC News and Univision Communications. ABC and Univision formally announced its launch on May 2, 2012. Launched on October 28, 2013, Fusion features a mix of traditional news and investigative programs along with satirical content aimed at English-speaking Hispanic and Latino American adults between the ages of 18 and 34.


== Personnel ==


=== Correspondents and reporters ===


=== Former ===
('+' symbol indicates person deceased)


== International broadcasts ==
Several ABC News programs are broadcast daily on OSN News, a service consisting of three 24-hour satellite and cable channels exclusively offering American news programs from ABC, NBC, PBS and MSNBC to U.S. expatriates and other viewers abroad, primarily geared towards an audience in the Arab countries. The network is available on digital cable and satellite in Europe, Northeast Asia and the Middle East, however, cable providers in Europe are currently unable to carry the channels due to unsolved rights issues. It is also available online on ABC News' website.
In the United Kingdom, ABC World News Tonight airs daily at 1:30 a.m. local time on the BBC News Channel, which itself may be simulcast on BBC One or BBC Two during the overnight hours. Commercials are not presented during the broadcast as the BBC's services in the U.K. are financed through license fees. ABC and the BBC also share video segments and reporters as needed for news coverage in their respective countries.
In Australia, Sky News Australia airs daily broadcasts of World News Tonight (at 10:30 a.m.) and Nightline (at 1:30 a.m.) as well as weekly airings of 20/20 (on Wednesdays at 1:30 p.m., with an extended version at 2:00 p.m. on Sundays) and occasionally Primetime (at 1:30 p.m. on Thursdays, with extended edition at 2:00 p.m. on Saturdays). Coincidentally, that country's public broadcaster, the Australian Broadcasting Corporation, operates its own unrelated news division that is also named ABC News. The U.S. ABC News maintains a content sharing agreement with the Nine Network, which also broadcasts GMA domestically in the early morning before its own breakfast program.
In New Zealand, ABC World News was broadcast daily at 5:10 p.m. and at again at 11:35 p.m. As with the BBC in the U.K., TVNZ 7 (owned by Television New Zealand) aired the program commercial-free, until the channel ceased operations on June 30, 2012.


== See also ==
ABC World News Tonight
ABC News Radio
American Broadcasting Company
ESPN
CNN
Diane Sawyer
Good Morning America
This Week
The Walt Disney Company


== References ==


== External links ==
Official website