ABC News and Current Affairs is the name of the division of the Australian Broadcasting Corporation (ABC) that controls content classified as news, public affairs and business and finance.
However, the other divisions of the ABC also produce a range of programming within these genres. All such content is covered here.


== ABC Television and ABC Online VOD ==


=== News ===
ABC News
ABC News 24
The Midday Report
ABC News Breakfast
The World
Afternoon Live


=== News Online ===
ABC News in 90 Seconds [1]
ABC World in 90 Seconds [2]
ABC News Business [3]
ABC News Sport [4]
ABC News Entertainment [5]
ABC News Weather [6]


=== Current affairs ===
The 7.30 Report
Lateline
Australian Story
Foreign Correspondent
Four Corners
Insiders
Capital Hill
Stateline
Media Watch
Offsiders


=== Discussion ===
Q&A
The Drum
Big Ideas [7]


=== Business and finance ===
Lateline Business
Inside Business
Business Today [8]


=== Science ===
Catalyst


=== Regional ===
Landline
Australia Wide


=== Entertainment ===
At The Movies


=== Asia Pacific ===
Asia Pacific Focus (aka "Focus") [9]
ABC News for Australia Network (aka "Australia Network News" / "ABC Asia Pacific News")
Newsline [10]
Business Today [11]


=== Children ===
Behind the News
News On 3


== ABC Radio programs ==


=== Current affairs ===
AM
PM
The World Today
Correspondents Report [12]


=== ABC Radio National ===
360 [13]
Awaye! [14]
Australia Talks [15]
Artworks [16]
Background Briefing [17]
Big Ideas [18]
Breakfast
EdPod [19]
ForaRadio [20]
Future Tense [21]
The Health Report [22]
The Law Report [23]
Late Night Live
The National Interest [24]
Saturday Extra [25]
The Science Show [26]


=== ABC Newsradio ===
StarStuff [27]
Health Minutes [28]


=== Radio Australia ===
24 hrs dans le Pacifique [29]
Asia Pacific [30]
Asia Pacific Business [31]
Asia Review [32]
Bay Vut Tin tức [33]
Connect Asia [34]
Correspondent's Notebook [35]
Innovations [36]
Pacific Beat [37]
Other programs in Mandarin, Indonesian, Tok Pisin, Khmer and French.


=== triple j ===
Hack
triple j news [38]
triple j music news [39]
Sunday Night Safran [40]


=== ABC Rural ===
National Rural News [41]
The Country Hour [42]
Bush Telegraph
Country Breakfast [43]
Rural Reporte] [44]
The Resources Beat [45]


=== ABC Local Radio ===
Nightlife
Overnights [46]
Speaking Out [47]
Sunday Nights [48]
Sunday Profile [49]
Various local programs


== ABC Current Affairs ==
The ABC produces many current affairs programmes, including The 7.30 Report and Lateline. These programmes often use resources or reports from one another. For instance, an Asia-Pacific-based report from the week's Foreign Correspondent will be edited for use during that same week's Asia-Pacific Focus programme.
News reports from ABC News are also edited, often with new material added on, for use on that night's Lateline.
The ABC's Current Affairs department has also won a number of awards over the years.


== ABC Public Affairs ==
Apart from the usual coverage of Public Affairs & Politics during programmes such as ABC News, Lateline and The 7.30 Report, the ABC has a dedicated hour-long Public Affairs programme every Sunday at 9am, Insiders, which is hosted by Barrie Cassidy. The programme comprises a news update, an interview with a prominent political figure (usually a Federal Government minister or Opposition spokesperson), two reviews of the week (one of political cartoons, the other of the political situation) and a discussion of the week in politics and public affairs with a panel of guests, most of whom are from prominent newspapers.
The programme is produced in Melbourne, hence the use of the nearby Australia Network ABC News set for both the news updates and Inside Business (which follows Insiders).


== External links ==
Official website
ABC News and Current Affairs programs
ABC Bureaux and Foreign Correspondents
50 Years of ABC TV News and Current Affairs