ABC Online is the brand name in Australia for the online services of the Australian Broadcasting Corporation, managed by ABC Innovation. It covers a large network of websites including those for ABC News, television, radio, and video-on-demand through ABC iView.
ABC Online is one of Australia's largest and most-visited web sites, according to Alexa the eleventh-most popular in the country. It currently contains over 5 million pages.


== History ==

The ABC established its original Multimedia Unit in 1995, to manage the Corporation's website (launched in August that year). Although the unit at first relied upon funding allocation to the Corporation's television and radio operations, in subsequent budgets it began to receive its own. The ABC provided live, online election coverage for the first time in 1996, and limited news content began to be provided in 1997.
ABC Online became a part of ABC New Media and Digital Services when the Multimedia division was renamed and changed to an 'output division' similar to Television or Radio. 'Broadband news' services were introduced in 2001.
In 2008, Crikey reported that certain ABC Online mobile sites in development were planned to carry commercial advertising. Screenshots, developed in-house, of an ABC Grandstand sport page include advertising for two private companies. Media Watch later revealed that the websites were to be operated by ABC Commercial and distinguished from the main, advertising-free, mobile website by a distinct logo.


== See also ==
Australian Broadcasting Corporation
ABC Innovation
ABC iView
ABC Rural


== References ==