Lucid Absinthe Supérieure is a traditional, French-made absinthe verte (green absinthe), whose formula was first approved in 2006. It was granted a COLA (Certificate of Label Approval) in the United States on March 5, 2007, making it the first genuine absinthe to gain approval for legal distribution in the U.S. since 1912.
Lucid is produced by Combier, SA (Saumur, France) for Viridian Spirits, LLC, New York. It is distilled using traditional French methods, and contains no artificial dyes or additives. Lucid satisfies the U.S. Alcohol and Tobacco Tax and Trade Bureau mandate that any finished food or beverage tests less than 10ppm or 10 mg/kg - well under the 35 mg/kg content that is legal within the European Union - for Thujone, pursuant to 21 CFR 172.510. The Lucid brand revived the antique term "Absinthe Supérieure" to differentiate itself from the negative connotations of absinthe that persisted in the TTB. The recipe includes Grande Wormwood (Artemisia absinthium), along with green anise, sweet fennel, and other herbs, and was developed by T.A. Breaux, an absinthe expert and historian. By early 2008, Lucid became available in most states, and has since been granted distribution in Canada and the EU.


== Notes ==


== External links ==
The Official Lucid Homepage