Accommodation (Acc) is the process by which the vertebrate eye changes optical power to maintain a clear image or focus on an object as its distance varies.
Accommodation acts like a reflex, but can also be consciously controlled. Mammals, birds and reptiles vary the optical power by changing the form of the elastic lens using the ciliary body (in humans up to 15 dioptres). Fish and amphibians vary the power by changing the distance between a rigid lens and the retina with muscles.

The young human eye can change focus from distance (infinity) to 6.7 cm from the eye in 350 milliseconds. This dramatic change in focal power of the eye of approximately 15 dioptres (the reciprocal of focal length in metres) occurs as a consequence of a reduction in zonular tension induced by ciliary muscle contraction. The amplitude of accommodation declines with age. By the fifth decade of life the accommodative amplitude has declined so that the near point of the eye is more remote than the reading distance. When this occurs the patient is presbyopic. Once presbyopia occurs, those who are emmetropic (do not require optical correction for distance vision) will need an optical aid for near vision; those who are myopic (nearsighted and require an optical correction for distance vision), will find that they see better at near without their distance correction; and those who are hyperopic (farsighted) will find that they may need a correction for both distance and near vision. The age-related decline in accommodation occurs almost universally to less than 2 dioptres by the time a person reaches 45 to 50 years, by which time most of the population will have noticed a decrease in their ability to focus on close objects and hence require glasses for reading or bifocal lenses. Accommodation decreases to about 1 dioptre at the age of 70 years. The dependency of accommodation amplitude on age is graphically summarized by Duane’s classical curves 


== Theories of mechanism ==
Helmholtz—The most widely held theory of accommodation is that proposed by Hermann von Helmholtz in 1855. When viewing a far object, the circularly arranged ciliary muscle relaxes allowing the lens zonules and suspensory ligaments to pull on the lens, flattening it. The source of the tension is the pressure that the vitreous and aqueous humours exert outwards onto the sclera. When viewing a near object, the ciliary muscles contract (resisting the outward pressure on the sclera) causing the lens zonules to slacken which allows the lens to spring back into a thicker, more convex, form.
Schachar—Ronald Schachar has proposed an alternative theory which indicates that focus by the human lens is associated with increased tension on the lens via the equatorial zonules; that when the ciliary muscle contracts, equatorial zonular tension is increased, causing the central surfaces of the crystalline lens to steepen, the central thickness of the lens to increase (anterior-posterior diameter), and the peripheral surfaces of the lens to flatten. While the tension on equatorial zonules is increased during accommodation, the anterior and posterior zonules are simultaneously relaxing. As a consequence of the changes in lens shape during human in vivo accommodation, the central optical power of the lens increases and spherical aberration of the lens shifts in the negative direction. Because of the increased equatorial zonular tension on the lens during accommodation, the stress on the lens capsule is increased and the lens remains stable and unaffected by gravity. The same shape changes that occur to the crystalline lens during accommodation are observed when equatorial tension is applied to any encapsulated biconvex object that encloses a minimally compressible material (volume change less than approximately 3%) and has an elliptical profile with an aspect ratio ≤ 0.6 (minor axis/major axis ratio). Equatorial tension is very efficient when applied to biconvex objects that have a profile with an aspect ratio ≤ 0.6. Minimal equatorial tension and only a small increase in equatorial diameter causes a large increase in central curvature. This explains why the aspect ratio of a vertebrate crystalline lens can be used to predict the qualitative amplitude of accommodation of the vertebrate eye. Vertebrates that have lenses with aspect ratios ≤ 0.6 have high amplitudes of accommodation; e.g., primates and falcons, while those vertebrates with lenticular aspect ratios > 0.6 have low amplitudes of accommodation; e.g. owls and antelopes. The decline in the amplitude of accommodation eventually results in the clinical manifestation of presbyopia As the equatorial diameter of the lens continuously increases over life, baseline zonular tension simultaneously declines. This results in a reduction in baseline ciliary muscle length that is associated with both lens growth and increasing age. Since the ciliary muscle, like all muscles, has a length-tension relationship, the maximum force the ciliary muscle can apply decreases, as its length shortens with increasing age. This is the etiology of the age-related decline in accommodative amplitude that results in presbyopia.
Catenary—D. Jackson Coleman proposes that the lens, zonule and anterior vitreous comprise a diaphragm between the anterior and vitreous chambers of the eye. Ciliary muscle contraction initiates a pressure gradient between the vitreous and aqueous compartments that support the anterior lens shape in the mechanically reproducible state of a steep radius of curvature in the center of the lens with slight flattening of the peripheral anterior lens, i.e. the shape, in cross section, of a catenary. The anterior capsule and the zonule form a trampoline shape or hammock shaped surface that is totally reproducible depending on the circular dimensions, i.e. the diameter of the ciliary body (Müeller’s muscle). The ciliary body thus directs the shape like the pylons of a suspension bridge, but does not need to support an equatorial traction force to flatten the lens.


=== Induced effects of accommodation ===
When humans accommodate to a near object, they also converge their eyes and, as a result, constrict their pupils. However, the constriction of the pupils is not part of the process called lens accommodation. The combination of these three movements (accommodation, convergence and miosis) is under the control of the Edinger-Westphal nucleus and is referred to as the near triad, or accommodation reflex. While it is well understood that proper convergence is necessary to prevent diplopia, the functional role of the pupillary constriction remains less clear. Arguably, it may increase the depth of field by reducing the aperture of the eye, and thus reduce the amount of accommodation needed to bring the image in focus on the retina.
There is a measurable ratio between how much convergence takes place because of accommodation (AC/A ratio, CA/C ratio). Abnormalities with this can lead to many binocular vision problems.


== Accommodative dysfunction ==
Duke-Elder classified a number of accommodative dysfunctions:
Accommodative insufficiency
Ill-sustained accommodation
Accommodative infacility
Paralysis of accommodation
Spasm of accommodation


== See also ==


=== Disorders of and relating to accommodation ===
Accommodative esotropia
Accommodative spasm
Latent hyperopia
Myopia
Presbyopia
Pseudomyopia


=== Other ===
Accommodation in fish
Adaptation (eye)
Amplitude of accommodation
Cycloplegia
Cyclospasm
Edinger-Westphal nucleus
Mandelbaum Effect
Negative relative accommodation
Positive relative accommodation


== References ==


== External links ==
oph/723 at eMedicine—"Presbyopia: Cause and Treatment"
Ocular Accommodation at the US National Library of Medicine Medical Subject Headings (MeSH)