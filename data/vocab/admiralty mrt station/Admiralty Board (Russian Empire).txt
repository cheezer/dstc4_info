Admiralty Board (Russian: Адмиралтейств-коллегия, Admiralteystv-Kollegiya) was a supreme body for the administration of the Imperial Russian Navy in the Russian Empire, established by Peter the Great on December 12, 1718, and headquartered in the Admiralty building, Saint Petersburg.
The responsibilities of the Admiralty Board had been changing throughout its history. It supervised the construction of military ships, ports, harbors, and canals and administered Admiralty Shipyard. The Admiralty Board was also in charge of naval armaments and equipment, preparation of naval officers etc. The first president of the Admiralty Board was Count Fyodor Apraksin. In 1720, the Admiralty Board published a collection of naval decrees called Книга - устав морской о всем, что касается доброму управлению в бытность флота в море (A Naval Charter On Everything That Has To Do With Good Management Of A Fleet At Sea), authored by Peter the Great himself among other people. In 1802, the Admiralty Board became a part of the Ministry of the Navy. Along with the Admiralty Board, there was also the Admiralty Department in 1805–1827 with the responsibilities of the Chief Office of the Ministry. In 1827, the Admiralty Board was turned into the Admiralty Council (Адмиралтейств-совет), which would exist until the October Revolution of 1917.
In new Russia (Russian Federation) the historic Admiralty Board has been reborn as the Maritime Board (Morskaya Kollegiya) having broad functions to coordinate Russia's maritime future.


== See also ==
Admiralty building, Saint Petersburg
Admiralty Shipyard
List of Russian Admirals


== External links ==
English site on the Admiralty