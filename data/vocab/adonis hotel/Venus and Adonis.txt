Venus and Adonis, a classical myth, was a common subject for art during the Renaissance and Baroque eras. Some works which have been titled Venus and Adonis are:


== Literary works ==
Venus and Adonis, a mythological episode from Book X of Ovid's Metamorphoses, and the inspiration for many of the following works
Venus and Adonis (Shakespeare poem), a poem by William Shakespeare
Venus and Adonis (opera), an opera by John Blow
Venus and Adonis (Constable poem), a poem by Henry Constable
Vénus et Adonis, an opera by Henry Desmarest
Venus und Adonis, an opera by Hans Werner Henze


== Paintings ==
A series of paintings by Titian:
Venus and Adonis (Titian, Madrid) (1553)
Venus and Adonis (Titian, London) (1555)
Venus and Adonis (Titian, Malibu) (c. 1555–1560)
Venus and Adonis (Titian, Oxford) (c. 1560)
Venus and Adonis (Titian, Rome) (c. 1560)
Venus and Adonis (Titian, New York) (c. 1560)
Venus and Adonis (Titian, Washington) (c. 1560)

Venus and Adonis (Veronese, Augsburg) (1562)
Venus and Adonis (Veronese, Madrid) (1580)


== See also ==
Venus (mythology)
Adonis