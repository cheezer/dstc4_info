Nat Geo People, formerly known as Adventure One (A1) and National Geographic Adventure (commonly abbreviated to Nat Geo Adventure), is a subscription TV channel part of National Geographic Channels International and 21st Century Fox. Targeted at female audiences, with programming focusing on people and cultures, the channel is available in 50 countries in both linear and non-linear formats.


== History ==

Adventure One (A1) launched on January 1, 1994, being rebranded on May 1, 2007 as National Geographic Adventure, stengthening the overall Nat Geo presence. All countries adopted the change, except the United Kingdom which instead changed A1 to Nat Geo Wild. Nat Geo Adventure is also a global adventure travel video and photography portal, launching worldwide in 2009.[1]
Nat Geo Adventure was aimed at younger audiences, providing programming based around outdoor adventure, travel and stories involving people having fun while exploring the world.
In early 2008, National Geographic Adventure Channel Australia and National Geographic Adventure Channel Italy launched a new video sharing feature on their website called Blognotes.
In 2010, Nat Geo Adventure launched their High Definition channel (HD) in Asia via AsiaSat 5.
It launched in HDTV on SKY Italia channel 410 from February 1, 2012. At the time of launch (0500 GMT) the Standard Definition version of the channel will be closed down.

On September 30, 2013 it was announced that in early 2014 Nat Geo Adventure would be replaced by Nat Geo People. This would see Nat Geo People launch in HD where currently available, as well as in linear and non-linear formats, in 50 countries internationally. The change will see the channel become more female focused, with programming to focus on people and cultures.
The channel is available in 40 countries around the world


== Programmes ==

62 Days at Sea
A World Apart
Adventure Wanted
Amazing Adventures of a Nobody UK
Amazing Adventures of a Nobody: Europe
Amazing Adventures of a Nobody
Around the World for Free
Art of Walking
Banged Up Abroad
Bite Me With Dr. Mike Leahy
Bluelist Australia
Bondi Rescue
By Any Means
Calcutta Or Bust
Chasing Che: Latin America On A Motorcycle
Chasing Time
City Chase
City Chase Marrakech
Cooking The World
Cruise Ship Diaries
Cycling Home From Siberia With Rob Lilwall
Danger Men
David Rocco's Amalfi Getaway
David Rocco's Dolce Vita
Deadliest Journeys
Departures
Delinquent Gourmet
Destination Extreme
Dive Detectives
Don't Tell My Mother
Earth Tripping
Eccentric UK
Endurance Traveller
Exploring The Vine
Extreme Tourist Afghanistan
Finding Genghis
First Ascent
Food Lovers Guide to the Planet
Food School
Geo Sessions
Going Bush
Gone to Save the Planet
Graham's World
Into The Drink
Keeping up with the Joneses
Kimchi Chronicles
Lonely Planet: Roads Less Travelled
Long Way Down
Madagascar Maverick
Madventures
Making Tracks
Market Values
Meet The Amish
Meet The Natives
Miracle on Everest
Motorcycle Karma
Naked Lentil
Nomads
On Surfari
On The Camino De Santiago
One Man & His Campervan
Perilous Journeys
Pressure Cook
Race to the Bottom of The Earth
Racing Around The Clock
Racing To America
Reverse Exploration
Sahara Sprinters
Solo
Somewhere in China
Surfer's Journal
The Best Job in the World
The Frankincense Trail
The Green Way Up
The Music Nomad
The Ride
Travel Madness
Treks in a Wild World
Ultimate Traveller
Warrior Road Trip
Wedding Crasher: The Real Deal
Weird And Wonderful Hotels
Wheel2Wheel
Which Way To
Wild Ride
Word Travels
Word of Mouth
Young Global Hotshots


== See also ==
National Geographic Channel
National Geographic Channel Australia


== References ==

Nat Geo Adventure Channel Australia
Nat Geo Adventure Channel News on Indian Television
Nat Geo Adventure Channel News on Manila Times
NGC Australia Press Room Nat Geo Adventure Launched


== External links ==
Nat Geo Adventure International Portal
Nat Geo Adventure Channel Australia
Nat Geo Adventure Channel Asia
Nat Geo Adventure Middle East
Nat Geo Adventure Italy
Nat Geo Adventure Germany
Nat Geo Adventure UK
Nat Geo Adventure Spain
Nat Geo Adventure Portugal
Nat Geo Adventure Romania
Nat Geo Adventure Travel and Video Portal