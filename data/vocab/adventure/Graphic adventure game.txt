A graphic adventure game is a form of adventure game. They are distinct from text adventures. Whereas a player must actively observe using commands such as "look" in a text-based adventure, graphic adventures revolutionized gameplay by making use of visual human perception. Eventually, the text parser interface associated with older interactive fiction games was phased out in favor of a point-and-click interface, i.e., a game where the player interacts with the game environment and objects using an on-screen cursor. In many of these games, the mouse pointer is context sensitive in that it applies different actions to different objects.


== History ==


=== Early years ===
Graphic adventure games were introduced by a company called On-Line Systems, which later changed its name to Sierra On-Line. After the rudimentary Mystery House (1980), and the first color adventure game Wizard and the Princess (1980), they established themselves with the full adventure King's Quest (1984), appearing on various systems, and went on to further success with a variety of strong titles.
A number of games were released on 8-bit home computer formats in the 1980s that advanced on the text adventure style originated with games like Colossal Cave Adventure and, in a similar manner to Sierra, added moveable (often directly-controllable) characters to a parser or input-system similar to traditional adventures. Examples of this include Accolade's Killed Until Dead Gargoyle Games's Heavy on the Magick (1986), which has a text-input system with an animated display screen, the later Magic Knight games such as Spellbound (1985), which uses a window-menu system for text-adventure style input, the original PC-6001 version of Yūji Horii's murder mystery game Portopia Serial Murder Case (1983), and Hideo Kojima's classic Snatcher (1988).


=== Point-and-click adventure ===

A new kind of graphic adventure emerged following the introduction of the point-and-click interface. The earliest example is the little-known Planet Mephius, authored by Eiji Yokoyama and published by T&E Soft only in Japan for the FM-7 computer in July 1983; alongside a command menu system, it introduced a point-and-click interface, utilizing a cursor to interact with objects displayed on the screen. A similar point-and-click cursor interface was later used in the adventure game Wingman, another title released only in Japan, for the NEC PC-8801 in 1984.
From 1984, mouse-controlled graphic adventures began emerging following the launch of the Apple Macintosh, with its mouse-controlled point-and-click interface. The first adventure game to take advantage of the Mac's point-and-click interface was the innovative but relatively unknown Enchanted Scepters released the same year, followed in 1985 with the ICOM Simulations game Deja Vu that completely banished the text parser for a point-and-click interface. That same year, the NES version of Chunsoft's Portopia Serial Murder Case worked around the console's lack of keyboard by taking advantage of its D-pad to replace the text parser of the original 1983 PC-6001 version with a cursor interface for the NES version. The following year, Square's Suishō no Dragon on the NES took it a step further with its introduction of visual icons and animated scenes.
In 1987, ICOM's second follow-up Shadowgate was released, and LucasArts also entered the field with Maniac Mansion, a point-and-click adventure that gained a strong following. A prime example of LucasArts' work is the Monkey Island series. In 1988, popular adventure game publisher Sierra Online created Manhunter: New York. It marked a major shift for Sierra, having used a text parser for their adventure games akin to text adventures. Another famous point-and-click graphic adventure game was Hideo Kojima's Policenauts (1994). Point-and-click was used in survival horror games such as Human Entertainment's Clock Tower series, which quickly became popular following its first release in 1995; it later branched off into a sequel and a spin-off.
Graphic adventure games were quick to take advantage of the storage possibilities of the CD-ROM medium and the power of the Macromedia Director multimedia-production software. Games such as Alice (1990), Spaceship Warlock (1991), The Journeyman Project (1993), and Iron Helix (1993) incorporated pre-rendered 3D elements and live-action video.
In 1993, Day of the Tentacle, a sequel to Maniac Mansion, was released. It featured the original game as an Easter egg. Its success allows lead game designer Tim Schafer to produce the 1995 hit Full Throttle, featuring the voice talent of Roy Conrad and Mark Hamill.
Space Quest IV became the first in the popular series to feature a point-and-click interface. King's Quest V was the first for its series. Eventually, the first games in both series would be remade in the point-and-click format with VGA graphics.
Other notable point-and-click adventure games include Zak McKracken and the Alien Mindbenders (1988), Loom (1990), Indiana Jones and the Fate of Atlantis (1992), The Legend of Kyrandia (1992), Freddy Pharkas: Frontier Pharmacist (1993), Sam & Max Hit the Road (1993), Gabriel Knight: Sins of the Fathers (1993), Beneath a Steel Sky (1994), The Dig (1995), Full Throttle (1995), Torin's Passage (1995), Phantasmagoria (1995), I Have No Mouth and I Must Scream (1995), Beavis and Butt-head in Virtual Stupidity, Broken Sword: The Shadow of the Templars (1996), Blade Runner (1997), Hollywood Monsters (1997) and Carmen Sandiego's Great Chase Through Time (1997).


=== Dialogue-based adventure ===
A variant on the graphic adventure involves many of the clues relying upon extensive dialogue between characters within the game. This dialogue not only extends the plot, but also adds development to the characters and settings as well as allowing for more complex puzzles. Examples include: The Longest Journey and Return of the Phantom.


=== First-person adventure ===

The 1980s also saw the development of first-person-adventure games, similar to point-and-click adventure games, but using a first-person perspective, often featuring limited or no other characters.
In April 1993, The 7th Guest garnered widespread attention as a horror-themed puzzle game in first person and led Microsoft founder Bill Gates to proclaim the game as "the new standard in interactive entertainment." The game's sequel, The 11th Hour, was released in 1995 to mixed reviews.
In the fall of 1993, Myst was released to even greater acclaim and eventually reached 6 million sales, making it one of the best selling PC games of all time. Various sequels to Myst were later published, starting with Riven in 1997.
Another notable first-person-adventure game is Lighthouse.


=== Decline and rebirth ===

The genre has since seen a relative decline, since the late 1990s, notably in the United States; graphic adventures remain popular in Japan and Europe. Reasons for the decline involve the ability for computer hardware to play more graphically and gameplay advanced action games such as first-person shooters, and the advent of online gaming where players can play against other gamers online. Such online features are irrelevant to adventure gaming. The popularity and sales of these games have made publishers less inclined to fund development teams making graphic adventures for fear of bad sales.
Notable events included Sierra almost entirely shutting down its studio in 1999, and LucasArts ceasing publication after 2000; the Tex Murphy franchise was also shelved after 1998. Commercially, Grim Fandango (1998) by LucasArts was considered a failure, selling under 100,000 copies in the 5 years after launch – compare with over 500,000 sales of King's Quest V (1990) less than a decade earlier – and while LucasArts published one further adventure game (Escape from Monkey Island in 2000), it canceled remaining games, dismissed most of the teams involved in 2004, and in 2006 declared that it was exiting the market for the time being and did not plan to make adventure games for another decade.
Recently however independent users have created many smaller graphic adventure games in Adobe Flash, such as the series The Several Journeys of Reemus and the Submachine series. Many of these challenge the player to interact with objects in an environment. These form very short and basic point-and-click adventure games. A popular subgenre is known as escape the room games.
The graphic adventure genre has seen a rebirth with the introduction of new video game hardware like the Nintendo DS, and Wii, that allows the gamer to interact with the game similarly to using a computer mouse. As a result, many developers have developed new graphic adventures for these platforms.
Recent examples of graphic adventures include Zack & Wiki: Quest for Barbaros' Treasure for the Wii, Ceville for the PC, Broken Sword: The Shadow of the Templars for the Nintendo DS, as well as games developed by Telltale Games, founded by former LucasArts employees.
Some recent adventure games have made attempts to revitalize and reinvent the adventure game genre by blending new technologies, interfaces, and gameplay elements into it. RealMyst and several other recent Myst games took the Myst series into realtime 3D, and Myst Online: Uru Live included multiplayer functionality and physics-based puzzles. Dreamfall, Portal, and many other games have mixed action elements with elements of the adventure genre, blurring genre lines. Some recent adventure games, including Machinarium and some of the titles by Telltale Games, most notably The Walking Dead, have integrated a variety of hint systems into their game design to make the genre more accessible to players. Also, recent interactive movies like Indigo Prophecy and Heavy Rain bears resemblance with graphic adventures.
The genre may have a new revival, thanks to new technologies such as virtual reality and motion controllers. Loading Human seems to be an example of what is to be expected from adventure games of the future.
In 2016, Maniac Mansion co-creators Ron Gilbert and Gary Winnick will release Thimbleweed Park, a point-and-click adventure game in the style of early LucasArts adventure games. Musician and comedian Neil Cicierega is also working on a point-and-click game, Icon Architect.


=== Kickstarter projects ===
In February 2012, Double Fine launched a Kickstarter campaign to fund their Double Fine Adventure. After the success of their fundraising campaign, other critically acclaimed developers have launched similar Kickstarter projects to secure crowd funding for adventure games: Al Lowe and Replay Games to finance an up-to-date port of the 1987 adventure game Leisure Suit Larry, Gabriel Knight creator Jane Jensen and Pinkerton Road to finance Moebius, and Space Quest creators Mark Crowe and Scott Murphy ("Two Guys from Andromeda") to finance their SpaceVenture. Big Finish Games, founded by Access Software veterans Chris Jones and Aaron Conners, launched a successful Kickstarter campaign to revive their Tex Murphy series. Revolution Software announced Broken Sword: The Serpent's Curse was to have its development completed by funding from Kickstarter. Ragnar Tørnquist, writer and designer of adventure games The Longest Journey and Dreamfall, started a crowd funding campaign at Kickstarter for Dreamfall Chapters on 8 February 2013. In May 2013, Doug TenNapel (creator of The Neverhood) and Pencil Test Studios (in-game animation Earthworm Jim) started a Kickstarter campaign to fund Armikrog, a cross-platform claymation point and click adventure game in the same vein as The Neverhood.


== Japanese development ==
In Japan this is known as a comic adventure, in which many of the graphic designers who worked on comic books (manga) and animation were able to use their talent on the computer, such as in the case of Metal Slader Glory. Thanks to the efforts of various artists in conjunction with the popularity of Dungeons & Dragons type role playing games, many revolutionary programs such as Deluxe Paint and Photoshop were actually put to use on actually hand-drawn images rather than typical photographs. The comic adventure games is part of the reason why the popularity of point-and-click and FMV games was able to survive.


== Parody ==
The creators of Homestar Runner developed a game known as Peasant's Quest. The game is mostly a parody of the first King's Quest game, but also references the second through fourth games in the series. Peasant's Quest also somewhat references Black Cauldron. In this game, the "hero" is a peasant who swears revenge upon a mighty, rampaging dragon known as Trogdor (created by Strong Bad in the email "dragon") after Trogdor "burninates" his thatched roof cottage.


== See also ==
List of graphic adventure games
Visual novel


== References ==


== Further reading ==
Richard Moss (2011). A truly graphic adventure: the 25-year rise and fall of a beloved genre". Ars Technica.