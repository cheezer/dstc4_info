African(s) may refer to:
Anything from or pertaining to Africa:
African people, people who live in Africa or trace their ancestry to indigenous inhabitants of Africa
Ethnic groups of Africa

African culture
African languages
African music

African grey parrot, an Old World parrot
"African" a song by Peter Tosh from his 1977 album Equal Rights
The African (short story), autobiographical story by French author J. M. G. Le Clézio

In publishing
African Business, a monthly magazine covering business events across Africa
African Communist, a political magazine published in Johannesburg
African Film (magazine), Nigerian weekly magazine


== See also ==
All pages with titles containing "African"
Africana (disambiguation)
Africanus (disambiguation)
African American
African British
Afro-Caribbean
Afro-Bahamian