The A-train (from Afternoon Train) is a satellite constellation of six Earth observation satellites of varied nationality in sun-synchronous orbit at an altitude of 705 kilometers above the Earth.
The orbit, at an inclination of 98.14°, crosses the equator each day at around 1:30 pm solar time, giving the constellation its name; the "A" stands for "afternoon;" and crosses the equator again on the night side of the Earth, at around 1:30 am.
They are spaced a few minutes apart from each other so their collective observations may be used to build high-definition three-dimensional images of the Earth's atmosphere and surface.


== Satellites ==


=== Active ===
The train, as of July 2014, consists of six active satellites:
OCO-2, lead spacecraft in formation, replaces the failed OCO and was launched for NASA on July 2, 2014.
GCOM-W1 "SHIZUKU", follows OCO-2 by 11 minutes, launched by JAXA on May 18, 2012.
Aqua, runs 4 minutes behind GCOM-W1, launched for NASA on May 4, 2002.
CloudSat, a cooperative effort between NASA and the Canadian Space Agency, runs 2 minutes and 30 seconds behind Aqua, launched with CALIPSO on April 28, 2006.
CALIPSO, a joint effort of CNES and NASA, follows CloudSat by no more than 15 seconds, launched on April 28, 2006.
Aura, a multi-national satellite, lags Aqua by 15 minutes, crossing the equator 8 minutes behind due to different orbital track to allow for synergy with Aqua, launched for NASA on July 15, 2004.


=== Past ===
PARASOL, launched by CNES on December 18, 2004 and moved to another (lower) orbit on December 2, 2009.


=== Failed ===
OCO, destroyed by a launch vehicle failure on February 24, 2009, and was replaced by OCO-2.
Glory, failed during launch on a Taurus XL rocket on March 4, 2011, and would have flown between CALIPSO and Aura.


== References ==


== External links ==
NASA A-Train Portal
NASA satellite program impacted
NASA Program Page
Orbital Sciences Program Page
L'Ecuyer, T.S.; Jiang, J.H. (2010). "Touring the atmosphere aboard the A-Train". Physics Today 63 (7): 36–41. doi:10.1063/1.3463626.