Agar Malwa district became the 51st district of Madhya Pradesh on 16 August 2013. It is carved out of existing Shajapur district. Agar town will be its headquarters.
The district was formed by excluding Agar, Badod, Susner and Nalkheda tehsils from the former Shajapur District. The population of the district is 4.80 lakh, spread over 2,785 km2. It has two sub-divisions, Agar and Susner. 


== References ==
^ "Agar-Malwa becomes MP's 51st district".