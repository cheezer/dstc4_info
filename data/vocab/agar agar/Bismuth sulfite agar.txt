Bismuth sulfite agar is a type of agar media used to isolate Salmonella species. It uses glucose as a primary source of carbon. BLBG and bismuth stop gram-positive growth. Bismuth sulfite agar tests the ability to utilize ferrous sulfate and convert it to hydrogen sulfide.
Bismuth sulfite agar typically contains (w/v):

1.6% bismuth sulfite Bi2(SO3)3
1.0% pancreatic digest of casein
1.0% pancreatic digest of animal tissue
1.0% beef extract
1.0% glucose
0.8% dibasic sodium phosphate
0.06% ferrous sulfate • 7 water
pH adjusted to 7.7 at 25 C

This medium is filter-sterilized, not autoclaved.


== References ==