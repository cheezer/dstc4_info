Muhamad Faisal bin Abdul Manap (born 6 June 1975) is a Singaporean politician from the Workers' Party of Singapore (WP) who has been serving as a Member of Parliament in Singapore since May 2011. He is currently a Member of Parliament (MP) of the Aljunied Group Representation Constituency (Aljunied GRC) in Singapore in charge of the Kaki Bukit division.


== Political career ==
Muhamad Faisal entered the Singapore political arena in the Singapore 2011 General Election contesting as a minority candidate in the hot-ward 5-man district Aljunied GRC representing Team-A of the Workers' Party of Singapore which was led by veteran non-ruling parties leader party-Secretary General cum MP Low Thia Khiang together with ex-NCMP Sylvia Lim Swee Lian, Chen Show Mao and Pritam Singh. They challenged the incumbent People's Action Party team which was led by ex-Minister George Yeo, ex-Minister of State Lim Hwee Hua, ex-Minister of State Zainul Abidin Bin Mohamed Rasheed, Cynthia Phua and rookie high-flying civil servant Ong Ye Kung. The Workers' Party team won with 54.72% of the votes and captured a Group Representation Constituency (GRC) for the first time in the political history of Singapore. Manap was officially declared for the first time as a Member of Parliament of the 12th Parliament of Singapore on 9 May 2011.


== Political Terms of Office ==
12th Parliament of Singapore ( May 2011 - current ) Aljunied GRC


== Education ==
Primary School Leaving Examination Eunos Primary School (1987)
GCE 'O' Levels Telok Kurau Secondary School (1991)
Diploma in civil and structural engineering from Singapore Polytechnic (1995)
Bachelor of Science (Psychology) from Monash University (2005)


== References ==
Singapore Parliament website - Member's CV : Mr Muhamad Faisal Bin Abdul Manap