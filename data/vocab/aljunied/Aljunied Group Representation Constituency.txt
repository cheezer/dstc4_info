Not to be confused with the smaller constituency with similar name of Aljunied Single Member Constituency, which existed since the pre-independence era. This article is describing a present ward.
Aljunied Group Representation Constituency (Traditional Chinese: 阿裕尼集選區; Simplified Chinese: 阿裕尼集选区) is a five-member group representation constituency (GRC) in the north-eastern region of Singapore. The GRC consists of a large part of Hougang, Serangoon Gardens, a portion of Bedok and Aljunied. Located within Aljunied GRC is an enclave single member constituency known as Hougang Single Member Constituency (SMC) which is currently manned by an opposition Member of Parliament.
Aljunied GRC was formed in 1988 and was held by the People's Action Party (PAP) till 2011. Currently, Aljunied GRC is led by the Secretary-General of the Workers' Party, Low Thia Khiang. This is the first time a Group Representation Constituency has been won by a party other than the PAP since the introduction of the Group Representation Constituency concept in 1988.


== Members of Parliament ==


== Operational responsibility ==
Bedok Reservoir-Punggol - Bedok Reservoir (Blocks 7xx), Defu, Paya Lebar Airbase and Hougang North (Blocks 4xx and part of 5xx)
Paya Lebar - Hougang South (excluding part of Blk 128 - 137 and 165 - 168) and Upper Paya Lebar Road (part)
Kaki Bukit - Bedok North (Blocks 5xx)
Eunos - Bedok Reservoir (Blocks 6xx and Eunos 1xx), including the Kaki Bukit Techpark
Serangoon - Serangoon North (Blocks 1xx, 211 - 230) and Serangoon Gardens, including all the private housing estates.


== Candidates and Results ==


=== Elections in 2010s ===


=== Elections in 2000s ===


=== Elections in 1990s ===


=== Elections in 1980s ===


== See also ==
Aljunied SMC


== External links ==
1991 General Election's Result
1997 General Election's Result
2001 General Election's Result
2006 General Election's Result
2011 General Election's Result
Background of 1997 General Election to show that Aljunied GRC had absorbed the hotly contested and defunct Eunos GRC
ChannelNewsAsia report on campaigning at Aljunied GRC during the 2011 General Elections
[1]
Lee Kuan Yew urges Muslims to 'be less strict'
Malay Integration: MM stands corrected