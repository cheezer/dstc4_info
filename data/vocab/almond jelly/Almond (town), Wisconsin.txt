Almond is a town in Portage County, Wisconsin, United States. The population was 679 at the 2000 census. The unincorporated community of West Almond is located in the town.


== §Geography ==

According to the United States Census Bureau, the town has a total area of 43.2 square miles (112.0 km²), of which, 43.1 square miles (111.7 km²) of it is land and 0.1 square miles (0.3 km²) of it (0.28%) is water.


== §Demographics ==
As of the census of 2000, there were 679 people, 256 households, and 197 families residing in the town. The population density was 15.8 people per square mile (6.1/km²). There were 282 housing units at an average density of 6.5 per square mile (2.5/km²). The racial makeup of the town was 98.23% White, 0.29% Native American, 0.74% from other races, and 0.74% from two or more races. 3.09% of the population were Hispanic or Latino of any race.
There were 256 households out of which 34.4% had children under the age of 18 living with them, 70.3% were married couples living together, 3.5% had a female householder with no husband present, and 22.7% were non-families. 16.4% of all households were made up of individuals and 6.6% had someone living alone who was 65 years of age or older. The average household size was 2.65 and the average family size was 3.03.
In the town the population was spread out with 26.5% under the age of 18, 5.9% from 18 to 24, 28.1% from 25 to 44, 28.1% from 45 to 64, and 11.3% who were 65 years of age or older. The median age was 39 years. For every 100 females there were 108.3 males. For every 100 females age 18 and over, there were 107.9 males.
The median income for a household in the town was $45,156, and the median income for a family was $52,708. Males had a median income of $37,083 versus $20,962 for females. The per capita income for the town was $17,962. About 4.7% of families and 6.6% of the population were below the poverty line, including 5.2% of those under age 18 and 2.2% of those age 65 or over.


== §Notable people ==
Adam A. Schider, Wisconsin State Assemblyman, farmer, and auctioneer, was born in the town.


== §References ==
^ "American FactFinder". United States Census Bureau. Retrieved 2008-01-31. 
^ 'Wisconsin Blue Book,' Biographical Sketch of Adam A. Schider, pg. 725