James Lindsay Almond, Jr. (June 15, 1898 – April 15, 1986) was a United States federal judge and politician. He served as the 58th Governor of Virginia from 1958 until 1962, and was the last governor of Virginia to have been born in the 19th century.


== Early life ==
Almond was born in Charlottesville, Virginia and raised in Orange County, Virginia. Almond attended Virginia Tech and served as a private in the Students Army Training Corps in 1917 and 1918 in World War I, after which he taught school in Locust Grove, Virginia. He served as a high school principal, and earned an LL.B. from the University of Virginia School of Law in 1923.


== Political career ==
Almond was assistant commonwealth attorney of Roanoke, Virginia from 1930 to 1933, and was a state court judge to the Hustings Court of Roanoke from 1933 to 1945. He was then elected to the U.S. House of Representatives from Virginia's 6th congressional district, serving in the 79th and 80th Congresses.
Almond resigned his Congressional seat in 1948, when he was elected Attorney General of Virginia. He argued the state's case for segregation of public schools before the Supreme Court in the case of Davis v. County School Board of Prince Edward County, which was consolidated with Brown v. Board of Education.
In 1957, he was elected Democratic Governor of Virginia, and took office in January 1958 for a single term that ended in 1962. He succeeded Governor Thomas B. Stanley. One of his notable accomplishments as Governor was ending massive resistance against the desegregation of schools, in opposition to other high-profile southern politicians, such as Virginia Senator Harry F. Byrd and Arkansas Governor Orval Faubus. Heeding the advice of several within his own party, including Senator Mosby G. Perrow, Jr., Almond realized that opposition to desegregation was ultimately futile as the state continued to lose in the courts; when Virginia's Stanley plan, the package of laws which implemented massive resistance, were declared unconstitutional he changed the state's policy, adopting the proposals of the Perrow Commission, and thereby earned the wrath of the Byrd Organization.
Almond had campaigned for President John F. Kennedy, who nominated him to be a judge on the United States Court of Customs and Patent Appeals (CCPA). Senator Byrd blocked his initial appointment, so Kennedy gave him a recess appointment. President Kennedy sent another appointment, and Almond was confirmed 164 days later when Senator Byrd eventually missed a floor session. He took senior status in 1973. By operation of the Federal Courts Improvement Act of 1982, the CCPA was eliminated and Judge Almond was reassigned to the United States Court of Appeals for the Federal Circuit as a senior judge, a position he held until his death in 1986.


=== Elections ===
1946; Almond was elected to the U.S. House of Representatives in a special election unopposed. H was re-elected in the general election with 64.78% of the vote, defeating Republican Frank R. Angell and Socialist Ruby Mae Wilkes.
1957; Almond was elected Governor of Virginia with 63.15% of the vote, defeating Republican Theodore R. Dalton and Independent C. Gilmer Brooks.


== Personal life ==
Almond married Josephine Katherine Minter in 1925. He was a Lutheran and taught a men's bible class. He was a 32nd degree Mason, a Shriner, and a member of Alpha Kappa Psi and Omicron Delta Kappa.


== Death ==
Almond died in 1986 in Richmond, Virginia. He is buried in Evergreen Burial Park in Roanoke, Virginia.


== References ==
^ James Lindsay Almond, Jr. at the Biographical Directory of the United States Congress Retrieved on 2009-9-28
^ James Lindsay Almond, Jr. at the Biographical Directory of Federal Judges, a public domain publication of the Federal Judicial Center. Retrieved on 2009-9-28
^ Brown v. Board of Education of Topeka, 347 U.S. 483 (1954)
^ Almond, J. Lindsay; Larry J. Hackman (02-07-1968). "J. Lindsay Almond Oral History Interview" (PDF). Oral History Project. John F. Kennedy Presidential Library and Museum. Retrieved 2006-08-17.  
^ Rich, Giles S. (1980). A brief history of the United States Court of Customs and Patent Appeals. Washington, D.C.: Published by authorization of Committee on the Bicentennial of Independence and the Constitution of the Judicial Conference of the United States : U.S. G.P.O.