KCSF ("Xtra Sports 1300") is a radio station serving the Colorado Springs area with a sports format. It broadcasts on AM frequency 1300 kHz and is under ownership of Cumulus Media.
The station features CBS Sports Radio, as well as Jim Rome, Denver Nuggets, Colorado Avalanche, and Colorado Springs Sky Sox.


== AM 1300 History ==

The 1300 AM frequency in Colorado Springs was the longtime home of news/talk KVOR until March 2000 when KVOR switched frequencies with then-KTWK on AM 740.
The format became Classic country in December 2000 when the station changed calls to KUBL (Standing for 'The Bull').
The station adopted a progressive talk format in 2002 and adopted the call letters KBZC, calling itself 'The Buzz 1300'
In 2004, the format changed again to a sports radio format, calling itself "Sports Animal 1300" and adopting the call letters KKML. The station signed on as an affiliate of Fox Sports Radio then signed on with ESPN Radio in 2007.
In July 2008, Citadel Broadcasting abandoned the sports format in favor of a 'Classic Country' format as "KCS 1300 AM" to compliment its sister Mainstream Country station KATC-FM and to pay homage to the former "KCS Country" station (see KCS History). This format change also brought Don Imus into the Colorado Springs Market, and used Jones Radio Networks Classic Country Satellite Programming as its lineup. The Jones format was changed in Mid 2009 to Citadel Media's Real Country format, featuring a mix of classic country and country currents.
The most recent change of 1300's format back to sports radio took effect on November 5, 2009, with returning 'Sports Animal 1300' to Colorado Springs. Citadel merged with Cumulus Media on September 16, 2011.


== KCS History ==
KKCS-FM went on the air in Colorado Springs on September 12, 1979 broadcasting a country music format. The station was known as "The Most Country KCS 102." KKCS remained a major competitor in the market for country until purchased by Bustos Media in 2005.
After being purchased, KKCS moved frequencies from 101.9 FM to 104.5 FM (taking that frequency from Canon City, Colorado country station KSTY). With the 104.5 transmitter being located in Canon City, the signal of KCS was severely limited to Colorado Springs. In 2006, the station let go of all personnel and played all music before going dark in 2007. The country format on that frequency has since returned to Canon City with KSTY.


== References ==


== External links ==
KCSF Official Website
Query the FCC's AM station database for KCSF
Radio-Locator Information on KCSF
Query Nielsen Audio's AM station database for KCSF