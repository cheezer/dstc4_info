Amara is a large genus of carabid beetles, commonly called the sun beetles. Many are holarctic, but a few species are neotropical or occur in eastern Asia.
These ground beetles are mostly black or bronze-colored, and many species have a characteristic "bullet-shaped" habitus, as shown in the photos, making them taxonomically difficult for a beginner. They are predominantly herbivorous, with some species known to climb ripening grasses to feed on the seeds. Other species are used as weed control agents. Numerous species are adventive in non-native habitats, particularly species that thrive in synanthropic settings.


== Species ==


== References ==


== External links ==
Amara Bonelli 1810