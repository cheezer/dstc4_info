Amari Torayasu (甘利 虎泰, 1498 – March 23, 1548) was a Japanese samurai of the Sengoku period, and served the Takeda clan under Takeda Nobutora and Shingen. Amari was a shukurō, or clan elder, following Shingen's accession to family headship and was one of "Twenty-Four Generals of Takeda Shingen". He was killed in action at the battle of Uedahara in 1548, together with Itagaki Nobukata. The two were fighting side by side on the frontlines when suddenly a volley of arrows shot them down.
Amari was succeeded by his son Amari Masatada.


== Amari in Fiction ==
In NHK's 2007 Taiga drama Fūrinkazan, Amari is played by Ryū Raita.


== References ==


== Further reading ==
Turnbull, Stephen (1998). 'The Samurai Sourcebook'. London: Cassell & Co.


== External links ==
"Legendary Takeda's 24 Generals" at Yamanashi-kankou.jp