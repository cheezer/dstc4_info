The 2015 Kurume Best Amenity Cup was a professional tennis tournament played on outdoor grass courts. It was the eleventh edition of the tournament and part of the 2015 ITF Women's Circuit, offering a total of $50,000 in prize money. It took place in Kurume, Fukuoka, Japan, on 11–17 May 2015.


== Singles main draw entrants ==


=== Seeds ===
1 Rankings as of 4 May 2015


=== Other entrants ===
The following players received wildcards into the singles main draw:
 Akari Inoue
 Mai Minokoshi
 Ayumi Morita
 Akiko Yonemura
The following players received entry from the qualifying draw:
 Miyu Kato
 Makoto Ninomiya
 Chiaki Okadaue
 Marianna Zakarlyuk


== Champions ==


=== Singles ===

 Nao Hibino def.  Eri Hozumi, 6–3, 6–1


=== Doubles ===

 Makoto Ninomiya /  Riko Sawayanagi def.  Eri Hozumi /  Junri Namigata, 7–6(12–10), 6–3


== External links ==
2015 Kurume Best Amenity Cup at ITFtennis.com
Official Website (Japanese)