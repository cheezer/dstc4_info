A list of films produced in the American film industry from the earliest films of the 1890s to present. Films are listed by year of release on separate pages in alphabetical order. Note: this list has just begun construction.


== 1890s ==
List of American films of the 1890s


== 1900s ==
List of American films of 1900
List of American films of 1901
List of American films of 1902
List of American films of 1903
List of American films of 1904
List of American films of 1905
List of American films of 1906
List of American films of 1907
List of American films of 1908
List of American films of 1909


== 1910s ==
List of American films of 1910
List of American films of 1911
List of American films of 1912
List of American films of 1913
List of American films of 1914
List of American films of 1915
List of American films of 1916
List of American films of 1917
List of American films of 1918
List of American films of 1919


== 1920s ==
List of American films of 1920
List of American films of 1921
List of American films of 1922
List of American films of 1923
List of American films of 1924
List of American films of 1925
List of American films of 1926
List of American films of 1927
List of American films of 1928
List of American films of 1929


== 1930s ==
List of American films of 1930
List of American films of 1931
List of American films of 1932
List of American films of 1933
List of American films of 1934
List of American films of 1935
List of American films of 1936
List of American films of 1937
List of American films of 1938
List of American films of 1939


== 1940s ==
List of American films of 1940
List of American films of 1941
List of American films of 1942
List of American films of 1943
List of American films of 1944
List of American films of 1945
List of American films of 1946
List of American films of 1947
List of American films of 1948
List of American films of 1949


== 1950s ==
List of American films of 1950
List of American films of 1951
List of American films of 1952
List of American films of 1953
List of American films of 1954
List of American films of 1955
List of American films of 1956
List of American films of 1957
List of American films of 1958
List of American films of 1959


== 1960s ==
List of American films of 1960
List of American films of 1961
List of American films of 1962
List of American films of 1963
List of American films of 1964
List of American films of 1965
List of American films of 1966
List of American films of 1967
List of American films of 1968
List of American films of 1969


== 1970s ==
List of American films of 1970
List of American films of 1971
List of American films of 1972
List of American films of 1973
List of American films of 1974
List of American films of 1975
List of American films of 1976
List of American films of 1977
List of American films of 1978
List of American films of 1979


== 1980s ==
List of American films of 1980
List of American films of 1981
List of American films of 1982
List of American films of 1983
List of American films of 1984
List of American films of 1985
List of American films of 1986
List of American films of 1987
List of American films of 1988
List of American films of 1989


== 1990s ==
List of American films of 1990
List of American films of 1991
List of American films of 1992
List of American films of 1993
List of American films of 1994
List of American films of 1995
List of American films of 1996
List of American films of 1997
List of American films of 1998
List of American films of 1999


== 2000s ==
List of American films of 2000
List of American films of 2001
List of American films of 2002
List of American films of 2003
List of American films of 2004
List of American films of 2005
List of American films of 2006
List of American films of 2007
List of American films of 2008
List of American films of 2009


== 2010s ==
List of American films of 2010
List of American films of 2011
List of American films of 2012
List of American films of 2013
List of American films of 2014
List of American films of 2015


== See also ==
Looney Tunes and Merrie Melodies filmography (1929-1969)


== External links ==
American film at the Internet Movie Database