Angkor Mikroheranhvatho (Kampuchea) Co., also known as AMK, is a registered microfinance institution (MFI) headquartered in Phnom Penh, Cambodia with over 280,000 active borrowers.  AMK is the largest provider of credit in Cambodia in terms of borrower numbers and has branches in 23 provinces and Phnom Penh city. It has over 10,000 active savers and over 1,000 employees. AMK provides several microfinance services, including microcredit, microsavings, and mobile money transfers.
AMK offers both group and individual loan products. 87% of borrowers are female, and 93% are group loan borrowers.


== History ==
AMK was established in 2002 as a microfinance company to manage Irish NGO Concern Worldwide’s microfinance activities in Cambodia. By 2003, AMK was functioning independently of Concern and subsequently received its license from the National Bank of Cambodia in 2004. In the following years, AMK experienced rapid growth in its core credit business, extending its branch network to every province in the country. In 2010, it became the largest microfinance institution in Cambodia in terms of client numbers, and by 2011, AMK surpassed all other banks and MFIs to become the largest provider of credit in Cambodia in terms of client numbers.
In 2010, AMK began a strategic transformation from a rural credit-only business into a broader provider of microfinance services. The granting of AMK’s Microfinance Deposit Institution (MDI) license in 2010 represented a key milestone because it allowed AMK to implement several new savings and credit products. Savings products were rolled out to all branches by mid-2011, and a domestic money transfer product was launched in July 2011. AMK began piloting an agent-based mobile banking solution in December 2011.


== Microcredit Methodology ==
The typical AMK customer joins as a Village Bank client who accesses small amounts of credit through AMK’s Group Guaranteed loan products. Currently, 93% of AMK borrowers receive group loans. Clients can choose specific loans based on their income. There are no gender requirements, but women are strongly encouraged to participate. Women currently represent 87% of AMK’s client base.
AMK utilizes a solidarity group lending methodology to administer its group loans. This process begins with potential clients self-selecting themselves into solidarity groups of four to six members. These solidarity groups are then organized into Village Banks that consist of four to twelve groups (or twenty to sixty clients). Being part of a self-selected solidarity group signifies that three to five other villagers trust the loan applicant to let him/her join their group. All loans are guaranteed by the respective group members. Loans are appraised and approved by an AMK Client Officer and the Village Bank President prior to disbursement. Each group nominates a group leader who is in charge of ensuring member attendance at meetings, troubleshooting, and liaising with the VBP, CO, and other members. AMK also offers several individual loan options to its clients. Generally, these require physical collateral and at least one personal guarantor.


== Savings Products ==
AMK offers distinct savings products to encourage clients to save their money safely, securely, and responsibly. These products range from general savings to high interest-earning accounts that require a predetermined time frame.


== Mobile Money Transfers ==
AMK is an innovation leader in the mobile banking arena. AMK is currently piloting a new village-based agent savings mobilization channel. Qualified agents are equipped with mobile phones for communication with AMK’s banking systems. Each phone is equipped with a mobile application in Khmer language to access AMK’s banking system, navigate the application interface, and conduct transactions. Agents offer deposit, withdrawal, and money transfer services to AMK customers. Customers can also conduct money transfers through branch offices.


== Awards ==
Fondazione Giordano Dell’Amore Microfinance International Best Practices Award (2011)
Global Microfinance Highest Customer Orientation Achievement Award (2011)
Microfinance Information Exchange (MIX) Social Performance Reporting Gold award (2010)


== References ==