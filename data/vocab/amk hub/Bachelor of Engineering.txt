The Bachelor of Engineering commonly abbreviated as (B.Eng.) is a undergraduate academic degree awarded to a student after three to five years of studying engineering at university or college. A BEng degree will be accredited by one of the Engineering Council's professional bodies as suitable for registration as a chartered engineer. That might include the British Computer Society. Alternatively, it might be accredited directly by another professional body, such as the Institute of Electrical and Electronics Engineers (IEEE). The BEng is mainly a route to chartered engineer, registered engineer or licensed engineer, and has been approved by representatives of the profession.
BEng has a greater emphasis on technical engineering aspects, for example, electronics and electrical circuits, than the BSc. However, topics covered in BEng can overlap with BSc.
Most universities in the United States and Europe award the Bachelor of Science Engineering (BSc Eng), Bachelor of Engineering (B.Eng), Bachelor of Engineering Science (BESc), Bachelor of Science in Engineering (BSE), or Bachelor of Applied Science (BASc) degree to undergraduate students of engineering study. For example, Canada is the only country that awards the BASc degree for graduating engineers. Other institutions award engineering degrees specific to the area of study, such as BSEE (Bachelor of Science in Electrical Engineering) and BSME (Bachelor of Science in Mechanical Engineering).
A less common variety of the degree is Baccalaureus in Arte Ingeniaria (BAI), a Latin name meaning Bachelor in the Art of Engineering. It is awarded by the University of Dublin, Ireland and is more commonly referred to as Bachelor of Engineering; some South African Universities refer to their Engineering degrees as B.Ing. (Baccalaureus Ingenierswese - Afrikaans).


== Engineering fieldsEdit ==
A Bachelor of Engineering degree will usually be undertaken in one field of engineering, which is sometimes noted in the degree postnominals, as in BE(Aero) or BEng(Elec). Common fields for the Bachelor of Engineering degree include the following fields:
Aerospace Engineering
Agricultural Engineering
Automotive engineering
Biological/Chemical Engineering — including Biochemical, Biomedical, Biosystems, Biomolecular, and Chemical engineering
Chemical Engineering — deals with the process of converting raw materials or chemicals into more useful or valuable forms
Clean Technology — use energy, water and raw materials and other inputs more efficiently and productively. Create less waste or toxicity and deliver equal or superior performance.
Computer Engineering
Civil Engineering — a wide-ranging field, including building engineering, civil engineering, construction engineering, industrial, manufacturing, mechanical, materials and control engineering
Electrical and Computer Engineering/Electronic Engineering — very diverse field, including Computer Engineering, Communication/Communication systems engineering, Information Technology, Electrical Engineering, Electronics Engineering, Microelectronic Engineering, Microelectronics, Nanotechnology, Mechatronics, Software Engineering, Systems, Wireless and Telecommunications, Photovoltaic and Power Engineering
Electrical Controls Engineering — a relatively new and more specialized subdiscipline of Electrical Engineering that focuses on integrating Electrical Controls, and their programming.
Engineering Management — the application of engineering principles to the planning and operational management of industrial and manufacturing operations
Environmental Engineering — includes fields such as Environmental, Geological, Geomatic, Mining, Marine, and Ocean engineering
Fire Protection Engineering — the application of science and engineering principles to protect people and their environments from the destructive effects of fire and smoke.
Geomatics Engineering — acquisition, modeling, analysis and management of spatial data. Focuses on satellite positioning, remote sensing, land surveying, wireless location and Geographic Information Systems (GIS).
Geotechnical Engineering — a combination of civil and mining engineering, and involves the analysis of earth materials.
Information Science and Engineering — same as Information Technology.
Industrial Engineering — studies facilities planning, plant layout, work measurement, job design, methods engineering, human factors, manufacturing processes, operations management, statistical quality control, systems, psychology, and basic operations management
Instrumentation Engineering — a branch of engineering dealing with measurement
Integrated Engineering — a multi-disciplinary, design-project-based engineering degree program.
Manufacturing Engineering: Includes methods engineering, manufacturing process planning, tool design, metrology, Robotics, Computer integrated manufacturing, operations management and manufacturing management]]
Materials Engineering — includes metallurgy, polymer and ceramic engineering
Mechanical Engineering — includes engineering of total systems where mechanical science principles apply to objects in motion including transportation, energy, buildings, aerospace, and machine design. Explores the applications of the theoretical fields of Mechanics, kinematics, thermodynamics, materials science, structural analysis, manufacturing, and electricity
Mechatronics Engineering - includes a combination of mechanical engineering, electrical engineering, telecommunications engineering, control engineering and computer engineering
Mining Engineering — deals with discovering, extracting, beneficiating, marketing, and utilizing mineral deposits.
Nuclear Engineering — customarily includes nuclear fission, nuclear fusion, and related topics such as heat/thermodynamics transport, nuclear fuel or other related technology (e.g., radioactive waste disposal), and the problems of nuclear proliferation. May also include radiation protection, particle detectors, and medical physics.
Production Engineering — term used in the UK and Europe similar to Industrial Engineering in N America - it includes engineering of machines, people, process and management. Explores the applications of the theoretical field of Mechanics.
Textile Engineering — based on the conversion of three types of fiber into yarn, then fabric, then textiles
Robotics and Automation Engineering — relates all engineering fields for implementing in robotics and automation
Systems Science — focuses on the analysis, design, development and organization of complex systems


== International variationsEdit ==


=== AustraliaEdit ===
In Australia, the Bachelor of Engineering (BEng) is a four-year undergraduate degree course and a professional qualification. It is also available as a six-year sandwich course (where students are required to undertake a period of professional placement as part of the degree) or an eight-year part-time course through some universities. The Institution of Engineers, Australia (Engineers Australia) accredits degree courses and graduates of accredited courses are eligible for membership of the Institution. Bachelor of Engineering graduates may commence work as a graduate professional engineer upon graduation, although some may elect to undertake further study such as a Master's or Doctoral degree. Chartered Professional Engineer (CPEng) status or the various State registration requirements are usually obtained in later years. Graduates with formal engineering qualifications in Australia are often referred to as Professional Engineers to distinguish them from other professions where the term "Engineer" is used loosely.


=== CanadaEdit ===
In Canada, a degree for studies in an accredited undergraduate engineering program is named Bachelor of Engineering (B.Eng., B.Ing. in French).
Through the Canadian Engineering Accreditation Board (CEAB), Engineers Canada accredits Canadian undergraduate engineering programs that meet the standards of the profession. Graduates of those programs are deemed by the profession to have the required academic qualifications to be licensed as professional engineers in Canada. This practice is intended to maintain standards of education and allow mobility of engineers in different provinces of Canada.
In Canada, A CEAB-accredited degree is the minimum academic requirement for registration as a professional engineer anywhere in Canada, and the standard against which all other engineering academic qualifications are measured. Graduation from an accredited program, which normally involves four years of study, is a required first step to becoming a Professional Engineer. Regulation and accreditation are accomplished through a self-governing body (the name of which varies from province to province), which is given the power by statute to register and discipline engineers, as well as regulate the field of engineering in the individual provinces.
A graduate of a non-CEAB-accredited program must demonstrate that his or her education is at least equivalent to that of a graduate of a CEAB-accredited program.


=== GermanyEdit ===
In Germany, the Bachelor of Engineering was introduced as part of implementation of the Bologna process. However, this degree is in fact mostly offered by German Fachhochschule-institutions Universities of Applied Sciences. German technical universities award a Bachelor of Science in engineering rather than the BEng degree.


=== FinlandEdit ===
The situation is similar in Finland as in Germany. Universities of applied sciences (ammattikorkeakoulu) grant professional Bachelor's degrees (insinööri (amk)). The degree does not traditionally prepare for further study, but due to the Bologna process, a completely new degree of ylempi insinööri (yamk) has been introduced for engineers who wish to continue studying after some work experience. Before 2005, academic universities (see Education in Finland) did not make an administrative distinction between studies on the Bachelor's and Master's level, and the Master's level diplomi-insinööri was the first degree to be received. Due to the Bologna process, an intermediate "Bachelor of Science in Engineering" (tekniikan kandidaatti) has been introduced.


=== PakistanEdit ===
In Pakistan, the Bachelor of Science in Engineering or Bachelor of Engineering (BS / BSc/ BE Engg.) is awarded to a student who has completed four years course in engineering which consists of eight semesters. The entry to an engineering programme is after 12 years of schooling. Students are required to appear in an entry test and final evaluation is made by calculating their Intermediate (HSSC / F.Sc. Pre Engineering) Marks and entry test marks. Students having three years diploma of Associate Engineering (D.A.E.) can also appear for entry test. The rest of the procedure is same. Pakistan Engineering Council (PEC), the regulatory body of Engineering profession in Pakistan since 1976, registers and awards registration certificates to those graduates only, who complete their engineering studies from Universities/Institutes offering Accredited Engineering Programmes, thus declaring them bona fide Professional Engineers, capable of carrying-out any professional engineering works in private or public sector and are entitled to use the prefix (Engr.) with their names in general and (R.E. / P.E.) titles in particular as per registration status. Engineers are registered in two categories namely: Registered Engineer (R.E.) and Professional Engineer (P.E.). Graduate Engineers completing a PEC accredited degree are registered as Registered Engineers (R.E.) and a further 5 years postgraduate experience with formal examination (EPE) gets them registered as Professional Engineers (P.E.).

In Pakistan: Bachelor of Technology (Honors) degree is a four-year undergraduate university level degree, including Engineering and Applied Science courses, supervised industrial Practical training and projects. The degree of B.Tech(Hons)04 years degree was treated at par with a Bachelor of Engineering/Bachelor of Science Engineering. B.Tech program was formally launched in 1973 in pakistan and then Ministry of Education directed to give status of B.Tech (Hons) degree at par with B.Sc Engineering/B.E degree,& professionals called B-Tech Engineers according to the letter No. 15-29/73-Tech. According to the letter no PEC/4-P/QEC, PEC stated that B.Tech degree will be considered equivalent to B.Sc/BE and the same decision was taken in 9th inter-provincial ministers conference at Quetta in 1986, 39th HEC meeting on 12-2-98, FPSC in its letter no F4-89/2002-R but now PEC is not ready to accept their status. While there is no difference in the program objectives and learning outcomes of BE or BTech.


=== NetherlandsEdit ===
In the Netherlands the Bachelor of Engineering was also introduced as part of implementation of the bologna process, the same as in Germany. The degree is only offered by Dutch Hogeschool-institutions and is equivalent to the Dutch engineer's degree "ingenieur" (ing.). A Dutch BEng involves a rigorous study of four years and is only awarded in the field of aeronautical engineering, mechanical engineering, software engineering, industrial engineering, or electrical engineering. Completion of a Dutch engineer's study in the field biochemical engineering, biomedical engineering, chemical engineering, environmental engineering, material engineering is however awarded with a Bachelor of Applied Science degree. Dutch technical universities award a Bachelor of Science in engineering (BScEng) instead of the BEng degree.


=== IndiaEdit ===
BE & B Tech is a professional engineering degree awarded by universities to students who have completed four years (eight semesters) worth of coursework related to a specific branch of engineering. In general Universites offer B Tech and colleges affiliated to Universities will offer as BE, however there is no difference in the program objectives and learning outcomes of BE or BTech.
Eligibility for entry is 12 years of school education. Generally the first year (first two semesters) is common to all branches, and has the same subjects of study. Courses divert after first year. The medium of instruction and examination is English. Generally, universities in India offer engineering degree in the following branches: Civil Engineering, Mechanical Engineering, Electrical (and Electronics), Electronics (and Communications or Telecom) Engineering, Instrumentation and Control Engineering, Computer Engineering, Information Technology, Chemical Engineering, Metallurgical Engineering, Aerospace Engineering, Production Engineering, Systems Science, Biotechnology Engineering(chemical engineering cluster) Biomedical Engineering and Tool Engineeing
The AICTE, UGC and the Indian Government are responsible for approving engineering colleges and branches/courses. Only those universities which have been approved by these bodies can award degrees which are legally valid and are accepted as qualifiers for jobs in the central/state government and in the private sector.


=== South AfricaEdit ===
In South Africa, the Bachelor of Engineering or Bachelor of Science in Engineering (BEng/BIng (for Afrikaans Universities) or B.Sc. Eng.) is awarded to a student who has completed four years course in engineering which consists of eight semesters. The degree is regulated by ECSA (Engineering Council of South Africa).


== See alsoEdit ==
Bachelor of Technology
Bachelor's degree
Bachelor of Applied Science
Bachelor of Science in Information Technology
Engineer's degree
Master of Engineering (M.E. or MEng)
Vocational university


== ReferencesEdit ==