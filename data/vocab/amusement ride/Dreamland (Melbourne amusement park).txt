Dreamland was an Australian amusement park in the Melbourne suburb of St Kilda, which was opened on 2 November 1906. It was demolished in 1909, except for the Figure Eight rollercoaster which remained open until 1914.


== History ==
In November 1906 Dreamland was opened in St Kilda, a suburb of Melbourne, Australia. It was built on an area of wasteland which included a lagoon. The lagoon had been drained in 1870 and the site had been unoccupied for more than a decade.
Dreamland was demolished in 1909, but in 1912 Luna Park was opened in the same area. Luna Park is still open and operating today.


== See also ==
Luna Park Melbourne


== References ==


== External links ==
Park History