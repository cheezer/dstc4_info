Dr. Lam Pin Min (Chinese: 藍彬明; pinyin: Lán Bīnmíng, born 1 September 1969) is a Singaporean Member of Parliament (MP) who represents Sengkang West Single Member Constituency, which encompasses Anchorvale and Fernvale of Sengkang New Town.
Lam became an MP in 2006 after the 2006 general elections. He was educated in Anglo-Chinese School and National Junior College.


== External links ==
Lam Pin Min's profile on the Singapore Parliament website