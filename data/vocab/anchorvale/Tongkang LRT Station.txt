Tongkang LRT Station (SW7) is an LRT station on the Sengkang LRT Line West Loop in Anchorvale, Singapore. It was opened in January 2005 together with the Punggol LRT East Loop.


== Etymology ==
A tongkang is a light boat, which was commonly used to carry goods along rivers in the past. The station was named after Sungei Tongkang, a stream which the station is in close proximity at.


== Station layout ==


== Transport connections ==


=== Rail ===