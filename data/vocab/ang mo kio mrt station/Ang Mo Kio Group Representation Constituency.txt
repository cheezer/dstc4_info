Not to be confused with the similarly named, now-defunct Ang Mo Kio Single Member Constituency that existed prior to the 1991 elections, when this ward was formed.
Ang Mo Kio Group Representation Constituency (Traditional Chinese: 宏茂橋集選區;Simplified Chinese: 宏茂桥集选区) is a six-member Group Representation Constituency (GRC) in the north eastern region of Singapore. The constituency encompasses the majority of Ang Mo Kio, Teck Ghee, Kebun Baru, Yio Chu Kang, Jalan Kayu, Seletar Hills, part of Serangoon North, the western portion of Sengkang - Fernvale, Anchorvale and a western portion of Hougang. In the 2001 general election, the constituency was enlarged to include most parts of the Cheng San Group Representation Constituency.
The town centre of Ang Mo Kio was part of the group representation constituency until the general election in 2006, when the boundaries were redrawn and an area was carved out into a new Single Member Constituency known as Yio Chu Kang Single Member Constituency. In 2011, it absorb into Ang Mo Kio GRC again, while Sengkang West division was carved out into newly Sengkang West Single Member Constituency.
Nee Soon South had been part of the GRC until 2011 and Kebun Baru had been part of the GRC until 2015, which transferred to newly created Nee Soon Group Representation Constituency. While the constituency was enlarge to most parts of Aljunied GRC Aljunied-Hougang ward and Pasir Ris-Punggol GRC Punggol South ward.
Ang Mo Kio GRC is led by Prime Minister Lee Hsien Loong who is the MP for Teck Ghee. The GRC has been held by the People's Action Party since its formation in 1991. The constituency had never been contested before in the three general elections before 2006.
The western portion of the GRC consists of parts of the Central Catchment Nature Reserve while in the northeastern corner, it fronts a short coastline in the Straits of Johor with 2 reclaimed islands, Pulau Punggol Barat and Pulau Punggol Timor.


== Members of Parliament ==
Dr Balaji Sadasivan died on 27th September 2010 during his term as Member of Parliament.


== Operational responsibility ==
Cheng San-Seletar - Blocks 501 - 540, 574 - 578, 584 - 586 and 596 Ang Mo Kio, Nanyang Polytechnic and Amoy Quee Camp (including the military Lentor East)
Jalan Kayu - Blocks 501 - 554 Serangoon North, Seletar Camp, Seletar Aerospace Park, Seletar Country Club, Seletar Jalan Tari Dulang, Mimosa Estate and Luxus Hills Estate
Kebun Baru - Blocks 1xx and 2xx of Ang Mo Kio
Teck Ghee - Blocks 3xx and 4xx of Ang Mo Kio
Yio Chu Kang - Blocks 6xx and 7xx of Ang Mo Kio and Lentor Estate/Far Horizon Gardens
Ang Mo Kio-Hougang - Blocks 601 - 680 and Block 9xx of Hougang


== Candidates and results ==


=== Elections in 2010s ===


=== Elections in 2000s ===


=== Elections in 1990s ===


== See also ==
Ang Mo Kio SMC


== References ==

1991 General Election's Result
1997 General Election's Result
2001 General Election's Result
2006 General Election's Result
2011 General Election's Result