Not to be confused with present ward in Ang Mo Kio, Singapore which is called Ang Mo Kio Group Representation Constituency that is led by the current Prime Minister of Singapore, Lee Hsien Loong. This article is describing a ward in a part of Ang Mo Kio town before the Group Representation Constituency scheme was introduced.
Ang Mo Kio Single Member Constituency (Traditional Chinese: 宏茂橋單選區;Simplified Chinese: 宏茂桥单选区) is a defunct single member constituency in Ang Mo Kio, Singapore that was formed in 1976 and continues throughout until 1991 where the Ang Mo Kio town was relatively developed which forms the present day of Ang Mo Kio Group Representation Constituency.
It was carved from Nee Soon constituency in 1976.


== Members of Parliament ==
Yeo Toon Chia (1976–1991)


== Candidates and results ==


=== Elections in 1980s ===


=== Elections in 1970s ===


== See also ==
Ang Mo Kio GRC


== References ==
1988 GE's result
1984 GE's result
1980 GE's result
1976 GE's result