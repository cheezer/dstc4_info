Annalakshmi is an international chain of vegetarian restaurants operating on the uncommon principle of trust in humanity, as it encourages patrons to pay what they believe is fair. The name derives from the Hindu goddess of abundance, as a compound word obtained from Anna (food) and Lakshmi (the eponymous deity). It is the culinary division of the non-profit organization Temple of Fine Arts (TFA).


== History ==
First started in Malaysia in 1984, Annalakshmi has branches in Australia, Singapore and India. The Coimbatore branch (started 1989), is known to participate in local charities and support the fine arts. In 2013, the Chennai branch won the Times Food and Nightlife Award, in the category Best South Indian Restaurant (Stand-alone).


== See also ==
Vegetarian and Vegan Restaurants in Singapore
List of vegetarian restaurants


== References ==


== External links ==
Annalakshmi Australia: Mission
Annalakshmi Singapore: About Annalakshmi
Annalakshmi Chennai: About Us