The Hindu Mission Hospital is a 220-bed multidisciplinary health facility in Chennai, India. Spread over an area of 74,000 sq ft, the ISO 9001:2000-certified hospital is registered under Society Act of Tamil Nadu.


== Location ==
The hospital is located on GST Road in Tambaram, opposite Tambaram railway station.


== History ==
The hospital was started as an out-patient clinic in a shed on 5 December 1982. On 14 April 1985, a 10,000 sq ft 3-storied Sri Rama Nama block was opened by the then Vice President of India, R. Venkataraman. The same year, consultation on ENT, gynaecology, and psychiatry and weekly camps on ophthalmologic surgery were started. In June 1988, the hospital started expanding on an adjacent 1.7 acre land. On 14 September 1988, an in-patient wing with 20 beds and a major operation theatre were opened and general surgeries were conducted. On 11 October 1992, a 30,000 sq ft building with 100 beds was opened. In May 1993, maternity and childcare services began. In January 1995, the ophtholmic and geriatric blocks were opened and round-the-clock laboratory became operational. On 1 November 1997, Phaco emulsification unit was started. Elisa reader and ventilator facilities were added in 1998. On 12 February 1999, a haemodialysis unit and speech therapy and audiogram facility were opened. On 3 March 2000, a blood bank was opened. In 2001, various facilities such as mammography, 300 MA x-ray unit, automatic film processor, C'Arm table, operating microscope and Yag laser were added. The blood bank too was upgraded besides the induction of a tempo traveller. In 2004, a critical care block was inaugurated. In 2007, color Doppler ultrasound scanner and stress test system were added to the hospital's facilities. In 2008, Prof. S. R. S. Varadhan Block, T. S. Santhanam Block, and cardio-thoracic block were added.
The first ambulance service was started in May 1993 with two vans. On 14 August 1997, a third vehicle was donated by the LIC of India.


== The hospital ==
The hospital is spread over an area of 74,000 sq ft and has 220 beds and 9 operation theatres. It has 7 ambulances in its fleet. Exclusive wings in the hospital include critical care with ICCU accident trauma block, cardio-thoracic block, and a dialysis wing. It serves nearly 450 outpatients every day.


== Civic services ==
The hospital's services include projects such as "Narayana Seva", "Bhakta Jana Seva", "Annalakshmi", the Free Artificial Limb Centre and the Kidney Care Endowment. Under the Narayana Seva, free intensive medical camps for rural areas are conducted every Sunday in various villages where patients screened earlier by paramedical staff are treated. The Bhakta Jana Seva services are free rural mobile clinics popular among slums and villages around Tambaram. The Annalakshmi scheme provides a balanced diet to patients under the supervision of a dietician. The free artificial limb centre of the hospital conducts camps every Wednesday and Saturday. Medical camps for students are conducted in schools in the Tambaram locality.


== Funding ==
The mission is funded by various organisations from the corporate sector. The Sundaram Finance Group and the TAFE group are among the first of the corporate sponsors. As part of the hospital's golden jubilee celebrations, the Sundaram Finance Group provided ₹ 5 million to build a critical care block with equipment on the hospital premises.


== See also ==

Healthcare in Chennai


== References ==


== External links ==
Official webpage of the Hindu Mission Hospital