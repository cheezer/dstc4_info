Janaki Sabesh is an Indian actress appearing predominantly in Tamil and Telugu movies. Throughout her career she has played the "screen mother" to several leading actors.


== Early life ==
Janaki Sabesh was born Bangalore in a Palghat Iyer family. She did her schooling from DTEA Senior Secondary School, New Delhi. She graduated in political science from Lady Sriram's and did her masters in Mass Communication from Jamia Millia, Delhi.


== Career ==
In 1991, Janaki Sabesh assisted Simi Garewal with her documentary on Rajiv Gandhi called India's Rajiv. She made her acting debut in the movie Minsara Kanavu starring Kajol and Arvind Swamy in which she played a nun. She later went on to act in Shankar's magnum opus Jeans where she played Aishwarya Rai's mother. Since then, she has acted in over 25 movies.


== Personal life ==
Janaki Sabesh is married to Mr. Sabesh Subramaniam and has a daughter, Dhwani Sabesh.


== Notable filmography ==


== References ==


== External links ==
Janaki Sabesh at the Internet Movie Database