The S. S. Appam, 243 U.S. 124 (1917), is a case in which the Supreme Court of the United States affirmed a lower court's decision to restore the British prize of a German warship to the British owners.


== Background ==

In February 1916 the British steamship Appam was captured at sea by the German auxiliary cruiser SMS Möwe. A prize crew was put aboard and she was taken to Hampton Roads. Appam‍ '​s British owner, the British and African Steam Navigation Co, filed suit to recover possession of her from her captors. Federal Judge Edmund Waddill of Virginia, in a 15,000-word opinion, directed, 29 July 1916, that the vessel, with the cargo remaining aboard her and the proceeds of the perishable cargo already sold, should be restored at once to her British owners.


== Supreme Court ==
The German government appealed to the Supreme Court of the United States, which on 6 March 1917 handed down a decision that a belligerent nation may not bring prizes of war into a neutral port. The Supreme Court held that it would be unneutral for the United States to permit either belligerent to bring prizes into American ports, and that Germany could not claim such right under any of the existing treaties between that country and the United States. In bringing Appam into an American port, it was held, the German officials were committing a clear breach of American neutrality.
Ship and cargo, valued at between three and four million dollars, were delivered to the British owners 28 March 1917. The decision, written by Justice William R. Day, affirmed decrees by Federal Judge Waddill, and upheld the original ruling by Secretary of State Robert Lansing that prizes coming into American ports unaccompanied by captor warships have the right to remain only long enough to make themselves seaworthy. The court stated that neither the Treaty of 1799 with Prussia, the Hague conventions nor the Declaration of London, entitled any belligerents to make American ports a place of deposit of prizes as spoils of war under such circumstances.
“The principles of international law,” the opinion adds, “leaving the treaty aside, will not permit the ports of the United States to be thus used by the belligerents. If such use were permitted, it would constitute the ports of a neutral nation harbors of safety into which prizes might be safely brought and indefinitely kept.
“From the beginning of its history this country has been careful to maintain a neutral position between warring governments, and not to allow use of its ports in violation of the obligations of neutrality, nor to permit such use beyond the necessities arising from perils of the seas or the necessities of such vessels as to seaworthiness, provisions and supplies.”


== Aftermath ==
After return to the rightful owners, the ship was renamed Mandingo for the rest of the war. Thereafter she traded for many years on routes to West Africa and was scrapped in 1936.


== See also ==
List of United States Supreme Court cases, volume 243


== Notes ==


== References ==
 Rines, George Edwin, ed. (1920). "Appam Case, The". Encyclopedia Americana.