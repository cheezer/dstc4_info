The Maritime Experiential Museum (Simplified Chinese: 海事博物馆及水族馆 (Maritime Museum and Aquarium)), formerly the Maritime Xperiential Museum and the Maritime Experiential Museum & Aquarium, is a museum in Resorts World Sentosa, Sentosa, Singapore, built to house the Jewel of Muscat and some of the 60,000 artefacts salvaged from the Belitung shipwreck, an Arabian dhow wrecked off the coast of Belitung Island.


== Background ==

The Jewel of Muscat is an accurate reproduction of the Arab dhow ship presented by the Sultanate of Oman to the government and people of Singapore after its arrival after recreating part of the Belitung ships's route from Oman to Indonesia.
The artefacts, purchased by the Sentosa Leisure Group in 2005, are on loan to the Singapore Tourism Board. The museum, opened on 15 October 2011, will support research and conservation.


== Shipwreck ==

The Belitung shipwreck was discovered in 1998 by sea-cucumber fishermen diving in shallow water, just off the coast of Belitung island near Java. The wreck was excavated by Tilman Walterfang where it was discovered that the cargo of 60,000 items was still in place.


=== Cargo ===
Thanks to Tilman Walterfang’s ethical philosophy the cargo was not sold off piece by piece to collectors. Walterfang kept the precious cargo intact as one complete collection so that it could be studied in its original context. It was housed in private storage for six years where the items have been painstakingly conserved (including desalination), studied and carefully restored by Tilman's company Seabed Explorations Ltd in New Zealand.
The cargo was eventually purchased for around $32 million US, and was put on display at the ArtScience Museum at Marina Bay Sands, in an exhibition called "Shipwrecked: Tang Treasures and Monsoon Winds" ran from 19 February to 31 July 2011.


== See also ==
Belitung shipwreck
Jewel of Muscat


== References ==


== External links ==
Official site
Seabed Explorations website
Story and some pictures of the treasures
Website of the Jewel of Muscat
Video interviews
Story about ship's discovery in The Independent