The Asian Athletics Championships is an event organized by the Asian Athletics Association.
The competition courted controversy with the International Association of Athletics Federations when political in-fighting arose though the participation of Israel in 1977. That edition of the competition was cancelled and championships between 1979 and 1989 were called the "Asian Track and Field Meeting" as a result. The situation was resolved when Israel began competing in European Athletic Association events instead.


== Editions ==


== Championships records ==


== References ==


== External links ==
Asian Athletics Association
Past medallists from GBR Athletics