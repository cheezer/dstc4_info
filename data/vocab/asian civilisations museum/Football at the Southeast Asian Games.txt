Football has been a Southeast Asian Games sport since the 1959 edition.


== Results ==


=== Men's tournament ===


==== Southeast Asian Peninsular Games ====
1 Decided by round-robin standings.2 The title was shared.3 There was no bronze medal game held.


==== Southeast Asian Games ====
*Under-23 tournament since 2001.4 Indonesia did not turn up at the appointed time. After waiting for 15 minutes the referee called off the game and reported to the technical committee which awarded the bronze to Burma.5 Decided by round-robin standings.


=== Women's tournament ===


==== Southeast Asian Games ====
1 Played as round-robin format.


== Medal tally ==


=== Men's tournament ===


=== Women's tournament ===
*Both teams on the same position.


== See also ==
ASEAN Football Championship
Football at the East Asian Games
Football at the West Asian Games
Football at the Asian Games


== External links ==
ASEANfootball.org - SEA Games
RSSSF - Football at the Southeast Asian Games (Men's Tournament)
RSSSF - Football at the Southeast Asian Games (Women's Tournament)