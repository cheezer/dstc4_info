The Asian Junior Athletics Championships are the Asian championships open for those of age according to junior. It is currently organized by the Asian Athletics Association.


== Editions ==


== Championship records ==


=== Men ===


=== Women ===


== References ==

India wins a gold and bronze in Asian Junior Athletics. Times of India (2010-07-02). Retrieved on 2010-08-29.
China dominates Asian junior sports meet as RI finishes 17th. The Jakarta Post (2010-06-16). Retrieved on 2010-08-29.


== External links ==
Asian Athletics Association
Championships Records