Asian refers to anything related to the continent of Asia, especially Asian people.
Asian may also refer to:
Asian American
More specific meanings depending on context:
In the Americas, Europe, Australia and New Zealand the term usually refers to people from East Asia or Southeast Asia
In the United Kingdom and some other Commonwealth countries, the term refers more frequently to those from South Asia

Culture of Asia, the culture of the people from Asia
Asian cuisine, food based on the style of food of the people from Asia
Asian (cat), a cat breed similar to the Burmese but in a range of different coat colors and patterns
Asii (also Asiani), a historic Central Asian ethnic group mentioned in Roman-era writings
Asian option, a type of option contract in finance
Asyan, a village in Iran


== See also ==
Asiatic (disambiguation)