The Southeast Asian Games (also known as the SEA Games), is a biennial multi-sport event involving participants from the current 11 countries of Southeast Asia. The games is under regulation of the Southeast Asian Games Federation with supervision by the International Olympic Committee (IOC) and the Olympic Council of Asia.


== History ==
The Southeast Asian Games owes its origins to the South East Asian Peninsula Games or SEAP Games. On 22 May 1958, delegates from the countries in Southeast Asian Peninsula attending the Asian Games in Tokyo, Japan had a meeting and agreed to establish a sport organisation. The SEAP Games was conceptualised by Luang Sukhum Nayaoradit, then Vice-President of the Thailand Olympic Committee. The proposed rationale was that a regional sports event will help promote co-operation, understanding and relations among countries in the Southeast Asian region.
Thailand, Burma (now Myanmar), Malaya (now Malaysia), Laos, South Vietnam and Cambodia (with Singapore included thereafter) were the founding members. These countries agreed to hold the Games biennially. The SEAP Games Federation Committee was formed.
The first SEAP Games were held in Bangkok from 12–17 December 1959 comprising more than 527 athletes and officials from Thailand, Burma, Malaya (now Malaysia), Singapore, South Vietnam and Laos participating in 12 sports.
At the 8th SEAP Games in 1975, the SEAP Federation considered the inclusion of Brunei, Indonesia and the Philippines. These countries were formally admitted in 1977, the same year when SEAP Federation changed their name to Southeast Asian Games Federation (SEAGF), and the games were known as the Southeast Asian Games. East Timor was admitted at the 22nd Southeast Asian Games in Vietnam.
The 2009 Southeast Asian Games was the first time Laos has ever hosted a Southeast Asian Games (Laos had previously declined hosting the 1965 Southeast Asian Peninsular Games citing financial difficulties). Running from 9–18 December, it has also commemorated the 50 years of the Southeast Asian Games, held in Vientiane, Laos.
The most recent SEA Games was the 2015 SEA Games held in Singapore. The next SEA Games, the 2017 SEA Games will be held in Malaysia while the 2019 SEA Games will be held in Philippines. Of the participating nations, Cambodia and East Timor have yet to host the SEA Games.


== Participating countries ==


== Sports ==

Below was the list of the types of sports played in the SEAGF from 1959. The bullet mark (•) indicates that the sport was played in the respective year.
1 – not an official Olympic Sport.
2 – sport played only in the SEAGF.
3 – not a traditional Olympic nor SEAGF Sport and introduced only by the host country.
4 – Beach volleyball was introduced in 1993.
o – a former official Olympic Sport, not applied in previous host countries and was introduced only by the host country.
h – sport not played in the previous edition and was reintroduced by the host country.
e – Netball was first included in 2001.


== Medal count ==

1 – Competed as Malaya in the inaugural games until 1961.
2 – The Republic of Vietnam was dissolved in July 1976 when it merged with the Democratic Republic of Vietnam (North Vietnam) to become the Socialist Republic of Vietnam also known as Vietnam. Therefore, the medal counts for this country are considered to be as until 1975. The International Olympic Committee (IOC) is not using codes for South Vietnam any more after unifying with North Vietnam.
3 – Competed as Cambodia, Kampuchea, and Khmer Republic.
4 – In the 1989 edition, a unified Vietnam rejoined the games with new name and new flag. Medals made by South Vietnam are already combined here. See table tally above for South Vietnam.
5 – Competed as Burma until 1987.


== Gold medal ranking tally ==


== Host nations and cities ==

Since the Southeast Asian Games began in 1959, it has been held in 15 different cities across all Southeast Asian countries except Cambodia and Timor Leste.


== Criticism ==
The games is unique in that there are no official limits to the number of sports which may be contested, and the range may be decided by the organising host pending approval by the Southeast Asian Games Federation. Albeit for some core sports which must be featured, the host is also free to drop or introduce other sports.
This leeway has resulted in hosts maximising their medal hauls by dropping sports which are disadvantages to themselves relative to their peers, and the introduction of obscure sports, often at short notice, thus preventing most other nations from building up credible opponents. Some examples of these include:
At the 2001 Southeast Asian Games, Malaysia introduced pétanque, lawn bowls and netball.
At the 2003 Southeast Asian Games, Vietnam added fin swimming, shuttlecock, and added wushu event to 33 golds from 16 in 2001.
In the 2005 Southeast Asian Games, the Philippines added arnis, a demonstration sport in 2003, with 6 sets of medals and it won 3 gold medals.
At the 2007 Southeast Asian Games, Thailand added some new categories of sepak takraw and used a new kind of ball that had been used by their athletes for a year while other countries had never used it before. Futsal was also added. Thailand won nearly all sets of medal from that discipline.
In the 2011 Southeast Asian Games, Indonesia dropped the team events in table tennis and shrunk the shooting events to just 14 golds from 19 in 2009 and 33 in 2007. At the same time, bridge, kenpō, paragliding, vovinam and wall climbing were introduced.
In the 2013 Southeast Asian Games, Myanmar introduced local sports Chinlone. The host went on to win 6 out of 8 gold medals in the event. Sittuyin, a traditional Burmese chess which other competing nations were not familiar was included as a traditional chess number along with common chess competition number.
Floorball was demonstrated by Singapore in the 2013 Southeast Asian Games and then was officially added in the 2015 Southeast Asian Games.


== See also ==
ASEAN ParaGames
East Asian Games
Asian Games
ASEAN School Games
Far Eastern Championship Games


== References ==


== External links ==
Olympic Council of Asia Regional Hosting List
SEAP Games Federation