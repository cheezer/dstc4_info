The Assam Tribune is an English daily newspaper published from Guwahati and Dibrugarh, Assam. It is the highest circulated English daily in North-East India.
First published from Guwahati, it is now published simultaneously from Guwahati and Dibrugarh. The newspaper is perhaps the best in the North-Eastern Region. It has a huge readership in Assam and is the most popular newspaper in the North-East India. The Assam Tribune has a wide reach in terms of circulation figures as well as the reliability of the news matter.
The present editor is Prafulla Govinda Baruah, son of Radha Govinda Baruah, and P. J. Baruah is the Executive Editor.


== See also ==
The Sentinel
Seven Sisters Post


== External links ==
Official Website
Facebook