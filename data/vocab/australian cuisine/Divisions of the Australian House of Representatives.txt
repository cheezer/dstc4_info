Electoral divisions for the Australian House of Representatives are commonly known as electorates, seats or divisions. There are currently 150 single-member electorates for the Australian House of Representatives.


== Apportionment ==
Section 24 of the Constitution of Australia requires that the total number of members of the Australian House of Representatives shall be "as nearly as practicable" twice as many as the number of members of the Australian Senate.
Section 24 also requires that electorates be apportioned among the states in proportion to their respective populations; provided that each original state has at least 5 members in the House of Representatives, a provision that gives Tasmania higher representation than its population would justify.
The Commonwealth Electoral Act 1918 sets out further provisions. There are also two electorates in both the Australian Capital Territory and the Northern Territory.
Within each state and territory, electoral boundaries are redrawn from time to time in a process known as redistribution. This takes place at least once every 7 years, or when the state's entitlement to the number of members of the House of Representatives changes. Boundaries are drawn by a Redistribution Committee, and apportionment within a state is on the basis of the number of enrolled voters rather than total residents.
Within a state or territory, the number of enrolled voters in each Division can not vary by more than 10% from the average across the state, nor can the number of voters vary by more than 3.5% from the average projected enrolment three-and-a-half years into the future.
Divisions are divided into four classes according to population and demographic: inner metropolitan (well-established portions of a state capital city), outer metropolitan (more recently developed portions of a state capital), provincial (divisions made up predominantly of regional cities) and rural.


== Naming ==
The Divisions of the House of Representatives are unusual in that many of them are not named after geographical features or numbered, as is the case in most other legislatures around the world. Most Divisions are named in honour of prominent historical people, such as former politicians (often Prime Ministers), explorers, artists and engineers.
In some cases where a Division is named after a geographical locality, the connection to that locality is sometimes tenuous. For instance, the Division of Werriwa, created in 1901, was named after the Aboriginal word for Lake George in the Canberra region. However, Werriwa has not contained Lake George for many decades, and has steadily moved some 200 km north to the south-western suburbs of Sydney over the past century.
The redistribution, creation and abolition of Divisions is the responsibility of the Australian Electoral Commission. Some of the criteria the AEC use when naming new Divisions are listed below:
Name divisions after deceased Australians who have rendered outstanding service to their country, with consideration given to former Prime Ministers
Retain the original names of Divisions proclaimed at Federation in 1901
Avoid geographical place names
Where appropriate use Aboriginal names
Do not duplicate names of state electoral districts


== List of Commonwealth Electoral Divisions, 2010–present ==
The maps below show the Division boundaries as they existed at the Australian federal election, 2010.


=== New South Wales ===
There are 48 Divisions:


=== Victoria ===
There are 37 Divisions:


=== Queensland ===
There are 30 Divisions:


=== Western Australia ===
There are 15 Divisions:
Brand
Canning
Cowan
Curtin
Durack
Forrest
Fremantle
Hasluck
Moore
O'Connor
Pearce
Perth
Stirling
Swan
Tangney


=== South Australia ===
There are 11 Divisions:
Adelaide
Barker
Boothby
Grey
Hindmarsh
Kingston
Makin
Mayo
Port Adelaide
Sturt
Wakefield


=== Tasmania ===
There are 5 Divisions:
Bass
Braddon
Denison
Franklin
Lyons


=== The Territories ===
Australian Capital Territory
There are 2 Divisions:
Canberra
Fraser (also covers Jervis Bay Territory)
Northern Territory
There are 2 Divisions:
Lingiari (also covers Christmas Island and Cocos (Keeling) Islands)
Solomon


== Abolished Divisions ==
These Australian electoral divisions no longer exist.


== See also ==
For a list of members of the current House of Representatives and the electorates they represent, see List of members of the Australian House of Representatives.
For a description of how the House of Representatives is elected, see Australian electoral system.


== External links ==
For an electoral history of each Division since Federation in 1901, see Adam Carr's Electoral Archive: Index of House of Representatives Divisions 1901-2001


== Notes ==