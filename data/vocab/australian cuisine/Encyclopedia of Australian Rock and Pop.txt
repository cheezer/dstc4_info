The Encyclopedia of Australian Rock and Pop or Rock and Pop by Australian music journalist Ian McFarlane is a guide to Australian popular music from the 1950s to the late 1990s. The encyclopedia was described in Australian Music Guide as "the most exhaustive and wide-ranging encyclopedia of Australian music from the 1950s onwards".
The encyclopedia is out of print, but was for a time available on the whammo.com.au online record store, and is still in the Internet Archive.


== Reviews ==
Publishers, Allen & Unwin describe McFarlane's encyclopedia as containing over 870 entries and is an "essential reference to the bands and artists who molded the shape of Australian popular music [...] in an A-to-Z encyclopedia format complete with biographical and historical details. Each entry also includes listings of original band lineups and subsequent changes, record releases, career highlights, and cross-references with related bands and artists."
United States Barnes & Noble reviewer, David Turkalo, found that although it was written solidly and had "a surprising number of Australian-American connections", it was too specialised for general American library patrons.
The book has a similar title to the 1978 work by Noel McGrath, Australian Encyclopaedia of Rock and Pop.


== References ==


== External links ==
Online version of the book as stored at the Internet Archive