Ayam goreng is a generic term to refer to various kinds of Indonesian and Malaysian dish of chicken deep fried in coconut oil. Ayam goreng literally means "fried chicken" in Indonesian and Malay. Unlike Southern United States style fried chicken, this Southeast Asian version is absent of batter coated flour and richer in spices.


== Marination and spices ==

The spice mixture may vary among regions, but usually it consists of a combination of ground shallot, garlic, Indian bay leaves, turmeric, lemongrass, tamarind juice, candlenut, galangal, salt and sugar. The chicken pieces are soaked and marinated in spice mixture for some time prior to frying, in order for the chicken to absorb the spices. The chicken is then deep fried in an ample amount of hot coconut oil. Ayam goreng is usually served with steamed rice, sambal terasi (chili with shrimp paste) or sambal kecap (sliced chili and shallot in sweet soy sauce) as a dipping sauce or condiment and slices of cucumber and tomato for garnish. Fried tempeh and tofu might be added as side dishes.


== Variants ==
There are many recipes of ayam goreng, among the popular ones are ayam goreng lengkuas (galangal fried chicken), Padang style ayam goreng, Sundanese hayam goreng, Jakarta style ayam goreng, Javanese ayam goreng Kalasan, ayam goreng kremes. East Javanese pecel ayam is actually an ayam goreng served with sambal. Another Javanese variant is ayam goreng penyet, penyet is Javanese word for "squeezed" since the fried chicken is served in earthenware mortar upon sambal and squeezed with pestle to mix it with sambal. In Indonesia and Malaysia various style of foreign fried chicken is often also called as ayam goreng. Common Southern United States fried chicken is often called ayam goreng tepung or flour-battered or breaded fried chicken.


== See also ==
Ayam bakar
Ikan bakar
List of chicken dishes
 Food portal


== References ==


== External links ==