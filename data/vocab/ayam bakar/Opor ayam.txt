Opor ayam is a chicken cooked in coconut milk from Indonesia, especially from Central Java. Spice mixture (bumbu) include galangal, lemongrass, cinnamon, tamarind juice, palm sugar, coriander, cumin, candlenut, garlic, shallot, and pepper. Opor ayam is also a popular dish for lebaran or Eid ul-Fitr, usually eaten with ketupat and sambal goreng ati (beef liver in sambal).


== See also ==
List of chicken dishes
List of Indonesian soups


== External links ==
Opor Ayam Recipe from Tasty Indonesian Food


== References ==