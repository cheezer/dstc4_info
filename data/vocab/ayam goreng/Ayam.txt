Ayam may refer to:
Ayam kampung(cap),
ʿAyam (people), Kuwaiti citizens of Persian origins.
Ayam, the Malay/Indonesian word for chicken, used in names of dishes and chicken or food-related entities:
Ayam Brand, food company
Ayam gulai, Indonesian curry dish
Ayam pansuh, Malaysian chicken dish
Opor Ayam, Javanese dish
Soto ayam, a yellow spicy chicken soup with vermicelli
Team Ayam, a Malaysian football club

Al Ayam ("The Days"), referring to several newspapers:
Al Ayam (Bahrain)
Al Ayam (Sudan)
Al-Ayyam (disambiguation)

Amur Yakutsk Mainline - partially complete railway in Eastern Siberia.