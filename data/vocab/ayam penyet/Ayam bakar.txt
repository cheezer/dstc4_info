Ayam bakar is a generic term to refer various kinds of Indonesian and Malaysian dish of charcoal-grilled chicken. Ayam bakar literally means "roasted chicken" in Indonesian and Malay.


== Marination and spices ==

In Java, the chicken is usually marinated with the mixture of kecap manis (sweet soy sauce) and coconut oil, applied with a brush during grilling. The bumbu spice mixture may vary among regions, but usually it consists of combination of ground shallot, garlic, chilli pepper, coriander, tamarind juice, candlenut, turmeric, galangal and salt. In Java, ayam bakar usually tastes rather sweet because of the generous amount of sweet soy sauce either as marination or dipping sauce, while the ayam bakar Padang, Bali, Lombok and most of Sumatra are usually spicier and more reddish in colour due to the generous amount of chilli pepper, turmeric and other spices, and the absence of sweet soy sauce.
The chicken pieces are usually partially cooked in the spice mixture using a small fire prior to grilling, in order for the chicken to absorb the spices. During grilling process the remaining spices are applied upon the chicken. Ayam bakar is usually served with sambal terasi (chilli with terasi) or sambal kecap (sliced chilli and shallot in sweet soy sauce) as dipping sauce or condiment and slices of cucumber and tomato as garnishing.


== Variants ==
There are many recipes of ayam bakar, among the popular ones are Padang-style ayam bakar, Ayam Percik from Malaysia, ayam bakar Taliwang of Lombok island, Sundanese bakakak hayam, and Javanese ayam bakar bumbu rujak (grilled spicy coconut chicken). Usually, the chicken is marinated with mixture of spice pastes, sometimes kecap manis (sweet soy sauce), and then grilled.


== See also ==
Ayam goreng
Barbecue chicken
Ikan bakar
List of chicken dishes
 Food portal


== References ==


== External links ==
Ayam Bakar Recipes (in Indonesian)