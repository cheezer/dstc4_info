Ayam Taliwang is a spicy Indonesian ayam bakar (grilled chicken) dish from Lombok.


== History ==
Although Ayam Taliwang is said to be a dish favoured by Sasak nobility, Abdul Hamid claims to have invented the dish in 1970. It is named after Karang Taliwang in Mataram, the capital of Lombok.


== Preparation ==
Ayam Taliwang is made with chicken (preferably free range), which is cut and cleaned prior to grilling. Once it has been grilled halfway, it is removed from the grill and tenderized with a pestle. It is then dipped in cooking oil; after several seconds in the oil, it is put in a spicy sauce of garlic, chili, and shrimp paste. It is then fried or grilled to order.


== Presentation ==
Ayam Taliwang physically appears similar to regular grilled or fried chicken, with a covering of sambal. Its taste is sweet and spicy, with traces of shrimp paste. It can be served with a side of water spinach (pelecing) and eggplant (beberuk) covered with chili sauce.


== Variations ==
The Taliwang flavour has been adapted for instant noodles.


== See also ==

List of chicken dishes


== References ==
Footnotes

Bibliography