Kreta Ayer-Tanglin Group Representation Constituency (Traditional Chinese: 牛車水-東陵集選區;Simplified Chinese: 牛车水-东陵集选区) was a four-member Group Representation Constituency largely based on the defunct Kampong Glam GRC. The Kampong Glam ward of that GRC was replaced with a Tanglin ward composed of Tanglin SMC and Cairnhill wards from the previous election. The divisions for this GRC were Kreta Ayer, Kim Seng, Tanglin and Moulmein.
The GRC was dissolved in 2001.


== Members of Parliament ==


== Candidates and results ==


== References ==

1997 General Election's result