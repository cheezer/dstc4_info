Bab Anar (Persian: باب انار‎, also Romanized as Bāb Anār and Bāb-e Anār; also known as Bābā Anār, Bābā Na‘am, Bābā Najm, and Bāba Nār) is a city in Khafr District, Jahrom County, Fars Province, Iran. At the 2006 census, its population was 1,702, in 445 families.


== References ==