Backpacker Magazine is an American publication that features information on wilderness hiking and adventure. It has been published since 1973. Backpacker magazine is currently published by Active Interest Media and is based in Boulder, Colorado. The magazine moved from Emmaus, Pennsylvania to Boulder in August 2007.


== History ==
The first issue of Backpacker appeared in the spring of 1973. The editor's note written by founding editor William Kemsley explains, "It took us three years to put together the first issue of Backpacker. In that time we debated some serious questions among ourselves." The note describes the founding editors' worries that America in the early 1970s did not contain a backpacking community large enough to support a magazine. It also expresses Kemsley's goal to support the magazine primarily through subscriptions rather than advertising.
The Winter/Spring 2007 issue of the journal Appalachia includes an essay by Kemsley in which he describes how he developed the idea to create a magazine about backpacking. The article, titled "How the 1970s Backpacking Boom Burst Upon Us," explains several pivotal moments that showed Kemlsey that an audience could exist for such a magazine.
Backpacker was owned first by Kemsley, who sold it to Ziff Davis in 1980, which sold it to CBS Publishing. In the late 1980s, it was bought by Rodale Press, which also publishes Men's Health, Bicycling, Runner's World, and others. In May 2007, Rodale sold Backpacker to Active Interest Media and the magazine moved to Boulder, Colorado in August 2007.


== Content ==
Backpacker Magazine goes beyond backpacking and hiking, featuring the latest in a wide variety of outdoor sports, including rock climbing, mountain biking, trail running, cycling, fly fishing and more. In each issue of Backpacker, readers will find outdoor gear reviews, wilderness and survival tips, trip reports, coverage of specific destinations and even strength and conditioning advice.


== Current status ==
On May 17, 2007 Active Interest Media, an El Segundo, CA-based magazine publisher, announced that it would buy Backpacker magazine from Rodale Press. A May 10 article in the New York Post reported a sale price of $14.5 million. Active Interest Media, which also publishes Yoga Journal and American Cowboy, moved Backpacker to Boulder, CO in the summer of 2007, and Jonathan Dorn was editor-in-chief. In 2013 Dennis Lewon became Editor-in-Chief.
Backpacker publishes nine issues per year, which includes an annual gear guide in April. Combined issues are published for December–January, February–March, and July–August. The magazine is divided into the service-centric Basecamp section (which won a National Magazine Award for 2005), a feature well, a gear review section, and concludes with maps of local trails corresponding to six regional editions. Every April Backpacker presents its "Editors Choice" awards to highlight the best gear of the year.
Starting in 2004 Backpacker began publishing regional editions of the magazine that include map cards for local trails. The regional concept developed from survey results that showed the magazine's readers were interested in hikes near where they lived. It discontinued these map cards in the April 2008 as part of their effort to become a 100% carbon neutral magazine. The regional sections are now on-line. An all digital version of the magazine was also made available.
In November 2006 Backpacker began posting podcasts and videos on its website to complement the content that appears in the magazine issues. Additional videos appear on Backpacker's YouTube channel.
In April 2007 Backpacker received its third National Magazine Award nomination in two years, this time in the single-topic issue category for its October 2006 "Survival" issue. National Magazine Awards are presented each May by the American Society of Magazine Editors, and are considered the magazine industry's highest editorial honor. Also in 2006 Backpacker received the MIN Best of the Web award in the Uses of Interactivity category for the backpacker.com/hikes website.
In May 2008 Backpacker won a first-ever National Magazine Award for "General Excellence" in the circulation category, 250,000 to 500,000. To win this honor, Backpacker beat national magazines like Cookie, New York, Wondertime, and W.
Backpacker is also used as part of the American Hiking Society's membership package.
Backpacker's primary competition in the magazine world includes Outside and Men's Journal.


== References ==


== External links ==
Backpacker.com - The official website of the Backpacker magazine.