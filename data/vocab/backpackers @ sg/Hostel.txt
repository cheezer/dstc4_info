Hostels provide budget-oriented, sociable accommodation where guests can rent a bed, usually a bunk bed, in a dormitory and share a bathroom, lounge and sometimes a kitchen. Rooms can be mixed or single-sex, although private rooms may also be available.
Hostels are generally cheaper for both the operator and the occupants; many hostels have long-term residents whom they employ as desk clerks or housekeeping staff in exchange for free accommodation.
In a few countries, such as the UK, Ireland, India and Australia, the word hostel sometimes also refers to establishments providing longer-term accommodation (often to specific classes such as nurses, drug addicts, or court defendants on bail) where the hostels are sometimes run by Housing Associations and charities. In India, Pakistan and South Africa, hostel also refers to boarding schools or student dormitories in resident colleges and universities. In other parts of the world, the word hostel mainly refers to properties offering shared accommodation to travellers or backpackers.
Within the "traveller" category, another distinction can be drawn between hostels that are members of Hostelling International (HI), a UK-based, non-profit organization encouraging outdoor activities and cultural exchange for the young (formerly the IYHA), and independently operated hostels. Hostels for travellers are sometimes called backpackers' hostels, particularly in Australia and New Zealand (often abbreviated to just "backpackers").


== History ==

In 1912, in Altena Castle in Germany, Richard Schirrmann created the first permanent Jugendherberge or "Youth Hostel". These first Youth Hostels were an exponent of the vision of the German Youth Movement to let poor city youngsters breathe fresh air outdoors. The youths were supposed to manage the hostel themselves as much as possible, doing chores to keep the costs down and build character as well as being physically active outdoors. Because of this, many Youth Hostels closed during the middle part of the day. Very few hostels still have a "lockout" or require chores beyond washing up after self-catered meals.


== Etymology ==
The words "hotel", "hostel", and "hostal" are etymologically related, coming into the English language from Old French hostel, itself from Late Latin hospitale, denoting a "hospice" or place of rest. Nowadays, however, they each refer to distinct types of accommodation. In particular, hostal is used in Spanish either with the same sense as "hostel", or it can also refer to a certain type of pension which is found mostly in Spain.


== Differences from hotels ==

There are several differences between hostels and hotels, including:
Hostels tend to be budget-oriented; rates are considerably lower, and many hostels have programs to share books, DVDs and other items.
For those who prefer an informal environment, hostels do not usually have the same level of formality as hotels.
For those who prefer to socialize with their fellow guests, hostels usually have more common areas and opportunities to socialize. The dormitory aspect of hostels also increases the social factor.
Hostels are generally self-catering.


=== Communal accommodation ===

There is less privacy in a hostel than in a hotel. Sharing sleeping accommodation in a dormitory is very different from staying in a private room in a hotel or bed and breakfast, and might not be comfortable for those requiring more privacy. Hostels encourage more social interaction between guests due to the shared sleeping areas and communal areas such as lounges, kitchens and internet cafes.
Care should be taken with personal belongings, as guests may share a common living space, so it is advisable to secure guests' belongings. Most hostels offer some sort of system for safely storing valuables, and an increasing number of hostels offer private lockers; there are other things to consider as well when choosing a safe hostel, such as whether they have a guest curfew, uphold fire codes, 24-hour security, and CCTV.
Noise can make sleeping difficult on occasions, whether from snoring, talking, sexual activity, someone either returning late or leaving early, or the proximity of so many people. To mitigate this, some wear earplugs and/or sleeping masks.


=== Self-contained facilities and services ===
In attempts to attract more visitors, many hostels nowadays provide additional services not previously available, such as airport shuttle transfers, internet cafes, swimming pools and spas, tour booking and car rentals. Some hostels may include a hot meal in the price.


== Types ==

The traditional hostel format involved dormitory style accommodation. Some newer hostels also include en-suite accommodation with single, double or quad occupancy rooms, though to be considered a hostel they must also provide dormitory accommodation. In recent years, the numbers of independent and backpackers' hostels have increased greatly to cater for the greater numbers of overland, multi-destination travellers (such as gap-year travellers and rail-trippers).
The quality of such places has also improved dramatically. While a few hostels do still insist on a curfew, daytime lockouts, and/or require occupants to do chores, this is becoming a rare exception rather than the rule, as hostels adapt to meet the changing expectations of guests.


=== Hostelling International (HI) ===
Richard Schirrmann's idea of hostels rapidly spread overseas and eventually resulted in Hostelling International, an organization composed of more than 90 different Youth Hostel associations representing over 4500 Youth Hostels in over 80 countries.
Some HI Youth Hostels cater more to school-aged children (sometimes through school trips) and parents with their children, whereas others are more for travellers intent on learning new cultures. However, while the exploration of different cultures and places is emphasized in many hostels, particularly in cities or popular tourist destinations, there are still many hostels providing accommodation for outdoor pursuits such as hillwalking, climbing and bicycle touring; these are often small friendly hostels retaining much of the original vision and often provide valuable access to more remote regions.
In the past several years, Hostelling International have increasingly added hotels and package resorts to their networks in addition to hostels.
Despite their name, in most countries membership is not limited to youth.


=== Independent hostels ===
Independent hostels are not necessarily affiliated with one of the national bodies of Hostelling International, Youth Hostel Association or any other hostel network. Often, the word independent is used to refer to non-HI hostels even when the hostels do belong to another hostelling organization such as SIH and Backpackers Canada.
The term "youth" is less often used with these properties. These non-HI hostels are often called "backpackers' hostels". Unlike a hotel chain where everything is standardized, these hostels can be very diverse, typically not requiring a membership card. There are chains of independent hostels throughout the world such as the Jazz Hostels on the East Coast and Banana Bungalow Hostels on the West Coast of the United States, or the Generator Hostels and Equity Point Hostels of Europe or Zostel of India. Each offers their own niche of services to travelers and backpackers. For example, one independent hostel might feature a lot of in house gatherings, another might feature daily and nightly tours or events in the surrounding city, and another might have a quieter place to relax in serenity, or be located on the beach. This is an independent hostel's personality and travelers will frequent the hostels that offer the personality that they find desirable. Oftentimes there is a distinction being a "party hostel" or not.
As the hostel industry evolves, independent hostels and HI hostels are becoming more similar, with the word "backpackers" also now applying to many Hostelling International hostels.


=== Boutique hostels ===
The general backpacking community is no longer exclusively typified by student travelers and extreme shoe string budgets. In response to demand, as well as increasing competition between the rapidly growing number of hostels, the overall quality of hostels has improved across the industry. In addition to the increase in quality among all styles of hostel, new styles of hostels have developed that have a focus on a more trendy, design interior.
The phrase "boutique hostel" an often-arbitrary marketing term typically used to describe intimate, luxurious or quirky hostel environments. The term has started to lose meaning because the facilities of many "boutique hostels" are often no different from hostels that aren't referred to with that label. Also, marketers and online booking websites sometimes include boutique hotels in lists of "boutique hostels," further diluting any specific meaning of the phrase.
A related term, "flashpackers", often refers to hostels that target themselves as catering to a slightly older, tech-savvy clientele, but in practice, many of the new class of higher-quality hostels across the industry offer these tech-oriented facilities, and even the flashpacker websites that appeared in 2006-2008 during the peak of the "flashpacker" hype are neglected or offline as of 2012 as the term has rapidly lost popularity.


=== Mobile hostels ===
Though very uncommon, a mobile hostel is a hostel with no fixed location. It can exist in the form of a campsite, a temporary building, bus, van, or a short term agreement in a permanent building. Mobile hostels have sprouted up at large festivals where there exists a shortage of budget accommodation. As with regular hostels, mobile hostels generally provide dormitory accommodation for backpackers or travelers on a shoe string budget. The first ever (and only) commercial example of a mobile hostel is Hostival. It has sprouted up at Oktoberfest, Carnival, San Fermin, Las Fallas, and the 2010 World Cup.


== Industry growth ==

The independent hostel industry is growing rapidly in many cities around the world, such as New York, Rome, Buenos Aires and Miami. This is reflected in the development and expansion of dozens of hostel chains worldwide. The recent eruption in independent hostels has been called "probably the single biggest news in the world of low-cost travel and very safe".
The development of independent backpackers hostels is a strong business model, with some cities reporting a higher average income per room for hostels than hotels. For example, in the city of Honolulu, Hawaii, upscale hotels are reportedly making $141 to $173 per room, while hostel rooms in the same city can bring in as much as $200 per night. Even during the 2008 economic crisis, many hostels are reporting increased occupancy numbers in a time when hotel bookings are down.

Even as the city’s hotel occupancy rate has fallen to 66 percent in February, from 81 percent in the same month last year, despite steep discounts, many youth hostels are reporting banner business.

Though in the past, hostels have been seen as low-quality accommodation for less wealthy travellers, at least one Australian study has shown that backpackers (who typically stay at hostels) spend more than non-backpackers, due to their longer stays. Backpackers in Australia contribute nearly $3.4 billion and stay on average 34.2 nights compared to the 31 nights spent by other travellers. In New Zealand, backpackers hostels had a 13.5% share of accommodation guest/nights in 2007.


=== The Youth Travel Accommodation Industry Survey ===
Annually the Association of Youth Travel Accommodation undertakes a review of the business operations of the hostel sector, to establish crucial business metrics and identify trends in this dynamic sector. The study is undertaken in partnership with Hostelling International and Web Reservations International.
The findings of the 2010 study included:
Average occupancy rates were around 56%
Occupancy levels were highest on Oceania and Asia
The sale of beds accounted for 70% of reported revenue. F&B sales accounted for 14% of total revenue
The average dorm bed rate varied between EUR 21 in the high season and EUR 15 in the low season
The main cost items for hostel establishments are staff and premises, which together accounted for 45% of total expenses
Marketing costs accounted for almost 10% of the total budget
Only 8% of hostel operators currently participate in green certification schemes
According to the Youth Travel Accommodation's Annual Survey one of the main reasons for a relatively strong performance of the hostel sector is the tendency for operators to innovate and adapt their products to suit market conditions. The fact that hostel operators could generally sustain business levels through the downturn was one of the main reasons why overall average bed rates for 2009 rose by more than 3% compared with 2008.


== In popular culture ==
Motion pictures have generally portrayed hostels in two ways: as fun places for young people to stay (for example, The Journey of Jared Price and A Map for Saturday), or alternatively, as dangerous places where unsuspecting Americans face potential horrors in Central Europe (see, e.g., Hostel, Hostel: Part II, and Eurotrip). There are some popular misconceptions that a hostel is a kind of a flophouse, homeless shelter, or halfway house. This does not reflect the high quality and level of professionalism in many modern hostels.


== See also ==
Lodging
Backpacking (travel)
Hostal – a different type of lodging found in Spain and Hispanic America
Pension
Bed and breakfast
Guest house


== Notes and references ==

Reulecke, Jürgen; Stambolis, Barbara (2009). 100 Jahre Jugendherbergen 1909-2009 : Anfänge, Wandlungen, Rück- und Ausblicke. Essen: Klartext. ISBN 978-3-89861-990-5.  History of hostels, (in German)