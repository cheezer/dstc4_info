Backpacker or backpackers may refer to:
A person who participates in:
Backpacking with animals, using pack animals to carry gear while backpacking
Backpacking (travel), low-cost, independent, international travel
Backpacking (wilderness), trekking and camping overnight in the wilderness
Ultralight backpacking, a style of wilderness backpacking with an emphasis on carrying as little as possible
Looking for backpacking equipment

Backpacker (magazine), an American magazine about wilderness hiking and adventure
Backpacker (series), a series of Swedish computer games in which the player travels the world and answers questions about each locale
Backpacker (US slang), American urban slang for graffiti artists and listeners of alternative hip hop
Backpackers (TV series), an Australian TV series following travelling backpackers in Europe
Backpackers (web series), a Canadian comedy web series, later adapted for American television
Backpacker, Australian and New Zealander slang for inexpensive sleeping accommodations, such as a hostel