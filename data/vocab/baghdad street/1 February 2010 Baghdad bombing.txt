The 1 February 2010 Baghdad Bombings was a suicide bombing in Baghdad Iraq which killed at least 54 people, and wounded another 100. The attack was aimed at a group of Shia pilgrims walking to a religious festival.


== Attack ==
The female suicide bomber blew herself up at a rest stop along the route the pilgrims were taking to a Shia religious festival held in Karbala. The rest stop had a security search area, which is where the bomber detonated her explosives belt. There was a similar attack on the same pilgrims last year, which killed 40.


== References ==


== See also ==
List of terrorist incidents, 2010
List of suicide bombings in Iraq in 2010