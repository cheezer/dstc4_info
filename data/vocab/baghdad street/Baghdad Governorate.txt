Baghdad Governorate (Arabic: محافظة بغداد‎ Muḥāfaẓät Baġdād), also known as the Baghdad Province, is the capital governorate of Iraq. It includes the City of Baghdad, as well as the surrounding metropolitan area, which includes Al-Mada'in (Seleucia-Ctesiphon), Taji, Husseinia, Mahmudiya District and Abu Ghraib District. The province has a total area of 4,071 square kilometres (1,572 sq mi), the smallest of the 18 provinces of Iraq but the most populous.


== Government ==
Governor: Ali Muhsin Al-Temimi
Provincial Council Chairman (PCC): Kamil az-Zubaydi
Provincial Council Chairman (PCC): Kamil az-Zaidi


== Description ==
Baghdad Governorate is one of the most developed parts of Iraq, with a better infrastructure than much of Iraq, though heavily damaged from the US-led invasion in 2003 and continuing violence today. It also has one of the highest rates for terrorism in the world with bombs, suicide attackers, and hit squads operating in the city.
Baghdad has at least 12 bridges spanning the Tigris river - joining the east and west of the city. The Sadr City district of the capital is the most densely populated area in Iraq.


== Province Administration ==

Baghdad is governed by the Baghdad Provincial Council. Representatives to the Baghdad Provincial Council were elected by their peers from the lower councils of the administrative districts in Baghdad in numbers proportional to the population of the various districts that were represented.


== Districts of Baghdad Governorate ==
Abu Ghraib
Husseinia
Al Istiqlal
Al-Mada'in
Mahmudiya
Taji
Al Tarmia
Baghdad (city)
Adhamiyah
Karkh
Karadah
Kadhimiya
Mansour
Sadr City
Al Rashid
Rusafa
9 Nissan


== Sister cities ==
The Baghdad Governorate has a sister relationship with the Denver-Aurora Metropolitan Area, in the United States of America.
 Denver-Aurora Metropolitan Area, State of Colorado, United States of America.
 State of Maryland, USA.


== See also ==

Baghdad


== References ==


== External links ==
http://www.humanitarianinfo.org/
Baghdad-Denver Region Partnerhip