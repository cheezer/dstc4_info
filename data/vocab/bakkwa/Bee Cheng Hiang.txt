Bee Cheng Hiang (Chinese: 美珍香; pinyin: Měizhēnxiāng; Pe̍h-ōe-jī: Bí-chiⁿ-hiuⁿ) is a company that produces Asian-style foodstuffs, especially that of Singaporean cuisine. Starting as a market stall in 1933 in Singapore, the company has expanded its operations to more than 260 retail outlets located across nine territories—China, Hong Kong, Indonesia, Macau, Malaysia, the Philippines, South Korea, Singapore and Taiwan.
Its best known product is bakkwa—smoked and roasted pieces of pork with a consistency similar to jerky. Bee Cheng Hiang introduced "Gourmet Bakkwa" in 2003 (which is bacon-like slices of bakkwa), and, in 2005, the "Chilli Gourmet Bakkwa". The company has evolved with the times and expanded its offerings to include prawn rolls, crispy pork floss, cuttlefish and sausages etc.


== References ==


== External links ==
Official website of Bee Cheng Hiang