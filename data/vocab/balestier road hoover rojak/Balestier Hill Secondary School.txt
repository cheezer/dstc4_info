Balestier Hill Secondary School (Abbreviation: BHSS) is a secondary school in Singapore.


== Culture ==
The school's motto is "Wisdom Through Knowledge". The school which is the mainstream school also accepts a special group of HI (hearing-impaired) students. Since 1991, the school has had Pastoral Care and Career Guidance (PCCG) and Cognitive Research Trust (CoRT) programmes. In 2004, the School Heritage Gallery was launched by alumnus Ron Sim, CEO of OSIM Pte Ltd. In 2009, the NE Corner was launched with the goal of engaging students with current affairs both globally and locally.


== History ==
On 24 June 1964, the Minister of Education, Ong Pang Boon, declared the school open as "Balestier Hill Integrated Secondary Technical School". It was one of the first six secondary technical schools to be opened in Singapore, and was an "integrated" school because it had both Chinese and English streams. The school was located at Balestier Hill, named after Joseph Balestier, the first United States Consul of Singapore. Cheong Pak Lo was the principal. In 1975, the school song was rewritten by two teachers, Chew Y.C. and Yeo S.B.
In 1986, the Chinese stream was discontinued; the year after, the Alumni Association had its inaugural AGM. In 1992, the school was renamed Balestier Hill Secondary School and relocated from Balestier Road to Dunearn Road to allow the building of new premises. In 1994, the first Balestierian of the Year Award was given to the pupil who contributed service to the school and performed best in studies and ECA. In 1997, a handing-over ceremony took place and the school began operations at the new site. The next year, the old school crest was changed to its present form. In 2000, the school was opened by Sinnakaruppan, MP, Kreta Ayer - Tanglin GRC (Moulmein Div). In 2001, Rangoon Secondary School merged with Balestier Hill Secondary School.
In 2007, Monk's Hill Secondary School merged with Balestier Hill Secondary School. The merged school operates from the current site at 11, Novena Rise.


== External links ==
Official website
Full chart of the school's history