In opera, a banda (Italian for band) refers to a musical ensemble (normally of wind instruments) which is used in addition to the main orchestra and plays the music which is actually heard by the characters in the opera. A banda sul palco (band on the stage) was prominently used in Rossini's Neapolitan operas. Verdi used the term banda to refer to a banda sul palco, as in the score for Rigoletto. He used the term banda interna (internal band), to refer to a band which is still separate from the orchestra but heard from the off-stage wings. The early scores of La traviata use a banda interna.


== Origins ==
Diegetic depictions of music making are present in the earliest operatic depictions of Orpheus accompanying himself but larger onstage ensembles seem to have first appeared in Don Giovanni, most spectacularly in the polymetric Act I ball where the wind Harmonie is joined by two violin-and-bass bands to simultaneously accompany minuet, contradance and waltz. Giovanni Paisiello's opera Pirro, which opened weeks later in December 1787, marks the first use of the term banda in the sense of a wind band. While Almaviva's serenade is accompanied from the pit in both Paisiello's (1782) and Rossini's (1816) Il barbiere di Siviglia, by 1818 Rossini's opera Ricciardo e Zoraide the banda was established as an independent institution in Italian opera houses.


== Organisation of the banda ==
Composers such as Rossini and Verdi made use of the banda in their early works. Composers wrote music on two staves, and the bandmaster added clarinets in the upper register and brass in the middle and lower. At the Paris Opera in the mid-nineteenth century, Adolphe Sax used only brass instruments, many of them saxhorns.
The banda was much like a military band. It comprised about twenty players, but was generally not part of the opera company - usually, the local impresario selected the players. In Palermo, Bourbon troops were recruited for operas, and, in Venice, Kinsky's Austrian army regiment, in a similar capacity, was highly regarded.


== Verdi and the banda ==
According to Warrack and West, the banda "makes its most marked appearance in Verdi's operas, where its stirring, frankly vulgar sound accorded well with the general Risorgimento feeling of such early works as Nabucco and I Lombardi. In his later operas he used the banda convention in a more flexible fashion, ranging from the subtlety of the ballroom scenes in Rigoletto and Un ballo in maschera to the stridency of the auto-da-fé scene of Don Carlos." Julian Budden, dismissing Rossini's use of the banda as "always naturalistic and perfunctory", continues by dismissing Donizetti's and Bellini's banda music, and finally writes "It was left to Verdi to plumb the depths of vulgarity with his banda marches, even at a time when his style as a whole had reformed. .... The so-called Kinsky band at Venice had a very high reputation, and Verdi, who had refused to write for it in Attila, took good care to include it in all his subsequent Venetian operas".


== See also ==
List of musical ensemble formats


== References ==

Further reading
Kimbell, David R. B. (1985). Verdi in the Age of Italian Romanticism. Cambridge University Press
Harwood, Gregory W. (1998). Giuseppe Verdi: A Guide to Research. Routledge, 1998
Tyler, Linda L. (23 (March 1990) "Striking up the Banda: Verdi's Use of the Stage Band in His Middle-Period Operas." The Opera Journal, pp. 2–22. ISSN 0030-3585
Van, Gilles (1998). Verdi's Theater: Creating Drama Through Music. University of Chicago Press


== External links ==
The cimbasso and tuba in the operatic works of Giuseppe Verdi: A pedagogical and aesthetic comparison (Abstract). Alexander Costantino, doctoral dissertation (August 2010, University of North Texas). "This acceptance of conical instruments is also evident when Verdi wrote for stage-band (banda)."