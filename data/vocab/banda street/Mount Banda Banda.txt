Mount Banda Banda, a mountain of the Mid North Coast region of New South Wales, Australia, is situated 320 kilometres (200 mi) from Sydney within the Willi Willi National Park. Banda Banda can be seen on the north western horizon from the town of Port Macquarie. At 1,258 metres (4,127 ft) AHD  it is the highest mountain in the region.


== Flora ==
The stands of Antarctic beech are some of the finest in existence, and the mountain was included in 1986 on the United Nations World Heritage List as part of Gondwana Rainforests of Australia.
Interesting eucalyptus plants occurring on the mountain include the Blue Mountains ash and Eucalyptus scias subsp. apoda. The endangered shrub Zieria lasiocaulis only occurs at Willi Willi National Park. Another endangered plant on Mount Banda Banda is Grevillea guthrieana.
The summit of the mountain is remarkably flat, and covered in grasses and the Blue Mountains ash. The isolated occurrence of the Blue Mountains ash is noteworthy, as it is most often seen in the Blue Mountains district, some 300 kilometres to the south west.


=== Cool temperate rainforest ===
The main tree species in the rainforest is the Antarctic beech. Associated species include prickly ash, coachwood, sassafras, soft corkwood and yellow carabeen. Walking stick palms are seen in the understorey, though usually not associated with such cooler rainforests. Orange berry is a common ground cover. Despite the impressive 40 metre canopy, the soil is only moderately fertile, being derived from porphyry. Rainfall on Mount Banda Banda is 2 metres per year, a very high amount for the otherwise arid Australian continent.


=== Arboretum ===
In 1964, the New South Wales Forestry Commission planted various exotic conifers amidst the cool temperate rainforest at Banda Banda. Arboretum species include the monkey puzzle tree, Douglas fir and the Sierra redwood. The possibility of the spread of exotic seedlings into the natural rainforest is of concern.


== Fauna ==
Rare fauna inhabiting Mount Banda Banda include the Hastings River mouse, Parma wallaby, sphagnum frog, Booroolong frog, giant barred frog, stuttering barred frog, the tree dwelling snail, the olive whistler, and the rufous scrub-bird, which was described by the International Union for Conservation of Nature as being of "universally outstanding significance to science and conservation". The reptile and invertebrate fauna of the mountain are not yet comprehensively understood. However, as this Gondwana rainforest is in good condition, future surveys should provide a valuable picture of the local ecology.


== Gallery ==


== See also ==
List of mountains in New South Wales
List of reduplicated Australian place names


== References ==


== Further reading ==
Adam, Paul (1987). New South Wales Rainforests - The Nomination for the World Heritage List. ISBN 0-7305-2075-7.