Bandung Raya ("Greater Bandung" in Indonesian) may mean:
Bandung Metropolitan Area
Bandung Raya (1987-1997), also called Mastrans Bandung Raya — a defunct Liga Indonesia Premier Division football team from 1987 to 1997
Pelita Bandung Raya — a current Indonesia Super League football team that changed its name from Pelita Jaya FC in 2012, and is co-owned with the Second Division team