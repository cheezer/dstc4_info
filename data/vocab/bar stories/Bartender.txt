A bartender (also known as a barkeep, barman, barmaid, or a mixologist) is a person who formulates and serves alcoholic beverages behind the bar, usually in a licensed establishment. Bartenders also usually maintain the supplies and inventory for the bar. A bartender can generally mix classic cocktails such as a Cosmopolitan, Manhattan, Old Fashioned, and Mojito. The bartending profession was generally a second occupation, used as transitional work for students to gain customer experience or to save money for university fees. This however is changing around the world and bartending has become a profession by choice rather than necessity. Cocktail competitions such as World Class and Bacardi Legacy have recognised talented bartenders in the past decade and these bartenders, and others, spread the love of cocktails and hospitality throughout the world. 
In America, where tipping is a local custom, bartenders depend on tips for most of their income. Bartenders are also usually responsible for confirming that customers meet the legal drinking age requirements before serving them alcoholic beverages. In certain countries, such as Australia and Sweden, bartenders are legally required to refuse more alcohol to drunk customers.


== United KingdomEdit ==
In the United Kingdom, bar work is often not regarded as a long-term profession (except if you are also a landlord), but more often as a second occupation, or transitional work for students to gain customer experience or to save money for university fees. As such, it lacks traditional employment protections and therefore has a high turnover. The high turnover of staff due to low wages and poor employee benefits results in a shortage of skilled bartenders. Whereas a career bartender would know drink recipes, serving techniques, alcohol contents, correct gas mixes, licensing law and would often have cordial relations with regular customers, short-term staff may lack these skills. Some pubs prefer experienced staff, although pub chains tend to accept inexperienced staff and provide training.
It is unusual to tip bartenders in the UK, however it is considered polite to if they have exhibited exceptional skill, diligence, or cordiality in their serving the customer or in the preparation of complicated drinks. The appropriate way to tip a bartender in the UK is to say 'have one for yourself', encouraging the bartender to buy themselves a drink with one's money.


== United StatesEdit ==
The Bureau of Labor Statistics retrieves and publishes extensive data on occupations in the United States, including that of bartender. It publishes a detailed description of the bartender's typical duties and employment and earning statistics by those so employed, with 55% of a bartender's take-home pay coming in the form of tips. Bartenders may attend special schools or learn while on the job.
Bartenders in the United States may work in a large variety of bars. These include hotel bars, restaurant bars, sports bars, gay bars, piano bars, and dive bars. Also growing in popularity is the portable bar; it allows a bar to be moved and set up in events and other venues. Therefore, such bartenders are quickly transitioning from the traditional notion such a job, in which one stays put in a single location.


== GalleryEdit ==


== See alsoEdit ==

Barback, a bartender's assistant
Hospitality
List of bartenders
List of public house topics
List of restaurant terminology
Tavern


== ReferencesEdit ==


== External linksEdit ==
 Media related to Bartenders at Wikimedia Commons