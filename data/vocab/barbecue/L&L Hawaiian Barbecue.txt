L (named el /ˈɛl/) is the 12th letter of the modern English alphabet and the ISO basic Latin alphabet.


== History ==
Lamedh may have come from a pictogram of an ox goad or cattle prod. Some have suggested a shepherd's staff.


== Use in English ==
In English orthography, ⟨l⟩ usually represents the phoneme /l/, which can have several sound values, depending on whether it occurs before or after a vowel. The alveolar lateral approximant (the sound represented in IPA by lowercase [l]) occurs before a vowel, as in lip or blend, while the velarized alveolar lateral approximant (IPA [ɫ]) occurs in bell and milk. This velarization does not occur in many European languages that use ⟨L⟩; it is also a factor making the pronunciation of ⟨L⟩ difficult for users of languages that lack ⟨L⟩ or have different values for it, such as Japanese or some southern dialects of Chinese. A medical condition or speech impediment restricting the pronunciation of ⟨L⟩ is known as lambdacism.
In English orthography, ⟨L⟩ is often silent in such words as walk or could (its presence can modify the preceding vowel letter's sound; otherwise walk might be pronounced to rhyme with sock). ⟨L⟩ is usually silent in such words as palm and psalm; however, there is some regional variation.


== Use in other orthographies ==
⟨L⟩ usually represents the sound [l] or some other lateral consonant.
Common digraphs include ⟨LL⟩, which has a value identical to ⟨L⟩ in English, but has the separate value voiceless alveolar lateral fricative (IPA [ɬ]) in Welsh, where it can appear in an initial position.
A palatal lateral approximant or palatal ⟨L⟩ (IPA [ʎ]) occurs in many languages, and is represented by ⟨GL⟩ in Italian, ⟨LL⟩ in Spanish and Catalan, ⟨LH⟩ in Portuguese, and ⟨Ļ⟩ in Latvian.
In phonetic and phonemic transcription, the International Phonetic Alphabet uses ⟨l⟩ to represent the lateral alveolar approximant.


== Other uses ==
The capital letter ⟨L⟩ is used as the currency sign for the Albanian lek and the Honduran lempira. It was often used, especially in handwriting, as the currency sign for the Italian lira. It is also infrequently used as a substitute for the pound sign (⟨£⟩), which is based on it.
In Roman numerals it represents 50.


== Forms and variants ==

In some fonts, the lowercase letter ⟨L⟩, ⟨l⟩ may be difficult to distinguish from the digit one, ⟨1⟩ or an uppercase letter ⟨I⟩. In recent times, many new fonts have curved the lowercase form to the right, and it is increasingly common, especially on European road signs and advertisements. A more modern version based on the handwritten letterlike ⟨ℓ⟩ is sometimes used in mathematics and elsewhere. Its LaTeX command is \ell, its codepoint is U+2113, and its numeric character reference is "&#8467;".


== Related letters and other similar characters ==
Ł ł : Latin letter L with stroke
Ľ ľ : Latin letter L with a caron
LL Ll ll : Latin digraph Ll
For a list of variants of the Latin letter L, see below
ℒ ℓ : Script letter L
£ : pound sign
₤ : lira sign
Λ λ : Greek letter Lambda
Л л : Cyrillic letter El
ל : Hebrew letter Lamedh


== Computing codes ==
1 Also for encodings based on ASCII, including the DOS, Windows, ISO-8859 and Macintosh families of encodings.


== Other representations ==


== References ==


== External links ==
 The dictionary definition of L at Wiktionary
 The dictionary definition of l at Wiktionary
 The dictionary definition of ℓ at Wiktionary