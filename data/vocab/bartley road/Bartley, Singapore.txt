Bartley is an area in the central district of Singapore. It gets its name from Bartley Road, which stretches from the end of Braddel Road to Upper Paya Lebar Road, as part of the Outer Ring Road System. There are some condominiums and houses in the area. It is also where the Gurkha Contingent is based. There are many Gurkhas and their families living there in private flats.


== Location ==
Bartley is located in mid-northeast Singapore forming part of the central district. It is situated just south of Hougang and borders Serangoon and Upper Aljunied.


== Amenities ==
There are a few amenities for residents living in Bartley. Bartley MRT Station links the Bartley area with Junction 8, Serangoon Bus Interchange and Serangoon Town Center. The Singapore Society for the Prevention of Cruelty to Animals is also in Bartley.


== Education ==
There are only two educational institutions in Bartley.
Bartley Secondary School
Maris Stella High School
There used to be another school, Elling Primary School, which closed down in the mid-1990s.


== Landmarks ==
The former Biddadari Christian, Muslim and Hindu Cemetery was in operation until 2005 on the left side of Bartley Road. Other landmarks includes the several factories which are currently in the Bartley area such as Wisma A.U.P.E, Times Centre and Tropical Industrial building.


== See also ==
Mount Vernon, Singapore
Outer Ring Road System
Bartley MRT Station