Bartley is a census-designated place (CDP) located in McDowell County, West Virginia, USA. It lies along the Norfolk and Western Railroad on the Dry Fork. As of the 2010 census, its population was 224. According to the Geographic Names Information System, Bartley has also been known as Bartlett and Peeryville.


== Mining disaster ==
Bartley was the site of one of the deadliest mine disasters in American history when the Pond Creek #1 mine, owned by the Pocahontas Coal Corporation, exploded on January 10, 1940 at 2:30 PM. 91 miners lost their lives that fateful day. The west side of the mine was not affected and 37 men escaped injury and another 10 men at the bottom of the shaft, used to drop the men into the mine, were also not affected. A rescue effort was mounted and seven hours after the first explosion, while men were working to save their fellow miners, a second explosion occurred thus sealing the fate and ending the rescue effort for the 91 lost men.

Upon investigation the cause of the explosion was found to be a gas pocket that had built up during the shift that was touched off by a spark of unknown origin. The mine had had a continuous history or having dangerous gas conditions throughout its history. The memorial marker to the 91 men can be found at the local United Methodist Church located at Bartley. The explosion left 51 widowed women along with 169 orphaned children.


== References ==