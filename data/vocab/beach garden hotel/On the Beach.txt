On the Beach may refer to:


== Literature ==
On the Beach (novel), a 1957 novel by Nevil Shute


== Film and television ==
"On the Beach" (ER), a 2002 episode of the long-running television series ER
On the Beach (1959 film), a 1959 film based on the novel
On the Beach (2000 film), a 2000 television film based on the novel On the Beach above


== Music ==
"On the Beach (In the Summertime)", a 1970 song by The 5th Dimension
"On the Beach", a song by the Chairmen of the Board
"On the Beach", a song by The Chameleons from their album What Does Anything Mean? Basically
"On the Beach", a song by Cliff Richard with The Shadows from his 1964 album Wonderful Life
On the Beach, an album by Phil Cohran and the Artistic Heritage Ensemble
On the Beach (Chris Rea album), a 1986 album by Chris Rea, and also its title track
On the Beach (Neil Young album), a 1974 album by Neil Young


== Business ==
On the Beach (business) a UK-based travel retailer