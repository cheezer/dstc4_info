This is a list of the notable beaches in India. List is sorted by states anti-clockwise.


== West coast ==


=== Gujarat ===
The beaches along the western state of Gujarat are:
Chorwed Beach
Diu Beach
Dandi Beach
Dabari Beach
Suvali Beach
Umbirgam Beach
Hazira Beach
Dumas Beach
Uhubrat Beach
Tithal Beach
Mandvi Beach — Mandvi — Kutch


=== Maharashtra ===
The state of Maharashtra has

Mumbai

Rest of Maharashtra


=== Goa ===
The beaches in the state of Goa are listed below.
Agonda Beach
Anjuna Beach
Arambol Beach
Ashvem Beach
Baga Beach
Benaulim Beach
Calangute
Candolim Beach
Cavelossim Beach
Chapora Beach
Colva Beach
Dona Paula
Majorda Beach
Mandrem Beach
Miramar
Mobor Beach
Morjim Beach
Palolem Beach
Sinquerim Beach
Vagator Beach
Varca Beach


=== Karnataka ===

Karwar Beach, Karnataka
Om beach, Gokarna
Murudeshwara
Kaup (village)
Kudle beach
Mangalore Beaches
Panambur
NITK Beach
Ullal
Tannirubhavi Beach
Sasihithlu Beach
Someshwar Beach

Maravanthe
Malpe Beach, Udupi
Mukka
St Mary's Island, Karnataka
Trasi
Gopinath Beach


=== Kerala ===
The beaches in Kerala state are:

Alappuzha Beach
Chavakkad Beach, Thrissur
Cherai Beach
Fort Kochi beach, Cochin
Kappad
Kappkadavu
Kollam Beach
Kovalam, Trivandrum
Marari beach
Meenkunnu Beach
Muzhappilangad Beach, Thalassery
Payyambalam Beach
Snehatheeram Beach, Thrissur
Shangumughom Beach, Trivandrum
Thekkumbhagam-Kappil Beach, Paravur
Thirumullavaram Beach, Kollam
Varkala, Trivandrum
See also : Beaches in Kerala


== East coast ==


=== Andhra Pradesh ===

The following are the beaches in Andhra Pradesh, India

Baruva, Srikakulam
Bhavanapadu, Srikakulam
Kalingapatnam Beach, Srikakulam
Kallepalli, Srikakulam
Mogadalapadu, Srikakulam
Sagarnagar beach, Visakhapatnam
Bheemunipatnam beach
Thotlakonda beach, Visakhapatnam
Rushikonda Beach, Visakhapatnam
Ramakrishna Mission Beach, Visakhapatnam
Tenneti Park Beach, Visakhapatnam
Yarada Beach, Visakhapatnam
Yanam Beach, Yanam(East Godavari)
Vakalapudi beach, Kakinada(East Godavari)
Uppada Beach, Uppada (East Godavari district)
Kakinada Beach, Kakinada (East Godavari)
Odalarevu Beach, Amalapuram(Konaseema)(East Godavari)
S Yanam Beach, Amalapuram (Konaseema)(East Godavari)
Pallamkurru Beach, Amalapuram (Konaseema)(East Godavari)
Anthervedi Beach, Razole (Konaseema)(East Godavari)
Perupalem Beach, Narasapur (West Godavari)
Manginapudi beach, Machilipatnam
Suryalanka Beach, Bapatla
Vodarevu Beach, Chirala
Rama Puram Beach, Chirala
Mypadu Beach, Nellore
Koduru beach, Nellore


=== Odisha ===
The beaches in Odisha are:

Chandipur Beach
Gopalpur-on-Sea
Chandrabhaga (Konark) Beach
Astaranga Beach
Talsari Beach
Pata Sonapur Beach
Satpada Beach
Baleshwar Beach
Paradeep Beach
Satabhaya Beach
Gahirmatha Beach
Puri Beach
Ramachandi Beach
Malud Beach
Baliharachandi Beach


=== Pondicherry ===
Promenade Beach
Karaikal Beach
Mahe Beach
Yanam Beach
Plage Paradiso (Paradise Beach)
Auroville Beach
Serenity Beach


=== Tamil Nadu ===

The beaches in the southern state of Tamil Nadu are:
Marina Beach, Chennai
Golden Beach, Chennai
Adyar Beach, Chennai
Thiruvanmiyur Beach, Chennai
Elliot's Beach, Chennai
Santhome Beach, Chennai
Thiruvottiyur Beach, Chennai
Kovalam Beach, Chennai Kovalam
Mahabalipuram Beach, Mahabalipuram
Ennore Beach, Ennore
Silver Beach, Cuddalore
Kanyakumari Beach, Kanyakumari
Tiruchendur Beach, Tiruchendur
Kayalpattinam Beach, Kayalpattinam
Velankanni Beach, Velankanni
Poombukar (Mayavaram) Beach Poombukar
Pondicherry Beach
Porto Novo Beach, Parangipettai
Tranqbar Beach, Tharangambadi
Nagapattinam Beach
Sikarim sagar Beach


=== West Bengal ===
The beaches in West Bengal are:
Digha Beach
Bakkhali Beach
Mandarmani Beach
Sankarpur Beach
Junput Beach
Tajpur, West BengalBeach


== Others ==

Wandoor Beach, Andaman

Colva Beach, Margao, India
Panayur Beach
The beaches in Andaman and Nicobar Islands:
Carbyn's Cove Beach
Wandoor Beach


== See also ==
List of beaches


== References ==