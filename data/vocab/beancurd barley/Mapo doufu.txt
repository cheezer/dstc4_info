Mapo doufu (or "mapo tofu") is a popular Chinese dish from China's Sichuan province. It consists of tofu set in a spicy chili- and bean-based sauce, typically a thin, oily, and bright red suspension, and often cooked with douchi (fermented black beans) and minced meat, usually pork or beef. Variations exist with other ingredients such as water chestnuts, onions, other vegetables, or wood ear fungus.


== Etymology ==
"Ma" stands for "ma-zi" (Chinese: mázi, 麻子) which means pockmarks. "Po" is the first syllable of "popo" (Chinese: 婆婆, pópo) which means an old woman or grandma. Hence, mapo is an old woman whose face is pockmarked. It is thus sometimes translated as "pockmarked grandma's beancurd".
According to Mrs. Chiang's Szechwan Cookbook: "Eugene Wu, the Librarian of the Harvard Yenching Library, grew up in Chengdu and claims that as a schoolboy he used to eat Pock-Marked Ma's Bean Curd or mapo doufu, at a restaurant run by the original Pock-Marked Ma herself. One ordered by weight, specifying how many grams of bean curd and meat, and the serving would be weighed out and cooked as the diner watched. It arrived at the table fresh, fragrant, and so spicy hot, or la, that it actually caused sweat to break out."


== Characteristics ==

Authentic Mapo doufu is powerfully spicy with both conventional "heat" spiciness and the characteristic "mala" (numbing spiciness) flavor of Sichuan cuisine. The feel of the particular dish is often described by cooks using seven specific Chinese adjectives: 麻 (numbing), 辣 (spicy hot), 烫 (hot temperature), 鲜 (fresh), 嫩 (tender and soft), 香 (aromatic), and 酥 (flaky). The authentic form of the dish is increasingly easy to find outside China today, but usually only in Sichuanese restaurants that do not adapt the dish for non-Sichuanese tastes.
The most important and necessary ingredients in the dish that give it the distinctive flavour are chili broad bean paste (salty bean paste) from Sichuan's Pixian county (郫县豆瓣酱), fermented black beans, chili oil, chili flakes of the heaven-facing pepper (朝天辣椒), Sichuan peppercorns, garlic, green onions, and rice wine. Supplementary ingredients include water or stock, sugar (depending on the saltiness of the bean paste brand used), and starch (if it is desired to thicken the sauce).


== Variations ==

Mapo Doufu can also be found in restaurants in other Chinese provinces as well as in Japan and Korea where the flavor is adapted to local tastes. In the west, the dish is often greatly changed, with its spiciness severely toned down to widen its appeal. This happens particularly in Chinese restaurants not specialising in Sichuan cuisine. In American Chinese cuisine the dish is often made without meat to appeal to vegetarians, with very little spice, a thick sweet-and-sour sauce, and added vegetables, a stark contrast from the original dish. This vegetarian version is sometimes referred to as MaLa Tofu.


== See also ==
Chinese cuisine
Kung Pao chicken
Cooking
List of Chinese dishes
List of tofu dishes
Szechuan cuisine
Chengdu
 Food portal


== References ==
^ Schrecker, Ellen with Shrecker, John. Mrs. Chiang's Szechwan Cookbook. New York, Harper & Row, 1976. p. 220.
^ "Mapo Tofu". Baidu (in Chinese). Retrieved 27 April 2013. 
^ "Mapo tofu practice". Meishi China (in Chinese). Retrieved 27 April 2013. 


== External links ==
How to make Mapo Tofu
Recipe of Mapo Tofu
Recipe for Vegetarian Mapo Tofu