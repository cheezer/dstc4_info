The Beary (also known as Byari) is a community concentrated mostly along the southwest coast of India, in coastal Dakshina Kannada, a district in the South Indian state of Karnataka. It is an ethnic society, having its own unique traditions and distinct cultural identity. The Beary community holds an important place among the other coastal Muslim communities, like Nawayath's of the Uttara Kannada district, Mappilas (Moplahs) of the Malabar coast and Labbay of the Coromandel coast.
Bearys incorporate the local Tulu culture of Dakshina Kannada and diverse traditions of the Moplahs of the Malabar coast.
The Beary community of Dakshina Kannada or Tulunadu is one among the earliest Muslim inhabitants of India, with a clear history of more than 1350 years. One mosque was built in the Bunder area of Mangalore by Habeeb Bin Malik, an Arab Da'ee, in 644 A.D.


== Etymology ==

The word 'Beary' is said to be derived from the Tulu word 'Byara', which means trade or business. Since the major portion of the community was involved in business activities, particularly trading, the local Tulu speaking majority called them as Beary or Byari.
According to the census of 1891, Dakshina Kannada had 92,449 Muslim businessmen consisting of 90,345 Bearys, 2,104 Nawayaths and 2,551 non-Muslims. This means that the district had 95,000 individuals involved in business activities. Records prove that, towards the end of the 19th century, the percentage of Muslim traders in the district was as high as 97.3%, and hence the local Tuluvas rightly named this community as Bearys.
Another popular theory is that the word "Beary" comes from Arabic word "Bahar" (Arabic: بحر). "Bahar" means "ocean" and "Bahri" (Arabic: بحري) means "sailor or navigator". It is said that the Beary community had trade relations with Arab businessmen travelling to coastal South India, especially the coastline of Tulunadu and Malabar. Inscriptions have been found in Barkur that proves the Arab trade links with Tulunadu.
A third theory says that the word "Beary" is derived from the root word "Malabar". The great Islamic Da'ee, Malik bin Deenar had arrived on the coast of Malabar during the 7th century with a group of Da'ees, or Islamic propagators. A member from his group, Habeeb bin Malik travelled through Tulunadu and preached Islam. He had also built mosques in Kasaragod, Mangalore and Barkur.


== Geographic distribution ==
The Bearys make up around 80% of the Dakshina Kannada Muslims, with others scattered in the neighbouring districts of Chikmagalur, Shimoga, Kodagu, Hassan and Uttara Kannada. Mumbai and Goa also have a considerable Beary population. Also, a good number of Bearys are in the Persian Gulf States of the Middle East doing a variety of jobs. The total Beary population numbers about 1.5 million.


== History ==
Beary community has a history of more than 1,350 years with an ethnic identity and speaking its own dialect called Beary bashe or nakk-nikk, also known as beary palaka.
Bearys used to refer the area south of Mangalore as Maikala or Maikal which is in fact their culture and economic capital. According to historian B. A. Saletore, Maikala was an area in the southern part of Mangalore. It got its name through the Kadri Manjunath Temple, which earlier was a Buddhist temple. The Buddhist goddess Tara Bhagavathi was also known as Mayadevi. In course of time it came to be called as Maikala, or Maikal. Historians are of the opinion that "Maikala" is one of the ancient names of Mangalore. But today, "Maikala" refers to the whole of Mangalore city covered by the Mangalore City Corporation.


=== Origin ===
The origin of the Beary community is still uncertain. History reveals that there were many rich traders, from the Malabar coastal belt, dealing with the traders of the Middle East through the Arabian Sea. Arab merchants have been visiting the coastal regions for business purposes even before the time of Muhammad, the prophet of Islam.


=== Da'wah ===
The first Muslim missionaries to Mangalore can be traced to Malik Bin Deenar, an Arab trader said to be the kith and kin of Sahaba (companions of Prophet Muhammad). He is said to have visited Malabar and landed near Manjeshwar in the northern Malabar coast. He constructed the first mosque in Kasargod, the Malik Dinar Mosque (where his shrine is still present). Also the Masjid Zeenath Baksh popularly known as Jumma Masjid or beliye palli, in the Bunder area is said to have been established in Mangalore by Habeeb bin Malik in AD 644, and the first Qadhi (Qazi) appointed was Hazarath Moosa Bin Malik, son of Malik Bin Abdullah. Records reveal this Mosque was inaugurated on Friday the 22nd of the month of Jumadil Awwal (fifth month of the Islamic Calendar) in year 22 of Hijri (644).


=== Other sources ===
There are several documents available which prove that at least 90 years prior to the invasion of Muhammad bin Qasim in North India, Arab Muslim businessmen were thriving in the south. This proves Islam was prevalent in South India much before Muslim conquerors came to North India. These facts are available in a research document Mykal, written by Ahmed Noori, who conducted a research on the Beary community in 1960.
Noori disputes the claim that the first Muslims came to India along with Alauddin Khilji between 1296–1316 AD and points out that according to renowned historian, Henry Miers Elliot, (The History of India as told by its own Historians, Part I) the first ship bearing Muslim travellers was seen on the Indian coast as early as 630 AD. H.G.(Hugh George) Rawlinson, in his book: Ancient and Medieval History of India, claims the first Arab Muslims settled on the Indian coast in the last part of the 7th century AD. Bartholomew also has similar things to say about the early Muslim settlers in India. J. Sturrock in his Madras Districts Manuals: South Kanara, says that Parsi and Arab businessmen settled in different places of the Malabar coast during the 7th century. Ahmed Noori has quoted these and other sources to validate his argument that the Arab and other settlers came to India much before the Arab,Turk and Afghan conquerors came to North India.
Dr. Susheela P. Upadhyaya, a research scholar in Beary bashe and Beary folklore is of the opinion that the Indian west coast came under Islamic influence long before any other part of India was influenced by Islam or Muslims. Historical research also reveals that during the rule of Banga and Chowta dynasty in the 16th century, Beary men had served as seamen in the naval force. The Chowta dynasty queen, Rani Abbakka had personally supervised the construction of dam at Malali; she had appointed Bearys for boulder work.
An ancient historical work – Keralolpathi – reveals that a king of Malabar, Cheraman Perumal, embraced Islam during the very beginning days of the advent of Islam in the Arab land. Thus the Arabs had royal patronage to practice and propagate Islam in the Malabar area. They were also given the permission of sea trading with a royal patronage. Because of the Da'wah activities of Arab traders, many people from the down-trodden section of society embraced Islam and assumed better social status as Muslims.
The Portuguese lost their dominance during the rule of Hyder Ali and Tippu Sultan in Mysore. During this period the Beary Muslims again received royal patronage and intensified their sea trade activities.


== Participation in the freedom struggle ==
The Bearys of the coast actively participated in the Indian freedom struggle against Portugal and British colonialism. There were a number of Beary men who served in the naval force, and also as soldiers and military commanders in the army of brave queen of Chowta dynasty, Rani Abbakka (Kannada: ರಾಣಿ ಅಬ್ಬಕ್ಕ) who ruled in the Ullal region. The Bearys had also joined the army of Nawab Hyder Ali and Tipu Sultan of Mysore. Historians and researchers have enlisted famous Beary personalities who participated in the freedom struggle of India. Many such freedom fighters were imprisoned by British, and a few died during imprisonment.
A sixteenth-century Arabic work of Malabar, Tuhfah al Mujahideen or Tuhafat Ul Mujahideen compiled by Shaikh Zainuddin Makhdoom II (grand son of Shaikh Zainuddin Makhdoom I) had motivated Malabar Muslims which had influence on Bearys of the Tulunadu as well to fight the foreign invaders. Thus the Bearys had actively participated in the freedom struggle against Portugal and British rule.


== Language ==

The dialect spoken by Beary (Byaris), is known as Beary Bashe. While Muslims of Uttara Kannada, called Nawayaths, speak a dialect of Konkani, and the Mappilas of Kerala speak Malayalam (Mappila Malayalam), the Bearys spoke a language made of Malayalam idioms with Tulu phonology and grammar. This dialect was traditionally known as Mappila Malayalam because of Bearys close contact with Mappilas. Due to vast influence of Tulu for centuries, it is today considered as a language, close to Malayalam and Tulu.
Beary bashe is largely influenced by the Arabic language. Most of the Bearys especially in coastal area still use a lot of Bearified Arabic words during their daily transactions. Saan, Pinhana, Gubboosu, Dabboosu, Pattir, Rakkasi, Seintaan, Kayeen, are the few words used in Beary bashe that have their roots in Arabic language. Beary Bashe also has words related to Tamil and Malayalam. Tamil and Malayalam speakers can understand Beary by about 75%.
'Beary Sahitya Sammelana (Literary Summit of Bearys)'[edit]
Total Four Beary Sahitya Sammelanas (The Beary Literature Summit) have been taken place so far. Cultural activities, exhibition related to Beary culture and society, talks on Beary society by Beary scholars, publications and Beary literature stalls are the centre of attraction during any Beary Sahitya Sammelana.
The first Beary Sahitya Sammelana, held on November 11, 1998 at Town hall Mangalore, was presided by B.M. Iddinabba, Poet and Former Member of Legislative Assembly, Ullal constituency, Karnataka State.
The second Beary Sahitya Sammelana, held on November 21, 1999 at the Indian Auditorium, Bantwal, was presided by Golthamajalu Abdul Khader Haji.
The third Beary Sahitya Sammelana, held on October 28,2001 at the Halima Sabju Auditorium, Udyavara, udupi, was presided by Beary research scholar Prof. B.M. Ichlangod.
The Fourth Beary Sahitya Sammelana was presided by novelist Fakir Mohammed Katpady. Fourth Beary Sahitya Sammelana[edit] The Fourth Beary Sahitya Sammelana (The Fourth Beary Literary Summit), held at Vokkaligara Samaja Bhavana in the city of Chikmagalur on 27 February 2007 which demanded that the state government establish a Beary Sahitya Academy. The Sammelana was jointly organized by Kendra Beary Sahitya Parishat, Mangalore, and Chickmagalur Bearygala Okkoota. Chikmagalur is the district that harbors the second largest Beary population, next to Dakshina Kannada.
The theme of the Sammelana was Prosperity through Literature, Development through Education and Integrity for Security. [15]
The sammelana also took up issues such as official recognition to the Beary Bashe by the State Government, setting up of Beary Sahitya Academy, and recognition to the community as linguistic minority. It is said that Beary Bashe is as old as Tulu and spoken by more than 1,500,000 people around the world. The history of this dialect is at least 1200 years old.[10]


== World Beary Convention ==
The World Beary Convention was held 2006 in Dubai under the banner World Beary Sammelana & Chammana 2006.
The word Chammana stands for felicitation. Since the organizers felicitated a few Beary dignitaries during this world convention held in Dubai, UAE, the convention is called World Beary Sammelana & Chammana 2006. The Convention was also attended by several dignitaries which included Dr. B.K.Yusuf, President/Patron of Karnataka Sangha, Dubai, M.B. Abdul Rahiman, Renowned Lawyer and Notary, Syed Beary, managing director, Bearys Group, B.M. Farooque, managing director, Fiza Group, Shiraj Haji. Director Universal Export Tradeways. S.M. Syed Khalil, Galadai Group, Dubai, M.B. Noor Mohamed, MD. Fakruddin, managing director, Ajmal Group, Mel, Abdul Jaleel, Abdussalam Puthige, Editor in Chief, Varthabharathi Kannada Daily, Haju Jamalluddin, Chairman, Crescent School, Shamshudeen, P.T. Abdul Rahiman, General Secretary of Indian Islamic Centre, T.S. Shettigar, Jamalludin, Apsara Group, Dr. Viquar Azeem, Dr. Azad Moopen, Ganesh Rai, M.K. Madhavan, Kumar, Indian Association Dubai, Kanukaran Shetty, President Hotel, Prabhakar, KOD, K.P. Ahmed, Yaseen Malpe etc. Some Beary dignitaries have been facilitated during the convention.


=== World Beary Convention & Chammana 2010 ===
Beary hosts a 2-day event, the World Beary Convention & Mega Cultural Event – Chammana 2010, held during 2 and 3 April 2010 at Zabil Ball Room, Radisson Blu Hotel, Creek Side, Dubai. During this convention Beary Personality of the year 2010 award was given to Mr. Mumtaz Ali, and Star of Bearys award for the year 2010 was conferred on Zakariya Bajpe.
Media of the Year award was jointly awarded to Varthabharathi a renowned Kannada daily (published from Mangalore and Bangalore) and Daijiworld.com, a web based newsportal, for their contribution towards media. Abdussalam Puthige, on behalf of Varta Bharti, and representatives of Daijiworld received the award from Mr. C. M. Ibrahim, Former Union Minister.


== Media activities ==
The people of this community have played a vital role in the media activities of Tulunadu or Dakshina Kannada district. Apart from publishing a lot of books in Beary bashe and Kannada, Bearys have also brought out periodicals, magazines and newspapers. Some of such works are now a history but some are running todate with good reputation. Very recently Beary community people have also launched an online news portal: coastaldigest dot com.


=== List of periodicals brought out by Bearys ===
Some of these periodicals are still being published and reaching to the hands of a sizeable population of Tulunadu and other adjacent districts and to the Persian Gulf States.


== Culture ==
The Bearys have a distinct culture different from other Muslim communities. The marriage customs of the Bearys seem to be a mix of the Tulu and Moplah customs along with some distinct customs of their own. Bearys do not follow the matrilineal tradition, unlike the Moplahs. Curiously, the Bearys until recently followed a custom known as the Gotra or illam, which resembles the Bunt bali custom. Though Islam is basically patriarchal, illam, influenced by the Tulu culture, has matriarchal tendencies. Marriage between people belonging to the same illam (comparable to the gotra) was not encouraged. People identified with an illam were known as talakkar. And people of low castes who converted to Islam were identified as tala illatavaru. A similar illam system with the same name illam is stringently being practised by some Brahmin families in Kerala. Those are basically Tulu Brahmins and are hailing from Tulunadu. In Tulu language word illu stands for house. So it is evident that illam system observed by Beary people is taken from the local Tulunadu culture. In the recent days, as this community has come closer to the original teachings of the religion they follow, Islam, this illam system has vanished from the day-to-day life of Bearys.
The marriage of the Bearys is considered to be a pointer to their prosperity. Marriage celebration is normally spread over three days, starting with Moilanji (henna tattooing) at the bride's house a day before the marriage (close relatives and friends are invited) and an Islamic-style Nikah known as Kayeen is performed at the groom's place on the day of the marriage. A garland exchange between Bride and groom is also part of Beary marriage which is an adoption from Tulu culture. The dowry system is still quite prevalent among most Bearys.
Modern marriages of most of the families are arranged in community halls with large number of invitees including relatives and community members.


=== Paunaraga of Maikala ===
Before the advent of the Portuguese, Maikala or Mangalore was one of the main centres of Jains with many Jain Muts, Basadis and also palaces. Especially the Bunder area Maikala was dominated by rich Jain houses. The Jains who enjoyed economical and social status maintained a system known as Jaina Beedu, which literally means Jain House.
Later when these Jains embraced Islam, they still maintained this Beedu system as status symbol. Beedu can be translated in Beary bashe as Aga which means House. The Paunar Aga or Paunaraga – which literally means sixteen houses – of high status are listed in the table below.
These houses enjoyed supreme social status amongst Bearys throughout the 19th century and treated other Bearys as second-class citizens. The people belonging to these houses were identified as Agakkar which means the People of the House. The history of these houses has a short-lived glory that these houses enjoyed socially and economically. Many of the social customs that the people of Paunaraga observed were special to them and had no roots in Islam.
Thus the lifestyle of Agakkar of the Beary community was largely influenced by Jains. Most of the ornaments used by Agakkar was of Jain pattern and had Jain names. Kharjana is the jewel box used by both Jains and Bearys. Today the people of Paunaraga or Agakkar have lost their social and economic status but some of the houses still remain in Bunder area. Their surnames tell the glory once they enjoyed.
Next to Agakkar comes Taalakkar and then Taala-illatavar. All these system the Bearys maintained in the olden days which they inherited from local people. However these systems are vanished with the advent of Islamic literature of late.


=== Some peculiar names of Bearys ===
Typically, Muslim community people name their children for Arabic roots. But olden day Bearys had some strange names which are not seen anywhere else in the Muslim world. Although those peculiar names are now vanishing, here are some such examples:
Kayiri, Sayiri, Sayirabba, Cheyya, Cheyyabba, Saunhi, Kayinhi, Sekunhi, Baduva, Mayabba, Puthabba, Hammabba, Cheyyabba, Ijjabba, Kunha, Kunhi, Bava, Bavunhi, Kunhibavu, Puttubavu Unha, Unhi, Unhimon, Iddinabba, Podiya, Podimonu, Pallikunhi, Kunhipalli, Kidavaka, Abbu, Abbonu, Chakaka, Addiyaka (Addi), Pudiyampule.


=== List of the Books related to Beary culture ===
These books are available at the largest library in the world, the Library of Congress in Washington, D.C., US.


== Marriage ==
In olden days, Bearys used to hold ceremonial marriage functions. Its prodigality some times remained for a full year. Pomp and flaunting rituals and dinner parties some times made some wealthy families victim of bankruptcy. Most of these customs and rituals were against the teachings of Islam, the religion of Bearys. There is no evidence for dowry system in the olden-day Beary marriages, but a lot of customs inherited from the local Tuluva communities living in Tulunadu have been noted.
Marriage in Beary bashe is known as "Mangila". A lot of ceremonial rituals related to Beary Mangila, which once was an essential part of nuptials, today is vanishing. Keli kekre, Naal kuri, Bethale beikre, Varappu, Moilanji, Kayeen, Beett, Birnd, Oppane, Kaikottu patt, Appathe mangila – all these are related to Mangila or the Beary marriage.


== Madrasah education system ==
Although this community is backward in modern education, it still has successfully achieved 100% literacy rate due to prevailing Madrasa education system. All the Beary children are sent to Madrasah (Arabic: مدرسة) which is managed and run by the community that imparts religious education. All such Madrasahs are affiliated to "Sunni Educational board of India" and Samastha Board which conducts well organised public examination for 5th, 7th and 10 and 12th grade students. Visiting inspectors called Mufattish are appointed to inspect the quality of education in Madrasahs. For administrative purposes divisions have been made as range, area, taluq and district. The teachers are qualified in Arabic language and religious education are known as Mu'allim and students as Muta'allim. The Madrasahs do use a centralised syllabus prepared by the Sunni educational board and Samstha Board and media of instruction is now shifted to Kannada from traditional Arabic based Malayalam called Arabi-Malayalam – a special language that uses Arabic script and Malayalam phonetics.
Mundu, Chatte and Toppi is the preferred uniform for boys in Madrasas. Girls do wear a long gown with a head-dress known as yalasara. But today this traditional dress pattern is vanishing. Boys are going for shirt – trousers and girls are adopting Churidars and Salwar kameez style.
In those villages where there is no separate building facility available to run Madrasas independently, this education is however imparted in the mosques itself. Thus mosques some time do play the role of Madrasas in many Beary dominant villages.
Apart from Sunni educational board and Samstha Board many other educational institutions also have been surfaced lately. The Salafi group has established their own Madrasahs throughout Dakshina Kannada district. Salafis also have started separate religious schools exclusively for girls in Ullal. Jamat-e-Islami is now running an exclusive college for girls in Deralakatte province. There are several other schools managed by Bearys which are aimed at providing both modern and religious education simultaneously to the children.


== Attire ==
The Beary attire is different from that of other south Indian community. Men wear a traditional white muslin turban and a Rani-mark belt (wide, green in colour) at the waist, with long full sleeve white shirts (known as Chatte) and bleached mundu. MS belle mundu and Moulana mark Kambai is also among traditional outfit of Bearys. Today due to a cultural shift young Bearys have adopted a shirt-trouser pattern.
Beary women are traditionally clad in three pieces of clothes, viz, tuni, kuppaya and yalasara. While going out the Beary women took a long rectangle blanket, known as valli, a sort of veil to cover entire body. If two women want to go out together they would use a joduvalli (double veil). Surprisingly Valli of Beary bashe and Veil of English language have similarity in pronunciation and convey the same meaning. Thus it demands an etymological research into these words.
Today, different varieties of burqa or Abaya have replaced the traditional valli. Hence Beary women now wear a black over-coat known as Burqa or Abaya while going out. The Abaya or Burqa is more like a business suit for a Beary woman while she is out of home. Many exclusive Arabic pattern Abaya shops have also emerged with commercial interest.


== Ornaments ==
The beary women has excessive love for ornaments and uses it on every possible occasion such as Mangila, Sunnat Mangila, Appate Mangila, Birnd, Moilanji and other social gatherings. There were different types of ornaments used by beary community in past which is at the verge of vanishing today due to the cultural invasion and urbanisation. These ornaments are made out of mainly gold and silver and used for the ornamentation of head, ears, neck, waist, wrist, fingers and feet. Beary research scholars are of the opinion that Beary ornaments were largely influenced by Jain ornament patterns. The ornament storage box used by Bearys was made out of brass and other metals was also used by Jain community and was called Kharjana by both Bearys and Jains.
Head ornaments: Tale singara, Tirupi, Kedage, Jadepalle, Nera Nilaavu, Chauri
Ear ornaments: Alikat, Kett Alikat, Illi Alikat, Kuduki, Bendole, Lolak, Voli, Jalara,Koppubalsara, Vale
Neck ornaments: Misri male, Sara, Naklees, Bandi male, Minni male, Nalchuttu male
Wrist ornaments: Cheth Bale, Alsande bale, Kett bale, Yeduru bale, Sorage Bale, Kadaga
Waist ornaments: Aranjana, Arepatti
Finger ornaments: Modara, Kallre modara
Feet ornaments: Kunipu, Kal sarapali, Chein


== Festivals ==

The traditional Islamic festivals of Eid ul-Fitr and Eid ul-Adha (also known as Bakrid) are celebrated. Special Eid prayer is offered during these two occasions. Mangalore city has a centralised Eidgah in Bavuta Gudda where congregational special prayers or Salat al Eid is held. The Eidgah of Mangalore city has a mosque which is said to have been built by then Mysore ruler Tipu Sultan towards the close of eighteenth century. Usually in the central Eidgah the Qadhi leads the Eid prayer and delivers Khutba. Colourful costumes, delicious food, exchanging the Eid greetings – Eid Mubarak (Arabic/Persian/Urdu: عید مبارک) and generous charity to the poor and needy are part of Eid celebration. Other occasions celebrated are 12th Rabi' al-awwal of the third month of Islamic calendar commemorating Meelad-al-Nabi – prophet Muhammad's birthday. Moon citation is an event of rejoice for Beary folk.


== Cuisine ==

Beary cuisine is highly influenced by the South Indian Cuisine. Just like Mangalorean cuisine it uses a lot of coconut, curry leaves, ginger, chilli and spices like pepper and cardamom. Beary cuisine boasts of a special kind of biryani, which is very different from the other types made elsewhere. Rice preparations, both fresh and dry fish, meat and eggs enjoy top place in Beary daily menu.
A few traditional dishes very popular amongst all the Tulu communities have unique names in Beary dialect. Pindi/pinde, basale-pinde, kunhi-pinde, erchiro-pinde, bisaliyappa (bisali beetiye appa/kaltappa), tondaredo-appa (syame), guliyappa, muttere-appa, neiyappa/nei-appa, chekkero-appa, manhel elero appa, acchi-appa, daliyappa, baale-appa, pondatte-appa, sank-roli, pattir (a distorted version of Arabic fatirah), nei pattir, pole, poo-pole, pulcho-pole, vodu-pole, uppu-molavu, kanhi, methero-kanhi, nei-kanhi, kulte kanhi, manni, nombure kanchi, pirni etc. are to name a few.


== Economic situation ==
The Bearys, who once enjoyed a high social status, slowly lost their position during the British and Portuguese rule. Their opposition to the English resulted in them being denied English education, which in course of time turned them into a socially backward community.
Today, hardly 20% of the community is engaged in trading and business, thanks to the modern education community offlate have been seeing professionally qualified members. Some Bearys are involved in the beedi industry and fish trade, and a majority are farmers. A few Bearys have progressed even further in the past few years and have achieved tremendous development in the field of Education, Business & Politics. Bearys today own many Educational Institutions. Professional Colleges in Mangalore are mostly owned or partnered by Bearys. Bearys have also achieved high positions in Karnataka Politics and few have also attained positions in the Central Government. In spite of these achievements, majority of the people of the community are still economically backward. The recent job opportunities in Persian gulf countries have improved the standard of living to some extent. However, Bearys in rural areas are still extremely backward socially, economically and educationally.
Since Islam prohibits interest based financial dealings, Beary community is financially undermined by the interest based or conventional banking system. In the modern days they have embarked on establishing a small scale interest free banking system namely Interest Free Loan and Welfare Society in Mangalore.


== Beary organisations ==
Today the Beary community of coastal Karnataka is surging ahead in diverse fields like international business, education, medicine and technology. Bearys have also formed various social and cultural organisations of diversified interests. Beary's Welfare Association, Bangalore Beary's Welfare Association is based in Bengaluru the capital of Karnataka state. The association came into being on 21 March 1988 with a motive to provide a means of communication and integration, and also to provide a platform to work towards the betterment of the Beary community in all aspects of life.
Beary Welfare Association has organised a number of cultural programs every year right from its very inception. Beary Prakashana is its sister concern and involved in print and publication activities. It has published a number of titles on Beary culture, Beary bashe, Beary history, and also on research studies on Bearys.
Kendra Beary Sahitya Parishath, Mangalore
The Kendra Beary Sahitya Parishath founded in the year 1998 in Mangalore has been the moving force of the Beary community in its collective endeavor. The Parishath has been spearheading a silent revolution in the community towards its progress in an environment of overall social amity. To streamline the community's efforts in this direction, the Parishath has been regularly organizing conferences, seminars, educational workshops, publishing activities, writers and thinkers conclave etc. The Parishath's demand for the official recognition for the BEARY language and establishing a BEARY Academy has received positive response from the Government of Karnataka. The Parishath has successfully organized 4 Sahitya Sammelanas viz Mangalore (in 1998 Presided by BM Iddinabba), Bantwal (in 1999 Presided by Abdul Khader Haji Goltamajal) Udupi (in 2001 Presided by BM Ichlangod) and Chikmagalur (in 2006 Presided by Fakir Mohd. Katpady)
The founding Office bearers of Kendra Beary Sahitya Parishath:
B.M. Iddinabba : Mentor and Adviser
Abdul Raheem Teekay : Founder President
M.B. Abdul Rahiman
Abdul Khader Haji Goltamajal
U.J.M.A Haq
U.T. Khader
U.A. Kasim Ullal
Basheer Baikampady
B.A. Mohammed Ali
Mohammed Kulai
U.H. Umar
Ibrahim Tannirbavi
Khalid Tannirbavi
P. Hussain Katipall
Sharief Nirmunje
D.M. Aslam
Abdul Rahman Kuttettur
Abdul Khader Kuttettur


=== Bearys Welfare Forum (BWF), Abu Dhabi ===
Bearys Welfare Forum of Abu Dhabi, popularly known as BWF is an association of Beary expatriats in Abu Dhabi, United Arab Emirates. It does community activities and mainly community welfare activities.,
BWF was established in the year 2004 with an intention of working for all sections of the society. It has helped the victims of communal riots in Mangalore by providing medical assistance and other aids. The BWF gained popularity when it held mass marriage ceremony of twelve pairs of poor and deserving youth at the Shadi Mahal of Mangalore city.
Bearys Welfare Forum, Abu Dhabi, organised a mass wedding ceremony at Milagres Auditorium, here on Monday 13 July 2009. Sixteen couples were solemnised in marriage by Kazi Al Haj Abdulla Musliar Chembarika.


=== Bearys Cultural Forum (BCF), UAE ===
Bearys Cultural Forum (BCF), UAE was constituted to provide education to the poor masses of the coastal parts of the State of Karnataka. BCF's main objective is to promote, educate and create social, cultural and educational awareness amongst the Bearys and the downtrodden population of the coastal Karnataka State and the UAE. The BCF regularly conducts cultural, sports, talent search, educational activities, Career Guidance Seminars, Iftar Party, etc. every year. BCF also provides educational scholarship to the poor downtrodden students for pursuing their higher studies in the field of Medicines, Engineering, Pharmacy, business, nursing, Journalism, Dentistry, etc. BCF is now committed to conduct "World Beary Convention & Chammana 2010" in Dubai, U.A.E.


=== M.G Rahim (Capman Media Makers ) ===
M.G Rahim has been active in 'Beary Movement' for the last eight years. It has felicitated Beary poets, writers and others who have come up with remarkable achievements in the society." beary naseehath majlis ", Surmatho Kannu, Maafi Mushkil, Pernal nilaavu', . "Moilanjipoo" " Maikalthoraja ",and stage programme beary oppane kali, beary kolkali, pernal nilaavu, Eid special programme on costal T.V channel yearly are some of the productions of Capman Media Makers., ..../


== References ==

www.kmbeary.com


== Other sources ==
Bearys of the coast, Article in Deccan Herald 12 December 1997 by B.M Hanif.
H.G. Rawlinson, Ancient and Medieval History of India
Sturrock, J., Madras District Manual. South Kanara (2 vols., Madras, 1894–1895).
Influence of Muslim thought on the east [4] retrieved 21 May 2006.
Muslims in Dakshina Kannada: a historical study up to 1947 and survey of recent developments, Author Wahab Doddamane, A. Green Words publication. Mangalore, 1993 [5]


== External links ==
Zeenath Baksh Juma Masjid
Another link on Zeenath Baksh Juma Masjid
Web page with information on Bearys
Vijay Times' Article on Ahmed Noori
Article on Beary's claim to a distinct ethnic identity
Bearys Welfare Association Home Page
The Arabi-Malayalam Script
Arabi-Malayalam history
References to Zainudhin Makhdum II
More references to Zainudhin Makhdum II
The Beary Welfare Association press release
Beary lyricist and composer
Beary book on Namaz (Islamic prayer)
Article which refutes claim that Beary bashe is only spoken by Muslims
Beary community mentioned in Coorg based website
Kudroli Jamia Masjid website