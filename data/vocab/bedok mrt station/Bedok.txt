Bedok /bəˈdɒk/ is a neighbourhood in the eastern part of Singapore. Bedok New Town is the fifth Housing and Development Board (HDB) new town.
The Bedok Planning Area, an urban planning zone under the Urban Redevelopment Authority, encompasses the Bedok New Town itself, the low-rise private residential areas along Upper East Coast Road, and in the districts of Kembangan, Siglap and Telok Kurau, and the high-rise private condominium developments in the eastern part of Marine Parade.


== Etymology and early history ==

"Bedok" seems to be a very old place name. In the 1604 Manuel Gomes de Erédia's map of Singapore, there is a reference to the Bedok River called sune bodo (Sungei Bedok).
Bedok is one of the early native place names in existence around the time of Sir Stamford Raffles. In the first comprehensive map of Singapore Island completed by Frankin and Jackson and reproduced in John Crawfurd's 1828 book, the place name appears on the south east coast of the island as a river, Badok S. (Sungei Bedok), around the "small red cliff", a part of present Tanah Merah.
The Malay word bedoh refers to a very large drum, used for calling people to a mosque for prayers or to sound the alarm in the days before loudspeakers. There was a prominent mosque in the 1950s at Jalan Bilal that still used the drum about five times a day. The "h" in the word bedoh was replaced with a "k", and, as with most Malay words that end with a "k", it is pronounced with an inaudible glottal stop.
A less popular version refers to an equally uncommon Malay term of biduk, a small fishing boat like the sampan, or more likely, a dugout canoe, as the east coast was dotted with many fishing villages.


=== Modern development ===
Bedok New Town had been developed since 1973 with the newer roads such as Bedok Plain, Bedok Highway and Bedok Heights being built all the way until 1975. The New Upper Changi Road was fully built and opened in 1979, where the massive development had been completed except Bedok Reservoir and Kaki Bukit, which was built later in 1983 - 1988.


== Bedok New Town ==

Bedok New Town covers a land area close to 9.4 km² with some 42% occupied for residential use. It was formerly a hilly region and hence the focal point of orientation of the town is the special landscaped park and sports complex built on the higher ground of the town. The residential blocks as well as the industrial area are planned based on the neighbourhood concept. There is also a town centre together with Bedok Mall and Bedok Point being built. Plans for an integrated complex, which will be as big as 3 football fields, have also been revealed in 2014. This complex will house a sports centre, library, clinic, centre for the elderly and the Kampong Chai Chee Community Club. The complex will be located in Bedok town centre and will be ready in 2017.


=== Residential development ===
There are some 58,000 units of flats built by the HDB in Bedok New Town. As one of the older towns, the majority of the flats are 3-room or 4-room. There are also some 2,700 and 583 units of executive and Housing and Urban Development Corporation (HUDC) flats. It provides housing for some 200,000 residents.


=== Transportation ===
The Mass Rapid Transit station, Bedok MRT Station, serves the Bedok neighbourhood and is centrally located, at the south-west corner of Bedok Town Center. Adjacent to the MRT station on the north side is the Bedok Bus Interchange, a major bus terminal connecting residents with SBS Transit Townlink and Feeder services 222, 225G, 225W, 228, 229 and Trunk services. SMRT Buses also operates as a minority, Trunk service 854 towards Yishun and shuttle services towards Tampines Retail Park.


== Education ==
The schools located in Bedok Planning Area include:
Primary Schools
Bedok Green Primary School
Bedok West Primary School
Damai Primary School
East Coast Primary School
Fengshan Primary School
Opera Estate Primary School
Red Swastika School
Saint Anthony's Canossian Primary School
Telok Kurau Primary School
Temasek Primary School
Yu Neng Primary School

Secondary Schools
Anglican High School
Bedok Green Secondary School
Bedok North Secondary School
Bedok South Secondary School
Bedok View Secondary School
Damai Secondary School
Haig Girls' School (Temporary)
Ping Yi Secondary School
Saint Anthony's Canossian Secondary School
Victoria School

Temasek Secondary School
Junior Colleges
Victoria Junior College
Temasek Junior College

Other Schools
Global Indian International School (GIIS}, East Coast Campus
Katong School (APSN)
NPS International School
Sekolah Indonesia Singapura


== See also ==

Bedok Village
Bedok Reservoir
Sungei Bedok Canal


== References ==

URA Draft Masterplan 2013 Bedok
Victor R Savage, Brenda S A Yeoh (2003), Toponymics - A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1
From National Library Board Singapore, Infopedia website.
Chin, Daryl. (2014). Bedok residents to get new sports complex, library and community club under one roof by 2017. The Straits Times. [1]


== External links ==
View Bedok Estate and surroundings
Bedok Free Online Community Forums (BedokToday.COM)