Bedok Bus Interchange (Chinese: 勿洛巴士转换站) is an air-conditioned bus interchange located in the approximate center of Bedok. It is situated along Bedok North Interchange and Bedok North Drive, on the second level of Bedok Mall. It is connected to Bedok MRT Station. It is the 2nd busiest bus interchange in Singapore, with 29 bus services operating from it.


== History ==
Opened in 1979, the original facility was built as part of the Bedok Town Centre, located along Bedok North Road and in between Blk 203 and 207, next to community amenities such as a food centre, library and sports complex, with the allocated Blk 207A. There were thirty three end-on berths with 10 services occupying it and 6 sawtooth berth each occupying 3 bus services, in the original facility. A portion of its bus services originated from Chai Chee Bus Terminal, which closed in 1985. Bedok MRT Station opened in 1989 at the south of the original facility, complementing the bus interchange to serve people travelling within Bedok town and also the nearby East Coast Park.
In the 2003 URA Masterplan, it was indicated that the bus interchange would be redeveloped. This was confirmed in early 2011 when HDB announced plans to rejuvenate Bedok, which involved redeveloping the Bedok town centre and rebuilding the original facility into an air-conditioned one, integrated with a proposed commercial development (Bedok Mall) and proposed residential development (Bedok Residences), resembling Boon Lay Bus Interchange. The original facility has been completely demolished.
On 19 November 2011, after operating from the original facility for 32 years, the bus interchange moved to its temporary facility west of the original facility, at the junction of Bedok North Drive and Bedok North Avenue 1, to allow the original facility to be rebuilt as planned. With the opening of the temporary facility, some amendments were made to three services, Service 9, 18 and 35.
The new Bedok Integrated Transport Hub (ITH) began operations on 30 November 2014 (Sunday). With the completion of the ITH, Bedok residents will be able to transfer in air-conditioned comfort, between bus and MRT services at Bedok MRT Station. In addition, it is the 7th air-conditioned bus interchange.


== References ==


== External links ==
Interchanges and Terminals (SBS Transit)
Interchange/Terminal (SMRT Buses)