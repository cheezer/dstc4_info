Not to be confused with another defunct ward, Bedok Group Representation Constituency that was formed since 1988 general election til 1997 general election where this ward is part of that larger Bedok GRC.
It is also not to be confused with another ward, Ulu Bedok Single Member Constituency, which is a short-lived pre-independence era ward that covers the entire of present-day Bedok and Geylang Serai as back then the Bedok was one of the remote and undeveloped area.
Bedok Single Member Constituency (Simplified Chinese: 勿洛单选区;Traditional Chinese: 勿洛單選區) is a defunct Single Member Constituency in parts of Bedok South, Singapore. It was formed in 1976 general elections by craving a portion from Siglap constituency and continues throughout till prior to 1988 general elections where this constituency along with neighbouring constituencies namely: Kampong Chai Chee and Tanah Merah forms the greater Bedok Group Representation Constituency.


== Members of Parliament ==
Sha'ari bin Tadin (1976 – 1980)
S Jayakumar (1980 – 1988)
Constituency Abolished (1988 – ) Notes: As of present, it still serves as a sub-constituency of East Coast GRC


== Candidates and results ==


=== Elections in 1980s ===


=== Elections in 1970s ===
Note: By end of 1976, the remnants of People's Front had moved to United Front which was the reason for the elections department of Singapore to view Sim Peng Kim as a candidate for United Front.


== See also ==
Bedok GRC
Ulu Bedok SMC
East Coast GRC


== References ==
1984 GE's result
1980 GE's result
1976 GE's result
Map of Bedok Constituency (Within Siglap) in 1972 GE
Map of Bedok Constituency in 1976 GE
Map of Bedok Constituency in 1980 GE
Map of Bedok Constituency in 1984 GE
Map of Bedok Constituency in 1988 GE (And other nearby wards in Bedok GRC)
Brief History on People's Front (Dormant after the 1976's GE)