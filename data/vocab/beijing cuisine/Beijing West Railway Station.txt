Beijing West (Beijingxi) Railway Station (Chinese: 北京西站; pinyin: Běijīng Xī Zhàn; colloquially referred to as West Station 西客站) is located in western Beijing's Fengtai District. Opened in early 1996 after three years of construction, it is the largest railway station in Asia with 510,000m². The station serves in average 150,000–180,000 passengers per day with a maximum of 400,000 people per day. It was expanded in 2000 and had a vast amount of parking spaces added.
The Beijing West station cost a record three-quarters-of-a-billion dollars and its construction has been criticized by some over suspicions of corruption during the construction.


== Platform Layout ==

Regular rail services leave from Platforms 1-11; HSR leaves from Platforms 12-18. A dedicated exit is used for passengers arriving on Platform 18.

All passengers will leave Beijing West from the Level 2 Departures Hall, except for passengers to Hong Kong, who must enter from a dedicated entrance with immigration and customs clearance facilities located by North Entrance 1. These passengers will clear customs and immigration at Beijing West and will not leave the train until it arrives at the Kowloon / Hung Hom terminus in Hong Kong. A VIP lounge is available for Business Class passengers travelling HSR.
Ticket counters and machines are available beside the main entrances.


== Services ==
Beijing West Railway Station is a terminal for both "traditional" and high-speed trains. It is the Beijing terminal for most trains leaving the city for destinations in western and southwestern China, including Xi'an, Chongqing, Chengdu, Lhasa and Urumqi. Major "traditional" rail lines beginning at this station include the Beijing-Guangzhou Railway (via Wuhan) and the Beijing-Kowloon Railway (via Nanchang and Shenzhen).
Beijing West is the terminal both for the Beijing-Kowloon Through Train and (since the opening of the Qingzang railway in 2006) for the Beijing-Lhasa trains.
Beijing West is the northern terminal of the Beijing-Guangzhou High-Speed Railway as of December 2012. High-speed trains leave the station for Guangzhou and Shenzhen, as well as various destinations on the connecting lines, such as Taiyuan and Xi'an. There are, however, plans to construct new major railway terminal (the new Fengtai Railway Station) in the southwestern part of Beijing, and to eventually make it the terminal for the high-speed trains on the Beijing-Guangzhou line.


== Beijing Subway ==
Beijing Subway: This station is currently served by Line 9 and Line 7. Passengers are able to change trains using the cross-platform interchange method. The Subway concourse is on the Arrivals level, with all platforms a level further below.
Beijing Bus stops:
Beijing West Station (北京西站): 9, 21, 40, 47, 50, 52, 54, 65, 67, 83, 89, 99, 205, 209, 212, 213, 301, 319, 320, 373, 374, 387, 414, 437, 616, 623, 661, 662, 663, 673, 694, 695, 741, 840, 843, 845, 901, 特2, 特6, 运通102
Beijing West Station South Square (北京西站南广场): 53, 72, 109, 122, 309, 349, 410, 616, 820, 890, 941, 982, 993,

Beijing Airport Bus: Route 7
Bus route numbers in bold denotes terminus at the stop.


== References ==
^ Rough Guide to China - fifth edition, page 83
^ "Shanghai to have Asia's largest railway station". Xinhua. 10 August 2006. Retrieved 15 May 2010. 
^ http://rail.bmc-ad.com/media-locations/railway-stations/bejing-west-railway-station/
^ "Bribery in Chinese station construction". BBC News. 3 September 1998. 


== External links ==
Beijing West Railway Station (Chinese)
Beijing Huoche Zhan Network (Chinese)
Beijing Railways Time Table