Sambal is sauce typically made from a variety of chili peppers and secondary ingredients such as shrimp paste, fish sauce, garlic, ginger, shallot, scallion, sugar, lime juice, and rice vinegar or other vinegars.
Various recipes of sambals usually are served as hot and spicy condiments for dishes such as ikan bakar (grilled fish), ikan goreng (fried fish), ayam goreng (fried chicken), and soto. Sambal is an Indonesian loan-word of Javanese origin (sambel).


== Preparation and availability ==

Traditional sambals are freshly made using traditional tools, such as a stone pestle and mortar. Sambal can be served raw or cooked. The chili pepper, garlic, shallot and tomato are often freshly ground using a mortar, while the terasi or belacan (shrimp paste) is fried or burned first to kill its pungent smell as well as to release its aroma. Sambal might be prepared in bulk, as it can be easily stored in a well-sealed glass jar in the refrigerator for a week to be served with meals as a condiment. However, some households and restaurants insist on making freshly-prepared sambal just a few moments prior to consuming in order to ensure its freshness and flavor; this is known as sambal dadak (lit. "sudden sambal"). Nevertheless, in most warung and restaurants, most sambal is prepared daily in bulk and offered as a hot and spicy condiment.
Today some brands of prepared, prepacked, instant, or ready-to-use sambal are available in warung, traditional markets, supermarkets and convenience stores. Most are bottled sambal, with a few brands available in plastic or aluminum sachet packaging. Compared to traditional sambal, bottled instant sambals often have a finer texture, more homogenous content, and thicker consistency, like tomato ketchup, due to the machine-driven manufacturing process. Traditionally made sambals ground in a pestle and mortar usually have a coarse texture and consistency.


== Varieties of chili ==

The most common kinds of peppers used in sambal are:
Adyuma, also known as habanero: a very spicy, yellow, and block-shaped pepper.
Cayenne pepper: a shiny, red, and block-shaped pepper.
Madame Jeanette: a yellow–light green, elongated, irregularly-shaped pepper.
Bird's eye chili, also known as cabe rawit in Javanese: a very spicy, green–red, elongated pepper approximately 10 millimetres (0.39 in) wide and 50 millimetres (2.0 in) long.
Chili peppers known as lombok in Javanese: a mild, green–red, elongated pepper. Green chili peppers are milder than red ones.
Cabe taliwang: a pepper spicier than the Bird's eye chili, similar in spiciness to the naga jolokia, its name is supposedly the origin from which Lombok Island, or "the Island of the Chili", derives its name.


== Indonesian sambal ==

In the Indonesian archipelago, there are as many as 300 varieties of sambal. The intensity ranges from mild to very hot. Some varieties include:
Sambal andaliman
Similar to sambal lado mudo but with the addition of andaliman pepper.
Sambal asam
This is similar to sambal terasi with an addition of tamarind concentrate. Asam means tamarind or sour in Indonesian.
Sambal bajak (badjak)
Banten sambal. Chili (or another kind of red pepper) fried with oil, garlic, terasi, candlenuts and other condiments. This is darker and richer in flavor than sambal asam.
Sambal balado
Minangkabau style sambal. Chili pepper or green chili is blended together with garlic, shallot, red or green tomato, salt and lemon or lime juice, then sauteed with oil.
Sambal buah
(lit: fruit sambal) specialty of Palembang, made from the mixture of chili, shrimp paste, kemang (a type of mango) and pineapple.
Sambal cibiuk
a sambal recipe specialty of Cibiuk village, Garut Regency, West Java. It consist of coarsely chopped and ground green bird's eye chili, green raw tomato, shallot, galangal, lemon basil, shrimp paste and salt.
Sambal colo-colo
From Ambon, it consists of Indonesian kecap manis (sweet soy sauce), chili, tomatoes bits, shallots and lime it has a chiefly sweet taste. It is suitable for barbecue dishes. Some variations will add butter or vegetable oil to the sambal.
Sambal dabu-dabu
Dabu-dabu comes close to the Mexican salsa sauce, it is of Manado's origin. It consists of coarsely chopped tomatoes, calamansi or known as lemon cui or jeruk kesturi, shallots, chopped bird's eye chili, red chili, basil, poured with hot vegetable oil, salt.
Sambal durian or Sambal tempoyak
It is made from fermented durian called tempoyak. The fermentation process takes 3 to 5 days. The chili and the tempoyak may be readily mixed or served separately, to cater the individual preference in ratio of chili to tempoyak to determine the scale of hotness. This sambal IS available in two varieties: raw and cooked. In the cooked variety, pounded chilis, shallots and lemongrass are stir-fried with anchovies, tempoyak and turmeric leaf (for aroma). Petai (Parkia speciosa) and tapioca shoots are also frequently added. The sweet-sour-hot sambal can be found in Sumatra and Kalimantan (Indonesian Borneo), especially in Palembang and Bengkulu.
Sambal gandaria
Freshly ground sambal terasi with shredded gandaria, a kind of tropical fruit native to Southeast Asia.
Sambal goreng
Literally means "fried sambal". It is a mix of crisp fried red shallots, red and green chili, shrimp paste and salt, briefly stir-fried in coconut oil. It can be made into a whole different dish by adding other ingredients, such as sambal goreng ati (mixed with diced liver) or sambal goreng udang (added with small shrimp).
Sambal jengkol
Freshly ground sambal terasi mixed with sliced fried jengkol, a kind of tropical bean with slightly stinky aroma native to Southeast Asia. Sambal jengkol can be found in Sundanese and Cirebon cuisine.
Sambal kalasan
Sometimes also called sambal jawa. Similar to sambal tumis, it is stir fried. It uses a heapful of palm sugar which gives its dark brown color, tomato, spices and chili. The overall flavor is sweet, with mild hints of spices and chili.
Sambal kacang
A mixture of chilli with garlic, shallot, sugar, salt, crushed fried peanuts, and water. Usually used as condiments for nasi uduk, ketan, or otak-otak. The simple version only employ cabe rawit chilli, crushed fried peanuts and water.
Sambal kecap
A sambal consists of Indonesian kecap manis (sweet soy sauce), red chili, tomatoes bits, shallots and lime, it has a sweet and spicy taste and usually used for barbecue dishes.
Sambal kemiri
This is similar to sambal terasi with an addition of candlenuts.
Sambal jenggot
Sambal with an addition of grated coconut, similar to urap.
Sambal lado mudo
Literally a Minangkabau word for "green sambal". It is also known as sambal hijau or sambal ijo, also "green sambal". Sambal lado mudo, a West Sumatran specialty, used green chili, with dried shrimp, red shallots, garlic, and spices. It is one of those taste sensations that’s hard to beat with a unique fresh flavor that compliments the richness of Sumatran food so extremely well. The sambal is stir fried.
Sambal matah
Raw shallot & lemongrass sambal of Bali origin. It contains a lot of finely chopped shallots, chopped bird's eye chili, terasi shrimp paste, with a dash of lemon.

Sambal petai
A mixture of red chilli, garlic, shallot, and petai green stinky bean as the main ingredients.
Sambal petis
Uses chili, petis, peanuts, young banana, herbs and spices. An east Javanese sambal.
Sambal pencit/mangga muda
Green mango sambal from Central Java. Freshly ground sambal terasi with shredded young unripe mango. This is a good accompaniment to seafood. Pencit means young mango in Indonesian.
Sambal plecing
Originating from Lombok island, the sambal consists of Lombok's chili variety and Lombok's lengkare shrimp paste,tomatoes, salt, and lime juice.
Sambal rica rica
A hot sambal from Manado region, it uses ginger, chili, lemon and spices. Suitable for barbecue meats.
Sambal setan
A very hot sambal with Madame Jeanette peppers (red brownish, very sharp). The name literally means "devil's sauce". It is popular in Surabaya
Sambal taliwang
This variant is native to Taliwang, a village near Mataram, Lombok Island, and is made from naga jolokia pepper grown specially in Lombok, garlic and Lombok shrimp paste. A kilogram of naga jolokia pepper is extracted, ground and pressed. This is mixed with ground garlic and shrimp paste, then cooked with vegetable oil.
Sambal tauco
A Sulawesi sambal, contains the Chinese tauco, lime juice, chili, brown sugar, and salt.

Sambal terasi
A common Indonesian style of sambal. Similar to the Malaysian belacan, but with a stronger flavor since terasi, is more tangy and fermented. Red and green peppers, terasi, sugar, salt, lemon or lime juice (tangy, strong). One version omits the lime juice and has the sambal fried with pounded tomatoes. Popularly eaten raw. Alternate spelling in the Netherlands: trassi or trassie.
Sambal teri lado
a Padang, (West Sumatra) speciality, sambal is made using chili pepper, tomato, shallot, spices, and mixed with salted ikan teri (anchovy). The sambal is stir fried and similar to Malay "sambal ikan".
Sambal tomat
Similar to sambal tumis but with the addition of crushed tomato and sugar. The tomato is stir fried along with the other ingredients until a paste like consistency. The overall taste is hot and sweet, it is a good mix with lalapan. For very young children, sambal tomat sometimes use very little or no chili at all, it is regarded as one of the first steps in introducing children to the taste of Indonesian sambal.
Sambal Tuktuk
is a Batak andaliman (Sichuan pepper) and aso-aso fish (dried and preserved mackerel) sambal from North Sumatra.
Sambal tumis
Chili fried with belacan shrimp paste, onions, garlic, tamarind juice. Tumis means "stir fry". Often the cooking oil is re-mixed with the sambal. It may be mixed with other ingredients to produce dishes such as sambal kangkong, sambal cumi (squid) and sambal telur (egg).
Sambal udang bawang
a specialty sambal from Surabaya. It is one of Indonesia's super hot sambal. It used simple ingredients, such as chili pepper, shallot, garlic, asam jawa and coconut oil. People of Surabaya often called it Njaluk Sambal, as they eat it with fragrant steamed white rice.
Sambal ulek (oelek)
Raw chili paste (bright red, thin and sharp tasting). Can be used as the base for making other sambals or as an ingredient for other cuisines. Some types of this variant call for the addition of salt or lime into the red mixture. Oelek is a Dutch spelling which in modern Indonesian spelling has become simply ulek; both have the same pronunciation. Ulek is Indonesian special stoneware derived from common village basalt stone kitchenware still ubiquitous in kitchens, particularly in Java. The Ulekan is a mortar shaped like a hybrid of a dinner and soup-plate with an old, cured bamboo root or stone pestle (ulek-ulek) employed in an ulek manner: a crushing and twisting motion (like using a screwdriver) for crushing lime leaves, chilies, peppers, shallots, peanuts, and other kinds of ingredients.
Sambal Stroberi
A sambal made with strawberries originated from Bandung, West Java.


== Malaysian sambal ==
Sambal belacan
A Malay style sambal. Fresh chilis are pounded together with toasted shrimp paste (belacan) in a stone mortar to which sugar and lime juice are added. Originally, limau kesturi or calamansi lime, is used but since this is scarce outside of Southeast Asia, normal lime is used as a replacement. Tomatoes are optional ingredients. Sometimes, sweet sour mangoes or equivalent local fruits are also used. It can be eaten with cucumbers or ulam (leafy herbs) in a meal of rice and other dishes. A Malaysian-Chinese version is to fry belacan with chili.
Sambal jeruk
Green or red pepper with kaffir lime. In Malaysia, it is called cili (chili) jeruk (pickle). Sometimes vinegar and sugar are substituted for the lime. Used as a condiment with fried rice and noodle based dishes.

Sambal tempoyak
This sambal exists in two varieties: raw and cooked. Raw sambal tempoyak is prepared from fresh chilis pounded together with dried anchovies and served with fermented durian (tempoyak). The sambal and the tempoyak may be readily mixed or served separately, so that the person eating can determine the ratio of sambal to tempoyak that they want (tempoyak has a sweet-sour taste that offsets the hotness of the chili). In the cooked variety, pounded chilis, shallots and lemongrass are stir-fried with anchovies, tempoyak and turmeric leaf (for aroma). Sambal tempoyak could be found also at Sumatra. Petai (Parkia speciosa) and tapioca shoots are also frequently added.


== Sri Lankan sambal ==
Sini sambal
This is a sambal of the Sri Lankan cuisine that includes onion, crumbled Maldive fish, and spices as its main ingredients. Its name, also spelled as "sini sambol" and "seeni sambal", is derived from the local word for "sugar".
Pol sambal/Thengkai sambal
This is a sambal made of scraped coconut (pol and thengkai mean coconut in Sinhala and Tamil, respectively), onion, green chili, red chili powder, and lime juice as its main ingredients. Sometimes, crumbled Maldive fish is also added, and tomatoes can be used instead of lime juice for flavor.
Lunu miris
The name "lunu miris" can be literally translated as "salt chili" and is a paste of red chili pounded with sea salt. A widespread derivative is katta sambal, which adds onions, crumbled Maldive fish, salt, and lime juice to the chili-and-salt mixture.
Vaalai kai sambal
This is a sambal made of boiled and mashed plantain, scraped coconut, chopped green chilies and onion, salt and lime juice. Vaalai kai means unripe plantain in Sri Lankan Tamil.


== Dishes ==

Sambal can also be used as an ingredient to a dish, which uses a large amount of chili peppers. In Padang cuisine, any dishes ended with -balado (lit: with chili pepper) indicate the sambal-mixed dish. Dishes bearing the word sambal include:
Sambal sotong
(with cuttlefish)
Sambal udang kering
(with dried prawns), also known in Penang as "Sambal Hae Bee"
Sambal lengkong
(with ikan parang/wolf herring).
Sambal goreng teri kacang
(with anchovy and peanuts)
Sambal goreng kering tempe
(with tempeh)
Sambal goreng ati
(with cow's liver, potato, and sometimes petai)
Sambal goreng udang
(with fresh shrimp), also known as udang balado.
Sambal radio
a traditional dish from Sarawak, it is an omelette mixed with fried belacan and anchovies.
Sambal ikan
a Malay-style dish prepared from fish and spices and cooked until the fish loses its shape. Available in varieties, some are in the shape of dry fish floss known as serunding ikan, and some are moist such as sambal ikan bilis (anchovies) or sambal ikan tongkol (skipjack tuna).
Sambal daging/serunding daging
A Malay style sambal prepared from meat and spices and cooked for more than 4 hours until the meat loses its shape, similar to meat floss.


== See also ==

Indonesian cuisine
Malaysian cuisine
Nam phrik, the Thai equivalent of sambal
Singaporean cuisine
Peranakan cuisine
Philippine cuisine
Hot sauce, also known as chili sauce or pepper sauce.


== References ==