Stir fried water spinach is a common Asian vegetable dish of stir-fried water spinach (Ipomoea aquatica). It is a popular Asian vegetable dish, commonly found throughout East and Southeast Asia; from Sichuan and Cantonese cuisine in China, to Vietnamese, Malaysian, Singaporean and Indonesian cuisine. As a result, it is known by many names; such as tumis kangkung or cah kangkung in Indonesia, kangkong goreng in Malaysia, stir fry rau muống xào kiểu in Vietnam, stir fry kong xin cai (空心菜), stir fry tung choy or ong choy (通菜) in China. Kangkung (in Indonesia) and kangkong (in Malaysia) are water spinach. In Indonesian tumis kangkung means "stir-fried water spinach".


== Cooking method ==
Stir fried water spinach is one of the simplest, easiest, and also cheapest vegetable dishes in Asia, which contributes to its popularity. Water spinach thrives in the waterways, rivers, lakes and swamps of tropical Southeast Asia and Southern China. The garlic and shallots or onion are stir-fried in cooking oil, then the cleaned and cut water spinach are added, stir-fried in a wok on a strong fire with a small amount of cooking oil. The stir-frying lightly caramelizes the vegetables. The seasoning sauce is added according to each preference and recipe. Some might add slices of red hot chili pepper for spicy tanginess, while fresh or dried shrimp might be added for flavour. Other recipes might add diced tofu.


== Seasonings and variations ==
The stir-fried water spinach might vary according to its seasonings. A stir-fried water spinach could be lightly seasoned in garlic, black pepper, fish sauce, soy sauce, oyster sauce, or in spicy chili pepper, tauco (fermented soybean paste), shrimp paste or other sauce. The Vietnamese version seems to favour fish sauce for seasoning, while the Indonesian and Malaysian version seems to favour shrimp paste. The Southern Chinese recipe might favour oyster sauce or fermented tofu (腐乳) seasoning. In West Java, the Chinese Indonesian version however, favours the use of tauco fermented soybeans paste as seasoning.


== Kangkung belacan ==

A specific stir-fried water spinach seasoned with shrimp paste (belacan or terasi) is called kangkung belacan, kangkong belacan or cah kangkung terasi. In Malay kangkong belacan means water spinach in shrimp paste seasoning. It is a popular vegetable dish in Malaysia, Singapore and Indonesia.


== See also ==
Plecing kangkung
Mie kangkung


== References ==


== External links ==
Cantonese style stir fried water spinach recipe
Vietnamese simple stir fried water spinach recipe
Indonesian tumis kangkung recipe
Indonesian cah kangkung terasi recipe
Malaysian kangkong belacan recipe