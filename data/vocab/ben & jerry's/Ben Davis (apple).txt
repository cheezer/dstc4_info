The Ben Davis is an apple cultivar.


== History ==
During the 19th century and early 20th century it was a popular commercial apple due to the ruggedness and keeping qualities of the fruit, but as packing and transportation techniques improved the cultivar fell out of favor, replaced by others considered to have better flavor. It was known to fruit growers of the late 19th and early 20th centuries as a "mortgage lifter" because it was a reliable producer and the fruit would not drop from the trees until very late in the season. By mid-twentieth century it was mostly used as a process apple rather than a table apple, and orchards were replacing it with more popular varieties.
The cultivar is now very rare to nonexistent in the commercial trade. It is still grown in parts of California and Maine.


== Related apples ==
The 'Ben Davis' was crossbred with the 'McIntosh' to create the 'Cortland', which has been a very successful pie apple.
Similar cultivars known as 'Gano' or 'Black Ben Davis' appeared in parts of the American South (notably Arkansas and Virginia) in the 1880s. They are said to be either seedlings of, or identical to the original 'Ben Davis', but the exact relationship is unknown.


== References ==


== External links ==
Practically Edible Food Encyclopedia
Apple Journal