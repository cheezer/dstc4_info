Bencoolen Street (Chinese: 明古连街; pinyin: Mínggǔlián Jiē, Malay: Lebuh Bengkulu) is a street in Central, Singapore that starts at the junction of Rochor Road and Jalan Besar and ends at the junction of Fort Canning Road, Stamford Road and Orchard Road. The street houses several landmarks including Sim Lim Square, Bencoolen Mosque and Albert Complex. A number of hotels and serviced apartments exist, namely Summer View Hotel, Bayview Hotel Singapore, Hotel 81 Bencoolen, Strand Hotel, Hotel Rendezvous and Somerset Bencoolen.


== Etymology and history ==
Bencoolen Street was named in remembrance of Sir Stamford Raffles' position as Lieutenant Governor in Bencoolen (now Bengkulu), Sumatra, as well as that it was located in Kampong Bencoolen.
When Raffles came to the island in 1819, a number of Bencoolen Malays came as well and settled in the area where the street was. When the British gave up Bencoolen in 1824, after signing the Anglo-Dutch Treaty of 1824, the number of Bencoolen Malays grew. The majority of the oldest Singaporean Malays and Eurasians descended from the Bencoolen Malays.
Kampong Bencoolen covers the area of Bencoolen Street itself, Waterloo Street, Prinsep Street, Middle Road and Albert Street. Bencoolen Muslims built a mosque between 1825 and 1845 known as the Bencoolen Mosque. The first building was made from attap. In 1845, an Arab merchant known as Syed Omar bin Aljunied built the second building. Bencoolen Mosque was demolished in the early 2000s and made way for a new mosque. The mosque is currently owned by Majlis Ugama Islam Singapura (MUIS). Along with the new mosque, a service apartment building run by the Ascott Group exists. The graveyards of the Bencoolen Muslims were located where the current Istana is, facing Orchard Road.
The Chinese Singaporean in pre-war days sometimes referred to the street as chai tng au, meaning "behind the vegetarian hall", alluding to the meeting place of the Chinese vegetarian guild located in that area.


== References ==
Victor R Savage, Brenda S A Yeoh (2004), Toponymics A Study of Singapore Street Names, Eastern University Press, ISBN 981-210-364-3