Bengkulu City (Indonesian: Kota Bengkulu; English historic name: Bencoolen, Dutch historic: Benkulen or Benkoelen) is a city on the west coast of Sumatra in Indonesia. It had a population of 308,756 at the 2010 Census, the latest estimate (for January 2014) is 328,827. The city is the capital and largest city of Bengkulu Province.


== History ==

The British East India Company founded Bengkulu (named Bencoolen by the British), in 1685, as their new commercial center for the region. In the 1600s, the British East India Company controlled the spice trade in the Lampung region of southern Sumatra from a port in Banten, in the north west of the neighbouring island of Java. In 1682, a troop of the Dutch East India Company attacked Banten. The local crown prince submitted to the Dutch, who then recognized him as Sultan. The Dutch expelled all other Europeans present in Banten, leading the British to establish Bengkulu. In 1714, the British built Fort Marlborough at Bengkulu.
The trading center was never financially viable, because of its remoteness and the difficulty in procuring pepper. Despite these difficulties, the British persisted, maintaining a presence there for over a century, ceding it to the Dutch as part of the Anglo-Dutch Treaty of 1824 to focus their attention on Malacca. Edmund Roberts, the first U.S. envoy to the Far East, visited Bengkulu in 1832. Like the rest of present-day Indonesia, Bengkulu remained a Dutch colony until World War II.
Sukarno (later the first president of Indonesia) was imprisoned by the Dutch in the 1930s, including a brief period in Bengkulu. Sukarno met his future wife, Fatmawati, during his time in Bengkulu.


== Geography ==
The region is at low elevation and can have swamps. In the mid 19th century, malaria and related diseases were common.
Bengkulu lies near the Sunda Fault and is prone to earthquakes and tsunamis. In June 2000 a quake caused damage and the death of at least 100 people. A recent report predicts that Bengkulu is "at risk of inundation over the next few decades from undersea earthquakes predicted along the coast of Sumatra" A series of earthquakes struck Bengkulu during September, 2007, killing 13 people.


== Demographics ==
As of 1832, the population of Bengkulu, and its surrounding area, was estimated at 18,000 people. During that time, the region had a varied population: Dutch, Chinese, Javanese, Indians, and more. Chinatown was located in the center of the city. 


== Economy ==
When under Dutch rule, Bengkulu had plantations. Parsi people harvested and processed nutmeg and mace. The nutmeg would be processed into confectionaries. Pepper was a large export, too. The area also produced smaller amounts of coffee and rice, however, both were primarily imported from Padang. Fruit and animal labor was also significant.


== Culture ==
Each year, in the Muslim month of Muharram, Bengkulu hosts the ceremony Tabot. The two centuries old ritual was made by artisans from Madras in India for the construction of Fort Marlborough. It celebrates the martyrdom of Imam Shiite Hussein's death at the Battle of Karbala. The Tabot is an opportunity for a grand procession, accompanied by songs and dances performed by young girls.


== Governance ==
Historically, there was a court where all legal investigations passed through. Criminals who were sentenced to death had copies of their trials sent to Java for review.


== Education ==
As of 1832, the city had two Lancasterian method Dutch schools. At one school, students were taught maths, religion and the Malay language. The students frequently used a Malay version of The New Testament to learn Malayan, which was created by Robert Boyle when the British occupied Bengkulu. The other school was at an orphanage.
In this town lies the only state university in the province of Bengkulu, the Universitas Bengkulu (UNIB).


== See also ==
Selebar


== References ==


== Bibliography ==
Ricklefs, M. C., A History of Modern Indonesia since c. 1300 (2de édition), 1993