Governors, Deputy governors etc. of the Presidency and Residency versions of British Bencoolen.


== List ==
This is a list, source partially from worldstatesmen.org 


=== Deputy governors ===
Subordinated to Madras Presidency
1685 Ralph Ord
On 1 September 1712, Collett arrived at York Fort in Bencoolen in Sumatra and was subsequently appointed Governor.
1716 - 1717 Theophilus Shyllinge
1717 - 1718 Richard Farmer
1757 - 1758 Randolph Marriott
1758 - 1760 Roger Carter


=== Governors of Bencoolen Presidency ===
1760 - 1767 Roger Carter
1767 - 1776 Richard Wyatt
1776 Robert Hay
1776 - 1780 William Broff
1780 - 1781 Hew Steuart
1781 - 1785 Edward Coles


=== Governors-General ===
In 1785, Bencoolen became a Residency, within the Bengal Presidency
1817 - 1822 Stamford Raffles, who enacted major reforms, including the abolishment of slavery, as well as creating Singapore.


== References ==


== External links ==
http://www.worldstatesmen.org/Indonesia.htm#Bencoolen