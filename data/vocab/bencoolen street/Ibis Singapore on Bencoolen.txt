Ibis Singapore on Bencoolen is a 3 star hotel located on Bencoolen street within the Bugis area of Singapore. The hotel has a total of 538 rooms of which 3 are disabled friendly and 85% non smoking.


== History ==
The hotel was officially opened by Mr S Iswaran, Senior Minister of State for Trade and Industry of Singapore, on 26 February 2009.  This is a relatively new hotel and hasn't undergone any major changes since its opening. The hotel cost S$145 million project to build.


== Facilities ==
Like most 3 star hotel, this hotel has no swimming pool or gym. It has a restaurant name "it's all about taste" and a bar. It has facilities for the disabled including disabled friendly toilets, ramp and 3 room that are friendly to the handicap. In room facilities includes the safe box, small 1 door fridge, queen or single beds. 85% of the rooms are non smoking rooms.-


== Pay What You Want Campaign ==
As a part of its opening campaign, the hotel launched "Pay What You Want" campaign with Syndacast. Guests were able to log onto website to put in the price they want to pay for a limited time each day. "This campaign was, to certain extent, in response to the global financial crisis, but it is also the best way to measure what the market is willing to pay.  This is similar to Steve Hofstetter's campaign launched in December 2007.


== Notes ==