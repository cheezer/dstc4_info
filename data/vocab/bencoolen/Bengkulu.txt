Bengkulu is a province of Indonesia. Located in the southwest coast of Sumatra, It borders the provinces of West Sumatra, Jambi, South Sumatra and Lampung. The province also includes Enggano Island. The capital and largest city is Bengkulu city.
The city was formerly the site of a British garrison, which they called Bencoolen.


== History ==

The English East India Company established a pepper-trading center and garrison at Bengkulu (Bencoolen) in 1685. In 1714 the British built Fort Marlborough, which still stands. The trading post was never profitable for the British, being hampered by a location which Europeans found unpleasant, and by an inability to find sufficient pepper to buy. It became an occasional port of call for the EIC's East Indiamen.
Despite these difficulties, the British persisted, maintaining their presence for roughly 140 years before ceding it to the Dutch as part of the Anglo-Dutch Treaty of 1824 in exchange for Malacca. Bengkulu remained part of the Dutch East Indies until the Japanese occupation in World War 2.
During Sukarno's imprisonment by the Dutch in the early 1930s, the future first president of Indonesia lived briefly in Bengkulu. Here he met his wife, Fatmawati, who bore him several children, one of whom, Megawati Sukarnoputri, became Indonesia's first female President.
Bengkulu lies near the Sunda Fault and is prone to earthquakes and tsunamis. In June 2000, an earthquake killed at least 100 people. A recent report predicts that Bengkulu is "at risk of inundation over the next few decades from undersea earthquakes predicted along the coast of Sumatra" A series of earthquakes struck Bengkulu during September 2007, killing 13 people.


== Population ==
The 2010 census reported a population of 1,715,568 including 875,663 males and 837,730.; by January 2014 this had risen to 1,828,291.


== Administrative divisions ==
Bengkulu Province is subdivided into nine regencies and the independent city of Bengkulu, which lies outside any regency. The regencies and city are listed below with their populations at the 2010 Census and at the latest (January 2014) estimates.
* The area of Central Bengkulu Regency is included in the figure for North Bengkulu Regency, of which it was formerly part.


== Economy ==
Three active coal mining companies produce between 200,000 and 400,000 tons of coal per year, which is exported to Malaysia, Singapore, South Asia, and East Asia. Fishing, particularly tuna and mackerel, is an important activity. Agricultural products exported by the province include ginger, bamboo shoots, and rubber.


== Notes ==


== References ==
Reid, Anthony (ed.). 1995. Witnesses to Sumatra: A traveller's anthology. Kuala Lumpur: Oxford University Press. pp. 125–133.
Reprints of British-era primary source material

Wilkinson, R.J. 1938. Bencoolen. Journal of the Malayan Branch Royal Asiatic Society. 16(1): 127-133.
Overview of the British experience in Bencoolen