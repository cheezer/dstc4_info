Dahi chutney is strained yogurt that is mixed into a chutney of mint and onions. It is popular in south India. It is a side dish for the popular Hyderabadi biryani.


== Ingredients ==
The traditional ingredients are yogurt, onions, tomatoes, mint leaves, coriander and salt to taste.


== See also ==
List of yogurt-based dishes and beverages
Raita
Perugu Pachadi


== References ==