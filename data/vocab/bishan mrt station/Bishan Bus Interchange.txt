Bishan Bus Interchange is a bus interchange serving Bishan in Singapore. The interchange commenced passenger service as Bishan Bus Terminal in 1988, with Services 56 and 58. Located at Bishan Street 13, it is designed in a unique squarish configuration with the building wrapping around the bus parking area.
The Ministry of Education Language Centre (MOELC) lies across the field to the north. Passengers also able to transfer to the Circle Line from an entrance located right outside the interchange.


== History ==
Bishan Bus Interchange opened on 30 April 1989. Built at a cost of SGD$5.5 million, it replaced the temporary bus station that had been serving the new town of Bishan since December 1985. It was among the first batch of bus interchanges built to integrate with a Mass Rapid Transit (MRT) station. As such, passengers can easily transfer between bus services at this interchange and the Bishan MRT station.


== Design ==
The unusual design of Bishan Bus Interchange was even described in the The New Paper as "a piece of Disneyland in Bishan". The interchange features a castle-like structure wrapping around the bus parking area. Above the interchange, there is a cafeteria as well as a car park. A children's playground and an adjoining structure that houses a now-defunct fast food restaurant was also constructed together with this interchange.
Bishan Bus Interchange is physically linked to the neighbouring Junction 8 Shopping Centre and to Bishan MRT Station.


== Facilities ==
Bishan bus interchange used to feature a fast food restaurant, being the first bus interchange in Singapore to have this facility. It also features a children's playground, which was designed to complement the fast food restaurant to make the bus interchange family-friendly.
The bus interchange also houses a 560-lot car park above its premises.


== References ==


== External links ==
 Media related to Bishan Bus Interchange at Wikimedia Commons
Interchanges and Terminals (SBS Transit)