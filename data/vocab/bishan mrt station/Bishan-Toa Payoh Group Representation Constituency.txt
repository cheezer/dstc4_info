Bishan-Toa Payoh Group Representation Constituency (Traditional Chinese: 碧山-大巴窯集選區;Simplified Chinese: 碧山-大巴窑集选区) is a five-member Group Representation Constituency (GRC) located in the central region of Singapore. The GRC consists the entire areas of Bishan, the majority of Toa Payoh and parts of Thomson. Its sub-constituencies are Bishan East, Bishan-Toa Payoh North, Thomson, Toa Payoh Central and Toa Payoh East.
The GRC was formed in 1997 by merging the Bishan East, Bishan North, and Thomson divisions of the Thomson Group Representation Constituency with that of the Toa Payoh Group Representation Constituency. The remaining Serangoon Gardens Division was split between the Ang Mo Kio, Aljunied, Cheng San and Marine Parade GRCs.
Bishan-Toa Payoh GRC is currently headed by Minister for Defence Dr Ng Eng Hen after the retirement of former Deputy Prime Minister Wong Kan Seng from the Singapore Cabinet.
Both the Thomson and Toa Payoh GRCs were uncontested since the 1991 General Elections, with the Bishan-Toa Payoh GRC enjoying the same situation from 1997 until 2011, with the first contest is with Singapore People's Party. Its boundaries also remained intact throughout its existence.
The westernmost portion of Bishan-Toa Payoh GRC consists of a small part of the Central Catchment Nature Reserve.


== Members of Parliament ==


== Areas of responsibility ==
Bishan East: Bishan St 11-13, Pemimpin Estate
Bishan North: Bishan St 22-24, Bright Hill Dr, Sin Ming, Thomson Gardens, Shunfu HUDC
Thomson-TPY: Winsdor Park, Thomson Ridge, Lakeview, Shunfu, Toa Payoh North, Toa Payoh Lor 1/2, The Peak@TPY,Raffles Institution
Toa Payoh Central: TPY Lor 1,2,4, HDB Hub
Toa Payoh East:TPY Lor 5-8, Kim Keat Ave, TPY East Ind Estate


== Candidates and results ==


=== Elections in 2010s ===


=== Elections in 2000s ===


=== Elections in 1990s ===


== References ==
1997 General Elections Result
2001 General Elections Result
2006 General Elections Result
2011 General Elections Result