Bishan Stadium (Malay: Stadium Bishan; Chinese: 碧山體育場) is a multi-purpose stadium in Bishan, Singapore, with a capacity of 4,000 people. It is currently used mostly for football matches and is the home stadium of Home United FC in the S-League. The stadium was constructed in 1998 and is managed by the Singapore Sports Council. The public can use the facilities from 4:30 am to 8:30 pm daily unless it is exclusively booked for a sporting event. An indoor sports hall with facilities for badminton, table tennis and gymnastics lies adjacent to the stadium.


== Location ==
Situated in the central part of Singapore, Bishan Stadium is part of the Bishan Sports and Recreation Centre, which includes Bishan Sports Hall and Bishan Swimming Complex.


== History ==

Since its opening in 1998, Bishan Stadium has been the ground of Home United Football Club, a professional football club in Singapore. From 2004 to 2006, the Stadium was used for the team's home matches in the Asian Football Confederation Cup tournament.
In September 2006, Bishan Stadium was one of two venues for the Asian Football Confederation U-17 championship hosted by Singapore. The Australian national team also used the Stadium as its training base for two weeks in June 2007 before it left for the Asian Cup.
Bishan Stadium was used as the athletics venue of the 2010 Summer Youth Olympics.
The Stadium was also the venue for the 35th Singapore Junior Athletics Championships 2009, which was held in May of that year.


== See also ==
2010 Summer Youth Olympics
List of stadiums in Singapore


== References ==


== External links ==
Bishan Stadium (Facebook Page)