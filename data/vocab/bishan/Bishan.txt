Bishan may refer to:
Bishan, Singapore, neighborhood in Singapore's Central Region
Bishan Road, major road in Bishan, Singapore
Bishan MRT Station, Singapore Mass Rapid Transit interchange station along the North South Line and the Circle Line

Bishan District, district in the west of Chongqing, China
Bishan Railway Station, railway station of Chengyu Passenger Railway


== People ==
Bishan Singh Bedi (born 1946), Indian Test cricketer
Bishan Singh (1672–1699), ruler of Amber and head of the Kachwaha Rajput clan from 1688–99
Bishan Narayan Dar (1864–1916), Indian politician
Bishan Singh Ram Singh (1944–2006), Malaysian social activist and environmentalist


== See also ==
Bishan Daur, village in the Punjab province of Pakistan
Gishan, Hormozgan, also known as "Bishan Takht", village in Takht Rural District, Iran
All pages with titles containing "Bishan"