A black operation or black op is a covert operation by a government, a government agency, or a military organization. This can include activities by private companies or groups. Key features of a black operation are that it is clandestine and it is not attributable to the organization carrying it out. The main difference between a black operation and one that is merely clandestine is that a black operation involves a significant degree of deception, to conceal who is behind it or to make it appear that some other entity is responsible ("false flag" operations).
A single such activity may be called a "black bag operation"; that term is primarily used for covert or clandestine surreptitious entries into structures to obtain information for human intelligence operations. Such operations are known to have been carried out by the FBI, the Central Intelligence Agency, Mossad, MI6, Research and Analysis Wing, MSS, and the intelligence services of other nations.


== Etymology ==
"Black" may be used as a generic term for any government activity that is hidden or secret. For example, some activities by military and intel agencies are funded by a classified "black budget," of which the details, and sometimes even the total, are hidden from the public and from most congressional oversight.


== Reported examples of black operations ==
In 2007 the Central Intelligence Agency declassified secret records detailing illegal domestic surveillance, assassination plots, kidnapping, and infiltration and penetration of other "black" operations undertaken by the CIA from the 1950s to the early 1970s. CIA Director General Michael Hayden explained why he released the documents, saying that they provided a "glimpse of a very different time and a very different agency".
In May 2007 ABC News, and later the Daily Telegraph, reported that United States president George W. Bush had authorized the CIA to undertake "black operations" in Iran in order to promote regime change as well as to sabotage Iran's nuclear program. ABC News was subsequently criticized for reporting the secret operation, with 2008 presidential candidate Mitt Romney saying he was "shocked to see the ABC News report regarding covert action in Iran," but ABC said the CIA and the Bush Administration knew of their plans to publish the information and raised no objections.


== References ==


== External links ==
Operation Orchard