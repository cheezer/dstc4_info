Black Flag or black flag may refer to:


== Anarchism ==
Black Flag (newspaper), a publication in Britain
Chernoe Znamia (Чёрное знамя), an organisation in Russia
An element of anarchist symbolism


== Flags ==
The Black Flag of Islam, see Black Standard
Pan-African flag, a trans-national unity symbol
Ferraria crispa, a plant
A type of racing flag
A signal to parley
The Jolly Roger flag associated with piracy
One of various flags that are primarily black: list of black flags


== Music ==
Black Flag (band), a punk rock band
Black Flag (Ektomorf album), a 2012 album by Ektomorf
Black Flag (MGK mixtape), a MGK mixtape


== Other uses ==
Black Flag, Western Australia
Black Flag (insecticide), a brand of pesticide
Black Flag Army, a militia in Vietnam
False flag, a type of warfare
Assassin's Creed IV: Black Flag, 2013 videogame by Ubisoft