Black Hand may refer to:


== People ==
Black Hand (graffiti artist), an Iranian street artist


== Extortionists and underground groups ==
Black Hand (extortion), an extortion racket practised by the Camorra and Mafia members in Italy and the United States
Black Hand (Palestine) (al-Kaff al-Aswad), an Islamist militant group in the British Mandate of Palestine in the 1930s
Black Hand (Serbia) (Црна Рука), a secret society devoted to Serbian unification in 1910s
La Mano Negra ("The Black Hand"), a supposed secret and violent anarchist organization in Spain at the end of the 19th century


== Art, entertainment, and media ==


=== Comics ===
Black Hand Gang, a children's comics series by Hans Jürgen Press


=== Fictional entities ===
Black Hand (comics) a DC Comics supervillain
Black Hand (World of Darkness), fictional sect of vampires
Black Hand, a mercenary faction in Just Cause (video game)


=== Film ===
The Black Hand, a 1906 movie directed by Wallace McCutcheon, Sr.
Black Hand (1950 film), starring Gene Kelly
The Black Hand (1973 film), starring Lionel Stander


=== Music ===
Mano Negra, a French pop-fusion band


== See also ==
Black Band (disambiguation)