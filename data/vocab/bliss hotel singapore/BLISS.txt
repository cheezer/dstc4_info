Bliss may refer to:


== People ==
Aaron T. Bliss (1837–1906), U.S. Representative and Governor of Michigan
Arthur Bliss (1891–1975), British composer
Atlanta Bliss, American jazz musician who specializes in the trumpet
Baron Bliss (Henry Edward Ernest Victor Bliss, 1869–1926), traveller
Bliss Carman (1861–1929), Canadian poet
Brian Bliss (born 1965), retired American soccer defender and former coach of the Kansas City Wizards of Major League Soccer
C. D. Bliss (1870–1948), American football player and coach in the United States
Caroline Bliss (born 1961), British actress
Charles K. Bliss (1897–1985), inventor of Blissymbols
Chester Ittner Bliss (1899–1979), biologist known for his contributions to statistics.
Cornelius Newton Bliss (1833–1911), US merchant and politician
Daniel Bliss (1823–1916), founder of AUB, the American University of Beirut
Dave Bliss (born 1943), former American college basketball coach
Diana Bliss (1954–2012), Australian theatre producer, second wife of Alan Bond
Douglas Bliss (1900–1984), Scottish painter
Duane Leroy Bliss (1835–1910), American industrialist
Ed Bliss (1912-2002), American journalist
Edward Bliss (1865-1960), American missionary to China
Eleanor Albert Bliss (1899–1987), American bacteriologist
Frank Bliss (1852-1929), American baseball player
Franklyn Bliss Snyder (1884-1958), American educator and academic
Frederick J. Bliss (1857–1939), American archaeologist
George Bliss (Congressman) (1813–1868), US Congressman
Gilbert Ames Bliss (1876–1951), American mathematician
Harry Bliss, American cartoonist
Henry E. Bliss (1870–1955), American librarian and inventor of the Bliss classification
Henry H. Bliss (1830–1899), first person killed in a motor vehicle accident in the United States
Johnny Bliss (1922–1974), Australian rugby league footballer
Ian Bliss, Australian actor
Laurie Bliss (1872–1942), American football player and coach in the United States
Lillie P. Bliss (1864–1931), American art collector and patron, founder of Metropolitan Museum of Art
Lucille Bliss (born 1916), American actress and voice artist
Michael Bliss (born 1941), Canadian historian and award-winning author
Mike Bliss (born 1965), American NASCAR driver
Nathaniel Bliss (1700–1764), English astronomer
Philemon Bliss (1813–1889),US Congressman and jurist
Philip Bliss (1838–1876), American hymn lyricist and composer
Philip Bliss (academic) (1787–1857), Registrar of the University of Oxford, etc.
Ray C. Bliss (1907–1981), one of the important national U.S
Raymond W. Bliss (1888–1965), Surgeon General of the U.S. Army 1947–1951
Richard Bliss, American telecommunications technician arrested in Russia on charges of espionage.
Ryan Bliss (born 1971), US digital artist
Sister Bliss (born 1970), British keyboardist, record producer, DJ, composer and songwriter
Stephen Bliss (1787–1847), American minister and politician
Sylvester Bliss (1814–1863), Millerite minister and editor
Timothy Vivian Pelham Bliss (born 1940), British neuroscientist
Thomas Bliss (born 1952), motion picture producer and executive producer
Tasker H. Bliss (1853–1930), U.S. Army officer
Willard Bliss (1825-1889), American doctor
William Dwight Porter Bliss (1856–1926), American religious leader and activist
William Henry Bliss (1835–1911), English scholar
William Wallace Smith Bliss (1815–1853), U.S. Army Officer
Zenas Bliss (1835–1900), U.S. Army General and Medal of Honor recipient


== Music ==
Bliss (opera), 2010 opera based on Peter Carey's novel of the same name
Bliss (band), English pop group
Bliss (Danish band), Danish music band
Bliss n Eso, Australian hip hop band, named after MC Bliss (Jonathan Notley)
Bliss!, (1973) Chick Corea album, originally Pete La Roca Turkish Women at the Bath
Bliss (12 Rods album) (1993), by indie rock band 12 Rods
Bliss (Birdbrain album), 1995 debut album by post-grunge band Birdbrain
Bliss (Vanessa Paradis album) (2000), by French pop singer Vanessa Paradis
BLISS (Nikki Webster album) (2002), the second studio album by Nikki Webster
Bliss (Tone Damli album) (2005), Norwegian singer Tone Damli's first studio album
The Bliss Album…? (Vibrations of Love and Anger and the Ponderance of Life and Existence), 1993 album by P.M. Dawn
"Bliss", a 1980 song by New Zealand rock band Th' Dudes
"Bliss" (Tori Amos song), a 1999 song by Tori Amos
"Bliss" (Muse song), a 2001 song by the English rock band Muse
"Bliss" (Mariah Carey song), song on Mariah Carey's 1999 Rainbow album
"Bliss", song on trio Blaque's Blaque Out album
"Bliss (I Don't Wanna Know), ", song on Hinder's Extreme Behavior album
"Bliss", Paul Gilbert song from his Burning Organ album
"Bliss", a song by Delirious? from their album Mezzamorphis
"Bliss", a song by Still Remains, from the album Of Love and Lunacy
"Bliss", a song by Phish, from the album Billy Breathes
"Bliss", a song by Alice Peacock
"Bliss", an early name for the Grunge group Nirvana (band)


== Literature ==
Bliss (comics)
Bliss (comic), single-panel comic by Harry Bliss
Bliss (Marvel Comics), member of the Marvel Comics mutant group The Morlocks
Bliss (Starman), incubus who appeared in the DC Comics series Starman
Bliss (Wildstorm), Wildstorm comic book character

Bliss (magazine), UK teen magazine
Bliss (novel) (1981), by Peter Carey
Bliss, 2005 novel by Fiona Zedde
Bliss (short story) (1920), by Katherine Mansfield
Blissenobiarella, also known as Bliss, a main character in Asimov's Foundation series


== Film ==
Bliss (1917 film), starring Harold Lloyd
Bliss (1985 film), directed by Ray Lawrence, adapted from the Peter Carey novel
Bliss (1995 film), a television film
Bliss (1997 film), starring Terence Stamp, Sheryl Lee and Craig Sheffer
Bliss (2006 film), aka Fu Sheng, Chinese film produced by Fruit Chan
Bliss (2007 film), aka Mutluluk, directed by Abdullah Oguz, adapted from the Zülfü Livaneli novel
Bliss (2011 film), a biographical film about the early years of New Zealand author, Katherine Mansfield


== Television ==
Bliss (TV channel), British music television channel playing classic hits
Bliss (TV series), Canadian produced dramatic television series that ran from 2002 to 2004
"Bliss" (Star Trek: Voyager), episode from the fifth season of Star Trek: Voyager
Dee Bliss, fictional character on the Australian soap Neighbours
The Bliss, sort of "mind control" used by Anna, leader of the Visitors in the television show "V (2009 TV series)"


== Computer science ==
BLISS, system programming language developed at Carnegie Mellon University
Bliss (video game), 2005 adult computer game
Bliss (image), computer wallpaper included with Microsoft Windows XP
Bliss (virus), computer virus that infects GNU/Linux systems


== Places ==


=== United States ===
Bliss, California
Bliss, Kentucky
Bliss, Idaho
Bliss, New York
Bliss, Oklahoma
Bliss Corner, Massachusetts
Bliss Township, Michigan
Fort Bliss, Texas


=== Lebanon ===
Bliss Street, Beirut


== Other uses ==
Bliss (charity), special care baby charity
Bliss (crater), small lunar impact crater that is located just to the west of the dark-floored crater Plato
Bliss (spa), spa and retail product company
Bliss bibliographic classification, library classification
Blissymbols, an ideographic writing system
Bliss, bite-sized chocolates manufactured by The Hershey Company
Bliss (automobile), from the early 1900s
Chicago Bliss, a team in the Lingerie Football League
Bliss, a name for a type of synthetic cannabis


== See also ==
Ignorance is bliss (disambiguation)
BLIS (disambiguation)
All pages with titles containing "Bliss"
All pages beginning with "Bliss"