Ignorance Is Bliss is a phrase coined by Thomas Gray in his Ode on a Distant Prospect of Eton College.
It can also refer to:
Ignorance is Bliss (album), a 1999 album by Face to Face
Ignorance Is Bliss (House), a 2009 episode of House
"Ignorance is Bliss", a song by punk rock band Ramones, from their Brain Drain (album) (1989)
"Ignorance is Bliss", a song by San Francisco 90s rock band Jellyfish, from the compilation album Nintendo: White Knuckle Scorin' (1991)
"Ignorance is Bliss", a song by Swedish death metal band Defleshed, from their album "Reclaim the Beat" (2005)
"Ignorance is Bliss", a song by hip-hop artist Kendrick Lamar, from his album "Overly Dedicated" (2010)
Ignorance Is Bliss, a BBC radio adaptation of the American comedy quiz It Pays to Be Ignorant which ran on the Light Programme from 1946 until 1950


== Film and television ==
"Alice: Ignorance is Bliss", a short documentary following the relationship between an 84-year-old dementia sufferer and her grandson.


== See also ==
Ignorance (disambiguation)
Bliss (disambiguation)
Ignorance is strength, a phrase used in the novel Nineteen Eighty-Four