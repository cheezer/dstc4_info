Sunbreaker Cove is a summer village in Alberta, Canada. It is located on the northern shore of Sylvan Lake.


== Demographics ==
In the 2011 Census, the Summer Village of Sunbreaker Cove had a population of 69 living in 32 of its 209 total dwellings, a -49.6% change from its 2006 population of 137. With a land area of 0.49 km2 (0.19 sq mi), it had a population density of 140.8/km2 (364.7/sq mi) in 2011.
In 2006, Sunbreaker Cove had a population of 137 living in 216 dwellings, a 59.3% increase from 2001. The summer village has a land area of 0.49 km2 (0.19 sq mi) and a population density of 279.1/km2 (723/sq mi).


== See also ==
List of communities in Alberta
List of summer villages in Alberta


== References ==


== External links ==
Official website