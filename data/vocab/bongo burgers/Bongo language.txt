Bongo (Bungu), also known as Dor, is a Central Sudanic language spoken by the Bongo people in sparsely populated areas of Bahr al Ghazal in South Sudan.


== Numerals ==
Bongo has a quinary-vigesimal numeral system.


== Scholarship ==
The first ethnologists to work with the Bongo language were John Petherick, who published Bongo word lists in his 1861 work, Egypt, the Soudan, and Central Africa; Theodor von Heuglin, who also published Bongo wordlists in Reise in das Gebiet des Weissen Nil, &c. 1862-1864 in 1869; and Georg August Schweinfurth, who contributed sentences and vocabularies in his Linguistische Ergebnisse, Einer Reise Nach Centralafrika in 1873. E. E. Evans-Pritchard published additional Bongo wordl ists in 1937.
More recent scholarship has been done by Eileen Kilpatrick, who published a phonology of Bongo in 1985.


== References ==


== Further reading ==
A Small Comparative Vocabulary of Bongo Baka Yulu Kara Sodality of St Peter Claver, Rome, 1963.
A Reconstructed History of the Chari Languages - Bongo - Bagirmi - Sara. Segmental Phonology, with Evidence from Arabic Loanwords. Linda Thayer, University of Illinois at Urbana-Champaign, 1974. Typewritten thesis 309 pages. Copy held by J.A. Biddulph (Africanist publisher, Joseph Biddulph, Pontypridd, Wales).


== External links ==
Bongo at Gurtong
The Jesus Film in Bongo
Bongo at WALS Online