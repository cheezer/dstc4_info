The term bongo may refer to:


== Entertainment ==
Bongo (Australian TV series), on air from August to November 1960
Bongo Comics Group, a comic book publishing company
Bongo (Dragon Ball) or Krillin, a character in Dragon Ball media
Bongo (Indian TV series), an Indian television drama for children 2004
Bongo, a character in the Matt Groening comic strip Life in Hell
Bongo, a dog who played drums in the ITV children's series Animal Kwackers
Bongo submarine, a fictional vehicle in the film Star Wars Episode I: The Phantom Menace
Bongo, the cartoon ape bouncer from the 1988 film, Who Framed Roger Rabbit
"Little Bear Bongo", a 1930 short story for children by Sinclair Lewis
Bongo, a segment of the 1947 Disney film Fun and Fancy Free, adapted from the Lewis story


=== Music ===
Bongo drum, a percussion instrument made up of two small drums attached to each other
"Bongo Bongo Bongo I Don't Want to Leave the Congo", an alternative name for the 1947 song "Civilization"
"Bongo Bong", a single by Manu Chao from the 2000 album Clandestino
Bongo Fury, a Frank Zappa and Captain Beefheart album, 1975
Bongo flava, a Tanzanian hip hop genre
The Bongos, an America pop music band
Music Man Bongo, a model of bass guitar


== Places ==
Bongo Country, the name of several places in Africa
Bongo District, a district in the Upper East Region of Ghana
Bongo, Ghana, town, capital of Bongo District
Bongo (Ghana parliament constituency), in Bongo District

Bongo Gewog, village in Chukha District, Bhutan
Bongo (Grand-Bassam), Ivory Coast
Bongo Island, island in the Sulu Archipelago, Philippines
Bongo Massif, mountain range in north-eastern Central African Republic
Bôngo, alternative name for Bengal (eastern India and Bangladesh)
Mbongo, Angola, in Huambo Province, Angola, known as Bongo


== Sociology ==
Bongo Bongo Land, a derogatory reference to Third World countries, particularly those in Africa
Bongo-Bongo (linguistics), used as a name for an imaginary language
Bongo people (Gabon), a forest people
Bongo people (South Sudan), an ethnic group in Sudan
Bongo–Bagirmi languages, the major branch of the Central Sudanic language family
Bongo–Baka languages
Bongo language, the language of the Bongo people of Sudan


== Other uses ==
Bongo (antelope), a species of forest antelope from Africa
Bongo (name)
Bongo Jeans/Bongo Apparel, a clothing brand that was acquired by the Iconix Brand Group in 1998
Bongo (software), an open source mail and calendar project
A tree, Cavanillesia platanifolia
DK Bongos, a video game controller
Kia Bongo, an automobile
Mazda Bongo, a van
Operation Bongo II, 1964 Oklahoma City sonic boom tests
Um Bongo, a fruit drink marketed at children