Boon or Af-Boon is a nearly extinct Cushitic language spoken by 59 people (as of 2000) in Jilib District, Middle Jubba Region, Somalia. In recent decades they have shifted to the Maay dialect of Jilib. All speakers are older than 60.
Ethnologue and Glottolog leave it unclassified within Eastern Cushitic. Blench (2006) leaves it as unclassified within Cushitic, as records are too fragmentary to allow more than that.


== Notes ==


== References ==