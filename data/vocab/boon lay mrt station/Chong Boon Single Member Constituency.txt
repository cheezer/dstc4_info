Chong Boon Single Member Constituency (Simplified Chinese: 崇文单选区;Traditional Chinese: 崇文單選區) is a defunct single member constituency in Ang Mo Kio, Singapore. It was carved from Ang Mo Kio division prior to 1980 elections til 1988 elections where it was absorbed into Cheng San Group Representation Constituency.


== Members of Parliament ==
Sitaram Chandra Dias (1980 - 1988)


== Candidates and results ==


=== Elections in 1980s ===


== See also ==
Cheng San GRC


== References ==
1984 GE's result
1980 GE's result