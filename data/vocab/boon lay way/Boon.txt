Boon may refer to:


== Places ==
Boon, Ontario, Canada
Boon Township, Warrick County, Indiana, United States
Boon Island, Maine, United States
Boon Township, Michigan, United States
Boon Lake Township, Minnesota, United States
Boon Lay, Singapore
Boon Tat Street, Singapore
Boon Keng MRT Station, Singapore
Boones Mill, Virginia, United States (The city was originally known as Boon Mill).


== People ==


=== Surname ===
Clint Boon (born 1959), English musician and member of indie rock group the Inspiral Carpets
D. Boon (1958–1985), American punk guitarist and singer of The Minutemen
Dany Boon stage name of Daniel Hamidou (born 1966), French comedian
David Boon (born 1960), Tasmanian cricketer
Ed Boon (born 1964), American video game programmer; co-creator of Mortal Kombat
Emilie Boon, Dutch-born American children's book author and illustrator
Gerrit Boon (1768–1821), Dutch agent of the Holland Land Company, settler of Boonville, New York
Jaak Boon (born 1948), Belgian television writer
Jan Boon (1911–1974), Dutch writer, journalist, and activist
Louis Paul Boon (1912–1979), Flemish novelist
Ronnie Boon (1909–1998), Welsh rugby player
Thierry Boon (born 1944), Belgian immunologist


=== Given name ===
Lim Boon Keng (1869–1957), Chinese doctor, social and educational reformer in China and Singapore
Boon Gould, born Rowland Charles Gould (1955), English lead guitarist of Level 42


== Other uses ==
Boon (novel), a 1915 satirical work by H. G. Wells
Boon language, a nearly extinct East Cushitic language
Boon (TV series), a British television series starring Michael Elphick
Boon Brewery, a Belgian brewery
Daihatsu Boon, a subcompact car
Marukibi Boon, an Osamu Tezuka stock character
Boon, a character in Black Water, the fifth book in The Pendragon Adventure series by D. J. MacHale


== See also ==
Boone (disambiguation)