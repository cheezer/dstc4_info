The Royal Botanic Garden Edinburgh (RBGE) is a scientific centre for the study of plants, their diversity and conservation, as well as a popular tourist attraction. Originally founded in 1670 as a physic garden to grow medicinal plants, today it occupies four sites across Scotland — Edinburgh, Dawyck, Logan and Benmore — each with its own specialist collection. The RBGE's living collection consists of more than 13,302 plant species, (34,422 accessions) whilst the herbarium contains in excess of 3 million preserved specimens.
The Royal Botanic Garden Edinburgh is an executive non-departmental public body of the Scottish Government. The Edinburgh site is the main garden and the headquarters of the public body, which is led by Regius Keeper, Simon Milne.


== History ==
The Edinburgh botanic garden was founded in 1670 at St. Anne's Yard, near Holyrood Palace, by Dr. Robert Sibbald and Dr. Andrew Balfour. It is the second oldest botanic garden in Britain after Oxford's. The plant collection used as the basis of the garden was the private collection of Sir Patrick Murray, 2nd Lord Elibank, moved from his home at Livingston Peel in 1672 following his death in September 1671 The original site was "obtained of John Brown, gardener of the North Yardes in the Holyrood Abby, ane inclosure of some 40 foot of measure every way. By what we procured frorn Levingstone and other gardens, we made a collection of eight or nyne hundred plants yr." This site proved too small, and in 1676 grounds belonging to Trinity Hospital were leased by Balfour from the City Council: this second garden was sited just to the east of the Nor Loch, down from the High Street. John Ainslie's 1804 map shows it as the "Old Physick Garden" to the east of the North Bridge. The site was subsequently occupied by tracks of the North British Railway, and a plaque at platform 11 of the Waverley railway station marks its location.
In 1763, the garden's collections were moved away from the city's pollution to a larger "Physick Garden" on the west side of Leith Walk, as shown in Ainslie's 1804 map.
In the early 1820s under the direction of the botanist Daniel Ellis and several others, the garden moved west to its present location as the "New Botanic Garden" adjacent to Inverleith Row, and the Leith Walk site was built over as Gayfield Square and surrounding development. The Temperate Palm House, which remains the tallest in Britain to the present day, was built in 1858. A small section of the Leith Walk garden and planting still exists in the gardens in Hopetoun Crescent.
In 1877 the City acquired Inverleith House from the Fettes Trust and added it to the existing gardens, opening the remodelled grounds to the public in 1881.
The botanic garden at Benmore became the first Regional Garden of the RBGE in 1929. It was followed by the gardens at Logan and Dawyck in 1969 and 1978.


== The garden at Edinburgh ==
The Botanic Garden's main site in Edinburgh is a hugely important player in a worldwide network of institutions seeking to ensure that biodiversity is not further eroded. Located one mile from the city centre it covers 70 acres (28 ha).
The RBGE is actively involved in, and coordinates numerous in situ and ex situ conservation projects both in the UK and internationally. The three main cross-cutting themes of scientific work at the RBGE are: Scottish Biodiversity, Plants & Climate Change, and Conservation.

In addition to the RBGE's scientific activities the garden remains a popular destination for both tourists and locals. Locally known as "The Botanics", the garden is a popular place to go for a walk, particularly with young families. Entrance to the botanic garden is free, although a small entry charge exists for the glasshouses. During the year the garden hosts many events including live performances, guided tours and exhibitions. The RBGE is also an important centre for education, offering taught courses across all levels.
In 2009, the John Hope Gateway was opened. John Hope was the first Regius Keeper of RBGE.


=== Living collection ===
Nearly 273,000 individual plants are grown at the Botanics in Edinburgh or its three smaller satellite gardens (known as Regional Gardens) located in other parts of Scotland. These represent around 13,300 different species from all over the world, or about 4% of all known plant species.
The RBGE Living Collection catalogue is available here and is updated nightly.
Some notable collections at the botanic garden Edinburgh include:
Alpine Plants
Chinese Hillside
Cryptogamic Garden
The Glasshouses
Palmhouse
Temperate Palms
Tropical Palms

Orchids and Cycads
Ferns and Fossils
Plants and people (including Giant Water Lily pond)
Temperate lands
Rainforest Riches
Arid Lands
Montane tropical house (including Carnivorous plants)
Wet Tropical House

Peat Walls
The Queen Mother's memorial garden.
Rock Garden
Scottish Heath Garden
Woodland Garden


=== Herbarium ===
The RBGE Herbarium (situated in a purpose built facility at the Edinburgh site) is considered a world-leading botanical collection, housing in excess of 3 million specimens. Prior to the formation of the Herbarium, plant collections tended to be the private property of the Regius Keeper. The Herbarium in its present form came with the fusion of the collections of the University of Edinburgh and the Botanical Society of Edinburgh in 1839-40. RBGE's Herbarium moved into its present, purpose-built home in 1964.
Over the years, a large number of collections have been added, belonging to individuals such as R.K. Greville and John Hutton Balfour, and institutions including the Universities of Glasgow, St Andrews and Hull. The most important historical collection is that of George Walker Arnott, which came with the University of Glasgow's foreign herbarium deposited on permanent loan in 1965. This collection contains specimens from all the major mid-19th century collectors, especially from India, North and South America, and South Africa, including type material of species described by ‘Hooker & Arnott'. From the early 20th century, collections have been made by members of staff.
The approx 20% of the Herbarium that has been databased is searchable here.


=== Library ===
RBGE's Library is Scotland's national reference collection for specialist botanical and horticultural resources. Housing around 70,000 books and 150,000 periodicals the research library is one of the country's largest . It has been built up to support the specific subject fields researched and taught at RBGE - Garden staff and students are its main users, along with visiting researchers. However, as a national reference collection, the Library is also open to members of the public, either in person or by telephone or e-mail.


=== Inverleith House ===
Inverleith House is an 18th century building, located centrally in the modern botanic gardens. From 1960 to 1984 it was the original base of the Scottish National Gallery of Modern Art, with exhibits in the house and in the gardens, before it moved to larger premises in Belford Road. Since then, Inverleith House has functioned as a contemporary art gallery, showing a programme of temporary exhibitions by invited artists. Its spring programmes feature works and specimens from the historical collections of the Botanics, together with exhibitions by modern and contemporary artists. The gallery is curated by the Royal Botanic Garden Edinburgh.


== Regional specialist gardens ==


=== Benmore ===

Situated on the West Coast of Scotland, Benmore Botanic Garden experiences a wetter and milder oceanic climate than the main site in Edinburgh. Benmore grows trees and shrubs from high rainfall areas, especially conifers and rhododendrons. Highlights of the collection include an avenue of Sequoiadendron and a recently refurbished Fernery, exhibiting rare ferns from both Britain and abroad.


=== Dawyck ===

Situated to the south of the Scottish Border town of Peebles, Dawyck botanic garden is particularly suitable for hardy plants from the world's cooler, drier areas. Dawyck is also renowned for its high diversity of fungi and crytogamics.


=== Logan ===

Logan, Scotland's most exotic garden, has an almost sub-tropical climate, and provides ideal growing conditions for southern hemisphere plants.


== Royal Botanic Garden Edinburgh Medal ==
The Royal Botanic Garden Edinburgh Medal, instituted in 2010, is awarded from time to time to recognise an outstanding individual contribution in any field related to the work of the RBGE (either by a member of staff or by any other person). The medal, struck in silver, has a Sibbaldia motif on one face and a portrait of Robert Sibbald on the other.
2010: Edward Edmund Kemp
2011: Sir Tim Smit
2013: Vernon H. Heywood
2015: Peter H. Raven


== See also ==
Gardens in Scotland
List of botanical gardens in the United Kingdom


== References ==


== External links ==
Official website
Royal Botanic Garden Edinburgh Independent article with photographs