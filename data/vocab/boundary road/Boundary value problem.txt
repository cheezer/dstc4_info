In mathematics, in the field of differential equations, a boundary value problem is a differential equation together with a set of additional constraints, called the boundary conditions. A solution to a boundary value problem is a solution to the differential equation which also satisfies the boundary conditions.
Boundary value problems arise in several branches of physics as any physical differential equation will have them. Problems involving the wave equation, such as the determination of normal modes, are often stated as boundary value problems. A large class of important boundary value problems are the Sturm–Liouville problems. The analysis of these problems involves the eigenfunctions of a differential operator.
To be useful in applications, a boundary value problem should be well posed. This means that given the input to the problem there exists a unique solution, which depends continuously on the input. Much theoretical work in the field of partial differential equations is devoted to proving that boundary value problems arising from scientific and engineering applications are in fact well-posed.
Among the earliest boundary value problems to be studied is the Dirichlet problem, of finding the harmonic functions (solutions to Laplace's equation); the solution was given by the Dirichlet's principle.


== Explanation ==
Boundary value problem is similar to initial value problem. A boundary value problem has conditions specified at the extremes ("boundaries") of the independent variable in the equation whereas an initial value problem has all of the conditions specified at the same value of the independent variable (and that value is at the lower boundary of the domain, thus the term "initial" value).
For example, if the independent variable is time over the domain [0,1], a boundary value problem would specify values for  at both  and , whereas an initial value problem would specify a value of  and  at time .
Finding the temperature at all points of an iron bar with one end kept at absolute zero and the other end at the freezing point of water would be a boundary value problem.
If the problem is dependent on both space and time, one could specify the value of the problem at a given point for all time the data or at a given time for all space.
Concretely, an example of a boundary value (in one spatial dimension) is the problem

to be solved for the unknown function  with the boundary conditions

Without the boundary conditions, the general solution to this equation is

From the boundary condition  one obtains

which implies that  From the boundary condition  one finds

and so  One sees that imposing boundary conditions allowed one to determine a unique solution, which in this case is


== Types of boundary value problems ==

If the boundary gives a value to the normal derivative of the problem then it is a Neumann boundary condition. For example, if there is a heater at one end of an iron rod, then energy would be added at a constant rate but the actual temperature would not be known.
If the boundary gives a value to the problem then it is a Dirichlet boundary condition. For example, if one end of an iron rod is held at absolute zero, then the value of the problem would be known at that point in space.
If the boundary has the form of a curve or surface that gives a value to the normal derivative and the variable itself then it is a Cauchy boundary condition.
Aside from the boundary condition, boundary value problems are also classified according to the type of differential operator involved. For an elliptic operator, one discusses elliptic boundary value problems. For an hyperbolic operator, one discusses hyperbolic boundary value problems. These categories are further subdivided into linear and various nonlinear types.


== See also ==


== References ==
A. D. Polyanin and V. F. Zaitsev, Handbook of Exact Solutions for Ordinary Differential Equations (2nd edition), Chapman & Hall/CRC Press, Boca Raton, 2003. ISBN 1-58488-297-2.
A. D. Polyanin, Handbook of Linear Partial Differential Equations for Engineers and Scientists, Chapman & Hall/CRC Press, Boca Raton, 2002. ISBN 1-58488-299-9.


== External links ==
Hazewinkel, Michiel, ed. (2001), "Boundary value problems in potential theory", Encyclopedia of Mathematics, Springer, ISBN 978-1-55608-010-4 
Hazewinkel, Michiel, ed. (2001), "Boundary value problem, complex-variable methods", Encyclopedia of Mathematics, Springer, ISBN 978-1-55608-010-4 
Linear Partial Differential Equations: Exact Solutions and Boundary Value Problems at EqWorld: The World of Mathematical Equations.
Boundary value problem at Scholarpedia.