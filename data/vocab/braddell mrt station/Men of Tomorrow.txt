Men of Tomorrow (1932) is a British drama film, directed by Zoltan Korda and Leontine Sagan, and starring Maurice Braddell and Joan Gardner. This featured Robert Donat's movie debut.


== Plot ==
This is the story of an Oxford University student in the years after his graduation. Allen Shepherd (Braddell) has become a successful novelist and has married Jane Anderson (Gardner). A firm proponent of traditional sex roles, Shepherd leaves Jane when she accepts a teaching post at Oxford. He changes his views and the couple is reunited. Robert Donat and Merle Oberon were given top billing when Men of Tomorrow was distributed in the United States in 1935.


== Status ==
The film is currently missing from the BFI National Archive, and is listed as one of the British Film Institute's "75 Most Wanted" lost films.


== Cast ==
Maurice Braddell as Allan Shepherd
Joan Gardner as Jane Anderson
Emlyn Williams as Horners
Robert Donat as Julian Angell
Merle Oberon as Ysobel d'Aunay
John Traynor as Mr. Waters
Esther Kiss as Maggie
Annie Esmond as Mrs. Oliphant
Charles Carson as Senior Proctor


== References ==


== External links ==
BFI 75 Most Wanted entry, with extensive notes
Men of Tomorrow at the Internet Movie Database