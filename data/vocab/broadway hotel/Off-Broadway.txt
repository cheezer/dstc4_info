An Off-Broadway theatre is a professional venue in New York City with a seating capacity between 100 and 499. These theatres are smaller than Broadway theatres, but generally larger than Off-Off-Broadway theatres.
An "Off-Broadway production" is a production of a play, musical or revue that appears in such a venue and adheres to related trade union and other contracts.


== History ==
Originally referring to the location of a venue and its productions on a street intersecting Broadway in Manhattan's Theater District, the hub of the theatre industry in New York, the term later became defined by the League of Off-Broadway Theatres and Producers as a professional venue in New York City with a seating capacity between 100 and 499, or a specific production that appears in such a venue, and which adheres to related trade union and other contracts.
Previously, regardless of the size of the venue, a theatre was not considered Off-Broadway if it was within the "Broadway Box" (extending from 40th to 54th Street, and from west of Sixth Avenue to east of Eighth Avenue, and including Times Square and 42nd Street). The contractual definition changed this to encompass theatres meeting the standard, which is beneficial to these theatres because of the lower minimum required salary for Actors' Equity performers at Off-Broadway theatres as compared with the salary requirements of the union for Broadway theatres. Examples of Off-Broadway theatres within the Broadway Box are the Laura Pels Theatre and the Snapple Theater Center.
According to Bloom and Vlastnik, the Off-Broadway movement started in the 1950s, as a reaction to the "perceived commercialism of Broadway" and provided an "outlet for a new generation" of creative artists. This "fertile breeding ground, away from the pressures of commercial production and critical brickbats, helped give a leg up to hundreds of future Broadway greats. The first great Off-Broadway musical was the 1954 revival of the Brecht/Weill Threepenny Opera."
A number of Off-Broadway musicals have had subsequent runs on Broadway. These have included musicals such as Hair, Godspell, A Chorus Line, Little Shop of Horrors, Sunday in the Park with George, Rent, Grey Gardens, Urinetown, Avenue Q, The 25th Annual Putnam County Spelling Bee, Rock of Ages, In The Heights, Spring Awakening, Next to Normal and Hedwig and the Angry Inch. Plays that have moved to Broadway include Doubt, I Am My Own Wife, Bridge & Tunnel, The Normal Heart and Coastal Disturbances. Other productions, such as Stomp, Blue Man Group, Altar Boyz, Perfect Crime and Naked Boys Singing have run for several years Off-Broadway. The Fantasticks, the longest-running musical in theatre history, spent its original 42-year run Off-Broadway.


== Awards ==
Off-Broadway shows, performers, and creative staff are eligible for the following awards: the New York Drama Critics' Circle Award, the Outer Critics Circle Award, the Drama Desk Award, the Obie Award (presented since 1956 by The Village Voice), the Lucille Lortel Award (created in 1985 by the League of Off-Broadway Theatres & Producers), and the Drama League Award. Although Off-Broadway shows are not eligible for Tony Awards, an exception was made in 1956 (before the rules were changed), when Lotte Lenya won for "Best Performance by a Featured Actress in a Musical", for the Off-Broadway production of The Threepenny Opera.


== See also ==
Broadway theatre
Off-Off-Broadway


== References ==


== External links ==
Internet Off-Broadway Database
The League of Off-Broadway Theatres and Producers