German culture began long before the rise of Germany as a nation-state and spanned the entire German-speaking world. From its roots, culture in Germany has been shaped by major intellectual and popular currents in Europe, both religious and secular. Historically Germany has been called Das Land der Dichter und Denker (the country of poets and thinkers).
There are a number of public holidays in Germany. The country is particularly associated with its traditional Oktoberfest celebrations, its carnival culture and globally influential Christmas customs known as Weihnachten. 3 October has been the national day of Germany since 1990, celebrated as the German Unity Day (Tag der Deutschen Einheit). The UNESCO inscribed 38 properties in Germany on the World Heritage List.
Germany has been named the world's second most respected nation among 50 countries in 2013. A global opinion poll for the BBC revealed that Germany is recognised for having the most positive influence in the world in 2011, 2013 and 2014.


== Language ==

German is the official and predominant spoken language in Germany. It is one of 23 official languages in the European Union, and one of the three working languages of the European Commission, along with English and French. Recognised native minority languages in Germany are Danish, Sorbian, and Frisian. They are officially protected by the ECRML. The most used immigrant languages are Turkish, Kurdish, Polish, the Balkan languages, and Russian.

Standard German is a West Germanic language and is closely related to and classified alongside English, Dutch, and the Frisian languages. To a lesser extent, it is also related to the East (extinct) and North Germanic languages. Most German vocabulary is derived from the Germanic branch of the Indo-European language family. Significant minorities of words are derived from Latin and Greek, with a smaller amount from French and most recently English (known as Denglisch). German is written using the Latin alphabet. In addition to the 26 standard letters, German has three vowels with Umlaut, namely ä, ö, and ü, as well as the Eszett or scharfes S (sharp s) which is written "ß".
German dialects are distinguished from varieties of standard German. German dialects are traditional local varieties and are traced back to the different German tribes. Many of them are not easily understandable to a speaker of standard German, since they often differ in lexicon, phonology, and syntax.
Around the world, German has approximately 100 million native speakers and also about 80 million non-native speakers. German is the main language of about 90 million people (18%) in the EU. 67% of German citizens claim to be able to communicate in at least one foreign language, 27% in at least two languages other than their first.


== Literature ==

German literature can be traced back to the Middle Ages, with the most notable authors of the period being Walther von der Vogelweide and Wolfram von Eschenbach. The Nibelungenlied, whose author remains unknown, is also an important work of the epoch, as is the Thidrekssaga. The fairy tales collections collected and published by Jacob and Wilhelm Grimm in the 19th century became famous throughout the world.
Theologian Luther, who translated the Bible into German, is widely credited for having set the basis for the modern "High German" language. Among the most admired German poets and authors are Lessing, Goethe, Schiller, Kleist, Hoffmann, Brecht, Heine and Schmidt. Nine Germans have won the Nobel Prize in literature: Theodor Mommsen, Paul von Heyse, Gerhart Hauptmann, Thomas Mann, Nelly Sachs, Hermann Hesse, Heinrich Böll, Günter Grass, and Herta Müller.


== Philosophy ==

The rise of the modern natural sciences and the related decline of religion raised a series of questions, which recur throughout German philosophy, concerning the relationships between knowledge and faith, reason and emotion, and scientific, ethical, and artistic ways of seeing the world.
German philosophers have helped shape western philosophy from as early as the Middle Ages (Albertus Magnus). Later, Leibniz (17th century) and most importantly Kant played central roles in the history of philosophy. Kantianism inspired the work of Schopenhauer and Nietzsche as well as German idealism defended by Fichte and Hegel. Marx and Engels developed communist theory in the second half of the 19th century while Heidegger and Gadamer pursued the tradition of German philosophy in the 20th century. A number of German intellectuals were also influential in sociology, most notably Adorno, Elias, Habermas, Horkheimer, Luhmann, Marcuse, Simmel, Tönnies, and Weber. The University of Berlin founded in 1810 by linguist and philosopher Wilhelm von Humboldt served as an influential model for a number of modern western universities.
In the 21st century Germany has been an important country for the development of contemporary analytic philosophy in continental Europe, along with France, Austria, Switzerland and the Scandinavian countries.


== Music ==

In the field of music, Germany claims some of the most renowned classical composers of the world including Bach and Beethoven, who marked the transition between the Classical and Romantic eras in Western classical music. Other composers of the Austro-German tradition who achieved international fame include Brahms, Wagner, Haydn, Schubert, Händel, Schumann, Johannes Brahms, Mendelssohn Bartholdy, Johann Strauss II, Bruckner, Mahler, Telemann, Richard Strauss, Schoenberg, Orff, and most recently, Henze, Lachenmann, and Stockhausen.
Germany is the largest music market in Europe, and third largest in the world. It has exerted a strong influence on techno and rock music, and pioneered trance music. Artists such as Herbert Grönemeyer, Scorpions, Blind Guardian, Rammstein, Nena, Xavier Naidoo, Tokio Hotel and Modern Talking have enjoyed international fame. German musicians and, particularly, the pioneering bands Tangerine Dream and Kraftwerk have also contributed to the development of electronic music.

German popular music of the 20th and 21st century includes the movements of Neue Deutsche Welle (Nena, Alphaville), Ostrock (City, Keimzeit), Metal/Rock, Punk (Nina Hagen, Die Ärzte, Die Toten Hosen), Pop rock (Beatsteaks), Indie (Tocotronic, Blumfeld) and Hip Hop (Die Fantastischen Vier, Deichkind). A global trendsetter is the German Minimal and Techno scene (e.g. Ricardo Villalobos, Paul Kalkbrenner and Sven Vath).
Germany hosts many large rock music festivals annually. The Rock am Ring festival is the largest music festival in Germany, and among the largest in the world. German artists also make up a large percentage of Industrial music acts, which is called Neue Deutsche Härte. Germany hosts some of the largest Goth scenes and festivals in the entire world, with events like Wave-Gotik-Treffen and M'era Luna Festival easily attracting up to 30,000 people. In addition, the country hosts Wacken Open Air, the biggest heavy metal open air festival in the world.
Since about 1970, Germany has once again had a thriving popular culture, now increasingly being led by its new-old capital Berlin, and a self-confident music and art scene. Germany is also very well known for its many renowned opera houses, such as Semperoper, Komische Oper Berlin and Munich State Theatre. Richard Wagner established the Bayreuth Festspielhaus.
One of the most famous composers of the international film business is Hans Zimmer.


== Cinema ==

German cinema dates back to the very early years of the medium with the work of Max Skladanowsky. It was particularly influential during the years of the Weimar Republic with German expressionists such as Robert Wiene and Friedrich Wilhelm Murnau. Austrian-based director Fritz Lang, who became a German citizen in 1926 and whose career flourished in the pre-war German film industry, is said to have been a major influence on Hollywood cinema. His silent movie Metropolis (1927) is referred to as the birth of modern Science Fiction movies. Founded in 1912, the Babelsberg Film Studio is the oldest large-scale film studio in the world.
In 1930 Josef von Sternberg directed The Blue Angel, which was the first major German sound film and it brought world fame to actress Marlene Dietrich. Impressionist documentary Berlin: Symphony of a Great City, directed by Walter Ruttmann, is a prominent example of the city symphony genre. The Nazi era produced mostly propaganda films although the work of Leni Riefenstahl still introduced new aesthetics to film.

During the 1970s and 80s, New German Cinema directors such as Volker Schlöndorff, Werner Herzog, Wim Wenders, and Rainer Werner Fassbinder put West German cinema back on the international stage with their often provocative films. More recently, films such as Good Bye Lenin! (2003), Gegen die Wand (Head-on) (2004), Der Untergang (Downfall) (2004), and Der Baader Meinhof Komplex (2008) have enjoyed international success.
The Academy Award for Best Foreign Language Film went to the German production Die Blechtrommel (The Tin Drum) in 1979, to Nowhere in Africa in 2002, and to Das Leben der Anderen (The Lives of Others) in 2007. Among the most famous German actors are Marlene Dietrich, Klaus Kinski, Hanna Schygulla, Armin Mueller-Stahl, Jürgen Prochnow, Thomas Kretschmann, Til Schweiger and Christoph Waltz.
The Berlin Film Festival, held annually since 1951, is one of the world's foremost film festivals. An international jury places emphasis on representing films from all over the world and awards the winners with the Golden and Silver Bears. The annual European Film Awards ceremony is held every second year in the city of Berlin, where the European Film Academy (EFA) is located. The Babelsberg Studios in Potsdam are the oldest large-scale film studios in the world and a centre for international film production.


== Media ==

Germany's television market is the largest in Europe, with some 34 million TV households. The many regional and national public broadcasters are organised in line with the federal political structure. Around 90% of German households have cable or satellite TV, and viewers can choose from a variety of free-to-view public and commercial channels. Pay-TV services have not become popular or successful while public TV broadcasters ZDF and ARD offer a range of digital-only channels.
Germany is home to some of the world's largest media conglomerates, including Bertelsmann, the Axel Springer AG and ProSiebenSat.1 Media.
The German-speaking book publishers produce about 700 million copies of books every year, with about 80,000 titles, nearly 60,000 of them new publications. Germany is in third place on international statistics after the English-speaking book market and the People’s Republic of China. The Frankfurt Book Fair is considered to be the most important book fair in the world for international deals and trading and has a tradition that spans over 500 years.
Many of Europe's best-selling newspapers and magazines are produced in Germany. The papers with the highest circulation are Die Zeit, Süddeutsche Zeitung, Frankfurter Allgemeine Zeitung and Die Welt, the largest magazines include Der Spiegel, Stern and Focus. The Bild is a tabloid and has the largest circulation of all German papers.
The German video gaming market is one of the largest in the world. The Gamescom in Cologne is the world's leading gaming convention. Popular game series from Germany include Turrican, the Anno series, The Settlers series, the Gothic series, SpellForce, the X series, the FIFA Manager series, Far Cry and Crysis. The most relevant game developers and publishers are Blue Byte, Crytek, Deck13, Deep Silver, Egosoft, Kalypso Media, Koch Media, Piranha Bytes, Related Designs and Yager Development. Bigpoint, Gameforge, Goodgame and Wooga are leading developers of online and social games.


== Architecture ==

Architectural contributions from Germany include the Carolingian and Ottonian styles, important precursors of Romanesque. The region then produced significant works in styles such as the Gothic, Renaissance and Baroque.
The nation was particularly important in the early modern movement through the Deutscher Werkbund and the Bauhaus movement identified with Walter Gropius. The Nazis closed these movements and favoured a type of neo-classicism. Since World War II, further important modern and post-modern structures have been built, particularly since the reunification of Berlin.


== Art ==

German art has a long and distinguished tradition in the visual arts, from the earliest known work of figurative art to its current output of contemporary art.
Important German Renaissance painters include Albrecht Altdorfer, Lucas Cranach the Elder, Matthias Grünewald, Hans Holbein the Younger and the well-known Albrecht Dürer. The most important Baroque artists from Germany are Cosmas Damian Asam. Further artists are the painter Anselm Kiefer, romantic Caspar David Friedrich, the surrealist Max Ernst, the conceptualist Joseph Beuys, or Wolf Vostell or the neo-expressionist Georg Baselitz.


== Religion ==

64.1% of the German population belongs to Christian denominations: 31.4% are Roman Catholic, and 32.7% are affiliated with Protestantism  (the figures are known accurately because Germany imposes a church tax on those who disclose a religious affiliation).

The North and East are predominantly Protestant, the South and West predominantly Catholic. Nowadays there is a non-religious majority in Hamburg and the former East German states. Germany formed a substantial part of the Roman Catholic Holy Roman Empire, but was also the source of Protestant reformers such as Martin Luther.
Historically, Germany had a substantial Jewish population. Only a few thousand people of Jewish origin remained in Germany after the Holocaust, but the German Jewish community now has about 100,000 members, many from the former Soviet Union. Germany also has a substantial Muslim minority, most of whom are from Turkey.
German theologians include Luther, Melanchthon, Schleiermacher, Feuerbach, and Rudolf Otto. Also Germany was the origin of many mystics including Meister Eckhart, Rudolf Steiner, and Jakob Boehme; and of Pope Benedict XVI.


== Science ==

Germany has been the home of many famous inventors and engineers, such as Johannes Gutenberg, who is credited with the invention of movable type printing in Europe; Hans Geiger, the creator of the Geiger counter; and Konrad Zuse, who built the first computer. German inventors, engineers and industrialists such as Zeppelin, Daimler, Diesel, Otto, Wankel, Von Braun and Benz helped shape modern automotive and air transportation technology including the beginnings of space travel.
The work of Albert Einstein and Max Planck was crucial to the foundation of modern physics, which Werner Heisenberg and Erwin Schrödinger developed further. They were preceded by such key physicists as Hermann von Helmholtz, Joseph von Fraunhofer, and Gabriel Daniel Fahrenheit, among others. Wilhelm Conrad Röntgen discovered X-rays, an accomplishment that made him the first winner of the Nobel Prize in Physics in 1901. The Walhalla temple for "laudable and distinguished Germans", features a number of scientists, and is located east of Regensburg, in Bavaria.
Germany is home to some of the finest academic centers in Europe. Some famous Universities include those of both Munich and Berlin, University of Tübingen, University of Göttingen, University of Marburg, University of Berlin, Mining Academy Freiberg and Freiburg University, among many others. Moreover, the Ruprecht-Karls-Universität Heidelberg is one of the oldest universities in Europe.


== Fashion and design ==

German designers were leaders of modern product design, with the Bauhaus designers like Mies van der Rohe, and Dieter Rams of Braun being essential.
Germany is a leading country in the fashion industry. The German textile industry consisted of about 1,300 companies with more than 130,000 employees in 2010, which generated a revenue of 28 billion Euro. Almost 44 percent of the products are exported. The textile branch thus is the second largest producer of consumer goods after food production in the country. Berlin is the center of young and creative fashion in Germany, prominently displayed at Berlin Fashion Week (twice a year). It also hosts Europe's largest fashion trade fair called Bread & Butter.
Munich, Hamburg and Düsseldorf are also important design and production hubs of the German fashion industry, among smaller towns. Renowned fashion designers from Germany include Karl Lagerfeld, Jil Sander, Wolfgang Joop, Philipp Plein and Michael Michalsky. Important brands include Hugo Boss, Escada and Triumph, as well as special outfitters like Adidas, PUMA and Jack Wolfskin. The German supermodels Claudia Schiffer, Heidi Klum, Tatjana Patitz and Nadja Auermann came to global fame.


== Cuisine ==

German cuisine varies from region to region. The southern regions of Bavaria and Swabia, for instance, share a culinary culture with Switzerland and Austria. Pork, beef, and poultry are the main varieties of meat consumed in Germany; pork is the most popular. Throughout all regions, meat is often eaten in sausage form. More than 1500 different types of sausage are produced in Germany. Organic food has gained a market share of around 3.0%, and this is predicted to increase further.
A popular German saying has the meaning: "Breakfast like an emperor, lunch like a king, and dine like a beggar." Breakfast is usually a selection of breads and rolls with jam and honey or cold cuts and cheese, sometimes accompanied by a boiled egg. Cereals or muesli with milk or yoghurt is less common but widespread. More than 300 types of bread are sold in bakery shops across the country. Occasionally, more traditional and heartier Breakfasts, like the Bavarian "Brotzeit" with Weisswurst, Sweet Mustard and Wheat beer, or the Bauernfrühstück are also popular.

As a country with many immigrants, Germany has adopted many international dishes into its cuisine and daily eating habits. Italian dishes like pizza and pasta, Turkish and Arab dishes like döner kebab and falafel, are well established, especially in bigger cities. International burger chains, as well as Chinese and Greek restaurants, are widespread. Indian, Thai, Japanese, and other Asian cuisines have gained popularity in recent decades. Among high-profile restaurants in Germany, the Michelin guide has awarded nine restaurants three stars, the highest designation, while 15 more received two stars. German restaurants have become the world's second most decorated eateries after France.
Although German wine is becoming more popular in many parts of Germany, the national alcoholic drink is beer. German beer consumption per person is declining but—at 116 litres annually—it is still among the highest in the world. Beer varieties include Alt, Bock, Dunkel, Kölsch, Lager, Malzbier, Pils, and Weizenbier. Among 18 surveyed western countries, Germany ranked 14th in the list of per capita consumption of soft drinks in general, while it ranked third in the consumption of fruit juices. Furthermore, carbonated mineral water and Schorle (its mixture with fruit juice) are very popular in Germany.


== Sport ==

Sport forms an integral part of German life. Twenty-seven million Germans are members of a sports club and an additional twelve million pursue such an activity individually. Association football is the most popular sport. With more than 6.3 million official members, the German Football Association (Deutscher Fußball-Bund) is the largest sports organisation of its kind worldwide. The Bundesliga attracts the second highest average attendance of any professional sports league in the world. The German national football team won the FIFA World Cup in 1954, 1974, 1990 and 2014 and the UEFA European Football Championship in 1972, 1980 and 1996. Germany has hosted the FIFA World Cup in 1974 and 2006 and the UEFA European Football Championship in 1988. Among the most successful and renowned footballers are Franz Beckenbauer, Gerd Müller, Jürgen Klinsmann, Lothar Matthäus, and Oliver Kahn. Other popular spectator sports include handball, volleyball, basketball, ice hockey, and tennis.

Germany is one of the leading motorsports countries in the world. Race-winning cars, teams and drivers have come from Germany. The most successful Formula One driver in history, Michael Schumacher, has set many significant motorsport records during his career, having won more Formula One World Drivers' Championships and more Formula One races than any other driver since Formula One's debut season in 1950. He is one of the highest paid sportsmen in history and became a billionaire athlete. Constructors like BMW and Mercedes are among the leading manufacturers in motorsport. Additionally, Porsche has won the 24 Hours of Le Mans, a prestigious annual endurance race held in France, 16 times, and Audi has won it 9 times. The Deutsche Tourenwagen Masters is a popular series in Germany.
Historically, German sportsmen have been some of the most successful contenders in the Olympic Games, ranking third in an all-time Olympic Games medal count, combining East and West German medals. In the 2008 Summer Olympics, Germany finished fifth in the medal count, while in the 2006 Winter Olympics they finished first. Germany has hosted the Summer Olympic Games twice, in Berlin in 1936 and in Munich in 1972. The Winter Olympic Games took place in Germany once in 1936 when they were staged in the Bavarian twin towns of Garmisch and Partenkirchen.


== Society ==

Germany is a modern, advanced society, shaped by a plurality of lifestyles and regional identities. The country has established a high level of gender equality, promotes disability rights, and is legally and socially tolerant towards homosexuals. Gays and lesbians can legally adopt their partner's biological children, and civil unions have been permitted since 2001. The former Foreign minister Guido Westerwelle and the mayor of Berlin, Klaus Wowereit, are openly gay.
During the last decade of the 20th century, Germany changed its attitude towards immigrants. Until the mid-1990s the opinion was widespread that Germany is not a country of immigration, even though about 20% of the population were of non-German origin. Today the government and a majority of the German society are acknowledging that immigrants from diverse ethnocultural backgrounds are part of the German society and that controlled immigration should be initiated based on qualification standards.
Since the 2006 FIFA World Cup, the internal and external evaluation of Germany's national image has changed. In the annual Nation Brands Index global survey, Germany became significantly and repeatedly more highly ranked after the tournament. People in 20 different states assessed the country's reputation in terms of culture, politics, exports, its people and its attractiveness to tourists, immigrants and investments. Germany has been named the world's second most valued nation among 50 countries in 2010. Another global opinion poll, for the BBC, revealed that Germany is recognised for the most positive influence in the world in 2010. A majority of 59% have a positive view of the country, while 14% have a negative view.
With an expenditure of €67 billion on international travel in 2008, Germans spent more money on travel than any other country. The most visited destinations were Spain, Italy and Austria.


== Gallery ==


== See also ==

German Forest
Goethe-Institut
Prussian virtues
Public holidays in Germany
German folklore
Weihnachten
Oktoberfest
List of museums in Germany


== References ==


== External links ==
Facts about Germany: culture
German Missions in the United States: Culture, Sports & Events
Goethe Institute