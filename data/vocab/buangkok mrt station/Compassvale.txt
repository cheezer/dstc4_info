Compassvale is a neighbourhood of Sengkang New Town, and is located between Rivervale and Anchorvale. The house numbers of the public apartment blocks in Compassvale begin with the number '2' (2xx). Compassvale encompasses the Sengkang Town Centre and Buangkok.
The existing housing estates nearer to the Sengkang Town Centre were completed by the Housing and Development Board (HDB) in 1999 and 2000, while those in Buangkok, many of which are under Build-To-Order (HDB), are being developed or nearing completion.


== Housing estates ==


=== Public housing estates ===
Compassvale South Gate 康埔桦南门 - Block nos. 200-204D
Compassvale Place/Villa 康埔桦坊 - Block nos. 205A-206
Compassvale Crest 康埔桦巷 - Block nos. 207A-207D
Compassvale Vista 康埔桦景 - Block nos. 223A-226D
Compassvale Court 康埔桦阁 - Block nos. 227A-230
Compassvale Plains 康埔桦平原 - Block nos. 231A-241A
Compassvale Haven 康埔桦港 - Block nos. 245-256A
Compassvale Gardens 康埔桦园 - Block nos. 257-259C
Compassvale Street 康埔桦街 - Block nos. 260-263
Compassvale Lodge 康埔桦苑 - Block nos. 290-296C
Compassvale Green 康埔桦林 - Block nos. 297-299D


==== Build-To-Order (HDB) ====
Compassvale Arcadia, Compassvale Link - Block nos. 267A-B, 268A-D & 269A-D
Coris 1 & 2, Compassvale Bow & Compassvale Link - Block nos. 264A-F, 265A-E & 266A-C
Aspella, Compassvale Link - Block nos. 275A-D & 277A-D
Atrina, Sengkang Central & Compassvale Link - Block nos. 272A-D & 273A-D
Tivela, Sengkang Central - Block nos. 270A & 271A-C
Compassvale View, Sengkang East Avenue - Block nos. 208A-B & 209A-C
Compassvale Ancilla, Sengkang East Avenue - Block nos. 279 - 282


=== Private housing estates ===
Beauty Garden
Compass Heights 堪宝岭


== Educational institutions ==
Primary schools
Compassvale Primary School 康柏小学 Website
North Vista Primary School Website
Seng Kang Primary School 成康小学 Website
Secondary schools
Compassvale Secondary School 康柏中学 Website
Seng Kang Secondary School 成康中学 Website


== Places of worship ==
Chinese temples
Pu Ti Buddhist Temple
Singapore Buddhist Welfare Services Website
Muslims Mosque
Mawaddah Mosque


== Shopping amenities ==
Compass Point Shopping Centre


== Public Transport ==
Compassvale is well served by many bus services originating from the Sengkang Bus Interchange and from other parts of the island. Sengkang MRT Station and the east loop of the Sengkang LRT Line also serves the area.
Buses that ply along Compassvale:


== Gallery ==
Housing estates

Educational institutions

Sengkang Sculpture Park

Sengkang town centre