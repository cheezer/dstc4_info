The North East Line (NEL) is the third Mass Rapid Transit line in Singapore and the world's second longest fully underground, automated and driverless, rapid transit line after the Circle Line. The line is 20 km long with 16 stations. Travelling from one end of this line to the other end takes 33 minutes. This line is coloured purple in the rail map. 


== OverviewEdit ==

As the name implies, the line connects central Singapore to the north-eastern part of the island. Transfer to the North South Line is provided at Dhoby Ghaut, to the East West Line at Outram Park and to the Circle Line at Serangoon, Dhoby Ghaut and HarbourFront. The North East Line interchanges with the Downtown Line at Chinatown and will interchange with a future extension of the Downtown Line at Little India. The North East Line will interchange with the future Thomson-East Coast Line at Outram Park. This line was the first in Singapore to be entirely underground.
The North East Line is the first line in Singapore to have artwork integrated into all its 16 stations under the Art in Transit programme. Other than the Art in Transit programme, the interior architecture for the stations are typical of the design of the early 2000s with lots of white, glass and steel. This kind of design can be seen in other subway stations built during this period like the Shenzhen Metro and Hong Kong MTR's Tseung Kwan O line.
As of 2013, trains operate at two-minute intervals during peak hours, five-minute intervals during mid-day service and six-minute intervals in the early morning and night times.


== HistoryEdit ==
North East Line dates back to 1986, where the Communications Minister Yeo Ning Hong had announced that it would be "feasible to extend the MRT once it is 3 million". The line would have gone from Punggol and Seletar/Jalan Kayu to Outram Park.
In the initial stages of planning, Outram Park was initially planned to be the southern terminus of the North East Line. However, in 1993, the Land Transport Authority decided to extend the line southwards to HarbourFront, after noting that many people liked to go to the World Trade Centre (the present day HarbourFront Centre).
The Land Transport Authority received approval for the construction of the North East Line on 16 January 1996. French company Alstom was chosen as the main contractor and manager of the project. The operating license was given to new rail operator SBS Transit in order to foster competition with SMRT Trains. Construction began on 1 January 1997 and was completed on 20 January 2001, but all the stations had been completed with new signages.
The line was scheduled for completion in December 2002, but due to various problems in the automation, the opening was repeatedly delayed. It was finally opened on 20 June 2003, with higher fares than existing lines supposedly to compensate for the heavy construction costs (S$4.6 billion).
At the time that the line was opened, all of the stations were operating except Buangkok and Woodleigh. SBS Transit announced that these would open only when there was a critical mass of passengers in those areas. With respect to Buangkok, which had already been fully built, the company claimed that the projected number of passengers was too low to cover operating costs. Constant public pressure, notably the "White Elephant" incident and subsequent media attention, forced it to review the situation and the station duly opened on 15 January 2006. The last station on the line, Woodleigh, opened on 20 June 2011 in anticipation of an American International School which is under construction in the vicinity of the station that is slated to open in 2012.


== Punggol Downtown Extension (PGDe)Edit ==
On 17 January 2013, a 2-kilometre extension which will run from Punggol through Punggol North including the new Punggol Downtown was announced. It is expected to be completed by 2030.


== Busiest MRT StationEdit ==
Dhoby Ghaut was previously the busiest station on the line. But a week after VivoCity (a shopping mall) opened, HarbourFront's daily ridership doubled to 60,000 passengers a day, becoming the busiest MRT station on the North East Line.


== StationsEdit ==

The North East Line's numbering scheme reserves station code Keppel (NE2) between HarbourFront (NE1) and Outram Park (NE3) and will open in the 2030s.


== Rolling stockEdit ==

The rolling stock consists of Alstom Metropolis C751A trains, running in six-car formation. These trains operate from Sengkang Depot near Buangkok Station on the North East Line. The Sengkang Depot has maintenance and train overhaul facilities for trains along the North East Line. In 2010, the government announced that they would be looking to increase the number of trains on the North East Line by 70% within 5 to 6 years to cater to an expected increase in passenger traffic. The new trains will be called Alstom Metropolis C751C.
This is the only line which uses a pantograph system for collecting power from overhead; all other lines take power from a third rail.


== Train ControlEdit ==
The North-East Line is equipped with Alstom's Urbalis 300 signalling system with MASTRIA strategy which uses automatic moving-block technology with two-way digital transmission with Automatic Train Protection(ATP) to eliminate the risks of collisions and derailments. The Automatic Train Operation System (ATO) which drives the train and Train Data Management System (TDMS) to concentrate and dispatch the rolling stock information with fixed equipment. A Waveguide information network has the capability to transmit video and is almost maintenance-free. Base stations are located within the signalling equipment room. Automatic platform screen doors supplied by Westinghouse provide safety for passengers, offering protection from arriving and departing trains.


== Line disruptionsEdit ==
On 15 March 2012, more than 117,000 commuters were affected during a peak hour train breakdown between Dhoby Ghaut and HarbourFront, caused by faulty overhead power cables at the tunnel of the Outram Park that had snapped. A second problem was discovered relating to electricity insulation, delaying the service further. Train services resumed at 4.35 pm after nearly 10 hours of disruption making it the third breakdown in 4 months. Subsequently, the Land Transport Authority (LTA) intends to impose financial penalties amounting to S$400,000 on SBS Transit for the train service disruption along the North East Line on 15 March 2012.


== See alsoEdit ==
List of driverless trains


== External linksEdit ==
North East Line
North East Line (SBS Transit)


== ReferencesEdit ==