Kampong Buangkok (Chinese: 罗弄万国村; pinyin: Luónòng Wàn Guó Cūn) is a village found in mainland Singapore. Built in 1956, it was the last surviving kampong (village) located at Kampong Lorong Buangkok in Hougang, in the north eastern region of Singapore in the 21st century. A wide canal ran alongside the kampong, which links to Sungei Punggol that drains into the eastern Straits of Johor.


== Etymology ==
The kampong was also known as Selak Kain in Malay, which meant 'hitching up one's sarong (skirt)' as people hitch up their sarongs to wade through floods whenever the village experienced flash floods in the 20th century Singapore. The land which the kampong rests on, was acquired in 1956 from Mr Huang Yu Tu by Sng Teow Koon, a traditional Chinese medicine seller. At the point of purchase, there were already 4 to 6 houses built on the land. He set up home in the village with his family. He started renting out land to people to build homes. The land was handed down to his children, one of whom is Ms Sng Mui Hong. She continues to live in the village with her nieces. Her 3 other siblings, who are the co-owners, have all married and moved out of the village.
It evolved into a kampong. It was initially a swampy piece of land with only 5-6 homes. By the 1960s, it housed about 40 families. The land area used to be 21460 m2. The land area has shrunken in size to 12248.1 m2. Electricity, running water and garbage collection are provided by the government. Post is provided by a postman on a motorcycle once a day.


=== The Residents ===

In the 1960s, most residents worked in the nearby Woodbridge Hospital or factories. The children attended nearby schools such as Yio Chu Kang Primary School. Residents used to pay rental of $2–$3 then. They tended to rear their own chickens for food and generally led a carefree and slower pace of life as compared to their urban contemporaries.
Present day residents consist of make-up artistes, workers and mostly elderly residents. Currently it houses 28 families (18 Chinese and 10 Malay). They pay about $13 in rent. They continue to enjoy the slower pace of life that the kampong setting offers. "We leave our doors open. That is the kind of trust we have in one another."


== Floods ==
In the past, the kampong was constantly hit by floods. When heavy rains coincided with high tides, flash floods resulted in low lying areas. The water level in drains and canals became so high that the water could not be drained off quickly enough, causing floods to occur. In 1970, a canal was built at Gerald Dr in an attempt to minimise or absolve the floods. However, it proved to be insufficient in the face of mighty floods. Currently, the area is still affected by floods, the most recent being in 2006. A $10 million plan to improve the drainage system and to raise the ground level was aborted as it was said to be a cost ineffective project for just 28 families.


== Media and Awareness ==
The kampong served as the main subject of interest in the documentary Selak Kain The Last Kampong. The film received national air-time on free-to-air television channel Arts Central. The documentary focused on the residents' struggles to maintain their way of life in the kampong in the 21st century. The documentary juxtaposes modernities and nostalgic kampong life to bring out the dualities of living in a kampong in a cosmopolitan city like Singapore and how both young and old residents adapt to the changes both within, and without. The film was Ngee Ann Polytechnic's entry in the 2006 Project Pilot Showcase and also was Singapore's entry in India's Consortium for Educational Communication's Student Documentary Category.
The area was recently featured in a January 2009 New York Times article headlined "Singapore prepares to gobble up its last village" which claimed the Kampong was due to be demolished and redeveloped by the Singapore government in the near future.
In 2013, in the drama Beyond, some scenes in the "Kampong Minpi" world in episodes 10 and 11 was filmed here.


== References ==


== Further reading ==
"A place and lifestyle trapped in time". The Straits Times. 1999-01-19. 
"Alert: Flood could hit on March 21 to 23". The Straits Times. 2004-03-11. p. H1. 
"Floods fail to wash away kampung's charm". The Straits Times. 2004-03-11. p. H2. 
"No spare S10m? Get out the changkul.". The Straits Times. 2004-03-17. p. H2. 
"Relak! Flood-prone kampung Lorong Buangkok ....". The Straits Times. 2004-03-23. p. H1, H6, H10. 
"Volunteers help tackle kampung's flood woes". The Straits Times. 2004-04-19. 
"DPM Wong lauds 'kampung spirit' tack to boost bonding". The Straits Times. 2004-05-04. 
Kor, Kian Beng (2004-03-08). "We told you so". The New Paper. 
"Kampung calm". The New Paper. 2004-03-23. 
Sim, Chi Yin (2004-03-23). "Kampunk weekend". The New Paper. 
"Peace and quiet: Priceless". The New Paper. 2007-08-20. 


== External links ==
Kampong Buangkok Website - Greenridge Primary School
Virtual Reality Tour of Kampong Buangkok
An interview with a resident
Photo Gallery of KB
Compilation of information on KB
Project on KB by NTU final year students
Blog on KB