Bubble hair deformity is an abnormality of the hair shaft.
It is characterized by rows of bubbles seen microscopically within localized areas of brittle hair.


== See also ==
List of cutaneous conditions


== References ==