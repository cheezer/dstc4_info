The Local Bubble is a cavity in the interstellar medium (ISM) in the Orion Arm of the Milky Way. It contains, among others, the Local Interstellar Cloud, G-cloud, and the Solar System. It is at least 300 light years across and has a neutral-hydrogen density of about 0.05 atoms/cm3, or approximately one tenth of the average for the ISM in the Milky Way (0.5 atoms/cm3), and one sixth of the Local Interstellar Cloud (0.3 atoms/cm3). The hot diffuse gas in the Local Bubble emits X-rays.
The very sparse, hot gas of the Local Bubble is the result of supernovae that exploded within the past ten to twenty million years. It was once thought that the most likely candidate for the remains of this supernova was Geminga ("Gemini gamma-ray source"), a pulsar in the constellation Gemini. More recently, however, it has been suggested that multiple supernovae in subgroup B1 of the Pleiades moving group were more likely responsible, becoming a remnant supershell.


== Description ==

The Solar System has been traveling through the region currently occupied by the Local Bubble for the last five to ten million years. Its current location lies in the Local Interstellar Cloud (LIC), a minor region of denser material within the Bubble. The LIC formed where the Local Bubble and the Loop I Bubble met. The gas within the LIC has a density of approximately 0.1 atoms per cubic centimeter.
The Local Bubble is not spherical, but seems to be narrower in the galactic plane, becoming somewhat egg-shaped or elliptical, and may widen above and below the galactic plane, becoming shaped like an hourglass. It abuts other bubbles of less dense interstellar medium (ISM), including, in particular, the Loop I Bubble. The Loop I Bubble was created by supernovae and stellar winds in the Scorpius–Centaurus Association, some 500 light years from the Sun. The Loop I Bubble contains the star Antares (also known as Alpha Scorpii), as shown on the diagram above right. Several tunnels connect the cavities of the Local Bubble with the Loop I Bubble, called the "Lupus Tunnel". Other bubbles which are adjacent to the Local Bubble are the Loop II Bubble and the Loop III Bubble.


== Observation ==
Launched in February 2003 and active until April 2008, a small space observatory called Cosmic Hot Interstellar Plasma Spectrometer (CHIPS or CHIPSat) examined the hot gas within the Local Bubble. The Local Bubble was also the region of interest for the Extreme Ultraviolet Explorer mission (1992–2001), which examined hot EUV sources within the bubble. Sources beyond the edge of the bubble were identified, but attenuated by the denser interstellar medium.


== See also ==
Gould Belt
List of nearest stars and brown dwarfs
Orion–Eridanus Superbubble
Perseus Arm
Superbubble


== References ==


== Further reading ==
Anderson, Mark (6 January 2007). "Don't stop till you get to the Fluff". The New Scientist 193 (2585): 26–30. doi:10.1016/S0262-4079(07)60043-8. 
Lallement, R.; Welsh, B. Y.; Vergely, J. L.; Crifo, F.; Sfeir, D. (December 1, 2003). "3D mapping of the dense interstellar gas around the Local Bubble". Astronomy & Astrophysics 411 (3): 447–464. Bibcode:2003A&A...411..447L. doi:10.1051/0004-6361:20031214. 
"Near-Earth Supernovas". Science@NASA Headline News. NASA. January 6, 2003. 
"A Breeze from the Stars". Science@NASA Headline News. NASA. December 17, 2004. 


== External links ==
A 3D map of the Milky Way Galaxy and the Orion Arm