The life of Siddhartha Gautama, the Buddha, has been the subject of several films.


== History ==
The first known film about the life of Buddha was Buddhadev (English title: Lord Buddha) which was produced by the well-known Indian filmmaker Dadasaheb Phalke (1870–1944) in 1923. Two years later, another important Buddha film was released, The Light of Asia (Hindi title: Prem Sanyas). This movie was made by the German filmmaker Franz Osten (1875–1956). Himansu Rai (1892–1940) played the Buddha. Its title suggests that the script was based on the book The Light of Asia composed by the British poet Sir Edwin Arnold, which was issued by the Theosophical Society in 1891. In fact, its contents deviate deliberately from Arnold's book. The film was a greater success in Europe than in India. It gives a somewhat romantic picture of the life of Buddha. Buddhadev as well as The Light of Asia were silent films.
On March 20, 1952, a Japanese feature film representing the life of Buddha had its premiere, Dedication of the Great Buddha. Director Teinosuke Kinugasa (1896–1982) directed the picture under the Japanese film company Daiei Eiga. It was nominated for the 1953 Cannes Film Festival.
Another film about Buddha was a documentary film entitled Gotama the Buddha. It was released by the government of India in 1957 as part of the Buddha's 2500th birthday celebration. Rajbans Khanna acted as director and Bimal Roy as producer. It got an honorable mention at the Cannes film festival in 1957. It is a black-and-white film consisting of beautiful images of natural environments, archeological sites, reliefs and paintings, ancient ones from Ajanta as well as modern ones accompanied by a voice over relating the history of Buddha.
The film Angulimal (1960) was not directly based on the life of Buddha, but on the life of a dacoit and killer who used to loot and kill innocent people and cut off their fingers and who made a garland of such fingers to wear around his neck, thus he got the name Angulimal (Angluli: finger, mala: garland). The film depicts an incident where the dreaded dacoit once met the Buddha when Buddha was passing by a forest and goes ahead to kill him, but was corrected by the compassion of Buddha.
The fifth film about Buddha was a Japanese one, Shaka, produced by Kenji Misumi in 1961. It was shown in the USA in 1963 under the title Buddha. On February 13, 1964 a Korean film about the life of the Buddha had its premiere, Seokgamoni, the Korean translation of the Sanskrit Shakyamuni, which in Mahayana Buddhism is the term for the historical Buddha.
In 1997 the Indian producer G.A. Sheshagiri Rao made a Buddha film. It was simply entitled Buddha. This one did not roll in cinemas, but it was only sold on DVD. This one is also the longest movie about Buddha, as it consists of five DVDs with approximately 180 minutes film each.
In 2008, K. Raja Sekhar produced another Buddha film entitled Tathagata Buddha. The original film was in Telugu, but later it was dubbed in Hindi. This film relates Buddha's life story until its end, his parinirvana. The film is available on DVD.
Nepali director and actor Tulsi Ghimire is reported to have been working in an animation movie titled Buddha in 2013.
It is known that Buddhists in countries like Sri Lanka and Burma abhor the very idea of any human being impersonating the Buddha in a film. After its release in 1925 The Light of Asia was banned in Sri Lanka and the Malay States (contemporary West Malaysia).


== List of films on the life of Buddha ==


== See also ==
Depictions of Jesus in film
Depictions of Muhammad in film


== References ==
^ Aloysius Pieris, Love Meets Wisdom: A Christian Experience of Buddhism, Maryknoll, NY: Orbis Books 1990, p. 54, 125.
^ Rachel Dwyer, Filming the Gods: Religion and Indian Cinema, London et al.: Routledge, p. 28.