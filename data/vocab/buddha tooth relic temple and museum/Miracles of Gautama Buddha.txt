Gautama Buddha was alleged to possess superhuman powers and abilities; however, due to an understanding of the workings of the skeptical mind, he reportedly responded to a request for miracles by saying, "...I dislike, reject and despise them," and refused to comply. He allegedly attained his abilities through deep meditation during the time when he had renounced the world and lived as an ascetic. He supposedly performed such miracles to bring the most benefit to sentient beings and he warned that miraculous powers should not be the reason for practising his path.


== Birth ==
It is said that immediately after the birth of Siddhartha Gautama (623 BCE), he stood up, took seven steps north, and uttered:

"I am chief of the world,
Eldest am I in the world,
Foremost am I in the world.
This is the last birth.
There is now no more coming to be."

Furthermore, every place the baby Buddha placed his foot, a lotus flower bloomed. There is a claim that the Buddha's birth was miraculous via a dream that his mother saw of a white elephant.


== Twin miracle ==
After the Buddha returned to his father's kingdom, uncertainty still existed about whether Gautama Buddha was really enlightened or not. In response, the Buddha allegedly displayed the Yamaka-pātihāriya or the "Twin Miracle", called so because of its simultaneous production of apparently contradictory phenomena; in this case, fire and water.
The twin miracle entailed Gautama Buddha producing flames from the upper part of his body and streams of water from the lower part of his body, alternating this, and doing similarly between the left and right sides of his body.
Afterwards, the Buddha took three giant steps, arriving in Tavatimsa. There, he preached the Abhidharma to his mother who had been reborn there as a Deva named Santussita.


== Brahma ==
On one occasion, the Buddha allegedly went into a Brahma's world, and explained to the Brahma that all things are transient and temporary and devoid of independent existence. After being persuaded by the Buddha's words, the Brahma decided to follow The Buddha's Dharma.
The Brahma then requested a competition of powers between the two of them. Whenever the Brahma hid himself, the Buddha ended up pointing out where he was located. Then, the Buddha hid himself in voidness and meditation but the Brahma could not spot him. The Brahma's faith in the Buddha was increased.


== Taming the elephant ==
Devadatta was a cousin of the Buddha. Devadatta was tormented from early in his life by jealousy against his cousin. After scheming against Gautama to no avail, Devadatta set loose an elephant, known as Nalagiri or Dhanapala, to destroy the Buddha. One account is that as this elephant, who had been intoxicated into a crazed state by his keepers, ran through the town towards the Buddha, a frightened woman accidentally dropped her baby at the Buddha's feet. Just as the elephant was about to trample the child, The Buddha calmly reached up and touched the elephant on the forehead. The elephant became calm and quiet, then knelt down before the Buddha.


== The clean water ==
Gautama Buddha asked his disciple Ananda to get him some drinking water from a well. Ananda, however, repeatedly told the Buddha that the well was filled with grass and chaff, and thus not drinkable. Despite this, the Buddha continuously asked Ananda for the well's water; eventually, Ananda went to the well. As Ananda walked to the well, the Buddha expelled all the grass and chaff from the well which resulted in the water becoming radiant and clean.


== Power over nature ==
R.C. Amore recounts a miracle from the first chapter of Mahavagga (Book of the Discipline, IV) where the Buddha displayed his power over nature. When an area was inundated by a flood, he commanded the waters to stand back so that he could walk between them on dry ground.


== Miraculous Powers ==
The Majjhima Nikāya states that the Buddha had more superpowers than any other being including being able to walk on water which is further verified in the Aṅguttara Nikāya. The Buddha could multiply into a million and then return, he could travel through space, he could make himself as big as a giant and then as small as an ant, walk through mountains, he could dive in and out of the earth, he could travel to Heavens to school the Gods and return to earth.


== Other miracles ==
Other miracles and powers that Gautama Buddha is alleged to have possessed and exercised include Iddhi, Telepathy, super-hearing, divine seeing, and seeing past lives. These are described in the Mahasihanada Sutta and other suttas in the pali canon.


== See also ==
Iddhi
Siddhi
Abhijna
Vibhuti


== References ==
^ The Long Discourses of the Buddha, A Translation of the Dīgha Níkāya by Maurice Walshe, Wisdom Publication, Boston 1995, p. 176
^ a b "The Life of the Buddha: The Birth of the Prince". BuddhaNet. 
^ "UN-backed project to conserve Buddha’s birthplace in Nepal begins". UN News Center. 
^ "Lumbini, the Birthplace of the Lord Buddha". UNESCO. 
^ "The Buddha: Story & Teachings". PBS. 
^ Jesus' walking on the sea: an investigation of the origin of the narrative
^ Maha-sihanada Sutta


== External links ==
Goldfield, Ari etc. Do You Believe in Miracles? Fall 2008.
parami.org
palikanon.com
buddhanet.net
ukonline.co.uk
ignca.nic.in
bartleby.com
seasite.niu.edu
astraltraveler.com