This is a list of Buddhist temples, monasteries, stupas, and pagodas for which there are Wikipedia articles, sorted by location.


== Australia ==


=== Australian Capital Territory ===
Mahamevnawa Buddhist Monastery (Sri Lankan)


=== New South Wales ===
Nan Tien Temple
Sunnataram Forest Monastery


=== Queensland ===
Chung Tian Temple


=== South Australia ===
Pháp Hoa Temple


=== Victoria ===
Tara Institute


=== Western Australia ===
Bodhinyana Monastery


== Bangladesh ==
Buddha Dhatu Jadi
Somapura Mahavihara
Shita Coat Bihar


== Bhutan ==


=== Bumthang ===
Kurjey Lhakhang - one of Bhutan's most sacred temples - image of Guru Rinopche enshrined in rock.


=== Paro ===
Rinpung Dzong
Paro Taktsang (Tiger's Nest) - perched on a 1,200 meter cliff, this is one of Bhutan's most spectacular monasteries.


=== Punakha ===
Punakha Dzong - constructed by Zhabdrung Ngawang Namgyal in 1637-38 it is the head monastery of the Southern Drukpa Kagyu school.


=== Phobjika ===
Gangteng Monastery


=== Thimphu ===
Chagri Monastery


== Cambodia ==


=== Angkor ===
Angkor Wat
Bayon
Krol Ko
Neak Pean
Preah Khan
Preah Palilay
Ta Prohm
Ta Som


=== Kampong Thom ===
Prasat Kuh Nokor


=== Phnom Penh ===
Wat Botum
Wat Ounalom
Wat Phnom
Wat Preah Keo (Silver Pagoda)


=== Pursat ===
Wat Bakan


== Canada ==


=== British Columbia ===

International Buddhist Temple, Richmond
Ling Yen Mountain Temple
Thrangu Monastery (Richmond, British Columbia)


=== Nova Scotia ===
Gampo Abbey


=== Ontario ===
Fo Guang Shan Temple, Toronto Toronto Branch, Mississauga
Mahamevnawa Buddhist Monastery (Theravada)
Toronto Zen Centre
Zen Centre of Ottawa


=== Quebec ===
Montreal Zen Center
Chua Quan Am
Chua thuyen ton


== People's Republic of China ==


=== Anhui ===
Huacheng Temple


=== Beijing ===
Big Bell Temple (Juesheng Temple)
Fayuan Temple
Guanghua Temple
Guangji Temple
Jietai Temple
Miaoying Temple
Tanzhe Temple
Temple of Azure Clouds
Tianning Temple
Wanshou Temple
Yonghe Temple
Yunju Temple
Zhenjue Temple
Zhihua Temple


=== Fujian ===
Cao'an (originally, a Manichaean temple)
Guanghua Temple
Kaiyuan Temple
Wanfu Temple


=== Guangdong ===
Nanhua Temple
Temple of Bright Filial Piety (Guangxiao Si)
Temple of the Six Banyan Trees


=== Hainan ===
Nanshan Temple (Sanya)


=== Hebei ===
Kaishan Temple
Longxing Monastery
Puning Temple
Putuo Zongcheng Temple


=== Henan ===
Daxiangguo Temple
Iron Pagoda
Shaolin Monastery
Songyue Pagoda
White Horse Temple
Youguo Temple


=== Hohhot ===
Five Pagoda Temple


=== Hubei ===
Guiyuan Temple


=== Hong Kong ===
Chi Lin Nunnery
Kwan Kung Pavilion
Po Lin Monastery
Tsing Shan Monastery
Tsz Shan Monastery
Yuen Yuen Institute


=== Jiangsu ===
Hanshan Temple
Huqiu Tower
Jiming Temple
Linggu Temple
Qixia Temple
Tianning Temple (天宁宝塔), Changzhou, the tallest pagoda in the world. Height: 153.8 metres (505 ft).


=== Jiangxi ===
Donglin Temple


=== Ningxia ===
Baisigou Square Pagoda


=== Quanzhou ===
Kaiyuan Temple


=== Shaanxi ===
Famen Temple
Giant Wild Goose Pagoda
Small Wild Goose Pagoda
Xingjiao Temple


=== Shandong ===
Four Gates Pagoda
Lingyan Temple
Zhanshan Temple


=== Shanghai ===
Donglin Temple
Jade Buddha Temple
Jing'an Temple
Longhua Temple


=== Shanxi ===
Foguang Temple
Nanchan Temple
Pagoda of Fogong Temple


=== Sichuan ===
Bao'en Temple


=== Tibet Autonomous Region ===

Ganden Monastery
Jokhang
Katok Monastery
Khorzhak Monastery
Menri Monastery
Mindrolling Monastery
Nechung
Palpung Monastery
Palyul
Ralung Monastery
Sakya Monastery
Samding Monastery
Samye
Sera Monastery
Shechen Monastery
Simbiling Monastery
Surmang
Tashilhunpo Monastery
Tsi Nesar
Tsurphu Monastery
Yerpa


=== Yunnan ===
Three Pagodas
Yuantong Temple


=== Zhejiang ===
Baoguo Temple
Guoqing Temple
Jingci Temple
Lingyin Temple
Liuhe Pagoda
Puji Temple
Fayu Temple


== Europe ==


=== Denmark ===
Egely Monastery, Østermarie


=== England ===
Aruna Ratanagiri Buddhist Monastery (Harnham Buddhist Monastery), Northumberland (Thai)
Amaravati Buddhist Monastery, Hertfordshire (Thai)
Chithurst Buddhist Monastery (Cittaviveka), West Sussex (Thai)
Fo Guang Shan, Marylebone, London (Chinese)
Kadampa Buddhist Temple, Ulverston
Mahamevnawa Buddhist Monastery, Watford, London (Sri Lankan)
Nipponzan-Myōhōji Temple and Peace Pagoda at Willen, Milton Keynes (Japanese)
Padmaloka Buddhist Retreat Centre, Norfolk
Throssel Hole Buddhist Abbey, Northumberland
Wat Buddhapadipa, Wimbledon, London (Thai)
Wat Charoenbhavana, Manchester (Thai)


=== France ===
Village des Pruniers; Plum Village
Kagyu-Dzong, Paris
Vajradhara-Ling and Temple for Peace, Aubry-le-Panthou, Normandy
Lerab Ling, Montpellier
Pagode de Vincennes, Bois de Vincennes


=== Germany ===
Das Buddhistische Haus (engl.: the Buddhist house; oldest Buddhist temple in Europe)
Mahamevnawa Buddhist Monastery, (Theravada)


=== Hungary ===
Hungarian Shaolin Temple
Wonkwangsa International Zen Temple, Esztergom, Kwan Um School of Zen, Korean tradition


=== Italy ===
Istituto Lama Tzong Khapa
Santacittarama Buddhist Monastery, Poggio Nativa (Thai)


=== The Netherlands ===


=== Poland ===
Drophan Ling


=== Scotland ===
Kagyu Samyé Ling Monastery and Tibetan Centre


=== Slovenia ===
Buddhist Congregation Dharmaling


=== Spain ===
Benalmádena Stupa, Benalmádena


=== Sweden ===
Buddharama Temple


== India ==


=== Andhra Pradesh ===

Amaravati
Bavikonda
Bojjannakonda
Nagarjunakonda
Pavurallakonda
Ramatheertham
Salihundam
Thotlakonda


=== Karnataka ===
Namdroling Monastery


=== Arunachal Pradesh ===
Tawang Monastery


=== Bihar ===
Mahabodhi Temple Complex at Bodh Gaya
Nalanda
Rajgir


=== Goa ===

Buddhist caves exist in following places in Goa:
Arambol (Harahara)
Bandora (Bandivataka)
Kudne (Kundivataka)
Margao (Mathagrama)
Rivona (Rishivana)
Buddha idols have been found in several places, and some temples, some are still in worship and are considered now as Hindu gods. Monasteries used to exist in many places, and it can be seen from the names of the modern villages. For example, Viharas have been found in modern Divachali or ancient Dipakavishaya, Lamgaon or ancient Lamagrama and many other places.


=== Himachal Pradesh ===

Dhankar Gompa
Gandhola Monastery
Gemur Monastery
Gozzangwa Monastery
Kardang Monastery
Key Monastery
Kibber
Kungri Monastery
Lhalung Monastery
lahotsava lhakhang,nako temples,kinnaur
Namgyal Monastery
Rewalsar
Shashur Monastery
Tabo Monastery
Tangyud Monastery
Tayul Monastery


=== Jammu and Kashmir ===
Dzongkhul Monastery
Ladakh
Alchi Monastery
Bardan Monastery
Basgo Monastery
Chemrey Monastery
Diskit Monastery
Hanle Monastery
Hemis Monastery
Hundur Monastery
Korzok Monastery
Kursha Monastery
Lamayuru Monastery
Likir Monastery
Lingshed Monastery
Mashro Monastery
Matho Monastery
Mulbekh Monastery
Namgyal Tsemo Monastery
Phugtal Monastery
Phyang Monastery
Rangdum Monastery
Rizong Monastery
Sani Monastery
Sankar Monastery
Shey Monastery
Spituk Monastery
Stakna Monastery
Stok Monastery
Stongdey Monastery
Sumda Chun
Takthok Monastery
Thikse Monastery
Zangla Monastery


=== Madhya Pradesh ===

Deur Kothar
Dharmrajeshwar
Sanchi


=== Maharahstra ===

Ajanta Caves
Aurangabad Caves
Bedse Caves
Bhaja Caves
Deekshabhoomi
Ellora Caves
Ghorawadi Caves
Jogeshwari Caves
Kanheri Caves
Karla Caves
Mahakali Caves
Pandavleni Caves


=== Orissa ===

Brahmani temple
Dhauli
Lalitgiri
Marichi temple
Puspagiri Mahavihara
Ratnagiri (Orissa)
Udayagiri (Orissa)
Udayagiri and Khandagiri Caves


=== Sikkim ===

Dubdi Monastery
Enchey Monastery
Pemayangtse Monastery
Phensang Monastery
Phodang Monastery
Ralang Monastery
Rumtek Monastery
Tashiding Monastery
Tsuklakhang Palace


=== Tamil Nadu ===
Chudamani Vihara


=== Uttar Pradesh ===
Parinirvana Stupa : Death (Nirvana) place of Gautam Buddha.
Chaukhandi Stupa
Dhamek Stupa
Jetavana, Sravasti
Kushinagar
Sarnath
Varanasi


=== West Bengal ===
Darjeeling district
Bhutia Busty Monastery, Darjeeling
Ghum Monastery, Darjeeling
Mag-Dhog Yolmowa Monastery, Darjeeling
Tharpa Choling Monastery, Kalimpong
Zang Dhok Palri Phodang, Kalimpong


== Indonesia ==


=== Sumatra ===
Candi Bahal
Candi Muaro Jambi
Candi Muara Takus


=== West Java ===
Candi Batujaya


=== Central Java ===
Candi Borobudur
Candi Mendut
Candi Pawon
Candi Kalasan
Candi Sari
Candi Sewu
Candi Banyunibo
Candi Plaosan


=== East Java ===
Candi Surawana
Candi Jabung


== Israel ==
Tel Aviv Zen Center


== Japan ==


=== Fukui ===

Eihei-ji


=== Fukuoka ===
Shōfuku-ji


=== Gifu ===
Eihō-ji
Shōgen-ji
Shōhō-ji


=== Hiroshima ===
Buttsū-ji
Ankoku-ji
Myōō-in


=== Hyōgo ===
Antai-ji
Chōkō-ji
Engyō-ji
Hōrin-ji
Hōun-ji
Ichijō-ji
Jōdo-ji in Ono
Kakurin-ji in Kakogawa
Sagami-ji
Taisan-ji in Kobe


=== Iwate ===
Chūson-ji
Mōtsū-ji


=== Kagawa ===

Motoyama-ji
Zentsū-ji


=== Kanagawa ===
Engaku-ji
Hōkoku-ji
Kenchō-ji
Kōtoku-in
Sōji-ji


=== Kyoto ===

Adashino Nembutsu-ji
Byōdō-in
Chion-in (Head temple of the Jodo Shu Buddhist sect)
Daigo-ji
Daikaku-ji
Daitoku-ji
Eikan-dō Zenrin-ji
Ginkaku-ji (Temple of the Silver Pavilion)
Higashi-Honganji (One of two head temples of the Jōdo Shinshū Buddhist sect)
Kinkaku-ji (Rokuonji, Deer Garden Temple, Temple of the Golden Pavilion)
Kiyomizu-dera
Kōdai-ji
Kōzan-ji
Manpuku-ji (Ōbaku temple at Uji)
Myōshin-ji
Nishi-Honganji (One of two head temples of the Jodo Shinshu Buddhist sect)
Nanzen-ji
Ninna-ji
Ryōan-ji
Saihō-ji
Sanjūsangen-dō
Tenryū-ji (major temple of the Rinzai school)
Tōfuku-ji
Manju-ji

Tō-ji


=== Miyagi ===
Zuigan-ji


=== Nagano ===
Zenkō-ji


=== Nagasaki ===
Fukusai-ji
Sōfuku-ji in Nagasaki


=== Nara ===

Asuka-dera
Daian-ji
Gangō-ji
Hase-dera
Hokke-ji
Hōki-ji
Hōryū-ji
Kimpusen-ji
Kōfuku-ji
Ōminesan-ji
Saidai-ji
Murō-ji
Shin-Yakushi-ji
Taima-dera
Tōdai-ji
Tōshōdai-ji
Yakushi-ji


=== Osaka ===
Shitennō-ji (the first Buddhist and oldest officially administered temple in Japan)


=== Saitama ===
Heirin-ji


=== Shiga ===

Eigen-ji
Enryaku-ji (Temple complex on a mountain northeast of the city)
Ishiyama-dera
Mii-dera


=== Shizuoka ===
Ryūtaku-ji
Shōgen-ji
Taiseki-ji


=== Tochigi ===
Rinnō-ji


=== Tokyo ===
Kan'ei-ji
Sengaku-ji
Sensō-ji
Shōfuku-ji in Higashimurayama


=== Toyama ===
Kokutai-ji


=== Wakayama ===

Chōhō-ji
Fudarakusan-ji
Jison-in
Mount Kōya (Temple complex)
Kongōbu-ji

Negoro-ji
Seiganto-ji


=== Yamagata ===
Yama-dera


=== Yamaguchi ===
Kōzan-ji in Shimonoseki


=== Yamanashi ===
Kōgaku-ji
Kuon-ji


== Laos ==


=== Vientiane ===
Pha That Luang, Vientiane
Wat Si Saket, Vientiane
That Dam, Vientiane


=== Luang Phrabang ===
Wat Xieng Thong, Luang Phrabang


== Malaysia ==


=== Kuala Lumpur ===
Buddhist Maha Vihara, Brickfields
Thai Buddhist Chetawan Temple
Thean Hou Temple
Sin Sze Si Ya Temple


=== Malacca ===
Cheng Hoon Teng (oldest Buddhist temple in Malaysia)


=== Penang ===
Kek Lok Si Temple (槟城极乐寺)
Dhammikarama Burmese Temple, Burmah Lane
Wat Chaiya Mangkalaram, Burmah Lane
Kuan Yin Temple
Snake Temple
Nibbinda Forest Monastery (丛林道场)


=== Kelantan ===
Wat Phothivihan


=== Perak ===
Sasanarakkha Buddhist Sanctuary
Sam Poh Tong
Kek Look Seah Temple


== Mongolia ==


=== Ulan Bator ===
Gandantegchinlen Khiid Monastery


=== Övörkhangai ===
Erdene Zuu Monastery


=== Selenge ===
Amarbayasgalant Khiid
Shankh Monastery


== Myanmar ==


=== Yangon Region ===
Yangon (Rangoon)
Botahtaung Pagoda
Kaba Aye Pagoda (World Peace Pagoda)
Shwedagon Pagoda
Sule Pagoda


=== Mandalay Region ===
Bagan (Pagan)
Ananda Temple
Bupaya Pagoda
Dhammayangyi Temple
Dhammayazika Pagoda
Gawdawpalin Temple
Htilominlo Temple
Lawkananda Pagoda
Mahabodhi Temple
Manuha Temple
Mingalazedi Pagoda
Payathonzu Temple
Shwegugyi Temple
Shwesandaw Pagoda
Shwezigon Pagoda
Sulamani Temple
Tharabha Gate
Thatbyinnyu Temple
Mandalay
Atumashi Monastery
Kuthodaw Pagoda
Mahamuni Buddha
Shwenandaw Monastery


=== Rakhine State ===
Shite-thaung Temple
Htukkanthein Temple
Koe-thaung Temple
Andaw-thein Ordination Hall
Le-myet-hna Temple


=== Bago Region ===
Bago
Shwethalyaung Buddha (Reclining Buddha)
Shwemawdaw Pagoda
Pyay
Shwesandaw Pagoda (Pyay)


=== Mon State ===
Kyaiktiyo Pagoda


=== Shan State ===
Hpaung Daw U Pagoda
Pindaya Caves


== Nepal ==


=== Kapilbastu District ===
Tilaurakot


=== Kathmandu District ===
Swayambhunath
Boudhanath


=== Mustang District ===
Muktinath


=== Rupandehi District ===
Lumbini, birthplace of Siddhartha Gautama Buddha


== New Zealand ==
Fo Guang Shan Temple, Auckland, North Island


== Philippines ==


=== Metro Manila ===
Seng Guan Temple
IBPS Manila, Philippines (Fo Guang Shan Manila) (菲律賓馬尼拉佛光山)
World Fellowship of Buddhists, Philippines (世界佛教徒联谊会)
Ocean Sky Chan Monastery, Philippines (海天禅寺)


=== Davao ===
Lon Wa Buddhist Temple, Philippines (隆華禪寺)


== Russia ==
List of Buddhist temples in Russia


== Singapore ==

Aaloka Buddhist Center
Amitabha Buddhist Centre
Bodhiraja Buddhist Society
Buddha of Medicine Welfare Society
Buddha Tooth Relic Temple and Museum
Buddhist Library
Burmese Buddhist Temple
Cheng Beng Buddhist Society
Dharma Drum Singapore
Fo Guang Shan
Foo Hai Ch'an Monastery
Hai Inn Temple
Hua Giam Si
Jin Long Si Temple
Karma Kagyud Buddhist Centre
Nyingma Kathok Buddhist Centre
Kong Meng San Phor Kark See Temple
Kwan Im Thong Hood Cho Temple
Kwan Yin Chan Lin
Nichiren Shoshu Buddhist Association (Singapore)
Palelai Buddhist Temple
Poh Ern Shih Temple
Sakya Muni Buddha Gaya Temple
Siong Lim Temple
Sri Lankaramaya Buddhist Temple
Thekchen Choling Tibetan Buddhist Temple
Thian Hock Keng
Ti-Sarana Buddhist Association
Tzu Chi Singapore
Vipassana Meditation Centre
Wat Ananda Metyarama Thai Buddhist Temple


== South Africa ==
Nan Hua


== South Korea ==
See also: Korean Buddhist temples


=== Seoul ===

Bongeunsa
Bongwonsa
Jogyesa


=== Gyeonggi ===
Bongseonsa
Silleuksa
Yongjusa


=== Gangwon ===

Sinheungsa
Oseam
Woljeongsa
Naksansa


=== North Chungcheong ===
Beopjusa
Guinsa


=== South Chungcheong ===
Magoksa
Sudeoksa


=== North Gyeongsang ===
Bunhwangsa
Donghwasa
Bulguksa (including Seokguram)
Hwangnyongsa
Jikjisa


=== South Gyeongsang ===

Ssanggyesa
Tongdosa (one of the Three Jewel Temples)
Haeinsa (one of the Three Jewel Temples)
Busan
Beomeosa


=== North Jeolla ===
Geumsansa
Miruk-sa
Seonunsa


=== North Pyeongan ===
Pohyonsa


=== South Jeolla ===
Songgwangsa (one of the Three Jewel Temples)
Hwaeomsa


=== Daejeon ===
Musangsa


== Sri Lanka ==


=== Ampara ===
Magul Maha Viharaya, Lahugala
Muhudu Maha Viharaya, Pottuvil


=== Anuradhapura ===
Atamasthana
Sri Maha Bodhiya
Lovamahapaya
Ruwanwelisaya
Mirisaveti Stupa
Isurumuniya
Thuparamaya
Lankarama
Jetavanarama
Abhayagiriya
Samadhi Statue
Sela Cetiya


=== Badulla ===
Muthiyangana Raja Maha Vihara


=== Balapitiya ===
Kothduwa temple, Maduganga River


=== Colombo ===
Bellanwila Rajamaha Viharaya, Bellanwila
Buddhist Cultural Centre, Dehiwala
Gangaramaya Temple, Colombo


=== Dambulla ===
Dambulla cave temple


=== Hambantota ===
Tissamaharama Raja Maha Vihara
Sithulpawwa Rajamaha Viharaya


=== Jaffna ===
Nagadeepa Purana Viharaya


=== Kandy ===
Devanapatissa Vipassana International Meditation Centre
Temple of the Tooth


=== Kalutara ===
Asokaramaya Buddhist Temple


=== Kelaniya ===
Raja Maha Vihara


=== Kurunegala ===
Ridi Viharaya


=== Madampe ===
Senanayake Aramaya


=== Mahiyanganaya ===
Mahiyangana Raja Maha Vihara


=== Matale ===
Aluvihare Rock Temple, Matale


=== Polgahawela ===
Mahamevnawa Buddhist Monastery


=== Polonnaruwa ===
Polonnaruwa Vatadage
Gal Vihara
Somawathiya Stupa


=== Trincomalee ===
Seruvila Mangala Raja Maha Viharaya


== Taiwan ==

Chung Tai Shan (中台山), Nantou, the tallest Buddhist temple in the world. Height: 136 metres (446 ft).
Dharma Drum Mountain (Fa Gu Shan) (法鼓山), international headquarters of Dharma Drum Mountain organization
Mengjia Longshan Temple (龍山寺)
Fo Guang Shan (佛光山)


== Tanzania ==
Tanzania Buddhist Temple and Meditation Center, Dar es Salaam


== Thailand ==


=== Bangkok ===
Wat Benchamabophit (marble temple)
Wat Suthat, with Giant Swing
Wat Ratchanadda
Wat Phra Kaew
Wat Pho
Wat Arun


=== Chiang Mai ===
Wat Aranyawiwake
Wat Chedi Liem
Wat Chedi Luang
Wat Chiang Man
Wat Doi Mae Pang
Wat Phra Singh
Wat Phrathat Doi Suthep
Wat Umong


=== Chiang Rai ===
Wat Phra That Doi Chom Thong
Wat Phra Kaew, Chiang Rai
Wat Phra Sing, Chiang Rai
Wat Rong Khun


=== Kanchanaburi ===
Tiger Temple


=== Pathum Thani ===
Wat Phra Dhammakaya


=== Phetchabun ===
Wat Pha Sorn Kaew


=== Saraburi ===
Wat Tham Krabok


=== Surat Thani ===


==== Ko Samui ====
Wat Phra Yai (Big Buddha)
Wat Khunaram (Temple of the Mummy Monk)
Wat Lamai
Wat Plai Laem (Guanyin temple)


=== Phitsanulok ===
Wat Phra Sri Rattana Mahatat Woramahawihan
Wat Nang Phaya
Wat Ratchaburana, Phitsanulok
Wat Sam Ruen
Wat Grong Greng Rin
Wat Laemphrathat


== Uganda ==
The Uganda Buddhist Centre, Kampala


== United States ==


=== Arizona ===
Kunzang Palyul Choling


=== Colorado ===
The Great Stupa of Dharmakaya, Shambhala Mountain Center


=== California ===

Abhayagiri Buddhist Monastery, Redwood Valley
Berkeley Zen Center, Berkeley
Buddha Essence Temple, Zen Center of Los Angeles
City Of Ten Thousand Buddhas, Talmage, Mendocino County
Deer Park Monastery, Escondido
Hartford Street Zen Center, San Francisco
Hazy Moon Zen Center, Los Angeles
Hsi Lai Temple, Hacienda Heights Suburb, Los Angeles County
Koyasan Buddhist Temple, Little Tokyo, Los Angeles
Mount Baldy Zen Center
Pao Fa Temple, Irvine, California
Pacific Zen Institute, Santa Rosa, California
San Fran Dhammaram Temple, San Francisco Thai Temple, West San Francisco
San Francisco Zen Center, San Francisco
Green Gulch Farm, Muir Beach
Beginner's Mind Temple, SFZC
Tassajara Zen Mountain Center, Carmel Valley

Shasta Abbey, Mount Shasta
Sonoma Mountain Zen Center, Santa Rosa, Sonoma County
Wat Buddhanusorn, Fremont, California
Wat Mongkolratanaram, Berkeley
Yokoji Zen Mountain Center, near Idyllwild
Zenshuji Soto Mission, Los Angeles


=== Connecticut ===
Do Ngak Kunphen Ling Tibetan Buddhist Center for Universal Peace, Redding, Connecticut


=== Florida ===
Guang Ming temple, Orlando
Tubten Kunga Center
Wat Florida Dhammaram


=== Georgia ===
Atlanta Soto Zen Center


=== Hawaii ===
Byodo-In Temple
Broken Ridge Buddhist Temple
Daifukuji Soto Zen Mission, Honalo
Hawaii Shingon Mission
Honpa Hongwanji Mission of Hawai'i


=== Illinois ===
Buddhist Temple of Chicago, independent, Chicago
Chicago Zen Center, Sanbo Kyodan lineage, Evanston
Daiyuzenji
Midwest Buddhist Temple, Jodo Shinshu lineage, Chicago


=== Kentucky ===
Furnace Mountain, Stanton


=== Louisiana ===
New Orleans Zen Temple


=== Maryland ===
Kunzang Palyul Choling


=== Massachusetts ===
Cambridge Zen Center
Pioneer Valley Zendo
Wat Boston Buddha Vararam


=== Michigan ===
Lake Superior Zendo
Still Point Zen Buddhist Temple, Detroit


=== Minnesota ===
Dharma Field Zen Center
Minnesota Zen Center


=== Nebraska ===
Nebraska Zen Center, Omaha, Nebraska


=== New Jersey ===
So Shim Sa Zen Center


=== New Mexico ===
Bodhi Manda Zen Center, Jemez Springs, New Mexico
Hokoji Zendo, Arroyo Seco, New Mexico
Kagyu Shenpen Kunchab, Santa Fe, New Mexico
Upaya Institute and Zen Center, Santa Fe, New Mexico
Zuni Mountain Stupa, Grants, New Mexico


=== New York ===
Blue Cliff Monastery
Chapin Mill Buddhist Retreat Center
Chogye International Zen Center
Chuang Yen Monastery
Dai Bosatsu Zendo Kongo-ji
Karma Triyana Dharmachakra
Mahamevnawa Buddhist Monastery (Theravada)
Namgyal Monastery
New York Mahayana Temple
New York Zendo Shobo-Ji
Rochester Zen Center
USA Shaolin Temple
Still Mind Zendo
Vajiradhammapadip Temple
Village Zendo
Zen Center of Syracuse
Zen Mountain Monastery


=== North Carolina ===
Wat Carolina Buddhajakra Vanaram


=== Ohio ===
Cleveland Buddhist Temple


=== Oregon ===
Great Vow Zen Monastery


=== Rhode Island ===
Providence Zen Center


=== Texas ===
American Bodhi Center, Waller County
Chua Buu Mon, Port Arthur
Chua Linh-Son Buddhist Temple, Austin
Jade Buddha Temple, Houston
Maria Kannon Zen Center, Dallas
Wat Buddhavas of Houston, Houston
Wat Buddhananachat of Austin, Del Valle


=== Utah ===
Kanzeon Zen Center, Salt Lake City


=== Virginia ===
Ekoji Buddhist Temple, Fairfax Station


=== Washington ===
Dai Bai Zan Cho Bo Zen Temple, Seattle
Seattle Buddhist Church, Seattle
Sravasti Abbey, Newport


=== Wisconsin ===
Deer Park Buddhist Center and Monastery, Oregon
Sotekizan Korinzenji, Madison


== Vietnam ==


=== An Giang ===
Tây An Temple


=== Bắc Ninh ===
Bút Tháp Temple
Dâu Pagoda
Phật Tích Temple


=== Bình Dương ===
Hội Khánh Temple


=== Đà Lạt ===
Linh Sơn Pagoda
Trúc Lâm Temple


=== Đồng Nai ===
Bửu Phong Temple


=== Hà Nội ===

One Pillar Pagoda
Quán Sứ Temple
Trấn Quốc Pagoda
Láng Temple
Perfume Pagoda
Thầy Temple


=== Hồ Chí Minh City (Sài Gòn) ===
Ấn Quang Pagoda
Giác Lâm Pagoda
Hoằng Pháp Temple
Quan Âm Pagoda
Thiên Hậu Temple
Tịnh Xá Trung Tâm
Vạn Hạnh Zen Temple
Vĩnh Nghiêm Pagoda
Xá Lợi Pagoda


=== Huế ===

Báo Quốc Pagoda
Diệu Đế Pagoda
Quốc Ân Temple
Thiên Mụ Pagoda
Thuyền Tôn Temple
Từ Đàm Pagoda


=== Hưng Yên ===
Chuông Temple


=== Kiên Giang ===
Sắc Tứ Tam Bảo Temple


=== Ninh Bình ===
Bái Đính Temple
Hoa Lư


=== Nam Định ===
Phổ Minh Temple


=== Nha Trang ===
Long Sơn Pagoda


=== Phú Yên ===
Phước Sơn Temple
Thuyền Tôn Temple


=== Sóc Trăng ===


=== Thái Bình ===
Keo Pagoda


=== Tiền Giang ===
Vĩnh Tràng Temple


=== Vũng Tàu ===
Thích Ca Phật Đài


== See also ==
Architecture of Bhutan
Architecture of India
Architecture of Mongolia
Architecture of Nepal
Architecture of Sri Lanka
Architecture of Thailand
Architecture of Vietnam
Buddhist architecture
Chinese architecture
Japanese architecture
Japanese Buddhist architecture
Khmer architecture
Korean architecture
Pakistani architecture
Thai temple art and architecture
Tibetan architecture


== Notes ==


== External links ==
BuddhaNet's Comprehensive Directory of Buddhist Temples sorted by country
Buddhactivity Dharma Centres database