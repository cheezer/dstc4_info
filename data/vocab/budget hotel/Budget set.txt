A budget set or opportunity set includes all possible consumption bundles that someone can afford given the prices of goods and the person's income level. The budget set is bounded above by the budget line. Graphically speaking, all the consumption bundles that lie inside the budget constraint and on the budget constraint form the budget set or opportunity set.
By most definitions, budget sets must be compact and convex.


== References ==