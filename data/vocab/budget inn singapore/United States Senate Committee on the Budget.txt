The United States Senate Committee on the Budget was established by the Congressional Budget and Impoundment Control Act of 1974. It is responsible for drafting Congress's annual budget plan and monitoring action on the budget for the Federal Government. The committee has jurisdiction over the Congressional Budget Office. The committee briefly operated as a special committee from 1919 to 1920 during the 66th Congress, before being made a standing committee in 1974.


== Contrasted with other committees ==
The Budget Committee is often confused with the Finance Committee and the Appropriations Committee, both of which have different jurisdictions: The Finance Committee is analogous to the Ways and Means Committee in the House of Representatives; it has legislative jurisdiction in the areas of taxes, Social Security, Medicare, Medicaid and some other entitlements. The Appropriations Committee has legislative jurisdiction over appropriations bills, which provide funding for government programs.
While the budget resolution prepared by the Budget Committee sets out a broad blueprint for the Congress with respect to the total levels of revenues and spending for the government as a whole, these other Committees prepare the legislation that actually enacts specific tax and spending policies.


== Members, 114th Congress ==
Source: 2013 Congressional Record, Vol. 159, Page S296


== Chairmen, 1974–present ==


== External links ==
Official website
Senate Budget Committee member profiles collected news and commentary at The Washington Post


== Notes ==