A Buffalo wing or Buffalo chicken wing in the cuisine of the United States is a chicken wing section (wingette or drumette) that is generally deep-fried, unbreaded, and coated in vinegar-based cayenne pepper hot sauce and butter. They are traditionally served hot, along with celery sticks and/or carrot sticks with blue cheese dressing for dipping. There are also boneless wings, from which the humerus and other bones have been removed.


== Preparation ==
Cayenne pepper hot sauce and melted butter or margarine are the basis of the sauce, which may be mild, medium, or hot. Typically, the wings are deep-fried in oil (although they are sometimes grilled or baked) until they are well browned. They are then drained, mixed with sauce, and shaken to coat the wings, completely covering them in the sauce.


== History ==
There are several different claims about how Buffalo wings were invented.
One of the more prevalent claims is that Buffalo wings were first prepared at the Anchor Bar in Buffalo, New York, by Teressa Bellissimo, who owned the bar with husband Frank. Several versions of the story have been circulated by the Bellissimo family and others:
Upon the unannounced, late-night arrival of their son, Dominic, with several of his friends from college, Teressa needed a fast and easy snack to present to her guests. It was then that she came up with the idea of deep frying chicken wings (normally thrown away or reserved for stock) and tossing them in cayenne hot sauce.

Dominic Bellissimo (Frank and Teressa's son) told The New Yorker reporter Calvin Trillin in 1980: "It was Friday night in the bar and since people were buying a lot of drinks he wanted to do something nice for them at midnight when the mostly Catholic patrons would be able to eat meat again." He stated that it was his mother, Teressa, who came up with the idea of chicken wings.
There was mis-delivery of wings instead of backs and necks for making the bar's spaghetti sauce. Faced with this unexpected resource, Frank Bellissimo says that he asked Teressa to do something with them.
However, a long article about the Anchor Bar in a local newspaper in 1969 does not mention Buffalo wings.
Another claim is that a man named John Young served chicken wings in a special "mambo sauce" at his Buffalo restaurant in the mid-1960s. His wings were breaded. Young had registered the name of his restaurant, John Young's Wings 'n Things, at the county courthouse before leaving Buffalo in 1970.
Marketing materials for Frank's RedHot claim that it was the hot sauce used in the Bellissimos' original recipe.


=== Growth and popularity ===
The city of Buffalo officially declared July 29, 1977, to be Chicken Wing Day.
Buffalo wings have become a popular bar food and appetizer across the United States and Canada. Large franchises specializing in Buffalo wings have emerged, notably Buffalo Wild Wings founded in 1982. As the market got larger, restaurants began to use a variety of sauces in addition to buffalo sauce. These sauces generally take influences from Chinese, Japanese, Caribbean, and Indian cuisines. Because of the mess caused by eating Buffalo wings, it is now common for restaurants to offer boneless "wings" that can be eaten with a fork. These are essentially small chicken tenders coated or spun in sauce. Many American-style restaurants in other countries will offer Buffalo chicken wings on their menus, especially if they also function as a bar.
Buffalo wings are used in competitive eating events, such as Philadelphia's Wing Bowl and at the National Buffalo Wing Festival. It has also become commonplace for restaurants to offer a contest featuring a customer eating a certain number of wings, coated in their hottest sauce. Many bars and restaurants intentionally create an extra-hot sauce for this purpose, and customers are usually rewarded with a picture on the wall or free meal.


== On television ==
The first mention of Buffalo wings on national television may have been on NBC's Today show in the 1980s. The dish gained prominence nationally after the Buffalo Bills' four consecutive appearances in the Super Bowl from 1991-1994 focused considerable media attention to the area for an extended period of time, giving Buffalo cuisine significant nationwide exposure. Clips showing cooks preparing the dish continue to be featured on nationally televised sporting events involving the Buffalo Bills and to a lesser extent the Buffalo Sabres.


== Variations ==

The appellation "Buffalo" is also now commonly applied to foods other than wings, including chicken fingers, chicken fries, chicken nuggets, popcorn chicken, shrimp, and pizza that are seasoned with the Buffalo-style sauce or variations of it.
The flavor of Buffalo wings is replicated by a number of dishes. A common variation on the "buffalo" sauce flavor is found in potato chips produced by a number of different companies. Many of these "Buffalo Chips" also incorporate a blue cheese flavoring to simulate the complete Buffalo wing experience.
Today, there are many flavors of prepared wings (wingettes and drumettes) available, besides the original hot Buffalo style. Flavors include barbecue, lemon pepper, pepper Parmesan, garlic, sweet-and-sour, honey mustard, Thai chili, and Caribbean jerk. Since the first introduction, restaurants have introduced hundreds of different flavors of chicken wings.


== See also ==

List of hors d'oeuvre
List of regional dishes of the United States


== References ==


== External links ==
National Buffalo Wing Festival
Anchor Bar official website
On the Wings of a Buffalo
from the Atlas of Popular Culture in the Northeastern United States
A nice gathering of buffalo wing recipes