Buffet Crampon is a manufacturer of woodwind musical instruments, including oboes, flutes, saxophones, and bassoons; however, the company is perhaps most famous for their clarinets, as Buffet is the brand of choice for many professionals.
Buffet Crampon began manufacturing musical instruments in 1825 exclusively in France, but has since expanded their business to include production facilities in Germany and China as well. Since the company's conception, Buffet Crampon has expanded to a worldwide market. Jérôme Perrod, Buffet Group's Chief Executive Officer, runs the Buffet Crampon, Besson, B&S, Antoine Courtois, Hans Hoyer, J. Keilwerth, Meinl Weston, Scherzer and W. Schreiber brands.


== History ==
Denis Buffet-Auger, of the Buffet family of French musical instrument makers, began making quality clarinets in Paris, France in 1825. The company expanded under Jean-Louis Buffet and his wife Zoé Crampon and became known as Buffet Crampon. (Another family member, Auguste Buffet jeune, who worked with famous clarinetist Hyacinthe Klosé to develop the Boehm system for clarinet, had his own business separate from Buffet Crampon.)
In 1850, Buffet Crampon established its headquarters at Mantes-la-Ville. The company continued to expand its range and quality in instrument production, beginning saxophone production in 1866, and winning numerous awards. The company began to take root in the American woodwind industry during the early 1900s.
In 1950, the company developed its famous R13 clarinet, an extremely popular professional-level clarinet. In 1981, Buffet joined Boosey & Hawkes, which sold the French company to The Music Group in 2003. Two years later Buffet was bought by a French group. In 2006 Buffet Crampon acquired two brass instrument manufacturers, Antoine Courtois Paris and Besson. In 2008 Buffet Crampon continues to pursue its strategy by the acquisition of the Leblanc clarinet factory in La Couture-Boussey, Département of Eure, Haute-Normandie in France.
Recently, Buffet has made some efforts to protect the African Blackwood trees which provide grenadilla wood for clarinets, from being eliminated by introducing some wood composite products to its line up. However, Buffet has decided not to support sustainably managed forestry under the management of the FSC. Buffet composite wood models do not have the grain structure of a true wood product and as such they do not have the issue of cracking due to environmental changes that are typically seen in clarinets and other wood instruments.


=== Evette and Evette & Schaeffer clarinets ===
Until the 1980s, only professional level clarinets carried the Buffet name. Lower priced clarinets for the beginner and intermediate market were branded "Evette" and "Evette & Schaeffer", respectively. For a time the Evette clarinets actually were built by other manufacturers under Buffet's sponsorship, and these instruments are marked "Evette sponsored by Buffet". By the early 1970s Buffet was making the Evettes in their own factory in Paris, and about 1979 manufacture was moved to a Buffet-owned factory in Germany. Evette & Schaeffer clarinets were made in Paris. Use of the Evette and Evette & Schaeffer brands ended about 1985, when the company began using the Buffet name on all its clarinets.


== Clarinet models ==

Buffet Crampon has released several clarinet models from the mid-20th century onwards, with models ranging from student to professional in marketing. The development of new models has sometimes led to the discontinuation of older models. The student models tend to be made from ABS resin, whereas intermediate and professional models are usually made from grenadilla wood. The professional models are usually made from more select grenadilla wood, and are usually unstained. Various options have been made available for select professional models, including the Greenline option, additional keywork, and gold-plated keys.


=== B♭ soprano clarinets ===


==== Student ====


==== Intermediate ====


==== Professional ====


=== Harmony clarinets ===
All of Buffet Crampon's harmony clarinets are professional models released under the "Prestige" label.


=== Bass clarinets ===


== Double Reeds ==


== Flute models ==
Originally Buffet Crampon flutes were made in Paris, France. But in 1981 the company was bought out by Boosey & Hawkes and their flutes were manufactured in Boosey & Hawkes factories in England (and later in Germany) over the period 1981 to 2004. In 2005 the Buffet Crampon company returned to French hands.
Modern Buffet Crampon flutes utilize the Cooper scale [see Albert Cooper (flute maker)] and have a reputation for accurate tuning. The 200 series flutes were of average construction quality and needed regular maintenance to play well. In the 1980s Boosey & Hawkes redesigned the Buffet Crampon flute as the 6000 series with improved key cups and stiffer keys. The 6000 series is generally regarded as mechanically superior to the 200 instruments.


=== 200 Series ===
225 - Silver plated, inline G, closed keys
227 - Silver plated, inline G, open hole (French style)
228 - Silver plated, offset G, closed keys


=== Redesigned 6000 series ===
6010 - Silver plated, inline G, closed keys
6020 - Silver plated, offset G, split E, closed keys
6040 - Silver plated, offset G, open hole (French style)
6050 - Silver plated, inline G, split E, open hole


=== 7000 series ===
Semi-pro models with silver heads and plated bodies. Model sub-numbers are similar to the 6000 series as above.


== Saxophone models ==
As early as 1866, Buffet Crampon was producing its first saxophones, 20 years after the invention of this instrument by the Belgian Adolph Sax. They were the first to manufacture saxophones, besides those made by Adolph Sax himself. Today, Buffet Crampon produces three series of saxophones: 100 Series, 400 Series and since 2013, the Senzo alto saxophone.


=== 100 Series ===
These are beginner instruments made in China
Series 100 alto saxophone, lacquer
Series 100 tenor saxophone, lacquer


=== 400 Series ===
These are intermediate/professional models made in China
Series 100 alto saxophone, lacquer
Series 400 alto saxophone, lacquer and matte
Series 400 tenor saxophone, lacquer and matte
Series 400 baritone saxophone, lacquer and matte


=== Senzo ===
This is their top model
Senzo alto saxophone


== References ==


== External links ==
Buffet Crampon website