Ovation Brands, Inc., with corporate offices in Greer, South Carolina and corporate support center in Eagan, Minnesota, is a company that owns several American national chains of buffet restaurants. Prior to October 30, 2013, the company was named Buffets, Inc.
In January 2008, predecessor company Buffets, Inc. and its affiliated companies operated at least 626 restaurants in 39 states. As of May 2013, the company had 347 restaurants in 35 states.
Their restaurants are now known as "Country Buffet", "Old Country Buffet", and "HomeTown Buffet".


== History ==
The company was founded by Roe Hatlen and C. Dennis Scott in 1983 along with Dermot Rowland and Doron Jensen. Rowland and Jensen left in 1986 to start Homestyle Buffet in Clearwater, Florida. Scott left in 1989 to start HomeTown, but the two chains merged in September 1996. The current Chief Executive Officer is Greg Graber.
On November 1, 2006, Buffets Inc. acquired Ryan's Restaurant Group adding 340 Ryan's Grill Buffet and Bakery and Fire Mountain Grills locations.
On January 22, 2008, Buffets Inc. filed for bankruptcy, under Chapter 11. Although all restaurants were said to be staying open and continuing to operate normally under the then-current reorganization plan, the company called for the immediate closing of 52 stores nationwide, effective February 12, 2008. The final day of normal business operations in these stores, as well as the announcement of closing to all team members in said stores, took place the previous day.
On April 28, 2009, Buffets Inc. emerged from Chapter 11 reorganization with reduced debt along with a new Board of Directors.
On January 18, 2012, Buffets, Inc. filed for Chapter 11 bankruptcy protection again, and the immediate closure of 81 restaurants throughout the country and was recapitalizing in an agreement with its lenders that will convert debt into a controlling equity stake.
On July 19, 2012, Buffets Inc. said it had completed the financial restructuring of its multichain enterprise and emerged from Chapter 11 reorganization, six months after its pre-negotiated filing on January 18.
On December 5, 2012, Buffets Inc., the operator of 347 steak-buffet restaurants in 35 states, named Anthony Wedo as its chief executive to lead the company on its post-bankruptcy "Plan to Win, Stay tuned." Wedo replaced R. Michael Andrews Jr., who had been chief executive of the company since 2005.
On April 25, 2013, Buffets, Inc., parent company of Old Country Buffet, Hometown Buffet, Country Buffet, Ryan's, and Fire Mountain restaurants reported sales up 1.7% for the quarter ending April 3, 2013**, the best year-over-year quarterly comparable sales performance in seven years.
On August 19, 2013, Buffets, Inc. reported that their restaurants, which are popular for their family dining experience, posted positive same store sales growth for a second quarter* in a row, beating the industry’s performance** and producing Buffets’ best annual same store comp sales performance in eight years.
“Our results indicate we have stabilized our business and are poised to grow our business for the first time in years,” said Anthony Wedo, CEO of Buffets, Inc. “The organization has been highly focused on innovations and investments that directly impact the guest experience. At the end of the day, what we deliver in our restaurants day in and day out is what really matters.”
“In the last year, we laid a strong ‘guest first’ foundation by rolling out our Great Steak Pledge, our Four Star Operating platform, and our highly successful Family Night initiative,” said Jason Abelkop, CMO of Buffets, Inc. “Families, especially young families, are our core target. Everything we’re doing is to improve the value equation for them.”
Abelkop stated that the new menu roll out in March dubbed “Real Favorites” had a positive impact on sales.
Anthony Wedo, a turnaround CEO, was hired in December 2012 to revive Buffets Inc., which had filed for bankruptcy twice in the previous five years. He appeared on the CBS television series Undercover Boss in a long blond wig, mustache, and tattoos to work in several of his restaurants, taking dishwasher, register and server jobs in Denver, Louisiana and Fresno.
Buffets Inc. operates 343 restaurants, most of which are Old Country Buffets. The chief financial officer and 125 employees are located in Eagan. Wedo and the company are headquartered in South Carolina, where there are 80 corporate employees.
On October 30, 2013, Buffets, Inc. – parent company to Ryan’s, HomeTown Buffet, Old Country Buffet, Country Buffet, FireMountain and Tahoe Joe’s Famous Steakhouse – celebrated its 30th anniversary with a corporate name change, to "Ovation Brands".
As of May 2013, the company had 347 restaurants in 35 states, under the brands "Country Buffet", "Old Country Buffet", "HomeTown Buffet", "Ryan's", "Fire Mountain" and "Tahoe Joe's Famous Steakhouse".
As of February 10, 2015 the company had 328 restaurants under the brands "Country Buffet", "Old Country Buffet", "HomeTown Buffet", "Ryan's", "Fire Mountain" and "Tahoe Joe's Famous Steakhouse".
On March 20, 2015 the company hired financial advisors to evaluate a possible sale of its business, less than three years after emerging from bankruptcy, CEO Anthony Wedo told Nation’s Restaurant News Tuesday.
The owner of 328 restaurants in 35 states has hired Duff & Phelps to evaluate strategic alternatives, including a potential sale. Ovation operates five buffet brands — Ryan’s, HomeTown Buffet, Old Country Buffet, Country Buffet and Fire Mountain — as well as a polished-casual brand called Tahoe Joe’s Famous Steakhouse.
Wedo, who has been Ovation’s CEO since December 2012, said the company has reached a point where it needs additional investment to continue what he calls its “reinvention.”
As of June 4th 2015 Anthony Wedo is no longer with the company Greg Graber has been named the new CEO of the comapny


== Chains ==


=== Ryan's Restaurant group ===
Ryan's was founded by Alvin A. McCall in 1977. It was acquired by Buffets, Inc. on November 1, 2006. Today there are over 200 restaurants in 23 states operating under the Ryan's Grill, Buffet and Bakery and Fire Mountain Grill names. Ryan's is based primarily in the southern and midwestern US, with its corporate headquarters located in Greer, South Carolina. Some of its accolades include being one of the "Best Small Companies in America" seven consecutive years, and named "Top Family Steakhouse in America" for 10 consecutive years.


=== HomeTown Buffet ===

HomeTown Buffet operates 168 locations throughout the United States, variably known as Old Country Buffet or Country Buffet, in addition to the flagship name. HomeTown Buffet locations have a mascot named, "H.T. Bee" (or "O.C. Bee" at Old Country Buffet). Usually, for in-restaurant appearances, the location manager or another employee wears the costume and greets guests and children.


=== Other restaurants ===
Tahoe Joe's Famous Steakhouse, which has nine locations in California.


== See also ==
List of buffet restaurants


== References ==


== External links ==
Ovation Brands Website