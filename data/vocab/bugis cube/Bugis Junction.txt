Bugis Junction (Chinese: 白沙浮广场) is an integrated development located at Victoria Street, Middle Road and North Bridge Road in Bugis, Downtown Core in Singapore. The development consists of a shopping mall, an office tower and the InterContinental Singapore Hotel.
The development was completed in 1995 and its owned by BCH Investments Pte Ltd which is a subsidiary of Keppel Corporation. CapitaLand's REIT, CapitaMall Trust owns a portion of the building. The mall also has a glass covered shopping streets which is the first in the country.
Anchor tenants include BHG which was formerly known as Seiyu. There is a youth-themed zone in the mall and numerous restaurants in the building. Kiosks are found along the shopping streets and shops are by the side, with shophouse themed buildings.


== Buildings ==
Bugis Junction consists of four sections. They are Bugis Junction Tower, Parco Bugis Junction, BHG Bugis Junction and InterContinental Singapore.


== Accessibility ==
Bugis Junction is accessible via an underpass from Bugis MRT station. The mall can be accessed by car through two car park entrances and there are bus stops located around the mall.


== External links ==
Official website
Bugis Junction, architecture by http://www.designinternational.com/bugis-junction