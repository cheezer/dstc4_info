Buginese (Basa Ugi, elsewhere also Bahasa Bugis, Bugis, Bugi, De) is the language spoken by about four million people mainly in the southern part of Sulawesi, Indonesia.


== History ==
The word Buginese derives from the word Bahasa Bugis in Malay. In Buginese, it is called Basa Ugi while the Bugis people are called To Ugi. Ugi is taken from the name to the first king of Cina, an ancient Bugis kingdom, La Sattumpugi. To Ugi basically means "the followers of La Sattumpugi".
Little is known about the early history of this language due to the lack of written records. The earliest written record of this language is Sureq Galigo, the epic creation myth of the Bugis people.
Another written source of Buginese is Lontara, a term which refers to the traditional script and historical record as well. The earliest historical record of Lontara dates to around the 17th century, and being influenced by myths, is not considered a reliable historical source.
Prior to the Dutch arrival in the 18th century, a missionary, B.F. Matthews, translated the Bible into Buginese, which made him the first European to acquire knowledge of the language. He was also one of the first Europeans to master Makassarese. The dictionaries and grammar books compiled by him, and the literature and folkfore texts he published, remain basic sources of information about both languages.
Upon colonization by the Dutch, a number of Bugis fled from their home area of South Sulawesi seeking a better life. This led to the existence of small groups of Buginese speakers throughout the Maritime Southeast Asia.


== Classification ==
Buginese is an ergative–absolutive language of the Austronesian language family, South Sulawesi branch. It has absorbed certain words from Malay and prestigious languages of the Sunda islands, such as anyarang 'horse', which is believed to come from the Javanese word janrang.


== Geographical distribution ==

Most of the native speakers (around 3 million) are concentrated in South Sulawesi, Indonesia but there are small groups of Buginese speakers in the island of Java, Samarinda and east Sumatra of Indonesia, east Sabah and Malay Peninsula, Malaysia and South Philippines. This Bugis diaspora are the result of migration since 17th centuries that was mainly driven by continuous warfare situations. (The Dutch direct colonization started in early 20th century.)


== Phonology ==
* /ʔ/ only occurs finally, and is therefore not written.


== Grammar ==
Did you eat yet?
Not yet.


=== Aspects ===
The following are grammatical aspects of the language:
Example of usage:
ᨆᨙᨒᨚ ᨀ ᨌᨛᨆᨙ (méllo-kaq cemmé)
Lit.: want-me bath
I want to take a bath


== Writing system ==

Buginese was traditionally written using the Lontara script, of the Brahmic family, which is also used for the Makassar language and the Mandar language. The name lontara derives from the Malay word for the palmyra palm, lontar, the leaves of which are the traditional material for manuscripts in India, South East Asia and Indonesia. Today, however, it is often written using the Latin script.


=== The Buginese Lontara ===
The Buginese lontara (locally known as Aksara Ugi) has a slightly different pronunciation from the other lontaras like the Makassarese. Like other Indic scripts, it also utilizes diacritics to distinguish the vowels [i], [u], [e], [o] and [ə] from the default inherent vowel /a/ (actually pronounced [ɔ]) implicitly represented in all base consonant letters (including the zero-consonant a).
But unlike most other Brahmic scripts of India, the Buginese script traditionally does not have any virama sign (or alternate half-form for vowel-less consonants, or subjoined form for non-initial consonants in clusters) to suppress the inherent vowel, so it is normally impossible to write consonant clusters (a few ones were added later, derived from ligatures, to mark the prenasalization), geminated consonants or final consonants.
Older texts, however, usually did not use diacritics at all, and readers were expected to identify words from context and thus provide the correct pronunciation. As one might expect, this led to erroneous readings; for example, bolo could be misread as bala by new readers.


== Dialects and subdialects ==
The Bugis still distinguish themselves according to their major precolony states (Bone, Wajo, Soppeng and Sidenreng) or groups of petty states (around Pare-Pare, Sinjai and Suppa.) The languages of these areas, with their relatively minor differences from one another, have been largely recognized by linguists as constituting dialects: recent linguistic research has identified eleven of them, most comprising two or more sub-dialects.
This is a partial list of Buginese dialects: Bone (Palakka, Dua Boccoe, Mare), Pangkep (Pangkajane), Camba, Sidrap (Sidenreng, North Pinrang, Alitta), Pasangkayu (Ugi Riawa), Sinjai (Enna, Palattae, Bulukumba), Soppeng (Kessi), Wajo, Barru (Pare-Pare, Nepo, Soppeng Riaja, Tompo, Tanete), Sawitto (Pinrang), Luwu (Luwu, Bua Ponrang, Wara, Malangke-Ussu).


== Numbers ==
The numbers are:


== Trivia ==
The phrase "thank you" cannot be translated into the Buginese language. The Malay phrase "terima kasih" was borrowed instead.
A Buginese poem is painted on a wall near the Royal Netherlands Institute of Southeast Asian and Caribbean Studies in Leiden, Netherlands, as one of the wall poems in Leiden.


== See also ==

Bugis of Sabah
Bugis
Pallawa


== References ==


== External links ==
Buginese Soppeng dialect
The I La Galigo Epic Cycle of South Celebes and Its Diffusion
Languages of South Sulawesi
http://unicode-table.com/en/sections/buginese/