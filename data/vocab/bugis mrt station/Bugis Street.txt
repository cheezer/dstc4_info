Bugis, in Singapore, was renowned internationally from the 1950s to the 1980s for its nightly gathering of trans women, a phenomenon which made it one of Singapore's top tourist destinations during that period.
In the mid-1980s, Bugis Street underwent major urban redevelopment into a retail complex of modern shopping malls, restaurants and nightspots mixed with regulated back-alley roadside vendors. Underground digging to construct the Bugis MRT station prior to that also caused the upheaval and termination of nightly transgender sex bazaar culture, marking the end of a colourful and unique era in Singapore's history. This change helped improve Singapore's international image as it began to be globally recognised.
Today, the original Bugis Street is now a cobblestoned, relatively wide avenue sandwiched between the buildings of the Bugis Junction shopping complex. On the other hand, the lane presently touted as "Bugis Street" by the Singapore Tourist Promotion Board is actually developed from New Bugis Street, formerly Albert Street, and is billed as "the largest street-shopping location in Singapore". An attempt by the Singapore Tourist Promotion Board to bring back the former glamour was unsuccessful. Although the street is not a well-known tourist destination, it is frequented by many Singaporeans.


== History ==

Bugis Street lies in an extensive area which was commonly referred to in the past, by the Chinese-educated community, as Xiao Po (小坡; little slope). The latter stretched all the way from Tanjong Pagar, through Singapore's Chinatown, to Jalan Sultan. The whole vicinity was thriving and crammed with merchants and traders, making it one of the most vibrant economic zones of old Singapore.


=== Pre-1950s ===
According to knowledgeable long-term residents of the area, before the arrival of the British, there used to be a large canal which ran through the area where the Bugis, a seafaring people from South Sulawesi province in Indonesia, could sail up, moor their boats and trade with Singaporean merchants.
It was these people after whom the thoroughfare was named. The Bugis, or Buginese, also put their sailing skills to less benign uses and gained a reputation in the region as being a race of bloodthirsty pirates.
During the early colonial era, there also used to be low mounds of whitish sand in the area, earning the street the familiar Hokkien (Min Nan) moniker of Peh Soa Pu or Bai Sha Fu in Mandarin (白沙浮; white sand mounds). The Cantonese, however, referred to the street as Hak Gaai or Hei Jie in Mandarin (黑街; black street) as there were many clubs catering to the Japanese invaders in the 1940s. During the first half of the 20th century, commuters could conveniently travel from Bugis Street to anywhere else in Xiao Po via a tram service which ran along North Bridge Road, which was referred to by the Chinese-educated as Xiao Po Da Ma Lu (小坡大马路; little slope main road).


=== 1950s–1980s ===
After World War II, hawkers gathered there to sell food and goods. There was initially also a small number of outdoor bars set up beside rat-infested drains.
When transvestites began to rendezvous in the area in the 1950s, they attracted increasing numbers of Western tourists who came for the booze, the food, the pasar malam shopping and the "girls". Business boomed and Bugis Street became an extremely lively and bustling area, forming the heart of Xiao Po. It was one of Singapore's most famous tourist meccas from the 1950s to the 1980s, renowned internationally for its nightly parade of flamboyantly-dressed transwomen and attracted hordes of Caucasian gawkers who had never before witnessed Asian queens in full regalia.
The latter would tease, cajole and sit on visitors' laps or pose for photographs for a fee.
Others would sashay up and down the street looking to hook half-drunk sailors, American GIs and other foreigners on R&R, for an hour of profitable intimacy. Not only would these clients get the thrill of sex with an exotic oriental, there would be the added spice of transgressing gender boundaries in a seamy hovel.
There was an adage amongst Westerners that one could easily tell who was a real female and who was not – the transvestites were drop-dead gorgeous, while the rest were real women. The amount of revenue that the transwomen of Bugis Street raked in was considerable, providing a booster shot in the arm for the tourism industry. The term 'Boogie Street' has always been that used by British servicemen and not, as some mistakenly believe, by Americans in the wake of the 1970s disco craze.
Veterans recall that the notorious drinking section began from Victoria Street west to Queen Street. Halfway between Victoria and Queen Streets, there was an intersecting lane parallel to the main roads, also lined with al fresco bars. There was a well-patronised public toilet with a flat roof of which there are archival photos, complete with jubilant rooftop transwomen.
One of the "hallowed traditions" bestowed upon the area by sojourning sailors (usually from Britain, Australia and New Zealand), was the ritualistic "Dance of the Flaming Arseholes" on top of the infamous toilet's roof. Compatriots on the ground would chant the signature "Haul 'em down you Zulu Warrior" song whilst the matelots performed their act.
Over the years this became almost a mandatory exercise and although it may seem to many to be a gross act of indecency, it was generally well received by the sometimes up to hundreds of tourists and locals. The Kai Tais or Beanie Boys, as the transwomen were referred to by Anglophone white visitors, certainly did not mind either. By the mid-70s Singapore started a crackdown on this type of lewd behaviour and sailors were arrested at gunpoint by the local authorities for upholding the tradition. By this time those sailors brave enough to try it were dealt with severely and even shipped home in disgrace. Though many locals accepted this part of Singaporean culture, many conservative Singaporeans felt that it was a disgrace and it defaced Singapore's image.
The earliest published description of Bugis Street found by Yawning Bread as a place of great gender diversity was in the book "Eastern Windows" by F.D. Ommaney, 1960. Ommaney did not date specifically his description of the street but his book made clear that he was in Singapore from 1955 to 1960. A first-person account of Bugis Street in the 1950s is by "Bob", a visiting Australian sailor is given here.
In the mid-1980s, Bugis Street underwent major urban redevelopment into a retail complex of modern shopping malls, restaurants and nightspots mixed with regulated back-alley roadside vendors. Underground digging to construct the Bugis MRT station prior to that also caused the upheaval and termination of nightly transgender sex bazaar culture, marking the end of a colourful and unique era in Singapore's history.
Tourist and local lamentation of the loss sparked attempts by the Singapore Tourist Promotion Board (STPB) to attempt to recreate some of the old sleazy splendour by staging contrived "Ah Qua shows" on wooden platforms, but these artificial performances fell flat on their faces and failed to pull in the crowds. They were abandoned after a short time.


=== The Movie ===
The transwomen of Bugis Street were immortalised in an English-language film made by a Hong Kong film company which did employ some local talent in its production. (For more details, see Singapore gay films: Bugis Street).


== Gallery ==
The fame of the original Bugis Street has spawned many namesakes eager to capitalise on the brand, even though many tourists, as well as some young Singaporeans, have no inkling as to the reasons for its erstwhile "glamour".
Amongst the new places, buildings and companies which carry the name of "Bugis" are New Bugis Street, Bugis Square, Bugis Village, Bugis Junction, Parco Bugis Junction, Bugis Junction Towers, Bugis Cineplex, Bugis MRT station, Bugis Pasar Malam, New Bugis Food Village, Bugis Music World, Bugis Money Changer, Bugis City Holdings, Bugis Health Centre, Bugis Store Trading, Bugis Backpackers, and Bugis Street Development.
This cacophony of Bugis'es clamouring for a spot in the limelight, reminiscent of the transwomen who gave the original its glory, leads to great confusion when trying to locate Bugis Street itself.


=== Bugis Street: original ===
The original Bugis Street is now a cobblestoned, relatively wide avenue sandwiched between the buildings of Bugis Junction. Midway through its length is the new entity of Bugis Square, a granite-tiled plaza containing a dancing fountain and surrounded by the food, shopping and entertainment outlets of the Bugis Junction complex on all sides.


==== Bugis Square ====


=== Bugis Junction ===


==== Bugis Junction Towers ====


=== New Bugis Street ===
The lane presently touted as 'Bugis Street' by the Singapore Tourist Promotion Board and advertised from August 2005 onwards with an enormous light bulb-studded sign at night, actually developed from New Bugis Street, the latter having been created after the whole area was redeveloped in the mid-1980s. The new 'Bugis Street' is a maze of lanes lined with stalls selling pasar malam goods. It stretches from its entrance along Victoria Street facing the original Bugis Street and Bugis Junction to its other entrance along Queen Street facing the entrance to Albert Street.


=== Bugis Village ===


=== Bugis MRT station ===


=== Kwan Im Temple (near Bugis Street) ===


== External links and References ==
Yawning Bread's account of Singapore's transgender and sex-change history:
Ommaney, F.D, Eastern Windows, 1960. London:Longmans. pp. 39–45
Food and restaurants in Bugis on Burpple


== See also ==
List of transgender-related topics


== References ==
^ Ommaney, F.D, Eastern Windows, 1960. London:Longmans. pp. 39–45
^ guw-078 The sailor's birthday present
^ http://www.yawningbread.org/arch_2005/yax-457.htm


== External links ==
Yawning Bread's account of Singapore's transgender and sex-change history:[1]