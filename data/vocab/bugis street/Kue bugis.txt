Kue bugis is Indonesian kue or traditional snack of soft glutinous rice flour cake, filled with sweet grated coconut. The name is suggested to be related to Bugis ethnic group of South Sulawesi as their traditional delicacy, and it is originated from Makassar. In Java the almost identical kue is called kue mendut. Kue bugis, together with kue lapis and nagasari are among popular kue or Indonesian traditional sweet snacks, commonly found in Indonesian traditional marketplace as jajan pasar (market munchies).


== Ingredients and cooking method ==
The cake is made of ketan (glutinous rice) flour as the skin, filled with grated coconut flesh sweetened with palm sugar. The skin is made of flattened dough made from the mixture of glutinous rice flour, wheat flour, mashed potatoes, santan (coconut milk), sugar and salt, and colored with suji or green colored pandan. The sweet filling is made of grated coconut, palm sugar, salt, and pandan leaf for aroma. Traditional kue bugis is wrapped in banana leaf, usually young banana leaf which is thin and green-yellowish in color, the contemporary version however might uses plastic wrapper.
There are several version of kue bugis, the traditional common one is green colored kue bugis acquired from suji or pandan leaf. Another version include black kue bugis which uses ketan hitam or black glutinous rice flour. Another variant is called kue bugis mandi which is green ball covered in whitish layer made of white glutinous rice with coconut milk.


== See also ==
Klepon
Kue lapis


== References ==


== External links ==
Kue Bugis Recipe
Black Glutinous Rice Kue Bugis Recipe
Video of Kue Bugis Ketan Hitam