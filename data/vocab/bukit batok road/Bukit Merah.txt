Bukit Merah is a large New Town and a hill situated in the central-south region of Singapore. Previously, it was a much larger hill that was situated at the present site of Henderson housing estate, which lies opposite the Delta Sports Complex. There was a Chinese cemetery on the reverse side of the hill. The hill was trimmed to its current state in 1973 with the surrounding kampong being demolished to make way for the current Redhill Close. Mount Faber Park is also located in this region. The nearest MRT station is Redhill. The area surrounding this hill forms the Bukit Merah planning area, one of the 55 urban planning areas under the Urban Redevelopment Authority, in the Central Region. Bukit Merah planning area is geographically divided into 10 regions, Alexandra, Bukit Ho Swee, Bukit Merah Central, Bukit Merah View, Bukit Purmei, HarbourFront, Mount Faber, Redhill, Telok Blangah and Tiong Bahru.


== Transportation ==
While Bukit Merah and Redhill have the same meaning, they cannot be used interchangeably to describe the place as these are two different areas in the vicinity of each other. The Mass Rapid Transit station, Redhill MRT Station part of Singapore's transport system is named after the translation of Bukit Merah. It serves the immediate area of Redhill, Rumah Tinggi, Henderson, and parts of Bukit Merah. Tiong Bahru MRT Station is nearer to certain areas of Bukit Merah.
The Bukit Merah Bus Interchange (红山巴士转换站) serves the Bukit Merah New Town and the interchange which is located at Bukit Merah Central is nearer to the Henderson Road junction. There are a few SMRT buses that call at the interchange as it is operated by SBS Transit SBS Transit. Service Number 132 links the Interchange to Redhill MRT Station while bus services 5, 16, and 851 links the interchange to Tiong Bahru MRT Station.


== Education ==
Bukit Merah Estate consists of several Primary and Secondary schools with Alexandra Primary School being the newest school.
Primary Schools
Alexandra Primary School
Blangah Rise Primary School
CHIJ Kellock
Gan Eng Seng Primary School
Radin Mas Primary School
Zhangde Primary School

Secondary Schools
Bukit Merah Secondary School
CHIJ St. Theresa's Convent
Crescent Girls' School
Gan Eng Seng School
Henderson Secondary School

Tertiary Institutions
PSB Academy


== See also ==
Kim Seng
Alexandra


== References ==


== Sources ==
Victor R Savage, Brenda S A Yeoh (2003), Toponymics - A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1


== External links ==
Legend of Bukit Merah at the Singapore Museum website. [1]
Redhill – Culturally Rich. 30 October 2013, Website: http://alexresidencescondo-sg.com/news-updates/redhill-culturally-rich/