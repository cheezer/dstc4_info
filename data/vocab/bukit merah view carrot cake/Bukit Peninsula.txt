The Bukit Peninsula (Indonesian:Semenanjung Bukit) is at the southern end of the island of Bali, Indonesia. It is traditionally considered to be the entire area south of Jimbaran beach. Unlike the bulk of the rest of the island, it features a dry, arid and stoney landscape. It is administered under Kuta South District. Bukit means 'hill' in Indonesian.
The Indonesian government has encouraged the development of the area — instead of more fertile land — for large upmarket tourist facilities. It is a popular destination for surfers, particularly at Uluwatu.
This area has undergone large scale investment and growth during the early 2000s, partially owing to its proximity to the Ngurah Rai International Airport (the only international one in Bali) and the stunning ocean views from its cliff tops. In 2006 a new golf course (Bali's 4th) started construction. Bukit now hosts international hotels.


== Notes ==


== External links ==

 Media related to Bukit Peninsula at Wikimedia Commons
 Bukit Peninsula travel guide from Wikivoyage