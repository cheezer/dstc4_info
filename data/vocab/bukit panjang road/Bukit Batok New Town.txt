Bukit Batok New Town is a new town located in western Singapore within the boundaries of the Bukit Batok Planning Area. It is bounded by Bukit Batok Road, the Pan Island Expressway, along the western edge of Toh Tuck private estate, Bukit Batok East Avenue 6, Old Jurong Road, Bukit Timah Road, the north edge of Bukit Batok Nature Park, south edge of Hillview estate, east and north edge of Bukit Batok Town Park, and Bukit Batok West Avenue 5 back to Bukit Batok Road.


== Sub-zones ==
There are 8 sub-zones in Bukit Batok, namely
Bukit Gombak
Hillview
Brickworks
Guilin
Bukit Batok West
Bukit Batok Central
Bukit Batok East
Bukit Batok South


== Neighbouring Place ==
Choa Chu Kang
Bukit Panjang
Jurong East


== History ==
Earthworks to build Bukit Batok started in 1979. The first HDB estate blocks numbered Blk 2xx were completed in 1983, followed by 1xx and 6xx in 1985. Subsequently, the blocks at Bukit Gombak were finished in 1987, with Block 3xx ready for occupancy in 1989. Earthworks at Bukit Batok West began later.


== Etymology ==

Numerous differing accounts describe the origin of the name Bukit Batok. Bukit means "hill" in Malay, thus the name of the town gives the impression of it being hilly. Batok, however, has several interpretations.
One version has it that, according to a Javanese village chief in the tiny village of Gassing, coconut trees grew on the hills in the area. Hence the name batok, the Javanese term for coconuts.
The Chinese interpretation is that the hills were formed from solid granite, which is called batu in Malay, and this was corrupted to bato and finally batok. Another story has it that the hill resembles a skull top and batok could also be construed to mean "skull top".
Others believe that batok, the Malay word for cough, is linked to the place either because of its cool air (causing coughs and colds), or due to the sound of explosives historically used at its granite quarry, Little Guilin.


== Commercial activities ==

Bukit Batok's main shopping complex West Mall was opened in mid-1998. Developed by Alprop Pte. Ltd., a joint venture between the United Industrial Corporation (UIC) and Singapore Land, it has 8 storeys in total gross floor area of 283,000 sq ft (26,300 m²) on a land area of 106,000 sq ft (9,800 m²). It was built at a cost of S$170 million and houses amenities including a Post Office, Community Library and Cineplex, together with shops, restaurants and a supermarket.


== Transport ==

There are two MRT Stations in the Bukit Batok area: Bukit Batok MRT Station and Bukit Gombak MRT Station.
The Bukit Batok Bus Interchange, sited near to Bukit Batok MRT station and West Mall, is of moderate size and wholly used by Tower Transit Singapore. The interchange and most of the services were previously operated by SBS Buses, the precursor of SBS Transit, until 1999/2000; and after that it is SMRT Buses. SBS Transit still has a bus depot in Bukit Batok.


== Education ==
There are several primary schools in Bukit Batok:
Bukit View Primary School
Dazhong Primary School
Hong Kah Primary School
Keming Primary School
Lianhua Primary School
Princess Elizabeth Primary School
St Anthony's Primary School
Bukit Batok's secondary schools are:
Bukit Batok Secondary School
Bukit View Secondary School
Dunearn Secondary School
Hillgrove Secondary School
Swiss Cottage Secondary School
Yusof Ishak Secondary School
Millennia Institute, formed from the merger of Jurong Institute and Outram Institute, moved to its new campus off Bukit Batok West Avenue 3 in January 2007. Bukit Batok is also home to Singapore Hotel and Tourism Education Centre (SHATEC).


== Recreation ==
The Bukit Batok swimming complex is located off East Avenue 6. There are many parks in the neighbourhood, including the Bukit Batok Town Park (often considered part of Bukit Gombak) and the Bukit Batok Nature Park.
The CDANS Bukit Batok Country Club, for reservist members of the Civil Defence forces and their families, was opened in 1998. It offers a golf driving range, swimming pool, bowling alley and sports facilities for relatively affordable prices.
The Civil Service Club Bukit Batok Clubhouse offers swimming, bowling and related recreational facilities to civil servants, their families and the public. It is located near Bukit Batok Town Centre and was opened on 1 March 2006.


== Politics ==
The jurisdiction of Bukit Batok is shared by the Jurong Group Representation Constituency (Jurong GRC), which has an office at Bukit Batok Central and manages much of Bukit Batok; and Chua Chu Kang Group Representation Constituency (Hong Kah GRC), which manages areas north of Bukit Batok West Avenue 3 and Bukit Batok Central. There are four members of Parliament, two from each GRC, representing various areas in Bukit Batok.
Bukit Batok was a single-member constituency by itself in the 1991 general elections and before. It was later mainly subsumed into the Hong Kah GRC. As of 2011, Bukit Batok represents in Jurong GRC, there are two constituencies in Bukit Batok, one of which is Halimah bin Yacob (Bukit Batok East) and David Ong Kim Huat (Bukit Batok).
In the same elections in 2011, Bukit Gombak represents in Chua Chu Kang GRC, there are two constituencies also in Bukit Gombak, one of which it is a different system. Bukit Gombak represents as Low Yen Ling MP, while Amy Khor Lean Suan is under Hong Kah North SMC.


== Media ==
Many of MediaCorp's television channels (Channel 5, HD5, Channel 8, Channel NewsAsia, Suria, Vasantham and okto) are transmitted from Bukit Batok.
The First Singapore Idol winner, Taufik Batisah, is currently residing in Bukit Batok with his mother and brother.


== External links ==
NHG Bukit Batok Polyclinic
Bukit Batok Division Website
Hillview Peak Condominium
West Mall
Bukit Batok Driving Centre
Bukit Batok Private Driving Lessons
CDANS Country Club facilities
Civil Service Club (CSC)
Singapore International Hotel and Tourism Education Centre (SHATEC)
Bukit Batok Picture Gallery ~ 65 digital photographs