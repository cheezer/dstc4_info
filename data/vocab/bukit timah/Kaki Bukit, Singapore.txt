Kaki Bukit is an industrial area on the East side of Singapore. It is home to many high tech industries companies and warehouses. Located there is the prison complex Kaki Bukit Centre.
On the south of Kaki Bukit is Jalan Tenaga and Jalan Damai neighbourhoods of Bedok Reservoir Road.


== Transport ==
SBS Transit services, 5, 15, 58, 59 and 87 plies along Kaki Bukit Avenue 1, the only main road in Kaki Bukit. The future MRT station, Kaki Bukit serves this vicinity. Due to the construction of Kaki Bukit MRT Station, Kaki Bukit Avenue 1 have to be closed during the construction. Jalan Tenaga and Jalan Damai were widened in the absence of Kaki Bukit Avenue 1.