Bulim is a sub-zone of Jurong West, Singapore.
This sub-zone is composed with Businesses, Educational Institutions, Residential developments and Civic and Community Institutions.


== History ==
Prior to the 1990s, the sub-zone was part of the Bulim estate (a Slum) and where the Bulim Hill Cemetery (a Chinese cemetery) was located. In 1984, the cemetery was closed and exhumed thereafter to make way for the Kranji and Pan Island expressways. This segregated the Bulim estate and its western section became a sub-zone of Jurong West and was given the name Wenya.
In the 1990s, the development of Wenya began. Originally designed to be a residential sub-zone, the first two housing estates were completed in 2000 and 2002 respectively, complemented with the re-location of Corporation Primary and Westwood Secondary schools to the area in 2001, and the building of associated facilities for living. In 2005, the Housing and Development Board released two land parcels in the sub-zone for interim uses.
In 2007, the sub-zone was however re-zoned to be an Industrial area and the eastern section (occupying majority of the sub-zone) carved out and re-designed as Wenya Industrial Park. In 2012, a study was conducted to assess a new road network at the sub-zone for the industrial development. This led to the construction of Wenya Avenue and Wenya Street in 2013
In 2014, the names of Wenya Avenue and Wenya Street were changed to Bulim Avenue and Bulim Street respectively. This indicates a possible rename of the sub-zone to Bulim - to recognise the heritage of this area.


== Economy ==


=== Bulim Industrial Park ===
Situated east of the existing residential developments in the sub-zone, this is an Industrial zone that has been steadily developing since 2012. It will be part of the 2 West Integrated industrial township, which includes the neighboring NTU, CleanTech Park and a section of Tengah.
With the completion of a data centre and a corporate headquarters in the zone, the first section of the Industrial Park is set to open in 2014, followed by the opening of the second section in 2015, when a new bus depot, to be operated by SMRT Buses, is ready.


==== Interim uses ====
Along Jurong West Street 25, there are 2 interim developments, namely a Nursery(Thong Hup Gardens) and a Sports Ground for Futsal(Golazo Futsal Singapore).


=== Commercial ===
At the ground floor of Block 276 (a multi-storey car park), there is an Eating House, Clinic, Salon, Betting kiosk, Food & Beverages-cum-Launderette shop, and 3 Provision Shops, as well as an automated teller machine (ATM).


== Transport ==
Bulim is linked to the PIE Expressway with Major Arterial Roads Jurong West Avenue 2 (from Changi) and Jalan Bahar (from Tuas and KJE Expressway), and to the AYE with Jalan Bahar (via Jalan Boon Lay). From the Major Arterial Roads, Minor Arterial Roads Jurong West Avenue 3, Bulim Avenue and Bulim Drive distribute the traffic to the various Primary Access Roads in the sub-zone.
The roads in Wenya Industrial Park, Bulim Avenue and Bulim Drive, are not yet open to traffic.


=== Bus ===
There are 3 bus routes plying in the sub-zone:
181 connects residents of Bulim to Gek Poh and Jurong Point, while linking them to the Boon Lay Bus Interchange and MRT Station.
185 links the sub-zone to the rest of Jurong West(excluding Boon Lay, Chin Bee and Taman Jurong), and also to selected sections of Jurong East, Clementi and Queenstown, plying to and fro Soon Lee Bus Park and Buona Vista Terminal.
651 operates only during morning and evening peak periods, and charges a flat fare comparable to the MRT, connects the sub-zone directly to the Downtown Core, via the AYE.
Along the boundary of the sub-zone,
there are 3 bus routes plying along Jalan Bahar:
172 connects the sub-zone to Boon Lay Bus Interchange and MRT Station (via Boon Lay Way and Jurong West Street 64), and Choa Chu Kang (via Old Choa Chu Kang Road).
199 connects the sub-zone to Boon Lay Bus Interchange and MRT Station (via Jurong West Central 1), and the Teacher Training Institute (via Nanyang Avenue).
405 operates only during Religious Festivals, connects the sub-zone to Choa Chu Kang Cemetery, while duplicating route 199 when plying to Boon Lay Bus Interchange and MRT Station.
there is 1 bus route plying along Jurong West Avenue 2:
157 connects the sub-zone to Hong Kah, Bukit Batok, Bukit Timah, the northern border of Novena, and Toa Payoh, while connecting it to the Boon Lay Bus Interchange and MRT Station (via Boon Lay Way and Jurong West Central 2).


== Education ==
There are four Education institutions in this sub-zone.
There is a Child Care centre at blk 274D operated by PCF Hong Kah North.
There is a Kindergarten at blk 276D operated by Agape Child Care(Jw) Pte. Ltd.
Corporation Primary School is located at Jurong West Street 24.
Westwood Secondary School is located at Jurong West Street 25.
Bulim Autism School is located at Bulim Avenue.


== Housing ==
There are 24 housing blocks in this sub-zone and all are Public Apartments.


=== Public Apartments ===
The Public Apartments and associated infrastructure are managed and maintained by Chua Chu Kang Town Council.


=== Politics ===
Bulim is part of Hong Kah North SMC and Dr Amy Khor is the constituency's MP.


== Community spaces ==


=== Health and Medical Care ===
The Saint Joseph's Home at Jurong Road [2] provides "shelter, care and love for the aged and the destitute."


=== Civic and Community Institutions ===
The Singapore Boys' Home and the Singapore Boys' Hostel at Jurong West Street 24  are for male Juveniles arrested and charged by the law.


=== Places of Worship ===
Located along Jalan Bahar and Jurong West Street 24, a new mosque will be constructed by 2015 or later.
2 other sites in the sub-zone have also been allocated for Place Of Worship, at Jurong West Street 22 and 23.


== Recreation ==
The Jalan Bahar Park, at the junction of Avenue 3 and Street 24, is a major park maintained by Chua Chu Kang Town Council.


== Neighbouring Areas ==


== References ==