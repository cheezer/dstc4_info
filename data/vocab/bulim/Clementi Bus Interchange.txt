Clementi Bus Interchange is an air-conditioned bus interchange in Clementi New Town, beside Clementi MRT Station. It is located at the junction of Clementi Avenue 3 and Commonwealth Avenue West, situated within the Clementi Mall and integrated with nearby Clementi. This interchange serves mainly commuters travelling to the various housing estates, schools, recreational areas, healthcare centres and places of worship in Clementi and West Coast.


== History ==
The interchange was first opened on 16 November 1980 by Chor Yeok Eng, Senior Parliamentary Secretary for Environment and Member of Parliament for Bukit Timah. It was a long covered walkway structure with queue railings and berths at the side of the road, leading many people to call it a "roadside bus terminal" resembling the interchanges built in the 1970s. This is especially so as the interchange was very small and cramped, and floods sometimes when it was rainy. It was sort of similar to the original Bedok Bus Interchange.
Operations then moved to the temporary facility built across the road at the junction of Commonwealth Avenue West and Clementi Avenue 3 (Land Lot MK05-08585W) on 29 October 2006. This facility was a blue-red-white structure that resembles the original interchange and was used for 5 years and 27 days while the original site was redeveloped into what is known as the new Clementi Town Centre that was planned by HDB which consisted of a new air-conditioned bus interchange, Clementi Mall and 2 HDB flats (Clementi Towers).
The current interchange is an 8,100 square meteres facility opened on 26 November 2011. It is the 6th air conditioned bus interchange in Singapore and 2nd along the East West Line. Together with Clementi and the nearby commercial developments, it is part of the Clementi Integrated Public Transport Hub.


== References ==


== External links ==
Interchanges and Terminals (SBS Transit)
Interchange/Terminal (SMRT Buses)