Cliff Bungalow is an inner city residential neighbourhood in the south-west quadrant of Calgary, Alberta. Centered along 5 Street SW, it is bordered by Elbow River to the south, by Uptown 17th Avenue to the north and by Fourth Street to the east.
Cliff Bungalow was developed on land owned by the Canadian Pacific Railway in the 1870s. It was mainly populated by CPR employees, due to the convenient distance from the district to CPR headquarters and railyards. The area was gradually annexed to the City of Calgary between 1906 and 1912. Cliff Bungalow was established in 1907 although the name "Cliff Bungalow" would not be applied until the 1970s. It was re-zoned in 1935 to allow for denser residential buildings.
It is represented in the Calgary City Council by the Ward 8 councillor. The community has an area redevelopment plan in place.


== Demographics ==
In the City of Calgary's 2012 municipal census, Cliff Bungalow had a population of 7003193600000000000♠1,936 living in 7003137700000000000♠1,377 dwellings, a 1.3% increase from its 2011 population of 7003191200000000000♠1,912. With a land area of 0.4 km2 (0.15 sq mi), it had a population density of 4,800/km2 (12,500/sq mi) in 2012.
Residents in this community had a median household income of CAN$47,126 in 2005, and there were 20.4% low income residents living in the neighbourhood. As of 2006, 16.7% of the residents were immigrants. A proportion of 91.5% of the buildings were condominiums or apartments, and 72.2% of the housing was used for renting.


== Education ==
The community is served by Western Canada High School.


== See also ==
List of neighbourhoods in Calgary


== References ==


== External links ==
Cliff Bungalow-Mission Community Association