The Chinese Bungalow, also known as Chinese Den, is a 1940 British drama film directed by George King and starring Kay Walsh, Jane Baxter and Paul Lukas. It was adapted from the play The Chinese Bungalow by Marion Osmond and James Corbett.


== Cast ==
Paul Lukas ... Yuan Sing
Kay Walsh ... Sadie Merivale
Jane Baxter ... Charlotte Merivale
Robert Douglas ... Richard Marquess
Wallace Douglas ... Harold Marquess
Jerry Verno ... Stubbins
Mayura ... Ayah
John Salew ... Mr. Lum
James Woodburn ... Dr. Cameron


== References ==
^ http://ftvdb.bfi.org.uk/sift/title/29353


== External links ==
The Chinese Bungalow at the Internet Movie Database