Buona Vista Bus Terminal (Chinese: 波那维斯达巴士终站) is a bus terminal in Buona Vista, Singapore. This terminal does not allow boarding or alighting on site, but passengers can board at the nearest bus stop. Together with the nearby Ghim Moh Bus Terminal, both terminals serve passengers in Buona Vista and Ghim Moh.


== External links ==
Interchanges and Terminals (SBS Transit)