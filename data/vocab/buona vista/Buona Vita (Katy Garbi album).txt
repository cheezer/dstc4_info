Buona Vita (English: Life is Good; Greek: Ωραία Ζωή; ) is the 1st Italian and 18th studio album by Greek recording artist Katy Garbi. The album will be released commercially in Italy on June 8, 2013 by EGEA Music, with releases in various European locations thereafter. The album was produced by well-known Italian Producer Alberto Zeppieri.
The album contains 12 newly recorded tracks, all Italian remakes of Garbi's hits as well as some well-known Italian tracks, including 5 duets with well-known Italian artists such as Ornella Vanoni and Carlo Marrale.


== §Background ==
Zeppieri listened Garbi's song "Esena Mono", and immediately fell in love with her voice, as well as many of her hits written by Greek composers such as Phoebus, Nikos Antipas, and Peagasus. After contacting Garbi, he proposed a challenge in which he would select tracks from her lengthy Greek repertoire and adapt them to Italian lyrics and musical taste, of which she would re-record in the Italian language. Garbi happily accepted, and travelled to Milan, Italy to commence the recording of the adapted material.


== §Tracklist ==
1. Scolpiscimi nel fango (I sculpt in the mud) - originally the song "Syneffa" from the album "Pazl" (2011), music by Nikos Antypas - 3:21
2. Ti sento (Se niotho) (I hear you (I fell you)) (Duet with Carlo Marrale) - originally the song "Ti sento" which had sung by Matia Bazar from the album "Melancholia" (1986), written by Salvatore Stellita, Sergio Cossu and Carlo Marrale - 3:33
3. A te soltanto (Just for you) - originally the song "Esena mono" from the album "Emmones Idees" (2003), music by Dimitris Kontopoulos - 3:33
4. Egoista (Selfish) - originally the song "Ego" from the album "Pazl" (2011), music by Nikos Antypas - 3:10
5. Buona vita (Zalizoi) (Good life (Amazing)) (Duet with Ornella Vanoni) - originally the song "Buona vita" which had sung by Ornella Vanoni from the album "Una bellissima ragazza" (2007), written by Teofilo Chantre, Alberto Zeppieri and Ornella Vanoni - 5:03
6. La malinconia (The melancholy) (feat. Tinkara) - originally the song "Tha melagholiso" from the album "Arhizo Polemo" (1996), music by Phoebus - 3:23
7. Se questo fosse un film (If all that was a film) (Duet with Toto Cutugno) - originally the song "Ola sta zitisa" from the album "Pazl" (2011), music by Nikos Antypas - 3:52
8. Un'altra me (Another me) - originally the song "Kainourgia ego" from the album "Kainourgia Ego" (2008), music by Nikos Antypas - 3:51
9. Anime (Anime) (Duet with Maurizio Lauzi) - originally the song "Pos allazei o kairos" from the album "Pos Allazei O Kairos" (2006), music by Pegasus - 4:02
10. Plastica (Plastic) - originally the song "Viastika" from the album "Apla Ta Pragmata" (2001), music by Antonis Pappas - 2:47
11. Bugie (Lies) - originally the song "Psemata" from the album "Pazl" (2011), music by Nikos Antypas - 3:17
12. Perduta (Lost) (feat. the Choir of Ruda) - originally the song "Hamena" from the album "Arhizo Polemo" (1996), music by Phoebus - 4:20


== §Singles ==
"Buona Vita"
"Buona Vita" (English: Life is Good; Greek: Ωραία Ζωή; ) was the first single off the album, released on July 27, 2012 many months prior to the album's release on 2 November 2009. The track is a remake of a well-known Italian song with the same title, and is a duet with Italian artist Ornella Vanoni. It has been adapted to include both Italian and Greek lyrics, with both Vanoni and Garbi singing in both languages. The single was released exclusively on Love Radio 97.5 (Greece) in an interview with Garbi, gaining substantial airplay on the station which commonly plays a foreign repertoire. It should be noted that the original version of the song has been used in a global campaign by the United Nations Organization to recognize children around the world.
"Ti Sento"

"Ti Sento" (English: I Hear You (from Italian) I Feel You (from Greek); Greek: Σε Νίωθω; ) was officially released as a music video on June 3, 2013 and is also expected to be released as a radio single to coincide with the album's Italian release date. The track is a remake of hit single by Italian band Matia Bazaar of the same name. The remake is as a duet with Italian artist Carlo Marrale, who was also the original composer of the track released in 1986. It has been adapted to include both Italian and Greek lyrics, with both Marrale and Garbi singing in both languages.
"Ti sento (Extended Version) [Luca Zeta vs Sander Remix]" was digitally released on July 11 via iTunes.


== §Music Videos ==
"Ti Sento"
The music video released virally on June 3, 2013 was directed by Viki Velopoulou. The music video was designed to showcase the Greek summer featuring the Aegean Sea and impressive scenic visuals of islands including Santorini and Rodes. It follows Garbi strolling down the iconic white stairs found in the Cyclades islands, overlooking a stunning view of the Thiran circular archipelago surrounded by the Aegean Sea. The video utilises underwater filming techniques featured in underwater scenes where Garbi is swimming with a dolphin and also caressing an underwater Herma. The video was filmed during August, 2012 with filming locations in Rodes, Dodecanese and Santorini, Cyclades.


== §Release History ==


== §References ==

totalmusic.it
http://www.rockol.it/news-500119/Ornella-Vanoni,-disponibile-a-breve-un-duetto-con-Kaiti-Garbi
https://itunes.apple.com/gr/album/buona-vita/id651696472


== §External links ==
Official website