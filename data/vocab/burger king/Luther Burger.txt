A Luther Burger, or doughnut burger (among several naming variations), is a hamburger or cheeseburger with one or more glazed doughnuts in place of the bun. These sandwiches have a disputed origin, and tend to run between approximately 800 and 1,500 calories (3,300 and 6,300 kJ).


== Origins ==
According to legend, the burger was named for and was a favorite (and possible invention) of singer, songwriter and record producer Luther Vandross. This origin is mentioned in a January 2006 episode of animated series The Boondocks, "The Itis", in which the character Robert Freeman creates a restaurant which serves the burger.
Mulligan's, a suburban bar in Decatur, Georgia, serves the Luther Burger in addition to its "hamdog". The Daily Telegraph reported that Mulligan's may be progenitor of the burger when the owner substituted the doughnuts when running out of buns.


== Products ==
The Gateway Grizzlies baseball team of Sauget, Illinois have served the Luther Burger at their ballpark. Their version consists of a deep-fried Krispy Kreme doughnut around a black Angus all-beef patty, melted cheese, and two strips of bacon. Dubbed a "cardiologist's worst nightmare", this burger is 1,000 calories (4,200 kJ). Aside from bringing in revenue from sales, it has drawn more fans to come out to the ballpark: apparently, attendance at games has increased and the burger is one of the most popular aspects of the park. The burger has met with criticism from burger purists and from people who are turned off by the thought of the taste and the health implications.
In Astoria, Queens, a Lebanese burger restaurant began selling a halal-compliant version of the Luther Burger (a Crave Doughnut Burger) with turkey bacon in 2010. The restauranteur added the 1⁄4-pound (0.11 kg) burger to the menu after a friend experienced one while traveling through the Southern United States. Restaurant owner Firas Zabib has said he sells 10–20 of the US$6.50 burgers per day, and that his was the first shop in New York City to sell them.

Around the United States, copies and variations of the Luther Burger have appeared at state fairs. The 2010 Wisconsin State Fair variation held a 1⁄4-pound Angus beef burger, topped with Wisconsin cheddar cheese and two strips of chocolate-covered bacon, on a toasted Krispy Kreme doughnut bun. The burger sold for US$5 and equaled 1,000 Calories. Erika Celeste, for National Public Radio's All Things Considered detailed a "doughnut burger" sold at the 2010 Indiana State Fair when discussing the imaginative and caloric "arms race" at state fairgrounds to devise new "wackier" menu items: "[a] fresh patty of beef and melted cheese sandwiched between two glazed doughnuts". The Indiana vendor explained his impetus to sell the burger after seeing Paula Deen "[do] it on TV". The 800-calorie (3,300 kJ) burger was described as having a salty-sweet taste, and surprisingly ungreasy. The "Krispy Kreme Doughnut Burger" as served at the 2010 Mississippi State Fair was garnished with "lettuce, tomato and a generous slice of raw onion", and was estimated to provide 1,500 calories (6,300 kJ). The Alabama, Arizona, Louisiana, North Carolina, South Carolina, and Tulsa State Fairs also served up Luther Burger variants in 2010.
Epic Burgers and Waffles (EBW) introduced their doughnut cheeseburger to the 2011 Canadian National Exhibition; they were inspired after seeing it at fairs in the Southern United States and noting its popularity at the Calgary Stampede the month before. EBW's version cost C$8 (or C$10 to add bacon and a fried egg), and contained 1,500 calories (6,300 kJ).


== See also ==

Bacon Explosion
List of hamburgers
List of American sandwiches
List of sandwiches


== References ==


== External links ==
 Media related to Luther Burger at Wikimedia Commons