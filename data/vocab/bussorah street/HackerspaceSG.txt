HackerspaceSG is a 1,202-square-foot (111.7 m2) community center and hackerspace in Singapore. Predominantly an open working space for software projects, HackerspaceSG hosts a range of events from technology classes to biology, computer hardware, and manufacturing and is open to all types of hackers.


== Organization ==
HackerspaceSG is run mostly democratically by its membership, under the oversight of four directors. Anybody can become a member, and student rates are available. HackerspaceSG is primarily financed through membership dues, but is open to 3rd party sponsorships to fund expansions and renovations.


== Culture ==
HackerspaceSG is entirely communal space, from the tools in the electronics lab, to the desks, to the food in the refrigerator. Anything left in the space is considered fair game for anybody to play with. Very few restrictions are placed upon people, as long as they do not detract from the experience of members or consume resources they do not replace. Any member may host an event in the space.


== Physical Space ==
HackerspaceSG was originally located at 70A Bussorah Street in Singapore. The 2-year lease was extended once, but in October 2013, the landlord expressed a desire to develop the Bussorah Street unit into two distinct units. This meant that members would have had to endure dust and noise, and it was unlikely that the units would be leased back to HackerspaceSG at existing rates when the lease was renewed.
It was then decided that HackerspaceSG would move out. A search for a new location commenced in the months of October and November, with the membership finally settling on 344B King George's Avenue. A lease was signed on November 11, 2013, and move-in occurred on the next day.


== Uses ==
The three primary uses of HackerspaceSG are for events, as a coworking space, and for social purposes. The intersection of these three uses are what HackerspaceSG hopes will make it consistently a nice place to be, a good place to meet people, and a place to be challenged intellectually.


=== Events ===
HackerspaceSG hosts tech meetups and a monthly Hack and Tell. Members can host events at HackerspaceSG free of charge, subject to approval from the HackerspaceSG events committee.


=== Coworking ===
A number of Singapore startups and independent developers work daily out of HackerspaceSG as their primary location.


=== Notable Businesses With HackerspaceSG History ===
JFDI.Asia
Viki


=== Social ===
HackerspaceSG also has movie and game nights.


== References ==


== External links ==
Hackerspace.SG
"The official HackerspaceSG Google Group"
"What do Seattle, Silicon Valley, Berlin and Cambridge (both Cambridges) Have In Common?". e27. October 14, 2009
"Co-Workers of the World, Unite at Hackerspace.SG". Tech in Asia. October 16, 2009
"Developing a 'hacker' culture of ideas, innovation". ZDNet. November 9, 2010.
"High-tech tinkerers on the rise" The Straits Times. May 5, 2011.