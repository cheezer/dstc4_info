This is a list of butter dishes in which butter is used as a primary ingredient or as a significant component of a dish or a food. Butter is a dairy product that consists of butterfat, milk proteins, and water. It is made by churning fresh or fermented cream or milk.


== Butter dishes and foods ==

Beurre noir
Bread and butter pudding
Butter cake
Butter chicken
Butter cookie
Butter Pecan
Butter pie
Butter salt
Butter tart
Butter tea
Butterbrot
Buttercream
Butterkist
Butterkuchen
Buttermilk pie
Butterscotch
Buttery (bread)
Chicken Kiev
Compound butter
Croissant
Danish pastry
Egg butter
Garlic butter
Gooey butter cake
Hard sauce
Karelian pasty
Linzer torte
Pain aux raisins
Puff pastry
Remonce
Torpedo dessert


== See also ==

List of butters
List of dairy products
List of pastries
Mound of Butter (Vollon) – famous painting depicting butter


== References ==