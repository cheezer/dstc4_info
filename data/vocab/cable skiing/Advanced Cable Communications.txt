Advanced Cable Communications is a division of Schurz Communications providing cable television, cable telephone, DVR, and broadband services to customers in the Florida communities of Coral Springs and Weston. Previously named Coral Springs Cablevision, the company was purchased by Schurz in 1978 who changed the name to Advanced Cable Communications.


== References ==
^ "About us" section of corporate website
^ Google Finance profile for Advanced Cable Communications