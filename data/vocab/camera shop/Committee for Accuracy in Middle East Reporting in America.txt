The Committee for Accuracy in Middle East Reporting in America, or CAMERA, is an American non-profit pro-Israel media-monitoring, research and membership organization. According to its website, CAMERA is "devoted to promoting accurate and balanced coverage of Israel and the Middle East." The group says it was founded in 1982 "to respond to the Washington Post‍ '​s coverage of Israel's Lebanon incursion", and to respond to what it considers the media's "general anti-Israel bias".
CAMERA is known for its pro-Israel media monitoring and advocacy. CAMERA releases reports to counter what it calls "frequently inaccurate and skewed characterizations of Israel and of events in the Middle East" that it believes may fuel anti-Israel and anti-Jewish prejudice. The group mobilizes protests against what it describes as unfair media coverage by issuing full-page ads in newspapers, organizing demonstrations, and encouraging sponsors to withhold funds. CAMERA has over 65,000 paying members and states that 46 news outlets have issued corrections based on their work.


== History ==
CAMERA has chapters in major US cities and Israel, including New York City, Chicago, Washington, D.C., Los Angeles, Miami, and in 1988 a Boston chapter and headquarters, founded and led by Andrea Levin; Charles Jacobs became deputy director of the Boston chapter.
In 1991, Levin succeeded Winifred Meiselman as executive director of CAMERA. According to the organization's website, CAMERA's membership grew from 1,000 in 1991 to 55,000 in 2007 The director of the Washington office of CAMERA is Eric Rozenman.
In 2002, The Jewish Daily Forward named CAMERA executive director and regular Jerusalem Post contributor Andrea Levin America's fifth most influential Jewish citizen, saying "Media-monitoring was the great proxy war of the last year, and its general is Andrea Levin."
In 2008 CAMERA launched a campaign to alter Wikipedia articles to support the Israeli side of the Israeli–Palestinian conflict. The campaign suggested that pro-Israeli editors should pretend to be interested in other topics until elected as administrators. Once administrators they were to misuse their administrative powers to suppress pro-Palestinian editors and support pro-Israel editors. Some members of this conspiracy were banned by Wikipedia administrators.


== Structure, staff, and activities ==
On its official website, CAMERA describes itself as "a media-monitoring, research and membership organization devoted to promoting accurate and balanced coverage of Israel and the Middle East" which "fosters rigorous reporting, while educating news consumers about Middle East issues and the role of the media." CAMERA further describes itself as a "non-partisan organization" which "takes no position with regard to American or Israeli political issues or with regard to ultimate solutions to the Arab-Israeli conflict." CAMERA complained in 2008 that the Conference of Presidents of Major American Jewish Organizations (of which it is a member) did not consult it before disinviting Republican vice presidential candidate Sarah Palin to an anti-Ahmadinejad rally. CAMERA has also criticized the Israeli non-governmental organization B'Tselem for some of its reporting on Israel.
When CAMERA perceives an inaccurate statement in the media, it says it gathers information, and sends findings asking for a printed or broadcast correction. CAMERA lists 46 news outlets which it says have issued corrections based on their work. The organization also publishes monographs about topics relating to the Arab-Israeli conflict. The Jerusalem Center for Public Affairs website says CAMERA has 55,000 paying members and thousands of active letter writers.
CAMERA is a member of the Israel Campus Roundtable, which includes the Anti-Defamation League, The David Project Center for Jewish Leadership, and other pro-Israel organizations. As a member of this Campus Roundtable, CAMERA operates on college campuses to combat what it perceives as "propagandistic assaults on Israel . . . creating harmful misperceptions of Israel" and is active on about 50 college campuses.
CAMERA runs a student-focused site, CAMERAonCampus.org, containing specialized information available for countering misinformation, providing news about events on campuses, and with information on people, films, and books that deal with the conflict. CAMERA also provides one-on-one assistance to students who encounter Middle East distortions in campus publications, flyers, rallies and classroom teaching, which can be read about on CAMERA's campus blog In Focus. The campus department of CAMERA consists of the Campus Director, Aviva Slomich, the Senior Campus Coordinator Samantha Mandeles, and the Campus Regional Coordinator Gilad Skolnick, who also serves as the editor in chief of CAMERAOnCampus.org. For undergraduates willing to write op-eds and organize pro-Israel events on campus, CAMERA has offered student representative positions which include compensation and training in Israel.


== Criticism by CAMERA ==

Among the organizations and works that have been criticized by CAMERA are:


=== ABC News anchor Peter Jennings ===
"We've long considered him anti-Israel", CAMERA's founder Andrea Levin has commented of Peter Jennings, after an incident in which CAMERA, and eventually also the Columbia Journalism Review, took issue with Jennings and ABC News for refusing to correct an alleged misquote of Benjamin Netanyahu.


=== National Public Radio ===
CAMERA's report, "A Record of Bias: National Public Radio's Coverage of the Arab-Israeli Conflict: September 26 – November 26, 2000" (2001) asserted that National Public Radio's "coverage of the Arab-Israeli conflict has long been marred by a striking anti-Israel tilt, with severe bias, error and lack of balance commonplace." CAMERA supported a boycott of NPR, and demanded the firing of NPR's foreign editor, Loren Jenkins. CAMERA said that Jenkins had a long record of partisanship in favor of Palestinian views, and let his personal views tilt NPR's coverage. CAMERA also said Jenkins compared Israel to Nazi Germany in his writings, and referred to it as a "colonizer".
NPR's then-Ombudsman, Jeffrey Dvorkin, said in a 2002 interview that CAMERA used selective citations and subjective definitions of what it considers pro-Palestinian bias in formulating its findings, and that he felt CAMERA's campaign was "a kind of McCarthyism, frankly, that bashes us and causes people to question our commitment to doing this story fairly. And it exacerbates the legitimate anxieties of many in the Jewish community about the survival of Israel."


=== Encarta ===
In an article originally published in the Jerusalem Post and posted on the official website of CAMERA, Andrea Levin, the Executive Director of CAMERA, describes Microsoft's digital multimedia encyclopedia Encarta as "a troubling mix of solid information, bias and error." In particular, Levin points to the articles written by Shaul Cohen of the University of Oregon, which Levin says "blurs Arab aggression against the Jews from the Mandate period to the present, repeatedly equating the violence by the parties."


=== Steven Spielberg's film Munich ===
In her film review of Munich (2005), posted on the official website of CAMERA, Andrea Levin states that the film (a collaboration of director Steven Spielberg and playwright–screenwriter Tony Kushner) promotes "its thesis of Israeli culpability" and that "Israel's action battling its adversaries is cast as aberrant, bloody and counterproductive." Levin continues: "indeed, it is stunning to watch Munich and realize that its director [Spielberg] brought Schindler's List to the world. Where that was artistry drawn from truth, Munich is cinematic manipulation rooted in lies."


=== Mearsheimer and Walt ===
CAMERA published Alex Safian's detailed critique of The Israel Lobby and U.S. Foreign Policy, a paper written by Harvard University professor Stephen Walt and University of Chicago professor John Mearsheimer. Safian argued that the paper is "riddled with errors of fact, logic and omission, has inaccurate citations, displays extremely poor judgement regarding sources, and, contrary to basic scholarly standards, ignores previous serious work on the subject. The bottom line: virtually every word and argument is, or ought to be, in 'serious dispute.' In other words, a student who submitted such a paper would flunk."


=== God's Warriors ===
CAMERA published a critique of Christiane Amanpour's CNN documentary series God's Warriors, calling it "one of the most grossly distorted programs to appear on mainstream American television", "false in its basic premise", and "a perfect illustration of classical propaganda techniques". Amanpour has responded that the documentary is not meant to compare religions, but rather to show "that each faith has their committed and fervent believers, and we're showing how each of those are active in the political sphere in today's world."


=== "Israel's Jewish Defamers" ===
In October 2007, CAMERA organized a conference entitled "Israel's Jewish Defamers," in which a panel of discussants accused selected Jewish critics of Israel, as well as one of Israel's leading newspapers, Haaretz, of distortions and falsehoods about Israel. CAMERA director Andrea Levin described the Jewish critics — who included Princeton University's Richard Falk, writer Norman Finkelstein, New York Review of Books contributor Henry Siegman, former New York Times columnist Anthony Lewis, Trent University professor Michael Neumann, and Tikkun magazine publisher Michael Lerner — of being guilty of "demonstrably false and baseless defaming of Israel, wildly distorted out of context accusations against Israel." Among the panelists were writer Cynthia Ozick and Harvard psychiatrist Kenneth Levin, who likened the Jewish critics to chronically abused children.
In response, Anthony Lewis told the New York Sun that the conference was "about a nonexistent phenomenon," noting that Jewish criticism of Israeli policies was not necessarily defamatory. Haaretz's editor-in-chief, David Landau, refused to comment on the conference, citing that "it was "a matter of policy and principle" not to respond to CAMERA, which Landau described as "McCarthyite." Tikkun editor Lerner also rejected the notion that he was anti-Israel.


=== 2008–2009 Israel–Gaza conflict ===
In response to coverage of the 2008–2009 Gaza War, CAMERA criticized the reporting of the Los Angeles Times, CNN, Ha'aretz, NPR, the U.N. Relief and Works Agency, and Norwegian doctors being funded by the Norwegian ministry who appeared on media outlets such as the BBC and CBS. CAMERA said that in its view reporting from the Los Angeles Times "consistently omitted key information about Gaza Strip sites targeted by the Israeli army" and "gives the false impression that Israel doesn't aim to prevent civilian deaths". CAMERA criticized Ha'aretz for "confusion and misinformation on the medical issue" in its "outlining what medical supplies Gaza is reportedly lacking and ignoring all incoming medical aid".


=== Quotation misattributed to Moshe Ya'alon ===
In early 2009 CAMERA began investigating the dissemination of a quotation widely misattributed to Moshe Ya'alon, "The Palestinians must be made to understand in the deepest recesses of their consciousness that they are a defeated people", after the quotation was cited by Rashid Khalidi, a Columbia University professor, in an op-ed article in the New York Times. The quotation, and variants, had previously been repeated throughout the world by news broadcasts, blogs, and in reputable publications such as the Chicago Tribune, Boston Globe, Toronto Star, Time Magazine, and the London Review of Books. The belief that Ya'alon, a high-ranking Israeli military official, had expressed disregard for Palestinian people in this manner reinforced an opinion among some readers and commentators that Israel was the aggressor and Palestinians its victims. However, Ya'alon did not make the statement in the 2002 Haaretz interview generally cited as its source, and appears never to have made the statement at all. In part due to CAMERA's campaign, a number of international newspapers, including the New York Times, issued corrections.


=== The New York Times‍ '​ coverage of the Palestinian-Israeli conflict ===
In a 2012 monograph of its study of The New York Times, CAMERA asserts that the newspaper shows a clear preference for the Palestinian narrative. It further says that The New York Times treats Israel with a harsher standard and omits context. The study called "Indicting Israel - New York Times Coverage of the Palestinian-Israeli Conflict - A July 1 – December 31, 2011 Study" is part of the Monograph Series. In the executive summary senior CAMERA research analysts Ricki Hollander and Gilead Ini, say that the dominant finding of the study was a disproportionate, continuous, embedded indictment of Israel that dominated both news and commentary sections. It further states that "Israeli views are downplayed while Palestinian perspectives, especially criticism of Israel, are amplified and even promoted." According to Rick Richman, writing in Commentary Magazine, the CAMERA study examines all news and editorial sections in the print version of the newspaper directly relating to the Palestinian-Israeli conflict (july 1 - december 31, 2011).


== Reception to CAMERA ==
CAMERA has attracted both critics and supporters. Gershom Gorenberg, a journalist for The American Prospect, has written that CAMERA is "Orwellian-named" and that "like others engaged in the narrative wars, it does not understand the difference between advocacy and accuracy". Other critics have described CAMERA as a special interest group fighting for a pro-Israeli bias. Holocaust survivor Eli Wiesel, US Senator Joe Lieberman, Harvard Law Professor Alan Dershowitz, and former Israeli cabinet minister Natan Sharansky have assisted CAMERA in its fund raising efforts, by speaking at their national conference. Richard Landes, an Associate Professor at Boston University, said "their work is that they are careful both to reason and cite sources scrupulously" and that those who dismiss their work "rely on a dismissal that is at least as partisan as that with which it charges others." Robin Shepherd has described them as a reputable institution. U.S. Representative Tom Lantos was previously on CAMERA's advisory board.


=== Boston Globe ===
In a 2003 profile of the organization in the Boston Globe, Mark Jurkowitz observed:

"To its supporters, CAMERA is figuratively - and perhaps literally - doing God's work, battling insidious anti-Israeli bias in the media. But its detractors see CAMERA as a myopic and vindictive special interest group trying to muscle its views into media coverage. ... To many in the media CAMERA is ... an advocacy group trying to impose its pro-Israeli views on mainstream journalism."


=== Washington Report on Middle East Affairs ===
Mitchell Kaidy, writing in the Washington Report on Middle East Affairs, criticized CAMERA's efforts to pressure university libraries to remove books that the organization finds offensive. CAMERA has criticized the organization as being "virulently anti-Israel".


=== Journalists ===
Writing in The Nation in 1987, journalist and author Robert I. Friedman described CAMERA as having been formed in the wake of Israel's 1982 invasion of Lebanon "to keep the U.S. press in line," noting that the organization's activities at the time included publishing a newsletter and placing advertisements in The Christian Science Monitor and The New Republic in support of Israel's West Bank settlement policies. According to Friedman, "CAMERA, the A.D.L., AIPAC and the rest of the lobby don't want fairness, but bias in their favor. And they are prepared to use McCarthyite tactics, as well as the power and money of pro-Israel PACs, to get whatever Israel wants."
In his 2006 book Public Editor #1, former New York Times public editor Daniel Okrent expressed gratitude to CAMERA as a notable example of organizations that "maintained an evenness of tone and an openness of communication no matter how much they disagreed" with his columns.
Writing about criticisms from CAMERA he and his colleagues have received, Jerusalem-based journalist Gershom Gorenberg wrote "It is not the press's job to provide PR for any government. Until CAMERA gets this straight, self-respecting journalists will regard an occasional snarl from the watchdog as proof that they're doing their job."
Ian Mayes, president of the Organisation of News Ombudsmen in Britain, wrote in 2006 that to him, methods employed by CAMERA seemed to go beyond reasonable calls for accountability. He mentioned CAMERA's campaign against the Middle East reporting of National Public Radio, in which he says CAMERA had attempted to influence NPR's supporters to withhold funds.


=== The Jewish Journal of Greater Los Angeles ===
Writing about attempts by CAMERA to get a local Pasadena, California church to cancel an appearance by Palestinian activist Reverend Naim Ateek, Rob Eshman, Editor-in-Chief of The Jewish Journal of Greater Los Angeles, wrote "I'm always leery when Jewish groups ride in from out of town to try to save us from the bad guys. We have plenty of sharp-eyed Jewish defense groups locally who can tussle on our behalf. It's just a bit condescending to think we rubes, out in America's second-largest Jewish city, don't know how and when to fight. Or whom." Eshman later clarified that his criticism was directed specifically at CAMERA's handling of the Ateek visit, and not toward the organization in general. "I think CAMERA, which in so many cases I find useful and correct, is in this case making things worse," he wrote.


=== Center for Middle Eastern Studies ===
In 2005, Donald Wagner, Executive Director of the defunct Center for Middle Eastern Studies and Associate Professor of Religion and Middle Eastern Studies at Northpark University, characterized the organization as "a well-known source of extremist pro-Israel propaganda that is routinely challenged by Israeli and international human rights and peace organizations for its consistent misrepresentation of the facts in the Israeli-Palestinian conflict."


=== The Atlanta Journal-Constitution ===
The Atlanta Journal-Constitution described the place CAMERA took in a debate among various Jewish groups about statements made by former U.S. President Jimmy Carter. Rabbi Marvin Hier, founder of the Simon Wiesenthal Center, said it was not right to outright dismiss his apology. CAMERA said true repentance required Carter to reverse any of the perceived harm he caused, and called on the president to take "concrete actions to redress troubling false statements" the group said he made about the war Israel waged in Gaza. Ira Forman, chief executive of the Washington-based National Jewish Democratic Council, said it was "mensch-like" to accept and encourage Carter's remarks.


=== Academia ===
In 1986, Florida International University political science professor Cheryl A. Rubenberg noted CAMERA was "Another pro-Israeli organization that was formed after 1982 to monitor the media..." She further stated that CAMERA was one of several 'new groups' which constituted the "Israeli lobby" at the time.
In 1988, Edward Said, a political activist and Professor of English and Comparative Literature at Columbia University, argued that not even the Israeli government has ventured arguments as extreme as CAMERA, and that "surely, the Israeli lobby can find better propaganda methods than this!"
Robin Shepherd, Director of International Affairs at the Henry Jackson Society, describes CAMERA as one of "several reputable institutions monitoring [media] trends closely."
In 2010, Richard Landes, a political activist and professor of history at Boston University argued that CAMERA is "careful both to reason and cite sources scrupulously" and that those who dismiss their work "rely on a dismissal that is at least as partisan as that with which it charges others.


== CAMERA campaign in Wikipedia ==
In an April 2008 article, the pro-Palestinian online publication Electronic Intifada revealed the existence of a Google group set up by CAMERA. The stated purpose of the group was "help[ing] us keep Israel-related entries on Wikipedia from becoming tainted by anti-Israel editors". Electronic Intifada accused CAMERA of "orchestrating a secret, long-term campaign to infiltrate the popular online encyclopedia Wikipedia to rewrite Palestinian history, pass off crude propaganda as fact, and take over Wikipedia administrative structures to ensure these changes go either undetected or unchallenged". Andre Oboler, a Legacy Heritage Fellow at the Israeli non-governmental organization NGO Monitor, responded that "Electronic Intifada is manufacturing a story."
Excerpts of some of the e-mails were published in the July 2008 issue of Harper's Magazine under the title of "Candid camera." In April 2008, CAMERA's "Senior Research Analyst" Gilead Ini would not confirm that the messages were genuine but maintained that there was a CAMERA email campaign which adhered to Wikipedia's rules. In August 2008, Ini argued the excerpts published in Harper's Magazine were unrepresentative and that CAMERA had campaigned "toward encouraging people to learn about and edit the online encyclopedia for accuracy".
A group of Wikipedia administrators strongly believed an editor on Wikipedia to be Gilead Ini and blocked that user account indefinitely. In April 2008 Gilead refused to say whether he was behind the Gni account, and in May 2008 he denied that the account belonged to him. Andre Oboler alleged that groups such as "Wikipedians for Palestine" have engaged in similar practices. Electronic Intifada co-founder Ali Abunimah insisted that his group would never encourage a similar e-mail campaign.
Commenting on the incident, Gershom Gorenberg, of the liberal magazine The American Prospect, stated "CAMERA is ready to exempt itself from the demands for accuracy that it aims at the media. And like others engaged in the narrative wars, it does not understand the difference between advocacy and accuracy." Gorenberg criticized CAMERA for telling members not to share information about the campaign with media, and he also argued Ini's definition of accuracy "only means not printing anything embarrassing to his own side". David Shamah, of The Jerusalem Post, stated that "the vast anti-Israel lobby that haters of our country have managed to pull together" hate it when groups like CAMERA mess up "their anti-Israel propaganda with (gasp!) facts".
Five editors involved in the campaign were sanctioned by Wikipedia administrators, who wrote that Wikipedia's open nature "is fundamentally incompatible with the creation of a private group to surreptitiously coordinate editing".


== Notes ==


== See also ==
Media coverage of the Arab-Israeli conflict
Facts and Logic About the Middle East (FLAME)
HonestReporting media watchdog "dedicated to defending Israel against prejudice in the Media"
If Americans Knew, a US-based media-monitoring organization which covers much the same issues, but from an opposing viewpoint.
The Jewish Internet Defense Force


== External links ==
CAMERA Official website.
"CAMERA: Fighting Distorted Media Coverage of Israel and the Middle East: An Interview with Andrea Levin" (Exec. Dir. of CAMERA) at Jerusalem Center for Public Affairs.
CAMERA at Nuclear Spin (part of SpinWatch, another media watchdog group which identifies itself as "monitoring PR and Spin").
The David Project Center for Jewish Leadership Official website.
FAIR: Region: Middle East Hyperlinked list of items headed CounterSpin, Media Advisory, Activism Update, Extra!
Online Home of the Project for Excellence in Journalism and the Committee for Concerned Journalists.
[7]