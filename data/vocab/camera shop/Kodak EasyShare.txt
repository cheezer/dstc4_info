Kodak EasyShare is a sub brand of Eastman Kodak Company products identifying a consumer photography system of digital cameras, snapshot thermal printers, snapshot thermal printer docks, all-in-one inkjet printers, accessories, camera docks, software, and online print services. The brand was introduced in 2001. The brand is no longer applied to all-in-one inkjet printers (now branded "ESP") or online printing services (now simply "Kodak Gallery"). Thermal snapshot printers and printer docks product lines have been discontinued.


== EasyShare Digital Cameras ==
There are presently three EasyShare camera lines, "series", that separate the cameras into different classes: EasyShare Point and Shoot (C series), EasyShare Performance(Z series), and EasyShare Sleek & Stylish (M-Series). The original products to use the EasyShare brand were the DX3600 and DX3500 digital camera along with the EasyShare Camera Dock.


=== Kodak EasyShare DX-Series ===
The DX series cameras were the first EasyShare models released. It was originally a very basic point and shoot camera series, compatible with the original EasyShare Camera Dock. The CX series eventually replaced the lower-end DX models, and the newer DX-Series models had more advanced features and higher megapixel resolution and zoom features. The DX series is now discontinued; the higher-end DX Series models eventually became the Z-Series. Models in the DX series were the last Kodak consumer digital cameras to use CompactFlash external memory cards. Models include the DX3215, DX3500(2.2 mp, 38 mm zoom lens), DX3600 (2.2 mp, 35-70mm zoom lens), DX3900 (3.3 mp, 35–70 mm zoom lens), DX4330 (3.1 mp, 38–114 mm zoom lens), Kodak EasyShare DX4530(5.2 mp, 38-114mm zoom lens), DX4900 (4.1 mp, 35-70mm zoom lens), Kodak EasyShare DX6440 (4.23 mp, 33–132 mm zoom lens), Kodak EasyShare DX6490 (4.23 mp, 38–380 mm zoom lens), DX7440 (4.0 mp, 33–132 mm zoom lens), Kodak Zoom Digital Camera DX7590 (5.0 mp, 38–380 mm zoom lens) and the DX7630 (6.2 mp, 39–117 mm zoom lens).


=== Kodak EasyShare CX-Series ===
The CX series is now discontinued, replaced by the C series. The CX series grew out of the DX series. At the time, it was the range of the lowest-priced, most basic point and shoot cameras, typically with no more than a 3× optical zoom.


=== Kodak EasyShare C-Series ===
The C-series is Kodak's current line of lower-priced, point and shoot, entry-level digital cameras.


=== Kodak EasyShare Z-Series ===
The Z-series is the current high-zoom and performance-oriented range of consumer digital cameras, replacing the original DX series. Typically, Z-Series cameras have higher optical zooms than any other series. The highest optical zoom camera offered by Kodak is the Z990  with a 30X Optical Zoom.


=== Kodak EasyShare V-Series ===
The V-Series was another style-oriented range of consumer digital cameras, replacing the original LS series. V-Series had a number of innovations, such as dual-lens technology, first introduced with the V570. The V-Series line has now discontinued, superseded by higher-end M-Series cameras.


=== Kodak EasyShare P-Series ===
The P-Series was Kodak's "Performance" series intended to bring DSLR-like features to a consumer model. The series is now discontinued, superseded by higher-end Z-Series models. These were the only consumer models to leverage an external flash, with the exception of the Z980.


=== Kodak EasyShare-One Series ===
The EasyShare-One series were the first consumer Wi-Fi Digital Cameras, that used an SDIO card to automatically upload pictures in wi-fi hotspots. The EasyShare-One series is now discontinued.


=== Kodak EasyShare M-Series ===
The EasyShare  were originally a blend between thinner point-and-shoot cameras (C series) and stylish cameras (V series), now positioned as "Sleek and Stylish" with the discontinuance of the V-Series. They are usually available in a variety of colors and generally have features not available in the C-Series line.
To promote the M-Series, which features the exclusive share button for social network media sharing, Kodak announced their "So Kodak" marketing campaign. To appeal to young and socially connected consumers, the campaign features urban artists Drake, Pitbull and Trey Songz.


== EasyShare Digital Frames ==


=== Kodak EasyShare SV-Series ===

The original line of digital frames that played pictures and videos (replaced by M-Series).


=== Kodak EasyShare EX-Series ===
The original line of digital frames that included the features of SV-Series frames but included wireless (Wi-Fi) capabilities (replaced by W-Series).


=== Kodak EasyShare M-Series ===
The M-series line of "multimedia" digital frames play pictures and videos.


=== Kodak EasyShare W-Series ===
The W-series line of "wireless" digital frames features Wi-Fi connectivity to a home computer or the internet.


=== Kodak EasyShare D-Series ===
The D-series line of "decor" digital frames allow mounting with any off-the-shelf standard 8x10 frame.


=== Kodak EasyShare S-Series ===
The S-series currently designates digital frames that are "cordless" in that they have a rechargeable battery allowing viewing without a power cable for several hours. There was a much older frame, the S510, that was not cordless and predated the P-Series.


=== Kodak EasyShare P-Series ===
The P-series line of digital frames stands for "Photo"; these frames can only be used for pictures and not multimedia.


== Other products ==
The EasyShare brand also was incorporated with the original 5000-series all-in-one inkjet printers (superseded by the ESP line), thermal photo printers and printer docks (now discontinued), and camera docks.


== EasyShare Software ==


=== EasyShare Software ===
Kodak EasyShare software is used to transfer and catalog images from EasyShare camera models and can also be used with existing images (in .gif, .png, .jpg, or .tiff format) and non-Kodak digital cameras. The most recent version of Kodak EasyShare software is version 8.3, which includes support for Windows 7. Included in the latest versions is the ability to upload pictures and videos to Facebook, YouTube and Kodak Gallery. Other features include the ability to rate, tag, and caption pictures (using industry tagging standards on the files themselves), online print ordering facilities, photo enhancement and alteration capabilities, and home printing page layout control.
As of September 5, 2012, the software is no longer available for download from Kodak's support website.
The updater component of the software is powered by BackWeb, which, though usually suspect for its spyware associations, is said to be benign in this case.
In version 6 of EasyShare software, the Bonjour software component from Apple is installed for remote service discovery, but serves no useful purpose for older models; it can be removed using "Add & remove programs" without impeding functionality.


=== EasyShare Custom Creations ===
This software, powered by RocketLife, is now discontinued. It was a desktop application that allowed the user to create a personalized gift (ex. Photo Book) and burn the applicable files to a CD. Ordering and fulfillment was handled by dropping the CD off at a retailer.


== References ==


== External links ==
 Media related to Kodak EasyShare cameras at Wikimedia Commons
Official Kodak page for Easyshare cameras
Kodak.com