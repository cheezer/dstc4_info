A cercle is the second level administrative unit in Mali. Mali is divided into eight régions and one capital district (Bamako); the régions are subdivided into 49 cercles. These subdivisions bear the name of their principal city.
During French colonial rule in Mali, a cercle was the smallest unit of French political administration that was headed by a European officer. A cercle consisted of several cantons, each of which in turn consisted of several villages. In 1887 the Cercle of Bafoulabé was the first cercle to be created in Mali. In most of former French West Africa, the term cercle was changed to Prefecture or Department after independence, but this was not done in Mali.
Some cercles (and the district) were, prior to the 1999 local government reorganisation, further divided into Arrondissements, especially in urban areas or the vast northern regions (such as Kidal), which consisted of a collection of Communes. Since these reforms, cercles are now directly subdivided into rural and urban communes, which in turn are divided in Quartiers (Quarters, or Villages and encampments in rural areas) which have elected councils at each level. There are 703 communes, 36 urban communes (including 6 in Bamako District) and 667 rural communes. The cercles are listed below.


== Bamako Capital District ==

Bamako


== Gao Region ==

Ansongo Cercle
Bourem Cercle
Gao Cercle
Menaka Cercle


== Kayes Region ==

Bafoulabé Cercle
Diema Cercle
Kita Cercle
Kéniéba Cercle
Kayes Cercle
Nioro du Sahel Cercle
Yélimané Cercle


== Kidal Region ==

Abeibara Cercle
Kidal Cercle
Tessalit Cercle
Tin-Essako Cercle


== Koulikoro Region ==

Banamba Cercle
Dioila Cercle
Kangaba Cercle
Koulikoro Cercle
Kolokani Cercle
Kati Cercle
Nara Cercle


== Mopti Region ==

Bandiagara Cercle
Bankass Cercle
Djenné Cercle
Douentza Cercle
Koro Cercle
Mopti Cercle
Tenenkou Cercle
Youwarou Cercle


== Ségou Region ==

Bla Cercle
Barouéli Cercle
Macina Cercle
Niono Cercle
Ségou Cercle
San Cercle
Tominian Cercle


== Sikasso Region ==

Bougouni Cercle
Kolondieba Cercle
Kadiolo Cercle
Koutiala Cercle
Sikasso Cercle
Yanfolila Cercle
Yorosso Cercle


== Tombouctou Region ==

Diré Cercle
Goundam Cercle
Gourma-Rharous Cercle
Niafunke Cercle
Timbuktu Cercle


== See also ==
Arrondissements of Mali
Regions of Mali


== References ==

MATCL - Ministère de l'Administration Territoriale et des Collectivités Locales: government of the Republic Mali.
Mali
Maplibrary: vector maps of national subdivisions of Mali.
Regions, Cercles and Places in Mali, African Development Information Services Database. Contains listing of Arrondissements under each Cercle page, as well as some Communes and places of interest in each Cercle.


=== Colonial usage ===
Benton, Lauren: Colonial Law and Cultural Difference: Jurisdictional Politics and the Formation of the Colonial State in Comparative Studies in Society and History, Vol. 41, No. 3 (Jul., 1999)
Crowder, Michael: West Africa Under Colonial Rule Northwestern Univ. Press (1968) ASIN: B000NUU584
Crowder, Michael: Indirect Rule: French and British Style Africa: Journal of the International African Institute, Vol. 34, No. 3 (Jul., 1964)
Mortimer, Edward France and the Africans, 1944–1960, A Political History (1970)
Jean Suret-Canele. French Colonialism in Tropical Africa 1900-1945. Trans. Pica Press (1971)