Cantonment Road redirects here. For Cantonment Road, Penang, see Street names of George Town, Penang.

Cantonment Road (Chinese: 广东明路) is a road located in Tanjong Pagar on the boundary between Bukit Merah Planning Area, Outram Planning Area and the Downtown Core in Singapore.
The road starts at its junction with Outram Road, Eu Tong Sen Street and New Bridge Road in the north and ends at its junction with Keppel Road in the south. It is intersected by the arterial Neil Road.
Namesake roads include Cantonment Link, a one-way road which connects Keppel Road to Cantonment Road, and Cantonment Close.
The Police Cantonment Complex is located along the road.


== Etymology and history ==
The area of Cantonment Road was the site assigned by Stamford Raffles in 1819 in his directions to Major William Farquhar for barracks for the East India Company's Sepoy troops. The contingent of Indian sepoys stationed here had accompanied Raffles to Singapore and were asked to stay. Cantonment refers to a group of lodgings assigned to troops, hence the name of the road. British troops were "cantoned" here between 1824 and 1858. Prior to 1853, Outram Road was also part of Cantonment Road.
The Cantonese refer to the road as ba suo wei or pa so bue, meaning "end of Bukit Pasoh".
Raffles intended this area for hospitals, magazines, barracks of the army, houses for civil and military offices of the East India Company, as well as godowns and offices for the government. In 1822, however, Raffles found that his vision for the Cantonment Plain was violated. The space reserved exclusively for public purposes was occupied by European merchants in masonry buildings.
A landmark on Cantonment Road is the Sri Manmatha Karunya Eswarar Temple, located near the Tanjong Pagar Railway Station, which had its origins in the 1940s as a modest wooden shed-like structure set up to serve the large Hindu community working in and living near the shipyards. It was rebuilt in 1987.


== See also ==
Police Cantonment Complex


== References ==
Victor R Savage, Brenda S A Yeoh (2004), Toponymics - A Study of Singapore Street Names, Eastern University Press, ISBN 981-210-364-3
National Heritage Board (2002), Singapore's 100 Historic Places, Archipelago Press, ISBN 981-4068-23-3