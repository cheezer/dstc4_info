The Ford Capri is a fastback coupé that was built by Ford Motor Company from 1969 to 1986, It was designed by American Philip T. Clark, famous for being one of the main designers of the Ford Mustang, which until 2010 was an unknown fact. Using the mechanical components from the Ford Cortina and intended as the European equivalent of the Ford Mustang for the European markets, the Capri went on to be a highly successful car for Ford, and sold nearly 1.9 million units in its lifetime. A wide variety of engines were used in the Capri, throughout its production lifespan, which most notably included the Essex and Cologne V6 s at the top of the range, whilst the Kent straight-four and Taunus V4 engines were used in lower specification models. Although the Capri was not officially replaced by any Ford model, the second-generation Probe was effectively its replacement after the later car's introduction to the European market in 1992.


== History ==


=== Ford Capri Mk I (1969–1974) ===
The first Ford Capri was introduced in January 1969 at the Brussels Motor Show, with sales starting the following month. The intention was to reproduce in Europe the success Ford had had with the North American Ford Mustang; to produce a European pony car. It was mechanically based on the Cortina and built in Europe at the Dagenham and Halewood plants in the United Kingdom, the Genk plant in Belgium, and the Saarlouis and Cologne plants in Germany. The car was named Colt during development stage, but Ford was unable to use the name, as it was trademarked by Mitsubishi.
Although a fastback coupé, Ford wanted the Capri Mk I to be affordable for a broad spectrum of potential buyers. To help achieve that, it was available with a variety of engines. The British and German factories produced different line-ups. The continental model used the Ford Taunus V4 engine in 1.3, 1.5 and 1.7 L engine displacement, while the British versions were powered by the Ford Kent straight-4 in 1.3 and 1.6 L form. The Ford Essex V4 engine 2.0 L (British built) and Cologne V6 2.0 L (German built) served as initial range-toppers. At the end of the year, new sports versions were added: the 2300 GT in Germany, using a double-barrel carburettor with 125 PS (92 kW), and in September 1969 the 3000 GT in the UK, with the Essex V6, capable of 138 hp (103 kW).
Under the new body, the running gear would have been familiar to anyone used to working on the underside of a 1966 Cortina. Rear suspension employed a live axle supported on leaf springs with short radius rods. MacPherson struts featured at the front in combination with rack and pinion steering which employed a steering column that would collapse in response to a collision.
The initial reception of the car was broadly favourable. In the June 1970 edition of the Monthly Driver's Gazette, tester Archie Vicar wrote of the gearchange that it was "...in Ford fashion easy to operate but not very jolly". In the same review Vicar summed up the car as follows: "Perhaps with a bit of work it can be given road-holding and performance less like an American car and more like a European one".
The range continued to be broadened, with another 3.0 variant, the Capri 3000E introduced from the British plant in March 1970, offering "more luxurious interior trim".
Ford began selling the Capri in the Australian market in May 1969 and in April 1970 it was released in the North American and South African markets. These versions all used the underpowered Kent 1.6 engine although a Pinto straight-four 2.0 L replaced it in some markets in 1971. An exception, though, was the Perana manufactured by Basil Green Motors near Johannesburg, which was powered by a 302ci V-8 Ford Windsor engine. All North American versions featured the "power dome" hood and four round 53⁄4" US-spec headlights. They carried no "Ford" badging, as the Capri was sold by only Mercury dealers and promoted to US drivers as "the sexy European".

The Capri was sold in Japan with both the 1.6 L and 2.0 L engines in GT trim, and sales were helped by the fact that this generation was compliant with Japanese Government dimension regulations. Sales were handled in Japan by Kintetsu Motors which is an exclusive importer of Ford products to Japan. The 2.0 litre engine obligated Japanese owners to pay more annual road tax in comparison to the smaller 1.6 litre engine, which affected sales.
A new 2637 cc version of the Cologne V6 engine assembled by Weslake and featuring their special all alloy cylinder heads appeared in September 1971, powering the Capri RS2600. This model used Kugelfischer fuel injection to raise power to 150 PS (110 kW) and was the basis for the Group 2 RS2600 used in the European Touring Car Championship. The RS2600 also received modified suspension, a close ratio gearbox, lightened bodywork panels, ventilated disc brakes and aluminium wheels. It could hit 100 km/h from a standstill in 7.7 seconds. The 2.6 L engine was detuned in September for the deluxe version 2600 GT, with 2550 cc and a double-barrel Solex carburettor. Germany's Dieter Glemser won the Drivers' title in the 1971 European Touring Car Championship at the wheel of a Ford Köln entered RS2600 and fellow German Jochen Mass did likewise in 1972.
The first Ford Special, was the Capri Vista Orange Special. The Capri Special was launched in November 1971 and was based on the 1600 GT, and 2000 GT models. It was only available in Vista orange and was optional dealer fitted with a Ford Rally Sport (RS) boot mounted spoiler and rear window slats – a direct link to the Mustang. The Special also had some additional standard extras such as a push-button radio, fabric seat upholstery, inertia reel seat belts, heated rear screen and black vinyl roof. There were only 1200 Vista Orange Capri specials made. One of the last limited editions of the original Mk I, was a version that came in either Metallic Green or black with Red interior and featured some additional extras, such as cloth inserts in the seats, hazard lights, map reading light, opening rear windows, vinyl roof and for the first time a bonnet bulge was fitted to the sub-3.0-litre models. This special edition was only available with a 1.6 or 2.0 engines and had the full title of GTXLR Special.


==== Mk I facelift ====
The Capri proved highly successful, with 400,000 cars sold until 1970. Ford revised it in 1972. It received new and more comfortable suspension, enlarged tail-lights and new seats. Larger headlamps with separate indicators were also fitted, with quad headlamps now featured on the 3000GXL model. The Kent engines were replaced by the Ford Pinto engine and the previously UK-only 3000 GT joined the German line-up. In the UK the 2.0 L V4 remained in use.
In addition, North American versions received larger rubber-covered bumpers (to comply with US DOT regulations) for 1973.
1973, saw the highest sales total the Capri ever attained, at 233,000 vehicles: the 1,000,000th Capri, an RS 2600, was completed on 29 August.
In December, Ford replaced the Cologne V6 based RS2600 with the Essex V6-based RS3100, with the usual 3.0 L Essex V6's displacement increased to 3,098 cc (189.1 cu in) by boring the cylinders from the 93.6mm of the 3.0L to 95.2mm. Unlike its predecessor, it used the same double-barrel Weber carburetor as the standard 3.0 L, and reached the same 150 PS (110 kW) as the RS 2600. Only 250 RS3100s were built for homologation purposes. However, the car was still competitive in touring car racing, and Ford Motorsport produced a 100-model limited edition with this new engine. The Group 4 RS3100's engine was tuned by Cosworth into the GAA, with 3412 cc, fuel injection, DOHC, four valves per cylinder and 435 hp (324 kW) in racing trim. The car also featured improved aerodynamics. Besides the racing RS3100, the GAA was also used in Formula 5000.
The last Mk1 Capri was produced was produced strangely 8 months after the launch of the Capri II on October 31st 1974. 


=== Ford Capri Mk II – 'Capri II' (1974–1978) ===
In February 1974, the Capri II was introduced. After 1.2 million cars sold, and with the 1973 oil crisis, Ford chose to make the new car more suited to everyday driving with a shorter bonnet, larger cabin and the adoption of a hatchback rear door (accessing a 630 litre boot). By the standards of the day, the Capri II was a very well evolved vehicle with very few reliability issues. For Germany the Capri now offered 1.3-litre (55 PS (40 kW)), 1.6-litre (72 PS (53 kW)), 1.6-litre GT (88 PS (65 kW)), or 2.0-litre (99 PS (73 kW)) in-line four-cylinder engines, complemented by a 2.3-litre V6 (108 PS (79 kW)) and the UK sourced 3.0-litre V6 with (140 PS (103 kW) , available with either a 4 speed Ford Type 5 manual transmission or one of Ford's new C3 3 speed automatic transmissions available on all models except the 1.3, the C3 Automatic became standard on all Ghia models models after the 1976 model year and the 4 speed manual became optional.
Although it was mechanically similar to the Mark I, the Capri II had a revised, larger body and a more modern dashboard including a smaller steering wheel. The 2.0 L version of the Pinto engine was introduced in the European model and was placed below the 2.3 litre V6 and the 3.0 litre V6. The Capri still maintained the large rectangular headlights, which became the easiest way to distinguish between a Mark II and a Mark III. Larger front disc brakes and a standard alternator finished the list of modifications.
Sales of the Capri continued in Japan as it remained compliant with Japanese Government dimension regulations, but sales were not as successful as the previous generation.

Ford introduced the John Player Special limited edition, (known as the JPS) in March 1975. Available only in black or white, the JPS featured yards of gold pinstriping to mimic the Formula 1 livery, gold-coloured wheels, and a bespoke upgraded interior of beige cloth and carpet trimmed with black. In May 1976, and with sales decreasing, the intermediate 3.0 GT models disappeared to give way for the upscale 3.0 S and Ghia designations. In October 1976, production was limited to the Saarlouis factory only and the following year the Capri left the American market with only 513,500 models sold.
In 1977 Ford RS dealerships started offering various different performance and handling upgrades for the Capri, Escort, Cortina, and Fiesta. Cars with these upgrades equipped are referred to as " X Pack " models.


==== Engines ====


=== Ford Capri Mk III (1978–1986) ===
The Capri Mk III was referred to internally as "Project Carla", and although little more than a substantial update of the Capri II, it was often referred to as the Mk III. The first cars were available in March 1978, but failed to halt a terminal decline in sales. The concept of a heavily facelifted Capri II was shown at the 1976 Geneva show: a Capri II with a front very similar to the Escort RS2000 (with four headlamps and black slatted grille), and with a rear spoiler, essentially previewed the model some time before launch. The new styling cues, most notably the black "Aeroflow" grille (first used on the Mk I Fiesta) and the "sawtooth" rear lamp lenses echoed the new design language being introduced at that time by Ford of Europe's chief stylist Uwe Bahnsen across the entire range. Similar styling elements were subsequently introduced in the 1979 Cortina 80, 1980 Escort Mk III and the 1981 Granada Mk IIb. In addition, the Mk III featured improved aerodynamics, leading to improved performance and economy over the Mk II and the trademark quad headlamps were introduced.
At launch the existing engine and transmission combinations of the Capri II were carried over, with the 3.0 S model regarded as the most desirable model although the softer, more luxurious Ghia derivative with automatic, rather than manual transmission, was the bigger seller of the two V6-engined models.
Ford began to focus their attention on the UK Capri market as sales declined, realising the car had something of a cult following there. Unlike sales of the contemporary 4-door Cortina, Capri sales in Britain were to private buyers who would demand less discounts than fleet buyers allowing higher margins with the coupé. Ford tried to maintain interest in 1977 with Ford Rallye Sport, Series X, "X Pack" options from the performance oriented RS parts range. Although expensive and slow selling these proved that the press would enthusiastically cover more developed Capris with higher performance.
Despite being the most popular sporting model in Britain for most of its production life, the third generation Capri was also one of the most stolen cars in Britain during the 1980s and early 1990s, being classified as "high risk" of theft in a Home Office report.
The 3.0 S was used extensively in the TV series The Professionals, with characters Bodie driving a silver 3.0 S and Doyle a gold 3.0 S, which was credited with maintaining interest in the car in the UK.


==== 2.8 Injection models ====
For the 1981 model year, the Essex 3.0 V6 powerplant which had been the range topper since September 1969 was dropped, while a new sporty version debuted at the Geneva Motor Show, called the 2.8 Injection. The new model was the first regular model since the RS2600 to use fuel injection. Power rose to a claimed 160 PS (118 kW), even though tests showed the real figure was closer to 150 PS (110 kW), giving a top speed of 210 km/h (130 mph), but the car still had a standard four-speed gearbox. The Capri 2.8 Injection breathed new life into the range and kept the car in production 2–3 years longer than Ford had planned. The four-speed gearbox was replaced with a five-speed unit early on – at the same time Ford swapped the dated looking chequered seats for more luxurious looking velour trim. A more substantial upgrade was introduced in 1984 with the Capri Injection Special. This development used half leather seating and included a limited slip differential. Externally the car could be easily distinguished by seven spoke RS wheels (without the customary "RS" logo since this was not an RS vehicle) and colour-coded grille and headlamp surrounds. At the same time the 2.0 Capri was rationalised to one model, the 2.0 S, which simultaneously adopted a mildly modified suspension from the Capri Injection. The 1.6 model was also reduced to a single model, the 1.6 LS.


==== X-pack ====
The Mark II and Mark III 3.0 litre X-pack special performance options pack for the Capri was produced between 1977 and 1981. It used a special wide bodykit and a choice of two performance upgrades for Essex V6 3.0 L engines , the first upgrade pack bumped power up to 170 hp (127 kW) this engine included larger valves, ported cylinder heads and a Weber 40 DFI5 carburetor, the second option offered 185 hp (138 kW) and 195 lb/ft of torque thanks to three Weber 42 DCNF two-barrel carburetors, larger inlet and exhaust valves, a compression ratio of 9.1:1 and a high performance camshaft. The X-pack was quipped with a wing as standard and it also featured unique 7,5 x 13 inch wheels. It could reach 60 mph from standing in 7.4 seconds, a full second faster than the standard 3.0-litre Capri, and had a top speed of 130 mph, these upgrades could only be bought through Ford RS dealerships. The X Pack was also available in mainland Europe between 1979 and 1980 where it was marketed as the 3.0 RS but only 100 were ever made , they were all white with blue stripes running down the doors.


==== 2.8 Turbo ====
From July 1981 to Sept 1982, German RS dealers marketed a limited edition, Zakspeed inspired, left-hand drive only, 'Werksturbo' model with 190 PS (140 kW), which could propel the car to 220 km/h (137 mph). Based on a 3.0 S, this derivative featured widened bodywork, front and rear 'Ford Motorsport' badged spoilers, deep 7.5j four-spoked RS alloy wheels and an RS badged engine. The engine was based on a normally aspirated 2.8-litre V6, Ford Granada (Europe) engine. Figures of around 200 produced examples are common, but numbered transmission tunnels possibly indicate 155 conversions were made.


==== Tickford Turbo ====
The Tickford Capri used a turbocharged 2.8 Injection Cologne engine which developed 205 hp (153 kW), allowing it to reach 60 miles per hour in 6.7 seconds and 100 miles per hour in 18.5 seconds, topping out at 137 miles per hour. This version also featured a luxury interior with optional full leather retrim and Wilton carpeting and headlining, large rear spoiler, colour-coded front grille, deeper bumpers and 'one off' bodykit designed by Simon Saunders, later of KAT Designs and now designer of the Ariel Atom.
Rear disc brakes were standard on the Tickford, which featured numerous other suspension modifications. This model was essentially rebuilt by hand by Tickford at approximately 200 hours per car. It sold fewer than 100 units. One problem was the relative price difference to the standard Capri Injection, with the Tickford version costing twice as much.


==== Turbo Technics conversions ====
Independent tuner Turbo Technics also released a turbocharged 200 hp (149 kW) and 230 hp (172 kW) evolution which came supplied with a specially built gearbox. The Tickford Capri pricing issues meant that Ford also sanctioned the Turbo Technics conversion as semi-official, although only the German RS and British Tickford ever appeared in Ford literature as official Ford products.


==== Capri Laser ====
From November 1984 onwards, the Capri was sold only in Britain, with only right hand drive cars being made from this date. The normally aspirated 1.6 and 2.0 variants were rebranded with a new trim level – "Lasers" – which featured a fully populated instrument pod, leather gear lever, leather steering wheel, four-spoke alloy wheels as used on the S models, an electric aerial and colour-coded grille and mirrors.


==== Capri 280 ====
The last run limited edition "Brooklands" Green, 280 model, featuring a limited slip differential, full leather Recaro interior and 15 inch versions of the seven spoke 13 inch wheels fitted to the superseded Capri Injection Special. Ford originally intended to make 500 turbo charged vehicles (by Turbo Technics) complete with gold alloy wheels and name it the Capri 500 but a change of production planning meant a name change to Capri 280 as the cars were simply the last models that ran down the production line. A total of 1,038 Capri 280s were built. There was no direct successor to the Capri, as Ford felt that there was not adequate demand for a car of this type in Europe to justify a direct replacement; Capri sales had been declining since 1980, with faster versions of more practical hatchbacks and saloons becoming popular at the expense of sports cars. British Leyland, for instance, had taken the decision not to replace its MG and Triumph sports cars on their demise at the beginning of the 1980s due to falling popularity, instead concentrating on mostly MG-badged versions of hatchbacks and saloons like the Metro and Montego, while Ford had enjoyed strong sales of its faster versions of the Fiesta, Escort and Sierra in the run-up to the Capri's demise.
When the last Capri was made on 19 December 1986, 1,886,647 cars had rolled off the production lines. Production had ended at Halewood, UK in 1976 and the Capri was made exclusively in Germany from 1976 to 1986. Most of those (more than a million) were the Mk I, because the Mk I sold well in North America and Australia, while the Mk II and Mk III were only exported outside Europe (to Asia and New Zealand) in limited numbers.


==== Engines ====


== Outside Europe ==


=== North America ===


==== Mercury Capri ====

From 1970 to 1978, the Capri was sold in North America through Ford's Lincoln-Mercury Division. All were German-produced. Headlamps were four round sealed-beams, and turn signal lamps were grill-mounted on all US-spec 1971–74 Capris and 1976–78 Capri IIs. Ford hood letters were replaced with Capri letters, and a trunk-mounted Mercury script was added. Full instrumentation wasn't available on 1971–72 four-cylinder models but was made standard equipment from 1973 on. An optional interior decor package, changed by name to the "Ghia" package for the Capri II, featured deluxe interior trim and features. 1973 Ford Capris were the Mk I face-lift models featuring the new grill, larger taillights and new interior and dash. The 1973 model had the federally mandated 2.5 mph front bumper for '73. The bumper was extended, the gap closed with a silver filler panel. 1974 models had larger bumpers front and rear with wraparound urethane, body-color bumper covers to meet the revised Federal front and rear 5 mph standard. 1976–78 models were the re-designed hatchback models offered worldwide since 1974, fitted with the grill-mounted turn signal lamps and the required round sealed-beam headlamps, 5 mph body-color bumpers and catalytic converter, requiring no-lead fuel. In 1976, an 'S' (JPS) special edition featured black or white paint with gold-coloured wheels, gold pin-striping, and upgraded two-tone interior in beige and black. Due to late production of Capri IIs, there were no 1975 models sold in the USA.

Originally, Cologne-built Capris imported to North America were fitted only with the British 1600 OHV (1.6 L), 64 hp (48 kW) Kent engine with the four-speed manual transmission. The 1971 Capri offered the Kent-built 1600 I4 and the optional, Cologne-built OHC 2000 (2.0 L) I4 engine for improved performance with 101 hp (75 kW). An optional three-speed automatic transmission was made available with the 2000 I4 engine. In 1972–73, the 2000 I4 became the standard engine, and an OHV 2600 (2.6 L) Cologne V6 was optional, which produced 120 hp (89 kW). The 1600 I4 was dropped. For 1974, new engines were used—the OHC 2300 (2.3 L) I4 and OHV 2800 (2.8 L) Cologne V6; producing 88 hp (66 kW) and 105 hp (78 kW) respectively. The engines were carried over for the 1976–77 Capri ll hatchback models, although the V6's power had crept up to 109 hp (81 kW) at 4,800 rpm. The last Capris were brought in 1977 although sales of leftovers continued into 1978. Capri sales had slid considerably by the time of the introduction of the Capri II, and the high price contributed to ending sales of German-built Capris in the US.

In 1979, no longer importing the Ford Capri, but capitalising on the model's positive image, Mercury dealers began selling a new Capri that was a re-styled Ford Mustang.


=== Australia ===

The Ford Motor Company of Australia manufactured the European-designed Mk I Capri at its plant in the Sydney suburb of Homebush from 1969 until 1972. The Capri was offered to the Australian market from 3 May 1969, as the 1600 Deluxe and the 1600 GT, using the 1.6 L Ford Kent OHV engine. On 25 February 1970, the 3000 GT was launched, equipped with the 3.0 L Ford Essex V6. At the same time the 1600 GT became the 1600 XL while the 1600 Deluxe remained unchanged.
In November 1972, production of the Capri ended in Australia, with a total of 15,122 vehicles having been made. In 1973, Ford Australia imported fifty Capri RS3100 models. Neither the Mk I facelift Capri nor the subsequent Mk II and Mk III models were ever produced in Australia.
From 1989 to 1993 Ford Australia re-used the Capri name for a complete manufacture of an unrelated two-door sports car, most of which were convertibles and exported to the US. A small portion were sold in Australia, but it was soon found that these models were considerably inferior to the export models. Few were to be seen on the roads after 1998. The Capri could not compete against the reliable Mazda MX-5, and Ford stopped production after fewer than four years.


=== South Africa ===
Ford of South Africa assembled the Capri from 1970 to 1972 with a similar model range to the UK. No facelift models or RS variants were marketed in South Africa.
About 500 Capris were converted by specialist Basil Green Motors to run the 302 Ford Windsor V8 engine. These models were known as the Capri Perana and were very successful in local touring car events, winning the 1970 South African championship and, in a different format, the 1971 championship as well.
No Mk II and Mk III Capris were exported to, or built in South Africa.


== Motorsport ==


=== Zakspeed Ford Capri ===

A Group 5 version of the Capri Mk III was built by Zakspeed to compete in the Deutsche Rennsport Meisterschaft motor racing series. Klaus Ludwig subsequently won the 1981 title.


== See also ==
Mercury Capri
Ford Mustang


== References ==


== External links ==
Mercury Capri at DMOZ