Carlton County is a county located in the State of Minnesota. As of the 2010 census, the population was 35,386. Its county seat is Carlton. A portion of the Fond du Lac Indian Reservation is in the county. The county was formed in 1857 and organized in 1870.
Carlton County is included in the Duluth, MN-WI Metropolitan Statistical Area.


== Geography ==
According to the U.S. Census Bureau, the county has a total area of 875 square miles (2,270 km2), of which 861 square miles (2,230 km2) is land and 14 square miles (36 km2) (1.6%) is water.


=== Major highways ===


=== Adjacent counties ===
Saint Louis County, Minnesota (north)
Douglas County, Wisconsin (east)
Pine County, Minnesota (south)
Aitkin County, Minnesota (west)


== Climate and weather ==
In recent years, average temperatures in the county seat of Carlton have ranged from a low of 1 °F (−17 °C) in January to a high of 80 °F (27 °C) in July, although a record low of −45 °F (−43 °C) was recorded in January 1912 and a record high of 105 °F (41 °C) was recorded in July 1936. Average monthly precipitation ranged from 0.87 inches (22 mm) in February to 4.34 inches (110 mm) in September.


== Demographics ==
As of the 2010 United States Census, there were 35,386 people residing in the county. 89.7% were White, 5.9% Native American, 1.4% Black or African American, 0.5% Asian, 0.2% of some other race and 2.4% of two or more races. 1.4% were Hispanic or Latino (of any race). 16.4% were of German, 13.5% Finnish, 8.9% Norwegian, 8.6% Swedish and 5.6% American ancestry.
As of the census of 2000, there were 31,671 people, 12,064 households, and 8,408 families residing in the county. The population density was 37 people per square mile (14/km²). There were 13,721 housing units at an average density of 16 per square mile (6/km²). The racial makeup of the county was 91.75% White, 0.97% Black or African American, 5.19% Native American, 0.35% Asian, 0.01% Pacific Islander, 0.21% from other races, and 1.52% from two or more races. 0.84% of the population were Hispanic or Latino of any race. 18.5% were of German, 16.9% Finnish, 12.5% Norwegian, 11.8% Swedish and 5.8% Polish ancestry. 95.5% spoke English, 1.8% Finnish and 1.1% Spanish as their first language.
There were 12,064 households out of which 32.60% had children under the age of 18 living with them, 56.50% were married couples living together, 9.00% had a female householder with no husband present, and 30.30% were non-families. 26.10% of all households were made up of individuals and 12.00% had someone living alone who was 65 years of age or older. The average household size was 2.50 and the average family size was 3.00.
In the county the population was spread out with 25.40% under the age of 18, 7.70% from 18 to 24, 28.40% from 25 to 44, 23.50% from 45 to 64, and 15.10% who were 65 years of age or older. The median age was 38 years. For every 100 females there were 102.70 males. For every 100 females age 18 and over, there were 102.20 males.

The median income for a household in the county was $40,021, and the median income for a family was $48,406. Males had a median income of $38,788 versus $25,555 for females. The per capita income for the county was $18,073. About 5.40% of families and 7.90% of the population were below the poverty line, including 8.20% of those under age 18 and 9.30% of those age 65 or over.


== Communities ==


=== Cities ===


=== Townships ===


=== Unorganized territories ===
Clear Creek
North Carlton


=== Unincorporated communities ===


== See also ==
National Register of Historic Places listings in Carlton County, Minnesota


== References ==
^ "Minnesota Place Names". Minnesota Historical Society. Retrieved March 17, 2014. 
^ a b "State & County QuickFacts". United States Census Bureau. Retrieved August 31, 2013. 
^ "Find a County". National Association of Counties. Retrieved 2011-06-07. 
^ Upham, Warren (1920). Minnesota Geographic Names: Their Origin and Historic Significance. Minnesota Historical Society. p. 73. 
^ "2010 Census Gazetteer Files". United States Census Bureau. August 22, 2012. Retrieved October 6, 2014. 
^ a b "Monthly Averages for Carlton, Minnesota". The Weather Channel. Retrieved 2011-11-06. 
^ "U.S. Decennial Census". United States Census Bureau. Retrieved October 6, 2014. 
^ "Historical Census Browser". University of Virginia Library. Retrieved October 6, 2014. 
^ "Population of Counties by Decennial Census: 1900 to 1990". United States Census Bureau. Retrieved October 6, 2014. 
^ "Census 2000 PHC-T-4. Ranking Tables for Counties: 1990 and 2000" (PDF). United States Census Bureau. Retrieved October 6, 2014. 
^ "American FactFinder"


== External links ==
Carlton County official website
Carltoncountyhelp.org: A guide to service organizations in Carlton County, MN
Mn/DOT – map of Carlton County