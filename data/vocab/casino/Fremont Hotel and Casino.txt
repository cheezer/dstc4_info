The Fremont Hotel & Casino is located in downtown Las Vegas, Nevada, on the Fremont Street Experience. The casino is operated by the Boyd Gaming Corporation.


== History ==
The Fremont Hotel is located on 200 Fremont Street. It was designed by architect Wayne McAllister and opened on May 18, 1956 as the tallest building in the state of Nevada. At the time of its opening it had 155 rooms, cost $6 million to open and was owned by Ed Levinson and Lou Lurie. In 1963 the Hotel was expanded to include the 14 story Ogden tower and one of the city's first vertical parking garages.
In 1974 Allen Glick's Argent Corporation purchased the Fremont and in 1976 expanded the casino at a cost of $4 million. In 1983 Sam Boyd bought the Fremont to add to his Boyd Gaming group properties.
The Fremont Hotel and Casino is one of the casinos and hotels currently located in Downtown Las Vegas that is part of the Fremont Street Experience. The casino is located on what is commonly referred to as the four corners. These are the four main hotels that are located on the corner of Casino Center Boulevard and Fremont Street. The four casinos making up the four corners are The Fremont, the Four Queens, the Golden Nugget, and Binion's Gambling Hall and Hotel. Casino Center Boulevard is the only through street that passes under the canopy of the Fremont Street Experience. It passes between the Fremont and the Four Queens located on one side of the boulevard and The Golden Nugget and Binions located on the opposite side of the Boulevard.
In 1959 Wayne Newton made his start in Las Vegas at the Fremont at its Carnival Lounge.


=== Film history ===
Many scenes from the Jon Favreau and Vince Vaughn movie Swingers were filmed inside the Fremont, including their games of blackjack. The casino also appears periodically in the 1992 Disney film, Honey, I Blew Up the Kid.


== References ==


== External links ==
Official website