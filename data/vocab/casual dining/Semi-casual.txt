Semi-casual is a dress code in the American context. It is less presentational than informal but not as loose as full casual dress.
Semi-casual dress may include:
Loafers or cap toe shoes. Sandals may also be appropriate in warm weather.
Dark socks are still considered de rigueur for men. Women are given more leniency to wear light colored stockings, especially in warm weather.
Trousers in a smooth material or corduroy. Women are allowed skirts or dresses.
A button-down dress shirt, sweater, polo shirt, or sweater vest. Again, women are given more leniency, and are often allowed to wear a T-shirt if it is of a luxurious material. Men may wear a necktie, but it is certainly not expected.
Almost any kind of coat or hat may go with semi-casual dress in inclement weather. However, a top hat or a woman's Easter bonnet would be decidedly too formal.
In most contexts, excessive jewelry is considered out of place with semi-casual dress. Traditionally, this is especially true for men.
Hair dyed in artificial colors and heavy cosmetics are generally considered inappropriate for semi-casual wear, although some offices exhibit leniency.
Jeans and tennis shoes can be acceptable, but they must be chosen carefully as they can be too informal for semi-casual.


== See also ==
ceremonial dress
sportswear (fashion)