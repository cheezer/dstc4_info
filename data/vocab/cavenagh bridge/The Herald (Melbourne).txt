The Herald was a broadsheet newspaper published in Melbourne, Australia from 1840 to 1990.
The Port Phillip Herald was first published as a semi-weekly newspaper on 3 January 1840 from a weatherboard shack in Collins Street. It was the fourth newspaper to start in Melbourne.
The paper took its name from the region it served. Until its establishment as a separate colony in 1851, the area now known as Victoria was a part of New South Wales and it was generally referred to as the Port Phillip district.
Preceding it was the short-lived Melbourne Advertiser which John Pascoe Fawkner first produced on 1 January 1838 as hand-written editions for 10 weeks and then printed for a further 17 weekly issues, the Port Phillip Gazette, and The Port Phillip Patriot and Melbourne Advertiser. But within eighteen months of its inauguration, the Port Phillip Herald had grown to have the largest circulation of all Melbourne papers.
It was founded and published by George Cavenagh (1808–1869). He was born in India, as the youngest son of a Major. He came to Sydney in March 1825 where he worked as a magistrates’ clerk and farmer, before eventually taking on the role editor of the Sydney Gazette in 1836.
Bringing his wife (Jemima Caroline née Smith) and eight children, his staff and machinery to Melbourne, Cavenagh first produced the Port Phillip Herald as free editions. Later copies were to sell for sixpence.


== Original staff ==
The paper opened with the adopted motto "impartial – but not neutral", which was to run under its masthead for 50 years.
It was edited by William Kerr (1812–1859) who left Cavenagh in 1841 to be editor of the Port Phillip Patriot and Melbourne Advertiser and then on to the Port Phillip Gazette about a decade later.
The editor who followed Kerr at the Port Phillip Herald was Thomas Hamilton Osborne (c. 1805 – 1853) who later became proprietor of The Portland Mercury and Port Fairy Register (originally known as The Portland Mercury and Normanby Advertiser) on 10 January 1844.
Edmund "Garryowen" Finn (1819–1898) worked as the star reporter on the Herald for thirteen years. He arrived in Melbourne on 19 July 1841 and he joined the newspaper's staff in 1845.
Under George Cavenagh's leadership the paper would denounce adversaries, challenge ideas, and employ negative emotive language in an astute invective manner. In the early 1840s this was manifest in dealing with Judge John Walpole Willis (1793–1877) which resulted in severe fines being imposed on Cavenagh. It was an editorial policy that often involved litigation and Cavenagh was defendant in the first civil libel case in the colony. He retired in 1853, returned briefly the next year, and then retired permanently in 1855.


== Daily ==
On 1 January 1849, the Port Phillip Herald changed its name to The Melbourne Morning Herald and General Daily Advertiser. It also upped its printing schedule from thrice-weekly to daily. The Argus, which would not yet be a daily until 18 June 1849, scorned its rival's change of schedule with this report on 2 January 1849:
For twenty years from 1854, a succession of owners struggled to keep the newspaper afloat during the goldrush period. This included two years in which it was reduced to a biweekly. The newspaper changed its name several times before settling on The Herald from 8 September 1855 – the name it held for the next 135 years. In 1869 it developed stability as an evening daily.


== Closure ==
In February 1987 the company publishing The Herald, The Herald and Weekly Times Ltd, was purchased by Rupert Murdoch through his company News Ltd (the Australian arm of international media conglomerate News Corp). The Herald ceased publication on 5 October 1990, but on 8 October 1990, The Herald's name was merged with sister morning newspaper The Sun News-Pictorial to form the Herald Sun, which contained columns and features from both The Herald and The Sun News-Pictorial.


== References ==


== Further reading ==
Printers of the streets and lanes of Melbourne by Don Hauser. Nondescript Press. Melbourne 2006
One Hundred & Fifty Years of News from The Herald by Geoff Gaylard. Southbank Editions. Fishermans Bend 1990