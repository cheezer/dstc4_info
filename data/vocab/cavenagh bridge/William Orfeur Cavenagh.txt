Major General Sir William Orfeur Cavenagh KCSI (1820–1891), known as Sir Orfeur Cavenagh, was the last India-appointed Governor of the Straits Settlements, who governed the Settlements from 1859 to 1867.


== Family background ==
Cavenagh was the third son of James Gordon Cavenagh and Ann née Coates.


== Career ==
Cavenagh trained at Addiscombe Military Seminary, the military academy of the British East India Company. He passed his examination in June 1837, and early in 1838 joined the 32nd Regiment Native Infantry. In 1840 he passed the prescribed examination at Fort William College, Calcutta. He was appointed interpreter and quartermaster to the 41st Regiment Native Infantry. In 1840–41 he was attached to the force employed in watching the Nepal frontier.
He was an Adjutant of the 4th Irregular Cavalry (Skinner's Horse), and in December 1843 was badly wounded in the Battle of Maharajpore. His leg was severed just above the ankle by a round shot and his horse was killed under him. He was wounded again in January 1846 during the First Anglo-Sikh War, when he was struck in the left arm by a ricochetting round shot. After this he was appointed as Superintendent of the Mysore Princes and of the ex-Ameers of Sindh.
In 1850 he travelled to Britain and France in political charge of the Nepal Embassy under Jung Bahadur Rana. In 1854 he was appointed Town and Fort Major of Calcutta. In this role he was responsible to the Governor-General, the Marquess of Dalhousie followed by Lord Canning, for the safety of Fort William during the time of the Indian Rebellion.
It was in recognition of his services during the Rebellion that Canning offered him the post of Governor of the Straits Settlements: he took up the post on 8 August 1857. Under a royal charter of 1826, Singapore, Malacca, Penang and Dinding had been combined to form the Straits Settlements. The Governor of the Settlements and his council were answerable to the Governor-General of India in Calcutta. The Governor had little formal power, but was able to influence the Calcutta authorities who relied largely on the recommendations of these representatives on legislation and policy in each settlement. Control passed from Bengal to the Colonial Office in London on 1 April 1867 and the Settlements became a Crown colony. Cavenagh was the last Governor who reported to the Governor-General in Calcutta. His successor, Sir Harry Ord, reported to the Colonial Office in London.


== Personal life ==
Cavenagh married Elizabeth Marshall Moriarty on 7 Sept 1842 at Dinapore, India. He died in 1891 leaving two sons.


== Legacy ==
Singapore's Cavenagh Bridge is named in honour of the governor. The coat of arms of the Cavenagh family can still be seen atop the signage at both ends of the bridge.


== References ==


== Bibliography ==
Cavenagh-Mainwaring, J. G. The Mainwarings of Whitmore and Biddulph in the County of Stafford. pp. 180–181. 
Vibart, H.M. (1894). Addiscombe: its heroes and men of note. Westminster: Archibald Constable. pp. 473–4. 


== External links ==
Cavenagh family fonds at University of Victoria, Special Collections
Cavenagh family letterbooks at University of Victoria, Special Collections