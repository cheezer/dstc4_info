William Herbrand Sackville, 11th Earl De La Warr (born 10 April 1948) is a British nobleman. He inherited on 9 February 1988 on the death of his father William Sackville, 10th Earl De La Warr.
He married Anne Pamela, Countess of Hopetoun, née Leveson, former wife of the Marquess of Linlithgow, and granddaughter of Admiral Sir Arthur Cavenagh Leveson, in 1978.
Lord and Lady De La Warr run the family home and estate at Buckhurst Park, which is open to the public.
Lord and Lady De La Warr have two sons (the Countess also has two sons by her first marriage).
Their elder son, William Herbrand Thomas Sackville, Lord Buckhurst (born 13 June 1979), is heir to the earldom. He is a godson of Princess Margaret. In 2010, he married jewellery designer Countess Xenia Tolstoy-Miloslavsky, daughter of the historian Count Nikolai Tolstoy-Miloslavsky, and a distant relative of writer Leo Tolstoy. Lord and Lady Buckhurst's son William Lionel Robert was born on 24 January 2014.
Lord and Lady De La Warr's second son is The Honourable Edward Geoffrey Richard Sackville (born 6 December 1980). In 2012 his engagement to Sophia Georgina Milton Akroyd was announced, and they married on April 6, 2013. Their daughter Viola Idina Edith Sackville was born in July 2013. Edward is a prominent bloodstock agent best known for buying Classic winner Sky Lantern.


== Ancestry ==


== References ==


== External links ==
Hansard 1803–2005: contributions in Parliament by the Earl De La Warr
Who's Who 2008
http://www.thepeerage.com/p14945.htm#i149442