In astronomy and navigation, the celestial sphere is an imaginary sphere of arbitrarily large radius, concentric with Earth. All objects in the observer's sky can be thought of as projected upon the inside surface of the celestial sphere, as if it were the underside of a dome or a hemispherical screen. The celestial sphere is a practical tool for spherical astronomy, allowing observers to plot positions of objects in the sky when their distances are unknown or unimportant.


== Introduction ==

Because astronomical objects are at such remote distances, casual observation of the sky offers no information on the actual distances. All objects seem equally far away, as if fixed to the inside of a sphere of large but unknown radius, which rotates from east to west overhead while underfoot, the Earth seems to stand still. For purposes of spherical astronomy, which is concerned only with the directions to objects, it makes no difference whether this is actually the case, or if it is the Earth which rotates while the celestial sphere stands still.
The celestial sphere can be considered to be infinite in radius. This means any point within it, including that occupied by the observer, can be considered the center. It also means that all parallel lines, be they millimetres apart or across the Solar System from each other, will seem to intersect the sphere at a single point, analogous to the vanishing point of graphical perspective. All parallel planes will seem to intersect the sphere in a coincident great circle (a “vanishing circle”). Conversely, observers looking toward the same point on an infinite-radius celestial sphere will be looking along parallel lines, and observers looking toward the same great circle, along parallel planes. On an infinite-radius celestial sphere, all observers see the same things in the same direction.
For some objects, this is over-simplified. Objects which are relatively near to the observer (for instance, the Moon) will seem to change position against the distant celestial sphere if the observer moves far enough, say, from one side of the Earth to the other. This effect, known as parallax, can be represented as a small offset from a mean position. The celestial sphere can be considered to be centered at the Earth's center, the Sun's center, or any other convenient location, and offsets from positions referred to these centers can be calculated. In this way, astronomers can predict geocentric or heliocentric positions of objects on the celestial sphere, without the need to calculate the individual geometry of any particular observer, and the utility of the celestial sphere is maintained. Individual observers can work out their own small offsets from the mean positions, if necessary. In many cases in astronomy, the offsets are insignificant.
The celestial sphere can be thus be thought of as a kind of astronomical shorthand, and is applied very frequently by astronomers. For instance, the Astronomical Almanac for 2010 lists the apparent geocentric position of the Moon, 1 Jan 2010 at 0h Terrestrial Time, in equatorial coordinates, as right ascension 6h 57m 48s.86, declination +23° 30' 05".5. Implied in this position is that it is as projected onto the celestial sphere; any observer at any location looking in that direction would see the "geocentric Moon" in the same place against the stars. For many rough uses (for instance, calculating an approximate phase of the Moon), this position, as seen from the Earth's center, is adequate. For applications requiring precision (for instance, calculating the shadow path of an eclipse), the Almanac gives formulae and methods for calculating the topocentric coordinates, that is, as seen from a particular place on the Earth's surface, based on the geocentric position. This greatly abbreviates the amount of detail necessary in such almanacs, as each observer can handle their own specific circumstances.


== Celestial coordinate systems ==
These concepts are important for understanding celestial reference systems, the methods in which the positions of objects in the sky are measured. Certain reference lines and planes on Earth, when projected onto the celestial sphere, form the bases of the reference systems. These include the Earth's equator, axis, and the Earth's orbit. At their intersections with the celestial sphere, these form the celestial equator, the north and south celestial poles and the ecliptic, respectively. As the celestial sphere is considered infinite in radius, all observers see the celestial equator, celestial poles and ecliptic at the same place against the background stars.
Directions toward objects in the sky can be quantified by constructing, from these bases, celestial coordinate systems. Similar to the terrestrial longitude and latitude, the equatorial system of right ascension and declination specifies positions relative to the celestial equator and celestial poles. The ecliptic system of celestial longitude and celestial latitude specifies positions relative to the Earth's orbit. There are more coordinate systems besides the equatorial right ascension/declination system and the ecliptic system.


== History ==
The ancients assumed the literal truth of stars attached to a celestial sphere, revolving about the Earth in one day, and a fixed Earth. The Eudoxan planetary model, on which the Aristotelian and Ptolemaic models were based, was the first geometric explanation for the "wandering" of the classical planets. The outer most of these "crystal spheres" was thought to carry the fixed stars. Eudoxus used 27 concentric spherical solids to answer Plato's challenge: "By the assumption of what uniform and orderly motions can the apparent motions of the planets be accounted for?"


== Star globe ==

A celestial sphere can also refer to a physical model of the celestial sphere or celestial globe. Such globes map the constellations on the outside of a sphere, resulting in a mirror image of the constellations as seen from Earth. The oldest surviving example of such an artifact is the globe of the Farnese Atlas sculpture, a 2nd-century copy of an older (Hellenistic period, ca. 120 BC) work.


== Bodies other than Earth ==

Observers on other worlds would, of course, see objects in that sky under much the same conditions - as if projected onto a dome. Coordinate systems based on the sky of that world could be constructed. These could be based on the equivalent "ecliptic", poles and equator, although the reasons for building a system that way are as much historic as technical.


== See also ==
Celestial coordinate system
Celestial spheres
Firmament
History of astronomy
Sky
Spherical astronomy
Stellar parallax, a type of short-term motion of distant stars
Proper motion, a type of longer-term motion of distant stars


== Notes ==


== References ==
Bowditch, Nathaniel (2002). The American Practical Navigator. Bethesda, MD: National Imagery and Mapping Agency. ISBN 0-939837-54-4. 
MacEwen, William A.; William Hayler; Turpin, Edward A. (1989). Merchant Marine officers' handbook: based on the original edition by Edward A. Turpin and William A. MacEwen (5th ed.). Cambridge, Md: Cornell Maritime Press. pp. 46–51. ISBN 0-87033-379-8. 


== External links ==
MEASURING THE SKY A Quick Guide to the Celestial Sphere Jim Kaler, University of Illinois
General Astronomy/The Celestial Sphere Wikibooks
Rotating Sky Explorer University of Nebraska-Lincoln
Interactive Sky Chart SkyandTelescope.com
Monthly skymaps for every location on Earth