Central is an adjective usually referring to being in the center of some place or (mathematical) object.
Central may also refer to:


== Directions and generalised locations ==
Central Africa, a region in the centre of Africa continent, also known as Middle Africa
Central America, a region in the centre of America continent
Central Asia, a region in the centre of Eurasian continent
Central Australia, a region of the Australian continent
Central Europe, a region of the European continent
Central United States, a region of the United States of America
Central Belt, an area in the centre of Scotland
Central Region (disambiguation)
Central London, the centre of London


== Specific locations ==


=== Countries ===
Central African Republic, a country in Africa


=== States and provinces ===
Blue Nile (state) or Central, a state in Sudan
Central Department, Paraguay
Central Province, Zambia
Central Province, Sri Lanka
Central Province (Solomon Islands)
Central Province (Kenya)
Central Province (Papua New Guinea)


=== Cities and districts ===
United States
Central, Alaska
Central, California (disambiguation)
Central City, Colorado, legally City of Central, Colorado
Central, Indiana
Central City, Louisiana
Central Township, Merrick County, Nebraska
Central, South Carolina
Central, Tennessee
Central, Utah
Central, West Virginia
Other nations
Central Federal District, a federal district of Russia
Central District Municipality, a district in North West, South Africa
Central, Hong Kong
Central and Western District, Hong Kong
Central, New South Wales, Australia
Central District (Botswana)


=== Railway stations and lines ===
Central station, a frequently-encountered element in the name of railway stations
Central (MBTA station), a station on the Boston subway's Red Line
Central (CTA) (disambiguation), stations on the Chicago Transit Authority's 'L' system
Central (CTA Purple Line station)
Central (CTA Green Line station)
Central (CTA Congress Line station)

Central line, on the London Underground
Central Railway (India)
Central Line (Mumbai Suburban Railway)

Central railway station, Sydney, Australia
Central railway station, Brisbane, Australia
Central Station (MTR), an MTR station in Hong Kong
Chennai Central, a railway station in India
Glasgow Central (disambiguation)
Mumbai Central railway station, a railway station in India
Tsentralna (Dnipropetrovsk Metro) (central), a station on the Dnipropetrovsk Metro


== Sports ==
Rosario Central, an important football club in Argentina
Central Córdoba de Santiago del Estero, a football (soccer) club in Argentina
Central Sport Club, a football (soccer) club in Brazil
Central United, a football (soccer) club in New Zealand
União Central Futebol Clube, a football (soccer) club in Brazil


== Companies ==
Central (home improvement store), a home improvement store chain located in Nova Scotia, Canada
Central Group, a department store company in Thailand
ITV Central (formerly Central Independent Television), the ITV company formed from ATV
Central Trains, a former UK train operating company owned by National Express Group


== Schools ==
Central C of E Junior School, a school in the United Kingdom.
Central Catholic High School, a school in Pittsburgh, PA.


== Others ==
Central Time Zone (Americas)
Central College (disambiguation)
Central High School (disambiguation), a common name for high schools
Central People's Government of the People's Republic of China, the communist government
Central School of Speech and Drama, London
Central, a fictional angel in Jack (webcomic)
Central, the fictional area which seceded from both the United States and the Confederate States during the American Civil War in My Name Is Earl
The Central, a commercial and residential building in Singapore
Central (Hypermarket), a retail chain of the Pantaloon Retail India
MediaCorp TV12 Central, a defunct TV channel in Singapore
A Division
Central angle, a geometric measurement
Central Christian Church (Henderson, Nevada), a postdenominational Evangelical megachurch
Central Zürich, a square in Zürich, Switzerland


== See also ==
All pages with titles containing "Central"
Tsentralny (disambiguation), Russia