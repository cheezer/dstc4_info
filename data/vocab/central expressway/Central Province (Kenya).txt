The Central Province (Swahili: Kati) is a region in central Kenya. It covers an area of 13,191 km² and is located to north of Nairobi and west of Mt. Kenya (see maps). The province had 4,383,743 inhabitants according to the 2009 census. The provincial capital is Nyeri.
Central Province is the ancestral home of the Gikuyu community.


== Climate ==
The climate of Central Province is generally cooler than that of the rest of Kenya, due to the region's higher altitude. Rainfall is fairly reliable, falling in two seasons, one from early March to May (the long rains) and a second during October and November (the short rains).


== General information ==
Central Province is a key producer of coffee, one of Kenya's key exports. Much of Kenya's dairy industry is also based in this province. The province's headquarters are in Nyeri. Central Province was divided into seven districts (wilaya'at) until 2007:


== Counties ==


=== Districts after 2007 ===
Several new districts (declared sub-counties in 2013) were created in 2007:


== History ==
The province is inhabited by the Kikuyu speaking community almost exclusively. They are part of the Kenya Eastern Bantu.
During Kenya's colonization by the British, much of the province was regarded as part of the 'White Highlands', for the exclusive use of the European community. Therefore it saw political activity from the local communities who felt that they had an ancestral right to the land. This tension culminated in the 1950s with the Mau Mau rebellion; it saw the region placed under a state of emergency and the arrest of many prominent political leaders.


== See also ==
Ruiru
Gichuru
Gitura
Gatithi
Barigito


== References ==