The Central Time Zone (CT) is a time zone in parts of Canada, the United States, Mexico, Central America, some Caribbean Islands, and part of the Eastern Pacific Ocean. Time in the zone is six hours behind Coordinated Universal Time (Greenwich Mean Time - GMT). During daylight saving time (DST), time in most (but not all) of the zone is five hours behind GMT.


== Regions using Central TimeEdit ==


=== CanadaEdit ===

The province of Manitoba is the only province or territory in Canada that observes Central Time in all areas.
The following Canadian provinces and territories observe Central Time in the areas noted, while their other areas observe Eastern Time:
Nunavut (territory): western areas (most of Kivalliq Region and part of Qikiqtaaluk Region)
Ontario (province): a portion of the northwest bordering northeastern Manitoba
Also, most of the province of Saskatchewan is on Central Standard Time year round, never adjusting for Daylight Saving Time. Major exceptions include Lloydminster, a city situated on the boundary between Alberta and Saskatchewan. The city charter stipulates that it shall observe Mountain Time and DST, putting the community on the same time as all of Alberta, including the major cities of Calgary and Edmonton.


=== United StatesEdit ===

The Central Time Zone is the second most populous in the US after the Eastern Time Zone. Several states straddle time zone boundaries:
Alabama: Although all of Alabama is legally on Central Time, Phenix City and the surrounding communities of Smiths Station, Valley, and Lanett unofficially observe Eastern Time, as these areas are part of the media market and metropolitan area of the considerably larger city of Columbus, Georgia in the Eastern Time Zone.
Arkansas
Florida: The Florida Panhandle west of the Apalachicola River, bordering on Alabama; the remainder of Florida is in the Eastern Time Zone
Illinois
Indiana: Southwestern and northwestern corners, bordering on Illinois and Kentucky (see Time in Indiana)
Iowa
Kansas: All except the westernmost counties; Sherman, Wallace, Greeley and Hamilton
Kentucky: Western half, generally everything west of Louisville. Some eastern counties observe Central Time because they are close to the northern border of the Middle Tennessee counties surrounding the Nashville metropolitan area.
Louisiana
Michigan: All of Michigan observes Eastern Time except the four Upper Peninsula counties (Gogebic, Iron, Dickinson, and Menominee) that border Wisconsin. Generally, other westernmost counties from this area such as Ontonagon unofficially observe Eastern Time.
Minnesota
Mississippi
Missouri
Nebraska: Eastern two-thirds
North Dakota: Entire state except southwestern quadrant (bordering Montana and South Dakota), south of the Missouri River
Oklahoma
The community of Kenton, at the extreme western end of the Panhandle, unofficially observes Mountain Time.

South Dakota: Eastern half as divided by the Missouri river adjacent to the state capital, Pierre. Note: the metropolitan area of Pierre is Central, including Fort Pierre.
Tennessee: Western two-thirds; all of Middle Tennessee and West Tennessee plus Bledsoe, Cumberland, and Marion counties in East Tennessee
Texas: All of Texas is in the Central Time Zone, except for Hudspeth County and El Paso County in the westernmost part.
Wisconsin


=== MexicoEdit ===

Most of Mexico—roughly the eastern three-fourths—lies in the Central Time Zone, with six of the northwestern states (and one southeastern state) being exceptions: Baja California (Norte), Baja California Sur, Chihuahua, Nayarit, Quintana Roo, Sinaloa, and Sonora.
The states of Mexico that observe Central Time in their entireties:

Mexico City, which is coterminous with the Federal District (Distrito Federal), also uses Central Time.
In Nayarit, however, only the municipality of Bahia de Banderas uses Central Time.


=== Central America and Caribbean IslandsEdit ===
Belize, Costa Rica, El Salvador, Guatemala, Honduras, and Nicaragua all use Central Standard Time year-round.


=== Eastern Pacific islands and other areasEdit ===


== Central Daylight TimeEdit ==
Daylight saving time (DST) is in effect in much of the Central time zone between mid-March and early November. The modified time is called Central Daylight Time (CDT) and is UTC−5. In Canada, Saskatchewan does not observe a time change. One reason that Saskatchewan does not take part in a time change is that, geographically, the entire province is closer to the Mountain Time Zone's meridian. The province elected to move onto "permanent" daylight saving by being part of the Central Time Zone. The only exception is the region immediately surrounding the Saskatchewan side of the biprovincial city of Lloydminster, which has chosen to use Mountain Time with DST, synchronizing its clocks with those of Alberta.
In those areas of the Canadian and American time zones that observe DST, beginning in 2007, the local time changes at 02:00 local standard time to 03:00 local daylight time on the second Sunday in March and returns at 02:00 local daylight time to 01:00 local standard time on the first Sunday in November. Mexico decided not to go along with this change and observes their horario de verano from the first Sunday in April to the last Sunday in October. In December 2009, the Mexican Congress allowed ten border cities, eight of which are in states that observe Central Time, to adopt the U.S. daylight time schedule effective in 2010.


== Alphabetical list of major Central Time Zone metropolitan areasEdit ==
Rockford, Illinois
Normal, Illinois
Bloomington, Illinois
Champaign, Illinois
Topeka, Kansas
Madison, Wisconsin
Janesville, Wisconsin
Waukegan, Illinois
Kenosha, Wisconsin
Racine, Wisconsin
Hammond, Indiana
Torreon, Coahuila
Red Lake, Ontario
Grand Forks, North Dakota
Minot, North Dakota
Arlington, Texas
Joplin, Missouri
Thompson, Manitoba
Grand Rapids, Manitoba
Jackson, Tennessee
Clarksville, Tennessee
Cookeville, Tennessee
Wheaton, Illinois


== See alsoEdit ==
Effects of time zones on North American broadcasting


== ReferencesEdit ==


== External linksEdit ==
World time zone map
History of U.S. time zones and UTC conversion
The official U.S. time for the Central Time Zone
Cities in CST
Official times across Canada