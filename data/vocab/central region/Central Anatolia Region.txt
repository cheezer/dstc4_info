The Central Anatolia Region (Turkish: İç Anadolu Bölgesi) is a geographical region of Turkey.


== Subdivisions ==

Konya Section (Turkish: Konya Bölümü)
Obruk Plateau (Turkish: Obruk Platosu)
Konya - Ereğli Vicinity (Turkish: Konya - Ereğli Çevresi)

Upper Sakarya Section (Turkish: Yukarı Sakarya Bölümü)
Ankara Area (Turkish: Ankara Yöresi)
Porsuk Gully (Turkish: Porsuk Oluğu)
Sündiken Mountain Chain Area (Turkish: Sündiken Dağları Yöresi)
Upper Sakarya Gully (Turkish: Yukarı Sakarya Yöresi)

Middle Kızılırmak Section (Turkish: Orta Kızılırmak Bölümü)
Upper Kızılırmak Section (Turkish: Yukarı Kızılırmak Bölümü)


== Ecoregions ==


=== Terrestrial ===


==== Palearctic ====
Palearctic regions include:


===== Temperate broadleaf and mixed forests =====
Temperate broadleaf and mixed forests include:
Central Anatolian deciduous forests
Eastern Anatolian deciduous forests


==== Temperate coniferous forests ====
Temperate coniferous forests are Northern Anatolian conifer and deciduous forests.


==== Temperate grasslands, savannas and shrublands ====
Central Anatolian steppe are classified as Temperate grasslands, savannas and shrublands.


==== Mediterranean forests, woodlands, and scrub ====
Mediterranean forests, woodlands, and scrub include:
Anatolian conifer and deciduous mixed forests
Southern Anatolian montane conifer and deciduous forests


== Provinces ==
Provinces that are entirely in the Central Anatolia Region:
Aksaray
Kırıkkale
Kırşehir
Nevşehir
Provinces that are mostly in the Central Anatolia Region:
Ankara
Çankırı
Eskişehir
Karaman
Kayseri
Konya
Sivas
Yozgat
Provinces that are partially in the Central Anatolia Region:
Afyonkarahisar
Bilecik
Çorum
Erzincan


== Climate ==


== See also ==
Geography of Turkey