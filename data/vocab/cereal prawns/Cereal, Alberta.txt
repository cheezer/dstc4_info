Cereal is a village in central Alberta east of Drumheller. It was named after the post office that was established in the area in 1910.


== Demographics ==
In the 2011 Census, the Village of Cereal had a population of 134 living in 71 of its 79 total dwellings, a 6.3% change from its 2006 population of 126. With a land area of 0.95 km2 (0.37 sq mi), it had a population density of 141.1/km2 (365.3/sq mi) in 2011.
In 2006, Cereal had a population of 126 living in 68 dwellings, a 32.6% decrease from 2001. The Village has a land area of 0.95 km2 (0.37 sq mi) and a population density of 133.2/km2 (345/sq mi).


== See also ==
List of communities in Alberta
List of villages in Alberta


== References ==