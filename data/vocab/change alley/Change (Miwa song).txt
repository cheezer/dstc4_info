"Change" (stylized as "chAngE") is Japanese singer-songwriter miwa's third major label single, released on September 1, 2010.


== Writing and inspiration ==
The song is an upbeat pop rock song. The song is entirely in Japanese, with the exception of the English phrase, "I wanna change". The lyrics describe a person with the desire to "change to be more like (themselves)." The letters a and e being capitalised in the song's title represents miwa's change from an acoustic guitar to an electric guitar. The guitar miwa used for this release was a Joan Jett Model Gibson Melody Maker.


== Promotion ==
The song was used as the 12th opening theme song for the Bleach anime, starting in April 2010 and succeeded by "Ranbu no Melody" by SID on October 12, 2010.
On August 21, performed the song live on the Fuji TV music show Music Fair, and also collaborated with Ryoko Moriyama and Akiko Yano to perform covers of "Kono Hiroi Nohara Ippai" (Moriyama's debut single in 1967) and "Super Folk Song" a 1992 single by Yano.
During promotions for the single, miwa held the first "miwa Keiongakubu" (miwa軽音楽部, "miwa's Light Music Club") event on the 5th of September, in which miwa held a meeting with many high school guitarists (a group of 20 electric guitar players, chosen from 300 applicants at miwa's official site). At the event, the 21 electric guitar players performed "Change" in unison.


== Music video ==

The music video features miwa performing the song with band members, in a cave lit by candles. Interspersed between the performance are two main separate scenes. The first features several women in a dark forest, whose white dresses are lit with glowing lights. The other is miwa in a red dress, exploring a forest at daytime. The women occasionally feature in the same scenes as Miwa, as she explores the forest.


== Track listing ==
All songs written and composed by miwa. 


== Chart rankings ==


=== Reported sales ===


== References ==


== External links ==
Sony Music "Change" profile (Japanese)