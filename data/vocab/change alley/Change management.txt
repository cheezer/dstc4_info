Change management is an approach to transitioning individuals, teams, and organizations to a desired future state. In a project management context, change management may refer to a project management process wherein changes to the scope of a project are formally introduced and approved.


== History ==


=== 1960s ===
Everett Rogers wrote the book Diffusion of Innovations in 1962. There would be five editions of the book through 2003, during which time the statistical analysis of how people adopt new ideas and technology would be documented over 5000 times. The scientific study of hybrid corn seed adoption led to the commonly known groupings of types of people: Innovators, Early Adopters, Early Majority, Late Majority and Laggards.


=== 1980s ===
McKinsey consultant Julien Phillips first published a change management model in 1982 in the journal Human Resource Management, though it took a decade for his change management peers to catch up with him.
Robert Marshak credits the big 6 accounting firms and management consulting firms with adopting the work of early organizational change pioneers such as Daryl Conner and thereby contributing to the legitimization of a whole "change management industry" when they branded their reengineering services groups as change management services in the 1980s.
Robert Marshak is incorrect in crediting the big 6 accounting firms with creating the change management industry since prior to 1983, Daryl Conner and Don Harrison founded ODR and developed a change management methodology known as MOC or Managing Organizational Change. Daryl Conner was a speaker on the IMS (Institute for Management Studies) circuit about change management and provided change management consultation to many industries during the 1980's well in advance of any of the other larger firms adopting the moniker of change management.


=== 1990s ===
In 1994, Daryl Conner founded Conner Partners and in 1993, he wrote the book, Managing at the Speed of Change. Conner penned the analogy "burning platform" based on the 1988 Piper off shore oil rig fire (North Sea off the coast of Scotland). Conner Partners influenced the large Management Consulting firms over the 80s and 90s as firms needed to understand the human performance and adoption techniques to help ensure technology innovations were absorbed and adopted as best as possible.


=== 2000s ===
Linda Ackerman Anderson states in Beyond Change Management that in the late 1980s and early 1990s, top leaders, growing dissatisfied with the failures of creating and implementing changes in a top-down fashion, created the role of the change leader to take responsibility for the human side of the change. The first "State of the Change Management Industry" report in the Consultants News was published in February 1995.


=== 2010s ===
In 2010, based on her book "RIMER Managing Successful Change", Christina  Dean, Managing Director of Uniforte Pty Ltd, established Change Management as a formal vocation in Australia by writing the Australian National Competency Standards in Organizational and Community Change Management, which led to the development of the first Australian Diploma of Organizational Change Management, and which is an internationally recognized qualification.


== Approach ==
Organizational change is a structured approach in an organization for ensuring that changes are smoothly and successfully implemented to achieve lasting benefits.


=== Reasons for change ===
Globalization and the constant innovation of technology result in a constantly evolving business environment. Phenomena such as social media and mobile adaptability have revolutionized business and the effect of this is an ever increasing need for change, and therefore change management. The growth in technology also has a secondary effect of increasing the availability and therefore accountability of knowledge. Easily accessible information has resulted in unprecedented scrutiny from stockholders and the media and pressure on management.
With the business environment experiencing so much change, organizations must then learn to become comfortable with change as well. Therefore, the ability to manage and adapt to organizational change is an essential ability required in the workplace today. Yet, major and rapid organizational change is profoundly difficult because the structure, culture, and routines of organizations often reflect a persistent and difficult-to-remove "imprint" of past periods, which are resistant to radical change even as the current environment of the organization changes rapidly.
Due to the growth of technology, modern organizational change is largely motivated by exterior innovations rather than internal moves. When these developments occur, the organizations that adapt quickest create a competitive advantage for themselves, while the companies that refuse to change get left behind. This can result in drastic profit and/or market share losses.
Organizational change directly affects all departments from the entry level employee to senior management. The entire company must learn how to handle changes to the organization.


=== Choosing what changes to implement ===
When determining which of the latest techniques or innovations to adopt, there are four major factors to be considered:
Levels, goals, and strategies
Measurement system
Sequence of steps
Implementation and organizational change


=== Managing the change process ===
Regardless of the many types of organizational change, the critical aspect is a company’s ability to win the buy-in of their organization’s employees on the change. Effectively managing organizational change is a four-step process:
Recognizing the changes in the broader business environment
Developing the necessary adjustments for their company’s needs
Training their employees on the appropriate changes
Winning the support of the employees with the persuasiveness of the appropriate adjustments
As a multi-disciplinary practice that has evolved as a result of scholarly research, organizational change management should begin with a systematic diagnosis of the current situation in order to determine both the need for change and the capability to change. The objectives, content, and process of change should all be specified as part of a Change Management plan.
Change management processes should include creative marketing to enable communication between changing audiences, as well as deep social understanding about leadership’s styles and group dynamics. As a visible track on transformation projects, Organizational Change Management aligns groups’ expectations, communicates, integrates teams and manages people training. It makes use of performance metrics, such as financial results, operational efficiency, leadership commitment, communication effectiveness, and the perceived need for change to design appropriate strategies, in order to avoid change failures or resolve troubled change projects.
Successful change management is more likely to occur if the following are included:
Benefits management and realization to define measurable stakeholder aims, create a business case for their achievement (which should be continuously updated), and monitor assumptions, risks, dependencies, costs, return on investment, dis-benefits and cultural issues affecting the progress of the associated work
Effective communication that informs various stakeholders of the reasons for the change (why?), the benefits of successful implementation (what is in it for us, and you) as well as the details of the change (when? where? who is involved? how much will it cost? etc.)
Devise an effective education, training and/or skills upgrading scheme for the organization
Counter resistance from the employees of companies and align them to overall strategic direction of the organization
Provide personal counseling (if required) to alleviate any change-related fears
Monitoring of the implementation and fine-tuning as required


== See also ==
Change management (ITSM)
Employee engagement
Human resource management
Leadership development
Organization studies
Organizational culture
Organizational structure
Performance management
Project management
Business process reengineering
Stakeholder management
Strategic change
Talent management
Training and development
Transtheoretical model


== References ==