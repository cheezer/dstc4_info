Changi Air Base (ICAO: WSSS) or Changi Air Base (West) (Changi West Complex), formerly the RAF Station Changi, is an airfield military airbase of the Republic of Singapore Air Force located at Changi, in the eastern tip of Singapore. Sited at two locations to the east and west of Singapore Changi Airport, it co-shares runway facilities with the civilian airport and currently occupies a third runway slated for future expansion for civilian use by Singapore Changi Airport. Together, the two airfields house 121 Squadron, 112 Squadron, No 145 Squadron, the Field Defence Squadron, the Air Logistics Squadron and the Airfield Maintenance Squadron. The air base badge carries the motto: "Together in Excellence".


== History ==


=== RAF Changi ===

First completed as a British artillery camp in 1940, it was used together with the nearby Changi Prison for housing many of the Allied prisoners-of-war (POWs) after the fall of Singapore in 1942. The construction of airfield was initiated by the occupying Imperial Japanese forces using those same Allied POWs as forced labours, building two unpaved landing strips between 1943 to 1944, intersecting in a cross layout and in approximately north-south and east-west directions. The airfield facility became a Royal Air Force station and was renamed RAF Station Changi in 1946 after the Japanese surrender. Imprisoned Japanese troops were then made to improve the runways, reinforcing the north-south runway for military aircraft and adding perforated steel plates to the east-west runway.
Completed post war, RAF Chia Keng — a GCHQ radio receiving station, was a satellite station of RAF Changi (being the Headquarters Air component part of British Far East Command) until the British Forces withdrawal from Singapore. Also, the nearby RAF Hospital Changi Old Changi Hospital (now defunct as Changi Hospital) functioned as the primary British military hospital providing medical care for all British, Australian and New Zealand servicemen stationed in the Eastern and Northern part of Singapore, while Alexandra Hospital was put in charge for those stationed in the Southern and Western part of Singapore.


=== Changi Air Base ===
Upon the withdrawal of British forces from Singapore, RAF Changi was renamed as Changi Air Base (CAB) and was handed over to the SADC (predecessor of Republic of Singapore Air Force) on 9 Dec 1971. Thereafter, the airfield received its first flying squadron of SADC - the Alouette Squadron and their Alouette IIIs helicopters shortly after New Year's Day 1972. With the arrival of the first Shorts Skyvans in 1973, SADC began to form the 121 Squadron at Changi Air Base and it is currently the oldest resident squadron of the airfield.
The novel 'The Sound of Pirates' by former RAF airman Terence Brand is based in the 1960s both on the airfield and in the surrounding areas.


==== Singapore Changi Airport ====

In June 1975, the Singapore government acquired about two-thirds of the airbase (saved for the main flight-line, hangar/aircraft maintenance facilities and control tower which were located in the western section of the airbase) for the construction of the new Singapore Changi Airport, with the new runways in close alignment with the original north-south runway. The east-west runway was almost erased from the map, currently surviving as a taxiway to the apron area which has remained operational as part of Changi Air Base.


==== Changi Air Base (West) ====

Following the opening of the new Changi Air Base (East) (Changi East Complex) on 29 November 2004, the existing facilities at Changi air base has been renamed as Changi Air Base (West) (Changi West Complex) and Headquarters Changi Air Base (HQ CAB).
The flying squadrons now are:
112 Squadron with 4 Boeing KC-135R Stratotankers; &,
121 Squadron with 4 Utility Transport Aircraft (UTA) and 5 Maritime Patrol Aircraft (MPA) versions of the Fokker F50.
The Support Squadrons are:
Field Defence Squadron (FDS)
Airfield Maintenance Squadron (AMS)
Airfield Operations Maintenance Squadron Fixed Wing 2 (AOMS-FW2)
Ground Logistics Liaison Office / Ground Logistics Squadron (GLLO/GLS)
Air Movement Centre (AMC)


== Gallery ==


== See also ==
Battle of Singapore
British Far East Command
Far East Air Force (Royal Air Force)
Far East Strategic Reserve
Former overseas RAF bases
Indonesia–Malaysia confrontation
Malayan Emergency
Republic of Singapore Air Force


== References ==


== External links ==
RSAF web page on Changi Air Base (CAB)
News article on new airbase
Changi International Airport history
Accident history for SIN at Aviation Safety Network
Current weather for WSSS at NOAA/NWS
Airport information for WSSS at World Aero Data. Data current as of October 2006.
History of RAF
Crest Badge and Information of RAF Changi
Memories of Singapore - RAF Changi