Changi Single Member Constituency (Simplified Chinese: 樟宜单选区;Traditional Chinese: 樟宜單選區) is a defunct single member constituency in Changi and eastern outer islands such as Pulau Ubin, Singapore; from 1951 elections to 1997 elections which most of the part (Including Pulau Ubin) had absorbed into Siglap ward in East Coast GRC while the remaining were to form as Changi-Simei ward in Aljunied GRC, only to be part of East Coast GRC in 5 years time on prior to 2001 general elections.


== Members of Parliament ==
Charles Joseph (1951 – 1955)
Lim Cher Kheng (1955–1959)
Teo Hock Guan (1959–1963)
Sim Boon Woo (1963–1976)
Teo Chong Tee (1976–1997)


== Candidates and results ==


=== Elections in 1990s ===


=== Elections in 1980s ===


=== Elections in 1970s ===


=== Elections in 1960s ===
Note: One of the component party in Singapore Alliance is United Malays National Organisation (UMNO), and hence the swing will be based on its previous election of UMNO candidate.


=== Elections in 1950s ===
Note 1: In 1957, Singapore Malay Union (SMU) was expelled by its alliance partners consisted of UMNO and MCA for fielding a candidate in that by-election which was the reason for the elections department of Singapore to view Fatimah as another independent candidate.
Note 2: Lim Cher Kheng was the then incumbent seeking for another term. He represented the Democratic Party (Not to be confused with the Singapore Democratic Party, which was only formed after Singapore's independence.) which was dissolved by merging with Progressive Party (Singapore) as Liberal Socialist Party within a year from the 1955 General elections. With that consideration, the vote swing for both independent candidate Lim and Liberal Socialist Party candidate Wee will be taken from Lim's previous election result because that is the result for the candidate himself and his party respectively.
Note 3: UMNO, MCA and MIC together with Singapore People's Alliance was informally formed as an alliance in 1961, where it still within this term of election which was the reason for the elections department of Singapore to view Abdul Rahman as a candidate for Singapore Alliance.


== See also ==
East Coast GRC
Aljunied GRC


== References ==
1991 GE's result
1988 GE's result
1984 GE's result
1980 GE's result
1976 GE's result
1972 GE's result
1968 GE's result
1963 GE's result
1959 GE's result
1955 GE's result
1951 GE's result
Brief History on Singapore Malay Union (Dissolved in 1960s)
Brief History on Democratic Party (Dissolved in 1956)
Brief History on Singapore Alliance