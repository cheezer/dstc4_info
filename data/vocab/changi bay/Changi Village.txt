Changi Village (Chinese: 樟宜村) is a modern village at the north-eastern end of Singapore. The Government of Singapore has pledged in 2005 that they would try to revive the "ghost town".


== Amenities ==


=== Hotel ===

There is a hotel located at Changi Village named Changi Village Hotel. It is re-opened after a complete refurbishment. There are also a couple places to stay overnight on Pulau Ubin island, the Celestial Resort and Singapore Villa.


=== Bus Terminal ===
A bus terminal, Changi Village Bus Terminal is located here. One way you can reach Changi Village via public transportation is by taking the #2 bus from Tanah Merah MRT subway station. The bus ride then takes around 45minutes. You could take a cab from central Singapore for around S$20 as of Summer 2014.


=== Ferry Terminal ===

A ferry terminal, named Changi Point Ferry Terminal, is located here. There are small passenger ferries which lead to the north-eastern islands (e.g. Pulau Ubin) and also to some destinations in Johor, Malaysia. The popular bumboat ferry ride to Pulau Ubin costs S$2.50 each way and takes around 15 minutes. Usually there is a short wait until each bumboat ferry has 12 passengers.


=== Hawker Centre ===

A hawker center is located here. It is located right next to the bus and ferry terminals. The hawker center is famous for its nasi lemak stall, where long queues are often seen. Stalls selling delicious wanton(fried fritters) which are recommended by many television programs and companies are also available here. Many residents living in the nearby estates of Pasir Ris, Tampines and Simei drive to Changi Village for drinks in the evening. Many of the coffeeshops are open till past midnight everyday. As of June 2012, the centre is closed for renovations. It has since reopened and business appears to be going very well as of August 2014. There is a great variety of food to choose from at the hawker stalls.


=== Sea Sports Club ===
There is also a People's Association Water Venture that offers kayaking courses and rentals.


=== Walkway to Changi Beach ===
There is a walkway to the nearby Changi Beach Park, where many families gather over the weekend for a nice picnic with a view of the nearby islands.


== See also ==
Changi
Changi Village Bus Terminal
Changi Boardwalk