Changi Prison (Chinese: 樟宜监狱; Malay: Penjara Changi; Tamil: சாங்கி சிறைச்சாலை) was a prison located in Changi in the eastern part of Singapore. It has since been relocated as Changi Prison Complex and Changi Women's Prison and Drug Rehabilitation Centre.


== History ==


=== First Prison and POW Camp ===

Changi Prison was constructed by the British administration of the Straits Settlements as a civilian prison, in 1936.
During World War II, following the Fall of Singapore in February 1942, the Japanese military detained about 3,000 civilians in Changi Prison, which was built to house only 600 prisoners. The Japanese used the British Army's Selarang Barracks, near the prison, as a prisoner of war camp, holding some 50,000 Allied—predominantly British and Australian—soldiers and, from 1943, Dutch civilians brought over by the Japanese from the islands in the Dutch East Indies (now Indonesia). Although POWs were rarely, if ever, held in the civilian prison, the name Changi became synonymous with the POW camp in the UK, Australia, The Netherlands, and elsewhere.
About 850 POWs died during their internment in Changi during the Japanese Occupation of Singapore, a relatively low rate compared to the overall death rate of 27% for POWs in Japanese camps. However, many more prisoners died after being transferred from Changi to various labour camps outside Singapore, including the Burma Railway and the Sandakan airfield.

Allied POWs, mainly Australians, built a chapel at the prison in 1944 using simple tools and found materials.Stanley Warren of the 15th Regiment, Royal Regiment of Artillery Stanley Warren painted a series of murals at the chapel. Another British POW, Sgt. Harry Stodgen built a Christian cross out of a used artillery shell. After the war, the chapel was dismantled and shipped to Australia, while the cross was sent to the UK. The chapel was reconstructed in 1988, and is now located at the Royal Military College, Duntroon, Canberra.
After the war, the prison was used to hold former Japanese staff officers, Kempeitai, police, and guards from concentration camps. Executions were conducted in the inner yard where three gallows were erected. British soldiers were stationed there as prison guards. On 17 Oct. 1945, all 260 German seamen of the former U-Boats were moved from Pasir Panjang to the prison. On 26 June 1946, all German soldiers and civilians were notfied they would be shipped back to England on a passenger liner, the Empress of Australia. 


==== Kempeitai ====
The prison also contained the headquarters of the Kempeitai, the Japanese military police. The Kempeitai tortured prisoners there, who they suspected were spies.


=== Changi Chapel and Museum ===

In 1988, Singapore built a replica chapel (which was built by the POWs) and museum next to the Changi Prison. When Changi Prison was expanded in 2001, the chapel and museum were relocated to a new site 1 km away and the Changi chapel and museum were officially established 15 February 2001.


== Present Day ==

In 2000, the prison was demolished and its inmates were relocated to the new consolidated prison complex in a neighbouring site. In view of its historical significance, the Preservation of Monuments Board worked with the Singapore Prison Service and the Urban Redevelopment Authority to allow the front gates of the old prison to be preserved and moved to the new prison. Earlier in 1994, Changi Women's Prison and Drug Rehabilitation Centre was opened.


== Past Prominent Detainees ==


=== Prisoners of War ===
Sir Norman Alexander Professor of Physics, Raffles College, Singapore, Vice-Chancellor, Ahmadu Bello University, Nigeria. Helped build a salt evaporation plant at Changi and a small industrial plant that fermented surgical spirit and other products for prison hospital.
Sir Harold Atcherley, businessman, public figure and arts administrator.
Geoffrey Bingham, AM, MM, (6 January 1919 – 3 June 2009), who returned to Australia and wrote several books reflecting on his experiences, including his conversion to Christian faith in The Story of the Rice Cakes, Angel Wings, and Tall Grow the Tallow Woods.
Russell Braddon, (25 January 1921 – 20 March 1995), Australian writer, who wrote "The Naked Island" about his POW experience.
Sheila Bruhn (née Allan), who wrote about her experiences in Diary of a Girl in Changi.
Anthony Chenevix-Trench (10 May 1919 – 21 June 1979), Headmaster of Eton College, 1964–1970.
James Clavell is one of the most famous survivors; he wrote about his experiences in the book King Rat.
Eugene Ernest Colman, chess master.
John Coast British, (30 October 1916 – 1989), writer and music promoter. He wrote one of the earliest and well-known POW memoirs of Changi The Railroad of Death, (1946). Coast admitted that he and his fellow officers regularly stole coconuts during the night to alleviate their hunger. Other works of Coast include Dancers of Bali (1953), and Dancing Out of Bali (1954).
Hugh Edward de Wardener, British, CBE, MBE, (8 October 1915 – 29 September 2013), physician and professor of medicine at Charing Cross Hospital. He was a member of the Royal Army Medical Corps. He operated a Cholera Ward at the prison hospital. He also treated British soldiers who were forced to build the Burma Railway, as fictionalized in the film The Bridge on the River Kwai (1957). He was a gifted renal research physicin having made many breakthroughs in that area. Although he attained the advanced age of nearly 98, in his last months he suffered from peripheral neuropathy, a legacy of Changi. He died in the De Wardener Ward, the intensive care renal unit at Hammersmith Hospital, Acton, London, England. Grandson of Rudolf Baron de Wardener
Lieutenant Colonel Sir Ernest Edward "Weary" Dunlop, AC, CMG, OBE (12 July 1907 – 2 July 1993) was an Australian surgeon who was renowned for his leadership
Carl Alexander Gibson-Hill, medical doctor and Director of the Raffles Museum.
John Hayter, Anglican priest who later wrote of his experiences in Priest in Prison.
Percy Herbert, actor.
Sir Percy McElwaine, the Chief Justice of the Straits Settlement.
Jim Milner AM (1933–1937)—Former chairman Washington H. Soul Pattinson and former President NRMA.
Sir Alexander Oppenheim, mathematician. In 1984, he published "The prisoner's walk: an exercise in number theory", based in part of his experiences at Changi.
Lieutenant-General Arthur Ernest Percival, commander of Allied forces in Singapore, following his surrender to the Japanese; he was moved to a camp in China in late 1942.
Sydney Piddington, postwar Australian mentalist entertainer with wife Leslie, "The Piddingtons" ABC and BBC radio and stage mindreading team, who developed his verbal code in Changi.
Rohan Deakin Rivett, Australian, writer (16 January 1917 – 5 October 1977) War correspondent and journalist with Malaya Broadcasting Corporation in Singapore. Formerly a soldier in the Australian Imperial Force. Although the British surrendered the island on 15 February 1942, he was not captured until 8 March, in Java, after harrowing journeys by sea and land. His harrowing experiences are vividly chronicled in Behind Bamboo (1946). The book, 392 pages long, was written in October–November 1945 while its author was recovering from the rigours of captivity; reprinted eight times, it sold more than 100,000 copies.
Tjalie Robinson, (1911–1974), Dutch Indo-European (Eurasian) author, activist, journalist.
Ronald Searle, cartoonist.
Robert Skene, ten goal polo player.
Colonel Julian Taylor FRCS, surgeon.
Ernest Tipson, linguist.
Sir Michael Turner, Chief Manager of the Hong Kong and Shanghai Bank (1953-1962)
Leo Vroman, Dutch poet.
Stanley Warren, artist and art teacher; murals produced during his incarceration remain at the prison.
Ian Watt (9 March 1917 – 13 December 1999), literary critic, literary historian and professor of English at Stanford University.
Leonard Wilson, Bishop of Singapore, and later Bishop of Birmingham.
Michael Woodruff, surgeon and scientist.


=== Convicted criminals incarcerated after conversion from a wartime prisoner camp ===
Hiroshi Abe, Japanese war criminal
Nick Leeson, former derivatives broker convicted of rogue trading in the collapse of Barings Bank
Janatin and Harun Thohir, executed in 1968 for the MacDonald House bombing


== Changi in popular culture ==
King Rat (1962 novel), by James Clavell
King Rat (1965 film), based on the novel
Changi: The fortunes of a fictional group of Australian POWs were dramatised in this television miniseries, screened by Australian Broadcasting Corporation in 2001.


== See also ==

Changi Murals
Double Tenth Incident
Elizabeth Choy - Singaporean woman who smuggled radio parts and provisions to PoWs in Changi
John Mennie – prisoner who drew life in the camps and the Selarang Square Squeeze.
Kempeitai East District Branch
Selarang Barracks Incident


== References ==
^ Changi Heritage: Changi and the War
^ Journal
^ Military History Online
^ Giese, O., 1994, Shooting the war: The memoir and photographs of a U-boat officer in World War II, Annapolis: United States Naval Institute, ISBN 1557503079
^ The story of the Rice Cakes
^ Angel Wings
^ Tall Grow the Tallow Woods
^ Richmond, Caroline (29 October 2013). "News Science Medical research Hugh de Wardener obituary". The Guardian. Retrieved 30 December 2013. 
^ John Hayter Priest in Prison Tynron Press 1991 ISBN 1-85646-051-7
^ Who's Who in Australia (Crown Content Melb, 2007) pp 1444: Millner, James Sinclair (1919–2007)
^ http://adb.anu.edu.au/biography/rivett-rohan-deakin-11533


== External links ==
Singapore Prison Service| Changi Prison
Changi drawings (1942–1945) / John Noel Douglas Harrison (1911–1980)
Fong, Tanya. "New Changi Prison goes high-tech." The Straits Times: 16 August 2004. [1]
Choo, Johnson. "New technology at Changi Prison Complex allows focus on rehabilitation." Channel News Asia: 16 August 2004. [2]

List of U.S. internees at Changi Prison