Changi is an area at the eastern end of Singapore. It is now the site of Singapore Changi Airport/Changi Air Base, Changi Naval Base and is also home to Changi Prison, site of the former Japanese prisoner of war camp during World War II which held Allied prisoners captured in Singapore and Malaysia after Singapore's fall in February 1942.
James Clavell based his novel King Rat on his experiences as a World War 2 Allied prisoner of war at Changi Prison. In addition to this, Swee Sen is from Changi.


== Etymology ==

The early Malay place name of Changi was Tanjong Rusa, which is found in the 1604 E.G. de Eredia map of Singapore.
The native place name Changi is found very early in Singapore's history. In the 1828 map by Franklin and Jackson, the extreme southeastern tip of the island is referred to as Tanjong Changi. The local name Changi must have been a significant point for the Malays, especially in the days of the sixteenth century Johor kingdom located on the Johor River. Vessels using the Johor Straits would have to pass by Changi.
There are many versions for the etymological roots of the name Changi. One source says that it comes from a climbing shrub, the changi ular (Apama corymbosa), which grew in the area. Another claims that it gets its name from a tall tree, the chengai (Balanscarpus heimii), which abounded in the area in the early nineteenth century. Changi could also be a variation of the local timber named chengai. This heavy local timber is commonly used for buildings and furniture, and is valued for its strength and renowned for its deep rich colour.
During the early surveys in the 1820s to 1830s of Singapore island, Changi was also named Franklin Point after Captain Franklin who was involved as one of the early surveyors of Singapore island. It would seem that the colonial authorities had decided to latch on their own place name to the existing Malay name for Changi.
Around the 1900s, Changi was the favourite haunt for tigers. The female tigers were said to swim from Johore to Pulau Ubin to rest, before completing their journey to Singapore. The tigers would land at Ferry Point and give birth in this neighbourhood, and so the tigers on the island were generally young.


== Economy ==
Singapore Airlines is headquartered in Airline House, at Changi Airport in Changi. SilkAir also has its head office in Airline House. Singapore Airlines Cargo has its head office in the SATS Airfreight Cargo Terminal 5. Jetstar Asia, Scoot, and Valuair have their head offices in Terminal 1 of the airport. Tiger Airways has its head office in the Honeywell Building in Changi Business Park Central 1


== Education ==
The Japanese School Singapore Changi Campus is in the community.


== See also ==

Changi Beach Park
Changi Boardwalk
Changi Village
Singapore University of Technology and Design


== References ==


== Sources ==