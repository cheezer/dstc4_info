Char may refer to:
Char, the solid material forming during the initial stage of combustion of a carbonaceous material
River Char, a river in Dorset, England
Char (genus), a genus of fish
Arctic char, a cold-water fish in the Salmonidae family, native to Arctic, sub-Arctic and alpine lakes and coastal waters
A char in ANSI/ISO C is a value holding one byte (which was the size of a character in legacy encodings such as ASCII)
A floodplain sediment island in the Ganges Delta
A common slang term for tea throughout British Empire in the 19th and 20th centuries
Community Hebrew Academy of Richmond Hill, a Jewish secondary school in Toronto
A charwoman, an English house cleaner, usually part-time
In people:
Char Aznable, a character from the Mobile Suit Gundam series
Char Margolis, an American spiritualist
René Char, a French poet
In tanks:
Any French tank (from char d'assaut), but more specifically one with a short designation such as:
Char B1, a French heavy tank manufactured before the Second World War
Char 2C, a super-heavy French tank developed during the First World War
Char D1, a pre-World War II French tank
Char D2, a French tank of the Interbellum
Char G1, a French replacement project for the Char D2 medium tank


== See also ==
Charr (disambiguation)
Chars, commune in France