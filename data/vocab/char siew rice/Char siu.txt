Char siu (Chinese: 叉燒 cha1 shao1, literally "fork-roast"; also Romanised chasu, cha siu, caa siu, char siew) is a popular way to flavor and prepare barbecued pork in Cantonese cuisine. It is classified as a type of siu mei (燒味), Cantonese roasted meat.


== Meat cuts ==
Pork cuts used for char siu can vary, but a few main cuts are common:
Pork loin
Pork belly - produces juicy and fatter char siu
Pork butt (shoulder) - produces leaner char siu
Pork fat - fat
Pork neck end - very marbled (chu kang yuk)


== Chinese cuisine ==

Char siu literally means "fork burn/roast" (siu being burn/roast and char being fork, both noun and verb) after the traditional cooking method for the dish: long strips of seasoned boneless pork are skewered with long forks and placed in a covered oven or over a fire.
In ancient times, wild boar and other available meats were used to make char siu. However, in modern times, the meat is typically a shoulder cut of domestic pork, seasoned with a mixture of honey, five-spice powder, hóngfǔrǔ (red fermented bean curd), lao chou (dark soy sauce, 老抽), hoisin sauce (海鮮醬), red food colouring (not a traditional ingredient but very common in today's preparations and is optional), and sherry or rice wine (optional). These seasonings turn the exterior layer of the meat dark red, similar to the "smoke ring" of American barbecues. Maltose may be used to give char siu its characteristic shiny glaze.
Char siu is typically consumed with starch, whether inside a bun (cha siu baau, 叉燒包), with noodles (cha siu mein, 叉燒麵), or with rice (cha siu fan, 叉燒飯) in fast food establishments, or served alone as a centerpiece or main dish in traditional family dining establishments. If it is purchased outside of a restaurant, it is usually taken home and used as one ingredient in various complex entrees consumed at family meals.


== Hong Kong cuisine ==
In Hong Kong, char siu is usually purchased from a siu mei establishment, which specializes in meat dishes—char siu pork, soy sauce chicken, white cut chicken, roasted goose, roasted pork, etc. These shops usually display the merchandise by hanging them in the window. As a result, char siu is often consumed alongside one of these other meat dishes when eaten as an independent lunch item on a per-person basis in a "rice box" meal. More commonly it is purchased whole or sliced and wrapped and taken home to be used in family meals either by itself or cooked into one of many vegetable or meat dishes which use char siu pork as an ingredient.


== Southeast Asian cuisine ==

In Malaysia, Singapore, Indonesia, and Thailand, char siew rice is found in many Chinese shāolà (烧腊) stalls along with roast duck and roast pork. It is served with slices of char siu, cucumbers, white rice and drenched in sweet gravy or drizzled with dark soy sauce. Char siu rice also popular food within Chinese community in Medan, Indonesia, it is more called as cha sio. Char siew rice can also be found in Hainanese chicken rice stalls, where customers have a choice of having their char siew rice served with plain white rice or chicken-flavoured rice, and the same choice of garlic chilli and soy sauces. Char siew is called mu daeng (Thai: หมูแดง, pronounced [mǔː dɛ̄ːŋ], "red pork") in Thailand.
In the Philippines, it is known as Chinese Asado and usually eaten with cold cuts or served stuffed in siopao.
Vegetarian char siu also exists. It can be found in vegetarian restaurants and stalls in South East Asian Chinese communities.


== Japanese cuisine ==

Japanese culture has adapted 叉燒 as chāshū. Unlike its Chinese variant, it is prepared by rolling the meat into a log and then braising it at a low temperature. The Japanese adaptation is typically seasoned with honey and soy sauce, without the red food colouring, sugar, or five-spice powder. It is a typical ingredient in rāmen.


== Pacific Rim cuisine ==
As a means of exceptional flavor and preparation, char siu applications extend far beyond pork. In Hawaii, various meats are cooked char siu style. The term char siu refers to meats which have been marinated in char siu seasoning prepared either from scratch or from store-bought char siu seasoning packages, then roasted in an oven or over a fire. Ingredients in marinades for char siu are similar to those found in China (honey, five-spice, wine, soy, hoisin, etc.), except that red food coloring is often used in place of the red bean curd for convenience. Char siu is used to marinate and prepare a variety of meats which can either be cooked in a conventional or convection oven (often not requiring the use of a fork or cha(zi) as traditional Chinese ovens do), on a standard barbecue, or even in an underground Hawaiian imu. In Hawaii, char siu chicken is as common as char siu pork, and various wild birds, mountain goat, and wild boar are also often cooked char siu style, as are many sausages and skewers.
As char siu grows in popularity, innovative chefs from around the world, especially chefs from around the Pacific Rim, from Australia to California, are using various meats prepared char siu style in their cuisines and culinary creations.


== See also ==
Spare ribs
Red cooking
List of pork dishes
Cookbook:Cha Shao
 Food portal


== References ==