"Chee Chee-Oo Chee (Sang the Little Bird)" is a popular song with music by Saverio Seracini (some sources give his first name as "Severio" but "Saverio" seems the consensus spelling), the original Italian language lyrics by Ettore Minoretti, and English lyrics by John Turner and Geoffrey Parsons, published in 1955.
A number of recorded versions were made in 1955, but the Perry Como/Jaye P. Morgan version was the biggest hit. The Como/Morgan recording (with Mitchell Ayres' orchestra), made on April 28, 1955, was released by RCA Victor Records as 78rpm catalog number 20-6137 and 45rpm catalog number 47-6137, with the flip side "Two Lost Souls." It reached #12 on the song charts in the United States.
The song has also been recorded by the Gaylords and by several other artists.


== References ==
^ a b "Chee Chee Oo Chee" on the Perry Como Discography site
^ RCA Victor Records in the 20-6100 to 20-6499 series