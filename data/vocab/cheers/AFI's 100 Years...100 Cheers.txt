100 Years…100 Cheers: America's Most Inspiring Movies is a list of the most inspiring films as determined by the American Film Institute. It is part of the AFI 100 Years… series, which has been compiling lists of the greatest films of all time in various categories since 1998. It was unveiled on a three-hour prime time special on CBS television on June 14, 2006.
The announcement of the series was made on November 16, 2005, and a ballot of 300 films was released to a jury of over 1,500 cinematography leaders.


== The list ==


== Criteria ==
Feature-length film: Narrative format, typically over 60 minutes long.
American film: English language film with significant creative and/or production elements from the United States.
Cheers: Movies that inspire with characters of vision and conviction who face adversity and often make a personal sacrifice for the greater good. Whether these films end happily or not, they are ultimately triumphant—both filling audiences with hope and empowering them with the spirit of human potential.
Legacy: Films whose "cheers" continue to echo across a century of American cinema.


== External links ==
AFI's 100 Years...100 Cheers
List of the 100 winning cheers films.
List of the 300 nominated films.