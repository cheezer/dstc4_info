Cheers is an American television sitcom.
Cheers or CHEERS may also refer to:


== Cultural conventions ==
Toast (honor), a social drinking ritual
"Cheers", a valediction


== Related to the U.S. TV series ==
Cheers (Spain), a Spanish remake of the American series
Cheers Beacon Hill, formerly the Bull & Finch Pub, a bar in Boston, Massachusetts, used for exterior shots of the sitcom bar


== Music ==
Cheers (album), a 2003 album by Obie Trice
Cheers, a 1959 album by Burl Ives
"Cheers (Drink to That)", a 2011 single by Rihanna


== Other uses ==
Cheers (proa), a Polynesian-inspired sailboat designed by Dick Newick in 1967
Children's Environmental Exposure Research Study (CHEERS)


== See also ==
All pages with titles containing "Cheers"
All pages beginning with "Cheers"
Cheering
The Cheers, an American vocal group in the 1950s