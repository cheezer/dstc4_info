Chef Boyardee, formerly Chef Boy-Ar-Dee, is a brand of canned pasta products sold internationally by ConAgra Foods. The company was founded by Italian immigrant Ettore "Hector" Boiardi in Cleveland, Ohio, U.S.A., in 1928.


== History ==

In 1924, Boiardi opened Il Giardino d'Italia restaurant at East 9th Street and Woodland Avenue in Cleveland, Ohio. The idea for Chef Boyardee came about when restaurant customers began asking Boiardi for his recipes. He opened a factory in 1928, moving production to Milton, Pennsylvania, ten years later, where enough tomatoes and mushrooms could be grown. He decided to name his product "Boy-Ar-Dee" to help Americans pronounce his name.

The U.S. military commissioned them during World War II for the production of army rations, requiring the factory to run 24/7. After the war, instead of reducing production, the company was sold to American Home Products in 1946 so that everyone working there would be able to keep their job. American Home Products turned its food division into International Home Foods in 1996. Four years later, International Home Foods was purchased by ConAgra Foods, which continues to produce Chef Boyardee canned pastas bearing Boiardi's likeness.


== Current products (canned or microwaveable) ==
Spaghetti and Meatballs (Jumbo, Mini, or Regular)
Beefaroni (Big, Regular, Whole Grain)
Ravioli (Beef (regular or mini), Cheese, Chicken)
Alfredo
Lasagna (Regular or Whole Grain)
Mini (ABCs & 123s, dinosaurs, ravioli, or spaghetti & meatballs)
Macaroni (Cheesy Burger Macaroni, Chili Mac, Mac & Cheese, Pizza Twist, Kickin' Sloppy Joe, or Cheeseburger Maxx)
Pizza Sauce
Pizza Maker (Cheese or Pepperoni)
Spaghetti Sauce


== References ==


== External links ==
Official site
Hector Boiardi (Encyclopedia of Cleveland History)
Gallery of classic graphic design featuring Chef Boyardee