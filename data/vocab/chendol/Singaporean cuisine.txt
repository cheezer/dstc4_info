Singaporean cuisine is diverse and contains elements derived from several ethnic groups, as a result of its history as a seaport with a large immigrant population. Influences include the cuisines of the native Malays and the largest ethnic group, the Chinese, as well as Indonesian, Indian, Peranakan, and Western traditions (particularly English and Portuguese-influenced Eurasian, known as Kristang). Influences from other regions such as Sri Lanka, Thailand, and the Middle East are also present.
In Singapore, food is viewed as crucial to national identity and a unifying cultural thread. Singaporean literature declares eating a national pastime and food a national obsession. Food is a frequent topic of conversation among Singaporeans. Religious dietary strictures do exist; Muslims do not eat pork and Hindus do not eat beef, and there is also a significant group of vegetarians. People from different communities often eat together, while being mindful of each other's culture and choosing food that is acceptable for all. There are also some halal restaurants catering to Muslim dietary preferences.
Other than Singaporean cuisine, it is also common in Singapore to find restaurants specialising in cuisine from a great variety of countries around the world.


== Hawker centres ==
When dining out, Singaporeans often eat at hawker centres, coffee shops or food courts rather than restaurants, due to its convenience, wide-range of options and affordability. These hawker centres are widespread, cheap and may feature hundreds of stalls in a single complex, with each stall offering its own specialty dishes. Well-known hawker centres include Lau Pa Sat and Newton Food Centre. Coffee shops are non-air conditioned versions of food courts and are commonly found islandwide, usually at the bottom of blocks of HDB flats.


== Singapore food internationally ==
Singaporean food is a significant cultural attraction for tourists and visitors. Some Singaporean dishes have become internationally known. In 2011, four Singaporean dishes were included in the list of 'World's 50 Most Delicious Foods (Readers' Pick)' — a worldwide online poll by 35,000 people held by CNN International. They are Hainanese chicken rice (13th), chili crab (29th), laksa (44th) and roti prata (45th).
Singaporean cuisine has been promoted as a tourist attraction by the Singapore Tourism Board. The Singapore Food Festival, held every year in July, is a celebration of Singapore's cuisine. The Overseas Singaporean Unit also organises Singapore Day in major cities around the world as a platform for Singaporeans living abroad. One of Singapore Day's major draws is the local Singaporean hawker food, which is prepared on-site by well-known hawkers specially flown in for the event.


== Common dishes and snacks ==


=== Chinese ===

The dishes that comprise "Singaporean Chinese cuisine" today were originally brought to Singapore by the early southern Chinese immigrants (Hokkien, Teochew, Cantonese, Hakka and Hainanese). They were then adapted to suit the local availability of ingredients, while absorbing influences from Malay, Indian, and other cooking traditions.
Most of the names of Singaporean Chinese dishes were derived from dialects of southern China, Hokkien (Min Nan) being the most common. As there was no common system for transliterating these dialects into the Latin alphabet, it is common to see different variants on the same name for a single dish. For example, Bah Kut Teh may also be called Bak Kut Teh, and Char Kway Tiao may also be called Char Kuay Teow.
Bak kut teh (Chinese: 肉骨茶; pinyin: ròu gǔ chá), pork rib soup made with a variety of Chinese herbs and spices
Bak Chang (Chinese: 肉粽; pinyin: ròu zòng), savoury glutinous rice dumplings, usually filled with pork, mushrooms and stewed egg, steamed in bamboo leaves. Chinese in origin, but a longtime favorite in Peranakan cuisine.
Bak chor mee / minced pork noodles (simplified Chinese: 肉脞面; traditional Chinese: 肉脞麵; pinyin: roù cuò miàn), egg noodles with minced pork or chicken and other ingredients, served dry or with soup. Usually the flat, tape-like mee pok noodle is used. This dish is a variation of fishball noodles.
Ban mian (simplified Chinese: 板面; traditional Chinese: 板麵; pinyin: bǎn miàn), hand-made flat noodles served with vegetables, minced meat, sliced mushrooms, and an egg in an anchovy (ikan bilis)-based soup. Noodle variations are common. "Ban mian" refers to flat, long noodles; mee hoon kuay (Chinese: 米粉粿; pinyin: mí fěn guǒ; literally: "rice vermicelli cake") refers to flat rectangular noodles; you mian (simplified Chinese: 幼面; traditional Chinese: 幼麵; pinyin: yòu miàn; literally: "thin noodles") refers to thin noodles.
Chai tow kway / carrot cake (simplified Chinese: 菜头粿; traditional Chinese: 菜頭粿; pinyin: cài tóu guǒ), also known as Char kway (Chinese: 炒粿; pinyin: chǎo guǒ), radish (or daikon) cakes that are diced and stir-fried with garlic, egg, chopped preserved radish, and sometimes with prawns. This dish comes in black (fried with sweet dark soy sauce) or white (fried into an omelette) versions, with a chili paste sometimes added.
Char kway teow (simplified Chinese: 炒粿条; traditional Chinese: 炒粿條; pinyin: chǎo guǒ tiáo), thick, flat rice flour (kuay teow) noodles stir-fried in dark soy sauce with prawns, eggs, beansprouts, fish cake, cockles, green leafy vegetables, Chinese sausage, and lard.
Chwee kueh (Chinese: 水粿; pinyin: shuǐ guǒ), steamed rice cake topped with preserved radish. Usually eaten for breakfast.
Drunken prawn (simplified Chinese: 醉虾; traditional Chinese: 醉蝦; pinyin: zuì xiā), prawns cooked with Chinese rice wine
Duck rice (simplified Chinese: 鸭饭; traditional Chinese: 鴨飯; pinyin: yā fàn), braised duck with rice cooked with yam and prawns. It can be served simply with plain white rice and a thick dark sauce, or with braised hard-boiled eggs, preserved salted vegetables, and hard beancurd (tau kua) on the side. Teochew boneless duck rice is a similar, but a more refined dish. The duck is deboned and sliced thinly, allowing the sauces to seep into the meat.
Har Cheong Gai (simplified Chinese: 虾酱鸡; traditional Chinese: 蝦醬雞; pinyin: xiā jiàng jī; literally: "shrimp paste chicken"), chicken wings fried in a batter with fermented shrimp paste
Hum chim peng (simplified Chinese: 咸煎饼; traditional Chinese: 咸煎餅; pinyin: xián jiān bǐng), a deep-fried bun-like pastry sometimes filled with bean paste
Kaya toast, a traditional breakfast dish. Kaya is a sweet coconut and egg jam which is spread over toasted bread. Combined with a cup of local coffee and a half-boiled egg, this constitutes a typical Singaporean breakfast.
Kuay chap / kway chap (Chinese: 粿汁; pinyin: guǒ zhī), a Teochew dish of flat, broad rice sheets in a soup made with dark soy sauce, served with pig offal, braised duck meat, various kinds of beancurd, preserved salted vegetables, and braised hard-boiled eggs
Min Chiang Kueh (Chinese: 面煎粿; pinyin: miàn jiān guǒ), a thick chewy pancake with a ground peanut and sugar filling. Other variations include grated coconut and red bean paste. This traditional snack also is served in blueberry, cheese, and chocolate varieties.
Pig's brain soup, a soup dish comprising pig brain with special herbs
Pig fallopian tubes, a dish comprising stir-fried pig Fallopian tubes with vegetables and sambal
Pig's organ soup (simplified Chinese: 猪杂汤; traditional Chinese: 豬雜湯; pinyin: zhū zá tāng; literally: "pig spare parts soup"), a soup-based variant of kuay chap
Popiah (simplified Chinese: 薄饼; traditional Chinese: 薄餅; pinyin: báo bǐng), Hokkien / Teochew-style spring roll or rolled crêpe, stuffed with stewed turnip, Chinese sausage, shrimp, and lettuce
Soon kway (Chinese: 笋粿; pinyin: sǔn guǒ), a white vegetable dumpling with black soy sauce
Turtle soup, soup or stews made from the flesh of the turtle
Vegetarian bee hoon (simplified Chinese: 斋米粉; traditional Chinese: 齋米粉; pinyin: zhāi mǐ fěn), thin braised rice vermicelli to which a choice of various gluten, vegetable, or beancurd-based delicacies may be added
You Tsia Kway油炸粿 (simplified Chinese: 油条; traditional Chinese: 油條; pinyin: yóu tiáo), fried dough crullers similar to those served in other Chinese cuisines around the world


=== Malay ===

Singaporean Malay dishes, influenced by the food of the Malay Peninsula, Sumatra, Java, and the Riau Islands, tend to be adapted to local tastes and differ from their counterparts in neighbouring countries. Although Malays are native to Singapore, most of the Malays in Singapore today are relatively recent immigrants from Indonesia and Malaysia and their descendants. Hence, Singaporean Malay cuisine features a unique set of influences, especially from Minang cuisine). Spices and coconut milk are common ingredients, although Chinese ingredients such as taupok (tofu puffs) and tofu (known as tauhu in Malay) have been integrated. Many Chinese and Tamil Muslim adaptations of the following dishes also exist.
Acar, pickled vegetables or fruits with dried chilli, peanuts, and spices. Indian and Peranakan versions can also be found.
Assam pedas, seafood and vegetables cooked in a sauce consisting of tamarind, coconut milk, chilli, and spices.
Ayam penyet, fried chicken dish consisting of fried chicken that is smashed with the pestle against mortar to make it softer.
Bakso, also Ba'so, meatballs served with noodles.
Begedil or Perkedel, mashed potato mixture that is fried into patties and eaten together with mee soto.
Belacan, not a dish in itself, but a shrimp paste commonly used in spice mixtures
Curry puff, also known as epok-epok, a flaky pastry usually stuffed with curry chicken, potato cubes, and a slice of hard-boiled egg. Sardines are sometimes used in place of chicken.
Dendeng paru, a dish of dried beef lung cooked in spices.
Goreng pisang, bananas rolled in flour, fried, and eaten as a snack.
Gudeg Putih, white jackfruit curry.
Gulai daun ubi, sweet potato leaves stewed in coconut milk
Keropok, deep fried crackers usually flavored with prawns, but sometimes with fish or vegetables.
Ketupat, rice cake that is steamed in a square-shaped coconut leaf wrapping and usually served with satay.
Lemak siput, shellfish cooked in a thick coconut milk-based gravy.
Lontong, compressed rice cakes (see ketupat) in a spicy vegetable soup.
Otak-otak / otah, spicy fish cake grilled in a banana leaf wrapping.
Pecel Lele, fried catfish served with chili paste.
Sambal, not a dish in itself, but a common chili-based accompaniment to most foods.
Satay, grilled meat on skewers served with spicy peanut sauce and usually eaten with ketupat, cucumber, and onions.
Soto ayam, a spicy chicken soup that features chicken shreds, rice cakes and sometimes begedil.
Tumpeng, is a cone-shaped rice dish with side dishes of vegetables and meat.


=== Indian ===

Like other Singaporean ethnic cuisines, Indian Singaporean Cuisine has been influenced by multiple cultural groups. Dishes from both North India and South India can be found in Singapore.
Appam, a fermented rice pancake.
Murtabak. Originating from the Middle East, this Indian-Muslim dish today consists of folded roti prata dough stuffed with spiced minced meat, onions, and egg and often served with curry.
Roti prata, a local evolution of the Pakistani and Indian paratha is popular for breakfast or supper. It is a fried bread pancake that is crispy on the outside and soft on the inside. The dough is flipped to attain the right texture, then cooked quickly on a greased stove and served with curry and sugar. A plethora of modern variations are available, including egg, cheese, chocolate, masala, durian, and even ice cream.
Soup kambing, a local Mamak (Tamil Muslim) dish of spiced mutton soup
Soup tulang, a local Mamak (Tamil Muslim) dish of mutton or beef leg bones stewed in a spicy red sauce. The bones are broken to allow the marrow to be eaten.
Tandoori, meat, usually chicken, marinated in a mixture of spices and yogurt and cooked in a clay oven
Dosa, rice and lentil pancake. Commonly served as a "masala" version that includes spiced potatoes and is served with different types of sambar.
Vadai, spicy, deep-fried snacks that are made from dhal, lentils or potato.


=== Cross-cultural ===
A number of dishes, listed below, can be considered as truly hybrid or multi-ethnic food.
Ayam buah keluak, a Peranakan dish of chicken stewed with spices and Southeast Asian black nuts (buah keluak).
Fish head curry, a dish created by Singapore's Malayalee (an Indian ethnic group from Kerala) community with some Chinese and Malay influences. The head of a red snapper (ikan merah, literally "red fish") stewed in curry consisting of varying amounts of coconut milk and tamarind juice, along with vegetables (okra and eggplant are common). Usually served with either rice or bread.
Kari lemak ayam, a Peranakan chicken curry with a coconut milk base
Kari debal, a Eurasian Singaporean curry dish with Portuguese and Peranakan influences. Includes chicken, cabbage, sausage, and bacon pieces stewed in a curry sauce.
Kueh pie tee, a thin and crispy pastry tart shell filled with a spicy, sweet mixture of thinly sliced vegetables and prawns.
Laksa, thick rice noodles (bee hoon) in a coconut curry gravy with prawn and egg. Sometimes chicken, tau pok (beancurd puffs) or fish cake may be added. Laksa is Peranakan in origin. A specifically Singaporean variant is Katong laksa; in this variant, raw or lightly blanched cockles are added and the noodles are cut with a pair of scissors so that they can be scooped up with a spoon.
Mee goreng, yellow egg noodles stir fried with ghee, tomato sauce, chilli, eggs, vegetables, and various meats and seafood.
Cereal prawns, prawns that have been stir fried with sweetened cereal.
Sambal kangkong, a dish of leafy green vegetables (water spinach) fried in sambal.
Tauhu goreng, fried bean curd with sweet sauce.
Tutu kueh, steamed rice flour pastries with a sweet shredded coconut / peanut filling


=== Seafood ===
Singaporeans also enjoy a wide variety of seafood including fish, squid (known as sotong in Malay), stingray, crab, lobster, clams, and oysters.
Popular seafood dishes include
Sambal stingray / hang hir (simplified Chinese: 魟鱼; traditional Chinese: 魟魚; pinyin: gōng yú), smothered in sambal and served on banana leaf, also known as ikan bakar in Malay.
Black pepper crab, hard shell crabs cooked in a black pepper sauce.
Chilli crab, hard shell crabs cooked in chilli sauce, usually served with man tou, or deep fried buns.
Oyster omelette, an oyster omelette mixed with flour and fried, served garnished with coriander.


=== Western cuisine ===
Singaporean-style "Western food" includes sirloin steak, chicken or lamb chops, fish and chips, mixed grills, baked beans, chicken pie, sausage rolls, fried chicken wings, and cheese fries, sometimes served on a hot metal plate. It can typically be found at "Western food" stalls in hawker centers and food courts.


== Fruit ==

A wide variety of tropical fruits are available all year round. By far the most well known is the durian, known as the "King of Fruits", which produces a characteristic odour from the creamy yellow custard-like flesh within its spiky green or brown shell. Durians are banned on public transport, elevators, certain hotels, and public buildings because of their strong odour.
Other popular tropical fruits include mangosteen, jackfruit, longan, lychee, rambutan, and pineapple. Some of these fruits also are used as ingredients for other dishes: iced desserts, sweet-and-sour pork, and certain types of salad such as rojak.


== Desserts ==

Singaporean desserts have a varied history. A typical food court or hawker centre dessert stall will usually have a large variety of desserts available, including:
Cheng tng, a light refreshing soup with longans, barley, agar strips, lotus seeds and a sweet syrup, served either hot or cold. It is analogous to the Cantonese Ching bo leung.
Ice kacang, a mound of grated ice on a base consisting of jelly, red beans, corn and attap seeds, topped with various kinds of coloured sugar syrups, palm sugar, rose syrup and evaporated milk.
Kuih or kueh, small cakes or coconut milk based desserts that come in a variety of flavors, usually containing fruit such as durian or banana. Pandan is a common flavouring.
Kueh lapis is a rich, multi-layered cake-style kueh using a large amount of egg whites and studded with prunes; Lapis sagu is also a popular kueh with layers of alternating color and a sweet, coconut taste.
Or-ni, a Teochew dish consisting of yam paste, coconut paste and gingko nuts.
Pulut hitam, a creamy dessert made of black glutinous rice and served with coconut cream.
Tau suan, mung daal beans in jelly, served hot with dough crullers.


== Drinks and beverages ==

Popular Singaporean drinks include:
Chin chow drink (Chinese: 仙草水; pinyin: xiān cǎo shuǐ), grass jelly made into a sweet beverage.
Lemon barley drink (柠檬薏米水)
Bandung, rose syrup with evaporated milk.
Sugar cane juice, usually blended to order from fresh sugar cane stalks.
Teh halia tarik, ginger tea with "pulled" milk (tarik)
Milo, chocolate/malt milk drink
Beer in Singapore


=== Local terms for coffee and tea ===

At kopi tiams (Chinese: 咖啡店; pinyin: kā fēi diàn; literally: "coffee shop"), coffee and tea are usually ordered using a specific vernacular featuring terms from different languages. "Kopi" (coffee) and "teh" (tea) can be tailored to suit the drinker's taste by using the following suffixes when ordering:
"Peng" (Chinese: 冰; pinyin: bīng; Pe̍h-ōe-jī: peng; literally: "ice"): with ice
"C": with evaporated milk
"Siew dai": less sugar
"O": black, no milk
"O Kosong" ("Kosong"="Nothing"): black, no sugar
"Gao": extra thick
"Poh": extra thin
"Milo": chocolate milk drink


== Singapore food publications ==
Makansutra, a guide devoted to Singaporean food with ratings of individual stalls, was launched in 1997 by KF Seetoh and now features both print and online editions. Another popular blog dedicated to Singaporean food is ieatishootipost. Many other Singaporean food blogs and review sites exist on the Internet.


== "Singaporean" dishes uncommon in Singapore ==
Singapore style noodles (Chinese: 星州炒米粉; pinyin: xīng zhōu chǎo mí fěn), an American Chinese dish featuring fried rice vermicelli flavoured with yellow curry powder, is not commonly found in Singapore. The close relative to this dish is fried bee hoon (thin rice noodles).
Singapore Sling. While the cocktail was invented in Singapore's Raffles Hotel, and is still served at the hotel's Long Bar, it is not common in most Singaporean bars.
Singapore fried kway tiao (simplified Chinese: 星州炒粿条; traditional Chinese: 星州炒粿條; pinyin: xīng zhōu chǎo guǒ tiáo), a dish featuring fried thick, flat rice noodles flavoured with dark soy sauce commonly available in some Chinese restaurants in Canada and the United States, is also not a Singaporean dish. The dish most resembling it is char kway teow.


== See also ==

Culture of Singapore
Cuisine of Malaysia
Hawker centre
Kopi tiam
Restaurants in Singapore


== References ==


== External links ==
Where to find the best hawker food in Singapore that are highly regarded by foodies
Singapore dining guide
Islamic Religious Council of Singapore Halal Dining Info
Singapore dining blog
Singapore's six must try foods
Famous Local Foods to Eat in Singapore