Cheng Chu Sian (Chinese: 钟础先; born 1 March 1986 in Kuala Lumpur) is an athlete from Malaysia, who competes in archery.


== 2008 Summer Olympics ==
At the 2008 Summer Olympics in Beijing Cheng finished his ranking round with a total of 660 points. This gave him the 26th seed for the final competition bracket in which he faced Matthew Gray of Australia in the first round, 109-101. With a win over Matti Hatava of Finland (110-103) he qualified for the third round and there he faced Lee Chang-Hwan of South Korea. Both archers scored 105 points in the regular match and had to go to an extra round. Here Cheng advanced with 19 points, while Lee hit 18 points. In the quarter finals Cheng was unable to beat Bair Badënov of Russia (109-104). Badënov went on to win the bronze medal.
Together with Wan Khalmizam and Muhammad Marbawi he also took part in the team event. With the 660 score from the ranking round combined with the 675 of Khalmizam and the 659 of Marbawi, Malaysia were in third position after the ranking round, which gave them a straight seed into the quarter finals. With 218-213 they were however eliminated by the Italian team that eventually won the silver medal.


== References ==