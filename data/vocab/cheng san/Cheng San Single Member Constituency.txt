Not to be confused with Cheng San Group Representation Constituency where the opposition veteran Joshua Benjamin Jeyaratnam made the last debut in his entire life and narrowly lost to the ruling party, People's Action Party.
Cheng San Single Member Constituency (Simplified Chinese: 静山单选区;Traditional Chinese: 靜山單選區) is a defunct single member constituency in Ang Mo Kio, Singapore; carved from Ang Mo Kio division in 1980. It was absorbed into the bigger Cheng San Group Representation Constituency prior to 1988 general elections.


== Members of Parliament ==
Lee Yock Suan (1980 - 1988)
Constituency Abolished (1988 – present)


== Candidates and results ==


=== Elections in 1980s ===


== See also ==
Cheng San GRC


== References ==
1984 GE's result
1980 GE's result