Pineapple Lumps are a flavoured chocolate covered confectionery with a soft, chewy pineapple-flavoured middle from New Zealand. They are often identified as Kiwiana. The semi-brittle shell of chocolate conceals a chewy, soft pineapple flavoured centre.


== History ==
The second Pineapple Lumps were made by the Regina Confectionery Company in Oamaru around 1952-54. Charles Diver, the confectionery chef and floor production manager at Regina, who would later create other classic kiwi sweets, was given the task of using up waste product from other lollies of the time. One sweet in particular, an early version of the chocolate fish with a banana flavoured marshmallow middle, had the most left overs.
Pineapple Lumps were originally called Pineapple Chunks, but the name was changed in the early 1960s to give them a more unique and original title.
The original Pineapple Chunks are now being manufactured again by Regina Lollies http://www.reginalollies.co.nz/portfolio/pineapple-chunks/
Pineapple Lumps are manufactured by Pascall, and typically come in 140g and 300g size packs.


== References ==