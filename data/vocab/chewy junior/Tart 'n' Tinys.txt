Tart 'n' Tinys were small, fruit-flavored candies distributed by Nestlé USA under the Wonka brand. Tart 'n' Tiny's came in five colors, bluish-purple (grape), yellow (lemon), orange (orange), red (cherry), and green (lime).
The original incarnation of Tart 'n' Tinys candies were small cylinders of compressed dextrose. The candy had a chalky appearance and consistency, with a firm crunch that would crumble in the mouth, similar to SweeTarts or Smarties. Along with Nerds and Wacky Wafers, Tart 'n' Tinys were top sellers for the Wonka company in the 1980s. In the 1990s the original candies were discontinued. A short time later, Wonka introduced Candy-coated Tart n Tinys, identical candies with a brightly colored candy coating. This candy was then marketed simply as Tart n Tinys. While the original version was hard in texture, a soft and chewy version of Tart 'n' Tinys was recently introduced, titled Chewy Tart 'n' Tinys, that has the same candy coating but with a chewy center. Tart 'n' Tinys have now been discontinued.


== Brand revival ==
In 2014, Leaf Brands, LLC acquired the Tart n' Tiny trademark and will have them back in stores the second quarter of the year. Leaf's focus is reintroducing the famous Tart n' Tinys candy as the original, uncoated product from the 1970s and 1980s, and not the later, hard-coated versions. The original flavors will be back as well, with new varieties of Tropical and sour soon after.
As of January 2015, Tart n' Tinys were made available to the public in both bulk and in 4.5 oz packages.


== References ==