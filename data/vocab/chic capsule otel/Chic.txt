Chic /ˈʃiːk/, meaning "stylish" or "smart", is an element of fashion.


== Etymology ==
Chic is a French word, established in English since at least the 1870s. Early references in English dictionaries classified it as slang and New Zealand-born lexicographer Eric Partridge noted, with reference to its colloquial meaning, that it was "not so used in Fr[ench]." Gustave Flaubert notes in Madame Bovary (published in 1856) that "chicard" (one who is chic) is then Parisian very current slang for "classy" noting, perhaps derisively, perhaps not, that it was bourgeoisie. There is a similar word in German, schick, with a meaning similar to chic, which may be the origin of the word in French; another theory links chic to the word chicane. Although the French pronunciation (shēk or "sheek") is now virtually standard and was that given by Fowler, chic was often rendered in the anglicised form of "chick".

In a fictional vignette for Punch (c. 1932) Mrs F. A. Kilpatrick attributed to a young woman who 70 years later would have been called a "chavette" the following assertion: "It 'asn't go no buttons neither ... That's the latest ideer. If you want to be chick you just 'ang on to it, it seems".
By contrast, in Anita Loos' novel, Gentlemen Prefer Blondes (1925), the diarist Lorelei Lee recorded that "the French use the word 'sheik' for everything, while we only seem to use if for gentlemen when they seem to resemble Rudolf Valentino" (a pun derived from the latter's being the star of the 1921 silent film, The Sheik).
The Oxford Dictionary gives the comparative and superlative forms of chic as chicer and chicest. These are wholly English words: the French equivalents would be plus chic and le/la plus chic. Super-chic is sometimes used: "super-chic Incline bucket in mouth-blown, moulded glass".
An adverb chicly has also appeared: "Pamela Gross ... turned up chicly dressed down".
The use of the French très chic (very chic) by an English speaker – "Luckily it's très chic to be neurotic in New York" – is usually rather pretentious, but sometimes merely facetious—Micky Dolenz of The Monkees described ironically the Indian-style suit he wore at the Monterey Pop Festival in 1967 as "très chic". Über-chic is roughly the mock-German equivalent: "Like his clubs, it's super-modern, über-chic, yet still comfortable".
The opposite of "chic" is unchic: "the then uncrowded, unchic little port of St Tropez".


== Quotes ==
Over the years "chic" has been applied to, among other things, social events, situations, individuals, and modes or styles of dress. It was one of a number of "slang words" that H. W. Fowler linked to particular professions – specifically, to "society journalism" – with the advice that, if used in such a context, "familiarity will disguise and sometimes it will bring out its slanginess."
In 1887 The Lady noted that "the ladies of New York ... think no form of entertainment so chic as a luncheon party."
Forty years later, in E. F. Benson's novel Lucia in London (1927), Lucia was aware that the arrival of a glittering array of guests before their hostess for an impromptu post-opera gathering was "the most chic informality that it was possible to conceive."
In the 1950s, Edith Head designed a classic dress, worn by Audrey Hepburn in the film Sabrina (1954), of which she remarked, "If it had been worn by somebody with no chic it would never have become a style."
By the turn of the 21st century, the travel company Thomas Cook was advising those wishing to sample the nightlife of the sophisticated Mediterranean resort of Monte Carlo that "casual is fine (except at the Casino) but make it expensive, and very chic, casual if you want to blend in."
According to American magazine Harper's Bazaar (referring to the "dramatic simplicity" of the day-wear of couturier Cristóbal Balenciaga, 1895–1972), "elimination is the secret of chic."


== See also ==

List of chics


== References ==