The Central Area or Central Business District (CBD) contains the core financial and commercial districts of Singapore, including eleven urban planning areas, namely Downtown Core, Marina East, Marina South, Museum, Newton, Orchard, Outram, River Valley, Rochor, Singapore River and Straits View as defined by the Urban Redevelopment Authority (URA). Part of the Central Region in the southern part of Singapore, it includes high value land intensely regulated by the URA's urban planning initiatives. It approximately equates to the area which may be referred to as the city despite Singapore being a city in itself.
Singapore River which currently empties into Marina Bay by the Merlion, is a major landmark in this Central Area. The river originally emptied into the Singapore Straits, the main maritime activity site for the colony. The commercial areas which developed on the south banks became the central business district for post-independence Singapore (also known as Golden Shoe). URA groups these areas of commercial activity and calls it the Central Area.
The Central Area has since been expanded by the Government of Singapore and the URA to include the land reclamation of Marina Bay. Many construction projects have been completed on these reclaimed lands with many more still under consolidation or development.


== HistoryEdit ==

Much of the central area bounded by Telok Ayer Road, which has a high concentration of skyscrapers is built on reclaimed land. Therefore the Thian Hock Keng Temple built in 1839 along Telok Ayer Road used to face the sea and it was visited by Chinese immigrants giving thanks to Ma Zu (Goddess of the Sea) for their safe voyage. Prior to 1839, the temple served as a joss-house for the Hokkien immigrants. This temple, which is also the first Hokkien temple to be built by the Hokkien clan under the leadership of Tan Tock Seng and Si Hoo Keh, is in the architectural style of southern China, using only materials imported from China and supported with no nails. The clan’s office was housed there and this temple was also used as a meeting venue. The construction of the temple was completed in 1842 and details of this temple’s history are recorded in granite tablets found on the wall inside the Entrance Hall.A plaque inscribed with the words Bo Jing Nan Ming (Gentle Waves over the South Seas) which was presented by the Guangxu Emperor in 1907, is found in this temple providing evidence of the temple’s stature. The temple comprises an architectural masterpiece of stone, tiles and wood, dragons and phoenixes, amazing cravings, sculptures and imposing columns.  The Thian Hock Keng temple has been gazetted as a national monument in 1973.

The adjacent placement of places of worship of various religions (such as the Thian Hock Keng Temple, Nagore Durgha, Singapore, the Central Christian Church and Al Abrar Mosque) and demonstrate the peaceful coexistence of the early multi-ethnic and multi-cultural communities in Singapore.
At Church Street is the Yueh Hai Ching Temple, one of the oldest Taoist temples, having been built in 1826. This temple was established by a group of Teochew settlers from Guangzhou in China when they dedicated a shrine to Tian Hou. This temple faced the sea and was therefore a place where newly arrived Chinese immigrants (sailors and travellers included) came to offer their thanks to the Goddess for their safe voyage across the seas. The renovation to the temple was undertaken by the Ngee Ann Kongsi, which was formed by a group of immigrants from the Teochew community. This temple served as a meeting place for the people of the Teochew community in the late 19th century when the Teochews became the second largest Chinese Dialect group in Singapore. The temple was honoured with a bian e, an imperial signboard from Emperor Guang Xu of the Qing Dynasty in 1907. In 1996 it was gazetted as a national monument and it underwent repairs and restoration works. This temple is a silent testimony to the faith and gratitude of many people in Singapore. The background contrast between the old temples and the new modern buildings indicate the juxtaposition and coexistence of new and old. The temple has a cover structure composed of models depicting the lives of Chinese villagers. The then-emperor of China also presented the temple with a signboard that is still displayed in the temple till today. The temple is separated into two sections; each section caters for the different people coming from the different parts of China.
The Armenian Church, located at Hill Street, is the oldest church in Singapore. The funding for the building of the church came from contributions of the Armenian community. It was built in the 1830s by George Coleman, the architect of many buildings in Singapore, for a small Armenian community that once resided in Singapore. The Church was credited as a national monument.
CHIJMES, located at Victoria Street, is currently converted into a modern dining area with restaurants. It was originally a Catholic Convent which was convented. The original structure was well preserved and the chapel is still present till today. The restaurants resides in the old structures of the convent.
The closeness of the locations of various religions worship indicated the peaceful coexistence of the early multi-ethnic and multi-cultural communities in Singapore.
The streets in the CBD area have a long history which showed a very different CBD as compared to the present. History past, Ang Siang Hill was the site of remittance houses where the Chinese immigrants go to when they wanted to remit money back home. Sago Lane (Street of the Dead) was formerly used as a place for people to die with the funeral parlor on the ground floor and living areas on the second floor. This area was infamous because of the large number of deaths found there. Although the Sago Street is referred to as the Street of the Dead, the funeral parlor was actually located in the Sago Lane. Sago Lane runs parallel to the Sago Street.


== Area descriptionEdit ==

Before independence, the central business district in the Central Area consisted of what is now the Downtown Core, Museum, Newton, Orchard, Outram, River Valley, Rochor, and the Singapore River planning areas. These included areas such as Dhoby Ghaut and Raffles Place. These planning areas were themselves designated and set apart only recently. Each of these districts have a heavy and dense commercial presence, especially in the Downtown Core, Orchard and Singapore River planning areas. Rochor, Newton, River Valley and Outram are commercially thriving, but have fewer skyscrapers and generally include a more substantial residential presence. Schools, condominiums and Housing Development Board apartments are located in these areas, albeit at higher prices.
The area is more densely packed than other parts of Singapore, and a great number of Mass Rapid Transit (MRT) stations tend to be concentrated in this area, especially interchange stations and stations along the Circle MRT Line.
In an attempt to expand the Central Area from the 1970s, the government of Singapore and the Urban Redevelopment Authority have reclaimed land portions from Marina Bay. Newly created portions of land surrounding Marina Bay have been organised and labelled into Marina East, Marina South, Straits View, with their own separate plans. The development of the reclaimed land surrounding Marina Bay, such as the construction of infrastructure, was similar to that of Jurong and Jurong Island, with the exception to use the land for commercial purposes. Many construction projects have been completed on the reclaimed lands since their creation, but much of it is still under consolidation or development.


=== Land useEdit ===

Over the course of its history, dating back to as far as the 18th Century, the land use of the various regions of Singapore have changed over time. The mixed land use in the central area is unlike other regions in Singapore, housing both the old and the new side by side.
Shophouses on Ann Siang Hill, for example, have changed pueposesover the years. In the past, these shophouses were used as clan houses for the various clans that existed in the Chinatown region. Due to a housing shortage exacerbated by the destruction of World War II, an HDB paper estimated that in 1966, 250,000 lived in squalid shophouses in the Central Area.
However, the regions closest to the core of the central area have continued to be upgraded and developed. In this region, though the physical structure may remain the same, the use has now been changed. Though there are still various clan houses there, alongside them are new offices and shops. Some shophouses have even adapted to house both the traditional and modern land uses in the same complex.
In the city centre, there are more skyscrapers and fewer shophouses. Despite the fact that there is a height limit of 280m, these skyscrapers are so dense that the density of offices here is among the highest in Singapore. There are several clusters throughout the region for the different business sectors – there are areas dedicated to the offices in fields of law and banking.


=== Significant landmarksEdit ===


==== CommercialEdit ====

Singapore's Central Business District comprises the core of Singapore's financial activities and thus includes many important and significant financial buildings. There are many skyscrapers in the Central Business District. Skyscrapers in the city include the relatively new ones such as the UOB building and the Sail at Marina Bay, as well as the older but still well maintained and used buildings such as the Swissôtel The Stamford, which once held the title as the world's tallest hotel.
Majority of Singapore's tallest building are found in the CBD, where land are the most expensive in Singapore. However, due to aviation restrictions, buildings in the CBD cannot go higher than 280m. Most of the iconic building that represents Singapore CBD skyline also stand on free hold land, which are extremely uncommon in Singapore today.
Some commercial buildings reflect the cultural Chinese heritage of Singapore in addition to the presence of other cultural heritage sites in the Central Area. The UOB Plaza makes use of the traditional Chinese symbol of the octagon in its design, which supposedly brings about good Feng Shui according to traditional Chinese geomancy.


==== CulturalEdit ====

The majority of Singapore’s tallest buildings are found in the Central Business District (high-valued land), but the skyscrapers cannot go higher than 280 metres due to air traffic regulations. Also, most of these buildings are built on freehold land, relatively uncommon in Singapore.
Some commercial buildings, however, do reflect the cultural Chinese heritage of Singapore. This is added to the presence of other cultural heritage sites in the Central Area. An example is the UOB Plaza, which makes use of the traditional Chinese symbol of the octagon in its design, which in turn is supposed to bring in good Feng Shui according to traditional Chinese geomancy.
Singapore's Central Area possesses some important cultural sites. Historical shophouses with their original façades preserved can be found along streets like Club and Ann Siang Streets. These shophouses have, however, been largely gentrified, displacing many of the original tenants, resulting in a loss of certain traditional trades such as crafts.
These shophouses were preserved as part of URA efforts to preserve the nation's cultural heritage. The façades are intact but the businesses operating inside today are now different, with law firms, design companies, insurance firms and bars and restaurants. These new companies fit into the old structures without too much alteration to the original façade. There are also shophouses that continue to serve as local clubhouses. Dates are present on the shophouses themselves to indicate date of construction, mostly in the 19th century.

Even though the shop houses today are filled with modern businesses and people, the cultural practices are still strong along these streets; for example, the hungry ghost festival, in which one can often see many shops laying out tables outside their shop house and offering prayers.
The Central Business District was the original location for many schools in Singapore, such as Anglo-Chinese School, Raffles Institution and Raffles Girls' School, Yeung Ching (Yangzheng) Primary School and Tao Nan School. These schools have since moved to newer locations.

The Asian Civilisations Museum lies near the bank of the Singapore River, and it is a cultural landmark that has been preserved since colonial times. This building is otherwise known as the Empress Place Building. It was converted to a museum in 2003. The Peranakan Museum is also nearby along Armenian Street. This building is the Old Tao Nan School, and since early 2008 has been specialising in Peranakan culture.
The CBD is very much rooted to the cultural traditions of the Chinese, such as the Chinese geomancy or Feng Shui, which is present in many buildings. The Furama City Hotel, for example, has a roofline which resembles the open palm of the hand to ward off evil. The ‘Fountain of Wealth’ at the Suntec Tower has the inward flowing water at the fountain for two reasons. Water is the symbol of life and wealth in Chinese culture and the inward motion of the water signifies the retention of wealth for Suntec City. According to feng shui experts, the water which flows inwards represents riches pouring in, thus the fountain is appropriately named ‘Fountain of Wealth’.


== TransportationEdit ==

There has always been a high flow of traffic within the CBD due to its identity as the core of the financial centre in Singapore, resulting in a high density of offices in this area of the city. This leads to a large amount of people commuting to and from the city to work everyday. In a bid to prevent congestion in the city, the Government has implemented several different measures to curb the problem. For example, 5 more ERP gantries were erected in the CBD since July 2008. The operating hours of these gantries are also tweaked to accurately reflect the traffic situation in the CBD. A multistory automated car park has also been constructed to allow workers around the area to park their cars with a peace of mind and it is also very convenient as the driver does not have to waste the time to find a parking lot.

SBS Transit and SMRT also offer bus services from the various parts of Singapore to the CBD via direct bus services. A new scheme of premium bus services running during the morning and evening peak hours provide quick bus connection downtown.

Singapore's rapid rail transit network, the MRT, calls at several stops downtown, including City Hall, Raffles Place and Tanjong Pagar. All five rail lines in Singapore run through the city area, which contains multiple rail interchanges between the East West Line, the North South Line, the North East Line, the Circle Line and Downtown Line A railway station in Tanjong Pagar once provided a rail link to Malaysia to the north, but is no longer in operation.


== Future DevelopmentEdit ==
Because of its history dating back to the origins of the country, the area also contains several buildings of intrinsic and immense cultural value. These include shophouses and temples, some of which are still used by historical groups in Singapore. (i.e. Hokkien clan associations) Some of these buildings are being preserved but put to good economic use in the process, as renovation works in the interior of these buildings allow them to be used for other purposes while preserving their historical and architectural value. Other examples of buildings put to such use in Singapore include the Asian Civilisations Museum and the Peranakan Museum.
There are also recreational facilities planned for development in the Central Area. These include the Gardens at the Bay, the Sports Hub, and the Fullerton Heritage. These will have significant impact on the recreational and sporting scenes in Singapore upon completion. Finally, the largest project in the area would be the Marina Bay Sands Integrated Resort along the waterfront of Marina Bay.


== See alsoEdit ==
Ann Siang Hill
Integrated Resort
Architecture of Singapore
Transport in Singapore
History of Singapore
Singaporean cuisine
Singapore River
Geography and climate of Singapore
Tourism in Singapore
Culture of Singapore


== ReferencesEdit ==


== Further readingEdit ==
Planning Areas Boundaries Map A, showing extent of the Central Area
LTA, (17 March 2008). Land Transport Authority- What's new. Retrieved 24 August 2009, from Land Transport Authority Web site: http://app.lta.gov.sg/corp_press_content.asp?start=1902
LTA, (Land Transport Authority) (22 October 2007). Taxi Stands in CBD. Retrieved 24 August 2009, Web site: http://taxisingapore.com/taxi-stands/
Tanjong Pagar Transformation. 20 April 2012, Website: http://www.eonatshenton.com/Case-Study/Tanjong-Pagar-The-Next-Waterfront-City/
The Central Business District – Singapore’s New Downtown. 22 October 2013, Website: http://alexresidencescondo-sg.com/news-updates/cbd-singapore-new-downtown/
(2006). New Page 1. Retrieved 24 August 2009, from CBD Web site: [2]