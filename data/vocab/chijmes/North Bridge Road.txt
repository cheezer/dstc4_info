North Bridge Road (Chinese: 桥北路) is a road in Singapore north of Singapore River, that starts at the junction of Crawford Street and ends before Elgin Bridge, which the road becomes South Bridge Road. The road is one of the oldest roads in Singapore and was outlined in Raffles' 1822 Town Plan. North Bridge Road was constructed by GD Coleman between 1833 and 1835, and built by convict labour. North Bridge road was a route for trolley buses and later "mosquito buses" until 1962, when the current motor bus system was implemented. North Bridge Road was called The Big Horseway in the past.


== Landmarks ==
Istana Kampong Glam (Malay Heritage Centre)
Masjid Sultan
Raffles Hospital
Parkview Square
Bugis Junction
National Library
Bras Basah Complex
Raffles Hotel
Raffles City
CHIJMES
City Hall MRT Station
Capitol Building
Saint Andrew's Cathedral
Peninsula Plaza
Funan DigitaLife Mall
The Treasury
Supreme Court
Parliament House