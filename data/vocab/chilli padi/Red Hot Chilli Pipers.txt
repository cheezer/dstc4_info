The Red Hot Chilli Pipers are an ensemble consisting of pipers, guitarists, keyboards and drummers formed in Scotland in 2002. They entered and won the BBC talent show When Will I Be Famous? Their name is a play on the American band the Red Hot Chili Peppers.
In 2004, the group appeared on the main stage at T in the Park with the headline band, the rock group The Darkness. They appeared on BBC Radio 1 on the Greg James show in July 2013 and The Radio 1 Breakfast Show with Nick Grimshaw in 2014.
The band currently has Steven Graham – a seven time world champion snare drummer. Three members graduated from the Royal Scottish Academy of Music and Drama.


== History ==
Since their formation the Red Hot Chilli Pipers have combined guitars, keyboards, drums and their bagpipes to create 'bagrock' sound. The Red Hot Chilli Pipers perform a fusion of traditional pipe tunes and contemporary pieces. Notable covers performed have included "We Will Rock You" by Queen, "Clocks" by Coldplay and "Smoke on the Water" by Deep Purple.
The band has released 5 studio albums as of 2013, of which their first, their 2005 release The Red Hot Chilli Pipers, was the least successful. Their 2007 album Bagrock to the Masses went platinum in Scotland and silver in Great Britain. Their third album and first live album, Blast Live (2008), went triple platinum in Scotland. Their fourth release Music for the Kilted Generation, however, is The Red Hot Chilli Pipers' best International record, reaching Number 2 on the US Amazon Chart. and was only held off of top spot due to Adele's record-breaking album, 21..
Their album titled Music for the Kilted Generation, is a parody of Music for the Jilted Generation by The Prodigy.
The band signed a contract in December 2012 for a new album to be released in 2013, including tracks from bands like Coldplay and Journey. Breathe was released in July 2013 on CD and iTunes.
The group appears on the soundtrack of How to Train Your Dragon 2.


== Discography ==
2005 – The Red Hot Chilli Pipers
2007 – Bagrock to the Masses
2008 – Blast Live
2010 – Music for the Kilted Generation
2013 – Breathe


== Tours and performances ==

The group's highest profile was on the Main Stage at T in the Park in 2004 where they performed alongside The Darkness.


=== When Will I Be Famous ===
In 2007, the Red Hot Chilli Pipers appeared on the BBC show When Will I be Famous? hosted by Graham Norton. The band were in the episode and competed against seven others. The eight contestants were paired into a head-to-head showdown in which the winner would be decided by 101 preregistered viewers who were dubbed the "Armchair Judges". The four winners of these head-to-heads would then compete against each other in the second show with the winner being decided by an open public phone vote. In their first head-to-head showdown, The Red Hot Chilli Pipers were pitted against the "Stringfever", a four-member string quartet. The Red Hot Chilli Pipers won the head-to-head 51-50 and went on to win the weekly prize of ten thousand pounds.


=== After TV Appearance ===
In 2010, The Red Hot Chilli Pipers completed an 11-date sold-out tour of Scotland, as well as an 11-week tour of Germany and a 7-week tour of America. They also toured Saudi Arabia, Malaysia, India and most of Europe. They performed concerts in New York and Beijing as well as at the Hebridean Celtic Festival in Stornoway.


== Musicianship ==
With a total of 4 degrees from the Royal Scottish Academy of Music and Drama (Royal Conservatoire of Scotland) the band members have competed at the highest level of bagpiping for many years. Founder and Music Director Cassells received 'BBC Radio Scotland Young Traditional Musician of the Year' in 2005 and became the first person ever to attain a degree in bagpipes from the Royal Scottish Academy of Music and Drama. Snare drummer Steven Graham is also a Double World Champion Snare Drummer.


== Former Members ==
On 23 September 2011, founder and frontman Stuart Cassells, left the band. Stuart decided that, after almost ten years, it was time to pursue new goals and opportunities. Cassells said, "I have had some absolutely amazing times in the band and I would never have imagined the success we’ve had. I want to thank all the great musicians I’ve had the pleasure of performing with and I’m excited about the new chapter in my life". Prior to leaving, Stuart had been struggling with 'focal hand dystonia', more commonly known as writer's cramp since 2008. Speaking to The Daily Record on the subject, Cassells said, "My left hand refused to stay on the chanter. It was like my left hand wasn't my own, it wouldn't stay still, like someone else was controlling it. I knew in my head how I wanted to play but I couldn't because my hands would curl right up and tense up and move involuntarily. They weren't doing what they were supposed to do. The brain was sending far too many signals to my hands." Stuart tried various treatments throughout this period, including surgery to release a trapped nerve, physiotherapy and botox injections into his arm to relax the muscles. It is hoped that he will recover and rejoin the band. A recent World Pipe Band Championship winner with the Field Marshal Montgomery Pipe Band, Kyle Warren, replaced him.


== Awards and Nominations ==


=== Scottish Live Act of the Year ===


=== BBC's 'When Will I Be Famous' ===
Winners 2007


== References ==


== External links ==
Official website