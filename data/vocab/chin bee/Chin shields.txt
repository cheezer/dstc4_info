Chin shields or chinshields, genials scales on a snake are scales found on the underside of the snake's head towards the anterior and touching the lower labial scales. Chin shields to the front of the snake (towards the snout) are called anterior chin shields while those to the rear of the snake (towards the tail) are called posterior chin shields.


== See also ==
Snake scales.
Scale (zoology).


== References ==