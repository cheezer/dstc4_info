The Kukish languages, also known as Kuki-Chin (Kuki/Chin), Mizo/Kuki/Chin, or Kuki Naga, are a branch of 50 or so Sino-Tibetan languages spoken in northeastern India, western Burma and eastern Bangladesh. Most speakers of these languages are known as Kukī in Assamese and as Chin in Burmese; some also identify as Naga. The Mizo people are ethnically distinct.
Kukish is sometimes placed under Kuki-Chin–Naga, a geographical rather than linguistic grouping.


== Subclassification ==
There is general agreement that the Karbi languages are related to, or part of, Kukish, but they are aberrant. However, Thurgood (2003) and van Driem (2011) leave Karbi unclassified within Sino-Tibetan. The Mru language, once classified as Kukish, is now thought to be closer to Lolo–Burmese.
The internal classification of the Kukish languages proper has changed little in a century:
Karbi (Mikir): Karbi, Amri
Kukish
Central: Mizo (Lushai), Bawm (Banjogi), Hmar, Hakha (Lai Pawi); maybe Ngawn, Tawr, Pangkhua
Maraic: Mara (Lakher), Zyphe, Senthang, Zotung, Lautu
Northern: Falam (Hallam, incl. Laizo, Zahao, Chorei), Anaal (Naga), Hrangkhol, Zo (Zou, Zome), Biete (Bete), Paite, Tedim (Tiddim), Thado, Chiru, Gangte, Kom (Kolhreng), Purum (Naga), Simte, Vaiphei; maybe Aimol–Saihriem, Siyin (Sizaang), Lamkang, Chothe (Naga), Kharam (Naga), Moyon (Naga), Ralte
Southern: Shö (Asho/Khyang, Bualkhaw, Chinbon, Shendu), Khumi (Khumi proper and Khumi Awa), Thaiphum, Daai (Nitu), Mro, Mün, Nga La, Welaung (Rawngtu), Kaang, Laitu, Rungtu, Songlai, Sumtu

Bradley (1997) includes Meithei. Ethnologue 16 had included several additional languages in Northern (or in the case of Darlong, Central) Kukish, but the 17th edition leaves them unclassified within Sino-Tibetan. They are:
Darlong, Monsang (Naga), Tarao (Naga), Ranglong, Sakachep.
The recently discovered Sorbung language may be a Kukish language, although it could also be a Tangkhul language (Mortenson & Keogh 2011). Rengmitca is unclassified within Kukish.
Anu-Hkongso is sometimes labeled as Chin but is closer to Mru.


== See also ==
Lai languages
Pau Cin Hau script


== Notes ==


== References ==


== Further reading ==
Button, Christopher. Proto Northern Chin. STEDT Monograph 10. ISBN 0-944613-49-7. http://stedt.berkeley.edu/pubs_and_prods/STEDT_Monograph10_Proto-Northern-Chin.pdf
Khoi Lam Thang. 2001. A Phonological Reconstruction of Proto Chin. M.A. dissertation. Chiang Mai: Payap University.
Mann, Noel, and Wendy Smith. 2008. Chin bibliography. Chiang Mai: Payap University.
S. Dal Sian Pau. 2014. The comparative study of Proto-Zomi (Kuki-Chin) languages. Lamka, Manipur, India: Zomi Language & Literature Society (ZOLLS) [Comparative word list of Paite, Simte, Thangkhal, Zou, Kom, Tedim, and Vaiphei]
VanBik, Kenneth. 2009. Proto-Kuki-Chin: A Reconstructed Ancestor of the Kuki-Chin Languages. STEDT Monograph 8. ISBN 0-944613-47-0.
Smith, Wendy and Noel Mann. 2009. Chin bibliography with selected annotations. Chiang Mai: Payap University.


== External links ==
Tlângsam: Latest News in Hmar language - Mizoram, Manipur, Assam, NE India