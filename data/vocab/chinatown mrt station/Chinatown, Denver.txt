Chinatown in Denver, Colorado, was a Chinatown on Wazee Street in what is now the "LoDo section of the city...." The first recorded Chinese person was of a man from southern China named "John" dated June 29, 1869, as documented by the Colorado Tribune.
It was also referred to as "Hop Alley" and it was torn apart by riots in the 1880s. A St. Louis newspaper dated November 1, 1880 documented the complete destruction of the neighborhood as "Chinatown Gutted by Murderous Scoundrels".


== History ==
The Chinatown originally consisted of laborers who were encouraged to move to the area by the Colorado Territorial Legislature. However, by 1880, a riot had virtually destroyed the Chinatown, mainly caused by whites whose jobs were taken by the laborers who moved from California.
An archaeological dig indicated that there were at least three Chinatowns in the city of Denver, with the last being located on Market and Larimer Streets. According to the article, Denver's Chinese population at the time grew to around 3,000 around beginning of the 20th century.


== References ==
^ "Remembering when Denver had a Chinatown". 
^ "Western Voices: 125 Years of Colorado Writing". 
^ "Race Riot Tore Apart Denver's Chinatown". 
^ "ANTI-CHINESE Denver Colorado Chinatown RIOT Democrats 1880 Old Newspaper". 
^ "DENVER CHINATOWN - Encyclopedia of the Great Plains". 
^ "DENVER'S ANTI-CHINESE RIOT". 
^ Sarah M. Nelson, K Lynn Berry, Richard F. Carillo, Bonnie J. Clark, Lori E. Rhodes, Dean Saitta. "Denver: An Archaeological History".