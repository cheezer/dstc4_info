According to the Argus Press in 1889, Detroit had at least two locations that were once called "Chinatown", with the first being in a downtown location at Third Avenue, Porter St and Bagley St, now the permanent site of the MGM Grand Casino. and relocated in a 1960s urban renewal efforts to Cass Avenue and Peterboro, which was also an opportunity for the Chinese business community to purchase property.


== History ==
According to historical records gathered through several sources and compiled by a research project, Detroit's Chinatown started in 1872, but was relocated to Cass and Peterboro sometime in the 1960s when the Detroit Housing Commission officially condemned Chinatown. After decades of depopulation and decline, the last Chinese restaurant, "Chung's", was closed in the year 2000 after 40 years of service. Although there is still a road marker indicating "Chinatown" and a mural commemorating the struggle for justice in the Vincent Chin case, few Chinese American establishments still operate within the City of Detroit. The Association of Chinese Americans Detroit Outreach Center, a small community center, serves a handful of new Chinese immigrants who still reside in the Cass Corridor.


== Healthcare and elderly services ==
The Association of Chinese Americans operated a Chinatown clinic in the Cass Corridor Chinatown, which opened on September 9, 1973 in the On-Leong merchants Association building. At the time of its opening, about 300 older Chinese American adults received services at the clinic. The clinic also serves individuals from other age groups. In 1985, the clinic moved to a renovated building on Peterboro Avenue. The On-Leong association invited them to make the move. It closed in 1996 due to demographic changes.
The Detroit Drop-In Center, a center providing services to older Chinese Americans in the Cass Chinatown, opened in October 1990. In January 2011 the main center moved to a new location in the Hannan House along Woodward Avenue.


== See also ==
History of the Chinese Americans in Metro Detroit
Chinatown, Windsor


== References ==
^ Chinatown, Burton collection, Detroit Public Library
^ "Detroit's Chinatown: forgotten part of history". 
^ Detroit News, Feb 19, 1960
^ "History-Timeline of Detroit Chinatown". 
^ a b "History of Association of Chinese Americans." (Archive) Association of Chinese Americans Detroit. Retrieved on December 3, 2013.