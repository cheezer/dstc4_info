The U.S. city of Pittsburgh, Pennsylvania was home to a "small, but busy" Chinatown, located at the intersection of Grant Street and Boulevard of the Allies where only two Chinese restaurants remain. The On Leong Society was located there. According to the article, "... the first Chinese community in Pittsburgh developed around Wylie Avenue above Court Place," according to a 1942 newsletter of the American Service Institute of Allegheny County. The Chinatown spread to Grant Street, and then "... to Water Street and then spread out to Second and Third avenues."


== History ==
The Chinatown grew from waves of Chinese immigrants who came east from California after the 1849 Gold Rush and the transcontinental railroads. The immigrants came from the area around Canton in China. According to the article, the Chinatown was primarily centered around Second Avenue with merchant names such as "Wing Hong Chinese Co., 519 Second Ave" and "Quong Chong Shing, 511 Second Ave", all of whom have been driven out when the Boulevard of the Allies was built forcing demolition of all buildings on Second Avenue, sometime by the 1950s. Even by the 1930s, "... the Chinatown was rapidly vanishing."


== Environment ==
Pittsburgh's Chinatown in the 1920s to 1930s could be described as a dangerous place as there were frequent skirmishes between the two warring Chinese gangs, otherwise known as the "Tong Wars", covered by the Pittsburgh Post-Gazette and the Pittsburgh Press. "On Second Avenue there stands the temple, pagoda style, lifting itself three stories, its tiled roof and leaded windows giving it an air of Oriental distinction. Inside is the splendor of embroidery and hangings, teakwood and mother of pearl, red lacquer and gilt carvings, a carved stone altar for worship, and a long table for meetings of the On Leong Merchants Association."


== References ==