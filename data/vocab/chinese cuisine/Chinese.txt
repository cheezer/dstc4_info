Chinese can refer to:
Something of, from, or related to China
Chinese people, people of Chinese nationality, or one of several Chinese ethnicities
Zhonghua minzu (中華民族), the supra-ethnic Chinese nationality
Han Chinese, the dominant ethnic group in mainland China, Taiwan, Singapore and Malaysia
List of ethnic groups in China
Ethnic minorities in China, the non-Han Chinese population in China

Overseas Chinese, people of Chinese ancestry who live outside mainland China and Taiwan

Chinese language, a language or family of languages spoken by the Han Chinese in China
Standard Chinese, the standard form of the Chinese language in mainland China, Taiwan and Singapore, and the variety most commonly taught as a foreign language
other varieties of Chinese
Written Chinese, the writing system of China

Chinese cuisine, styles of cooking originating from China
Chinese Peak (disambiguation)


== See also ==
List of all pages beginning with "Chinese"