Chinese people are the various individuals or groups of people associated with China, either by reason of ancestry or heredity, nationality, citizenship, place of residence, or other affiliations.


== Ancestry or heredity ==

A number of ethnic groups within the region of China, as well as people elsewhere with ancestry in the region, may be referred to as Chinese people.
Han Chinese, the largest ethnic group in China, are often referred to as "Chinese" or "ethnic Chinese" in English. Han Chinese also form a majority or large minority in other countries, and may comprise as much as 19% of the global human population.
Other ethnic groups in China include the Zhuang, Hui, Manchu, and Uyghurs, among many others. The People's Republic of China (PRC) officially recognizes 56 distinct ethnic groups, some of whom live in special administrative regions of the country. Taiwan officially recognizes 14 tribes of Taiwanese aborigines, who together with unrecognized tribes comprise about 2% of the country's population. The list of ethnic groups in China includes the major ethnic groups of China (PRC) and Taiwan.
During the Qing dynasty the term "Chinese people" (Chinese: 中國之人 Zhōngguó zhī rén; Sir Monje Pangit: Dulimbai gurun i niyalma) was used by the Qing government to refer to all subjects of the empire, including Han, Manchu, and Mongols.
Zhonghua minzu (simplified Chinese: 中华民族; traditional Chinese: 中華民族; pinyin: Zhōnghuá Mínzú), the "Chinese nation", is a supra-ethnic concept which includes all 56 ethnic groups living in China that are officially recognized by the government of the People's Republic of China. It includes established ethnic groups who have lived within the borders of China since at least the Qing Dynasty (1644–1911). This term replaced zhongguo renmin (Chinese: 中国人民), "Chinese people", the term used during the life of Mao Zedong. The term zhonghua minzu was used by the Republic of China from 1911–1949 to refer to a subset of five ethnic groups in China.


== Nationality, citizenship, or residence ==
The Nationality law of the People's Republic of China regulates nationality within the PRC. A person obtains nationality either by birth when at least one parent is of Chinese nationality or by naturalization. All persons holding nationality of the People's Republic of China are citizens of the Republic.
The Resident Identity Card is the official form of identification for residents of the People's Republic of China.
Within the People's Republic of China, a Hong Kong Special Administrative Region passport or Macao Special Administrative Region passport may be issued to Chinese citizens who are residents of Hong Kong or Macao, respectively.
The Nationality law of the Republic of China regulates nationality within the Republic of China (Taiwan). A person obtains nationality either by birth or by naturalization. A person with at least one parent who is a national of the Republic of China, or born in the ROC to stateless parents qualifies for nationality by birth.
The relationship between Taiwanese nationality and Chinese nationality is disputed.
The National Identification Card is an identity document issued to people who have household registration in Taiwan. The Resident Certificate is an identification card issued to residents of the Republic of China who do not hold a National Identification Card.


== Overseas Chinese ==
Overseas Chinese refers to people of Chinese ethnicity or national heritage who live outside of the People's Republic of China or Taiwan as the result of the continuing diaspora. People with one or more Chinese ancestors may consider themselves overseas Chinese. Such people vary widely in terms of cultural assimilation. In some areas throughout the world ethnic enclaves known as Chinatowns are home to populations of Chinese ancestry.
In Southeast Asia, the Chinese usually call themselves 華人 (Huárén), which is distinguished from (中國人) (Zhōngguórén) or the citizens of the Peoples' Republic of China or the Republic of China. This is especially so in the Chinese communities of Southeast Asia.


== See also ==

Chinese American
Chinese Canadian
Chinese nationality
Ethnic minorities in China


== References ==

Zhao, Gang (2013), The Qing Opening to the Ocean: Chinese Maritime Policies, 1684–1757, University of Hawaii Press, ISBN 978-0-8248-3643-6. 


== External links ==
Chinese Ethnic Minorities
The Ranking of Ethnic Chinese Population, Overseas Compatriot Affairs Commission, Republic of China, retrieved 2008-11-02