The Chinese calendar is a lunisolar calendar which arranges the year, month and day number upon the astronomical date. It is used for traditional activities in China and overseas Chinese communities. It determines the date for the Chinese traditional holidays, and instructs Chinese people in selecting the lucky day of a wedding or funeral, for opening a venture, or a relocation.
In the Chinese calendar, the days begin at midnight and end at midnight of the next day. The months begin on the day with the dark (new) moon and end on the day before the next dark moon. The years begin on the day with the dark moon near the Vernal Commences. The solar terms are the important components of the Chinese calendar. There are one or two, and occasionally three, solar terms within a month.
The present Chinese calendar is the product of evolution. Many astronomical and seasonal factors were added by ancient scientists, and people can reckon the date of natural phenomena such as the moon phase and tide upon the Chinese calendar. The Chinese calendar has over 100 versions, and the characteristics of these versions reflect the evolutionary route of the Chinese calendar. Like Chinese characters, these versions of Chinese calendar are adopted throughout the Sinosphere. In Korea, Vietnam, and the Ryukyu Islands, the Chinese calendar was adopted completely. In Japan, the Chinese calendar was used before the Edo period, and the later Japanese calendar used the algorithm of the Chinese calendar.
The Chinese calendar is not the official calendar in China (see Minguo calendar for post-1912 history), but plays an important role there. The official name of the Chinese calendar is the Rural Calendar (traditional Chinese: 農曆; simplified Chinese: 农历; pinyin: Nónglì), but people often refer to the Chinese calendar with other names, such as the Former Calendar (traditional Chinese: 舊曆; simplified Chinese: 旧历; pinyin: Jiùlì), the Traditional Calendar (traditional Chinese: 老曆; simplified Chinese: 老历; pinyin: Lǎolì), or the Yin Calendar (traditional Chinese: 陰曆; simplified Chinese: 阴历; pinyin: Yīnlì).


== Structure ==


=== Codes ===
Several coding systems are used for some special circumstances in order to avoid ambiguity, such as continuous day or year count.
The heavenly stems is a decimal system. The characters of the heavenly stems are: jiǎ, yǐ, bǐng, dīng, wù, jǐ, gēng, xīn, rén, guì (Chinese: 甲乙丙丁戊己庚辛壬癸).
The earthly branches is a duodecimal system. The characters of the earthly branches are: zǐ, chǒu, yín, mǎo, chén, sì, wǔ, wèi, shēn, yǒu, xū, hài (Chinese: 子丑寅卯辰巳午未申酉戌亥). The earthly branches are used to mark the shí and climate terms usually.
The stem-branches is a sexagesimal system. The heavenly stems and earthly branches match together and form stem-branches. The stem-branches are used to mark the continuous day and year.


=== Day, hour, and week ===
In the Chinese calendar, the days begin at midnight and end at the next midnight, but people tend to regard the days as beginning at dawn.
Currently, the time in days are scaled with hour-minute-second system.
In ancient China, the time is divided according to either the shí-kè and gēng-diǎn systems. There are 10 gēng or 12 shí, 60 dián or 100 kè, 6000 fēn in a day. For example:
The current Chinese standard time is 02:00:43 or 35.52 (52 fēn, sāngēng 5 diǎn).
Currently, the days are grouped into 7-day weeks and the weekdays are marked with the ordinal number except Sunday.
In ancient China, the days were grouped into 60-day weeks with the stem-branches.


=== Month ===
In the Chinese calendar, the months begin on the day of the dark moon, and end on the day before the next dark moon. There are 29 or 30 days in a month, but the month length is float. The month with 30 days is called the long month (Chinese: 大月), and the month with 29 days is called the short month (Chinese: 小月). The days in the months are marked with the ordinal number from 1. However, in the Chinese calendar, the dates are normalized to two characters, such as Chūyī (Chinese: 初一), Shíwǔ (Chinese: 十五), Niànsān (Chinese: 廿三).
As a practice, the months are divided into 3 xún (Chinese: 旬). The first 10 days is the early xún (Chinese: 上旬), the middle 10 days is the mid xún (Chinese: 中旬), and the last 9 or 10 days is the late xún (Chinese: 下旬). The ten-day week was used in antiquity (reportedly as early as in the Bronze Age Xia Dynasty). The law in the Han Dynasty (206 BC – AD 220) required officials of the empire to rest every five days, called mu (沐), while it was changed into 10 days in the Tang Dynasty (AD 618 – 907), called huan (澣/浣) or xún (旬). Months were almost three weeks long (alternating 29 and 30 days to keep in line with the lunation). The weeks were labelled shàng xún (上旬), zhōng xún (中旬), and xià xún (下旬) which mean roughly "early", "mid" and "late" week. Markets in Japan followed the Chinese jun (旬) system; see Japanese calendar. In Korea, it was called "Sun" (순,旬).


==== Relation between moon phases and dates ====
The first day of each month is the day with dark moon. In the 7th or 8th day of each month, the first quarter moon is visible in the afternoon and early evening. In the 15th or 16th day of each month, the full moon is visible all night, and in the 22nd or 23rd day of each month, the last quarter moon is visible late at night and in the morning .


=== Solar year and solar term ===

In the Chinese calendar, the solar year (traditional Chinese: 歲; simplified Chinese: 岁; pinyin: Suì) is the time from a winter solstice to the next winter solstice. The solar year is divided into 24 solar terms which corresponding to 15° along the ecliptic each. Two solar terms are associated with each climate term. The beginning of the first term in each climate term is labelled "pre-climate" (traditional Chinese: 節氣; simplified Chinese: 节气; pinyin: Jiéqì), and the beginning of the last term in each couple is labelled "mid-climate" (traditional Chinese: 中氣; simplified Chinese: 中气; pinyin: Zhōngqì).
In a solar year, there are 11 or 12 whole months. The year with 11 months is a common year. The year with 12 months is a leap year, and the first month without a mid-climate is the intercalary month.
The months in the solar year are numbered with the ordinal number from 0, except the intercalary month. The intercalary month follows the number of the previous month.


==== Relations between solar terms and months ====
The Chinese use an astronomical calendar, and the years and months are irregular. But, there are some rhythms for the laws of astronomy.
In general, the month number corresponds the climate term.
1. The pre-climates are always within 15 days before or after the first day of the corresponding months.
the Moderate Cold is always within 15 days before or after 1st of the 0th month(always Làyuè), the Vernal Commences is always within 15 days before or after 1st of the 1st momth(Zhēngyuè),
the Insects Waken is always within 15 days before or after 1st of the 2nd month(Èryuè) the Bright and Clear is always within 15 days before or after 1st of the 3rd month(Sānyuè),
the Summer Commences is always within 15 days before or after 1st of the 4th month(Sìyuè) the Corn on Ear is always within 15 days before or after 1st of the 5th month(Wǔyuè),
the Moderate Heat is always within 15 days before or after 1st of the 6th month(Liùyuè), the Autumn Commences is always within 15 days before or after 1st of the 7th month(Qīyuè),
the White Dew is always within 15 days before or after 1st of the 8th month(Bāyuè) the Cold Dew is always within 15 days before or after 1st of the 9th month(Jiǔyuè),
the Winter Commences is always within 15 days before or after 1st of the 10th month(Shíyuè), the Heavy Snow is always within 15 days before or after 1st of the month with the winter solstice(Shíyīyuè).
2. The mid-climates are always within the corresponding months.
the Great Cold is always within Làyuè, the Vernal Showers is always within Zhēngyuè the Vernal Equinox is always within Èryuè,the Corn Rain is always within Sānyuè,
the Corn Forms is always within Sìyuè, the Summer Solstice is always within Wǔyuè, the Moderate Heat is always within Liùyuè, the End of Heat is always within Qīyuè,
the Autumnal Equinox is always within Bāyuè, the First Frost is always within Jiǔyuè, the Light Snow is always within Shíyuè, and the Winter solstice is bound to Shíyīyuè.
The Chinese calendar follows the Metonic cycle in general.
1. The pre-climates and mid-climates are about at the same date in the Chinese calendar 19 years later.
For example, the vernal commences is about at the New Year's Day of 1905,1924, 1943, ...; and the winter solstice is about at Shíyīyuè 1st of 2014, 2033, 2052,....
2. A representative sequence of common and leap years is lcc lcc lc lcc lcc lcc lc, and intercalary months are about following the 7th, 5th, 3rd, 8th, 6th, 4th, 2nd month in each section.
The intercalary month of the leap year between 1854 and 2100 in the Chinese calendar is list as below:


=== Year ===
In the Chinese calendar, the years begin on the 1st day of the 1st month, and the first and last month are named as Zhēngyuè(Chinese: 正月) and Làyuè(traditional Chinese: 臘月; simplified Chinese: 腊月) specially. The stem-branches are used to count the year, and according to usual practice, each branches corresponds an animal zodiac. people used to call the animal zodiac. After the era system was abolished, the stem-branches is the only formal year numbering method.
There are 12 or 13 months in a year. The years with 12 months are common years, there are 353, 354 or 355 days in the common year. The years with 13 months are leap years, there are 383, 384 or 385 days in the leap year. For example, the year from February 19, 2015 to February 07, 2016 is a Yǐwèinián or Mùyángnián (Year of wooden goat ), a common year with late Vernal Commences(Chinese: 年尾春) , 12 months or 354 days long.


==== Age recognition in China ====
In China, age for official use is based on the Gregorian calendar. For traditional use, age is based on the Chinese calendar. From birthday to the end of the year, it's one year old. After each New Year's Eve, add one year. For example, if one's birthday is Làyuè 29th 2013, he is 2 years old at Zhēngyuè 1st 2014. On the other hand, people say months old instead of years old, if someone is too young. It's that the age sequence is "1 month old, 2 months old, ... 10 months old, 2 years old, 3 years old..."
After the actual age (traditional Chinese: 實歲; simplified Chinese: 实岁) was introduced into China, the Chinese traditional age was referred to as the nominal age (traditional Chinese: 虛歲; simplified Chinese: 虚岁). Divided the year into two halves by the birthday in the Chinese calendar, the nominal age is 2 older than the actual age in the first half, and the nominal age is 1 older than the actual age in the second half (traditional Chinese: 前半年前虛兩歲,後半年虛一歲; simplified Chinese: 前半年前虚两岁,后半年虚一岁).


==== Year number system ====
Era system
In the ancient system, the years are numbered from 1 when the new emperor ascended the throne or the authority released the reign title. The first reign title is Jiànyuán (Chinese: 建元, from 140BC), the last reign title is Xuāntǒng(traditional Chinese: 宣統; simplified Chinese: 宣统, from 1908). The era system was abolished in 1910. The Christian era or ROC era took over to express the year in the Chinese calendar.
On the other hand, the Christian era is simulated in the 20th century, such as:
Anno Huángdì (Chinese: 黄帝紀年), upon the regal of Huángdì, who is honored as the ancestor of Chinese, 2698+AD=AH
Anno Yáo (Chinese: 唐尧紀年), upon the regal of Emperor Yao, 2156+AD=AY
Anno Gònghé (Chinese: 共和紀年), upon Gònghé 1, when there's faithful historical records. 841+AD=AG
Anno Confucius(Chinese: 孔子紀年), upon the birth year of Confucius, who is honored the sage of the ancient Chinese, 551+AD=AC
Anno Unity (Chinese: 統一紀年), upon the regal of the First Emperor of Qin, who unify China for the first time, 221+AD=AU
Generally, no reference date is widely accepted. On January 2, 1912, Sun Yat-sen declared a change to the official calendar and era. In his declaration, Jan 1, 1912 is called Shíyīyuè 13th, 4609 AH which implied an epoch of 2698 BC. The implication was adopted by many overseas Chinese communities outside Southeast Asia such as San Francisco's Chinatown.
In the 17th century, the Jesuits tried to determine what year should be considered the epoch of the Han calendar. In his Sinicae historiae decas prima (first published in Munich in 1658), Martino Martini (1614–1661) dated the royal ascension of Huangdi to 2697 BC, but started the Chinese calendar with the reign of Fu Xi, which he claimed started in 2952 BC. Philippe Couplet's (1623–1693) Chronological table of Chinese monarchs (Tabula chronologica monarchiae sinicae; 1686) also gave the same date for the Yellow Emperor. The Jesuits' dates provoked great interest in Europe, where they were used for comparisons with Biblical chronology. Modern Chinese chronology has generally accepted Martini's dates, except that it usually places the reign of Huangdi in 2698 BC and omits Huangdi's predecessors Fu Xi and Shennong, who are considered "too legendary to include".
Starting in 1903, radical publications started using the projected date of birth of the Yellow Emperor as the first year of the Han calendar. Different newspapers and magazines proposed different dates. Jiangsu, for example, counted 1905 as year 4396 (use an epoch of 2491 BC), whereas the Minbao (traditional Chinese: 明報; simplified Chinese: 明报, the organ of the Tongmenghui) reckoned 1905 as 4603 (use an epoch of 2698 BC). Liu Shipei(劉師培; 1884–1919) created the Yellow Emperor Calendar, now often used to calculate the date, to show the unbroken continuity of the Han race and Han culture from earliest times. Liu's calendar started with the birth of the Yellow Emperor, which he determined to be 2711 BC. There is no evidence that this calendar was used before the 20th century. Liu calculated that the 1900 international expedition sent by eight foreign powers to suppress the Boxer Uprising entered Beijing in the 4611th year of the Yellow Emperor.
Calendric epoch
There is an epoch for each version of the Chinese calendar, which is called Lìyuán(traditional Chinese: 曆元; simplified Chinese: 历元). The epoch is the optimal origin of the calendar, and it's a Jiǎzǐrì, the first day of a lunar month, and the dark moon and solstice are just at the mid-night(Chinese: 日得甲子夜半朔旦冬至). And tracing back to a perfect day, such as that day with the magical star sign, there's a supreme epoch(Chinese: 上元; pinyin: shàngyuán). The continuous year based on the supreme epoch is shàngyuán jīnián(traditional Chinese: 上元積年; simplified Chinese: 上元积年). More and more factors were added into the supreme epoch, and the shàngyuán jīnián became a huge number. So, the supreme epoch and shàngyuán jīnián were neglected from the Shòushí calendar.
Yuán-Huì-Yùn-Shì system
Shao Yong (Chinese: 邵雍, Courtesy name: Yáofū, Posthumous title: Kāngjié), 1011–1077, a philosopher, cosmologist, poet, and historian who greatly influenced the development of Neo-Confucianism in China, introduced a timing system in his The Ultimate which Manages the World (traditional Chinese: 皇極經世; simplified Chinese: 皇极经世; pinyin: Huángjíjīngshì) In his time system, 1 yuán (Chinese: 元), which contains 12'9600 years, is a lifecycle of the world. Each yuán is divided into 12 huì (traditional Chinese: 會; simplified Chinese: 会). Each huì is divided into 30 yùn (traditional Chinese: 運; simplified Chinese: 运), and each yùn is divided into 12 shì (Chinese: 世). So, each shì is equivalent to 30 years. The yuán-huì-yùn-shì corresponds with nián-yuè-rì-shí. So the yuán-huì-yùn-shì is called the major tend or the numbers of the heaven, and the nián-yuè-rì-shí is called the minor tend or the numbers of the earth.
The minor tend of the birth is adapted by people for predicting destiny or fate. The numbers of nián-yuè-rì-shí are encoded with stem-branches and show a form of Bāzì. The nián-yuè-rì-shí are called the Four Pillars of Destiny. For example, the Bāzì of Emperor Qiánlóng is Xīnmǎo,Dīngyǒu,Gēngwǔ,Bǐngzǐ (辛卯、丁酉、庚午、丙子). Shào‍‍ '​‍s Huángjíjīngshì recorded the history of the timing system from the first year of the 180th yùn or 2149th shì (HYSN 0630-0101, 2577 BC) and marked the year with the reign title from the Jiǎchénnián of the 2156th shì (HYSN 0630-0811, 2357 BC, Tángyáo 1, traditional Chinese: 唐堯元年; simplified Chinese: 唐尧元年). According to this timing system, 2014-1-31 is HYSN/YR 0712-1001/0101.
The table below shows the kinds of year number system along with correspondences to the Western (Gregorian) calendar. Alternatively, see this larger table of the full 60-year cycle.
1 As of the beginning of the year.2 Timestamp according to Huángjíjīngshì, as a format of Huìyùn-Shìnián.3 Huángdì era, using an epoch (year 1) of 2697 BC. Subtract 60 if using an epoch of 2637 BC. Add 1 if using an epoch of 2698 BC.


=== Phenology ===
The plum rains season is the rainy season during the late spring and early summer. The plum rains season starts on the first Bǐngrì after the Corn on Ear, and ends on the first Wèirì after the Moderate Heat.
The Sanfu days are the three sections from the first Gēng-day after the summer solstice. The first section is 10 days long, and named the fore fu (Chinese: 初伏; pinyin: chūfú). The second section is 10 or 20 days long, and named the mid fu (Chinese: 中伏; pinyin: zhōngfú). The last section is 10 days long from the first Gēng-day after autumn commences, and named the last fu (Chinese: 末伏; pinyin: mòfú).
The Shujiu cold days are the nine sections from the winter solstice. Each section is 9 days long. The shǔjǐu are the coldest days, and named with an ordinal number, such as Sìjǐu (Chinese: 四九).
The forecast of drought(traditional Chinese: 分龍; simplified Chinese: 分龙) is the key day which forecast if it's a drought year. the day is the first Chénrì after the Summer Solstice.


=== Holidays ===
In the Sinosphere, the traditional festivals are upon the Chinese calendar (or local version of the Chinese calendar).
Major traditional holidays upon the date
The Preliminary Eve (Chinese: 小年) is Làyuè 23rd or 24th. The following preliminary eve is at 2016-02-01 in north China, and at 2016-02-02 in south China.
The New Year's Eve (Chinese: 除夕) is the last day of the year, Làyuè 29th or 30th. The following New Year's Eve is at 2016-02-07, which is a statutory holiday.
The New Year's Day (traditional Chinese: 春節; simplified Chinese: 春节) is Zhēngyuè 1st.The following New Year's Day is at 2016-02-08, which is a statutory holiday.
The Yuanxiao (Chinese: 元宵) is Zhēngyuè 15th. The following Yuanxiao is at 2015-03-05
The Shangsi (Chinese: 上巳) is Sānyuè 3rd. The following Shangsi is at 2015-04-21
The Buddha's Birthday (traditional Chinese: 佛誕; simplified Chinese: 佛诞) is Sìyuè 8th. The following Buddha's Birthday is at 2015-05-25
The Duanwu (Chinese: 端午) is Wǔyuè 5th. The following Duanwu is at 2015-06-20, which is a statutory holiday.
The Qixi (Chinese: 七夕) is Qīyuè 7th. The following Qixi is at 2015-08-20
The Zhongyuan (Chinese: 中元) is Qīyuè 15th. The following Zhongyuan is at 2015-08-28
The Mid-autumn (Chinese: 中秋) is Bāyuè 15th. The following Mid-autumn is at 2015-09-27, which is a statutory holiday.
The Dual-yang (traditional Chinese: 重陽; simplified Chinese: 重阳) is Jiǔyuè 9th. The following Dual-yang is at 2015-10-21
The Xiayuan (Chinese: 下元) is Shíyuè 15th. The following Xiayuan is at 2015-11-26
The Laba Festival (traditional Chinese: 臘八節; simplified Chinese: 腊八节) is Làyuè 8th. The following Laba Festival is at 2016-01-17
Major traditional holidays upon solar term
The Hanshi (Chinese: 寒食) is the 105th day after the Winter Solstice. The following Hanshi is April 4 or Éryuè 16th, 2015
The Qingming Festival (Chinese: 清明) is just on the day of the Bright and Clear. The following Qingming Festival is April 5 or Éryuè 17th, 2015, which is a statutory holiday.
The Winter Solstice (Chinese: 冬至) is just on the day of the Winter Solstice. The following Winter Solstice is December 22 or Shíyīyuè 12th, 2015.
The Vernal/Autumn Sacrifice (Chinese: 春社/秋社) is the fifth Wùrì after Vernal/Autumn Commences.
The traditional business festivals
In the old days, merchants used to open their stores from Zhēngyuè 5th, and host a prayer service on that day. Zhēngyuè 5th is called God of Wealth's Day, and the prayer service is called God of Wealth is Welcome.
In the Fujian and Taiwan areas, businesses host a year-end dinner for employees at Làyuè 16th. Làyuè 16th is called Weiya (Chinese: 尾牙).


== History ==


=== Earlier Chinese calendars ===
Before the Spring and Autumn period, the Chinese Calendars were solar calendars. According to literature, the first version was the five-phases calendar (traditional Chinese: 五行曆; simplified Chinese: 五行历). In the five-phases calendar, the year begins at the Vernal Commences, and consists of 10 months and a transition. Each month is 36 days long, and the transitions are 5 or 6 days long. The months are named with the heavenly stems, and a couple of months form a phase. Phases are named with five-phases. The second version is the four-seasons calendar (traditional Chinese: 四時八節曆; simplified Chinese: 四时八节历). In the four-seasons calendar, the year begins at the Vernal Commences, and consists of 12 months and a year day.


=== Ancient Chinese calendars ===
Pre-Qin calendars
During the Warring States period, the primitive lunisolar calendars are established by royal of Zhou Dynasty and vassal states. Several representative calendars are Huangdi's calendar(traditional Chinese: 黃帝曆; simplified Chinese: 黄帝历), Zhuanxu's calendar(traditional Chinese: 顓頊曆; simplified Chinese: 颛顼历), Xia's calendar(traditional Chinese: 夏曆; simplified Chinese: 夏历), Yin's calendar(traditional Chinese: 殷曆; simplified Chinese: 殷历), Zhou's calendar(traditional Chinese: 周曆; simplified Chinese: 周历), and Lu's calendar (traditional Chinese: 魯曆; simplified Chinese: 鲁历). These 6 calendars are called as the six ancient calendars(traditional Chinese: 古六曆; simplified Chinese: 古六历), and are the quarter remainder calendars (traditional Chinese: 四分曆; simplified Chinese: 四分历; pinyin: sìfēnlì). The months of these calendars begin on the day with the darkmoon, and there're 12 or 13 month within a year. The intercalary month is placed at the end of the year, and called as 13th month. But, the year beginning is different in these calendars. The years in the Xia's calendar begin on the day with the darkmoon close to the Vernal Commences. The years in the Yin's calendar end on the last day of the month with the Winter Solstice. The years in the Huangdi's calendar, Zhou's calendar, and Lu's calendar begin on the first day of the month with the Winter Solstice. And, the years in the Zhuanxu's calendar begin on the day with the darkmoon close to the Winter Commences.
Calendar of Qin and earlier Han dynasty
After Emperor Qin Shihuang unified China, the Qin's calendar (traditional Chinese: 秦曆; simplified Chinese: 秦历) was released. The Qin's calendar follows the rules of the Zhuanxu's calendar, but the months order follows the Xia's calendar. The months in the year are from the 10th month to the 9th month, and the intercalary month is called as the second Jiuyue(traditional Chinese: 後九月; simplified Chinese: 后九月). In the earlier Han dynasty, Qin's calendar continued to be used.
Taichu calendar and the calendars from Han to Ming dynasty.
Emperor Wu of the Han dynasty introduced reforms in the halfway of his administration. His Taichu or Grand Inception Calendar (traditional Chinese: 太初曆; simplified Chinese: 太初历) introduced 24 solar terms which decides the month names. The solar year was defined as 365 385/1539 days, and divided into 24 solar terms. Each couples of solar terms are associated into 12 climate terms. The lunar month was defined as 29 43/81 days and named according to the closest climate term. The mid-climate in the month decides the month name, and a month without mid-climate is an intercalary.
The Taichu calendar established the frame of the Chinese calendar, Ever since then, there are over 100 official calendars in Chinese which are consecutive and follow the structure of Tàichū calendar both. There're several innovation in calendar calculation in the history of over 2100 years, such as:
In the Dàmíng Calendar released in Tiānjiān 9(Chinese: 天监九年, 510) of Liáng Dynasty, Zhǔ Chōngzhī introduced the equation of equinoxes.
Actual syzygy method was adopted to decide the month from the Wùyín Yuán Calendar, which was released in Wǔdé 2(Chinese: 武德二年, 619) of Táng Dynasty.
The real measured data was used in calendar calculation from Shòushí Calendar, which was released in Zhìyuán 18(Chinese: 至元十八年,1281) of Yuán Dynasty. And the tropical year is fixed at 365.2425 days, the same as the Gregorian calendar established in 1582., and derived spherical trigonometry.


=== Modern Chinese calendars ===
Shíxiàn calendar
In the late Ming dynasty, Xu guangqi and his colleagues worked out the new calendar upon the western astronomical arithmetic. But, the new calendar is not released before the end of the Ming Dynasty. In the earlier Qing Dynasty, Johann Adam Schall von Bell submitted the calendar to Emperor of Shunzhi. The authority of Qing Dynasty released the calendar with a name, the Shíxiàn calendar, which means seasonal charter. In the Shíxiàn calendar, the solar terms each correspond to 15° along the ecliptic. It makes the Chinese calendar develop into an astronomical calendar. However, the length of the climate term near the perihelion is shorter than 30 days and there may be two mid-climate terms. The rule of the mid-climate terms decides the months, which is used for thousands years, lose its validity. The Shíxiàn calendar changed the rule to "decides the month in sequence, except the intercalary month".
Current Chinese calendar
Currently, the Chinese calendar follows the rules of the Shíxiàn calendar, except that: changed the baseline to the Chinese Standard Time, and adopt the real astronomical data of observatories against the theoretical calculation.
Optimize the Chinese calendar
To optimize the Chinese calendar, astronomers and atrophiles released many proposal. A typical proposal is released by Kao Ping-tse (Chinese: 高平子; 23 December 1888 - 23 March 1970, a Chinese astronomer, one of the founders of Purple Mountain Observatory). In his proposal, the month numbers are calculated before the dark moons and the solar terms were rounded to day. Upon his proposal, the month numbers are the same for the Chinese calendar upon different time zones.


=== Other practice ===
In the mountain and plateau ethnic groups of the southwest China, and grasslands ethnic groups of the north China, the civil calendars shows a diversity of practice upon the characteristic phenology and culture, but they're upon the algorithm of the Chinese calendar of different periods, especially those of the Tang Dynasty and pre-Qin period. For example, the Thai, Tibet and Mongolia calendar is upon the algorithm of Tang Dynasty; and the Miao and Yi calendar is upon the algorithm of pre-Qin period.


== See also ==
Culture of China
Dates in Chinese
East Asian age reckoning
Festivals of Korea
Guo Shoujing, an astronomer tasked with calendar reform during the 13th century
List of festivals in Vietnam
Public holidays in China
Sexagenary cycle
Chinese Traditional Date and Time


== References ==


== Further reading ==
Cohen, Alvin (2012). "Brief Note: The Origin of the Yellow Emperor Era Chronology" (PDF). Asia Major 25 (pt 2): 1–13. 


== External links ==
Calendars
Chinese months
Gregorian-Lunar calendar years (1901–2100)
Chinese calendar and holidays
Chinese calendar with Auspicious Events
Calendar conversion
2000-year Chinese-Western calendar converter From 1 AD to 2100 AD. Useful for historical studies. To use, put the western year 年 month 月day 日in the bottom row and click on 執行.
Western-Chinese calendar converter
Rules
Mathematics of the Chinese Calendar
The Structure of the Chinese Calendar