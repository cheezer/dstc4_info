CHOA redirects here. For the radio station in Stettler, Alberta that previously used those calls, see CKSQ-FM. For the Thai monarchy title, see Chao (monarchy).
CHOA-FM is a Canadian radio station, broadcasting at 96.5 FM in Rouyn-Noranda, Quebec. The station has an adult contemporary format branded as Rythme FM.

Owned by RNC Media, the station aired an adult contemporary format from its inception, both under independent branding and as an affiliate of Astral Media's RockDétente network. The station briefly adopted a modern rock format branded as Radio X after RNC Media acquired CHOI in Quebec City in 2006, but later reverted to the Couleur FM name and format, and was rebranded as Planète in 2008. The Radio X branding later resurfaced at sister stations CHGO-FM and CJGO-FM.
CHOA also has rebroadcast transmitters in Val-d'Or (103.5 FM) and La Sarre (103.9 FM). RNC applied in 1996 to convert CHOA's retransmitter in Val-d'Or into an originating station, but was denied because the market could not support a new commercial station.
In December 2014, it was announced that CHOA-FM would become an affiliate of Cogeco's Rythme FM network, starting on March 9, 2015. The station is the second Rythme FM outlet owned by RNC, with CHLX-FM Gatineau being the first.


== References ==


== External links ==
Rythme FM Abitibi
CHOA-FM history at Canadian Communications Foundation
Query the REC's Canadian station database for CHOA-FM
Online Radio Stream