Chop, CHOP, Chops, or CHOPS may refer to:


== Art ==
Chop!, the English name for the Chinese series 大刀向鬼子们的头上砍去 (lit. "Chop Off the Devils' Heads with Your Machete!")
Embouchure, in music, a synonym for chops
CHOPS, Asian-American hip-hop producer, rapper and member of rap group Mountain Brothers
Chops (Euros Childs album), 2006
Chops (Joe Pass album), 1978


== Medicine ==
Children's Hospital of Philadelphia, one of the largest and oldest children's hospitals in the world
CHOP, a chemotherapy treatment for non-Hodgkin's lymphoma
CCAAT/Enhancer-Binding Protein Homologous Protein (CHOP), an activating protein of apoptosis
DNA damage-inducible transcript 3, another gene also known as "CHOP", for "C/EBP-homologous protein"


== Sports ==
Knifehand strike, also known as the karate chop, a fast and focused strike with the side of the hand
Chop (wrestling), offensive move in professional wrestling, used to set up an opponent for a submission hold or for a throw
Chops (juggling), a juggling pattern using three balls or clubs
Chopping the blinds, in poker, when all players fold to the blinds, who then remove their bets
Michael Chopra (born 1983), football striker from Cardiff City's team
Chan Ho Park (born 1973), pitcher for the New York Yankees
Backspin, a shot such that the ball rotates backwards (as though rolling back towards the player) after it is hit


== Other ==
Chop, Ukraine
Meat chop, a cut of meat usually containing a rib and served as an individual portion
Chop (water), the roughness of a body of water due to wind-generated waves
Chop (furniture), an identifying mark on cabinets and furniture
Chop (fiberglass), a form of fiberglass
Cold heavy-oil production with sand, a technique for extracting difficult heavy crude oil where sand is used as a means enhancing the productivity of the oil well
Seal (East Asia), or "chop" colloquially, used in China, Japan, and other parts of East Asia to prove identity (typically on documents or art in East Asia)
Mutton chops, a colloquial term for sideburns
CHOP or Crew Hands-Off Point, location where space vehicles automatically dock without interruption from the crew
CHOP or Change of Operational Control, the procedure of transitioning a U.S. naval ship from one force commander to another
Chop, Franklin Clinton's dog in Grand Theft Auto 5


== See also ==
Chop Chop (disambiguation)
Chopped (disambiguation)
Chopping (disambiguation)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        , Mize EM, Glick JH, Coltman CA Jr, Miller TP (1993). "Comparison of a standard regimen (CHOP) with three intensive chemotherapy regimens for advanced non-Hodgkin's lymphoma.". N Engl J Med 328 (14): 1002–6. doi:10.1056/NEJM199304083281404. PMID 7680764.