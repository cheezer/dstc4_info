Chop Chop Ninja is an iOS game developed by Gamerizon and released on December 25, 2009.


== Critical reception ==
The game has a Metacritic rating of 75% based on 4 critic ratings.
AppSpy wrote "If you enjoy platformers, Chop Chop Ninja is worth a play for its style, and its controls. " No Dpad said "Despite some shortcomings, Chop Chop Ninja is overall a fun game, a welcome entry to the App Store's mostly underwhelming line-up of ninja titles, and likely to be the last sleeper hit of 2009. " SlideToPlay wrote "Clumsy controls take the stealth action out of this ninja game. " TouchGen said "Chop Chop Ninja is a cool action platformer with cute and bright presentation, innovative touch controls mixed with a bit too easy difficulty and a short story mode. It is definitely interesting to pick up for the controls alone in my opinion. "


== TV series ==
An animated series based on the game debuted on Teletoon in Canada in November 2014.


== References ==