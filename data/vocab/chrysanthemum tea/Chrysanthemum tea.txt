Chrysanthemum tea (Chinese: 菊花茶; pinyin: júhuā chá) is a flower-based infusion made from chrysanthemum flowers of the species Chrysanthemum morifolium or Chrysanthemum indicum, which are most popular in East Asia. To prepare the tea, chrysanthemum flowers (usually dried) are steeped in hot water (usually 90 to 95 degrees Celsius after cooling from a boil) in either a teapot, cup, or glass; often rock sugar is also added, and occasionally also wolfberries. The resulting drink is transparent and ranges from pale to bright yellow in color, with a floral aroma. In Chinese tradition, once a pot of chrysanthemum tea has been drunk, hot water is typically added again to the flowers in the pot (producing a tea that is slightly less strong); this process is often repeated several times. Chrysanthemum tea was first drunk during the Song Dynasty (960–1279).


== Varieties ==
Several varieties of chrysanthemum, ranging from white to pale or bright yellow in color, are used for tea. These include:
Huángshān Gòngjú (黄山贡菊, literally "Yellow Mountain tribute chrysanthemum"); also called simply Gòngjú (贡菊)
Hángbáijú (杭白菊), originating from Tongxiang, near Hangzhou; also called simply Hángjú, (杭菊)
Chújú (滁菊), originating from the Chuzhou district of Anhui
Bójú (亳菊), originating in the Bozhou district of Anhui
The flower is called kek-huai in Thai, from kiok-hoe, Min Nan for júhuā. In Tamil it is called saamandhi.
Of these, the first two are most popular. Some varieties feature a prominent yellow flower head while others do not.


== Medicinal use ==

Chrysanthemum tea has many purported medicinal uses, including an aid in recovery from influenza, acne and as a "cooling" herb. According to traditional Chinese medicine the tea can aid in the prevention of sore throat and promote the reduction of fever. In Korea, it is known well for its medicinal use for making people more alert and is often used as a pick-me-up to render the drinker more awake. In western herbal medicine, Chrysanthemum tea is drunk or used as a compress to treat circulatory disorders such as varicose veins and atherosclerosis.
In traditional Chinese medicine, chrysanthemum tea is also said to clear the liver and the eyes. It is believed to be effective in treating eye pain associated with stress or yin/fluid deficiency. It is also used to treat blurring, spots in front of the eyes, diminished vision, and dizziness. The liver is associated with the element Wood which rules the eyes and is associated with anger, stress, and related emotions. No scientific studies have substantiated these claims.


== Commercial availability ==

Although typically prepared at home, chrysanthemum tea is also available as a beverage in many Asian restaurants (particularly Chinese ones), and is also available from various drinks outlets in East Asia as well as Asian grocery stores outside Asia in canned or packed form. Due to its medicinal value, it may also be available at Traditional Chinese medicine outlets, often mixed with other ingredients.


== See also ==
Chinese herb tea
List of Chinese teas
Xia Sang Ju
 Drink portal
 China portal


== References ==