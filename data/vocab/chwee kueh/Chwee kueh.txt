Chwee kueh (known also as chwee kwee or chwee kweh) (Chinese: 水粿; pinyin: shuǐguǒ; Pe̍h-ōe-jī: chúi-kóe; literally: "water rice cake") is a type of steamed rice cake, a cuisine of Singapore and Johor.
To make chwee kueh, rice flour and water are mixed together to form a slightly viscous mixture. They are then placed in small cup-shaped containers that look like saucers and steamed, forming a characteristic bowl-like shape when cooked. The rice cakes are topped with diced preserved radish and served with chilli sauce. Chwee kueh is a popular breakfast item in Singapore and Johor.


== See also ==
Idli


== References ==