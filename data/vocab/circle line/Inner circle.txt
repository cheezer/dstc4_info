Inner circle may refer to:
Inner Circle (London Underground), an early name for the central circuit route of the London Underground that is now known as the Circle Line
Inner Circle railway line, Melbourne, a former railway line in Melbourne, Australia
Inner Circle of Advocates, trial lawyer group
Inner Circle (parody group), a New York parody group
friendship networks, where "inner circle" may describe the closest of friends
esoteric teaching, knowledge that is confined to an inner group
Inner/outer directions, a method of labeling direction of travel for geographic loops
Inner Circle (addiction recovery), Inner-most circle of three circles in addiction recovery diagram
Inner circle (psychoanalysis) or Freud's inner circle
Birmingham Inner Circle, a circular bus route following Birmingham's inner ring road in the West Midlands County of England


== Music ==
Inner Circle (band), a Jamaican reggae group
The Inner Circle (album), a 2004 album by Evergrey
The Black Metal Inner Circle from the early Norwegian black metal scene


== Film and television ==
The Inner Circle (1912 film), a film directed by D. W. Griffith
The Inner Circle (1946 film), an American film directed by Philip Ford
The Inner Circle (1991 film), a film about KGB officer Ivan Sanchin
"The Inner Circle" (The Office), a 2011 episode of The Office
The Inner Circle (2005 film), a film produced by Tommy G. Warren and Spiderwood Productions


== Literature ==
The Inner Circle (novel), a 2004 novel by T. C. Boyle about Alfred Kinsey
The Inner Circle, a 2011 novel by Brad Meltzer