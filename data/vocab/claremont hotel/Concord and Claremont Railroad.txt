The Concord and Claremont Railroad was an American railroad company during the mid-nineteenth century in New Hampshire spanning from Concord to Claremont.


== History ==
Chartered on June 24, 1848, the Concord and Claremont Railroad was established and construction had begun on November 19, 1848. Approximately ten months later on September 21, the railroad was opened from Concord to Warner.
The very first train to travel the line left Warner and had approximately 500 passengers aboard the 9 passenger coaches. On the return trip from Concord, the train carried about 800 passengers on the 18 passenger coaches which required a second locomotive pushing from the rear. The original railroad line was only 18 miles (29 km) long, but it was soon extended through Bradford, adding 9 miles (14 km) to the line.
In 1852, the railroad filed for bankruptcy; it was merged in 1853 with the New Hampshire Central Railroad forming what was known as the Merrimac and Connecticut Rivers Railroad Company. In 1874, the Sugar River Railroad, which built and ran their rail line from Newport to Claremont, merged with the Contoocook Valley Railroad and again created the Concord and Claremont Railroad, under the control of the Northern Railroad.
In 1887, the Boston and Maine Railroad absorbed the Concord and Claremont, and the line was now known as the Claremont branch of the Boston & Maine.


== Abandonments ==
Difficult economic times and the advancement of the automobile forced many railroads to close rail lines. The Boston & Maine Railroad was to sell off the rights to many of the lines and rights of way it held, freeing up the Claremont branch as an independent in 1954, this time as the Claremont and Concord Railroad. Once this had taken place, the Claremont and Concord's abandonments took place from east to west:
1960: 9 miles, W. Concord - west to Contoocook
1961: 17.6 miles, Contoocook - west to Bradford
1964: 16+ miles, Bradford - northwest to Newport
1977: 11 miles, Newport - west to Claremont
1988: 1.5 miles within Claremont
1988: 3 mile section - Electric Claremont Railway (opened in 1903)


== Present uses ==
The Claremont and Concord Railroad now operates on a short line between Claremont Junction on the main line to Claremont.
9 miles (14 km) of the line between Claremont and Newport is now the Sugar River Recreational Rail Trail, owned and managed by the New Hampshire Division of Parks and Recreation.


== References ==