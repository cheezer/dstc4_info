ATMA Classique is a Canadian record label based in Montreal, Quebec. Founded in 1994 by Johanne Goyette, the company has over 450 titles in its catalogue and distributes in over 25 countries and on the internet. Several recordings released by the company have won Juno and Felix Awards.


== Artists ==
Marc Hervieux, tenor
Marie-Josée Lord, soprano
Yannick Nézet-Séguin, conductor
Orchestre Métropolitain
Bernard Labadie, conductor
Les Violons du Roy
Karina Gauvin, soprano
Orchestre Symphonique de Québec
David Jalbert, pianist
Les Voix humaines
Janina Fialkowska, pianist
Marianne Fiset, soprano
Les Voix Baroques
Pentaèdre
Suzie LeBlanc, soprano
Musica intima
Jean-Marie Zeitouni, conductor
Quatuor Alcan


== References ==


== External links ==
ATMA Classique Official Website