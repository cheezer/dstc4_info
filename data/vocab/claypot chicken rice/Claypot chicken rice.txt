Claypot chicken rice (Chinese: 砂煲鸡饭, 瓦煲鸡饭 or 煲仔鸡饭) is usually a dinner dish in the southern regions of China, Malaysia and Singapore. It is typically served with Chinese sausage and vegetables. More often than not, the rice is cooked in the claypot first and cooked ingredients like diced chicken and Chinese sausage are added in later.
Traditionally, the cooking is done over a charcoal stove, giving the dish a distinctive flavour. Some places serve it with dark soya sauce and also dried salted fish. Salted fish enhances the taste of the claypot chicken rice, depending on the diner's preference. Due to the time-consuming method of preparation and slow-cooking in a claypot, customers might have to wait a period of time (typically 15–30 minutes) before the dish is ready.


== See also ==

Clay pot cooking
List of rice dishes


== References ==