Clementi can refer to:
People
Aldo Clementi (1925–2011), Italian composer
Anna Clementi, singer, soprano; daughter of Aldo
Cecil Clementi (1875–1947), a British colonial administrator and the Governor of Hong Kong from 1925 to 1930
Enrico Clementi (born 1931), an Italian pioneer in computational techniques
Muzio Clementi (1752–1832), an Italian/English composer
Rich Clementi, an American mixed martial artist
Suicide of Tyler Clementi, a 2010 incident in which a college student committed suicide after his sexual encounter with another man was video-streamed over the internet
Other
Clementi, Singapore, a neighbourhood of Singapore
Clementi MRT Station, Singapore
Clementi Secondary School, a secondary school in North Point Hong Kong
Clementi Police Division, a police division of the Singapore Police Force