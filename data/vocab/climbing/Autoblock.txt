Using an Autoblock is a climbing and caving technique used in rappelling and ascending.
An autoblock is a rope device that slides freely over the rope during a controlled descent, but jams in the descender in the event of a sudden drop or loss of control, stopping the descent. This prevents uncontrolled falls in the event of an accident in which the abseiler loses control of the rope.
It is often made using a friction hitch around the rope that is being descended, and may be combined with other climbing equipment for further safety.


== See also ==
Bachmann knot
Klemheist knot
Prusik knot


== References ==
^ "How to Tie and Use an Autoblock Knot for Climbing". Retrieved 2015-04-24. 
^ "6-Step Guide to Rappelling with an Autoblock Backup". Retrieved 2015-04-24. 


== External links ==
"The Klemheist", Animated Knots by Grog. Accessed: May 5, 2013.
Backing up an Abseli