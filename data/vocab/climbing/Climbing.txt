Climbing is the activity of using one's hands, feet, or any other part of the body to ascend a steep object. It is done recreationally, competitively, in trades that rely on it, and in emergency rescue and military operations. It is done indoors and out, on natural and manmade structures.


== Types ==
Climbing activities include:
Bouldering: Ascending boulders or small outcrops, often with climbing shoes and a chalk bag or bucket. Usually, instead of using a safety rope from above, injury is avoided using a crash pad and a human spotter (to direct a falling climber on to the pad. They can also give beta, or advice)
Buildering: Ascending the exterior skeletons of buildings, typically without protective equipment.
Canyoneering: Climbing along canyons for sport or recreation.
Chalk climbing: Ascending chalk cliffs uses some of the same techniques as ice climbing [1].
Competition Climbing: A formal, competitive sport of recent origins, normally practiced on artificial walls that resemble natural rock formations. The International Federation of Sport Climbing (IFSC) is the official organization governing competition climbing worldwide and is recognized by the IOC and GAISF and is a member of the International World Games Association (IWGA). Competition Climbing has three major disciplines: Lead, Bouldering and Speed.
Ice climbing: Ascending ice or hard snow formations using special equipment, usually ice axes and crampons. Techniques of protecting the climber are similar to those of rock climbing, with protective devices (such as ice screws and snow wedges) adapted to frozen conditions.
Indoor climbing: Top roping, lead climbing, and bouldering artificial walls with bolted holds in a climbing gym.
Mountaineering: Ascending mountains for sport or recreation. It often involves rock and/or ice climbing.
Pole climbing (gymnastic): Climbing poles and masts without equipment.
Lumberjack tree-trimming and competitive tree-trunk or pole climbing for speed using spikes and belts.
Rock climbing: Ascending rock formations, often using climbing shoes and a chalk bag. Equipment such as ropes, bolts, nuts, hexes and camming devices are normally employed, either as a safeguard or for artificial aid.
Rope access: Industrial climbing, usually abseiling, as an alternative to scaffolding for short works on exposed structures.
Rope climbing: Climbing a short, thick rope for speed. Not to be confused with roped climbing, as in rock or ice climbing.
Scrambling which includes easy rock climbing, and is considered part of hillwalking.
Sport climbing is a form of rock climbing that relies on permanent anchors fixed to the rock, and possibly bolts, for protection, (in contrast with traditional climbing, where the rock is typically devoid of fixed anchors and bolts, and where climbers must place removable protection as they climb).
Top roping: Ascending a rock climbing route protected by a rope anchored at the top and protected by a belayer below
Traditional climbing (more casually known as Trad climbing) is a form of climbing without fixed anchors and bolts. Climbers place removable protection such as camming devices, nuts, and other passive and active protection that holds the rope to the rock (via the use of carabiners and webbing/slings) in the event of a fall and/or when weighted by a climber.
Free solo climbing: Climbing without ropes or protection.
Tree climbing: Recreationally ascending trees using ropes and other protective equipment.
A tower climber is a professional who climbs broadcasting or telecommunication towers or masts for maintenance or repair.
Rock, ice and tree climbing all usually use ropes for safety or aid. Pole climbing and rope climbing were among the first exercises to be included in the origins of modern gymnastics in the late 18th century and early 19th century.


== See also ==
Fall factor
Arboreal locomotion
Climbing clubs
Climbing equipment
Climbing organisations
List of climbers - Notable rock and ice climbers
List of climbing topics
Glossary of climbing terms
Glossary of knots common in climbing
Outdoor education
Outdoor activity
Rock climbing
Parkour
Scrambling


== References ==


== External links ==
Climbing at DMOZ