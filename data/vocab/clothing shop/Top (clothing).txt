A top is clothing that covers at least the chest, but which usually covers most of the upper human body between the neck and the waistline. The bottom of tops can be as short as mid-torso, or as long as mid-thigh. Men's tops are generally paired with pants, and women's with pants or skirts. Common types of tops are t-shirts, blouses and shirts.


== Design ==
The neckline is the highest line of the top, and may be as high as a head-covering hood, or as low as the waistline or bottom hem of the top. A top may be worn loose or tight around the bust or waist, and may have sleeves or shoulder straps, spaghetti straps (noodle straps), or may be strapless. The back may be covered or bare. Tops may have straps around the waist or neck, or over the shoulders.


== Types ==


== Neck styles ==


== See also ==
Toplessness


== References ==


== External links ==