Coffee is a widely consumed beverage.
Coffee may also refer to:
In computers:
COFFEE (Cinema 4D), a computer scripting language
CoFFEE, the Collaborative Face-to-Face Educational Environment, an educational collaborative software for digital classrooms
In geography:
Coffee County (disambiguation), several counties in the United States
Coffee Precinct, Wabash County, Illinois
Coffee Crater, British Columbia, Canada
Coffee Creek (disambiguation)
Isle aux Herbes (Alabama), also known as Coffee Island
Coffee Swamp, Wisconsin
Other uses:
Coffee (color)
Coffee (surname)
Schweigt stille, plaudert nicht, BWV 211, a cantata by Johann Sebastian Bach, also known as the Coffee Cantata
Ethiopian Coffee FC, a football club based in Addis Ababa, Ethiopia
Centre of Full Employment and Equity (CofFEE), a research centre at the University of Newcastle, Australia
Coffee High School, Douglas, Georgia
"Coffee", a song on the album None Shall Pass by the American alternative hip hop musician Aesop Rock
Coffee, euphemism for sex
Coffee (Miguel song) (Released May 4th, 2015)


== See also ==
Coffee City, Texas, a town
Coffea, a genus of flowering plants, several species of which provide the seeds for the popular beverage of coffee
COFEE, the Computer Online Forensic Evidence Extractor, a tool kit for computer forensic investigators
Coffey (disambiguation)
Coffy (1973), blaxploitation film