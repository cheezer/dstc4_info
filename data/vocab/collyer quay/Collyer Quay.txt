Collyer Quay (Chinese: 哥烈码头) is a road in Downtown Core, Singapore that starts after Fullerton Road and ends at the junction of Raffles Quay, Finlayson Green and Marina Boulevard. The road houses several landmarks namely, Clifford Pier, Change Alley (Singapore), Hitachi Tower, Ocean Towers and Ocean Financial Centre.


== History ==
Built around 1864 and 1865, it originally referred to the sea wall designed in 1858. The sea wall was named after Captain (later Colonel) George Chancellor Collyer, the army engineer of the Madras Engineers who was appointed Chief Engineer in 1862 for the fortification of the colony. However, his fortifications were useless, and therefore it was known as Collyer's Folly. Collyer did a lot of work in town planning and was connected to the scheme of the Tanjong Pagar Dock.
In the past, there was no road named "Collyer Quay" with all the buildings along the island's shoreline were directing facing the sea. The buildings were linked by a continuous verandah on the second storey, where people walked from one building to another. Steam trams plied the quay in 1882, and by the 1900s, a new generation of buildings started to appear. Currently, some have up to six buildings built on the site such as the HSBC Building, Hitachi Tower and Ocean Towers. A simple memorial has been erected near Clifford Pier, to pay tribute to the immigrants of different races for their contributions to developing the nation. The foundation stone was laid by the first president Encik Yusof Bin Ishak.
In Cantonese, the road was known as hoong tang and in Hokkien, ang theng lor, meaning "red lamp road", after the red warning beacon lights at Johnston's Pier which was later rebuilt to make way for the Clifford Pier. The Chinese also called it tho kho au, meaning "at the back of the godowns". The original development of the quay had its first buildings facing Commercial Square (Raffles Place) and only outhouses and sheds were facing the shore.
Until the late 1960s the front of Clifford Pier was a carpark. After office hours the carpark was transformed into a gathering place for musicians, mobile foodstalls and prostitutes. The carpark later made way for road-widening and construction of new developments.


== New developments ==

There are new developments at the water front property along Collyer Quay between Marina Boulevard and One Fullerton. A new waterfront hotel, called the Fullerton Bay Hotel, opened in 2010. The historical buildings, these being Clifford Pier and the former Customs Harbour Branch Building, were incorporated into the new developments. The hotel and the historical buildings are connected to incorporate the Marina Promenade and to allow free pedestrian traffic along Marina Bay. A new office building, OUE Bayfront was also built to replace the former OUE building which stood beside Clifford Pier.


== References ==
^ "History of Collyer Quay". Retrieved 2009-12-08. 
Victor R Savage, Brenda S A Yeoh (2004), Toponymics A Study of Singapore Street Names, Eastern University Press, ISBN 981-210-364-3