Collyer may refer to:


== People ==
Bud Collyer, an American television game show star
Chad Collyer, an American professional wrestler
Collyer brothers, famous recluses
Collyer brothers (game designers), the creators of Championship Manager
Elsie Collyer (E. Collyer), a zoologist
Geoff Collyer, a Canadian computer scientist
Jaime Collyer, a Chilean writer
Joseph Collyer, an English engraver
Mary Collyer, an English translator and novelist
Robert Collyer, an American clergyman


== Locations ==
Collyer, Kansas, United States, a city in Trego County
The College of Richard Collyer, also known as Collyer's, in Horsham, West Sussex, England


== See also ==
Collier (disambiguation)