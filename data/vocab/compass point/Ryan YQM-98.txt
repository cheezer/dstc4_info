The Ryan YQM-98 R-Tern (also called Compass Cope R) was a developmental aerial reconnaissance drone developed by Ryan Aeronautical. It could take off and land from a runway like a manned aircraft, and operate at high altitudes for up to 24 hours to perform surveillance, communications relay, or atmospheric sampling.


== Design and development ==
"Compass Cope" was a program initiated by the United States Air Force (USAF) in 1971 to develop an upgraded reconnaissance drone that could take off and land from a runway like a manned aircraft, and operate at high altitudes for up to 24 hours to perform surveillance, communications relay, or atmospheric sampling. Two aircraft, the Boeing YQM-94 Compass Cope B, and the Ryan Aeronautical YQM-98A Compass Cope R participated in the program.
Boeing was originally selected as a sole source for the Compass Cope program, with the USAF awarding the company a contract for two YQM-94A (later YGQM-94A) demonstrator vehicles in 1971. However, Ryan then pitched an alternative, and the next year the USAF awarded Ryan a contract for two YQM-98A (later YGQM-98A) demonstrators as well.
Ryan's entry into the competition was the Model 235, an updated variant of the Model 154 / AQM-91 Firefly. The YQM-98A's general configuration was similar to that of the Boeing Compass Cope B, resembling a jet sailplane with a twin-fin tail, retractable tricycle landing gear, and an engine in a pod on its back. The engine was a Garrett YF104-GA-100 turbofan, with 4,050 pound-force (18.0 kN) thrust. The Compass Cope R had a clear resemblance to the Model 154, though its wings were straight instead of swept.
Initial flight of the first Compass Cope R demonstrator was on 17 August 1974. However, the Boeing Compass Cope B won the competition in August 1976 on the basis of lower cost, with the company awarded a contract to build preproduction prototypes of the YQM-94B operational UAV.
Since the evaluation of the Compass Cope prototypes had shown the Ryan YQM-98 to be superior to the Boeing YQM-94 in some respects, Ryan challenged the award. It did no good, since the entire Compass Cope program was cancelled in July 1977, apparently because of difficulties in developing sensor payloads for the aircraft.


== Specifications (Compass Cope R) (performance estimated) ==
Data from Jane's All The World's Aircraft 1976–77
General characteristics
Crew: None
Length: 38 ft 4 in (11.68 m)
Wingspan: 81 ft 2½ in (24.75 m)
Height: 8 ft 0 in (2.44 m)
Wing area: 347 ft2 (32.24 m2)
Aspect ratio: 19:1
Empty weight: 5,600 lb (2,540 kg)
Gross weight: 14,310 lb (6,490 kg) each
Performance
Maximum speed: Mach 0.6
Endurance: 30 hours


== See also ==
Related development
Ryan AQM-91 Firefly
Aircraft of comparable role, configuration and era
Boeing YQM-94


== References ==

Taylor, John W. R. Jane's All The World's Aircraft 1976–77. London: Jane's Yearbooks, 1976. ISBN 0-354-00538-3.
This article contains material that originally came from the web article Unmanned Aerial Vehicles by Greg Goebel, which exists in the Public Domain.


== External links ==
Compass Cope USAF fact sheet
Teledyne Ryan GQM-98 R-Tern