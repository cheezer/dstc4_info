New Concorde (NC) is a Los Angeles, California based film distribution company founded by Roger Corman. NC got its start in 1983 when Corman formed the production and distribution Concorde-New Horizons (CNH) as one of the first production companies to develop and take advantage of video as a distribution tool.


== History ==
The company was originally Concorde-New Horizons, which was itself created when Corman combined his two new companies Concorde Pictures (CP) and New Horizons Pictures in 1983. The company is now officially known as New Horizons Picture Corp.


=== Beginnings ===
Corman founded New World Productions (NWP) in 1970, and had been making low budget genre films until 1982. When larger studios began producing the same genres with larger budgets that his company could not meet, and after being approached by a consortium of attorneys wishing to buy the company, he opted in 1982 to sell his interests. However, when he left NWP, he retained all rights to his large back catalogue of films and took most members of his creative team with him. He wished to continue producing films but without the tedious negotiations required for distribution, and so in 1983 he formed Concorde Pictures and then New Horizons pictures as companies to produce films that New World was to distribute. He met with various contractual and legal problems when dealing with New World, causing his plans with them to fall through. He then combined the two firms to create Concorde-New Horizon, and began talks with independent producers in the hopes of setting up a distribution arm for CNH. Those talks were not successful, and this left Corman distributing his own films.


=== Growth ===
This proved fortunate for Corman, as CNH came into existence during the beginnings of the home video boom. He found he was in an ideal position to capitalize on the new market. Using his extensive back catalogue and his creative team he was able to take full advantage of the new and growing video market and created films specifically targeted toward home video. This made Concorde-New Horizons one of the first production companies to fully develop and capitalize on video as a distribution tool. This allowed Corman's Concorde-New Horizons to be more prolific than his former New World Productions, but that productivity resulted in a lowering of standards. The new New World films were themselves seen as producers of low quality products, but their films were seen as having an energy and charm that Corman's films seemed to lack. However, his goal to create films for a direct-to-video market has been financially successful.
In early 2000 Corman renamed the firm 'New Concorde', sold the New Horizons Pictures (NHP) branch, and reorganized to form New Concorde Home Entertainment.
In 2005 Concorde signed a 12-year deal with Buena Vista Home Entertainment giving BVHE distribution rights to the more than 400 Roger Corman produced films.
As of 2010, Shout! Factory has acquired the rights to the films.


== Production & distribution ==
Concorde-New Horizons has produced over 122 films, including Bloodfist 2050, Shadow Dancer, The Sea Wolf, Munchie Strikes Back, and Summer Camp Nightmare, and has distributed over 39 films, including Supergator, Slaughter Studios, Dragon Fire, and Eye of the Eagle. The New Concorde Home Entertainment concentrates on distribution, and has released over 90 films, including Dinocroc, Avalanche Alley, Humanoids from the Deep, Munchies, and The Slumber Party Massacre. Concorde Pictures, has produced 9 films, including Killer Instinct, Watchers II, Time Trackers, and The Drifter, and has distributed over 144 films, including Avalanche Alley, The Sea Wolf, Humanoids from the Deep, Star Hunter, and Wizards of the Lost Kingdom.


=== Partial filmography ===


==== Production ====


==== Distribution ====


== References ==


== External links ==
Official website
About New Horizons Pictures
Concorde Pictures (US) at the Internet Movie Database
New Horizons Picture (US) at the Internet Movie Database
Concorde-New Horizons (US) at the Internet Movie Database