Copthorne Hotel Cardiff-Caerdydd is a four star hotel (formerly five star) in Culverhouse Cross, a western suburb of Cardiff, capital of Wales.
The hotel is operated by Millennium & Copthorne. It is located near the Wenvoe transmitting station, off the A4050 road near the Culverhouse Cross roundabout (which intersects with the A48 and the A4232).
The hotel has 135 bedrooms and also has a swimming pool, steam room, sauna, whirlpool and gymnasium. It was built in 1993 and has a lake at the front of the hotel which, when built, had a mound built at the front to hide the lake from the view of the road.
The hotel was one of the earliest developments to take place at the Culverhouse Cross site as the development boom took off throughout the 1990s. Today the hotel is closely surrounded by retail outlets.
The hotel made the news in the early 2000s after two guests had died from Legionnaires' disease, caused by an incorrectly-installed humidifier. The hotel was fined £40,000 on top of £15,000 costs.


== References ==


== External links ==
Official site