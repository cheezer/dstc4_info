A cross is a geometrical figure consisting of two lines or bars perpendicular to each other, dividing one or two of the lines in half. The lines usually run vertically and horizontally; if they run obliquely, the design is technically termed a saltire, although the arms of a saltire need not meet at right angles.
The cross is one of the most ancient human symbols, and has been used by many religions, most notably Christianity. It may be seen as a division of the world into four elements (Chevalier, 1997) or cardinal points, or alternately as the union of the concepts of divinity, the vertical line, and the world, the horizontal line (Koch, 1955).


== Etymology ==
The word cross comes ultimately from Latin crux, a Roman torture device used for crucifixion, via Old Irish cros. The word was introduced to English in the 10th century as the term for the instrument of the torturous execution of Jesus as described in the New Testament, gradually replacing the earlier word rood.


== History ==

Due to the simplicity of the design (two crossing lines), cross-shaped incisions make their appearance from deep prehistory; as petroglyphs in European cult caves, dating back to the beginning of the Upper Paleolithic, and throughout prehistory to the Iron Age.
Prior to 2000 B.C. the cross symbol, †, was already in use in ancient alphabets (Paleo-Hebrew [✗], Canaanite, Phoenician) as the letter 'Tau' ('Taw'/'Tav') which corresponds to the modern letter 'T' and meant 'Mark' (Its literal usage in the Torah denotes a wound). It is probably derived from two sticks crossed to mark a place similar to the Egyptian hieroglyph.
Use of the cross as a Christian symbol may be as early as the 1st century, and is certain for the 3rd century. A wide variation of cross symbols is introduced for the purposes of heraldry beginning in the age of the Crusades.
The earliest depiction of the cross as a christian symbol may be as early as 200 A.D. when it was used to mock the faith in the Alexamenos graffito.


== As markings ==

Written crosses are used for many different purposes, particularly in mathematics.
The addition (or plus) sign (+) and the multiplication (or times) sign (×) are cross shapes.
A cross is often used as a check mark because it can be clearer, easier to create with an ordinary pen or pencil, and less obscuring of any text or image that is already present than a large dot. It also allows marking a position more accurately than a large dot.
The Chinese character for ten is 十 (see Chinese numerals).
The dagger or obelus (†) is a cross
The Georgian letters ქ and ჯ are crosses.
In the Latin alphabet, the letter X and the minuscule form of t are crosses.
The Roman numeral for ten is X.
A large cross through a text often means that it is wrong or should be considered deleted. A cross is also used stand-alone (✗) to denote rejection.


== Cross-like emblems ==
For variants of the Christian cross symbol, see Christian cross variants and Crosses in heraldry.

As a design element


== Other noteworthy crosses ==
Crux, or the Southern Cross, is a cross-shaped constellation in the Southern Hemisphere. It appears on the national flags of Australia, Brazil, New Zealand, Niue, Papua New Guinea and Samoa.
The tallest cross, at 152.4 metres high, is part of Francisco Franco's monumental "Valley of the Fallen", the Monumento Nacional de Santa Cruz del Valle de los Caidos in Spain.
A cross at the junction of Interstates 57 and 70 in Effingham, Illinois, is purportedly the tallest in the United States, at 198 feet (60.3 m) tall.
The tallest freestanding cross in the United States is located in Saint Augustine, FL and stands 208 feet.
The tombs at Naqsh-e Rustam, Iran, made in the 5th century BC, are carved into the cliffside in the shape of a cross. They are known as the "Persian crosses".


== As physical gestures ==
Cross shapes are made by a variety of physical gestures. Crossing the fingers of one hand is a common invocation of the symbol. The sign of the cross associated with Christian genuflection is made with one hand: in Eastern Orthodox tradition the sequence is head-heart-right shoulder-left shoulder, while in Oriental Orthodox, Catholic and Anglican tradition the sequence is head-heart-left-right. Crossing the index fingers of both hands represents the number 10 in Chinese-speaking societies and a charm against evil in European folklore (hence its frequent appearance in vampire movies). Other gestures involving more than one hand include the "cross my heart" movement associated with making a promise and the Tau shape of the referee's "time out" hand signal.


== See also ==
Astrological symbols -the cross symbolically represents matter in many of these glyphs.
Astronomical symbols -the crossmark may have been added to Christianize pagan god symbols.
Cleché
Cross-ndj (hieroglyph)
Cross and Crown
Cross burning
Cross necklace
Crossbuck
Crossroads (mythology)


== References ==


=== Notes ===


=== Sources ===
Chevalier, Jean (1997). The Penguin Dictionary of Symbols. Penguin ISBN 0-14-051254-3.
Drury, Nevill (1985). Dictionary of Mysticism and the Occult. Harper & Row. ISBN 0-06-062093-5.
Koch, Rudolf (1955). The Book of Signs. Dover, NY. ISBN 0-486-20162-7.
Webber, F. R. (1927, rev. 1938). Church Symbolism: an explanation of the more important symbols of the Old and New Testament, the primitive, the mediaeval and the modern church. Cleveland, OH. OCLC 236708.


== External links ==
Seiyaku.com, all Crosses - probably the largest collection on the Internet
Lutheransonline.com, variations of Crosses—images and meanings
Cross & Crucifix - Glossary: Forms and Topics
Nasrani.net, Indian Cross
Freetattoodesigns.org, The Cross in Tattoo Art
The Christian Cross of Jesus Christ: Symbols of Christianity, Images, Designs and representations of it as objects of devotion