Crystal Mountain or Crystal Mountains may refer to:


== Ski areas ==
Crystal Mountain (Washington), a ski resort in Washington, United States
Crystal Mountain (British Columbia), a day-use ski area near West Kelowna, British Columbia in Canada
Crystal Mountain (Michigan), a four-star ski resort located in Benzie County, Michigan
Crystal Mountain (Egypt), between Bahariya Oasis and Farafra Oasis, Egypt


== Mountain ranges ==
Crystal Range, a small group of mountains in California
Crystal Mountains (Brazil), a mountain range located in Brazil
Crystal Mountains (Africa), a mountain range located on the west coast of central Africa


== Populated place ==
Crystal Mountain, Michigan, United States, a census-designated place


== Music ==
"Crystal Mountain", a 1995 song by Death from their album Symbolic
"Crystal Mountain", a 1982 song by Kenny G from his eponymous debut album Kenny G


== Other ==
Crystal Mountains National Park, national park in northwestern Gabon
Sierra Cristal National Park, a park in Cuba