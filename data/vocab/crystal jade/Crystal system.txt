In crystallography, the terms crystal system, crystal family, and lattice system each refer to one of several classes of space groups, lattices, point groups, or crystals. Informally, two crystals tend to be in the same crystal system if they have similar symmetries, though there are many exceptions to this.
Crystal systems, crystal families, and lattice systems are similar but slightly different, and there is widespread confusion between them: in particular the trigonal crystal system is often confused with the rhombohedral lattice system, and the term "crystal system" is sometimes used to mean "lattice system" or "crystal family".
Space groups and crystals are divided into 7 crystal systems according to their point groups, and into 7 lattice systems according to their Bravais lattices. Five of the crystal systems are essentially the same as five of the lattice systems, but the hexagonal and trigonal crystal systems differ from the hexagonal and rhombohedral lattice systems. The six crystal families are formed by combining the hexagonal and trigonal crystal systems into one hexagonal family, in order to eliminate this confusion.


== Overview ==

A lattice system is generally identified as a set of lattices with the same shape according to the relative lengths of the cell edges (a, b, c) and the angles between them (α, β, γ). Each lattice is assigned to one of the following classifications (lattice types) based on the positions of the lattice points within the cell: primitive (P), body-centered (I), face-centered (F), base-centered (A, B, or C), and rhombohedral (R). The 14 unique combinations of lattice systems and lattice types are collectively known as the Bravais lattices. Associated with each lattice system is a set of point groups, sometimes called lattice point groups, which are subgroups of the arithmetic crystal classes. In total there are seven lattice systems: triclinic, monoclinic, orthorhombic, tetragonal, rhombohedral, hexagonal, and cubic.
A crystal system is a set of point groups in which the point groups themselves and their corresponding space groups are assigned to the same lattice system. Of the 32 point groups that exist in three dimensions, most are assigned to only one lattice system, in which case the crystal system and lattice system both have the same name. However, five point groups are assigned to two lattice systems, rhombohedral and hexagonal, because both lattice systems exhibit threefold rotational symmetry. These point groups are assigned to the trigonal crystal system. In total there are seven crystal systems: triclinic, monoclinic, orthorhombic, tetragonal, trigonal, hexagonal, and cubic.
A crystal family is determined by lattices and point groups. It is formed by combining crystal systems which have space groups assigned to the same lattice system. In three dimensions, the crystal families are identical to the crystal systems except the hexagonal and trigonal crystal systems are combined into one hexagonal crystal family. In total there are six crystal families: triclinic, monoclinic, orthorhombic, tetragonal, hexagonal, and cubic.
Spaces with less than three dimensions have the same number of crystal systems, crystal families, and lattice systems. In zero- and one-dimensional space, there is one crystal system. In two-dimensional space, there are four crystal systems: oblique, rectangular, square, and hexagonal.
The relation between three-dimensional crystal families, crystal systems, and lattice systems is shown in the following table:
Caution: There is no "trigonal" lattice system. To avoid confusion of terminology, don't use the term "trigonal lattice"; use the definition that "trigonal lattice" = "hexagonal lattice" ≠ "rhombohedral lattice".


== Crystal classes ==
The 7 crystal systems consist of 32 crystal classes (corresponding to the 32 crystallographic point groups) as shown in the following table:
Point symmetry can be thought of in the following fashion: consider the coordinates which make up the structure, and project them all through a single point, so that (x,y,z) becomes (-x,-y,-z). This is the 'inverted structure'. If the original structure and inverted structure are identical, then the structure is centrosymmetric. Otherwise it is non-centrosymmetric. Still, even for non-centrosymmetric case, inverted structure in some cases can be rotated to align with the original structure. This is the case of non-centrosymmetric achiral structure. If the inverted structure cannot be rotated to align with the original structure, then the structure is chiral (enantiomorphic) and its symmetry group is enantiomorphic.
A direction (meaning a line without an arrow) is called polar if its two directional senses are geometrically or physically different. A polar symmetry direction of a crystal is called a polar axis. Groups containing a polar axis are called polar. A polar crystal possess a "unique" axis (found in no other directions) such that some geometrical or physical property is different at the two ends of this axis. It may develop a dielectric polarization, e.g. in pyroelectric crystals. A polar axis can occur only in non-centrosymmetric structures. There should also not be a mirror plane or 2-fold axis perpendicular to the polar axis, because they will make both directions of the axis equivalent.
The crystal structures of chiral biological molecules (such as protein structures) can only occur in the 11 enantiomorphic point groups (biological molecules are usually chiral).


== Lattice systems ==
The distribution of the 14 Bravais lattice types into 7 lattice systems is given in the following table.

In geometry and crystallography, a Bravais lattice is a category of symmetry groups for translational symmetry in three directions, or correspondingly, a category of translation lattices.
Such symmetry groups consist of translations by vectors of the form

where n1, n2, and n3 are integers and a1, a2, and a3 are three non-coplanar vectors, called primitive vectors.
These lattices are classified by space group of the translation lattice itself; there are 14 Bravais lattices in three dimensions; each can apply in one lattice system only. They represent the maximum symmetry a structure with the translational symmetry concerned can have.
All crystalline materials must, by definition fit in one of these arrangements (not including quasicrystals).
For convenience a Bravais lattice is depicted by a unit cell which is a factor 1, 2, 3 or 4 larger than the primitive cell. Depending on the symmetry of a crystal or other pattern, the fundamental domain is again smaller, up to a factor 48.
The Bravais lattices were studied by Moritz Ludwig Frankenheim (1801–1869), in 1842, who found that there were 15 Bravais lattices. This was corrected to 14 by A. Bravais in 1848.


== Crystal systems in four-dimensional space ==
The four-dimensional unit cell is defined by four edge lengths () and six interaxial angles (). The following conditions for the lattice parameters define 23 crystal families:
1 Hexaclinic: 
2 Triclinic: 
3 Diclinic: 
4 Monoclinic: 
5 Orthogonal: 
6 Tetragonal Monoclinic: 
7 Hexagonal Monoclinic: 
8 Ditetragonal Diclinic: 
9 Ditrigonal (Dihexagonal) Diclinic: 
10 Tetragonal Orthogonal: 
11 Hexagonal Orthogonal: 
12 Ditetragonal Monoclinic: 
13 Ditrigonal (Dihexagonal) Monoclinic: 
14 Ditetragonal Orthogonal: 
15 Hexagonal Tetragonal: 
16 Dihexagonal Orthogonal: 
17 Cubic Orthogonal: 
18 Octagonal: 
19 Decagonal: 
20 Dodecagonal: 
21 Di-isohexagonal Orthogonal: 
22 Icosagonal (Icosahedral): 
23 Hypercubic: 
The names here are given according to Whittaker. They are almost the same as in Brown et al, with exception for names of the crystal families 9, 13, and 22. The names for these three families according to Brown et al are given in parenthesis.
The relation between four-dimensional crystal families, crystal systems, and lattice systems is shown in the following table. Enantiomorphic systems are marked with asterisk. The number of enantiomorphic pairs are given in parentheses. Here the term "enantiomorphic" has different meaning than in table for three-dimensional crystal classes. The latter means, that enantiomorphic point groups describe chiral (enantiomorphic) structures. In the current table, "enantiomorphic" means, that group itself (considered as geometric object) is enantiomorphic, like enantiomorphic pairs of three-dimensional space groups P31 and P32, P4122 and P4322. Starting from four-dimensional space, point groups also can be enantiomorphic in this sense.


== See also ==
Crystal cluster
Crystal structure
List of the 230 crystallographic 3D space groups
Polar point group


== Notes ==


== References ==
Hahn, Theo, ed. (2002). International Tables for Crystallography, Volume A: Space Group Symmetry A (5th ed.). Berlin, New York: Springer-Verlag. doi:10.1107/97809553602060000100. ISBN 978-0-7923-6590-7. 


== External links ==
Overview of the 32 groups
Mineral galleries – Symmetry
all cubic crystal classes, forms and stereographic projections (interactive java applet)
Crystal system at the Online Dictionary of Crystallography
Crystal family at the Online Dictionary of Crystallography
Lattice system at the Online Dictionary of Crystallography
Conversion Primitive to Standard Conventional for VASP input files
Learning Crystallography