A cultural center or cultural centre is an organization, building or complex that promotes culture and arts. Cultural centers can be neighborhood community arts organizations, private facilities, government-sponsored, or activist-run.


== Asia ==

Bangkok Art and Culture Centre
Cultural Center of the Philippines
Hong Kong Cultural Centre, Hong Kong, China
Kaohsiung Cultural Center, Kaohsiung, Taiwan
Keelung Cultural Center, Keelung, Taiwan
Ketagalan Culture Center, Taipei, Taiwan
King Abdulaziz Center for World Culture, Dhahran, Saudi Arabia
Mongolian and Tibetan Cultural Center, Taipei, Taiwan
Taichung City Dadun Cultural Center, Taichung, Taiwan
Tainan Municipal Cultural Center, Tainan, Taiwan
Taipei Cultural Center, Taipei, Taiwan
Thailand Cultural Centre
Xinying Cultural Center, Tainan, Taiwan


== Europe ==

Vooruit, Ghent, Belgium
National Palace of Culture, Sofia, Bulgaria
Kulturværftet, Helsingør, Denmark
Centre Georges Pompidou, Paris, France
De Balie, Amsterdam, Netherlands
Letterkenny Regional Cultural Centre, County Donegal, Ireland
Glaspaleis, Heerlen, Netherlands
OT301, Amsterdam, Netherlands
Centro Cultural de Belem, Lisbon, Portugal
Dom omladine Beograda, Belgrade, Serbia
Kuryokhin Center, Saint Petersburg, Russia


== North America ==

El Centro Cultural de Mexico, Mexico
Polyforum Cultural Siqueiros, Mexico City, Mexico
Eyedrum, Atlanta, United States
Centro Cultural de la Raza, San Diego, CA, United States
Detroit Cultural Center, MI, United States
Cultural Center of Charlotte County, Port Charlotte, FL, United States
Self Help Graphics & Art, Los Angeles, United States
Tia Chucha's Centro Cultural, Los Angeles, CA, United States
La Peña Cultural Center, Berkeley, CA, United States
Chicago Cultural Center, Chicago, IL, United States
Irish Museum and Cultural Center, Kansas City, Missouri, United States
Asheville Culture Project, Asheville, NC, United States
Polynesian Cultural Center, Hawaii, United States
Howland Cultural Center, Beacon, United States
El Museo del Barrio, New York, NY, United States
The Kitchen, New York, NY, United States
ISSUE Project Room, New York, NY, United States
Park Performing Arts Center, Union City, New Jersey, United States
William V. Musto Cultural Center, Union City, New Jersey, United States
Centro Cultural Baudilio Vega Berríos, Mayagüez, Puerto Rico
The Largo Cultural Center, Largo, FL, United States


== Oceania ==

Perth Cultural Centre, Perth, Australia
Queensland Cultural Centre, Brisbane, Australia
Vanuatu Cultural Centre, Port Vila, Vanuatu


== South America ==
Agustín Ross Cultural Center, Pichilemu, Chile
Centro Cultural Néstor Kirchner, Ciudad de Buenos Aires, Argentina
Centro Cultural Banco do Brasil, Brasília, Belo Horizonte, Rio de Janeiro, São Paulo
Centro Cultural São Paulo, São Paulo, Brazil
Centro Cultural Palacio de La Moneda, Santiago, Chile
Centro Cultural Gabriela Mistral, Santiago, Chile
Ema Gordon Klabin Cultural Foundation, São Paulo, Brazil
Narguila Pub Lounge Cultural, Bogotá. Colombia
Centro Cultural Banco do Brasil, Rio de Janeiro, Brasilia, and São Paulo. Brazil.


== See also ==
art space
community centre
infoshop
music venue
social centre