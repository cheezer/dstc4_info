Green curry (Thai: แกงเขียวหวาน, rtgs: kaeng khiao wan, pronounced [kɛ̄ːŋ kʰǐaw wǎːn], literally sweet green curry) is a Central Thai variety of curry.


== EtymologyEdit ==
The name "green" curry derives from the color of the dish, which comes from green chillies. The "sweet" in the Thai name (wan means "sweet") refers to the particular color green itself and not to the taste of the curry. As this is a Thai curry based on coconut milk and fresh green chillies, the color comes out creamy mild green or, as this color is called in Thai, "sweet green". Its ingredients are not exactly fixed. The curry is not necessarily sweeter than other Thai curries but, although the spiciness varies, it tends to be more pungent than the milder red curries.


== IngredientsEdit ==
Apart from a main protein, traditionally fish, meat, or fish balls, the other ingredients for the dish consist of coconut milk, green curry paste, palm sugar, fish sauce. Thai eggplant (aubergine), pea aubergine, or other vegetables and even fruit are often included. The consistency of its sauce varies with the amount of coconut milk used. Green curry paste is traditionally made by pounding in a mortar green chillies, shallots, garlic, galangal, lemongrass, kaffir lime peel, coriander root, red turmeric, roasted cilantro (coriander) and cumin seeds, white peppercorns, shrimp paste and salt.


== Cooking methodEdit ==
The paste is fried in split coconut cream until the oil is expressed to release the aromas in the paste. Once the curry paste is cooked, more coconut milk and the remaining ingredients are added along with a pinch of palm sugar and fish sauce. Finally, as garnishes, Thai basil, fresh kaffir lime leaves, sliced phrik chi fa ("sky-pointing chilies", large mild chilies) are often used. For a more robust green curry, such as with seafood, julienned krachai (fingerroot/wild ginger/Chinese keys), white turmeric, and holy basil can be used as garnishes.


== ServingEdit ==
Green curry is typically eaten with rice as part of a wider range of dishes in a meal, or with round rice noodles known as khanom chin as a single dish. A thicker version of green curry made with, for instance, simmered beef, can also be served with roti, an Indian style flatbread that is similar to the roti canai in Malaysia.


== GalleryEdit ==


== See alsoEdit ==
Thai curry


== ReferencesEdit ==