Bartley Road is a major road in Singapore extending from Upper Serangoon Road to Tampines Avenue 10. The road has a distance of 5.8 km. En route, it passes through the areas of Serangoon, Bartley, Paya Lebar, Defu, Kaki Bukit and Bedok Reservoir. Bus services 28, 93 and 158 ply along the original Bartley Road from Upper Serangoon Road to Upper Paya Lebar Road.


== Sections ==
Bartley Road is split into three sections with the dividers at Upper Paya Lebar Road and Kaki Bukit Avenue 4. The three sections are the original Bartley Road, Paya Lebar Viaduct and Bartley Road East (Kaki Bukit Viaduct).


=== Bartley Road (original) ===
With a distance of 1.5 km, the original stretch of Bartley Road extending from Upper Serangoon Road to Upper Paya Lebar Road. This section is also part of the Outer Ring Road System.


=== Paya Lebar Viaduct ===
With a distance of 2.2 km, the Paya Lebar Viaduct section extending from Upper Paya Lebar Road to Airport Road. It has a 1.3 km four-lane carriageway long viaduct above the road. The viaduct also has a slip road at the Hougang Avenue 3 junction.


=== Bartley Road East ===
With a distance of 2.1 km, Bartley Road East, or Kaki Bukit Viaduct, is a viaduct starting from Kaki Bukit Avenue 4 and ends at the junction of Tampines Avenue 10 and Bedok Reservoir Road. It was opened to traffic on the 30 December 2003.


== Bartley Road Extension Project ==
Started on December 2000 and completed on 17 January 2010, the Bartley Road Extension Project consist of building new roads to connect Tampines Avenue 10 and Upper Serangoon Road with two viaducts. Once completed, it will provide a direct link from Tampines to Upper Serangoon Road and this will help to alleviate the heavy traffic conditions along Pan-Island Expressway and Bedok Reservoir Road during peak hours. It will also provide an alternative access from Paya Lebar to Tampines. By travelling along Bartley Road East itself, it can save about 10 minutes of travelling time and cut down the travelling distance of about 500 m compared to before.
The project was divided into two phases: Kaki Bukit Viaduct phase and Paya Lebar Viaduct phase. The Kaki Bukit Viaduct phase was completed by the third quarter of 2003 and the Paya Lebar phase was completed on 17 January 2010.


== Landmarks ==
Along the road, landmarks include (from west to east) Upper Serangoon Viaduct, Maris Stella High School, Bartley MRT Station, Bartley Christian Church, SPCA Singapore, Bartley Secondary School, Kim Chuan Depot, Paya Lebar Air Base and Bedok Reservoir Park.


== References ==
Opening Of A New Vehicular Viaduct-To Provide a More Direct Route between Tampines and Kaki Bukit
Construction Of Viaduct From Bartley Road To Airport Road