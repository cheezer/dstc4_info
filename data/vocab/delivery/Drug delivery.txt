Drug delivery refers to approaches, formulations, technologies, and systems for transporting a pharmaceutical compound in the body as needed to safely achieve its desired therapeutic effect. It may involve scientific site-targeting within the body, or it might involve facilitating systemic pharmacokinetics; in any case, it is typically concerned with both quantity and duration of drug presence. Drug delivery is often approached via a drug's chemical formulation, but it may also involve medical devices or drug-device combination products. Drug delivery is a concept heavily integrated with dosage form and route of administration, the latter sometimes even being considered part of the definition.
Drug delivery technologies modify drug release profile, absorption, distribution and elimination for the benefit of improving product efficacy and safety, as well as patient convenience and compliance. Drug release is from: diffusion, degradation, swelling, and affinity-based mechanisms. Most common routes of administration include the preferred non-invasive peroral (through the mouth), topical (skin), transmucosal (nasal, buccal/sublingual, vaginal, ocular and rectal) and inhalation routes. Many medications such as peptide and protein, antibody, vaccine and gene based drugs, in general may not be delivered using these routes because they might be susceptible to enzymatic degradation or can not be absorbed into the systemic circulation efficiently due to molecular size and charge issues to be therapeutically effective. For this reason many protein and peptide drugs have to be delivered by injection or a nanoneedle array. For example, many immunizations are based on the delivery of protein drugs and are often done by injection.
Current efforts in the area of drug delivery include the development of targeted delivery in which the drug is only active in the target area of the body (for example, in cancerous tissues) and sustained release formulations in which the drug is released over a period of time in a controlled manner from a formulation. In order to achieve efficient targeted delivery, the designed system must avoid the host's defense mechanisms and circulate to its intended site of action. Types of sustained release formulations include liposomes, drug loaded biodegradable microspheres and drug polymer conjugates.


== See alsoEdit ==
Thin film drug delivery
Self-microemulsifying drug delivery system
Acoustic targeted drug delivery
Neural drug delivery systems
Drug carrier
Bovine Submaxillary Mucin Coatings
Retrometabolic drug design


== ReferencesEdit ==
M. N. V. Ravi Kumar (2008), Handbook of Particulate Drug Delivery (2-Volume Set), American Scientific Publishers. ISBN 1-58883-123-X


== External linksEdit ==
Article in Chemical and Engineering News