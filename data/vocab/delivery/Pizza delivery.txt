Pizza delivery is a service in which a pizzeria or pizza chain delivers a pizza to a customer. An order is typically made either by telephone or over the internet to the pizza chain, in which the customer can request pizza type, size and other products alongside the pizza, commonly including soft drinks. Pizzas may be delivered in pizza boxes or delivery bags, and deliveries are made with either an automobile, motor scooter, or bicycle. Customers can, depending on the pizza chain, choose to pay online, or in person, with cash, credit or a debit card. A delivery fee is usually charged with what the customer has bought.


== HistoryEdit ==
It is difficult to pinpoint the exact origin of pizza delivery. DomiNick's Pizza (which would later become Domino's Pizza and recently just Domino's) is known to have used delivery in February 1961, since the delivery vehicle was involved in defining the company's ownership, however, Domino's founder Tom Monaghan notes that in the 1960s "Most places just delivered to get some volume before they could afford to cut out the delivery".


== OrderingEdit ==
Ordering pizza for delivery usually involves contacting a local pizza restaurant or chain by telephone or online. Online ordering is available in many countries, where some pizza chains offer online menus and ordering.
The pizza delivery industry has kept pace with technological developments since the 1980s beginning with the rise of the personal computer. Specialized computer software for the pizza delivery business helps determine the most efficient routes for carriers, track exact order and delivery times, manage calls and orders with PoS software, and other functions. Since 2008 GPS tracking technology has been used for real-time monitoring of delivery vehicles by customers over the Internet.
Some pizzeria, such as the Ontario-based Canadian chain Pizza Pizza will incorporate a guarantee to deliver within a predetermined period of time, or late deliveries will be free of charge. For example, Domino's Pizza had a commercial campaign in the 1980s and early 1990s which promised "30 minutes or it's free". This was discontinued in the United States in 1993 due to the number of lawsuits arising from accidents caused by hurried delivery drivers, but is still offered in some countries. Pizzerias with no time guarantee will commonly state to the customer an approximate time frame for a delivery, without making any guarantees as to the actual delivery time.
According to Domino's, Super Bowl Sunday is the most popular day for its pizza deliveries; others are Halloween, New Year's Day, New Year's Eve, and the day before Thanksgiving. Unscheduled events may also cause an increase in pizza deliveries; for example, Domino's stated that its sales during the O. J. Simpson slow-speed chase were as large as on Super Bowl Sunday.


== ChargeEdit ==
For decades, "free delivery" was a popular slogan for almost all pizza stores. In Australia, a portion of the delivery charge is given to the driver as the store is required to reimburse the driver for the use of a personal vehicle.
Domino's Pizza is credited with popularizing free pizza delivery in the United States. Pizza Hut began experimenting in 1999 with a 50-cent delivery charge in ten stores in the Dallas-Fort Worth area. By mid-2001 it was implemented in 95% of its 1,749 company-owned restaurants in the U.S., and in a smaller number of its 5,250 franchisee-owned restaurants. By 2002, a small percentage of stores owned or franchised by U.S. pizza companies Domino's and Papa John's were also charging delivery fees of 50 cents to $1.50, and some of Little Caesar's franchisees charged delivery fees. In 2005, Papa John's implemented delivery charges in the majority of its company-owned stores.
In some countries, it is common to give the pizza deliverer an optional tip upon paying for the order. In Canada and the United States, tipping for pizza delivery is customary. Opinions on appropriate amounts vary widely. Employees are legally obligated to report tips to their employer for income tax purposes, while independent contractors, who may charge a per-delivery fee to a restaurant, are legally obligated to report tips to the Internal Revenue Service.


== Delivery technologyEdit ==


=== Delivery bagEdit ===
Bags used to keep pizza hot while being transported are commonly referred to as hotbags or hot bags. Hotbags are thermal bags, typically made of vinyl, nylon, or Cordura, that passively retain heat. Material choice affects cost, durability, and condensation. Heated bags supply added heat through insertion of externally heated disks, electrical heating elements, or pellets heated by induction from electrically generated magnetic waves. Innovations in delivery bag designs have allowed without the usage of a fixed box for bike delivery, such as a hard frame, back straps, and waterproofing. These systems proved to be cheaper, more efficient and faster to use.


=== Pizza boxEdit ===

Modern pizza boxes are made of corrugated fiberboard. Corrugated board has a number of advantages for pizza delivery: it is cheap, recyclable, and disposable, it is stiff yet light, it is absorbent thus keeping oil and juice from leaking, and the channels of air in the cardboard have excellent insulation properties.
The history of the pizza box began in Naples during the 1800s where bakers put pizzas into metal containers called stufas: round, vented tin or copper containers with shelves that held the pizzas apart from one another. Since the 1940s pizza take-out was done with the pizza sitting on a round cardboard base and covered with a paper bag. It is believed Domino's developed the modern corrugated flat square pizza box in the early 1960s, but they never patented it. Patent designs for pizza boxes date to at least 1968. Innovations since have included various venting configurations; built-in holders for extra sauces; designs for easier recycling; and perforated tops so wedge-shaped pieces of cardboard can be used as plates. The lid of the box is often supported by a disposable plastic tripod on top of the pizza.
Pizza boxes have a large amount of corrugated fiberboard, individually and in total volume produced each year, but they are not accepted by some municipal recycling programs because the cardboard is often soaked with grease, making them unsuitable for some forms of recycling. Boxes may thus be commonly thrown away with household waste into landfills; a more environmentally friendly disposal option that has been proposed is a form of composting for pizza boxes. It is also possible to tear off unstained or unsaturated sections such as the lid and/or sides of the box and recycle those.


=== Pizza saverEdit ===

In 1985 Carmela Vitale was issued a patent for a plastic 3-legged tripod stool that would sit in the middle of the box and keep the top from sagging into the pie. Vitale called her device a "package saver" and used that term also as the title of her patent, but it has since been renamed the "pizza saver". Variations on the device have since been invented, such as a disposable plastic spatula whose handle holds the box top up; and a plastic tripod like that made by Vitalie, but with one of the legs serrated like a knife, making for easy cutting of stuck cheese and bread.


== EmploymentEdit ==

In the United States, two labor unions have been formed specifically for pizza delivery drivers: the now-defunct Association of Pizza Delivery Drivers (APDD) and the American Union of Pizza Delivery Drivers (AUPDD).
APDD was formed in 2002. Its initial claim to fame was as an Internet-based union, eschewing traditional methods of organizing, and making contributions and the sale of goods the center of its fundraising activities, instead of dues. People could join APDD using a form at their website, or chat with its officers in an IRC-compatible Java chat every Tuesday evening. At its peak, it claimed approximately 1,000 members in 46 US states. APDD held several certification votes in the US, but was never successful in organizing a local. In March 2006 APDD lost a lawsuit against a Domino's franchise in Mansfield, Ohio. This combined with massive debt left the union with little choice but to shut down.
AUPDD was founded in early 2006 by Jim Pohle, a driver for a Domino's Pizza store in Pensacola, Florida. It was certified as the representative union for his store in April 2006. Pohle cites the sub-minimum wage paid by his store as the instigating factor in forming a union.
While formed in the more traditional method of organizing at one's own workplace, AUPDD uses certain Internet-based techniques originated by APDD, such as its mass communications with the press and its fundraising activities (although more traditional dues are collected from the eleven members of the fledgling local). It also uses the Internet as its primary outreach to those wishing to start locals across the US.


== HazardsEdit ==

Pizza delivery, by its nature, can pose risks for those engaged in it, as they are required to go to the homes of strangers, in unfamiliar neighborhoods. In the U.S., pizza delivery drivers have been subjected to assault, kidnappings, robbery, and have sometimes been raped or killed on the job.
In 2004, Pizza Hut fired a delivery driver who shot and killed a robber while on the job, citing its company policy against employees carrying weapons. Other national chains such as Domino's and Papa John's also prohibit carrying weapons, though many independent pizzerias allow delivery persons to carry weapons in a legal manner. Employer restrictions on carrying weapons is a controversial issue in the U.S.
Fake orders are sometimes used to lure robbery victims or kidnappings, and delivery people have been injured and killed in robberies and kidnappings.


=== Prank orderEdit ===
Pizza places may be subject to prank orders for numerous pizzas or to random houses or a target house. A prank order may cost businesses money and aggravation, resulting in the restaurant throwing away the unpaid pizzas. For example, in November 2010 in Amherst, Massachusetts, a man claiming to be part of Bob Dylan's crew placed an order for 148 pizzas which cost nearly $4,000. Prank callers have been fined in Singapore for placing false orders.


== In popular cultureEdit ==

Pizza delivery has been featured as a major element in several media in popular culture. There are several works of fiction where the main character delivers pizzas, including Tom Wolfe's novel I Am Charlotte Simmons (2004), and Neal Stephenson's cyberpunk novel Snow Crash (1992), which posits a future in which pizza delivery is organised by the Mafia as one of the US's four major industries. Several feature films also use pizza delivery prominently, including the 1984 comedy Delivery Boys and the Spike Lee 1989 film Do the Right Thing. In the 2000 comedy film Dude, Where's My Car?, the cannabis-smoking protagonists are pizza deliverymen. In the case of other films, use of pizza delivery has been regarded by critics as "overly integrated product placement".
Pizza delivery has been the subject of films, even to the point of being the subject of such feature length films as Drivers Wanted and Fat Pizza: The Movie, as well as Pizza: The Movie. Pizza delivery has served as major plot element of such films as Loverboy. The 2011 American comedy film 30 Minutes or Less is concerned about the kidnapping of a pizza-delivery driver and his subsequent coercive involvement in a bank robbery, which was loosely based on the 2003 death of Erie, Pennsylvania pizza deliveryman Brian Douglas Wells. In television, the Australian comedy series Pizza centres on Pauly and his co-workers who deliver pizzas for a Sydney-based pizzeria called Fat Pizza.


== See alsoEdit ==
List of pizza chains


== ReferencesEdit ==


== External linksEdit ==
 Media related to Pizza delivery at Wikimedia Commons