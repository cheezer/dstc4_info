John Dempsey may refer to:


== Sports ==
John Dempsey (footballer, born 1946), Republic of Ireland international football (soccer) player
John Dempsey (footballer, born 1951), Tranmere Rovers player


== Politicians ==
John J. Dempsey (1879–1958), American politician, governor of New Mexico
John N. Dempsey (1915–1989), American politician, governor of Connecticut


== Others ==
John Dempsey (lyricist), also playwright
John Dempsey (Medal of Honor) (fl. 1848–1875), American sailor and Medal of Honor recipient
John Dempsey (cartoonist), American single-panel cartoonist


== See also ==
Jack Dempsey (disambiguation)
John Dempsey Hoblitzell, American politician