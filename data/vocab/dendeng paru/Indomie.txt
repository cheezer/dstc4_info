Indomie is a brand of instant noodle by Indofood, the largest instant noodle manufacturer in Indonesia with 72% of market share. It is distributed in Australia, Asia, Africa, New Zealand, the United States and European and Middle Eastern countries. Outside its main manufacturing plants in Indonesia, Indomie is also produced in Nigeria since 1995 where it is a popular brand and has the largest instant noodle manufacturing plant in Africa.


== History ==
Instant noodles were first introduced into the Indonesian market in 1969. Indofood is one of Indonesia's largest pre-packaged food companies, and was founded in 1982 by Sudono Salim, a Chinese Indonesian tycoon that also owned Bogasari flour mills. Indomie instant noodle brand was first launched in 1982 with Indomie Kuah Rasa Kaldu Ayam (Chicken stock) flavour, followed by Indomie Kuah Rasa Kari Ayam (Chicken curry) flavour. In 1983 Indomie launched its first dry (served without soup) Indomie Mi Goreng variant, which quickly become popular in Indonesian market. Indomie has become a household name for instant noodle in Indonesia, and accounted for around 70 percent of instant noodle market shares in Indonesia. Indomie has won several awards, among others are Indonesia Best Brand Award (IBBA), The Most Effective Ad, Indonesia Consumer Satisfaction Award (ICSA), and Indonesia Best Packaging Award.


== Description ==
Indomie is a form of instant noodle spiced for the Indonesian palate. The most popular flavour is Indomie Mi goreng, followed by Indomie Soto Mie (Noodle Soto flavour), Indomie Bawang (Onion Chicken), Indomie Kari Ayam (Chicken Curry), Indomie Kaldu Ayam (Chicken stock) and Indomie Ayam Spesial (Special Chicken).
Indomie claims to be made from high quality flour supplied by Bogasari flour mills, meeting international standards, and fortified with vitamin A, B1, B6, B12, niacin, folic acid and iron. Indomie is certified as halal by Majelis Ulama Indonesia, certified ISO 9001:2001 and HACCP from SGS.
However, just like many instant noodles, Indomie contains high levels of carbohydrate, sodium and fat (in the oil seasoning). Without additional ingredients such as eggs, meat and vegetables, its nutrient value is poor, with low protein and dietary fibre. It also contains phenylalanine and Monosodium glutamate (MSG : listed as flavour enhancer 621)


== Naming and slogan ==
"Indo" stands for "Indonesia" and "mie" stands for "noodles" in the Indonesian language, therefore "Indomie" stands for "noodles from Indonesia or Indonesian noodle". Both Indomie and IndoMie can be used to describe the brand. Several advertisements in Indonesia show a variety of customers eat Indomie, usually ended by the chorus "Indomie, Seleraku", "Majulah Indonesiaku" (Indonesia Independence Day only), and "Maafkan Lahir Batin" (Eid Mubarak only).


== Varieties ==
Indomie noodles comes in a variety of brands and flavours. Currently, there are around 38 flavours of Indomie instant noodle variants available in the market, with some variants production has been discontinued. Indomie brand is divided into 7 product categories; Indomie goreng (fried noodle served without soup), Indomie kuah (with soup), Indomie Jumbo (large), Selera Nusantara and Kuliner Indonesia (Indonesian cuisine), Mi Keriting (premium curly noodle),and "Taste of Asia".
Indomie Mi goreng is the instant version of Indonesian mie goreng (fried noodle); flavours include Mi goreng Sate and Mi goreng Rasa Ayam (Chicken). Indomie kuah refer to soupy variants of Indomie; flavours include Rasa Ayam Spesial (Special Chicken Flavour), and Rasa Baso Sapi (Beef Meatball Flavour).
Indomie Jumbo is larger variant with net weight 127-129 gram, flavours include Mi goreng Spesial Jumbo (Special Fried Noodle Flavour), and Mi Goreng Jumbo Rasa Ayam Panggang (Grilled Chicken Fried Noodle Flavour). Indomie Selera Nusantara refer to Indonesian traditional cuisine variants; flavours include Mi Rasa Soto Banjar (Banjar Soto Flavour) from South Kalimantan,Mi Rasa Cakalang(Skipjack tuna Flavour) from North Sulawesi,Mi Rasa Dendeng Balado(Dendeng Balado Flavour) from West Sumatera, and Mi Rasa Soto Lamongan(Lamongan Soto flavour) from East Java. Mi Keriting, which means "curly noodle", is the premium variant with additional toppings; flavours include Mi Keriting Goreng Spesial (Special Curly Fried Noodle Flavour) and Mi Keriting Rasa Ayam Panggang (Special Curly Fried Noodle with Grilled Chicken Flavour).
"The Taste of Asia" range includes flavours such as Korean Bulgogi Flavour Fried Noodles and Thai Tom Yum Flavour Noodles Soup. The Kuliner Indonesia range includes Mi Goreng Rasa Dendeng Balado (Spicy beef) (since 2014) and Rasa Soto Lamongan (Lamongan Soto Flavour) (since 2014). Crunchy variants include Mi goreng Kriuuk Ayam and Mi goreng Kriuuk Bawang. Vegan variants include Mi Goreng Vegan (Vegetarian Fried Noodles) and Rasa Sup Sayuran Vegan (Vegetables Flavour).


=== Mi goreng ===

The "Mi goreng" ("stir fry") range of instant noodles by Indomie entered the market in 1983 and are distributed in North America, Europe, Africa, Australasia and various regions in Asia. The brand name "Mi goreng" is Indonesian for "fried noodle", and is based on the Indonesian dish Mie goreng. The brand flavours are sold in varying weight packets, around 85g, and contain two sachets of flavourings. The first sachet has three segments and carries the liquid condiments: sweet soy sauce, chilli sauce, and seasoning oil with garlic flakes. The other sachet has two segments for dry seasoning powder and flake of fried scallion. In some regions, Indomie Mi goreng is also available in jumbo (120 gram) packs.
In 2006, Indomie launched a new variant Indomie Mi Goreng Kriuuk.. 8x. In this product, 8x means "Lebih banyak, Lebih renyah, Lebih gurih, Lebih gede" (translated: more noodles, crunchier, more delicious, larger), in three flavours: Chicken, Onion, Spicy. Indomie Mi Goreng is certified halal.
Indomie Mi Goreng contains phenylalanine, so individuals suffering from phenylketonuria should avoid the seasoning. Indomie Mi Goreng also contains Monosodium glutamate (MSG : listed as flavour enhancer 621), making it unsuitable for some individuals with "severe, poorly controlled asthma".
Additionally flavour enhancer 631 can be prepared from meat extract and/or dried sardines, but it can also be created via a microbial synthesis process from a vegetable source, and flavour enhancer 627 is isolated from sardines and/or yeast extract. The inclusion of flavour enhancer 631 and the way it was prepared means Indomie Mi Goreng may or may not be suitable for vegetarians and vegans.


=== Pop Mie ===
Indomie cup noodle variety is called Pop Mie (Indomie Cup in Egypt, Syria and Saudi Arabia; Handy Indomie in the Virgin Islands); it comes in a variety flavours including Mi goreng Spesial (Special) and Mi goreng Pedas (Spicy).


== Popularity ==
On September 9, 1970, Indomie was first announced to the market in Indonesia then launched two years later, in 1972.
In 1988 Indomie was introduced in Nigeria through import, and in 1995 open its first production factory in Nigeria under Dufil Prima Foods. It is the first instant noodles manufacturing plant in Nigeria and the largest in Africa. Indomie Instant Noodles has grown to become a household name across the country.
In 2005, Indomie broke the Guinness Book of World Records category for “The Largest Packet of Instant Noodles”, creating a packet that was 3.4m x 2.355m x 0.47m, with a net weight of 664.938 kg, which is about 8,000 times the weight of a regular pack of instant noodles. It was made using the same ingredients as a regular pack of instant noodle and was certified fit for human consumption.
On December 13, 2009, Roger Ebert, popular film critic from United States for Chicago Sun-Times, ranked Indomie in one of his "Twelve Gifts of Christmas" in position #1.
On January 3, 2010, Indomie launched the new pack design from its company.
On October 7, 2010, In Taipei, The Taipei County Public Health Bureau announced that cosmetic preservatives were found in the Indonesian instant noodle “Indomie” and ordered all vendors to withdraw the product from the market.
On October 11, Official Statement released by Indofood regarding the Taiwan Incident is The "The Company believes that the recent reports in the Taiwanese media arose in relation to instant noodle products manufactured by ICBP that were not intended for the Taiwanese market."
Since December 6, 2010 Taiwan authorities have allowed Indomie instant noodle products to re-enter Taiwan market.
May 2011, Jesse Two Ocean (J2O) from London, United Kingdom performing "Indomie" Produced by Acen.
March 26, 2013, Indomie Pepper Chicken Flavour makes its official launch.
In November 2013, Indofood launched the Taste of Asia flavours with Tom Yum, Bulgogi and Laksa flavours.
January 1, 2014, Indomie launched a new flavour called Iga Penyet (Spareribs).
September 2014, Indomie launched two new flavours called Dendeng Balado (Spicy dendeng beef) and Soto Lamongan (Lamongan Soto). Its TV commercial featured the song by Two Door Cinema Club, "What You Know".


== References ==


== External links ==
Indomie - Product website
Indofood - Mi Goreng Manufacturer