Deposit may refer to:
Deposit (finance)
Deposit (town), New York
Deposit (village), New York
Deposit account, a bank account that allows money to be deposited and withdrawn by the account holder
Demand deposit, the funds held in demand deposit accounts in commercial banks

Damage deposit, a sum of money paid in relation to a rented item or property to ensure it is returned in good condition
Container deposit, a deposit on a beverage container paid when purchased and refunded when returned
Deposit (politics), a sum that a candidate must pay in return for the right to stand in an election
Deposit (geology), material added to a landform


== See also ==
Deposit formation or fouling, the accumulation of unwanted material on solid surfaces
Deposit model, a method of identifying the character and degree of survival of buried archaeological remains
Deposit of faith or Fidei depositum, the Apostolic constitution by which Pope John Paul II ordered the publication of the Catechism of the Catholic Church
Deposition (disambiguation)