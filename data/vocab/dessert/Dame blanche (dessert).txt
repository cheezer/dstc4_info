Dame blanche (French, "white lady") is the name used in Belgium and the Netherlands for a sweet dessert consisting of vanilla ice cream with whipped cream, and warm molten chocolate. In Germany, the same type of dessert is known as a Coupe Dänemark. The dessert is similar to the American sundae.


== References ==