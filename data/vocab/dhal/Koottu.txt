Kootu (Tamil:கூட்டு) is a Tamil word means 'add' i.e. vegetable added with lentils which form the dish, made of vegetable and lentils and are semi-solid in consistency, i.e., less aqueous than sambhar, but more so than dry curries. Virundhu Sappadu (Typical Tamil feast) comes with the combo of boiled rice ('Choru' in Tamil), sambar, rasam, curd, poriyal, kootu, appalam, pickle and banana. All kootus by default have some vegetables and lentils, but many variations of kootu exist:
Poricha Kootu: A kootu made with urid dhal and pepper is called poricha (means 'fried' in Tamil) kootu. Fried urad dhal, pepper, few red chilies, some cummin and fresh coconut are ground together. The moong dhal and the cut vegetables are cooked separately. Then, the ground paste, cooked vegetable and moong dhal are mixed and heated. Vegetables such as beans and snake gourd are common ingredients in this kootu.
Araichivita Kootu: A kootu which has a ground (freshly powdered) masala in it; the word araichivita in Tamil literally translates to 'the one which has been ground and poured.' The ground paste is a mixture of fried urid dhal, cummin seeds and coconut.
Araichivita sambar: The chopped vegetables and the toor dhal are cooked separately. Then, the ground paste, cooked vegetable and dhal are heated together. Then add the ground paste of coconut, bengal gram, dhaniya, red chilies, few pepper corns, (optionally, a piece of cinnamon) - roasted and ground; season mustard seeds and methi in oin, add the vegetables, including Madras onions, saute and then add water. Then add tamarind extract, and then the ground paste and boiled dal. Served with rice.
Many other regional variations exist.


== See also ==
List of stews


== References ==


== External links ==
"White Pumpkin Kootu". Retrieved 15 March 2013.