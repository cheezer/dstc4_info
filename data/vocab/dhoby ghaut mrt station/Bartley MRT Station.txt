Bartley MRT Station (CC12) is an underground Mass Rapid Transit station on the Circle Line in Singapore. The station is situated along Bartley Road outside Maris Stella High School. There are two exit and entry points for this station which was constructed under Stage 3 of the Circle Line.


== History ==
This station is being built in tandem with the Outer Ring Road System project which consists of extending Bartley Road to Eunos Road via a viaduct over the part of the underground site of the Kim Chuan Depot. As part of the project, Upper Paya Lebar Road was rebuilt and opened on 17 January 2010 as an underpass heading towards MacPherson and Paya Lebar. The station opened first on 28 May 2009 along with the rest of Stage 3 of the Circle Line due to the result of Nicoll Highway collapse on 20 April 2004.
Before the station opened, Singapore Civil Defence Force conducted the third Shelter Open House at this station on 4 April 2009, together with Bishan and Lorong Chuan stations. It also held an open house for the SMRT staff on 1 May. This station was a terminal for the Circle Line until the opening of the Dhoby Ghaut wing of the Circle Line on 17 April.


== Art in Transit ==
The art piece at this station, which called The Coin Mat, consists of a mural consisting of 160,000 one-cent coins embedded in glass, done by Jane Lee.


== Station layout ==


== Exits ==
A: Bartley Road, Maris Stella High (Pri), Maris Stella High Sch, Society for the Prevention of Cruelty to Animals (SPCA), Bartley Sec Sch, Fragrant Gardens
B: Bartley Residences


== Transport connections ==


=== Rail ===


== References ==


== External links ==
Official website