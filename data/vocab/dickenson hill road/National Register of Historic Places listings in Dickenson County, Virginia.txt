This list includes properties and districts listed on the National Register of Historic Places in Dickenson County, Virginia. Click the "Map of all coordinates" link to the right to view a Google map of all properties and districts with latitude and longitude coordinates in the table below.

This National Park Service list is complete through NPS recent listings posted August 7, 2015.


== See also ==
National Register of Historic Places listings in Virginia
List of National Historic Landmarks in Virginia


== References ==