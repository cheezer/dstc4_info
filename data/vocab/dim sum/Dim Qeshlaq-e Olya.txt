Dim Qeshlaq-e Olya (Persian: ديم قشلاق عليا‎, also Romanized as Dīm Qeshlāq-e ‘Olyā; also known as Dem, Dem Qeshlāq, and Dīm Qeshlāq) is a village in Chaybasar-e Shomali Rural District, Bazargan District, Maku County, West Azerbaijan Province, Iran. At the 2006 census, its population was 1,056, in 196 families.


== References ==