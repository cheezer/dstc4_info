The Double Diamond Dude Ranch Dining Hall was built in 1945 as the centerpiece of a dude ranch operated by Frank Williams and Joseph S. Clark, Jr. in Grand Teton National Park. The ranch was opened in 1924 with a dozen tent cabins and log buildings for a kitchen and dining hall, lounge and commissary. In 1943 Williams built log tourist cabins, followed by the larger dining hall in 1945. The 1985 Taggart Lake Fire destroyed much of the ranch, sparing only the dining hall and five cabins. The dining hall is listed on the National Register of Historic Places as an example of rustic architecture. Since 1970 the Double Diamond property has been a hostel for mountain climbers in the Teton Range, and is known as the Climbers' Ranch.


== History ==
Frank Williams, originally from Colorado, was a local guide and former Bar B C Dude Ranch wrangler, who partnered with Clark, a young eastern dude from a wealthy Philadelphia family. Clark provided financial backing to open the Double Diamond in 1924 on a homestead claim that had been filed by Frank Williams' wife Emma. Clark, who had been to the Bar B C and who had met Williams at the Bar None ranch, later became mayor of Philadelphia and was a U.S. senator from 1957 to 1969. The ranch expanded from 14 acres (5.7 ha) to 54 acres (22 ha) in 1926 with the purchase of a portion of the neighboring Manges property. Catering primarily to boys from eastern families, accommodations consisted of tents, with a small dining hall, lounge and commissary. The ranch drew its clientele from the Philadelphia area, charging $800, including transportation from Philadelphia, for a June 15 - September 15 season. References were required for prospective dudes. The ranch had an initial capacity of 25 dudes, expanded in the 1930s to 35. Clark, who was launching a legal and political career, became less involved in the 1930s, finally selling his share to Harry and Nola Williams Brown in 1946, but buying back an acre for a summer cabin.
Cabins were added in 1943 with a shift in emphasis to families. The main dining hall was built in 1945. Harry Brown and Nola Williams Brown, took half of the ownership in 1948. The Williams family continued to operate the Double Diamond until Frank's death in 1964, when the National Park Service bought the ranch for $315,000, retaining a lease to continue operations through 1969.


== Description ==
The1945 dining hall is a one story U-shaped log building. It features a large stone fireplace and chimney on the north side. The former dining room is now a library and lounge, while the original commercial kitchen is a communal kitchen and dining area. The exterior is stained a uniform brown. Following local custom, the log chinking is covered with 1/4 pole strips.
The five surviving original cabins (of 15 extant before the fire) are not part of the historic designation. New and relocated cabins have replaced those lost, in locations approximating the missing cabins. Two cabins, designed by Ogden, Utah architect Eber Piers, were moved from the Hunter Hereford Ranch, confusing the local historical context.
The complex, including the dining hall, has been leased to the American Alpine Club since 1970. Renamed the Climbers' Ranch, the facility is a hostel for mountain climbers. The cabins are used for dormitory-style climber accommodations, while the dining hall is a shared facility.
The Double Diamond dining hall was placed on the National Register of Historic Places on August 18, 1998.


== See also ==
Historical buildings and structures of Grand Teton National Park


== References ==


== External links ==
Auto Camps at Grand Teton National Park
Double Diamond Dude Ranch Dining Hall at the Wyoming State Historic Preservation Office
Grand Teton Climbers' Ranch at the American Alpine Club
Friends of Grand Teton Climbers' Ranch