Dinner usually refers to the most significant and important meal of the day, which can be the noon or the evening meal. However, the term "dinner" can have many different meanings depending on the culture; it may mean a meal of any size eaten at any time of day. Historically, it referred to the first meal of the day, eaten around noon, and is still sometimes used for a noontime meal, particularly if it is a large or main meal. The meaning as the evening meal, generally the largest of the day, is becoming standard in many parts of the English-speaking world.


== Etymology ==
The word is from the Old French (c. 1300) disner, meaning "breakfast", from the stem of Gallo-Romance desjunare ("to break one's fast"), from Latin dis- ("undo") + Late Latin ieiunare ("to fast"), from Latin ieiunus ("fasting, hungry"). The Romanian word "dejun" and the French "déjeuner" retain this etymology and to some extent the meaning (whereas the Spanish word "desayuno" and Portuguese "desjejum" are related but are exclusively used for breakfast). Eventually, the term shifted to referring to the heavy main meal of the day, even if it had been preceded by a breakfast meal (or even both breakfast and lunch).


== History ==
In Europe, the fashionable hour for dinner began to be incrementally postponed during the 18th century, to two and three in the afternoon, until at the time of the First French Empire an English traveler to Paris remarked upon the "abominable habit of dining as late as seven in the evening".


== Time of day ==
In many modern usages, the term dinner refers to the evening meal, which is now often the most significant meal of the day in English-speaking cultures. When this meaning is used, the preceding meals are usually referred to as breakfast, lunch and tea. In some areas, the tradition of using dinner to mean the most important meal of the day regardless of time of day leads to a variable name for meals depending on the combination of their size and the time of day, while in others meal names are fixed based on the time they are consumed.
The divide between different meanings of "dinner" is not cut-and-dried based on either geography or socioeconomic class. However, the use of the term dinner for the midday meal is strongest among working-class people, especially in the English Midlands, North of England and the central belt of Scotland. Even in systems in which dinner is the meal usually eaten at the end of the day, an individual dinner may still refer to a main or more sophisticated meal at any time in the day, such as a banquet, feast, or a special meal eaten on a Sunday or holiday, such as Christmas dinner or Thanksgiving dinner. At such a dinner the people who dine together may be formally dressed and consume food with an array of utensils. These dinners are often divided into three or more courses. Appetizers consisting of options such as soup, salad etc., precede the main course, which is followed by the dessert.
A casual poll conducted by Jacob's Creek, an Australian winemaker, found the average evening meal time in the U.K. to be 7:47pm, but does not state (other than in the reporter own title) if the people surveyed call their evening meal dinner or tea.


== Dinner parties ==

A dinner party is a social gathering at which people congregate to eat dinner.


=== Ancient Rome ===
During the times of Ancient Rome, a dinner party was referred to as a convivia, and was a significant event for Roman emperors and senators to congregate and discuss their relations. The Romans often ate and were also very fond of fish sauce called liquamen (also known as Garum) during said parties.


=== England ===
In greater London, England (c. 1875–c. 1900), dinner parties were sometimes formal occasions that included printed invitations and formal RSVPs.  The food served at these parties ranged from large, extravagant food displays and several meal courses to more simple fare and food service. Activities sometimes included singing and poetry reciting, among others.


== See also ==


== References ==


== Bibliography ==


== Further reading ==


== External links ==
"Dinner" definition from Cambridge.org
Wikibooks Cookbook
BBC article on history of dinner