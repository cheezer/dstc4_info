Doctor of Divinity (D.D. or DD, Divinitatis Doctor in Latin) is an advanced or honorary academic degree in divinity.


== Contrast with other religious degrees ==
Doctor of Divinity is not to be confused with Doctor of Theology (Th. D.) which is a research doctorate, in theology, awarded by universities and divinity schools such as Harvard Divinity School and many others. Many universities award a Ph.D. rather than a Th.D. to graduates of higher-level religious studies programs. Doctor of Sacred Theology is a research doctorate in theology, but particular to Catholic Pontifical Universities and Faculties. Doctor of Ministry is another doctorate-level religious degree, but is a professional rather than a research doctorate. 


== Doctor of Divinity by country or church ==


=== United Kingdom and Ireland ===
In the United Kingdom, Doctor of Divinity has traditionally been the highest religious doctorate granted by universities, usually conferred upon a religious scholar of standing and distinction. It is a higher doctorate generally awarded for accomplishments beyond the Ph.D level.
The Doctor of Divinity degree is awarded in recognition of a substantial body of original research undertaken over the course of many years. Typically the candidate will submit a collection of work which has been previously published in a peer-reviewed context and pay an examination fee. The university then assembles a committee of academics both internal and external who review the work submitted and decide on whether the candidate deserves the doctorate based on the submission. Most universities restrict candidacy to graduates or academic staff of several years' standing.


=== United States ===
In the United States, Doctor of Divinity is traditionally an honorary degree granted by a church-related college, seminary, or university to recognize the recipient's ministry-orientated accomplishments.
As most American universities do not confer higher doctorates, the degree is generally conferred honoris causa. For example, Martin Luther King (who received a Ph.D in systematic theology from Boston University in 1955) subsequently received honorary Doctor of Divinity degrees from the Chicago Theological Seminary (1957), Boston University (1959), Wesleyan College (1964), and Springfield College (1964). Billy Graham (who has received honorary Doctor of Divinity degrees from The King's College and the University of North Carolina at Chapel Hill) is regularly addressed as "Dr. Graham", though his highest earned degree is a B.A. in anthropology from Wheaton College. Under federal law, a 1974 judgement accepted expert opinion that an "Honorary Doctor of Divinity is a strictly religious title with no academic standing. Such titles may be issued by bona fide churches and religious denominations, such as plaintiff (Universal Life Church), so long as their issuance is limited to a course of instruction in the principles of the church or religious denomination". Similarly, under the California Education Code, "an institution owned, controlled, and operated and maintained by a religious organization lawfully operating as a nonprofit religious corporation pursuant to Part 4 (commencing with Section 9110) of Division 2 of Title 1 of the Corporations Code" that offers "instruction... limited to the principles of that religious organization, or to courses offered pursuant to Section 2789 of Business and Professions Code" may confer "degrees and diplomas only in the beliefs and practices of the church, religious denomination, or religious organization" so long as "the diploma or degree is limited to evidence of completion of that education"; institutions "shall not award degrees in any area of physical science", while "any degree or diploma granted under this subdivision shall contain on its face... a reference to the theological or religious aspect of the degree's subject area... a degree awarded under this subdivision shall reflect the nature of the degree title, such as 'associate of religious studies,' 'bachelor of religious studies,' 'master of divinity,' or 'doctor of divinity.'" In a 1976 interview with Morley Safer of 60 Minutes, Universal Life Church founder Rev. Kirby J. Hensley professed that the Church's honorary Doctor of Divinity degree was "...just a little piece of paper. And it ain't worth anything, you know, under God's mighty green Earth—you know what I mean?—as far as value." In 2006, Universal Life Church minister Kevin Andrews advised potential degree recipients not to misrepresent the title as an educational achievement to employers, recommending instead that it would be appropriate to list such credentials "under the heading of Titles, Awards, or Other Achievements" on curricula vitae.


=== Catholic Church ===
In the Catholic Church, Doctor of Divinity is an honorary degree denoting ordination as bishop.


== The Doctor and Student ==
Christopher St. Germain's 1528 book The Doctor and Student describes a dialogue between a Doctor of Divinity and a law student in England containing the grounds of those laws together with questions and cases concerning the equity thereof. 


== See also ==


== References ==


== External links ==
The Doctor and Student pdf files