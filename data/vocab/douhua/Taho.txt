Taho (Tagalog: [tɐˈhoʔ]) (Chinese: 豆花; Pe̍h-ōe-jī: tāu-hoe) is a Philippine snack food made of fresh soft/silken tofu, arnibal (sweetener and flavoring), and sago pearl (similar to tapioca pearls). This staple comfort food is a signature sweet and taho peddlers can be found all over the country. The Indonesian and Malaysian equivalent of this snack is tauhue.


== History ==
Through early records, it is evident that taho traces its origin to the Chinese douhua. Prior to the Spanish Colonization, Chinese were common traders with the native Malays, influencing Philippine cuisine.


== Processing and preparation ==
Most taho vendors prepare the separate ingredients before dawn. The main ingredient, fresh soft/silken tofu, is processed to a consistency that is very similar to a very fine custard. The brown sugar is heated, caramelized and mixed with water to create a viscous amber-colored syrup called arnibal. Flavors, like vanilla are sometimes added to the arnibal. Sago pearls, purchased from the local market, are boiled to a gummy consistency until they are a translucent white.


== Marketing ==
The Magtataho (taho vendor) is a common sight in the Philippine streets. A magtataho carries two large aluminum buckets that hang from each end of a yoke. The larger bucket carries the tofu base; the smaller bucket holds the arnibal and sago pearls.
Taho vendors peddle their product in a distinctive manner, calling its name in a full, rising inflection as they walk at a leisurely pace on the sidewalk or along the side of the road. Most magtataho keep a habitual route and schedule, calling out "Tahoooooo!" to attract a customer's attention. Though vendors are most likely to ply their routes early in the morning, it is not uncommon for a magtataho to be spotted in the late afternoon or evening as well.
Most magtataho carry plastic cups, often in two sizes, and spoons for their product. Some customers in residential areas tend to use their own cups, and the vendors price their product accordingly. Using a wide, shallow metal watch glass-shaped scoop, they skim the surface of the bean curd and toss out any excess water, before scooping the bean curd itself into a cup. Then, using a long, thin metal ladle, they scoop sago or tapioca pearls and arnibal into the cup, loosely mixing it in.


== Eating ==
Taho is enjoyed either with a spoon, sipping it with a straw, or by simply slurping it straight from the cup. Though traditionally served warm, cold varieties exist in supermarkets and food stalls in cafeterias with bean curd in a solid, unbroken state. These pre-packed cups, sold with a plastic spoon or wooden popsicle stick, tend to contain a firmer tofu.


== Varieties ==
In Baguio, there is also a strawberry variety of taho, wherein strawberry syrup is used instead of arnibal. Other varieties are chocolate and buko pandan flavor.


== See also ==
Cuisine of the Philippines
Filipino Chinese cuisine
Tofu
Douhua
List of tofu dishes
 Food portal


== References ==