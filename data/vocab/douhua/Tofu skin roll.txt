Tofu skin roll or Tofu roll is a dim sum dish. It can be found in Hong Kong and among overseas Chinese restaurants. It is usually served in a small plate in twos or threes. In all cases, the outer layer is made of tofu skin.


== VarietyEdit ==
There are a number of cooking styles. The fillings range from pork with vegetable, to fish or beef.


=== FriedEdit ===

The fried version is known as (腐皮捲, fu pei gyun). The first character "fu" comes from tofu, though a more accurate description is that the skin is made from the ingredient bean curd. Some Cantonese restaurants serve the fried crispy version at night, often with mayonnaise as dipping sauce. Another name is the (豆腐捲, tofu gyun). Some ingredients include shrimp, leek, chicken, bamboo shoot, small carrots, tofu, scallions, sesame oil, bean sprouts.


=== SteamedEdit ===
The bamboo steamed version is generally known as (鮮竹捲, sin zuk gyun). It is wrapped with dried tofu skin (腐竹, fu zhu). During the cooking process, the tofu skin is hydrated. It makes the roll very soft and tender. This is the version most commonly served as a dim sum dish during yum cha sessions. The steamed tofu skin rolls often contain bamboo shoots.


== See alsoEdit ==

Dim sum
List of tofu dishes
How to make fried tofu skin rolls http://www.souschef.co.uk/bureau-of-taste/recipe-beancurd-skin-rolls/


== ReferencesEdit ==