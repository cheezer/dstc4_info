Dover is a local government district in Kent, England. Dover is its administrative centre. It was formed on 1 April 1974 by the merger of the boroughs of Deal, Dover, and Sandwich along with Dover Rural District and most of Eastry Rural District. There are three towns within the district: Dover, Deal and Sandwich; and the parishes below:
The northern boundary of the district is the River Stour; on its western side is the district of Canterbury; to the south the parish of Capel-le-Ferne; and to the east the Straits of Dover. The southern part of the latter is the point where the North Downs meets the sea, at the so-called ‘White cliffs of Dover’. Further north along the coast, from Deal onwards, the land is at sea level, where the River Stour enters the sea by a circuitous route. It is here, on the sand-dunes, that the Royal St George's Golf Club, founded in 1887, and of international repute, is situated.
In the district are industrial remains of the erstwhile Kent coalfield, situated around Tilmanstone and Betteshanger. Technically speaking, half of the underwater section of the Channel Tunnel is under British Sovereignty and thus part of the district.


== Communications ==
Deal Timeball is a Victorian maritime Greenwich Mean Time signal located on the roof of a waterfront four-storey tower. It was established in 1855 by the Astronomer Royal George Biddell Airy in collaboration with Charles V. Walker.
Crossing Dover district are the Roman roads of Watling Street and that leading from Richborough. Today the main road, the A2, closely follows Watling Street to Dover.


== See also ==
Dover District local elections


== External links ==
Parish listing
Deal High Street winner of the Telegraph's first High Street of the Year award
Kent coalfield history
Dover Soul Local Website for Dover & White Cliffs Country: history, geography, Kent's radio history, pictures and community information
Dover pictorial website
Dover Community Radio (DCR)