Dover, Kent has had numerous railway stations due to the legacy of competition between the South Eastern Railway (SER) and London, Chatham and Dover Railway (LCDR)

and the subsequent rationalisation attempts by their successors; South Eastern and Chatham Railway (SECR), Southern Railway and British Rail Southern Region.


== Current station ==

Opened in 22 July 1861 as Dover Town (Priory) by the LCDR, Dover Priory railway station became a through station on 1 November the same year, upon completion of a tunnel though the Western Heights connecting it to LCDR's new Dover Harbour Station in the Western Docks area. The renaming in July 1863 as Dover Priory led rival SER to adopt the name "Dover Town" for one of its Dover stations. Dover Priory is the only station still open in Dover.


== Former stations ==
Besides Dover Priory, there have been five other stations in Dover, all of which are now closed.


=== Dover Harbour ===

The name Dover Harbour has been used for two separate stations. The first was opened by the LCDR on 1 November 1861; this was closed in June 1863 when it was replaced by a new station named Dover Town and Harbour. This was renamed Dover Harbour on 1 July 1899 and closed on 10 July 1927. The LCDR constructed a tunnel through the Western Heights to allow continental traffic to access the Western Docks in 1861, and just south of the tunnel, on the current track to Folkestone, a station was built. Later an extension was also built onto Admiralty Pier. It was closed in 1927 by Southern as part of their rationalisation and modernisation of the Dover area.


=== Dover Town ===
The SER's original station opened on 7 February 1844 as Dover, and was situated on Shakespeare Beach just east of where the current line from Folkestone turns north towards Dover Priory. This was originally a terminus, but later the line continued onto Admiralty Pier. It was renamed Dover Town in December 1861, and was closed on 14 October 1914 as part of rationalisation and modernisation of the Dover area by SECR.


=== Admiralty Pier ===
In 1860 SER started running trains on to the pier, the LCDR following in 1864. This exposed halt was extended and replaced by Dover Marine in 1909. The SER station at Dover Admiralty Pier opened on 30 August 1864 and closed in August 1914.


=== Dover Prince of Wales Pier ===
This station was opened in 1902 and closed in 1909.


=== Dover Marine/Dover Western Docks ===
Situated on Admiralty Pier for connection to ships, this was constructed on an expanded pier by SECR, finished in 1914, began to be used on 2 February 1915 but was not available for public use until 18 January 1919; in the meantime it had been renamed Dover Marine on 5 December 1918. It was a large terminus with four platforms covered by a full roof. It was renamed again to Dover Western Docks on 14 May 1979, and was closed by British Rail on 26 September 1994 with the demise of boat trains and the opening of the Channel Tunnel. It is now a cruise liner terminal.
Regie voor Maritiem Transport used to run ferries until 1994 from here to Oostende railway station which connected into Belgian railway line 50A run by NMBS. There was a fast ferry service using the Jetfoil as well as conventional ferries.
The Southern Railway opened a large locomotive depot at the site in 1928. This was closed in 1961 and demolished.


== References ==


== External links ==
Subterranea Britannica's page on Admiralty Pier
Subterranea Britannica's page on Dover Harbour
Subterranea Britannica's page on Dover Western Docks