Downtown Trenton is an unincorporated community and neighborhood located within the city of Trenton in Mercer County, New Jersey, United States. It encompasses several distinct districts, including the Capital District, Mill Hill, the Downtown Transit Village/Train Station, the Riverfront District, the Central Business District and Hanover/Academy. Business development in Downtown Trenton is supported by the Trenton Downtown Association.


== References ==