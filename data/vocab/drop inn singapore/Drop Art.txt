In 1961, filmmaker Gene Bernofsky and artist Clark Richert, art students from the University of Kansas, developed an art concept they called Drop Art or droppings. Informed by the "happenings" of Allan Kaprow and the impromptu performances a few years earlier of John Cage, Robert Rauschenberg and Buckminster Fuller at Black Mountain College, Drop Art began when Richert and Bernofsky started painting rocks and dropping them from a loft roof onto the sidewalk of Lawrence Kansas's main drag — watching the reactions of passersby. Early Drop Art included such pieces as Egg Drop and Pendulum (pictured) .
Drop Art eventually led to the creation of Drop City, an experimental artist's community founded in 1965 near Trinidad, Colorado. The intention was to create a live-in work of Drop Art.


== External links ==
Ruins of Drop City, Trinidad, Colorado a print by Joel Sternfield
Clark Richert Website Clark Richert Website


== References ==
Peter Rabbit. Drop City. 
Tim Miller. "The Farm, Roots of Communal Revival 1962–1966". 
Paul Hildebrandt. Zome-inspired Sculpture (PDF).