Typhoon Durian has been used to name two tropical cyclones to name in the northwestern Pacific Ocean. The name was submitted by Thailand, and refers to a Southeast Asian fruit of the same name.
2001's Severe Tropical Storm Durian (T0103, 05W), made landfall in the People's Republic of China killing 78.
2006's Typhoon Durian (T0621, 24W, Reming), killed 1,497 people in the Philippines and Vietnam.
The name Durian has been retired by the WMO in 2006. The name was replaced by Mangkhut.
2013's Tropical Storm Mangkhut (T1310, 10W, Kiko)