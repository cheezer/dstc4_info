Craig Road (Chinese: 克力路) is a road located in Tanjong Pagar within the Outram Planning Area in Singapore. The road links Neil Road and Tanjong Pagar Road, and is also accessible via Duxton Road.


== Etymology and history ==
Craig Road was named after Captain James Craig, a member of the Merchant Service Guild and an officer of the Freemason's Zetland Lodge, a club. The road was the living quarters of Chinatown's poor in the late 19th and early 20th centuries.
The road is known as gu chia chui kia in Hokkien, which means "side of Kreta Ayer".


== References ==
Victor R Savage, Brenda S A Yeoh (2004), Toponymics - A Study of Singapore Street Names, Eastern University Press, ISBN 981-210-364-3