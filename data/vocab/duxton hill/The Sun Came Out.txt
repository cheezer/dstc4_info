The Sun Came Out is a charity studio album released on 31 August 2009 by 7 Worlds Collide, a musical project of New Zealand singer/songwriter Neil Finn for the benefit of Oxfam. The album was recorded at Finn's Roundhead Studios and is a follow up to the 7 Worlds Collide live album of 2001. Contributors to the project include 7 Worlds Collide's 2001 alumni Johnny Marr, Ed O'Brien, Sebastian Steinberg, Phil Selway, Lisa Germano, Tim Finn and Liam Finn; as well as Don McGlashan, Bic Runga, Glenn Richards, KT Tunstall and Wilco members Jeff Tweedy, John Stirratt, Glenn Kotche and Pat Sansone.
The album was produced by Jim Scott and Neil Finn with additional production by Neil Baldock and is available as a single CD or as a double CD edition.


== Wilco's involvement ==
According to Wilco bassist John Stirratt, in an interview with NZPA:

Neil Finn came to us backstage when we were playing in Chicago in 2008 and he had just had the idea of doing another 7 Worlds Collide and invited us soon after that. I think he got introduced to [us] through his kids and ... he thought we would be a good fit.
It was wonderful, Neil was amazing; there are not many guys who can oversee a project like that. First of all he owns a studio that has three functioning rooms in it with all the stuff. Not many people could have done that, and Neil is one and maybe (Paul) McCartney would be the other guy who could maybe do something like that.
We had sketched [the album] out very well [before arriving in New Zealand]...but obviously the fact that we did do it so fast is probably representative of New Zealand because we were all really relaxed and were having a lot of fun.


== Track listing ==


=== Single CD ===


=== Double CD ===


==== Disc one ====


==== Disc two ====


=== Personnel ===
Neil Finn
Johnny Marr
Ed O'Brien
Phil Selway
Sebastian Steinberg
Lisa Germano
Liam Finn
Bic Runga
Don McGlashan
KT Tunstall
Jeff Tweedy
John Stirratt
Pat Sansone
Glenn Kotche
Glenn Richards
Tim Finn


=== Film ===
A film with the same name, which was made during the 20-day studio production and includes footage from a live performance in Auckland, had its world premiere on February 9, 2010 at the Santa Barbara International Film Festival.


== References ==

McCormick, Neil (2009-08-26). "7 Worlds Collide: The Sun Came Out, pop CD of the week". Telegraph.co.uk (London). Retrieved 2010-05-25. 


== External links ==
Official website