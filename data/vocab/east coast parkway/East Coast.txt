East Coast may refer to:


== Geography ==


=== New Zealand ===
Gisborne District
East Coast (New Zealand electorate)


=== United States ===
East Coast of the United States
East Coast Greenway


=== Elsewhere ===
Eastern states of Australia
Atlantic Canada
East Coast of Peninsular Malaysia
East Coast Park, Singapore
Levante, Spain


== Transport ==
East Coast Main Line, the electrified high-speed railway link between London, Peterborough, Doncaster, Leeds, York, Newcastle and Edinburgh
Virgin Trains East Coast, the UK train operating company currently running on the East Coast Main Line
East Coast Parkway, an expressway that runs along the southeastern coast of Singapore
East Coast Railway Zone (India), one of the sixteen railway zones of Indian Railways
Florida East Coast Railway, a Class II railroad operating in the US state of Florida
East Coast (train operating company), the publicly owned predecessor to Virgin Trains East Coast.
New Brunswick East Coast Railway, a historic Canadian railway that operated in the province of New Brunswick


== Other ==
East Coast Akalat, a small passerine bird which can be found in the east of Africa from Kenya to Mozambique
East Coast bias, where sports teams of the East Coast of the United States are given undue weight by broadcasters there
Australian south-east coast drainage division, the very long, narrow area of southern Australia
East Coast Swing, a form of social partner dance that evolved from the Lindy Hop
East Coast FM, a radio station in Co. Wicklow, Ireland