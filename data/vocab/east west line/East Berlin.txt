East Berlin existed between 1949 and 1990. It comprised the eastern regions of Berlin and consisted of the Soviet sector of Berlin that was established in 1945. The American, British, and French sectors became West Berlin, a part strongly associated with West Germany. East Berlin was the de facto capital of East Germany. From 13 August 1961 until 9 November 1989, East Berlin was separated from West Berlin by the Berlin Wall. The East German government referred to East Berlin simply as "Berlin" or often "Berlin, Hauptstadt der DDR" (Berlin, capital of the GDR). The term "Democratic Sector" was also used until the 1960s. (See also Naming conventions).


== History ==


=== Overview ===
When the German Democratic Republic was formed in 1949, it immediately claimed East Berlin as its capital--a claim that was recognized by all Communist countries. However, due to the legal fiction that East Berlin was still occupied territory, its representatives to the People's Chamber did not have full voting rights until 1968.
The Western Allies (the US, Britain, and France) never formally acknowledged the authority of the East German government to govern East Berlin; the official Allied protocol recognized only the authority of the Soviet Union in East Berlin in accordance with the occupation status of Berlin as a whole. The United States Command Berlin, for example, published detailed instructions for U.S. military and civilian personnel wishing to visit East Berlin. In fact, the three Western commandants regularly protested the presence of the East German National People's Army (NVA) in East Berlin, particularly on the occasion of military parades. Nevertheless, the three Western Allies eventually established embassies in East Berlin in the 1970s, although they never recognized it as the capital of East Germany. Treaties instead used terms such as "seat of government."
On 3 October 1990, West and East Germany and West and East Berlin were reunited, thus formally ending the existence of East Berlin.


=== East Berlin today ===
Since reunification, the German government has spent vast amounts of money on reintegrating the two halves of the city and bringing services and infrastructure in the former East Berlin up to the standard established in West Berlin. Despite this, there are still obvious differences between eastern and western Berlin. Eastern Berlin has a distinctly different visual aspect, partly because of the greater survival of prewar façades and streetscapes, some still showing signs of wartime damage, and partly because of the distinctive style of urban Stalinist architecture used in the GDR. As in other former East German cities, a small number of GDR-era names commemorating socialist heroes have been preserved, such as Karl-Marx-Allee, Rosa-Luxemburg-Platz, and Karl-Liebknecht-Straße; this followed a long process of review in which many such street names were deemed inappropriate and were changed. Still visible throughout former East Berlin are the characteristic "Ampelmännchen" on some pedestrian traffic lights. These days they are also visible in parts of the former West Berlin following a civic debate about whether the "Ampelmännchen" should be abolished or disseminated more widely. While both sides have now unified as Berlin, there is still a noticeable difference between East and West Berliners.


== Soviet and East German Commandants of East Berlin ==


== Boroughs of East Berlin ==
At the time of German reunification, East Berlin comprised the boroughs of
Friedrichshain
Hellersdorf (since 1986)
Hohenschönhausen (since 1985)
Köpenick
Lichtenberg
Marzahn (since 1979)
Mitte
Pankow
Prenzlauer Berg
Treptow
Weißensee


== Images of East Berlin ==


== See also ==

History of Berlin
Berlin Wall
Bonn
Cold War
Checkpoint Charlie
East Germany
Ghost station
History of Germany since 1945
West Berlin
West Germany


== References ==
Durie, W. (2012). The British Garrison Berlin 1945–1994 "No where to go" Berlin: Vergangenheits/Berlin. ISBN 978-3-86408-068-5.


== External links ==
Berlin Photos 1989–1999
East Berlin Past and Present
Pictures of the GDR and East Berlin 1949–1973
Old East Berlin Fades Away Amid Renovations