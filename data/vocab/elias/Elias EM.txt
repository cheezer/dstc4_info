The Elias EM was a 1920s American general-purpose and marine expeditionary biplane built by Elias.


== Development ==
The company designed the Elias EM-1 to meet a United States Marine Corps requirement for a multirole marine expeditionary aircraft. It was required to operate on either floats or wheels. The EM-1 was a tandem two-seat unequal-span biplane powered by a 300 hp (224 kW) Wright-Hispano H engine. The prototype was modified with equal-span wings and delivered to the Marine Corps in 1922. Six production aircraft were built (designated EM-2) having equal-span wings, being powered by 400 hp (298 kW) Liberty 12 inline engines.
One production aircraft was delivered to the United States Marine Corps and five to the United States Navy. One of the Navy aircraft was modified as an observation aircraft and redesignated EO-1.


== Variants ==
EM-1
Prototype with a 300hp (224kW) Wright-Hispano H engine, one built.
EM-2
Production aircraft with a 400hp (298kW) Liberty 12 inline engine, six built including one later converted to an EO-1.
EO-1
One EM-2 aircraft converted as an observation aircraft.


== Operators ==
 United States
United States Marine Corps
United States Navy


== Specifications (EM-2) ==
Data from The Illustrated Encyclopedia of Aircraft
General characteristics
Crew: two
Length: 28 ft 6 in (8.69 m)
Wingspan: 39 ft 8 in (12.09 m)
Height: 10 ft 9 in (3.28 m)
Gross weight: 4233 lb (1920 kg)
Powerplant: 1 × Liberty 12 inline piston engine, 400 hp (298 kW)
Performance
Maximum speed: 120 mph (193 km/h)
Armament
dorsal 0.3 inch machine gun


== See also ==
Related lists
List of military aircraft of the United States (naval)


== References ==

John Andrade, U.S.Military Aircraft Designations and Serials since 1909, Midland Counties Publications, 1979, ISBN 0-904597-22-9 (Page 171)
The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985), 1985, Orbis Publishing