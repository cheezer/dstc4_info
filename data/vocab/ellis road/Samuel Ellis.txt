Sam, Sammy or Samuel Ellis may refer to:
Sam Ellis (footballer) (born 1946), English footballer
Sam Ellis (athlete) (born 1982), British athlete
J. Sam Ellis, American politician
Sammy Ellis (born 1941), American baseball player
Samuel Ellis (merchant) (1733-1794), owner of land in New York harbor after whom 'Ellis Island' is named
Samuel Ellis (cricketer) (1851–1930), English cricketer
Samuel Burdon Ellis (1787–1865), British Royal Marines officer