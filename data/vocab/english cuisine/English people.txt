The English are a nation and ethnic group native to England, who speak the English language. The English identity is of early mediaeval origin, when they were known in Old English as the Angelcynn ("nationality of the Angles"). England is one of the countries of the United Kingdom and English people in England are British citizens. Their ethnonym is derived from the Angles, Germanic peoples who migrated to Great Britain in the 5th century AD.
Historically, the English population is descended from several peoples — the earlier Britons (or Brythons), the Germanic tribes that settled in the region (including Angles, Saxons and Jutes, collectively known as the Anglo-Saxons) who founded what was to become England (from the Old English Englaland) and the later Danes, Normans and other groups. Following the Acts of Union 1707, in which the Kingdom of England was succeeded by the Kingdom of Great Britain, English customs and identity became closely aligned with British customs and identity.
Today some English people have recent forebears from other parts of the United Kingdom while some are also descended from more recent immigrants from other European countries and from the Commonwealth.
The English people are the source of the English language, the Westminster system, the common law system and numerous major sports. These and other English cultural characteristics have spread worldwide, in part as a result of the former British Empire.


== English nationality ==
Although England is no longer an independent nation state, but rather a constituent country within the United Kingdom, the English may still be regarded as a "nation" according to the Oxford English Dictionary's definition: a group united by factors that include "language, culture, history or occupation of the same territory".
The concept of an "English nation" is far older than that of the "British nation", and the 1990s witnessed a revival in English self-consciousness. This is linked to the expressions of national self-awareness of the other British nations of Wales and Scotland  – which take their most solid form in the new devolved political arrangements within the United Kingdom  – and the waning of a shared British national identity with the growing distance between the end of the British Empire and the present.
Many recent immigrants to England have assumed a solely British identity, while others have developed dual or hyphenated identities. Use of the word "English" to describe Britons from ethnic minorities in England is complicated by most non-white people in England identifying as British rather than English. In their 2004 Annual Population Survey, the Office for National Statistics compared the ethnic identities of British people with their perceived national identity. They found that while 58% of white people described their nationality as "English", the vast majority of non-white people called themselves "British".


=== Relationship to Britishness ===
It is unclear how many British people consider themselves English. In the 2001 UK census, respondents were invited to state their ethnicity, but while there were tick boxes for 'Irish' and for 'Scottish', there were none for 'English', or 'Welsh', who were subsumed into the general heading 'White British'. Following complaints about this, the 2011 census was changed to "allow respondents to record their English, Welsh, Scottish, Northern Irish, Irish or other identity." Another complication in defining the English is a common tendency for the words "English" and "British" to be used interchangeably, especially overseas. In his study of English identity, Krishan Kumar describes a common slip of the tongue in which people say "English, I mean British". He notes that this slip is normally made only by the English themselves and by foreigners: "Non-English members of the United Kingdom rarely say 'British' when they mean 'English'". Kumar suggests that although this blurring is a sign of England's dominant position with the UK, it is also "problematic for the English [...] when it comes to conceiving of their national identity. It tells of the difficulty that most English people have of distinguishing themselves, in a collective way, from the other inhabitants of the British Isles".
In 1965, the historian A. J. P. Taylor wrote,
"When the Oxford History of England was launched a generation ago, "England" was still an all-embracing word. It meant indiscriminately England and Wales; Great Britain; the United Kingdom; and even the British Empire. Foreigners used it as the name of a Great Power and indeed continue to do so. Bonar Law, by origin a Scotch Canadian, was not ashamed to describe himself as "Prime Minister of England" [...] Now terms have become more rigorous. The use of "England" except for a geographic area brings protests, especially from the Scotch."
However, although Taylor believed this blurring effect was dying out, in his book The Isles (1999), Norman Davies lists numerous examples in history books of "British" still being used to mean "English" and vice versa.
In December 2010, Matthew Parris in The Spectator, analysing the use of "English" over "British", argued that English identity, rather than growing, had existed all along but has recently been unmasked from behind a veneer of Britishness.


=== Historical origins and identity ===

The traditional view of English origins was that the English are primarily descended from the Anglo-Saxons, the term used to describe the various Germanic tribes that migrated to the island of Great Britain following the end of the Roman occupation of Britain, with assimilation of later migrants such as the Vikings and Normans. This version of history is now regarded by most historians as incorrect, on the basis of more recent genetic and archaeological research. Based on a re-estimation of the number of settlers, some have taken the view that it is highly unlikely that the existing British Celtic-speaking population was substantially displaced by the Anglo-Saxons and that instead a process of acculturation took place, with an Anglo-Saxon ruling elite imposing their culture on the local populations. However, many historians, while making allowance for British survival, still hold to the view that there was significant displacement of the indigenous population.
The Celtic-speaking populations, particularly in their use of Brythonic languages such as Cornish, Cumbric and Welsh, held on for several centuries in parts of England such as Cornwall, Devon, Cumbria and a part of Lancashire. Historian Catherine Hills describes what she calls the "national origin myth" of the English:
The arrival of the Anglo-Saxons ... is still perceived as an important and interesting event because it is believed to have been a key factor in the identity of the present inhabitants of the British Isles, involving migration on such a scale as to permanently change the population of south-east Britain, and making the English a distinct and different people from the Celtic Irish, Welsh and Scots ....this is an example of a national origin myth ... and shows why there are seldom simple answers to questions about origins.
Modern research into the genetic history of the British Isles, popularised by Stephen Oppenheimer, Bryan Sykes and others, does not show a clear dividing line between the English and their 'Celtic' neighbours, but a gradual clinal change from west coast Britain to east coast Britain. They suggest that the majority of the ancestors of British peoples were the original palaeolithic settlers of Great Britain, and that the differences that exist between the east and west coasts of Great Britain though not large, are deep in prehistory, mostly originating in the upper palaeolithic and Mesolithic (15,000–7,000 years ago). Oppenheimer stated that genetic testing suggests that "75% of British and Irish ancestors arrive[d] between 15,000 and 7,500 years ago" (that is, long before the arrival of the Anglo-Saxons, and even before that of the Celts).


== History of English people ==


=== Early Middle Ages ===

The first people to be called 'English' were the Anglo-Saxons, a group of closely related Germanic tribes that began migrating to eastern and southern Great Britain, from southern Denmark and northern Germany, in the 5th century AD, after the Romans had withdrawn from Britain. The Anglo-Saxons gave their name to England (Engla land, meaning "Land of the Angles") and to the English.

The Anglo-Saxons arrived in a land that was already populated by people commonly referred to as the 'Romano-British'—the descendants of the native Brythonic-speaking population that lived in the area of Britain under Roman rule during the 1st–5th centuries AD. The multi-ethnic nature of the Roman Empire meant that small numbers of other peoples may have also been present in England before the Anglo-Saxons arrived. There is archaeological evidence, for example, of an early North African presence in a Roman garrison at Aballava, now Burgh-by-Sands, in Cumbria; a 4th century inscription says that the Roman military unit Numerus Maurorum Aurelianorum ("unit of Aurelian Moors") from Mauretania (Morocco) was stationed there.
The exact nature of the arrival of the Anglo-Saxons and their relationship with the Romano-British is a matter of debate. Traditionally, it was believed that a mass invasion by various Anglo-Saxon tribes largely displaced the indigenous British population in southern and eastern Great Britain (modern-day England with the exception of Cornwall). This was supported by the writings of Gildas, the only contemporary historical account of the period, describing slaughter and starvation of native Britons by invading tribes (aduentus Saxonum).
Added to this was the fact that the English language contains no more than a handful of words borrowed from Brythonic sources (although the names of some towns, cities, rivers etc. do have Brythonic or pre-Brythonic origins, becoming more frequent towards the west of Britain). However, this view has been re-evaluated by some archaeologists and historians since the 1960s, and more recently supported by genetic studies, who see only minimal evidence for mass displacement. Archaeologist Francis Pryor has stated that he "can't see any evidence for bona fide mass migrations after the Neolithic."
While the historian Malcolm Todd writes "It is much more likely that a large proportion of the British population remained in place and was progressively dominated by a Germanic aristocracy, in some cases marrying into it and leaving Celtic names in the, admittedly very dubious, early lists of Anglo-Saxon dynasties. But how we identify the surviving Britons in areas of predominantly Anglo-Saxon settlement, either archaeologically or linguistically, is still one of the deepest problems of early English history."
In a survey of the genes of British and Irish men, even those British regions that were most genetically similar to (Germanic speaking) continental regions were still more genetically British than continental: "When included in the PC analysis, the Frisians were more 'Continental' than any of the British samples, although they were somewhat closer to the British ones than the North German/Denmark sample. For example, the part of mainland Britain that has the most Continental input is Central England, but even here the AMH+1 frequency, not below 44% (Southwell), is higher than the 35% observed in the Frisians. These results demonstrate that even with the choice of Frisians as a source for the Anglo-Saxons, there is a clear indication of a continuing indigenous component in the English paternal genetic makeup."


=== Vikings and the Danelaw ===

From about 800 AD waves of Danish Viking assaults on the coastlines of the British Isles were gradually followed by a succession of Danish settlers in England. At first, the Vikings were very much considered a separate people from the English. This separation was enshrined when Alfred the Great signed the Treaty of Alfred and Guthrum to establish the Danelaw, a division of England between English and Danish rule, with the Danes occupying northern and eastern England.
However, Alfred's successors subsequently won military victories against the Danes, incorporating much of the Danelaw into the nascent kingdom of England. Danish invasions continued into the 11th century, and there were both English and Danish kings in the period following the unification of England (for example, Æthelred II (978–1013 and 1014–1016) was English but Cnut (1016–1035) was Danish).
Gradually, the Danes in England came to be seen as 'English'. They had a noticeable impact on the English language: many English words, such as anger, ball, egg, got, knife, take, and they, are of Old Norse origin, and place names that end in -thwaite and -by are Scandinavian in origin.


=== English unification ===

The English population was not politically unified until the 10th century. Before then, it consisted of a number of petty kingdoms which gradually coalesced into a Heptarchy of seven powerful states, the most powerful of which were Mercia and Wessex. The English nation state began to form when the Anglo-Saxon kingdoms united against Danish Viking invasions, which began around 800 AD. Over the following century and a half England was for the most part a politically unified entity, and remained permanently so after 959.
The nation of England was formed in 937 by Athelstan of Wessex after the Battle of Brunanburh, as Wessex grew from a relatively small kingdom in the South West to become the founder of the Kingdom of the English, incorporating all Anglo-Saxon kingdoms and the Danelaw.


=== Norman and Angevin rule ===

The Norman conquest of England during 1066 brought Anglo-Saxon and Danish rule of England to an end, as the new French speaking Norman elite almost universally replaced the Anglo-Saxon aristocracy and church leaders. After the conquest, "English" normally included all natives of England, whether they were of Anglo-Saxon, Scandinavian or Celtic ancestry, to distinguish them from the Norman invaders, who were regarded as "Norman" even if born in England, for a generation or two after the Conquest. The Norman dynasty ruled England for 87 years until the death of King Stephen in 1154, when the succession passed to Henry II, House of Plantagenet (based in France), and England became part of the Angevin Empire until 1399.
Various contemporary sources suggest that within 50 years of the invasion most of the Normans outside the royal court had switched to English, with Anglo-Norman remaining the prestige language of government and law largely out of social inertia. For example, Orderic Vitalis, a historian born in 1075 and the son of a Norman knight, said that he learned French only as a second language. Anglo-Norman continued to be used by the Plantagenet kings until Edward I came to the throne. Over time the English language became more important even in the court, and the Normans were gradually assimilated, until, by the 14th century, both rulers and subjects regarded themselves as English and spoke the English language.
Despite the assimilation of the Normans, the distinction between 'English' and 'French' survived in official documents long after it had fallen out of common use, in particular in the legal phrase Presentment of Englishry (a rule by which a hundred had to prove an unidentified murdered body found on their soil to be that of an Englishman, rather than a Norman, if they wanted to avoid a fine). This law was abolished in 1340.


=== In the United Kingdom ===

Since the 18th century, England has been one part of a wider political entity covering all or part of the British Isles, which today is called the United Kingdom. Wales was annexed by England by the Laws in Wales Acts 1535–1542, which incorporated Wales into the English state. A new British identity was subsequently developed when James VI of Scotland became James I of England as well, and expressed the desire to be known as the monarch of Britain.
In 1707, England formed a union with Scotland by passing an Act of Union in March 1707 that ratified the Treaty of Union. The Parliament of Scotland had previously passed its own Act of Union, so the Kingdom of Great Britain was born on 1 May 1707. In 1801, another Act of Union formed a union between the Kingdom of Great Britain and the Kingdom of Ireland, creating the United Kingdom of Great Britain and Ireland. In 1922, about two-thirds of the Irish population (those who lived in 26 of the 32 counties of Ireland), left the United Kingdom to form the Irish Free State. The remainder became the United Kingdom of Great Britain and Northern Ireland, although this name was not introduced until 1927, after some years in which the term "United Kingdom" had been little used.

Throughout the history of the UK, the English have been dominant in population and in political weight. As a consequence, notions of 'Englishness' and 'Britishness' are often very similar. At the same time, after the Union of 1707, the English, along with the other peoples of the British Isles, have been encouraged to think of themselves as British rather than to identify themselves with the constituent nations.


=== Immigration and assimilation ===

Although England has not been conquered since the Norman conquest nor extensively settled since, it has been the destination of varied numbers of migrants at different periods from the seventeenth century. While some members of these groups maintain a separate ethnic identity, others have assimilated and intermarried with the English. Since Oliver Cromwell's resettlement of the Jews in 1656, there have been waves of Jewish immigration from Russia in the nineteenth century and from Germany in the twentieth.
After the French king Louis XIV declared Protestantism illegal in 1685 with the Edict of Fontainebleau, an estimated 50,000 Protestant Huguenots fled to England. Due to sustained and sometimes mass emigration from Ireland, current estimates indicate that around 6 million people in the UK have at least one grandparent born in Ireland.
There has been a black presence in England since the 16th century due to the slave trade, and an Indian presence since at least the 17th century because of the East India Company and British Raj, or 16th century with the arrival of Romanichal migrants. Black and Asian populations have grown in England as immigration from the British Empire and the subsequent Commonwealth of Nations was encouraged due to labour shortages during post-war rebuilding. However, these groups are often still considered to be ethnic minorities and research has shown that black and Asian people in the UK are more likely to identify as British rather than with one of the state's four constituent nations, including England.


=== Current national and political identity ===
The 1990s witnessed a resurgence of English national identity. Survey data shows a rise in the number of people in England describing their national identity as English and a fall in the number describing themselves as British. Today, black and minority ethnic people of England still generally identify as British rather than English to a greater extent than their white counterparts; however, groups such as The Campaign for an English Parliament (CEP) suggest the emergence of a broader civic and multi-ethnic English nationhood. Scholars and journalists have noted a rise in English self-consciousness, with increased use of the English flag, particularly at football matches where the Union flag was previously more commonly flown by fans.
This perceived rise in English self-consciousness has generally been attributed to the devolution in the late 1990s of some powers to the Scottish Parliament and National Assembly for Wales. In policy areas for which the devolved administrations in Scotland, Wales and Northern Ireland have responsibility, the UK Parliament votes on laws that consequently only apply to England. Because the Westminster Parliament is composed of MPs from throughout the UK, this has given rise to the "West Lothian question", a reference to the situation in which MPs representing constituencies outside England can vote on matters affecting only England, but MPs cannot vote on the same matters in relation to the other parts of the UK. Consequently, groups such as the Campaign for an English Parliament have called for the creation of a devolved English Parliament, claiming that there is now a discriminatory democratic deficit against the English. The establishment of an English parliament has also been backed by a number of Scottish and Welsh nationalists. Writer Paul Johnson has suggested that like most dominant groups, the English have only demonstrated interest in their ethnic self-definition when they were feeling oppressed.
John Curtice argues that "In the early years of devolution...there was little sign" of an English backlash against devolution for Scotland and Wales, but that more recently survey data shows tentative signs of "a form of English nationalism...beginning to emerge among the general public". Michael Kenny, Richard English and Richard Hayton, meanwhile, argue that the resurgence in English nationalism predates devolution, being observable in the early 1990s, but that this resurgence does not necessarily have negative implications for the perception of the UK as a political union. Others question whether devolution has led to a rise in English national identity at all, arguing that survey data fails to portray the complex nature of national identities, with many people considering themselves both English and British.
Recent surveys of public opinion on the establishment of an English parliament have given widely varying conclusions. In the first five years of devolution for Scotland and Wales, support in England for the establishment of an English parliament was low at between 16 and 19%, according to successive British Social Attitudes Surveys. A report, also based on the British Social Attitudes Survey, published in December 2010 suggests that only 29% of people in England support the establishment of an English parliament, though this figure had risen from 17% in 2007. One 2007 poll carried out for BBC Newsnight, however, found that 61 per cent would support such a parliament being established. Krishan Kumar notes that support for measures to ensure that only English MPs can vote on legislation that applies only to England is generally higher than that for the establishment of an English parliament, although support for both varies depending on the timing of the opinion poll and the wording of the question. Electoral support for English nationalist parties is also low, even though there is public support for many of the policies they espouse. The English Democrats gained just 64,826 votes in the 2010 UK general election, accounting for 0.3 per cent of all votes cast in England. Kumar argued in 2010 that "despite devolution and occasional bursts of English nationalism – more an expression of exasperation with the Scots or Northern Irish – the English remain on the whole satisfied with current constitutional arrangements".


== English diaspora ==

From the earliest times English people have left England to settle in other parts of Great Britain and Northern Ireland, but it is not possible to identify their numbers, as British censuses have historically not invited respondents to identify themselves as English. However, the census does record place of birth, revealing that 8.08% of Scotland's population, 3.66% of the population of Northern Ireland and 20% of the Welsh population were born in England. Similarly, the census of the Republic of Ireland does not collect information on ethnicity, but it does record that there are over 200,000 people living in Ireland who were born in England and Wales.
English emigrant and ethnic descent communities are found across the world, and in some places, settled in significant numbers. Substantial populations descended from English colonists and immigrants exist in the United States, Canada, Australia, South Africa and New Zealand.


=== United States ===

According to the American Community Survey in 2009 data, Americans reporting English ancestry made up an estimated 9.0% of the total U.S. population, and form the third largest European ancestry group after German Americans and Irish Americans. However, demographers regard this as an undercount, as the index of inconsistency is high, and many, if not most, people from English stock have a tendency to identify simply as Americans or, if of mixed European ancestry, nominate a more recent and differentiated ethnic group.
In the 2000 United States Census, 24,509,692 Americans described their ancestry as wholly or partly English. In addition, 1,035,133 recorded British ancestry.
In the 1980 United States Census, over 49 million (49,598,035) Americans claimed English ancestry, at the time around 26.34% of the total population and largest reported group which, even today, would make them the largest ethnic group in the United States.


=== Canada ===

In the 2006 Canadian Census, 'English' was the most common ethnic origin (ethnic origin refers to the ethnic or cultural group(s) to which the respondent's ancestors belong) recorded by respondents; 6,570,015 people described themselves as wholly or partly English, 16% of the population. On the other hand, people identifying as Canadian but not English may have previously identified as English before the option of identifying as Canadian was available.


=== Australia ===

In Australia, the 2006 Australian Census recorded 6,298,945 people who described their ancestry, but not ethnicity, as English. 1,425,559 of these people recorded that both their parents were born overseas. Australians of English descent continue to form the largest single ethnic group in Australia.


=== Other communities ===
Significant numbers of people with at least some English ancestry also live in Scotland and Wales, as well as in Ireland, Chile, Argentina, New Zealand, South Africa and Brazil.
Since the 1980s there have been increasingly large numbers of English people, estimated at over 3 million, permanently or semi-permanently living in Spain and France, drawn there by the climate and cheaper house prices.


== Culture ==

The culture of England is sometimes difficult to separate clearly from the culture of the United Kingdom, so influential has English culture been on the cultures of the British Isles and, on the other hand, given the extent to which other cultures have influenced life in England.


== See also ==

Language:

Diaspora:


== Notes ==


== References ==
Expert Links: English Family History and Genealogy Useful for tracking down historical inhabitants of England.
Condor, Susan; Gibson, Stephen; Abell, Jackie (2006). "English identity and ethnic diversity in the context of UK constitutional change" (PDF). Ethnicities 6 (2): 123–158. doi:10.1177/1468796806063748. 
Kate Fox (2004). Watching the English. Hodder & Stoughton. ISBN 0-340-81886-7. 
Kumar, Krishan (2003). The Making of English National Identity. Cambridge University Press. ISBN 0-521-77736-4. 
Kumar, Krishan (2010). "Negotiating English identity: Englishness, Britishness and the future of the United Kingdom". Nations and Nationalism 16 (3): 469–487. doi:10.1111/j.1469-8129.2010.00442.x. 
Paxman, Jeremy (1999). The English. Penguin Books Ltd. ISBN 0-14-026723-9. 
Robert J.C. Young (2008). The Idea of English Ethnicity. Blackwell Publishers. ISBN 1-4051-0129-6. 


=== Diaspora ===
Bueltmann, Tanja, David T. Gleeson, and Donald M. MacRaild, eds. Locating the English Diaspora, 1500–2010 (Liverpool University Press, 2012) 246 pp.


== External links ==
BBC Nations Articles on England and the English
The British Isles Information on England
Mercator's Atlas Map of England ("Anglia") circa 1564.
Viking blood still flowing; BBC; 3 December 2001.
UK 2001 Census showing 49,138,831 people from all ethnic groups living in England.
Tory MP leads English protest over census; The Telegraph; 23 April 2001.
On St. George's Day, What's Become Of England?; CNSNews.com; 23 April 2001.
Watching the English – an anthropologist's look at the hidden rules of English behaviour.
The True-Born Englishman, by Daniel Defoe.
The Effect of 1066 on the English Language Geoff Boxell
BBC "English and Welsh are races apart"
New York Times, When English Eyes Are Smiling Article on the common English and Irish ethnicity
Y Chromosome Evidence for Anglo-Saxon Mass Migration
Origins of Britons – Bryan Sykes