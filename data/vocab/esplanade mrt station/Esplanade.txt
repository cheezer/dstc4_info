An esplanade or promenade is a long, open, level area, usually next to a river or large body of water, where people may walk. The original meaning of esplanade was a large, open, level area outside fortress or city walls to provide clear fields of fire for the fortress' guns. In modern usage the space allows people to walk for recreational purposes; esplanades are often on sea fronts, and allow walking whatever the state of the tide, without having to walk on the beach. Esplanades became popular in Victorian times when it was fashionable to visit seaside resorts. A promenade, often abbreviated to '(the) Prom', was an area where people - couples and families especially - would go to walk for a while in order to 'be seen' and be considered part of 'society'.
In North America, esplanade has another meaning, being also a median (strip of raised land) dividing a roadway or boulevard. Sometimes they are just strips of grass, or some may have gardens and trees. Some roadway esplanades may be used as parks with a walking/jogging trail and benches.
Esplanade and promenade are sometimes used interchangeably. The derivation of "promenade" indicates a place specifically intended for walking, though many modern promenades and esplanades also allow bicycles and other nonmotorized transport. Some esplanades also include large boulevards or avenues where cars are permitted.
A similar term with the same meaning in the eastern coastal region of Spain is rambla, but more widely referred to as paseo marítimo ("esplanade"), paseo ("promenade") or explanada ("esplanade") in the Hispanic world.


== Examples ==


=== Coastal ===


==== Asia ====


===== India =====
Kamarajar Salai, Chennai in Chennai, India
Marine Drive, Kochi in Kochi, India
Marine Drive in Mumbai
Bandstand Promenade in Mumbai
Promenade Beach in Pondicherry


===== Malaysia =====
Karpal Singh Drive in George Town, Penang
Gurney Drive in Tanjung Tokong, Penang
Padang Kota Lama in George Town, Penang


===== Philippines =====
Baywalk, Manila
Dipolog Boulevard, Dipolog City
Rizal Boulevard Promenade, Dumaguete City


===== Others =====
Tsim Sha Tsui East, Victoria Harbour in Hong Kong
Corniche Beirut, Beirut, Lebanon
The Esplanade, Singapore
Galle Face Green, Colombo, Sri Lanka
Breakwater Corniche or Al-Kasr in Abu Dhabi, United Arab Emirates


==== America and Caribbean ====


===== United States =====
The Charles River Esplanade, Boston, Massachusetts, United States
The Esplanade, Redondo Beach, California, United States
The Esplanade, Rio Del Mar, California, United States
Third Street Promenade, Santa Monica, Los Angeles County, California, United States


===== Others =====
Calçadão de Copacabana and Calçadão de Ipanema (Rio de Janeiro, Brazil)
Malecón, Havana, Cuba


==== Europe ====
Usedom Beach Promenade, Western Pomerania, Germany (Europe’s longest beach promenade)
Riga, Esplanade, Latvia
The Esplanade, in Weymouth, England
La Promenade des Anglais, in Nice, France
The Esplanade, in Bangor, Dalriada
Herbert Samuel Promenade, in Tel Aviv, Israel
Corniche (Alexandria), Alexandria, Egypt


==== Others ====
St. Clair Esplanade in Dunedin, New Zealand
Marine Parade, in Napier, New Zealand
Świnoujście, by the Baltic Sea, Poland
Trzebież, by the Szczecin Lagoon, Poland
The Promenade, at Portobello, in Edinburgh, Scotland
The Golden Mile, Durban, South Africa
Sea Point, Cape Town, South Africa
The Scarborough Esplanade, Scarborough, Tobago
Rambla de Montevideo, Montevideo, Uruguay


=== Inland ===
Esplanade of the European Parliament, in Brussels, Belgium
Iloilo River Esplanade in Iloilo City, Philippines
Thames Embankment, in London, England
Brühl's Terrace, Dresden, Germany
The Danube Promenade in Budapest, Hungary
Esplanade, in Calcutta, India
Sibu Esplanade, Sibu, Sarawak, Malaysia
Błonia, Kraków, Poland
L'Enfant Plaza in Washington, D.C., United States
Charles River Reservation, in Boston, Massachusetts, United States
The Eastbank Esplanade, in Portland, Oregon, United States


== Gallery ==


== See also ==
Boardwalk
Foreshoreway
Oceanway
Processional walkway
Similar areas inland: Boulevard, mall


== References ==