Esplanade (Bengali: এসপ্ল্যানেড) is an area in central Kolkata, earlier known as Calcutta, in the Indian state of West Bengal. This is not a conventional esplanade in the sense that the place is not exactly situated alongside a waterbody. However, the river Ganga, also known as the Ganges or the Hooghly, is located nearby. This esplanade is located in the heart of the city and is one of the busiest portions.


== History ==
The Esplanade was the name given to the northern portion of jungle, which later formed the Maidan. In olden days, it stretched from Dhurmotollah (now Lenin Sarani) to Chandpal Ghat on the Hooghly River. In the days of Warren Hastings, it formed a favourite promenade for 'elegant walking parties'. The five principal streets of Kolkata abutted on it, says Sophia Goldborne, who wrote in 1780. Both Daniell and William Baillie give a picture of Esplanade as it appeared in the closing years of the eighteenth century. The old Government House and the Council House are conspicuous objects in each drawing. Danielle presents an unfamiliar addition view in the shape of two elephants with a crowd of attendants. Bourne & Shepherd, photographic studio was established here in 1867 by British photographers, Samuel Bourne and Charles Shepherd, and still exist here.
The strengthening of British power, subsequent to their victory in the Battle of Plassey was followed by the construction of the new Fort William, in 1758. The European inhabitants of Kalikata gradually forsook the narrow limits of the old palisades and moved to around the Maidan .


== The neighbourhood ==
There are a number of roads and streets which form part of or have been linked with the Esplanade for many years.


=== Esplanade Row ===
The street named Esplanade Row, finds a mention in the map of 1784, running from Dhurmotollah right on to the banks of the river at Chandpal Ghat, passing on its way Government House and Council House, both facing south. Subsequently, it was divided by the gardens of Government House into two parts, Esplanade Row (East) and Esplanade Row (West). Esplanade Row (West) now runs between Government Place (West) and Strand Road, through the High Court. Esplanade Row (East) has been renamed Sidhu Kanu Dahar and occupies the northern end of the Maidan.

As a result of the movement of Europeans from the old palisades in the eighteenth century, Esplanade Row took on a new look. The most important public buildings and imposing private houses lined the northern side of the Esplanade, facing the Maidan on the south. “Esplanade Row,” wrote Mrs. Fay, “seems to be composed of palaces.”


=== Old Court House Street ===
Old Court House Street connects Esplanade Row (East) (or presently Sidhu Kanu Dahar) with B. B. D. Bagh, earlier known as Dalhousie Square. It acquired its name from the old court house, that was located where St. John's Church now stands. The northern part of the stretch is known as B.B.D. Bagh (East). It was constructed around 1781, when the finishing touches were put to the new Fort William. It is linked with the name of Col. Henry Watson, who brought about many improvements in Calcutta, including the laying out of surrounding Esplanade. The Red Road is an extension of this street. The stretch of Old Court House Street from the crossing with Ganesh Avenue or the south-eastern corner of B.B.D.Bagh to the crossing with Waterloo Street has been renamed Hemanta Bose Sarani. The stretch from the crossing with Waterloo Street to the crossing with Rani Rashmoni Avenue has been renamed Marx-Engels Bithi.


=== Council House Street ===
Council House Street connects the western part of B.B.D.Bagh with Esplanade Row. It acquires its name from the old council house, which stood on the western portion of Government House. It was pulled down in 1800. The southern part of the street was subsequently called Government Place West.Fort William College was located at the corner of Council House Street.


=== Dacres Lane ===
The short lane between Esplanade Row (East) and Waterloo Street was once a fashionable locality. It was named after Philip Milner Dacres, Collector of Kolkata in 1773 and afterwards a member of the Council. It has been renamed James Hicki Sarani. With numerous eateries, the road is popularly referred to a Tiffin Gali (lane).One wall side shop namely "Chitto dar dokan" is very famous for its various snacks particularly bread & mutton stew. One dance/music bar "Metrpol Bar" is also existing there.


=== Tipu Sultan Mosque ===
Tipu Sultan Mosque at the junction of Esplanade Row (East) and Dhurmotollah Street was built by his eighth son, Prince Gholam Mohammad. It was declared a heritage building by Kolkata Municipal Corporation


== Around the Esplanade ==


== Soccer ==
The first recorded soccer match in Kolkata was played on the Esplanade in the second week of April 1858 between the Calcutta Club of Civilians and the 'Gentlemen of Barrakpur'. There had been matched earlier but those were not documented. With traffic taking over the Esplanade, games have shifted further south on the Maidan.


== Traffic hub ==
The Esplanade has always been a major traffic hub. In 1902, the first electric tram car ran from Esplanade to Kidderpore. In 1984, the first underground railway in India, started from Tollygunge to Esplanade. Today, Esplanade is the busiest tram terminus & busiest metro station of Kolkata
In 2002, it was estimated that during rush hours some 200,000 to 300,000 vehicles passed through the Esplanade. Experts say 50–60 per cent of air pollution in Kolkata is due to vehicle emission. Increase in number of vehicles may have added to the problem. The noise tolerance level of an average human being is 60–65 decibels (dB). Recommended levels are 45 dB for residential areas and a maximum of 85 dB for industrial areas without resident population. In the Esplanade area, the noise levels are 75–84 dB.
According to transport department officials, over 2,000 long-distance buses operate from the city. Most of the state and private buses originate from the Esplanade and Babughat, nearby. Some private buses operate from Howrah station and Rabindra Setu. The state transport department will construct two central bus terminuses here, on the lines of the Central Bus Terminus in Delhi, to avoid the increasing traffic congestion that officials described as "alarming". One will come up along the Eastern Metropolitan Bypass, near one of the entrances to Salt Lake city. The other will be located in Howrah, near Vidyasagar Setu. .
A newspaper reported, “Promontories of garbage, vendors happily hawking away fruits and woollens, harassed commuters chasing buses... Add to this winding trams, rallies, clouds of black smoke and puddles of water. You’ve guessed right, this is Esplanade on any given day. But by the end of this year, this strategic junction located in the heart of the city will get a new look, thanks to a brainwave of transport minister Subhash Chakraborty. His plan is simple: Push the long-distance buses to another spot and Esplanade will breathe freely.”
The proposed second metro line, cutting across Kolkata from east to west, will not touch the Esplanade. It will meet the old metro at Central Station.
The state government has accepted a proposal to add two northward flanks to the Park Street flyover to ensure smooth flow of traffic through Esplanade during an agitation in front of Metro cinema. The flanks will descend on Chittaranjan Avenue and Bentinck Street.


== Rallies ==
The growing influence of the Left, particularly the Communist Party of India in organising urban protest was strongly felt from around 1957. The old institutions of mass mobilisation were strengthened to suit the agitation-oriented strategy of the left parties. A particularly significant expression of this agitation-oriented form of opposition to government policies was the way in which massive demonstrations of tens of thousands of people were organised in the heart of the city. Various administrative steps were taken to impede or disrupt large public rallies. Police resorted to large scale arrests or even shooting. Esplanade East, the Monument, and Brigade Parade ground, lying just outside the cordon thrown around the citadels of power, were the new rallying points of mass protest.
With the Left Front in power for decades, the opposition has picked up similar agitation-oriented tactics quite often bringing different parts of the city, including the Espalanade to a halt. Even the ruling party in the state continues to organise rallies (sometimes against the policies of the Central Government). In March 2007, CITU organised an exhibition of buses that had been damaged during the previous general strike called by the opposition.
Not only political rallies, sometimes religious ceremonies, as for example Muharram, throw traffic out of gear. Even Rath Yatra has to pass through the Esplanade. Nobody wants to miss the limelight of a busy street junction.


== See also ==
Metropolitan Building


== References ==


== Statues ==
There are a number of statues spread across the Esplanade. Here are photographs of some.


== External links ==
 Kolkata/Esplanade travel guide from Wikivoyage