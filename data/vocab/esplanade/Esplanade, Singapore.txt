The Esplanade is a waterfront location just north of the mouth of the Singapore River in downtown Singapore. It is primarily occupied by the Esplanade Park, and was the venue where one of Singapore's largest congregation of satay outlets until their relocation to Clarke Quay as a result of the construction of a major performance arts venue, the Esplanade - Theatres on the Bay, which took its name from this location.
Apart from the shows, Singaporeans usually visit the Esplanade for its scenic outdoor view. It overlooks the Singapore river and is in between the Singapore Flyer and Merlion park, making for a good stroll around the vicinity. River ferries stops have been added recently, enhancing its accessibility.


== Etymology and history ==
In the 1850s, the Esplanade referred to what is now the Padang, previously known as The Plain, in front of City Hall. It is best captured in John Turnbull Thomson's 1851 painting, "The Esplanade from Scandal Point".
With land reclamation in 1943, the term Esplanade then referred to the area of the present Queen Elizabeth Walk. Both the Esplanade Park and Queen Elizabeth Walk were built on the reclaimed land. In the park are several memorials, including The Cenotaph (completed in 1922), the Former Indian National Army Monument, the Tan Kim Seng Fountain (moved here in 1925 from Fullerton Square), and the Lim Bo Seng Memorial (which was unveiled in 1954).

Queen Elizabeth Walk covers an area of approximately 9.5 acres (38,000 m2). The walk was substantially completed in 1953 and was named in honour of Her Majesty, Queen Elizabeth's coronation in the same year. It was declared open on Coronation Day, 30 May 1953 by T.P.F. McNiece. There was an open air bandstand near the Lim Bo Seng Memorial.
The Tamils call the Esplanade "January thidal" or "January place" because of the former sports activities held there on New Year's Day. The Chinese called the place tua kok cheng chau po or "the grass field in front of the great court", referring to the Padang in front of the Supreme Court.


== References ==
Victor R Savage, Brenda S A Yeoh (2004), Toponymics - A Study of Singapore Street Names, Eastern University Press, ISBN 981-210-364-3


== External links ==
Esplanade - Theatres on the Bay
Esplanade - Theatres on the Bay Reviews
Esplanade celebrates 10 years of artful existence -Pravasi Express
Esplanade the National Performing Arts Centre