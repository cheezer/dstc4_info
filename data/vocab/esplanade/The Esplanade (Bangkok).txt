The Esplanade is a shopping and entertainment complex on Ratchadaphisek Road in Din Daeng district, Bangkok, with a second branch on Rattanathibet Road in Nonthaburi.


== History ==
Anchored by the Esplanade Cineplex, the shopping mall opened in December 2006. The cineplex hosted the 5th World Film Festival of Bangkok in October 2007.
With a retail space of more than 100,000 square metres, the seven-floor complex includes the 12-screen Esplanade Cineplex with 3,000 seats, a B2S book shop along with other shops and restaurants. Other facilities include a Rhythm & Bowl alley, the Sub Zero ice-skating arena, a Tops Supermarket and the 1,500-seat Ratchadalai Musical Theatre.


== Design ==
The architecture of the Esplanade shopping mall was designed by The Office of Bangkok Architects in collaboration with Contour. The interior of the mall was designed by J+H Boiffils, who had previously designed The Emporium and Siam Paragon shopping malls.


== Facilities ==


=== Anchors ===
Major Cineplex Group (Cineplex, bowling alley, disco ice skate arena, karaoke club)
Central Retail Group (Tops Market, B2S book shop)


=== About ===
Esplanade Ratchadapisek The Esplanade Shoping mall and Entertainment Complex on Ratchadapisek Road in Din Deang District in Bangkok , was opened in December 2006. The Cineplex hosted the 5th World Film Festival of Bangkok. Esplanade have entertainment such as ice skate, Cineplex, bowling, mini land education zone and parking zone.


=== Retail outlets ===
Blue 'o' Bowling Club and karaoke have promotion every week. For example 3 games and get one free game.

in Blue o have brewery zone that so chill out. Karaoke is very good sound and rooms.

Skate Board Club is so similar but it has fashion clothing.
Central B2S Page One Book Store
Boots Pharmacy
Fresh Air Entertainment Music Live House
Orangery Too Bar & Restaurant
KPN Music Academy
iStudio by SPV Group


=== Entertainment and leisure activities ===
California Wow Xperience Fitness Club
Ratchadalai Musical Theatre - Operated by Scenario Co., Ltd., the 1,500-seat theatre maintains a regular schedule of musical theatre productions.
Sub Zero ice-skating rink
An indoor skatepark
Cineplex on the top floor for the latest movies. Fantastic seating and good size screens.


=== Restaurants ===
There are many restaurants in the complex from Thai cuisine to western food. Also a few fast food restaurants including KFC in the basement area, McDonalds on the ground floor and recently opened in November 2013, Subway. The basement as the majority of restaurants from Italian, Vietnam, Chinese and Japanese food. On the ground floor, there are a few Thai restaurants and coffee shops including Starbucks and Coffee World. The area on the ground floor has extra seating in front of their restaurants and coffee shops.
Prices range from 100 to 350 baht per meal depending on choice of food. Since the opening of Param 9 Central, many restaurants at Esplanade are losing business slowly. But new restaurants are coming in all the time.


=== Transportation and nearby attractions ===
Traveling to esplanade is easy by car, bus, boat and subway.
MRT: Take the MRT (underground train) to Thailand Culture Center and Exit number 3 and turn left after going down the steps. Walk 50 meters and Esplanade will be on the left.
Taxi: Take a taxi to Esplanade. Esplanade is located on Ratchada Road. From Sukhumvit (ASOK Area), take the road towards Petchaburi. At Param 9 intersection, you will see Fortune Town on the left. Go straight along Ratchada road for 2 km. Esplanade will be on the left. Most taxi drives know Esplanade and Ratchada Road. Normal taxi fee should be around 60 baht from Asok, 100 baht from Khasoan Road.
Motorcycle Taxi: Same as above but as most motorcycle taxi drivers won't drive that far, it is best to keep within a certain range (no more than 3 km) of Esplanade when taking a motorcycle taxi.
Bus Route: 36,54,73,136,157,163,206,185,204,514,517,528,529.

The Esplanade is adjacent to the Bangkok Metro's

Thailand Cultural Centre station (exit 3). 

Other nearby stores include and Big C Extra hypermarket and Robinsons. Behind Esplanade is a nightclub called Inch along with further nightclubs on Ratchada Soi 3 & 4. The Ratchada nightclub strip is located opposite the mall.


== Branches ==
Ratchadapisek Bangkok
Rattanathibet Nonthaburi


== See also ==
List of shopping malls in Bangkok
List of shopping malls in Thailand
List of largest shopping malls in Thailand
List of cinemas in Thailand


== References ==


== External links ==
Esplanade Cineplex
Siam Future property profile