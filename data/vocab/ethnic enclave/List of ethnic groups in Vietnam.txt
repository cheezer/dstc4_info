Vietnam is a multiethnic country with over fifty distinct groups (54 are recognized by the Vietnamese government), each with its own language, lifestyle, and cultural heritage. Many of the local ethnic groups are known collectively in the West as Montagnard or Degar. The largest ethnic groups are: Kinh (Viet) 86.2%, Tay 1.9%, Tai Ethnic 1.7%, Mường 1.5%, Khmer Krom (Khơ Me Crộm) 1.4%, Hoa 1.1%, Nùng 1.1%, Hmong 1%, others 4.1% (1999 census). The Vietnamese term for ethnic group is người thiểu số or dân tộc thiểu số (literally "minority people").


== Alphabetical list ==
Ba Na (Bahnar)
Bố Y
Brâu
Bru
Chăm - Descendants of the Champa polities of Southern Vietnam
Chơ Ro
Chu Ru (Chru)
Chứt - related to the Kinh, only 2000-4000 people
Co
Cờ Ho (Koho)
Cờ Lao (Gelao)
Cơ Tu
Phunoi (Cống)
Dao - Yao people, also known as Mien, many speak Iu Mien language
Ê Đê (Rade)
Gia Rai (Jarai)
Giáy
Giẻ Triêng
Hà Nhì (Hani)
Hmong (formerly known as Mèo, classified as Miao in China)
Hoa people (Overseas Chinese, not to be confused with the Ngái Hokkien, who are classified separately)
Hrê
Kháng
Khmer Krom (Khơ Me Crộm)
Khơ Mú (Khmu)
Kinh (also called Viet, the largest ethnic group in Vietnam)
La Chí
La Ha
La Hủ
Lao
Lô Lô (Yi)
Lự
Mạ
Mảng
Mnông
Mường - Closest to the Kinh, the other main part of the Viet–Mường branch of the Vietic subfamily
Ngái (Hakka Chinese, classified separately from the Hoa)
Nùng
Ơ Đu
Pà Thẻn (Pa-Hng)
Phù Lá
Pu Péo (Pubiao)
Ra Glai (Roglai)
Rơ Măm
Sán Chay (San Chay, Cao Lan)
Sán Dìu (San Diu, Yao that speak Cantonese, though some know Iu Mien)
Si La
Tà Ôi (Ta Oi)
Tày (Tay) - The largest minority in Vietnam
Thái (Thai)
Thổ (Tho) - Related to Kinh Vietnamese
Xinh Mun (Xinh-mun)
Xơ Đăng (Sedang, Xo Dang)
Xtiêng (Stieng)


== Listed by language group ==
Vietic - Kinh, Mường, Chứt, Thổ
Austroasiatic - Bahnar, Brâu, Bru, Chơ Ro, Co, Cờ Ho, Cơ Tu, Giẻ Triêng, Hrê, Kháng, Khmer Krom, Khơ Mú (Khmu), Mạ, Mảng, Mnông, Ơ Đu, Rơ Măm, Tà Ôi, Xinh Mun, Xơ Đăng (Sedang), and Xtiêng (Stieng).
Tay-Thai - Bố Y (Bouyei), Giáy, Lao, Lự, Nùng, Sán Chay (Cao Lan), Tày and Thái
Tibeto-Burman - Cống, Hà Nhì (Hani), La Hủ (Lahu), Lô Lô (Yi), Phù Lá, and Si La
Malayo-Polynesian - Chăm, Chu Ru (Chru), Ê Đê (Rade), Gia Rai (Jarai), and Raglai
Kadai - Cờ Lao (Gelao), La Chí, La Ha, and Pu Péo
Mong-Dao - Dao (Yao), Hmong, and Pà Thẻn
Han - Hoa (Overseas Chinese), Ngái (Hokkien), and Sán Dìu


== Listed by population (as of the 1999 census) ==
Kinh (also called Viet, the largest ethnic group in Vietnam) 86.2%
Tày (Tay) - The largest minority in Vietnam 1.9%
Thái (Thai) 1.7%
Mường - Closest to Kinh Vietnamese, other half of Viet–Muong language family 1.5%
Khmer Krom (Khmer, Khơ Me Crộm) 1.4%
Hoa (Chinese) 1.1%
Nùng 1.1%
H'Mông (Hmong, Hơ-mông, Mong; formerly known as Mèo) - Also known as Miao in China 1%
Dao - Yao people, also known as Mien, many speak Iu Mien language, distant relatives of Hmong
Gia Rai (Jarai, J'rai)
Ê Đê (Rhade)
Ba Na (Bahnar)
Sán Chay (San Chay, Cao Lan)
Chăm - Descendants of the Champa kingdom in southern Vietnam
Xơ Đăng (Sedang, Xo Dang)
Sán Dìu (San Diu)
Hrê (H're)
Cờ Ho
Ra Glai (Raglai)
M'Nông
Thổ (Tho) - Related to Kinh Vietnamese
Xtiêng (Stieng)
Khơ Mú (Khmu)
Bru-Vân Kiều
Giáy
Cơ Tu
Giẻ Triêng
Tà Ôi (Ta Oi)
Mạ
Co
Chơ Ro
Hà Nhì (Hani)
Xinh Mun (Xinh-mun)
Chu Ru (Chru)
Lao - People from Laos
La Chí
Phù Lá
La Hủ
Kháng
Lự
Pà Thẻn
Lô Lô (Lo Lo, Yi)
Chứt - related to Vietnamese, only 2000-4000 people
Mảng
Cờ Lao (Gelao)
Bố Y (Buyei)
La Ha
Cống (Cong)
Ngái
Si La
Pu Péo
Brâu
Rơ Măm
Ơ Đu


== Ethnic groups not included in official list ==


=== Natives ===
Nguồn - possibly Mường group, officially classified as a Việt (Kinh) group by the government, Nguồn themselves identify with Việt ethnicity; their language is a member of the Viet–Muong branch of the Vietic sub-family
Sui people (Người Thủy) - officially classified as Pa Then people.
According to news from Dantri, an online newspaper in Vietnam, the Thừa Thiên-Huế People's Committee in September 2008 announced a plan to do more research in a new ethnic group in Vietnam. It is Pa Kô, also called Pa Cô, Pa Kô, Pa-Kô or Pa Kôh. This ethnic group settles mainly in A Lưới suburban district (Thừa Thiên-Huế) and mountainous area of Hướng Hóa (Quảng Trị). At the present, however, they have been being classified in Tà Ôi ethnic group.


=== Europeans ===
People of European, North American, Australian, and Asian (non-Vietnamese) origin. Many people of these origins are temporary residents in Vietnam as expatriate workers and some are permanently settled there, some through marriage. Included in permanent settlers are Europeans of French descent, who are descendants of the colonial settlers. Most of them left after its independence.


== Sources ==
1999 Census results
Socioeconomic Atlas of Vietnam, 1999
^ Việt Nam sẽ có dân tộc thứ 55?
^ Pa Kô được bổ sung vào danh mục các dân tộc Việt Nam


== External links ==
Ethnic groups of Vietnam (State Committee for Ethnic Minority Affairs)
Vietnamese Ethnic Groups
vietnam tours
Ethnic - Linguistic Map of Vietnam
Vietnamese ethnic groups by population
Ethnologue report for Vietnam
Story and Images of Missionaries interacting with Mountain Tribe groups in Vietnam from 1929-1975
Authentic Old photographs of all Ethnic groups in Vietnam


== See also ==
Demographics of Vietnam
Vietnam#Demographics