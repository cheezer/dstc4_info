The Mazda Persona (also sold as the Eunos 300) is a mid-sized front wheel drive sedan that was sold by Mazda in Japan in the late 1980s. It used Mazda's MA platform and was replaced by the Efini MS-8 in March 1993.

The Persona was Mazda's answer to the Toyota Carina ED, a Japanese sedan that attempted to capture the hardtop look and proportion of large American sedans. Transposed onto a smaller Japanese sedan, this proportion often led to a small, low cabin in context of longer front and rear ends.
Mazda placed much emphasis on the Persona's interior. It featured lounge-style door trims that appears completely integrated into the rear seats when the doors are closed.
When Mazda launched the Eunos dealership channel in Japan in 1990, the Persona became available also as an Eunos 300. Along with the Eunos Roadster (Mazda MX-5 Miata). the Eunos 100 (Mazda Familia Astina), and the 1990 Eunos Cosmo, these formed the initial Eunos brand lineup. The Eunos 300 was a stop-gap solution until the January 1992 launch of the Eunos 500, also known as the Mazda Xedos 6.


== External links ==
Specifications