Eunos is a neighbourhood in Singapore in the Paya Lebar urban planning area east of the Central Area. Eunos MRT Station serves this area.


== Etymology and early history ==
The settlement was originally named Kampong Melayu, a large Malay village that used to include Kampong Ubi and Kaki Bukit areas. It was later renamed Kampong Eunos in honour of its founder, Inche Muhammad Eunos Abdullah.
Inche Eunos was the Chairman and co-founder of the Kesatuan Melayu Singapura (the Singapore Malay Union) and the first Malay representative of the Legislative Council, the then governing body of Singapore. In 1927, he appealed to the government and was granted $700,000 that the Kesatuan Melayu Singapura used for the purchase of 240 hectares of land, which later became known as Kampong Melayu.


== External links ==
Reference from National Library of Singapore Infopedia website
Inche Muhammad Eunos Bin Abdullah
Jalan Eunos