The European Amateur Boxing Championships is the highest competition for boxing amateurs in Europe, organised by the continent's governing body EUBC, which stands for the European Boxing Confederation. The first edition of the tournament took place in 1924, although the first 'competitive' championships were hosted by the city of Stockholm (Sweden) in 1925.


== Men's European Amateur Boxing Championships History ==


== Women's European Amateur Boxing Championships History ==


== See also ==
World Amateur Boxing Championships
European Union Amateur Boxing Championships


== References ==
European Boxing Confederation
All available Results