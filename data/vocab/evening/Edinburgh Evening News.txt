The Edinburgh Evening News is a local newspaper based in Edinburgh, Scotland, that was first published in 1873. It is printed daily, except on Sundays. It is owned by Johnston Press, which also owns The Scotsman.
Much of the copy contained in the Evening News concerns local issues such as transport, health, the local council and crime in Edinburgh and the Lothians.
According to ABC figures of February 2014, the paper's circulation was 28,000, down from 32,160 in the preceding February.


== See also ==
List of newspapers in Scotland


== References ==


== External links ==
Official website