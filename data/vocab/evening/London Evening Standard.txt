The London Evening Standard (simply the Evening Standard before May 2009) is a local, free daily newspaper, published Monday to Friday in tabloid format in London.
It is the dominant local/regional evening paper for London and the surrounding area, with coverage of national and international news and City of London finance. In October 2009, the paper ended a 180-year history of paid circulation and became a free newspaper, doubling its circulation as part of a change in its business plan.


== History ==
The newspaper was founded by barrister Stanley Lees Giffard on 21 May 1827, as the Standard. The early owner of the paper was Charles Baldwin. Under the ownership of James Johnstone, The Standard became a morning paper from 29 June 1857. The Evening Standard was published from 11 June 1859. The Standard gained eminence for its detailed foreign news, notably its reporting of events of the American Civil War (1861–1865), the Austro-Prussian War of 1866, and the Franco-Prussian War of 1870, all contributing to a rise in circulation.


=== Lebedev takeover ===
On 21 January 2009, Russian businessman and former KGB agent Alexander Lebedev and son Evgeny Lebedev, now the newspaper's owner, agreed to purchase control of the newspaper. The paper was formerly published by Associated Newspapers Ltd., a division of Daily Mail and General Trust, which also publishes the Daily Mail, The Mail on Sunday, and Metro, a free morning paper distributed from Monday to Friday at London stations.
In November 2009, it was announced that the London Evening Standard would drop its midday "News Extra" edition from 4 January 2010 with the first edition being the West End Final, available from 2pm. One edition of 600,000 copies would be printed starting at 12:30pm, ending 3am starts for journalists and the previous deadline of 9am for the first edition; twenty people were expected to lose their jobs as a result.
Previously there were three editions each weekday, excluding Bank holidays. The first, "News Extra", went to print at 10:00am and was available around 11am in central London, slightly later in more outlying areas (such as Kent). A second edition, "West End Final", went to print at 3pm, and the "Late Night Final" went to print at 5pm and was available in the central area from about 6pm There was often considerable variation between the editions, particularly with the front page lead and following few pages, including the Londoner's Diary, though features and reviews stayed the same. The page changes were indicated by stars in the bottom left-hand corner of each page: two stars for the second edition, three stars for the third. In January 2000, circulation was increased to 900,000.


=== May 2009 relaunch ===
In May 2009, the newspaper launched a series of poster ads, each of which prominently featured the word "Sorry" in the paper's then-masthead font. These ads offered various apologies for past editorial approaches, such as "Sorry for losing touch". None of the posters mentioned the Evening Standard by name, although they featured the paper's Eros logo. Ex-editor Veronica Wadley criticised the "Pravda-style" campaign saying it humiliated the paper's staff and insulted its readers. The campaign was designed by McCann Erickson. Also in May 2009 the paper relaunched as the London Evening Standard with a new layout and masthead, marking the occasion by giving away 650,000 free copies on the day, and refreshed its sports coverage.


=== October 2009: freesheet ===
After a long history of paid circulation, on 12 October 2009 the Standard became a free newspaper, with free circulation of 700,000, limited to central London. In February 2010, a paid-for circulation version became available in suburban areas of London for 20p. The newspaper won the Media Brand of the Year and the Grand Prix Gold awards at the Media Week awards in October 2010. The judges said, "[the Standard has] quite simply ... stunned the market. Not just for the act of going free, but because editorial quality has been maintained, circulation has almost trebled and advertisers have responded favourably. Here is a media brand restored to health." The Standard also won the daily newspaper of the year award at the London Press Club Press Awards in May 2011.


== Editorial style ==
The newspaper's editor is Sarah Sands who replaced Geordie Greig following his departure to the Mail on Sunday in March 2012. Veronica Wadley was the newspaper's editor between 2002 and 2009. Max Hastings was editor from 1996 until he retired in 2002.
The Evening Standard, although a regional newspaper for London, also covers national and international news, though with an emphasis on London-centred news (especially in its features pages), covering building developments, property prices, traffic schemes, politics, the congestion charge and, in the Londoner's Diary page, gossip on the social scene. It also occasionally runs campaigns on local issues that national newspapers do not cover in detail.
It has a tradition of providing arts coverage. Its most well known visual art critic is Brian Sewell. Sewell is noted for his acerbic view of conceptual art, Britart and the Turner Prize and his views have attracted controversy and criticism in the art world. He has been described as "Britain's most famous and controversial art critic".


=== 2008 London mayoral election ===
During the 2008 London mayoral election the newspaper –  and particularly its correspondent Andrew Gilligan –  published articles in support of Conservative candidate Boris Johnson, including frequent front-page headlines condemning Ken Livingstone. This included the headline "Suicide bomb backer runs Ken's campaign".


=== 2010 general election ===
On 5 May 2010, the newspaper stated in an editorial that having supported Labour under Tony Blair, the newspaper would be supporting David Cameron and the Conservatives in the General Election, saying that "the Conservatives are ready for power: they look like a government in waiting."


=== 2015 general election ===
On 5 May 2015, an editorial stated that the newspaper would again be supporting David Cameron and the Conservatives in the 2015 General Election, saying that the Conservatives have "shown themselves to be good for London." The Newspaper did however also claim "there may be good tactical reasons to vote Liberal Democrat."


== Freesheet and supplements ==

On 14 December 2004, Associated Newspapers launched a freesheet edition of the Evening Standard called Standard Lite to help boost circulation. This had 48 pages, compared with about 80 in the main paper, which also had a supplement on most days.
In August 2006, the freesheet was relaunched as London Lite. It was designed to be especially attractive to younger female readers, and featured a wide range of lifestyle articles, but less news and business news than the main paper. It was initially only available between 11.30 a.m. and 2.30 p.m. at Evening Standard vendors and in the central area, but later became available in the evening from its street distributors. With the sale of the Evening Standard, but not the London Lite, to Alexander Lebedev on 21 January 2009, the ownership links between the Standard and the Lite were broken.
On Fridays, the newspaper includes a free glossy lifestyle magazine, ES, and the circulation was increased to 350,000 in September 2014. This has moved from more general articles to concentrate on glamour, with features on the rich, powerful and famous. On Wednesdays, readers can pick up a free copy of the Homes & Property supplement, edited by Janice Morley, which includes London property listings as well as articles from lifestyle journalists including Barbara Chandler, Katie Law and Alison Cork.
An entertainment guide supplement Metro Life (previously called Hot Tickets) was launched in September 2002. This was a what's-on guide with listings of cinemas and theatres in and around London, and was given away on Thursdays. It was discontinued on 1 September 2005.
The paper also supplies occasional CDs and DVDs for promotions. It also give Londoners a chance to win exclusive tickets to film premieres and sports tournament tickets, such as the Wimbledon Ladies Singles Final.


== In popular culture ==
In the apocalyptic horror film 28 Days Later (2002), directed by Danny Boyle, the newspaper is one of the last printed papers in the UK, informing Londoners about a deadly epidemic sweeping the country as well as that the city is to be evacuated and the Prime Minister has declared a state of emergency.


== Websites ==
The newspaper's This Is London website carries some of the stories from the Evening Standard and promotions, reviews and competitions. It also includes a number of blogs by Evening Standard writers, such as restaurant critic Charles Campion, theatre critic Kieron Quirke and music critic David Smyth. A separate website contains images of each page of the print edition (two versions) and supplements.


== Editors ==


== Chairman ==


== References ==


== External links ==

Evening Standard stories from the Evening Standard
London Evening Standard E-edition (requires registration to view)