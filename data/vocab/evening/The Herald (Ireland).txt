The Herald is a mid-market tabloid evening newspaper published in Dublin, Ireland by Independent News & Media. It is published Monday-Saturday, and has three editions — City Edition, City Final Edition and National Edition. The paper was previously known as the Evening Herald, with its name change coming in 2013.


== History ==
Until November 2000, the Evening Herald was both produced and pressed in Independent House on Middle Abbey St, Dublin 1. The monochrome printing facility in the basement of this building was then retired, and the paper is now printed in full colour at a purpose-built plant in Citywest, along with the Irish Independent, the Sunday Independent and various other regional newspapers owned by Independent News & Media. In 2004, production of the paper was moved from Independent House to a new office on Talbot Street and the paper's old home was sold to the neighbouring department store, Arnotts, for an estimated €26 million.
The life of Herald music critic Chris Wasser was threatened by fans of boy band The Wanted in 2012 following the publication of his review of their gig in Dublin.
In March 2013, it was reported that the Evening Herald was to rebrand as The Herald. It is also set to become a morning rather than an evening newspaper.


== Format and circulation ==
The newspaper has been variously marketed as both a Dublin local newspaper and a national newspaper. It is available throughout most of the country. It has an extensive classified ad section. On Mondays it comes with a free soccer supplement called Striker which is marketed as "the real voice of amateur and schoolboy football". The Dubliner magazine comes free inside the paper on Thursdays.
According to the Audit Bureau of Circulations, it had an average daily circulation of 61,179 during the first six months of 2012.
Circulation then further declined to 58,826 for the period July to December 2012. This represented a fall in circulation of 6% on a year-on-year basis.
Circulation then further declined to 49.512 for the period July to December 2014. This represented a fall in circulation of 12% on a year-on-year basis.
On 10 October 2005, a free version of the Evening Herald, published in the mornings and entitled Herald AM, began distribution, as a defensive measure against the Daily Mail and General Trust-owned Metro, launched on the same date. It joined with another morning freesheet Metro to become the Metro Herald.


== References ==


== External links ==
Official website
Reviews from the Evening Herald