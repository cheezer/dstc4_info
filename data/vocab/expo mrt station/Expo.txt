Expo may refer to:
Expo (exhibition), short for "exposition", and also known as world's fair
A trade fair, an exhibition where companies in an industry showcase and demonstrate their latest products
Computer expo, a trade fair focused on computers and electronics

Singapore Expo, convention and exhibition venue
Expo MRT Station, part of the Singapore MRT Changi Airport Extension
Expo (magazine), an anti-fascist magazine
Expo (album), a 2005 album by Robert Schneider/Marbles
Expo (Stockhausen) (1970), a composition for three players by Karlheinz Stockhausen
Expo Channel, a home shopping channel in Australia
Montreal Expos, a baseball team located in Montreal from 1969 to 2004
Windows Live Expo, a social classifieds web site
Expo Dry Erase Products, Sanford Corp's brand of dry erase markers
Expo Design Center, a chain of high end home furnishing and decor stores owned by The Home Depot
LG eXpo, a mobile phone


== See also ==
Exposition (disambiguation)
Exhibition (disambiguation)