This is a list of the extreme points and extreme elevations in Russia.
The northernmost and easternmost points of Russia coincide with those of Eurasia (both for the mainland and including the islands).
The extreme points of the Soviet Union were identical, except that the southernmost point of the Soviet Union was Kushka in Turkmenistan, and the extreme elevation was the Communism Peak in Tajikistan, at 7,495 m.
From 1799 until 1867 the easternmost point of the Russian Empire was located in North America, on the border between Russian Alaska and British North America, which superimposed on the then International Date Line. The westernmost point of the Empire was located at Pyzdry in Congress Poland from 1815 until its occupation by German and Austro-Hungarian troops in 1915. Kushka in present-day Turkmenistan had been the southernmost point of the Empire since 1885. Again, until the Alaska Purchase, the peak of Mount McKinley was the extreme point of elevation in the Empire, at 6,194 m above sea level.
The remaining extreme points were the same as the extreme points of the Soviet Union.


== Extreme coordinates ==
Including islands and exclaves
Northernmost point — Cape Fligely, Franz Josef Land, Nenets Autonomous Okrug (81°50′35″N 59°14′22″E)
Southernmost point — Mount Bazardyuzyu, Republic of Dagestan (41°13′14″N 47°51′28″E)
Westernmost point — Narmeln, Vistula Spit, Kaliningrad Oblast (19°38'E)
Easternmost point1 — Big Diomede Island, Chukotka Autonomous Okrug (65°47′N 169°01′W)
Contiguous mainland only
Northernmost point — Cape Chelyuskin, Krasnoyarsk Krai (77°43'N)
Southernmost point — Mount Bazardyuzyu, Republic of Dagestan (41°12'N)
Westernmost point — near Lavry, Pskov Oblast (27°19'E)
Easternmost point1 — Cape Dezhnev (East Cape), Chukotka Autonomous Okrug (169°40'W)
Towns and cities

Northernmost — Pevek, Chukotka Autonomous Okrug (69°42′N)
Southernmost — Derbent, Republic of Dagestan (42°04′N)
Westernmost — Baltiysk, Kaliningrad Oblast (19°55′E)
Easternmost — Anadyr, Chukotka Autonomous Okrug (177°30′E)
Permanent settlements
Northernmost — Dikson (73°30′N)
Southernmost — Kurush, Republic of Dagestan (41°16′N)
Westernmost — Baltiysk, Kaliningrad Oblast (19°55′E)
Easternmost1 — Uelen, Chukotka Autonomous Okrug (169°48′W)
1according to the path of the International Date Line.


== Elevation extremes ==

Lowest point: Caspian Sea level: −28 m
Highest point: west summit of Mount Elbrus: 5,642 m


== See also ==
Extreme points of Earth
Geography of Russia