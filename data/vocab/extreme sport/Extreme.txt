Extreme or xtreme may refer to:


== Science and mathematics ==


=== Mathematics ===
Extreme point, a point in a convex set which does not lie in any open line segment joining two points in the set
Maxima and minima, extremes on a mathematical function


=== Science ===
Extremophile, an organism which thrives in or requires "extreme"
Extremes on Earth
List of extrasolar planet extremes


== Politics ==
Extremism, political ideologies or actions deemed outside the acceptable range


== Business ==
Xtreme Drilling and Coil Services, an oilfield rig operator
Extreme Networks, a California-based networking hardware company
Extreme Records, an Australia-based record label
Extreme Associates, a California-based adult film studio


== Sports and entertainment ==


=== Sports ===
Los Angeles Xtreme, a defunct XFL franchise
Xtreme Turf, an artificial turf system


=== Music ===
Extreme (band), an American band
Extreme (album), an album by Extreme

Xtreme (group), a bachata duo
Xtreme (album), an album by Xtreme

Extremes (album), an album by Collin Raye
X-Treme, a stage name of Italian singer and producer Agostino Carollo


=== Entertainment ===
Extreme Sports Channel A global TV channel dedicated to extreme sports and youth culture
Extreme (1995 TV series), an American action series
Extreme (Travel Channel series), a US television series
"Extreme" (CSI: Miami), a season two episode of CSI: Miami


== Literature ==
Extremes (novel), by Kristine Kathryn Rusch
Extreme Studios, a forerunner of the American comic book studio Image Comics
Adam X the X-Treme, a character in the Marvel Comics universe
Extreme, an autobiography by Sharon Osbourne


== Other uses ==
Chevrolet Extreme, a name for the Chevrolet S-10 pickup truck
Xtreme Mod, a peer-to-peer file sharing client for Windows


== See also ==
Extremities (disambiguation)
Lunatic fringe (disambiguation)