EZ Rock is a brand of adult contemporary music radio stations heard primarily in Canada. The branding was originally created by Telemedia in the mid-1990s for its adult contemporary stations, and based on the call sign of its former Toronto flagship CJEZ (now adult hits-formatted CHBM).
Ownership of the EZ Rock brand would pass from Telemedia to Standard Broadcasting in 2002, then to Astral Media in 2007, and then to Bell Media in 2013.
On August 29, 2013, Rogers Media's EZ Rock stations in Northern Ontario re-branded to "KiSS"; these stations were former Standard stations that were acquired by Rogers in 2002. 
As of July 2013, the EZ Rock branding is heard on Bell Media-owned stations in British Columbia and Ontario.
The EZ Rock stations are generally programmed independently, although special Canadian content versions of shows such as Delilah and the John Tesh Radio Show are heard across the network. Some stations in smaller markets may also share voice-tracked programming in weekend and overnight slots.
Despite the pronunciation of the letter "Z" as "zed" in Canadian English, the name is pronounced "E-Zee Rock" (as in "Easy Rock") using the American English pronunciation of the letter, not "E-Zed Rock".


== EZ Rock stations by province ==
The following radio stations in Canada use the "EZ Rock" branding, and are all owned by Bell Media Radio:


=== British Columbia ===
Kelowna - CILK-FM
Summerland - CHOR-FM
Penticton - CKOR
Osoyoos - CJOR
Salmon Arm - CKXR-FM
Revelstoke - CKCR-FM
Golden - CKGR-FM
Invermere - CKIR
Trail - CJAT-FM
Nelson - CKKC-FM
Prince Rupert - CHTK-FM
Terrace - CFTK
Kitimat - CKTK-FM


=== Ontario ===
St. Catharines - CHRE-FM


== Former EZ Rock stations ==
All stations are owned by Bell Media Radio, unless otherwise specified.


=== Alberta ===
Edmonton - CFMG-FM (identified as EZ Rock from 1990s until 2011)


=== New Brunswick ===
Fredericton - CIBX-FM (identified as EZ Rock from 2000 until 2005)
Woodstock - CJCJ-FM (identified as EZ Rock from 1999 to 2009)


=== Nova Scotia ===
Truro - CKTO-FM (identified as EZ Rock from 2001 until 2002)


=== Ontario ===
Orillia - CICX-FM (identified as EZ Rock from 1996 until 2002; former Standard/Rogers station owned by Larche Communications)
Ottawa - CJOT-FM (identified as EZ Rock from May 27, 2010 until June 30, 2011, former flagship station; now owned by Corus Radio)
Toronto - CHBM-FM (identified as EZ Rock from 1995 to 2009 using the CJEZ-FM callsign, former flagship station; now owned by Newcap Radio)
London - CIQM-FM (identified as EZ Rock from 1996 to August 17, 2012)
North Bay - CHUR-FM (identified as EZ Rock from 1999 to August 29, 2013; owned by Rogers Media)
Sault Ste. Marie - CHAS-FM (identified as EZ Rock from 1999 to August 29, 2013; owned by Rogers Media)
Sudbury - CJMX-FM (identified as EZ Rock from 1999 to August 29, 2013; owned by Rogers Media)
Timmins - CKGB-FM (identified as EZ Rock from 2001 to August 29, 2013; owned by Rogers Media)


== References ==
Bell Media: EZ Rock