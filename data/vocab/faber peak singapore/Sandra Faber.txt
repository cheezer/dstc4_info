Sandra Moore Faber (born 1944) is a University Professor of Astronomy and Astrophysics at the University of California, Santa Cruz, and works at the Lick Observatory. She has made important discoveries linking the brightness of galaxies to the speed of stars within them and was the co-discoverer of the Faber–Jackson relation. Faber was also instrumental in designing the Keck telescopes in Hawaii.


== Education ==
In 1966, Faber obtained a B.A., with high honors, in physics from Swarthmore College. She went on to receive her Ph.D. in Astronomy from Harvard University in 1972.


== Professional work ==
Faber was the head of a team (known as the Seven Samurai) that discovered a mass concentration called "The Great Attractor". She was also the Principal Investigator of the Nuker Team, which used the Hubble Space Telescope to search for supermassive black holes at the centers of galaxies. Faber was deeply involved in the initial use of Hubble as a member of the WFPC-1 camera team, and was responsible for diagnosing the spherical aberration in the Hubble primary.
Faber says, "I hope that you take time to contemplate the implications of what you are studying. This is one of the rare opportunities to think about where the human race is going." Indeed, on the importance of astronomical knowledge, Faber states that "it's astronomy that puts us in perspective; it tells us where we come from, and cosmically, where we're going."
At UCSC she focuses her research on the evolution of structure in the universe and the evolution and formation of galaxies. In addition to this, she led the development of the DEIMOS instrument on the Keck telescopes to obtain spectra of cosmologically distant galaxies. On August 1, 2012 she became the Interim Director of the University of California Observatories.
Sandra Faber is co-editor of the Annual Review of Astronomy and Astrophysics.


== Awards ==
Faber was elected to the National Academy of Sciences in 1985 and the American Philosophical Society on 29 April 2001. Faber received the Heineman Prize in 1985 and the Harvard Centennial Medal in 2006.
Sandra M. Faber received the 2009 Bower Award and Prize for Achievement in Science from The Franklin Institute for three decades of research on the formation and evolution of galaxies, and for her altruistic dedication to building new tools for the astronomy community. Her research revolutionized the way cosmologists understand and model the universe.
In May, 2012, she received the Bruce Medal from the Astronomical Society of the Pacific. In September 2012, Faber received the Karl Schwarzschild Medal, which is awarded by the German Astronomical Society.
In February, 2013 she received the National Medal of Science from President Barack Obama.


== See also ==
Faber–Jackson relation


== References ==


== External links ==
Dr. Faber's page @ UCSC
See video of Dr. Faber @ Meta-Library.net
UC Santa Cruz's biography of Sandra Faber
Video of Faber explaining How Galaxies Were Cooked from the Primordial Soup on YouTube, from the Silicon Valley Astronomy Lectures
Oral History interview transcript with Sandra M. Faber 31 July 2002, American Institute of Physics, Niels Bohr Library and Archives
Video of Faber talking about her work, from the National Science & Technology Medals Foundation
Photographs of Sandra Faber from the UC Santa Cruz Library's Digital Collections