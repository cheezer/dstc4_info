Fairmont State University is a public university located in Fairmont, West Virginia, United States. Enrollment of the university is about 4,500 and offers master's degrees in business, education, teaching, architecture, and criminal justice in addition to 90 baccalaureate programs. 


== History/name changes ==
Fairmont State was founded as a private institution in 1865 in the basement of the Methodist Protestant Church at 418 Quincy Street. It was known as the West Virginia Normal School at Fairmont and was dedicated to educating teachers. March 9, 1868, it was purchased by the state from the Regency of the West Virginia Normal School which had been formed in 1866. With this purchase, this private normal school became a branch of the State Normal School at Marshall College.
From 1868 to 1892, the school was known variously as Fairmont Normal School, the Fairmont Branch of the West Virginia Normal School, the Branch of the West Virginia Normal School at Fairmont, a branch of the West Virginia State Normal School at Marshall College, but most commonly as Fairmont State Normal School. By 1892 the designation of "branch" had fallen into disuse by FSNS. It was renamed Fairmont State Teachers College in 1931 and Fairmont State College in 1943. On April 7, 2004, Governor Bob Wise signed legislation changing its name to Fairmont State University.
The Fairmont Normal School Administration Building was listed on the National Register of Historic Places in 1994.


== Community and Technical College ==
In 1974, a community college component was founded. This became independently accredited as the Fairmont State Community and Technical College in 2003. In 2006 Fairmont State was given direction by the state to split with the community and technical college, which then became known as Pierpont Community and Technical College. While both institutions still operate on the Fairmont campus, they are recognized as independent institutions and offer completely separate degree programs; Pierpont focuses more on 2-year technical associate's programs, while Fairmont State's main focus is four-year baccelaureate degrees and master's programs.


== Athletics ==
Fairmont State's athletic teams, known as the Falcons, compete in the Mountain East Conference in National Collegiate Athletic Association Division II.


== Honor societies ==
Alpha Phi Sigma (Criminal Justice)
Alpha Psi Omega (Dramatics) founded at the college in 1925 by professor Paul F. Opp.
Beta Beta Beta (Biology)
Delta Sigma Rho (Forensics)
Epsilon Pi Tau (Technology)
Family & Consumer Science Honor Society
Kappa Delta Pi (Education)
Kappa Kappa Psi (Band)
Kappa Pi (Art)
Nursing Honorary
Phi Alpha Theta (History)
Phi Theta Kappa
Pi Gamma Mu (Social Science)
Pi Sigma Alpha (Political Science)
Psi Chi (Psychology)
Sigma Alpha Iota (Music)
Sigma Tau Delta (English)
Society for Collegiate Journalists


== Social organizations ==
Inter-Panhellenic Council
Inter-Fraternity Council
Women’s Panhellenic Council
Student Graphics Organization
Alpha Eta Rho
Alpha Sigma Tau
Delta Xi Omicron (local)
Delta Zeta
Sigma Sigma Sigma
Phi Sigma Phi
Tau Beta Iota (local)
Tau Kappa Epsilon
Student Accountant Society
Fairmont State University Model United Nations


== Notable alumni ==
Wendell R. Beitzel, member of Maryland House of Delegates.
George C. Edwards, member of Maryland State Senate
Leroy Loggins, American professional basketball player in Australia
Herbert Morrison, radio reporter whose voice is heard in the footage of the Hindenburg Disaster.
Ira E. Robinson, West Virginia politician and judge, first chairman of the Federal Radio Commission
Jenna Stone, Fulbright Scholar, Author, Radio Personality, Actor
Richard Louis Skinner, Inspector General, U.S. Department of Homeland Security
Bill Stewart, former Football Coach of West Virginia University


== Notable faculty ==
Ruth Ann Musick, noted folklorist and author


== References ==
^ Turner, Dr. William P., "A Centennial History of Fairmont State College", Fairmont State College, Fairmont, WV, 1970
^ "Marion County Architecture". Marion County Historical Society & Museum. Retrieved 6 May 2015. 
^ "National Register Information System". National Register of Historic Places. National Park Service. 2010-07-09. 


== External links ==
Official university site
Official Pierpont Community & Technical College site
Official athletics site


== See also ==
Fairmont, West Virginia
Helen Whitney
Luella Mundel