NTUC FairPrice Co-Operative (Chinese: 新加坡职工总会平价合作社, Tamil: NTUC FairPrice கூட்டுறவு) is a supermarket chain based in Singapore and the largest in the country. The company is a co-operative of the National Trades Union Congress or NTUC. The group has 100 supermarkets across the island, with over 50 outlets of Cheers convenience stores island-wide.
NTUC FairPrice has partnered with ExxonMobil to run several stations with a FairPrice branding at the minimarts at their stations. The supermarket has a slogan known as Singapore's very own.


== History ==

NTUC FairPrice was first established in 1973 as NTUC Welcome Supermarket in Toa Payoh, to solve the rising oil and daily prices then due to inflation. The then prime minister Lee Kuan Yew opened the first supermarket at Block 192, Toa Payoh Lorong 4, and it was the first of its kind. Around the same time, other unions such as the Singapore Industrial Labour Organisation and Pioneer Industries Employees Union also set up co-operatives to run supermarkets. The two organisations later merged in the early 1980s to form the Singapore Employees Co-operative (SEC).
In May 1983, due to competition, NTUC Welcome and SEC merged to form a larger co-operative which was known as NTUC FairPrice Co-Operative Limited.
The company started a central-distribution system to change the way goods were delivered to stores, to enhance efficiency. This was owned by another company, but in 1998, FairPrice took full ownership of the warehouse and distribution company. It was renamed Grocery Logistics of Singapore (GLS). In 2003, FairPrice opened a new 13,000-square-metre Fresh Food Distribution Centre. The refrigerated-distribution facility centralises the distributions of fresh and chilled products to all the outlets in the chain. It is the first supermarket retailer in Singapore to build, own and operate its own fresh-food distribution centre.


=== FairPrice Xpress ===

That same year, FairPrice tied up with oil company ExxonMobil to open FairPrice Express and Cheers convenience outlets at seven Esso and Mobil stations as a pilot project. The station will be run by NTUC FairPrice Co-operative, which sets the fuel prices and run the station's minimart. ExxonMobil will supply the fuel, and will sell its oil products inside the FairPrice-run stations. If this pilot project proves successful, all stations will be converted to either FairPrice Express or Cheers with the co-operative taking over the management of each station. ExxonMobil also announced that all the stations will be renamed "Esso" by year-end.


=== FairPrice Xtra ===

On 28 December 2006, FairPrice opened its largest branch in Singapore, the 7,150-square metre (77,000 square feet) FairPrice Xtra hypermarket, at Ang Mo Kio Hub. The new S$12 million outlet offers a wider range of goods than the typical FairPrice supermarket, and includes new lines like electronic products and fashion wear.


=== FairPrice Finest ===
In August 2007, FairPrice opened its upmarket outlet at Bukit Timah Plaza named FairPrice Finest, after five months of refurbishment. The move was to cater to the changing tastes of Singaporeans who are increasingly well-travelled. The 4,000 m² (43,100 ft²), two-storey outlet has an offering of products different from other FairPrice stores, and also features a Swiss-style delicatessen, a wine cellar and a European bakery. FairPrice has plans to open more Finest outlets.


=== Eco-friendly ===
In September 2009, FairPrice stepped up its commitment to environmentally friendly practices by launching Singapore's first eco-friendly supermarket name City Square Mall.
Designed with the basic principle of reduce, reuse and recycle in mind, this pilot eco-friendly FairPrice supermarket, measuring about 2,244 square metres, is a showcase of the latest eco-friendly solutions in grocery retailing. These include usage of recycled and biodegradable materials for store fixtures, energy-saving equipment and lighting, green products and services, as well as cutting edge "Green Technology".
Energy-saving features like LED lighting, T5 (28 watts) fluorescent tubes, energy saving refrigeration units, and motion sensor lighting control in office and staff areas help to reduce the energy consumption of FairPrice's eco-friendly store by about 30 per cent, compared to using standard equipment and fittings.
Other green touches include the use of non-toxic, environment friendly paint for walls and columns, ceiling boards and seats made from recycled materials like compressed board and mineral wood, and the use of wooden pallets made from recycled wood waste. Fixtures from other stores have also been reused and integrated into the overall design of the NTUC FairPrice City Square Mall branch.
The eco-friendly supermarket will also use 100% biodegradable shopping bags – environmentally friendly alternatives to the usual plastic bags, that are made from 40 per cent corn and yam starch, and 60 per cent polypropylene.
Apart from this, customers who bring their own reusable grocery bags not only get to use dedicated check-out lanes but also enjoy rebates through the ongoing Green Rewards programme. Since this programme was introduced in 2007, FairPrice has saved more than 43 million plastic bags and given out more than $500,000 in rebates to customers and these practices are an Eco Friendly Initiative which is in line with the strict 'International Standards' required by 'Eco Friendly Approved' www.ecofriendlyapproved.com and endorsed by 'Oceans 5' www.oceans-5.com .


== Regional expansion ==
FairPrice has ventured into the People's Republic of China and Vietnam to open supermarkets in various parts. It will be a joint venture with DBS Private Equity, New Hope Group, Silver Tie and Taiwan's Apex Group. The venture will be known as Nextmall, and will provide merchandising, management and logistics for a fee to Nextmart which is a China incorporated hypermarket. It has opened seven hypermarkets in China, with its first in Shaoxing, Zhejiang.


== Individual Outlets ==
Cheers by FairPrice – This 24-hour convenience store chain run by NTUC FairPrice was introduced in 1999. It offers similar facilities to rival 7-Eleven. Cheers can also be found at selected ExxonMobil stations around the island, in which NTUC FairPrice co-operative runs the station, instead of ExxonMobil itself.
FairPrice Express – A concept which can be found only at ExxonMobil stations, it is larger than Cheers and sells things like fresh produce, meat and seafood. It also sells more products meeting daily needs than a Cheers convenience store.
FairPrice Finest – This is a separate store offering up-market food supplies. It was officially opened in September 2007, and also it merged the former Liberty Market.
FairPrice Xtra – A Hypermarket Chain which combines a normal supermarket and the FairPrice Homemart in one store. It sells items such as electronics, clothing and household merchandise in addition to the regular supermarket items, and also it merged the former FairPrice Homemart.


== Membership programmes ==
Every year, FairPrice offers NTUC Union Members (NTUC Cardholders) and FairPrice shareholders dividends, along with cash-back rebates for all purchases made at FairPrice supermarkets island-wide, including FairPrice Xtra, FairPrice Homemart, FairPrice Finest and FairPrice Express – excluding Cheers outlets. Such payout rates are subject to approval at Annual General Meetings (typically in the month of August). In 2007, FairPrice declared rebates amounting to 4% of purchases of up to S$6,000 made in the last financial year, as well as a dividend of 5% and special dividend of 3%.
NTUC Plus! Cardholders (union), Plus! Cardholders (non-union) are entitled to 2 LinkPoints for every S$1 spent (for a minimum spending of S$20 in receipts issued on the same day) under the Plus! programme by NTUC Link.
In collaboration with OCBC Bank, FairPrice launched its FairPrice Plus membership programme in April 2007, offering banking solutions and promotions at all FairPrice supermarkets islandwide for all supermarket customers. For a minimum transaction of S$10, VISA payment is now accepted at all FairPrice supermarkets for the FairPrice Plus Card and the NTUC Plus Card (previously known as the NTUC-OCBC Visa Card) for NTUC Union Members.


== Notes and references ==


== External links ==
Official Website