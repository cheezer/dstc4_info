Far Eastern economic region (Russian: Дальневосто́чный экономи́ческий райо́н; tr.: Dalnevostochny ekonomichesky rayon) is one of twelve economic regions of Russia.

Bordering on the Pacific Ocean, the region has Komsomolsk-on-Amur, Khabarovsk, Yakutsk, and Vladivostok as its chief cities. Machinery is produced, and lumbering, fishing, hunting, and fur trapping are important. The Trans-Siberian Railroad follows the Amur and Ussuri Rivers and terminates at the port of Vladivostok.


== Composition ==
Amur Oblast
Chukotka Autonomous Okrug
Jewish Autonomous Oblast
Kamchatka Krai
Khabarovsk Krai
Magadan Oblast
Primorsky Krai
Sakha Republic
Sakhalin Oblast


== References ==