An automated fare collection (AFC) system is the collection of components that automate the ticketing system of a public transportation network - an automated version of manual fare collection. An AFC system is usually the basis for integrated ticketing.


== System description ==
AFC systems often consist of the following components (the "tier" terminology is common, but not universal):
Tier 0 - Fare media
Tier 1 - Devices to read/write media
Tier 2 - Depot/station computers
Tier 3 - Back office systems
Tier 4 - Central clearing house
In addition to processing electronic fare media, many AFC systems have equipment on vehicles and stations that accepts cash payment in some form.


=== Fare media ===
AFC systems originated with tokens or paper tickets dispensed by staff or from self-service vending machines. These have generally been replaced with magnetic stripe cards.
Since their introduction in 1997 with the Octopus card in Hong Kong, contactless smart cards have become the standard fare media in AFC systems, though many systems support multiple media types.
More recently, contactless smart cards from bank networks have been seen more frequently in AFC.
And Korea Smart Card,Co.(KSCC) has provided its AFC service overseas including New Zealand and Malaysia etc.
The success story of New Zealand case follows;
New Zealand developed “cost-effective” Smartcard based payment system for Wellington in 2008.
The uniqueness of this system is that KSCC serves as an ASP for the New targeted market, where settlement data are transferred back to the clearing center located in KSCC’s headquarters, and the settlement reports are provided 9AM following working day.
The bus solution being implemented now in Wellington and operating more than 400 buses is wholly consistent with the philosophy of quick boarding and focusing the bus driver’s attention on operating the bus safely, reliably and courteously. A state-of-the-art touch screen bus driver console handles the driver’s sign-on, dispensing of receipts to cash-paying customers and counting of riders who present paper passes or multi-ride tickets, and controlling the card validator for smartcard users.
The driver console which KSCC developed also contains the processing power for the bus’ fare payment devices that are located at both the front and rear doors. The card validators are attractive, highly visible card readers with simple displays and the ability to handle audible messages.


=== Devices to read/write media ===
These take numerous forms, including:
Ticket office terminals - where a media holder can purchase a right to travel from staff in an office, or enquire as to the value and travel rights associated with the media

Ticket vending machines - where a media holder can purchase a right to travel from a self-service machine, or enquire as to the value and travel rights associated with the media
Fare gate - often used in a train station so a media holder can gain access to a paid area where travel services are provided
Stand-alone validator - used to confirm that the media holds an appropriate travel right, and to write the usage of the media onto the media for later verification (e.g. by a conductor/inspector). Often used in proof-of-payment systems.
On-vehicle validator - used by a media holder to confirm travel rights and board a vehicle (e.g. bus, tram, train)
Inspector/conductor device - used by staff such as a conductor to verify travel rights
Unattended devices are often called "validators", a term which originated with devices that would stamp a date/time onto paper tickets to provide proof of valid payment for a conductor.


=== Depot/station computers ===
Used to concentrate data communications with devices in a station or bus depot. Common in older AFC systems where communication lines to upper tiers were slow or unreliable.


=== Back office ===
Servers and software to provide management and oversight of the AFC system. Usually includes:
Fare management - changing of fares and fare products
Media management - support for blacklisting of lost/stolen media
Reporting - periodic reports on performance of the AFC system, financial details and passenger movements


=== Clearing house ===
In environments where multiple system operators share common, interoperable media, a central system similar to those used in stock exchanges can be used to provide financial management and other services to the operators such as:
Clearing and settling of funds
Common reporting
Apportionment of revenue between operators


== Automated fare collection in the United States ==

The first faregates in United States were installed experimentally in 1964 at Forest Hills and Kew Gardens Long Island Rail Road stations in Queens; the first systemwide installation was on Illinois Central Railroad (IC) in 1965 for its busy Chicago commuter service (today’s Metra Electric.) Financed entirely from private funds, AFC was expected to reduce operating costs by decreasing on-board crew sizes and eliminating station agents at all but the busiest stations. Cubic’s IC system featured entry-exit swipes (NX) to enforce zonal fare structures, checks against fraud, used ticket collection, and ridership/revenue data collection capabilities. It served as a prototype for the San Francisco Bay Area Rapid Transit (BART), Washington Metropolitan Area Transit Authority (WMATA), and Philadelphia’s Port Authority Transit Corporation (PATCO) Lindenwold Line NX-zonal AFC systems. These railroad-style systems required complex computer data processing on faregates or remotely on a central computer, and thus were not suitable for buses. Similar systems are still in use on Japan and Taiwan’s commuter railroads, and the London Underground.
Metropolitan Atlanta Rapid Transit Authority (MARTA)’s desire for simpler AFC systems resulted in Duncan (traditionally a parking meter vendor) developing turnstile machines for entry-only subway fare collection. Chicago Transit Authority (CTA)’s ChicagoCard, Boston Massachusetts Bay Transportation Authority (MBTA)’s previous generation “T-Pass”, and New York City Transit (NYCT)’s MetroCard systems could all be considered MARTA’s 1977 system’s conceptual descendents.
Bus fareboxes had hitherto been much simpler devices, mechanically registering coins deposited on accumulating registration counters. Duncan’s 1973 “Faretronic” farebox was the first to electronically count coins and collect revenue/ridership data by fare class. Keene quickly followed suit, introducing a design meeting Urban Mass Transit Administration (UMTA) Section 15 reporting requirements, also collecting fuel consumption and bus mileage data. In New York, mechanical fareboxes were preferred for ease of maintenance until widespread deployment of Cubic’s MetroCard for buses in 1997. Venerable GFI fareboxes featuring magnetic pass readers requiring cash single fares lasted in Boston until Scheidt-Bachmann’s CharlieCard was introduced in 2006.


== Automated fare collection in Canada ==
Canada’s first public transit agency, the Toronto Street Railway Co., started in 1861 with a horse-drawn streetcar service but it was not until 1912 that the City of Toronto began deliberations on fare collection. It was not until 126 years later (in 1987) that Mississauga Transit became one of the first Transit Agencies in Canada to implement an Electronic Farebox. Since then, almost every major city in Canada has adopted use of electronic fare boxes.
Notably, Canada also produces fare collection devices for various transit agencies in North America. Fare Logistics Corp., located in Victoria, British Columbia, currently manufactures and develops high tech fare collection solutions. They are best-known for having the most modular and customizable products available on the market today with offerings such as the Voyager farebox and the Talisman.


== Examples ==
This is a list of a few notable AFC systems. (See List of smart cards for a comprehensive list of AFC and other systems based on contactless smart cards.)


== References ==