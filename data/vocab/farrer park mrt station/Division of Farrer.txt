The Division of Farrer is an Australian electoral division in the state of New South Wales. The division was created in 1949 and is named for William Farrer, agricultural scientist.
The division is located in the southern rural area of the state and includes Albury, Balranald, Broken Hill, Deniliquin and Jerilderie.
The sitting member, since the 2001 election, is Sussan Ley, a member of the Liberal Party of Australia.
It has always been a safe non-Labor seat, alternating between the Liberal Party and the National Party. It was held by three Cabinet ministers in succession, including Tim Fischer, leader of the National Party from 1993 to 2001 and Deputy Prime Minister during the first half of the Howard Government.


== Members ==


== Election results ==


== References ==


== External links ==
Division of Farrer - Australian Electoral Commission