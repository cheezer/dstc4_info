Farrer is a suburb in the Canberra, Australia district of Woden. The postcode is 2607.


== Name origin ==
Named for William James Farrer (1845–1906), a wheat-breeding pioneer, many of whose experiments were conducted at Lambrigg near Tharwa. The suburb's streets are named after agriculturalists, with the exception of Lambrigg Street, which was the name of Farrer's property in Tharwa.


== Location ==
It is next to the suburbs of Torrens, Mawson, Isaacs and the Canberra Nature Park of Farrer Ridge. It is bordered by Beasley Street, Athllon Drive and Yamba Drive. Located in the suburb is Farrer Primary School and neighbourhood oval, a Croatian Catholic parish centre, the Serbian Orthodox Church of St Sava, a small shopping centre, the Long Gully scout hall and the Goodwin Village for elderly citizens.


== General information ==
Farrer is a relatively big suburb for Canberra, with over 3,300 residents. It was named on 12 May 1966, after William James Farrer, who had lived in the area late in the 19th century, making a significant contribution to wheat-breeding in New South Wales by producing climate species, thus extending the wheat-belt and enabling the breeding of resistant wheat.
The suburb's size resulted in possession of its own oval, located next to the primary school, a scout hall for the local group named Long Gully, and three playgrounds at various locations. Pearce's Melrose High School and Wanniassa's Erindale College are only 2.4 and 2.7 kilometres away respectively. While the small convenience store is often frequented by locals, Farrer is in proximity to Southlands Shopping Centre at Mawson and Woden Plaza in Phillip, only 4.2 km away. The Canberra Hospital in Garran is nearer still, at 3.8 km.
The Farrer Ridge Nature Reserve, part of the Canberra Nature Park and the southern limit of the suburb, provides for bushwalking and stunning vistas.


== Geology ==
Deakin Volcanics green-grey and purple rhyodacite underlies the whole suburb. Deakin Volcanics cream rhyolite occurs on the top of Farrer Ridge. Quaternary alluvium can be found in the valley bottom.


== References ==