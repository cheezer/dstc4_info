Farrer may refer to


== People ==
Austin Farrer (1904–1968), English theologian, philosopher, and friend of C. S. Lewis
Henry Farrer (1844–1903), 19th century American artist
Joe Farrer (born 1962), member of the Arkansas House of Representatives
Matthew Farrer (1852–1928), English amateur footballer who appeared in 1875 and 1876 FA Cup Finals
Reginald Farrer (1880–1920), pioneering English botanist
Thomas Farrer, 1st Baron Farrer (1819–1899), English statistician
William Farrer (1845–1906), Australian agricultural scientist


== Places ==
Farrer, Australian Capital Territory, a suburb of Canberra, Australia, named for William Farrer


== Other ==
Farrer & Co, a well-known London law firm
the Division of Farrer, also named after William Farrer, is an electorate of the Australian House of Representatives in rural New South Wales
the Farrer hypothesis, a biblical theory
Farrer Hall, a residential college at Monash University
Farrer Memorial Agricultural High School, an agricultural school for boys near Tamworth, New South Wales, Australia