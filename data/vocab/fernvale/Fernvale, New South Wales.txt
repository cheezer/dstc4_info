Fernvale is a town located in north-eastern New South Wales, Australia, in the Tweed Shire.


== Demographics ==
In the 2011 Census the population of Fernvale is 238, 49.2% female and 50.8% male.
The median/average age of the Fernvale population is 42 years of age, 5 years above the Australian average.
81.2% of people living in Fernvale were born in Australia. The other top responses for country of birth were England 6.3%, India 1.3%, Germany 1.3%, Belgium 1.3%, Japan 1.3%.
87.8% of people speak English as their first language 1.7% Japanese, 1.3% Russian, 1.3% French, 1.3% Maltese, 1.3% Thai.


== References ==
^ Australian Bureau of Statistics (31 October 2012). "Fernvale (NSW) (State Suburb)". 2011 Census QuickStats. Retrieved 20 January 2014.