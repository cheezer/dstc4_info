Filipino Australians (Filipino: Pilipino-Australyano) are Australians of Filipino ancestry. Filipino-Australians are the fifth-largest subgroup of the Overseas Filipinos. According to the 2006 census, there are over 160,374 Filipino Australians. In Sydney, people born in the Philippines comprise 5.9% of the population in the City of Blacktown and it is the largest directly born ethnic group in Blacktown.


== Population ==

Currently Filipinos are the third largest Asian Australian immigrant group behind Vietnamese Australians and Chinese Australians, Females accounted for 65.5% of the Philippine community while males represented 34.5% of the Filipino Australian population. According to census data, 50.2 per cent of the Philippines-born were resident in New South Wales, followed by 21.6% in Victoria, 14.9% in Queensland and 5.2% in Western Australia.


== History ==
Filipinos were excluded from entering Australia under the White Australia policy. As a consequence, their numbers in Australia remained minimal; confined to descendants of those few Filipinos who had migrated to the north west pearling areas of Western Australia and the sugar cane plantations of Queensland prior to 1901; until the abolition of racially selective immigration policies in 1966. The 1901 census had recorded 700 Filipinos in Australia.

Martial law in the Philippines, declared by former Philippine president Ferdinand Marcos in 1972, and the renunciation of the White Australia policy made Australia an attractive destination for Filipino emigrants, particularly skilled workers. Many Filipinos also settled in Australia from the 1970s onward as either migrant workers or the spouses of Australian citizens. Marriages between Filipinos and Australians rose very sharply from 1978, peaked in 1986, and remained high as of 2000, despite a dip in the early 1990s. The 1980s were the period of the greatest Filipino immigration, with 1987-1988 being the peak year.


== Notable people ==


=== Philippine-born ===
Mig Ayesa, theatre actor and rock vocalist
Merlinda Bobis, writer
Kathleen de Leon Jones, former Hi-5 member
Natalie Jackson Mendoza, actress (Hotel Babylon)
Rose Porteous, socialite
Israel Cruz, singer
RJ Rosales, actor and musical theatre
Chris Cayzer, actor and singer
Felino Dolloso, theatre actor
Jim Paredes, singer, song writer, member of APO Hiking Society trio


=== Filipino ancestry ===
Shey Bustamante, actress and model
Chris Cayzer, actor and singer
Kate Ceberano, singer
Anne Curtis, actress and model
Jasmine Curtis-Smith, actress and model
Rod Davies, wing for Queensland Reds Super 15 team
Kylie Padilla, actress and model
Queenie Padilla, actress and model
Jason Day, golfer
Ezekiel Dimaguila, reality show contestant
Kevin Gordon, Gold Coast Titans Rugby League player
Michael Letts, rugby player
Bobby Morley, actor from Home and Away TV series
Chad Peralta, singer
James Reid, actor, singer and dancer
Flip Simmons, theatre actor and musician
Craig Wing, Australian Rugby League player (South Sydney Rabbitohs)
Melissa De Leon, film publicist
Grace Arena, film marketing executive and President of the Passions fan club
Iain Ramsay, professional association footballer. Has chosen to represent the Philippines national football team at International level.


== See also ==

Australia–Philippines relations


== References ==


== External links ==
The Philippine Consulate General in Sydney
Embassy of the Philippines in Canberra