The little finger, often called the pinky in American English, pinkie in Scottish English (from the Dutch word pink, "little finger"), or fifth digit in medicine, is the most ulnar and usually smallest finger of the human hand, opposite the thumb, next to the ring finger.


== Muscles ==
There are nine muscles that control the fifth digit: Three in the hypothenar eminence, two extrinsic flexors, two extrinsic extensors, and two more intrinsic muscles:
Hypothenar eminence:
Opponens digiti minimi muscle
Abductor minimi digiti muscle (adduction from third palmar interossei)
Flexor digiti minimi brevis (the "longus" is absent in most humans)

Two extrinsic flexors:
Flexor digitorum superficialis
Flexor digitorum profundus

Two extrinsic extensors:
Extensor digiti minimi muscle
Extensor digitorum

Two intrinsic hand muscles:
Fourth lumbrical muscle
Third Palmar interosseous muscle

Note: the dorsal interossei of the hand muscles do not have an attachment to the fifth digit


== Cultural significance ==


=== Gestures ===

A pinky swear or pinky promise is made when a person wraps one of their pinky fingers around the other person's pinky and makes a promise. Traditionally, it's considered binding, and the idea was originally that the person who breaks the promise must cut off their pinky finger. In a similar vein, among members of the Japanese yakuza (gangsters), the penalty for various offenses is removal of parts of the little finger (known as yubitsume).


=== Rings ===
The Iron Ring is a symbolic ring worn by most Canadian engineers. The Ring is a symbol of both pride and humility for the engineering profession, and is always worn on the little finger of the dominant hand.
In the United States the Engineer's Ring is a stainless steel ring worn on the fifth finger of the working hand by engineers that belong to the Order of the Engineer and have accepted the Obligation of an Engineer.


== References ==


== See also ==
Fifth metacarpal bone - the bone in the hand proximal to the little finger
Pinky ring
Pinky swear - a type of oath involving the little finger
Yubitsume - a Japanese ritual of apology by amputation of the little finger
Red string of fate - a Japanese belief that soulmates are bound by a string attached to the little finger