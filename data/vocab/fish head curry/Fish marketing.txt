Fish marketing is the marketing and sale of fish products.


== Live fish trade ==

The live fish trade is a global system that links fishing communities with markets, primarily in Hong Kong and mainland China. Many of the fish are captured on coral reefs in Southeast Asia or the Pacific Island nations.


== Shrimp marketing ==

Most shrimps are sold frozen and are marketed in different categories; the main factors for categorization are presentation, grading, colour, and uniformity. 


== Fish markets ==

A fish market is a marketplace used for marketing fish products. It can be dedicated to wholesale trade between fishermen and fish merchants, or to the sale of seafood to individual consumers, or to both. Retail fish markets, a type of wet market, often sell street food as well.


== Chasse-marée ==

The fundamental meaning of un chasse-marée was "a wholesale fishmonger", originally on the Channel coast of France and later, on the Atlantic coast as well. He bought in the coastal ports and sold in inland markets. However, this meaning is not normally adopted into English. The name for such a trader in Britain, from 1500 to 1900 at least, was 'rippier'.


== See also ==
List of harvested aquatic animals by weight


== Notes ==


== References ==
Young, J A and Muir, J F (2002) Handbook of Fish Biology and Fisheries, Chapter 3, Marketing fish. Blackwell Publishing. ISBN 0-632-06482-X