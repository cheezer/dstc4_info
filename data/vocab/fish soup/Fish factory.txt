A fish factory, also called a fish plant, fish processing facility, is a facility where fish processing is performed. Fish factories range in the size and range of species of fish they process. Some species of fish, such as mackerel and herring, and can be caught at sea by large pelagic trawlers and offloaded to the factory within a few days of being caught. Or the fish can be caught by freezer trawlers that freeze the fish before providing it to factories, or by factory ships which can do the processing themselves on board. Some fish factories have fishing vessels catching fish for them at a given times of the year. This is to do with quotas and seasons conflicting how much and when the fish can be landed.


== Gallery ==


== See also ==
Cannery Row
Factory ship
Fish company
Gulf of Georgia Cannery
List of harvested aquatic animals by weight


== References ==
Garrity-Blake, Barbara J (1994) The Fish Factory: Work and Meaning for Black and White Fishermen of the American Menhaden Industry University of Tennessee Press. ISBN 978-0-87049-856-5
http://p2pays.org/ref/24/23296/f_chp2.pdf
http://emagazine.com/view/?507
http://www.mdi-nepal.org/case4.html
http://dare.co.in/opportunities/agriculture-rural/fresh-water-fish-farming.htm


== External links ==
The fish factory