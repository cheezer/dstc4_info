George Martin Lane (December 24, 1823 – June 30, 1897)  was an American scholar.


== Life and career ==
He was born at Charlestown, Massachusetts. He graduated in 1846 at Harvard, and in 1847-1851 studied at the universities of Berlin, Bonn, Heidelberg and Göttingen. In 1851 he received his doctor's degree at Göttingen for his dissertation Smyrnaeorum Res Gestae et Antiquitates, and on his return to America he was appointed University Professor of Latin in Harvard College.
From 1869 until 1894, when he resigned and became professor emeritus, he was Pope Professor of Latin in the same institution. His Latin Pronunciation, which led to the rejection of the English method of Latin pronunciation in the United States, was published in 1871.
His Latin Grammar, completed and published by Professor Morris H. Morgan in the following year, is of high value. Lane's assistance in the preparation of Harper's Latin lexicons was also invaluable. He wrote English light verse with humor and fluency, and two of his efforts Jonah or In the Black Whale at Ascalon and the Ballad of the Lone Fish Ball became famous as songs after being set to music.
Upon Lane's retirement in 1894, Harvard granted him an honorary degree as well as the first pension it had ever granted a faculty member; which, according to Lane, was enough to support him for the rest of his life.


== "The Lone Fish Ball" ==
In 1855 while living at Cloverden in Cambridge, Massachusetts, Lane wrote the song "The Lone Fish Ball"; after decades as a staple of Harvard undergraduates, it was modernized into the popular hit "One Meat Ball".
According to Morgan, the song is based upon an actual experience of Lane's at a restaurant in Boston, although the reality involved a half-portion of macaroni, rather than a fish ball. The song goes on to relate the impoverished diner's embarrassment at the hands of a disdainful waiter. After becoming popular among Harvard undergraduates, it was translated into a mock Italian operetta, "Il Pesceballo", by faculty members Francis James Child, James Russell Lowell and John Knowles Paine, set to a pastiche of grand opera music, and performed in Boston and Cambridge to raise funds for the Union army.
In 1944, the song was revived by Tin Pan Alley songwriters Hy Zaret and Lou Singer in a more bluesy format as "One Meat Ball", and the recording by Josh White became one of the biggest hits of the early part of the American folk music revival. Over the years, it was recorded by The Andrews Sisters, Bing Crosby, Jimmy Savo, Lightnin' Hopkins, Lonnie Donegan, Dave Van Ronk, Ry Cooder, Washboard Jungle, Tom Paxton, Shinehead, and Ann Rabson, among many others.


== References ==
^ a b c d e f g "Song for Hard Times", Harvard Magazine, May–June 2009
^ Il pesceballo, opera in one act History and libretto of Il Pesceballo, including history, lyrics, and music of "The Lay of the One Fishball"
^ "100-Year-Old Hit" Time Magazine April 8, 1945
 This article incorporates text from a publication now in the public domain: Chisholm, Hugh, ed. (1911). Encyclopædia Britannica (11th ed.). Cambridge University Press.