Fab Five may refer to:
Fab Five (University of Michigan), the 1991 University of Michigan men's basketball freshman recruiting class
The Fab Five (film), a 2011 documentary on the above group

Duran Duran, an English rock band dubbed the Fab Five in comparison to the Fab Four of The Beatles
The five stars of the reality television series Queer Eye for the Straight Guy, often referred to as the Fab Five
FAB 5, a Greek reality TV show
Fab Five: The Texas Cheerleader Scandal, a 2008 movie
Fab Five Freddy (born 1959), an American hip hop historian, hip hop pioneer and former graffiti artist


== See also ==
Fabulous Five (Kentucky Wildcats), nickname of the 1948 Kentucky Wildcats NCAA Championship team
Fab Four
Famous Five (disambiguation)