Flower Hospital is a public hospital in Sylvania, Ohio that is part of the ProMedica Health System. This level III trauma center has 1500 health care professionals caring for patients in any of the 279 beds in the premises.


== History ==
In memory of his late wife, Ellen, Stevens Warren Flower took up the task of building Flower Hospital in 1910. It was originally built near his home in downtown Toledo, Ohio, but it was later relocated to Sylvania, Ohio in 1975.


== Amenities ==
The hospital has a cafeteria with a Subway restaurant, a Starbucks coffee shop, two gift shops, internet access, a library, and pastoral services.


== References ==


== External links ==
Flower Hospital