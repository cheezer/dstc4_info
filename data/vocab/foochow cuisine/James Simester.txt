James Simester (Chinese: 沈雅各; Foochow Romanized: Sīng Ngā-gáuk; Pinyin: Shěn Yǎgè; February 18, 1871 - October 19, 1905) was an American Methodist missionary and educator to Foochow, China.


== Life ==
Born on February 18, 1871 in Huntington, Staffordshire, England, James Simester was converted at age fourteen and called to preach when sixteen. He received A.B. from Baldwin University in 1893, and A.M. in 1901, D.D. in 1904, and B.D. from Drew Theological Seminary in 1896.
He entered the Newark Conference in 1896, and had several appointments (1890 in Strongsville, Ohio, 1891 and 1892 in Litchfield, Ohio and Whippany, New Jersey) before transferring to the Foochow Conference in China in 1896. He married Winifred Smack (1875 - 1952) in Madison, New Jersey on August 6, 1896, and the couple arrived in Foochow on September 27, 1896.
From 1896 to 1899 James Simester taught in the Anglo-Chinese College, Foochow (福州英华书院) and from 1899 to 1901 was the acting president and from 1901 to 1904 the president. He was president of S.L. Baldwin School of Theology from 1904 to 1905. He also worked as the editor of the Educational Department of the Methodist Forum from 1902 to 1903, and the editor of Sunday-school Literature, Foochow Conference, from 1899 to 1903.
James Simester died of Dengue fever on October 19, 1905 in Foochow. His widow and four children went back to America and resided at Delaware, Ohio. His daughter, Edith Winifred Simester, returned to Foochow and worked here as a missionary in the 1930s and 40s.


== References ==