The Ancient footprints of Acahualinca (Spanish pronunciation: [akawaˈliŋka]; Spanish: Huellas de Acahualinca) exist in Managua, Nicaragua near the southern shore of Lake Managua. The region was once called "El Cauce". The tracks are fossil Late Holocene human footprints left behind in volcanic ash and mud, which solidified about 2,120±120 years ago, shortly after the group of up to 15 people passed by.
It is sometimes reported that the people were running to escape from a volcanic explosion, but the distance between the footprints indicates a walking gait. Fossilized footprints of several animals are also present, but the fact that they intersect the human footprints shows they were not traveling with the people.


== Scientific analysis of the footprints ==
In 1874, construction workers discovered the footprints. The United States medical doctor and archaeological collector, Earl Flint, brought the footprints to the attention of the international science community and media in 1884.
The Carnegie Institution of Washington began the first scientific analysis and excavations of the area in 1941 and 1942. They also constructed a museum and a building to protect the footprints.
Work was continued by Joaquín Matilló, Allan L. Bryan and Jorge Espinosa in the 1960s and 1970s. Allan L. Bryan, from the University of Alberta, used radiocarbon dating to determine the age of soil humates, from a buried soil directly underlying the footprints, to 5,945±145 radiocarbon years Before Present. Based upon this date, he estimated that the footprints dated to about 5,000 Before Present. But, later dating of the volcanic deposits in which they occur, known as the Masaya Triple Layer, demonstrated that the fossil tracks are only about 2,120 ± 120 years old. In 1978, the Nicaraguan researcher Jorge Espinosa continued the excavation near the termination of the original excavation. He uncovered more footprints at a depth of 4 meters. The track is believed to continue further.
Specimens of these footprints can be viewed at both the Peabody Museum of Archaeology at Harvard University and the United States National Museum.


== Acahualinca Museum ==

The "Museo Sitio Huellas de Acahualinca" is located at west of Managua, in the eponymous Acahualinca town. The museum was founded in 1953 by Nicaraguan scientist Leonor Martínez, later in 1989, was rescued, restored and fitted out with the support of "ASDI" and the Historical Museum of Sweden. In addition to footprints, the museum features a small collection of pottery and other items of archaeological interest from several sites in Nicaragua. Previously there were stone tools and a skull from León Viejo.


== References ==