This is a list for articles on notable historic forts which may or may not be under current active use by a military. There are also many towns named after a Fort, the largest being Fort Worth, Texas, United States.


== Australia ==
Sydney Harbour fortifications

Other fortifications


== Bahamas ==


== Bahrain ==


== Bangladesh ==


== Belarus ==


== Belgium ==


=== Province of Antwerp ===
Antwerp

Antwerp (1914, internal defenses)


=== Province of Liège ===
Liège (1914, clockwise from N, right bank of Meuse River)

Liège

Liège (1940)


=== Province of Namur ===
Namur (1914, clockwise from E, right bank of Meuse River)


== Bermuda ==
Bermuda had around 90 coastal defense scattered all over the island chain. Early colonial defense works constructed before the 19th century were primarily small coastal batteries built of stone having anywhere from two to ten guns. Some of these early forts and batteries are the oldest standing masonry forts in the new world. Later forts constructed by the royal engineers were much larger and more complex.


== Brazil ==


== Canada ==
Many places in Canada which bear the name "Fort" were never military establishments. Many were simply stockades, log enclosures for fur trading posts of the Hudson's Bay Company or the North West Company. Many in the West were also originally police outposts set up by the North-West Mounted Police prior to European-Canadian settlement of the area. See also Category: Forts in Canada


=== Alberta ===


=== British Columbia ===


=== Manitoba ===


=== New Brunswick ===


=== Newfoundland and Labrador ===


=== Northwest Territories ===


=== Nova Scotia ===


=== Nunavut ===


=== Ontario ===


=== Prince Edward Island ===
Fort Amherst


=== Quebec ===


=== Saskatchewan ===


=== Yukon ===
Fort Reliance


== Channel Islands ==
Alderney
Fort Clonque
Guernsey

Jersey
Fort Regent


== Chile ==


== China ==


=== Beijing ===


=== Hong Kong ===
Chinese (Qing dynasty) forts


=== Macau ===
All forts in Macau were built during or used during Portuguese rule:


=== Tianjin ===
Taku Forts, Tianjin


== Colombia ==
Castillo San Felipe de Barajas (Muralla de Cartagena de Indias)


== Croatia ==


== Congo (Republic of the) ==
Fort de Shinkakasa


== Cyprus ==


== Denmark ==


== Dominican Republic ==
Fortaleza San Felipe


== Egypt ==


== Estonia ==

Fortifications of Peter the Great's Naval Fortress


== Finland ==


== France ==


=== Vauban ===


=== Séré de Rivières system ===


=== Maginot Line (Northeast) ===


=== Alpine Line (Maginot Southeast) ===


=== Former German fortifications ===
Fort de Mutzig


=== Moselstellung ===


== Germany ==


== India ==


=== Andhra Pradesh ===


=== Daman and Diu ===


=== Gujarat ===


=== Karnataka ===


=== Kerala ===


=== Madhya Pradesh ===
Gwalior Fort, largest in India


=== Maharashtra ===


=== Rajasthan ===


=== Tamil Nadu ===


=== Union Territory of Delhi ===
Delhi Fort


=== Uttar Pradesh ===


=== West Bengal ===


== Indonesia ==


=== Sumatra ===


=== Java ===


=== Sulawesi ===


=== The Moluccas ===


=== Papua ===


== Israel ==


== Italy ==


=== Abruzzo ===
Forte Spagnolo, L'Aquila


=== Aosta Valley ===
Fort Bard, Bard


=== Apulia ===
Forte a Mare, Brindisi


=== Liguria ===


=== Marche ===
Fortress of San Leo, Marche


=== Tuscany ===
Belvedere, Florence


== Japan ==
Goryōkaku, Hakodate, Hokkaidō


== Libya ==
Fort Capuzzo


== Lithuania ==

Kaunas Fortress fortifications (listed in order of number)


== Malaysia ==


== Malta ==


== The Netherlands ==


=== Forts on the Dutch Water Line ===


=== Forts on the Stelling van Amsterdam ===


== New Zealand ==


== Norway ==


== Pakistan ==


== Panama ==


== Philippines ==


== Poland ==


== Portugal ==


== Russia ==


== Saint Kitts and Nevis ==
Brimstone Hill Fortress


== Saudi Arabia ==

Maskmak Fortress


== Serbia ==


== Singapore ==


== Sri Lanka ==


== South Africa ==


== South Korea ==
Hwaseong Fortress, Suwon


== Spain ==


=== Albacete ===
Fort Chinchilla


=== Badajoz ===
Fort Jerez de los Caballeros


=== Burgos ===
Fort Cartagena


=== Cáceres, Spain|Cáceres ===
Fort Coria


=== Cádiz ===


=== Province of Castellón ===
Fort Peníscola


=== Córdoba ===
Fort Molina de Aragón


=== Girona ===
Fort Sant Ferran (Figueres)


=== Huelva ===


=== A Coruña ===
Muralla de Santiago de Compostela


=== Málaga ===
Muralla urbana de Marbella


=== Menorca ===
Fort San Felipe


=== Murcia ===
Fort Caravaca de la Cruz


=== Navarre ===


=== Palma de Mallorca ===
Fort Bellver


=== Segovia ===
Alcázar of Segovia


=== Soria ===
Fort Berlanga de Duero


=== Toledo ===
Fort Toledo


=== Valladolid ===


=== Zaragoza ===
Aljaferia


== Sweden ==


== Taiwan (Republic of China) ==


== Turkey ==


== Ukraine ==


== United Kingdom ==
See also the list of castles, as many early forts were called castles, and many castle sites were reused for later fortifications. Also Palmerston Forts lists the many British fortifications built in the 1860s.


=== England ===


==== General ====
Maunsell Sea Forts


==== SE England ====


==== Thames ====


==== Medway ====


==== Solent ====
Portsdown Hill

Gosport

Portsmouth

Sea Forts

Isle of Wight


==== SW England ====
Berry Head
Bristol Channel
Brean Down Fort

Dartmouth
Bayard's Cove Fort

Plymouth

Isle of Portland

Weymouth
Nothe Fort


==== East Anglia ====


==== NW England ====


==== NE England ====


=== Wales ===
Milford Haven

Fort Belan


=== Scotland ===


== United States ==


=== Alabama ===


=== Alaska ===


=== Arizona ===


=== California ===


=== Colorado ===


=== Connecticut ===


=== Delaware ===


=== Florida ===

Fort DeSoto


=== Georgia ===


=== Hawaii ===


=== Idaho ===


=== Illinois ===


=== Indiana ===


=== Iowa ===


=== Kansas ===


=== Kentucky ===


=== Louisiana ===


=== Maine ===


=== Maryland ===


=== Massachusetts ===


=== Michigan ===


=== Minnesota ===


=== Mississippi ===


=== Missouri ===


=== Montana ===


=== Nebraska ===


=== Nevada ===


=== New Hampshire ===


=== New Jersey ===


=== New Mexico ===


=== New York ===


=== North Carolina ===


=== North Dakota ===


=== Ohio ===


=== Oklahoma ===


=== Oregon ===


=== Pennsylvania ===


=== Puerto Rico ===


=== Rhode Island ===
Fort Adams


=== South Carolina ===


=== South Dakota ===


=== Tennessee ===


=== Texas ===


=== Utah ===


=== Vermont ===


=== Virginia ===


=== Virgin Islands (U.S.) ===


=== Washington ===


=== Washington, D.C. ===


=== West Virginia ===


=== Wisconsin ===


=== Wyoming ===


=== Cities and areas with Fort in the name ===


== See also ==
List of castles
List of fortifications


== References ==