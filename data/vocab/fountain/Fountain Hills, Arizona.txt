Fountain Hills is a town in Maricopa County, Arizona, United States, neighboring the Fort McDowell Yavapai Nation, Salt River Pima-Maricopa Indian Community, and Scottsdale, Arizona. The population is 22,489, as of the 2010 census. It was the eighth fastest-growing place among cities and towns in Arizona between the 1990 and 2000 censuses.


== History ==
Fountain Hills was named after the towering man-made fountain in the center of town, and was incorporated in 1989.


== Geography ==
According to the United States Census Bureau, the town has a total area of 18.2 square miles (47.1 km2), of which 18.1 square miles (46.9 km2) is land and 0.1 square miles (0.26 km2) (0.55%) is water.
To the east of the town is the Verde River, a tributary to the Salt River. Inside the town there are many washes that run through Fountain Hills into the Verde River. Some of these washes include the Ashbrook, Balboa, Legend, and Colony Washes. During times of rain the washes flood with water and can sometimes block roads with their water.
To the southwest and northwest regions of Fountain Hills are the McDowell Mountains, a chain of extinct volcanic mountains. The highest mountains in the range are East End (4,033 ft (1,229 m)) and Thompson Peak (3,910 ft (1,190 m)).


=== Climate ===
This area has a large amount of sunshine year round due to its stable descending air and high pressure. According to the Köppen Climate Classification system, Fountain Hills has a mild desert climate, abbreviated "Bwh" on climate maps.


== Demographics ==
As of the census of 2010, there were 22,489 people, 10,339 households, and 7,121 families residing in the town. The population density was 1,113.8 inhabitants per square mile (430.0/km2). There were 13,167 housing units at an average density of 577.5 per square mile (223.0/km2). The ethnic makeup of the town was 94.1% White, 1.0% Black or African American, 0.6% Native American, 1.8% Asian, 0.1% Native Hawaiian and Other Pacific Islander, 1.1% from other races, and 1.4% from two or more races. Hispanic or Latino of any race make up 4.1% of the population.
There were 10,339 households out of which 18.6% had children under the age of 18 living with them, 59.4% were married couples living together, 6.7% had a female householder with no husband present, and 31.1% were non-families. Householders living alone make up 25.7% of all households and 11.5% had someone living alone who was 65 years of age or older. The average household size was 2.16 and the average family size was 2.56.
In the town, the population was spread out with 14.4% under the age of 18, 85.6% 18 years and over, and 27.8% who were 65 years of age or older. The median age was 53.9 years. There were 47.8% males and 52.2% females.
According to a 2010 Census American Community Survey 3-year estimate, the median income for a household in the town was $75,038, and the median income for a family was $91,585.
The per capita income for the town was $47,441. About 3.0% of families and 5.0% of the population were below the poverty line, including 8.6% of those under age 18 and 0.7% of those age 65 or over.


== Arts and culture ==


=== The Fountain ===

Fountain Hills has the world's fourth tallest fountain. The eponymous fountain was built in 1970, by Robert P. McCulloch, the year before the reconstruction of the London Bridge in Lake Havasu City, another of McCulloch's projects. The fountain sprays water for about 15 minutes every hour between 9am and 9pm. The plume rises from a concrete water-lily sculpture in the center of a large man-made lake. The fountain, driven by three 600 horsepower (450 kW) turbine pumps, sprays water at a rate of 7,000 US gallons (26,000 l; 5,800 imp gal) per minute through an 18-inch (460 mm) nozzle. With all three pumps under ideal conditions, the fountain reaches 560 feet (170 m) in height, though in normal operation only two of the pumps are used, with a fountain height of around 300 feet (91 m) feet. When built, it was the world's tallest fountain and held that record for over a decade.


=== Annual fairs ===
The town has three annual fairs, a local art fair, the Fountain Hills Great Fair which incorporates arts and a carnival, as well as Thunderbird Artists' Fountain Hills Fine Art & Wine Affaire. Each year the town's fountain is dyed green in celebration of St. Patrick's day. Fountain Hills Theater is an award winning performing arts venue entering its 28th season and offering over 16 productions a year to local communities as well as performing an arts education year round for youth.


== Government ==
Fountain Hills has a Council-Manager system. The current Mayor of Fountain Hills is Linda Kavanagh, elected in Mar 2012 after running in an uncontested election for the seat vacated by Jay Schlum. The current Town Council consists of the Mayor and six Councilmembers: Vice Mayor Dennis Brown, Councilmembers Henry Leger, Alan Magazine, Cecil Yates, Nick DePorter and Cassie Hansen. Ken Buchanan is the Town Manager. Among other council appointed staff are Town Attorney Andrew McGuire and Town Magistrate Robert Melton.
The town does not have its own law enforcement. It is patrolled by the Maricopa County Sheriff's Office with the elected Sheriff Joe Arpaio, who lives in town.
The Town contracts with Rural Metro for staffing of its fire department. Randy Roberts is the Fire Chief.
Fountain Hills is in Arizona’s 6th Congressional District, served by Representative David Schweikert and Arizona’s 23rd State Legislative District served by Representatives Jay Lawrence and Michelle R. Ugenti and Senator John Kavanagh, all Republicans.


== Education ==
Fountain Hills Public Schools are part of the Fountain Hills Unified School District #98. The district has two elementary schools, one middle school and one high school. Students attend Fountain Hills High School.


== Media ==

The Fountain Hills Times is the town's weekly newspaper and is published in Fountain Hills. The paper has a weekly circulation of about 3,000.
The parent company of the Times, Western States Publishers, Inc., also publishes the Fountain Hills/Rio Verde Telephone Directory, Fountain Hills Community Guide, Fountain Hills HOME.


== Notable people ==
Joe Arpaio - current sheriff of Maricopa County
Kathy Ahern - professional golfer


== Sister cities ==
Fountain Hills has three sister cities, as designated by Sister Cities International:
 - Kasterlee (Antwerpen, Belgium)
 - Dierdorf (Rhineland-Palatinate, Germany)
 - Concepcion de Ataco, El Salvador
 - Zamość, Poland


== Gallery ==


== References ==


== External links ==
Fountain Hills City website
"Fountain Hills Chamber of Commerce". Retrieved October 26, 2011. 
"Fountain Hills Times Newspaper". Retrieved October 26, 2011. 
"Fountain Hills School District". Retrieved October 26, 2011. 
"Fountain Hills Charter School". Retrieved October 26, 2011.