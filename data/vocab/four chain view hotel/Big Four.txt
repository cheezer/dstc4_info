Big Four may refer to:


== People ==
The Big Four (Calgary), Alberta businessmen and philanthropists of the early 20th century
Big Four Conference, various conferences between the victorious nations after World War I and World War II
Big Four (Central Pacific Railroad), US railroad entrepreneurs of the 19th century
Big Four (debutantes), prominent Chicago debutantes in the early 20th century
Big Four of Maryland Thoroughbred racing, four trainers who dominated the sport in the 1960s and 1970s
Big Four (Najaf), the four leading Grand Ayatollahs in Iraq
Big Four (Scotland Yard), in charge of the CID at Scotland Yard from about 1919 onwards


== Places ==
Big Four, West Virginia
Big Four Bridge, Louisville, Kentucky
Big Four Mountain, Snohomish County, Washington


== Companies ==
Big Four airlines, AA, EA, TW, and UA airlines, the four U.S. air carriers which dominated air travel prior to deregulation
Big Four (audit firms), the largest international accountancy and professional services firms
Big Four (British railway companies), the companies formed after the Grouping Act in 1922
Big Four (banks), several groupings of banks in different countries
Big Four Railroad, a nickname of the CCC&StL Railway in the Midwestern United States
Big Four, the four largest national registered agent service providers in the United States
Big Four in US broadcast television, the traditional Big Three television networks of ABC, CBS and NBC plus Fox
Big Four in the United States wireless communications service providers, AT&T Mobility, Verizon Wireless, Sprint Corporation, and T-Mobile US
Japanese Big Four, the four largest Japanese motorcycle manufacturers: Honda, Kawasaki, Suzuki, and Yamaha
Big Four, the most influential international technology companies, Amazon, Apple, Facebook, and Google.


== Education ==
Big Four, a term used in reference to the four most selective universities in Shanghai: Fudan, Shanghai Jiao Tong, East China Normal and Tongji University


== Sports ==
Big Four (tennis), top male tennis players Roger Federer, Rafael Nadal, Novak Djokovic and Andy Murray
Big Four (English football), Arsenal, Chelsea, Liverpool and Manchester United domination in the Premier League
Big Four (bowling), a type of split in ten pin bowling
Big Four (polo), an American polo team of the early 20th century
Big Four (marathon), four major marathon races comprising four of the five World Marathon Majors
Big Four, the four leading major professional sports leagues in the United States and Canada
Big Four, the four leading soccer leagues in Europe, Premier League, La Liga, Bundesliga, and Serie A
Big Four, a nickname for WWE's major pay-per-view events, SummerSlam, Survivor Series, Royal Rumble, and WrestleMania
Big Four, officially the Interprovincial Rugby Football Union, the forerunner to the Canadian Football League East Division


== Music ==
The Big 4 Live from Sofia, Bulgaria concert recording with thrash metal bands Metallica, Slayer, Megadeth and Anthrax
Big Four (band), a Chinese male pop group
Big Four (Eurovision), the four main sponsoring countries of the Eurovision Song Contest
Big Four (Grammy Awards) or the General Field, four standard Grammy Awards spanning all genres
The Big Four, the name for the top music industry record labels from 2004 through 2011
The Big Four (Grunge), used to describe, Nirvana, Pearl Jam, Soundgarden, and Alice in Chains, arguably the four most popular bands on the grunge scene in the early nineties.
The Big Four (Thrash metal), used to describe Metallica, Slayer, Megadeth, and Anthrax, arguably the most popular and recognisable bands on the thrash metal scene in the eighties and early nineties.
The Big Teutonic Four, used to describe Kreator, Sodom, Destruction, and Tankard, the German equivalent to the American Big Four of thrash metal.


== International relations ==
Big Four, the four major Allies powers (as well as their leaders) that won World War I: France, Italy, Britain and United States
Big Four, the four major Allies powers of World War II: China, Soviet Union, United Kingdom and United States
Big Four (European Union): France, Germany, Italy and the UK


== Other uses ==
The Big Four (novel), by Agatha Christie
The Big Four, 2013 film
Big Four (Indian snakes), four snake species responsible for the most snake-bites in India
Big Four (White Star Line), a quartet of British ocean liners of the early 20th century
Big 4 (lottery), an online game in the Pennsylvania Lottery
Norton Big 4, a British motorcycle produced between 1907 and 1954
Big Four (Motorcycle gang), four motorcycle clubs designated by police as outlaw gangs and liable for RICO prosecution
Big 4 (sculpture), outside the Channel 4 headquarters in London
The largest four known asteroids until 1910, when 704 Interamnia was discovered – 1 Ceres, 2 Pallas, 4 Vesta, and 10 Hygiea


== See also ==
Big One (disambiguation)
Big Two (disambiguation)
Big Three (disambiguation)
Big Five (disambiguation)
Big Six (disambiguation)
Big Seven (disambiguation)
Big Eight (disambiguation)
Big Ten (disambiguation)
Big 11
Big 12
Core Four
Fab Four


== References ==