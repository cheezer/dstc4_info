A four-star rank is the rank of any four-star officer described by the NATO OF-9 code. Four-star officers are often the most senior commanders in the armed services, having ranks such as (full) admiral, (full) general, or (in the case of air forces with a separate rank structure) air chief marshal. This designation is also used by some armed forces that are not North Atlantic Treaty Organisation (NATO) members.


== Australia ==

Admiral (Royal Australian Navy four-star rank)
General (Australian Army four-star rank)
Air chief marshal (Royal Australian Air Force four-star rank)
The four-star rank is reserved in Australia for the Chief of the Defence Force, the highest position in peace time.
In times of major conflict, the highest ranks are the five-star ranks: admiral of the fleet, field marshal, and marshal of the Royal Australian Air Force.


== Brazil ==

General de exército (Brazilian Army four-star rank)
Almirante de esquadra (Brazilian Navy four-star rank)
Tenente brigadeiro (Brazilian Air Force four-star rank)
The four-star rank is reserved in Brazil for the highest post in the military career. The officers in this position take part of the high command of their corporations. The commanders of army, navy and air force are also four-star generals, but they have precedence to all the others military in this rank.


== Canada ==
Admiral/amiral (Canadian Forces officers authorized to wear naval uniform four-star-equivalent rank)
General/général (Canadian Forces officers authorized to wear army and air force uniform four-star-equivalent rank)
Four maple leaves appear with St. Edward's crown and crossed sabre and baton on epaulettes and shoulder boards. This is the highest rank that is defined in the Canadian Forces under the current National Defence Act Schedule. Usually, only one officer carries the rank of full admiral or general at any one time: the Chief of the Defence Staff. In exceptional circumstances, from time-to-time, the minister may authorize additional officers at that level, for instance, Canadian officers in the position of Chairman of the NATO Military Committee are usually former Chiefs of the Defence Staff seconded to NATO for that duty.
The Command-in-Chief is vested in the Head of State, Queen Elizabeth II; however, the duties are exercised by the Governor General of Canada, who has the title of Commander-in-Chief of Canada. Likewise, the Minister of National Defence, since he or she is not a member of the Canadian Forces, nor in the chain-of-command, also bears no rank. Prince Philip holds the four-star rank of admiral in the Royal Canadian Navy in an honorary capacity as of 2011.
Before unification in 1968, the rank of air chief marshal (maréchal en chef de l'air) was the four-star equivalent for the Royal Canadian Air Force.


== Germany ==
The equivalent modern German four-star ranks (OF-9) of the Bundeswehr are as follows:
General
Admiral
Not to be confused with Generaloberst, the Wehrmacht (West Germany) equivalent until 1945, or Armeegeneral, the National People´s Army (East Germany) equivalent until 1990.


== India ==

General (Indian Army four-star rank)
Air chief marshal (Indian Air Force four-star rank)
Admiral (Indian Navy four-star rank)
Director of Intelligence Bureau (India) (Indian Police four-star rank)


== Pakistan ==
General (Pakistan Army four-star rank)
Air chief marshal (Pakistan Air Force four-star rank)
Admiral (Pakistan Navy four-star rank)


== United Kingdom ==
Admiral (Royal Navy four-star rank)
General (British Army and Royal Marines four-star rank)
Air chief marshal (Royal Air Force four-star rank)
See also:
List of Royal Marines full generals
List of British Army full generals
List of Royal Air Force air chief marshals


== United States ==
Admiral (United States Navy, Coast Guard, NOAA Corps, and Public Health Service Commissioned Corps four-star rank)
General (United States Army, Air Force, and Marine Corps four-star rank)
See also:
List of active duty United States four-star officers
List of United States Navy four-star admirals
List of United States Public Health Service Commissioned Corps four-star admirals
List of United States Army four-star generals
List of United States Air Force four-star generals
List of United States Marine Corps four-star generals


== Former USSR and Russia ==

General armii (infantry and marines of the Red Army, Russian Army and Air Force four-star rank)
Glavnii marshal and marshal (four-star equivalents of the Soviet Air Force, and of the other branches of the Red Army)
Admiral flota (four star equivalent of the Soviet and the Russian Navies)
While the general armii wore shoulder insignia with four small stars, the marshal and admiral flota wore one single large star on their shoulder boards, and the glavnii marshal the same large star with a laurel wreath, very similar to the modern army general insignia of the Russian Army.
Upon their formation, the Russian armed forces discontinued the ranks of marshal and glavnii marshal.


== See also ==
Ranks and insignia of NATO
General officer


== Notes ==