ULTA Salon, Cosmetics & Fragrance, Inc. is a chain of beauty superstores located in the United States. ULTA Beauty carries a variety of cosmetics and skincare brands, men's and women's fragrances, and professionally licensed haircare products. Each store is also equipped with a full-service salon. The company is headquartered in Bolingbrook, Illinois.  ULTA Beauty's logo is a white "ULTA Beauty" print against their signature peach colored background.


== History ==
Ulta Salon, Cosmetics and Fragrance was founded by Richard E. George, the former President of Osco Drug, Inc. Following his tenure, Terry J. Hanson, another Osco veteran, became the CEO. In September 1996, Hanson hired Charles "Rick" Weber, another Osco veteran, to be Chief Financial Officer. In December 1999, Lyn Kirby, formerly of Sears Circle of Beauty, became the President and Chief Executive Officer and Weber became Senior Executive Vice President, Chief Operating Officer and Chief Financial Officer. The two ran the business together until Weber left in September 2006 when he was selected by Tom Stemberg, founder of Staples, to become Chairman and Chief Executive officer of Rec Room Furniture and Games, Inc.  On October 25, 2007, the company became publicly traded on the NASDAQ. In 2008, the company opened a second distribution center in Phoenix, Arizona. On April 26, 2010, ULTA announced that Mr. Carl "Chuck" Rubin would be appointed as Chief Operating Officer, President, and as a member of the Board of Directors, effective May 10, 2010. According to public filings, Chuck Rubin earned over $11.1MM in 2010, making him one of the 20 highest paid CEOs in Chicago.

As of March 2011, ULTA has $730.5 million in total assets.
On June 24, 2013, it was announced that Mary Dillon would be appointed as Chief Executive Officer and a member of the Board of Directors, effective July 1, 2013. Mary Dillon previously held positions as the former chief of U.S. Cellular and senior executive at McDonalds and PepsiCo. 


== Finances ==
ULTA Beauty has been on the rise as the cosmetics retailer reported sales of $713.8 million for the first quarter ending in May 2014. This was a 22.5 percent increase from the same period last year, exceeding Wall Street's estimate of $700 million. At the end of the second quarter ending in August 2014, ULTA Beauty reported that total sales increased by 22.2 percent and comparable store sales Increased 9.6 percent.  Wall Street expects their total sales to reach $3.19 billion for the fiscal year ending in February 2015, which would be almost three times the amount of ULTA Beauty's sales levels in 2009. 


== Locations ==
In 2013, ULTA opened 125 stores in the United States, bringing their total number of locations to 675 stores. They have announced plans to open 100 more locations by the end of 2014.  As of August 2, 2014, ULTA Beauty operates 715 stores in 47 states.  A majority of ULTA Beauty stores are located in the East Coast region, however on the West Coast, California has a large presence of company owned stores as well.  ULTA aims to have 1,000 stores located in the United States by 2017. 


== Brands ==
ULTA Beauty carries over 20,000 products and 459 brands in their traditional 10,000 square feet store, offering both high-end and drugstore cosmetics, skincare, and fragrances.  Some of the high-end brands that ULTA carries includes bareMinerals, Benefit Cosmetics, Clinique, Smashbox, Stila, and Tarte. Drugstore brands include popular brands like Covergirl, L'Oréal, Maybelline, NYX Cosmetics, and Revlon. ULTA also has their own store brand which includes makeup, bath and body, skincare, hair, nail polish, and fragrances. ULTA Beauty also offers exclusive "Only at ULTA" cosmetics for their loyal customers including Stila palettes, IT Brushes for ULTA, Tarte kits, and bareMinerals and Benefit Cosmetics sets.


== Services ==
ULTA has their own full-service salon located in every store nationwide. The store format includes a 950 square feet modern salon area with eight to ten stations.The whole salon has a concierge desk, skin treatment room, and semi-private shampoo and hair color processing areas. This salon offers services including but not limited to hair cuts and hair coloring from skilled Redken trained hair stylists, skin treatments and facials from ULTA's Dermalogica trained and licensed skin therapists, and manicures from OPI trained professionals. 


=== Benefit Brow Bar ===
ULTA Beauty also has Benefit Brow Bar's located in select stores nationwide. Benefit Brow Bar's signature services include brow arching, brow tinting, facial waxing, and eyelash application. All Benefit aestheticians are licensed professionals that are highly trained in the art of brow shaping. 


== Charitable Giving ==
ULTA Beauty has been striving to make a difference in impactful ways. Starting from 2008, ULTA Beauty's sole focus in charity giving was their commitment to The Breast Cancer Research Foundation. Their charitable donations fund research related to the causes, treatment, and possible prevention of breast cancer. By supporting this cause, which benefits women, their families, and their loved ones across the nation, ULTA Beauty seeks to not only bring a greater sense of community amongst women worldwide but also raise awareness about the importance of finding a cure. 
ULTA Beauty also hosts programs such as their annual Donate with a Kiss campaign, which raises funds for The Breast Cancer Research Foundation throughout the month of October, ULTA Beauty's Annual Charity Gold Outing, as well as ULTA Beauty's Annual Cut-A-Thon, where customers donate ten dollars to the cause in exchange for a haircut. In 2012, ULTA Beauty raised over 2 million dollars for The Breast Cancer Research Foundation.  In 2014, ULTA Beauty partnered with Ellen DeGeneres during Breast Cancer Awareness Month in October and donated $1.5 million for The Breast Cancer Research Foundation. 


== Customer Loyalty Program ==
ULTA Beauty has a free customer loyalty program called ULTAmate Rewards, where customers can redeem points on any in-store or online purchase they make with their membership card. Customers accumulate one point for every dollar that they spend and are not limited to what products they can redeem their points on. The points that ULTA customers earn are valid for one year before they expire. For ULTA customers who spend $400 within one year are upgraded to the next level called Platinum status. The main perks of obtaining Platinum status is that customers are able to earn points 25 percent faster and the points that they earn never expire. 


== References ==
^ "ULTA Company Overview". ULTA. 
^ "About ULTA Beauty". ULTA. 
^ "ULTA Headquarters". ULTA. 
^ http://www.ulta.com/ulta/common/history.jsp
^ "Chicago's $10 million CEOs". Chicago Tribune. 
^ "Ulta 2011 Sales Expected Between 5% and 7% for Established Stores – Beauty Salon Coupons Enhance Growth". Quarterly Retail Review. March 14, 2011. 
^ "Ulta Beauty Names Mary Dillion Chief Executive Officer". Yahoo! Finance. 
^ "ULTA Earnings". Chicago Tribune. 
^ "ULTA News Release". ULTA. 
^ "ULTA's Growth". 
^ "Beauty Emporiums Including Sephora and Ulta Are Riding a Wave of Popularity". New York Times. April 30, 2014. 
^ "ULTA Company Overview". ULTA. 
^ "ULTA Beauty Locations". ULTA. 
^ "Beauty Retailer Ulta aims for 1000 stores by 2017". Reuters. 
^ http://www.theagencysd.com/2012/11/battle-of-the-brands-sephora-vs-ulta/
^ "ULTA Salon". ULTA. 
^ "ULTA Benefit Brow Bar". ULTA. 
^ "ULTA Beauty Charitable Giving". ULTA. 
^ "ULTA Beauty 2012 Donation". 
^ "ULTA Beauty Donates". Ellen TV. 
^ "ULTAmate Rewards Benefits". ULTA. 
http://stores.ulta.com/store/index.php?action=state&state=NY&stores_number=66&zoom=2&latitude=40.777914&longitude=-73.032289


== External links ==
Official website