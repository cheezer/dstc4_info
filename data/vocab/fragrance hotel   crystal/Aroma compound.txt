An aroma compound, also known as odorant, aroma, fragrance, or flavor, is a chemical compound that has a smell or odor. A chemical compound has a smell or odor when it is sufficiently volatile to be transported to the olfactory system in the upper part of the nose.
Generally molecules meeting this specification have molecular weights of <300. Flavors affect both the sense of taste and smell, whereas fragrances affect only smell. Flavors tend to be naturally occurring, and fragrances tend to be synthetic.
Aroma compounds can be found in food, wine, spices, perfumes, fragrance oils, and essential oils. For example, many form biochemically during ripening of fruits and other crops. In wines, most form as byproducts of fermentation. Also, many of the aroma compounds play a significant role in the production of flavorants, which are used in the food service industry to flavor, improve, and generally increase the appeal of their products.
An odorizer may add an odorant to a dangerous odorless substance, like propane, natural gas, or hydrogen, as a warning.


== Aroma compounds classified by structure ==


=== Esters ===


=== Linear terpenes ===


=== Cyclic terpenes ===
Note: Carvone, depending on its chirality, offers two different smells.


=== Aromatic ===


=== Amines ===


== Other aroma compounds ==


=== Alcohols ===
Furaneol (strawberry)
1-Hexanol (herbaceous, woody)
cis-3-Hexen-1-ol (fresh cut grass)
Menthol (peppermint)


=== Aldehydes ===
High concentrations of aldehydes tend to be very pungent and overwhelming, but low concentrations can evoke a wide range of aromas.
Acetaldehyde (ethereal)
Hexanal (green, grassy)
cis-3-Hexenal (green tomatoes)
Furfural (burnt oats)
Hexyl cinnamaldehyde
Isovaleraldehyde – nutty, fruity, cocoa-like
Anisic aldehyde – floral, sweet, hawthorn. It is a crucial component of chocolate, vanilla, strawberry, raspberry, apricot, and others.
Cuminaldehyde (4-propan-2-ylbenzaldehyde) – Spicy, cumin-like, green


=== Esters ===
Fructone (fruity, apple-like)
Hexyl acetate (apple, floral, fruity)
Ethyl methylphenylglycidate (strawberry)


=== Ketones ===
Cyclopentadecanone (musk ketone)
Dihydrojasmone (fruity woody floral)
Oct-1-en-3-one (blood, metallic, mushroom-like)
2-Acetyl-1-pyrroline (fresh bread, jasmine rice)
6-Acetyl-2,3,4,5-tetrahydropyridine (fresh bread, tortillas, popcorn)


=== Lactones ===
gamma-Decalactone intense peach flavor
gamma-Nonalactone coconut odor, popular in suntan lotions
delta-Octalactone creamy note
Jasmine lactone powerful fatty fruity peach and apricot
Massoia lactone powerful creamy coconut
Wine lactone sweet coconut odor
Sotolon (maple syrup, curry, fenugreek)


=== Thiols ===

Allyl thiol (2-propenethiol; allyl mercaptan; CH2=CHCH2SH) (garlic volatiles and garlic breath)
(Methylthio)methanethiol (CH3SCH2SH), the "mouse thiol", found in mouse urine and functions as a semiochemical for female mice
Ethanethiol, commonly called ethyl mercaptan (added to propane or other liquefied petroleum gases used as fuel gases)
2-Methyl-2-propanethiol, commonly called tert-butyl mercaptan is added as a blend of other components to natural gas used as fuel gas.
Butane-1-thiol, commonly called normal butyl mercaptan is a chemical intermediate.
Grapefruit mercaptan (grapefruit)
Methanethiol, commonly called methyl mercaptan (after eating Asparagus)
Furan-2-ylmethanethiol, also called furfuryl mercaptan (roasted coffee)
Benzyl mercaptan (leek or garlic-like)


=== Miscellaneous compounds ===
Methylphosphine and dimethylphosphine (garlic-metallic, two of the most potent odorants known)
Phosphine (Green Apple, Zinc Phosphide poisoned bait)
Diacetyl (Butter flavor)
Acetoin (Butter flavor)
Nerolin (orange flowers)
Tetrahydrothiophene (added to natural gas)
2,4,6-Trichloroanisole (cork taint)
Substituted pyrazines


== Aroma compound receptors ==
Animals that are capable of smell detect aroma compounds with olfactory receptors. Olfactory receptors are cell membrane receptors on the surface of sensory neurons in the olfactory system that detect air-borne aroma compounds.
In mammals, olfactory receptors are expressed on the surface of the olfactory epithelium in the nasal cavity.


== Safety ==

In 2005–06, fragrance mix was the third-most-prevalent allergen in patch tests (11.5%).
'Fragrance' was voted Allergen of the Year in 2007 by the American Contact Dermatitis Society. The composition of fragrances is usually not disclosed in the label of products, hiding the actual chemicals of the formula, which raises concerns among some consumers.
Fragrances are regulated in the United States by the Toxic Substances Control Act of 1976 that "grandfathered" existing chemicals without further review or testing and put the burden of proof that a new substance is not safe on the EPA. The EPA, however, does not conduct independent safety testing but relies on data provided by the manufacturer.


== List of chemicals used as fragrances ==
In 2010 the International Fragrance Association published a list of 3,059 chemicals used in 2011 based on a voluntary survey of its members. It was estimated to represent about 90% world's production volume of fragrances.


== References ==
^ Karl-Georg Fahlbusch, Franz-Josef Hammerschmidt, Johannes Panten, Wilhelm Pickenhagen, Dietmar Schatkowski, , Kurt Bauer, Dorothea Garbe and Horst Surburg "Flavors and Fragrances" Ullmann's Encyclopedia of Industrial Chemistry, 2003, Wiley-VCH. doi:10.1002/14356007.a11_141
^ Gane, S; Georganakis, D; Maniati, K; Vamvakias, M; Ragoussis, N; Skoulakis, EMC; Turin, L (2013). "Molecular vibration-sensing component in human olfaction". PLoS ONE 8: e55780. doi:10.1371/journal.pone.0055780. 
^ a b Glindemann, D., Dietrich, A., Staerk, H., Kuschk, P. (2005). "The Two Odors of Iron when Touched or Pickled: (Skin) Carbonyl Compounds and Organophosphines". Angewandte Chemie International Edition 45 (42): 7006–7009. doi:10.1002/anie.200602100. PMID 17009284. 
^ Block, E. (2010). Garlic and Other Alliums: The Lore and the Science. Royal Society of Chemistry. ISBN 0-85404-190-7. 
^ Lin, D.Y.; Zhang, S.Z.; Block, E.; Katz, L.C. (2005). "Encoding social signals in the mouse main olfactory bulb". Nature 434: 470–477. doi:10.1038/nature03414. 
^ Zug KA, Warshaw EM, Fowler JF Jr, Maibach HI, Belsito DL, Pratt MD, Sasseville D, Storrs FJ, Taylor JS, Mathias CG, Deleo VA, Rietschel RL, Marks J. Patch-test results of the North American Contact Dermatitis Group 2005–2006. Dermatitis. 2009 May–Jun;20(3):149-60.
^ Toxic chemicals linked to birth defects are being found at alarming levels in women of childbearing age
^ Randall Fitzgerald. The Hundred Year Lie. Dutton, 2006. p. 23. ISBN 0-525-94951-8. 
^ "IFRA Survey:Transparency List". IFRA. Retrieved December 3, 2014. 


== See also ==
Flavour and Fragrance Journal
Fragrances of the World
Foodpairing
Odor
Odor detection threshold
Olfaction
Olfactory system
Olfactory receptor
Odorizer, a device for adding an odorant to gas flowing through a pipe
Pheromone
Aroma of wine
Eau de toilette