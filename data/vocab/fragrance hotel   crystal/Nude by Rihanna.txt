Nude by Rihanna is the third fragrance for women by Barbadian recording artist Rihanna. The fragrance was released on November 23, 2012 (Black Friday), under the perfume line, Parlux Fragrances.


== Information ==
The fragrance is described as a sweet, floral [and] fruity fragrance with a vanilla background. It also consists of woody and skin musky notes. Fruity aromas of guava, mandarin and pear are located at the opening of the composition. The heart is blended out of white flowers: gardenia petals, velvety Sambac jasmine and creamy orange blossom. The base consists of sandalwood, vanilla orchid and "second skin" musk.


== Promotion ==
On October 5, 2012, Rihanna revealed the official promotional image for Nude commenting on Twitter, "Here is my new add for a brand new fragrance #NUDE!!! Make sure you smell sexy, especially naked, this fall." The promotional campaign for the perfume features a blonde Rihanna "wearing nothing but cream lace lingerie, a piece of gauze fabric draped strategically over her crotch."
Rihanna launched the perfume at Macy's on December 1, 2012. Speaking of her third fragrance, Rihanna told Women's Wear Daily, "When creating my first scent, Reb'l Fleur, I wanted it to be a really strong scent – daring and bold. I have gotten more personal with Nude."


== Products ==
100 ml/ 3.4 oz
50 ml/ 1.7 oz
30 ml/ 1.0 oz
15 ml/ 0.5 oz


== References ==