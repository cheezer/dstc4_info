Forever is a women's fragrance from Elizabeth Arden, and is the third fragrance to be endorsed by Mariah Carey, following Luscious Pink.


== Design ==
The title came from the song "Forever", released on the 1995 album Daydream. The bottle design was created after Carey's love for art deco style. The fragrance reflects on Carey's life at the moment. Ron Rolleston, executive vice president for global fragrance marketing at Elizabeth Arden, stated the inspiration for the perfume came from the mood of Carey's life right now, which is joy and optimism. Carey has stated: "I am in a wonderful place right now, surrounded by all the things I love, and Forever captures this moment in time." Her scent reflects on her album, Memoirs of an Imperfect Angel, her marriage, and her role in the movie, Precious.


== Ingredients ==
The scent has lotus blossom, tuberose, gardenia, green apple, neroli, exotic wood and white musk.


== References ==