Sexy Darling is a women's fragrance from Coty, Inc., and the Third perfume to be endorsed by Kylie Minogue. Sexy Darling was created by Sophie Labbe.


== Scent ==
The scent contains the base of Darling with elements of blood orange, pear, pink pepper, red rose, belle de nuit, jasmine, sandalwood, and musk.


== Design ==
The Elegant, Gracefully rounded Bottle will be a stylish must have addition to any dressing table. The packaging echos the bottle with its deep red box, Kylie's trademark 'K' in a black lace pattern that evokes her seductive glamour.