Rebelle is the second fragrance for women by Barbadian recording artist Rihanna. The fragrance was released in Spring 2012 under the perfume line Parlux Fragrances. The fragrance was made available for purchase on Belk.com, on February 15, 2012. in the United States. It was also available in a special pack at Macy's for a limited time. The pack included the pop singer's previous fragrance, Reb'l Fleur, her sixth studio album, Talk That Talk and Rebelle.


== Background ==
In a poll, on the official website, for Rihanna's first fragrance, Reb'l Fleur, Rihanna asked fans to state whether they were a Rebelle or a Fleur. After an overwhelming 68% of Rihanna's fans voted Rebelle, the singer decided to embark on the release of her second fragrance, Rebelle, having achieved great success with her debut fragrance, Reb'l Fleur.


== Information ==
Rihanna described the fragrance, as having a "feminine romantic element," while maintaining a "defiant quality." She added, "my new fragrance is about taking control but still being a lady."  The top notes of the fragrance include; plum, strawberry and ginger, the middle notes include orchid, cacao and heliotrope, and the base notes include musk, patchouli, amber and coffee. The fragrance is packaged in a flacon shape, created of red and gold surfaces, similar to that of Rihanna's previous fragrance.


== Promotion ==
The promotional campaign for Rebelle, was shot by renowned music video director, Anthony Mandler, who also shot the promotional campaign for Reb'l Fleur. The fragrance was also marketed on the social networking service, Facebook and on Twitter. Additionally, a large billboard, displayed in Times Square, New York, features Rihanna, promoting the fragrance.


== Products ==
100 ml/ 3.4 oz
50 ml/ 1.7 oz
30 ml/ 1.0 oz
15 ml/ 0.5 oz
Body Lotion 200 ml/ 6.7 oz
Shower Gel 90 ml/ 3.0 oz


== Reception ==
Since the release of Rebelle in the United Kingdom in February 2012, it has sold over 2 million bottles. Rihanna's fragrance has beaten other celebrity scents, in terms of bottles sold, including Heat by Beyoncé, Glow by Jennifer Lopez and Midnight Fantasy by Britney Spears. Rebelle sold a record-breaking, 4 million bottles, in the UK, at the end of 2012.


== References ==