Como La Flor (English: Like The Flower) is a women's fragrance from Catherine Des Champs, and is the second posthumous fragrance to be endorsed by Mexican American singer-songwriter Selena. The perfume sold more than 700,000 bottles by 1999, which helped boost the Selena perfume line.


== Background and Inspiration ==
In January 1995, Selena approached Leonard Wong, a Chinese-Mexican who set up a company to market products by direct sales to the Hispanic community, with an idea to start the Selena perfume line. Selena decided that she wanted to start her own perfume line, which was to be released simultaneously with her crossover album, Dreaming of You. Gathering inspiration from her idols, Selena wanted a perfume that was "sweet, and good to smell". Selena's release and production of the perfume continually was delayed due to her on-going Amor Prohibido Tour, recording sessions for her crossover album, fashion shows for her Selena Etc. store, and a then, growing problem with Yolanda Saldivar who was embezzling money from the official Selena Fan Club.
Como La Flor (English: Like A Flower) was named after Selena's #1 Latin Regional Mexican Airplay song, Como La Flor. Suzette Quintanilla, the sister of Selena, decided to release some of her perfumes after Selena's Billboard's Hot Latin Tracks songs. Suzette was in-charge of sales of Selena's perfumes, starting with Forever. She helped promote the perfume to Sears, Wal-Mart, and other perfume stores.


== Sales ==


== References ==