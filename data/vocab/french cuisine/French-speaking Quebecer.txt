French-speaking Quebecers or Quebeckers (in French Québécois) are francophone residents of the Canadian province of Québec.
The majority of francophone Quebecers are of French-Canadian descent, but many immigrants integrate themselves into the francophone majority in Québec. Many French Canadians have French and Irish origins; Irish surnames are common and some have English surnames, Scottish surnames, Italian surnames, German surnames, and so on.
According to the 2006 Census, 72% of residents of the Montreal Census Metropolitan Area speak French natively; outside the Montréal CMA, this figure is 95%. Major francophone universities include Université Laval, Université de Montréal, Université de Sherbrooke and the Université du Québec system.


== See also ==

Quebec
Québécois (word)
Canadians
French Canadian
Acadian
English-speaking Quebecer
Franco-Ontarian


== References ==