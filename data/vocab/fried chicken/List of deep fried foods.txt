This is a list of deep fried foods and dishes. Deep frying is a cooking method in which food is submerged in hot fat e.g. oil. This is normally performed with a deep fryer or chip pan; industrially, a pressure fryer or vacuum fryer may be used. Deep frying is classified as a dry cooking method because no water is used. Due to the high temperature involved and the high heat conduction of oil, it cooks food extremely quickly.


== Deep fried foods ==

Deep fried foods


== By main ingredient ==


=== Chicken ===


=== Tofu ===

Agedashi tofu
Stinky tofu - sometimes deep fried
Tahu goreng
Tahu sumedang


== By cuisine ==


=== Chinese cuisine ===


==== Cantonese ====

Zaa Leung
Yau Zaa Gwai (youtiao)
Dace fish balls
Deep-fried marinated pigeon


=== Japanese cuisine ===
Agemono (揚げ物): Deep-fried dishes
Karaage (唐揚げ) : bite-sized pieces of chicken, fish, octopus, or other meat, floured and deep fried. Common izakaya (居酒屋) food, also often available in convenience stores.
Nanbanzuke (南蛮漬け): marinated fried fish.

Korokke (croquette コロッケ): breaded and deep-fried patties, containing either mashed potato or white sauce mixed with minced meat, vegetables or seafood. Popular everyday food.
Kushikatsu (串カツ): skewered meat, vegetables or seafood, breaded and deep fried.
Tempura (天ぷら): deep-fried vegetables or seafood in a light, distinctive batter.
Tonkatsu (豚カツ): deep-fried breaded cutlet of pork (chicken versions are called chicken katsu).

Agedashi dofu (揚げ出し豆腐): cubes of deep-fried silken tofu served in hot broth.


=== Thai cuisine ===
Main: List of Thai dishes – Deep-fried dishes


== See also ==

Frietmuseum
List of fried dough foods
List of hors d'oeuvre


== References ==


== External links ==
 Media related to Deep-fried food at Wikimedia Commons