Fruit salad is a dish consisting of various kinds of fruit, sometimes served in a liquid, either in their own juices or a syrup. When served as an appetizer or as a dessert, a fruit salad is sometimes known as a fruit cocktail or fruit cup. In different forms fruit salad can be served as an appetizer, a side-salad, or a dessert.


== Description ==

There are a number of home recipes for fruit salad that contain different kinds of fruit, or that use a different kind of sauce other than the fruit's own juice or syrup. Common ingredients used in fruit salads include strawberries, pineapple, honeydew, watermelon, grapes, banana, and kiwifruit. Various recipes may call for the addition of nuts, fruit juices, certain vegetables, yogurt, or other ingredients.
One variation is a Waldorf-style fruit salad, which uses a mayonnaise-based sauce. Other recipes use sour cream (such as in ambrosia), yogurt or even mustard as the primary sauce ingredient. An ever-popular variation also uses whipped cream mixed in with many varieties of fruits (usually a mixture of berries), and also often include miniature marshmallows. Rojak, a Malaysian fruit salad, uses a spicy sauce with peanuts and shrimp paste. In the Philippines, fruit salads are popular party and holiday fare, usually made with buko, or young coconut, and condensed milk in addition to other canned or fresh fruit.
Mexico has popular variation of the fruit salad called Bionico which consists various fruits drenched in condensed milk and sour cream mix.
There is also an extended variety of fruit salads in Moroccan cuisine, often as part of a kemia, a selection of appetizers or small dishes analogous to Spanish tapas or eastern Mediterranean mezze.
A fruit salad ice cream is also commonly manufactured, with small pieces of real fruit embedded in, flavored either with juices from concentrate, fruit extracts, or artificial chemicals.


== Fruit cocktail ==
Fruit cocktail is often sold canned and is a staple of cafeterias, but can also be made fresh. The use of the word "cocktail" in the name does not mean that it contains alcohol, but refers to the secondary definition "An appetizer made by combining pieces of food, such as fruit or seafood". Fruit cocktail is sometimes used to make pruno.
In the United States, the USDA stipulates that canned "fruit cocktail" must contain a certain percentage distribution of pears, grapes, cherries, peaches, and pineapples to be marketed as fruit cocktail. It must contain fruits in the following range of percentages:
30% to 50% diced peaches, any yellow variety
25% to 45% diced pears, any variety
6% to 16% diced pineapple, any variety
6% to 20% whole grapes, any seedless variety
Few to no cherry halves, any light sweet or artificial red variety (like maraschino cherries)
Both William Vere Cruess of the University of California, Berkeley and Herbert Gray of the Barron-Gray Packing Company of San Jose, California have been credited with the invention of fruit cocktail. Canned fruit cocktail and canned fruit salad are similar, but fruit salad contains larger fruit while fruit cocktail is diced.


== In popular culture ==
"Fruit Salad" is also the name of a song by Australian children's band The Wiggles and on the television show Wonder Pets.
"Fruit-salad" is also a slang term used for medals on a soldiers uniform, e.g. "Look at the fruit-salad on that colonel." The term refers to the bright colors of a high percentage of the ribbons that usually go with medals.
"Fruit salad" is also an alternative name for the party game Fruit Basket Turnover.


== See also ==
Compote
Macedonia (food)
Green papaya salad


== References ==


== External links ==
History of fruit salad