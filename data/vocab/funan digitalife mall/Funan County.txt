Funan County (simplified Chinese: 阜南县; traditional Chinese: 阜南縣; pinyin: Fùnán Xiàn) is a county of western Anhui Province, East China, under the administration of Fuyang City.
Funan has an area of 1,768 km2 (683 sq mi) and a population of 1,553,000.


== Administrative divisions ==
Funan County administers twenty towns and 11 townships. Former divisions are indicated in italics.
Towns:
Lucheng, formed from the merger on 27 December 2006 of Chengguan (城关镇) and Chengjiao Township (城郊乡)
Fangji (方集镇)
Zhonggang (中岗镇)
Caoji (曹集镇)
Tianji (田集镇)
Chaiji (柴集镇)
Yuanji (袁集镇), which was assigned to Yingzhou District's administration on 10 October 2006
Zhaoji (赵集镇)
Miaoji (苗集镇)
Huanggang (黄岗镇)
Xincun (新村镇)
Zhuzhai (朱寨镇)
Zhangzhai (张寨镇)
Jiaopo (焦陂镇)
Wanghua (王化镇)
Wangyan (王堰镇)
Dicheng (地城镇)
Wangjiaba (王家坝镇)
Hongheqiao (洪河桥镇)
Santaji (三塔集镇)

Townships:
Wangdianzi (王店孜乡)
Gongqiao (公桥乡)
Gaotai (郜台乡)
Xutang (许堂乡)
Huilong (会龙乡)
Longwang (龙王乡)
Duanying (段郢乡)
Yuji (于集乡)
Laoguan (老观乡)


== References ==


== External links ==
Official website of the Funan County government