The Furama Hong Kong Hotel (富麗華酒店), once known as the Furama Kempinski Hotel, was a 33-storey hotel in Central, Hong Kong, located at 1 Connaught Road Central. The hotel was known for its revolving restaurant on the top floor. The site is now occupied by the AIA Central office building.


== §History ==
The hotel was designed by Eric Cumine Associates and built at the prestigious address of 1 Connaught Road Central for the Furama group in 1973. It had 33 storeys and a total height of 361 feet (110 m), with a revolving restaurant on the top floor named "La Ronda".
The management of the hotel was taken over by Inter-Continental Hotels Ltd. in 1976 for a period of 15 years, consequently the hotel was named "Hotel Furama Inter-continental".
In 1990 the hotel was acquired by Kempinski and renamed the "Furama Kempinski Hotel".
Lai Sun Development (LSD) acquired the hotel's parent company, Furama Hotel Enterprises, in June 1997 for HK$7 billion. This was done by acquiring a 45.42 per cent stake for $3.13 billion, and a general offer at $33.50 for each remaining shares at a total cost of $6.893 billion. To finance the acquisition, the company geared itself up heavily taking on $5 billion in bank loans and issuing bonds for more than $2 billion.
While the other Furama Group hotels were integrated into the hotels division, Lai Sun Hotels, the Furama plot was to be combined with the Ritz Carlton plot, which it already owned, for redevelopment into a prime office block. Then the Asian financial crisis struck, plunging the entire group into distress and forced asset sales.
In March 2000, LSD announced that a 65% stake in the Furama would be sold to a 50:50 joint venture between Pidemco, controlled by Temasek Holdings, and AIG for HK$1.88 billion. As part of the deal, Lai Sun would continue to operate the hotel until its redevelopment, at an annual rental of HK$145 million. Together with CapitaLand, and AIG, LSD formed Bayshore Development Group to develop AIG Tower, a 39-storey commercial office block with a gross floor area of 450,000 square feet (42,000 m2). AIG and CapitaLand each own 35 per cent, and Lai Sun owns 30 per cent. The Furama closed in November, and was demolished in December 2001.


== §References ==


== §External links ==
CTBUH entry
Corporate website, Furama Group