Ford Fusion may refer to:
Ford Fusion (Americas), mid-size car produced since the 2006 model year
Ford Fusion Hybrid, gasoline-electric hybrid powered version
Ford Fusion Energi, plug-in hybrid version

Ford Fusion (Europe), mini MPV produced from 2002 to 2012 sold in Europe