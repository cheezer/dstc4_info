Rimin Gado (or Rafin Gado) is a Local Government Area in Kano State, Nigeria. Its headquarters are in the town of Rimin Gado about 20 km west of the state capital Kano.
It has an area of 225 km² and a population of 104,790 at the 2006 census.
The postal code of the area is 701.


== References ==