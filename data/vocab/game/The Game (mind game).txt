The Game is a mental game where the objective is to avoid thinking about The Game itself. Thinking about The Game constitutes a loss, which must be announced each time it occurs. It is impossible to win most versions of The Game. Depending on the variation of The Game, the whole world, or all those aware of the game, are playing it all the time. Tactics have been developed to increase the number of people aware of The Game and thereby increase the number of losses.
The origins of The Game are unknown; a game featuring ironic processing was played by Leo Tolstoy in 1840. The Game has received media attention in different countries and the number of players is estimated to be in the millions.


== Gameplay ==
There are three commonly reported rules to The Game:
Everyone in the world is playing The Game. (Sometimes narrowed to: "Everybody in the world who knows about The Game is playing The Game", or alternatively, "You are always playing The Game.") A person cannot choose to not play The Game; it does not require consent to play and one can never stop playing.
Whenever one thinks about The Game, one loses.
Losses must be announced. This can be verbally, with a phrase such as "I just lost The Game", or in any other way: for example, via Facebook. Some people may have signals or expressions that remind others of The Game.
What constitutes thinking about The Game is not always clear. If one discusses "The Game" without explicitly realizing that they have lost, this may or may not constitute a loss. If someone says "What is The Game?" before understanding the rules, whether they have lost is up for interpretation. According to some interpretations, one does not lose when someone else announces their loss, although the second rule implies that one loses regardless of what made them think about The Game. After a player has announced a loss, or after one thinks of The Game, some variants allow for a grace period between three seconds to thirty minutes to forget about the game, during which the player cannot lose the game again.

The common rules do not define a point at which The Game ends. However, some players state that The Game ends when the Prime Minister of the United Kingdom announces on television that "The Game is up."
Most variations of The Game do not allow for a person to win. One interpretation is that one is winning the game whenever they are not thinking about it. An xkcd comic reads: "You just won The Game" and tells players, "You're free!" Another is that one has won when they have completely forgotten about The Game.


=== Strategies ===
Strategies focus on making others lose The Game. Common methods include saying "The Game" out loud or writing about The Game on a hidden note, in graffiti in public places, or on banknotes. Associations may be made with The Game, especially over time, so that one thing inadvertently causes one to lose. Some players enjoy thinking of elaborate pranks that will cause others to lose the game.
Other strategies involve merchandise: T-shirts, buttons, mugs, posters and bumper stickers have been created to advertise The Game. The Game has been spread via social media websites such as Facebook, Twitter, deviantArt and Shelfari. More unusual strategies involve legislation: petitions in Britain trying to pass laws involving The Game have been created. Messages have been sent to country leaders, attempting to get them to talk about The Game in public or on television. An email sent to Kevin Rudd, the Prime Minister of Australia at the time, received a reply saying "unfortunately, my commitments do not allow me the time to fulfil many of the requests I receive, such as participating in The Game on television".


== Origin ==
The origins of The Game are uncertain. In a 2008 news article, Justine Wettschreck says The Game has probably been around since the early 1990s, and may have originated in Australia or England. One theory is that it was invented in London in 1996 when two British engineers, Dennis Begley and Gavin McDowall, missed their last train and had to spend the night on the platform; they attempted to avoid thinking about their situation and whoever thought about it first lost. Another theory also traces The Game to London in 1996, when it was created by Jamie Miller "to annoy people".
However, The Game may have been created in 1977 by members of the Cambridge University Science Fiction Society when attempting to create a game that did not fit with the "game theory". A blog post by Paul Taylor in August 2002 described The Game; Taylor claimed to have "found out about [the game] online about 6 months ago". This is the earliest known reference on the internet.
The Game is most commonly spread through the internet, such as via Facebook or Twitter, or by word of mouth.


== Psychology ==

The Game is an example of ironic processing (also known as the "White Bear Principle"), in which attempts to avoid certain thoughts make those thoughts more persistent. There are early examples of ironic processing: in 1840, Leo Tolstoy played the "white bear game" with his brother, where he would "stand in a corner and not think of the white bear". Fyodor Dostoyevsky mentioned the same game in 1863 in the essay Winter Notes on Summer Impressions.
One psychological study of The Game by Cory Antiel involved 12 participants; they were asked to record when and why they lost over four weeks. The study itself caused 57% of participants' losses; Antiel claimed The Zeigarnik effect contributed to this. The participants recorded vastly different numbers of losses; common reasons included "references to taking notes", "references to time" and "seeing or thinking about other people who also play The Game". Priming and sensitization played a large part in losses; no strong correlation with habituation was found.


== Reception ==

The Game has been described as challenging and fun to play, or as pointless, childish and infuriating. In some Internet forums, such as Something Awful and GameSpy, and several schools, including one in Ohio, The Game has even been banned. The Game has been described as a game, a meme and a "mind virus".
Newspapers, including Metro, Rutland Herald, The Canadian Press and De Pers, have contained articles about The Game; Wikinews has interviewed the owner of LoseTheGame.com. Webcomics Saturday Morning Breakfast Cereal, xkcd and Real Life have featured sketches relating to The Game. YouTube videos about The Game have attracted hundreds of thousands of views.
Several celebrities know about The Game. Actor Simon Pegg has tweeted about The Game. Alex Baker has talked about The Game on multiple occasions on Kerrang Radio and Facebook.
Other attempts to increase losses of The Game have involved hoaxes and hacking. The 2009 Time 100 poll was manipulated by the hacktivist group Anonymous, so that the top 21 people's names formed an acrostic for "marblecake also the game", referencing The Game.
The number of people aware of The Game is unknown, but estimated to be in the millions. Roughly a million people have lost The Game from a Metro article, a million through Kerrang Radio and 500,000 lost from an xkcd comic. In 2008, the largest Facebook group relating to The Game had over 200,000 members.


== See also ==
Catch-22 (logic)
Inside joke
Meme
Mornington Crescent
Paradox


== References ==


== External links ==

 Media related to The Game (mind game) at Wikimedia Commons
 News related to Wikinews interviews manager of site 'Lose The Game' at Wikinews