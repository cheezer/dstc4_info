Secret Garden may refer to:


== In literature ==
My Secret Garden, Nancy Friday
The Secret Garden, 1911 novel by Frances Hodgson Burnett, which spawned several adaptations:
The Secret Garden (1919 film), directed by Gustav von Seyffertitz
The Secret Garden (1949 film), starring Dean Stockwell and Margaret O'Brien
The Secret Garden (1975 TV series), BBC TV serial starring Sarah Hollis Andrews and Hope Johnstone
The Secret Garden (1987 film), a Hallmark Hall of Fame TV movie
The Secret Garden (1993 film), a movie directed by Agnieszka Holland
The Secret Garden (1994 film), an animated adaptation produced by DIC Entertainment, Greengrass Productions, and Kalisto Ltd.
The Secret Garden (musical), a 1991 Broadway musical with music by Lucy Simon
The Secret Garden (opera), 2013 opera by Nolan Gasser

"The Secret Garden", a short story by G. K. Chesterton featuring his Father Brown character
Secret Garden: An Inky Treasure Hunt and Colouring Book, 2013 coloring book for adults by Johanna Basford


== In music ==
Secret Garden (duo), an Irish-Norwegian duo playing New Instrumental Music


=== Albums ===
"Secret Garden" (Album) A 2014 album by Angra.
Secret Garden (EP), the third mini-album by South Korean girl group A Pink


=== Songs ===
"The Secret Garden (Sweet Seduction Suite)", a 1990 single by Quincy Jones
"Secret Garden" (Bruce Springsteen song), a 1995 single by Bruce Springsteen
"Secret Garden" (T'Pau song), a 1988 single by T'Pau
"Secret Garden" (Gackt song), a 2000 single by Gackt
"Secret Garden", a 1992 song by Madonna
"Secret Garden", a 2007 song by Patrick Wolf from The Magic Position
"My Secret Garden", Depeche Mode
"My Secret Garden", Johnny Hates Jazz


== Television ==
Secret Garden (South Korean TV series), a 2010 South Korean fantasy drama
Secret Garden (Singaporean TV series), a 2010 Singaporean drama


== Other ==
Secret Garden (outdoor nursery), a playschool in the woods of Letham, Fife, Scotland.
Secret Garden (Big Brother), a secret room used in Big Brother (UK) series 6
Secret Garden (비원), Park of a Royal Palace in Seoul, Korea


== See also ==
My Secret Garden, 1973 book by Nancy Friday
"My Secret Garden", a 1982 song by Depeche Mode
Secret Garden Party, a music festival in Cambridgeshire, England