Aljunied MRT Station is an above-ground Mass Rapid Transit station on the East West Line in Singapore. It serves the urban neighbourhoods of Aljunied, Sims Place and Geylang East. Its livery is blue.


== History ==
From the start, Aljunied had no physical barriers between the platforms and tracks. In view of many incidents over the years, the Land Transport Authority made the decision in 2008 to install half height platform screen doors for all above-ground stations in phases which is operational on 11 February 2011.


== High volume, low speed (HVLS) fans ==
Station installed with Rite Hite Revolution High Volume, Low Speed (HVLS) fans and began operating on 27 July 2012.


== Station layout ==


== Exits ==
A: Sims Avenue, Central Grove Condominium, Geylang Methodist Church, Geylang Post Office, Highpoint Social Enterprise Ark, Hotel 81 / Fragrance Hotel, KH Plaza / CPA House, Khadijah Mosque, Singapore Buddhist Free Clinic, SPCS Building / POSB Branch, Yu Li Industrial Building
B: Aljunied Road, Brighton Furniture Display Center, Foo Hai Monastery, Geylang East HDB Branch Office, Geylang East Library, Geylang East Polyclinic, Geylang East Post Office, Geylang East Swimming Complex/ S'pore Basketball ACC, Geylang Methodist Sec / Pri School, Geylang Serai NPP, Geylang West NPP, Prosper House, Sims Drive HDB Branch Office, Sims Place Bus Terminal, Sims Place Market & Food Centre, Geylang East Market & Food Centre, Sri Sivan Temple, THK Building, Thong Teck Home For SNR Citizen, Yong Da Building


== Transport connections ==


=== Rail ===


== References ==


== External links ==
Official website