Dover is a relatively small neighbourhood of Singapore, located in Queenstown. It is south of Ghim Moh and north of Kent Ridge. It is often a noted location due to the sheer number of educational facilities it holds, while lying in a zone between Singapore's central business district in the Central Area and her industrial zone in the vicinity of Jurong. The general geographical area is classified under the Queenstown Planning Area by the Urban Redevelopment Authority, and is a few kilometres away from Queenstown itself.


== Etymology and history ==
The Dover neighbourhood was formerly a British military and residential area. Hence, many road names in the area are derived from English (specifically Kentish) place names. For example, those in Medway Park off Dover Road include names associated with the southern coastal areas of England, including Folkestone and Maidstone, apart from Dover itself. The name "Dover" is from the Celtic word meaning "The Waters", alluding to the English Channel. Medway Park, for instance, is named after the River Medway, which marks the middle of Kent. Interestingly, Kent is the nearest English county to Singapore.


== Roads in Dover ==
Dover Road
Dover Crescent
Dover Close East
Dover Avenue
Dover Drive
Dover Rise


== Transport ==
There are two Mass Rapid Transit (MRT) stations which are in proximity to Dover. One is Dover MRT Station, which is EW22 on the East West MRT Line, as well as Buona Vista MRT Station which is EW21/CC22. However, both MRT stations are not situated near the heart of Dover. The former is located next to Singapore Polytechnic, whilst the latter at the intersection of North Buona Vista Road and Commonwealth Avenue. A portion of most residents drop at Dover MRT Station and transfer to a bus (14, 74 or 166), while another portion drop at Buona Vista MRT and take a kilometre walk to Dover.
In addition, the following bus services pass through Dover:
SBS Transit


== Education ==

Dover is known for having many schools and education institutions in its neighbourhood and vicinity.
Primary schools
Fairfield Methodist Primary School

Secondary schools
Anglo-Chinese School (Independent)
Fairfield Methodist Secondary School
Nan Hua High School
New Town Secondary School
NUS High School of Mathematics and Science
School of Science and Technology, Singapore

Higher institutions of learning
National University of Singapore (NUS) (Kent Ridge)
Singapore University of Technology and Design (SUTD) (Dover Campus)
Singapore Polytechnic
Anglo-Chinese Junior College
ITE College West (Clementi Campus)
ITE College West (Dover Campus)
INSEAD

International schools
Dover Court Preparatory School
Japanese Secondary School (Clementi)
Norwegian Supplementary School
United World College of South East Asia

Other education institutions
Ministry of Education (Singapore)


== References ==

Victor R Savage, Brenda S A Yeoh (2003), Toponymics - A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1