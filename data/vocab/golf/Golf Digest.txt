Golf Digest is a monthly golf magazine published by Condé Nast Publications in the United States. It is a generalist golf publication covering recreational golf and men's and women's competitive golf. Condé Nast Publications also publishes the more specialized Golf for Women, Golf World and Golf World Business. The magazine started in 1950, and was sold to The New York Times Company in 1969. The Times company sold their magazine division to Condé Nast in 2001.


== "America's 100 Greatest Golf Courses" ==
Since 1965, Golf Digest has produced biennial rankings of "America's 100 Greatest Golf Courses". The courses are voted on by a panel of several hundred golf experts. Although Pine Valley Golf Club has topped the list every year except 2001, when Pebble Beach Golf Links ranked first, it was again outranked on the 2009-10 list - this time by Augusta National Golf Club. The magazine also produces lists of the best new courses, the best golf resorts and the best courses in each U.S. state. Before the "Greatest" rankings were introduced in 1985, Golf Digest produced lists called at different times America's 100 Most Testing Courses and America's 100 Greatest Tests of Golf.
The top ten on the 2009-10 list are as follows:
Augusta National Golf Club - Augusta, Georgia
Pine Valley Golf Club - Pine Valley, New Jersey
Shinnecock Hills Golf Club - Southampton, New York
Cypress Point Club - Pebble Beach, California
Oakmont Country Club - Oakmont, Pennsylvania
Pebble Beach Golf Links - Pebble Beach, California
Merion Golf Club (East Course) - Ardmore, Pennsylvania
Winged Foot Golf Club (West Course) - Mamaroneck, New York
Fishers Island Club - Fishers Island, New York
Seminole Golf Club - Juno Beach, Florida
The top ten on the 2007-08 list, published in May 2007, was as follows:
Pine Valley Golf Club - Pine Valley, New Jersey
Shinnecock Hills Golf Club - Southampton, New York
Augusta National Golf Club - Augusta, Georgia
Cypress Point Club - Pebble Beach, California
Oakmont Country Club - Oakmont, Pennsylvania
Pebble Beach Golf Links - Pebble Beach, California
Merion Golf Club (East Course) - Ardmore, Pennsylvania
Winged Foot Golf Club (West Course) - Mamaroneck, New York
Seminole Golf Club - Juno Beach, Florida
Crystal Downs Country Club - Frankfort, Michigan
The top ten on the 2005-06 list, published in May 2005, was as follows:
Pine Valley Golf Club - Pine Valley, New Jersey
Augusta National Golf Club - Augusta, Georgia
Shinnecock Hills Golf Club - Southampton, New York
Cypress Point Club - Pebble Beach, California
Oakmont Country Club - Oakmont, Pennsylvania
Pebble Beach Golf Links - Pebble Beach, California
Merion Golf Club - Ardmore, Pennsylvania
Winged Foot Golf Club (West Course) - Mamaroneck, New York
National Golf Links of America - Southampton, New York
Seminole Golf Club - Juno Beach, Florida


== "America's 100 Greatest Public Golf Courses" ==
Alongside the "100 Greatest Courses" ranking, and using the same methodology, Golf Digest publishes a list of "America's 100 Greatest Public Golf Courses". In this context, "public" means a golf course that is open to play by the public, as opposed to a private club—not necessarily a course operated by a governmental entity.
The top ten on the 2007-08 list, also published in May 2007, was as follows:
Pebble Beach Golf Links – Pebble Beach, California
Pacific Dunes Golf Course – Bandon, Oregon
Pinehurst No. 2 – Pinehurst, North Carolina
The Straits Course, Whistling Straits – Haven, Wisconsin
Bethpage Black Course – Farmingdale, New York
Shadow Creek Golf Course – North Las Vegas, Nevada
Bandon Dunes Golf Course – Bandon, Oregon
The Ocean Course at Kiawah Island – Kiawah Island, South Carolina
Prince Golf Course – Princeville, Hawaiʻi
Arcadia Bluffs Golf Course – Arcadia, Michigan
Of these courses, the only one that is operated by a governmental entity is Bethpage Black.
In addition to its national rankings, Golf Digest also ranks courses at a state level. For example, in a 1998 survey of Connecticut Public Golf Courses, Golf Digest ranked Crestbrook Park Golf Course as one of Connecticut's top public golf courses.


== "100 Best Golf Courses Outside the United States" ==
The magazine also compiles a list of the leading courses outside the United States. This is created using information from national golf associations, plus votes by the same panelists supplemented by some additional ones with international knowledge.
In 2007, the most represented countries were Scotland with fourteen courses in the top 100, Canada with ten, England with ten, Canada with nine, and Australia and Republic of Ireland with eight. The top 10 were:
Royal County Down Golf Club - Newcastle, Northern Ireland
Old Course at St Andrews - St. Andrews, Scotland
Royal Dornoch Golf Club (Championship Course) - Dornoch, Scotland
Royal Portrush Golf Club (Dunluce Course) - Portrush, Northern Ireland
Muirfield - Gullane, Scotland
Royal Melbourne Golf Club (Composite Course) - Melbourne, Australia
Ballybunion Golf Club (Old Course) - Ballybunion, Ireland
Turnberry (Ailsa Course) - Ayrshire, Scotland
Carnoustie Golf Links - Carnoustie, Scotland
Cape Kidnappers Golf Course - Hawke’s Bay, New Zealand
In 2005, the most represented countries were Scotland and Canada with thirteen courses each in the top 100. The top 10 were:
Old Course at St Andrews - St. Andrews, Scotland
Royal Melbourne Golf Club - Melbourne, Australia
Royal Portrush Golf Club - Portrush, Northern Ireland
Royal County Down Golf Club - Newcastle, Northern Ireland
Royal Dornoch Golf Club - Dornoch, Scotland
Muirfield - Gullane, Scotland
Ballybunion Golf Club - Ballybunion, Ireland
New South Wales Golf Club - Sydney, Australia
National Golf Club - Woodbridge, Canada
St. George's Golf and Country Club - Toronto, Canada


== Recognition ==
In 2009, Golf Digest was nominated for a National Magazine Awards by the American Society of Magazine Editorsin the Magazine Section in recognition of the excellence of a regular section of a magazine based on voice, originality and unified presentation.


== Controversy ==
In April 2014, Golf Digest was widely criticized when, after neglecting to picture a female golfer on their cover for six years, they chose to picture model Paulina Gretzky in a revealing outfit as their May 2014 cover. The move was "particularly frustrating" to LPGA golfers.   LPGA Tour Commissioner Mike Whan issued a statement echoing the concerns expressed by LPGA players.


== See also ==
Kingdom magazine
HK Golfer


== References ==


== External links ==
Official site
Official site of Golf for Women magazine, a Golf Digest publication
Official site of Golf World magazine, a Golf Digest publication
Golf course rankings
Composite of all U.S. courses ever ranked
Official site for Golf Digest Golf Schools