Goodwood is an inner southern suburb of the city of Adelaide. It neighbours the Royal Adelaide Showgrounds and features several churches in its commercial district. Its major precinct is Goodwood Road, which is home to many shops and businesses, as well as the local state school (Goodwood Primary School).


== History ==
The original land surveyed of 1839 was granted to the South Australian Company and named Goodwood. Two other sections of land had been sold to settler Thomas Hardy in May 1838, who sold it to his son, Arthur in 1841. The 1840 census shows that there was a Village of Goodwood with a population of 100, but the first registration of a contact for sale was not until 1846. In 1849, Arthur Hardy subdivided his property into a number of four acre blocks, naming it Goodwood Park.


== Governance ==
Goodwood is in the City of Unley local government area. It straddles the boundary of the state electorates of Ashford and Unley. It is in the federal Division of Adelaide. Its postcode is 5034.


== See also ==
Capri Theatre
Forestville hockey club
Electoral district of Goodwood


== References ==


== External links ==
Stories from Goodwood Residents
Forestville Hockey Club