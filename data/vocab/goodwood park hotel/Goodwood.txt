Goodwood may refer to:


== Events ==
Goodwood Festival of Speed, a motorsport event in the United Kingdom
Glorious Goodwood, a horseracing event in the United Kingdom
Goodwood Revival, a historical motorsport event in the United Kingdom


== Places and structures ==
in Australia
Goodwood, South Australia, Australia
Goodwood, Tasmania, Australia
In Canada
Goodwood, Nova Scotia
Goodwood, Ontario
In New Zealand
Goodwood, New Zealand, a farming community near Palmerston
in South Africa
Goodwood, Cape Town, South Africa
in the United Kingdom
Goodwood, Leicestershire, England
Goodwood, West Sussex, England - the site of the famous circuit mentioned below
Goodwood plant, of Rolls-Royce Motor Cars

Chichester/Goodwood Airport, West Sussex, England
Goodwood Circuit, West Sussex, England
Goodwood House, West Sussex, England
Goodwood Racecourse, West Sussex, England
Goodwood Cricket Club, West Sussex, England
in the United States
Goodwood Plantation, Tallahassee, Florida, listed on the NRHP in Florida
Goodwood (Richmond, Massachusetts), listed on the NRHP in Massachusetts


== Other ==
Operation Goodwood: a Second World War British military operation during the Battle of Normandy, July 1944
Operation Goodwood (naval): a series of Royal Naval attacks on the German battleship Tirpitz in August 1944 during the Second World War
Operation Goodwood (1968−1969) (Battle of Hat Dich): actions fought between the 1st Australian Task Force and the Viet Cong and North Vietnamese Army during the Vietnam War