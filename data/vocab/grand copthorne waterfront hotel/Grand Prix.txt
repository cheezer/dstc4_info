Grand Prix (French pronunciation: ​[ɡʁɑ̃pʁi], meaning "Big Prize"; plural Grands Prix) may refer to:


== Sports competitions ==
Grand Prix (snooker), a former name of the World Open
Six-red World Grand Prix, a former name of the Sangsom Six-red World Championship
IAAF Grand Prix Final, an athletics competitions replaced by the IAAF World Athletics Final
Grand Prix de Futsal, international futsal competition
Grand Prix gliding
ISU Grand Prix of Figure Skating
FIVB World Grand Prix, for women's volleyball
World Grand Prix (darts)
Grand Prix (Magic: The Gathering)
Aero GP, flying competition
IAAF Super Grand Prix, a series of athletics meetings held until 2009
Some of the meetings in the series that succeeded the IAAF Super Grand Prix
Adidas Grand Prix
British Grand Prix (athletics)
London Grand Prix
Qatar Athletic Super Grand Prix
Shanghai Golden Grand Prix


=== Racing ===
Grand Prix motor racing, form of racing that has evolved into Formula One racing
List of Formula One Grands Prix
Formula One

Grand Prix motorcycle Road Racing, known commonly as MotoGP after its premier class.
List of Grand Prix motorcycle races

Grand Prix Chantal Biya, a road bicycle racing event
U.S. Gran Prix of Cyclocross, a cyclo-cross bicycle racing series
Grand Prix Masters
A1 Grand Prix
D1 Grand Prix
Speedway Grand Prix (motorcycle speedway)
Macau Grand Prix


=== Tennis ===
Grand Prix tennis circuit
Budapest Grand Prix
Marrakech Grand Prix
Porsche Tennis Grand Prix


=== Chess ===
Grand Prix Attack, a chess opening
USCF Grand Prix
FIDE Grand Prix


=== Equestrianism ===
Grand Prix (horse race), the original name of the Prix Gladiateur, a horse race in France
Nakayama Grand Prix, original name of the Arima Kinen horse race in Japan
Grand Prix Dressage
Grand Prix show jumping


=== Combat sports ===
International Wrestling Grand Prix, the governing body for the wrestling promotion New Japan Pro Wrestling
K-1 World Grand Prix, K-1's annual kickboxing World Grand Prix tournaments
Strikeforce Grand Prix, the name for a competition run by Strikeforce
Pride Grand Prix, the name for a series of tournaments held by Pride
DREAM Grand Prix, the name for DREAM's various mixed martial arts Grand Prix tournaments
IFL Grand Prix, the name for International Fight League's various mixed martial arts Grand Prix tournaments


== Awards ==
Grand Prix (Cannes Film Festival)
Grand Prix (Belgian Film Critics Association)
Grand Prix de Littérature Policière, annual literary award for French and international crime fiction
Grand Prix de la ville d'Angoulême, a comics award
Prix Saint-Michel, another comics award
Grand Prix at International Film Festival Bratislava


== Entertainment and arts ==
European Grand Prix for Choral Singing, an annual choral competition between the winners of six European choral competitions
Eurovision Song Contest (originally named Grand Prix d'Eurovision de la Chanson)
Melodi Grand Prix, Norwegian selection for Eurovision Song Contest
Dansk Melodi Grand Prix, Danish selection for Eurovision Song Contest

Grand Prix (album), by Teenage Fanclub
Grand Prix (1934 film), a British film
Grand Prix (1966 film), a 1966 film about Formula One motor racing
Grand Prix (2010 film), a South Korean film
"Grand Prix" (CSI: Miami), seventh episode from second season of CSI: Miami
Grand Prix (TV programme), BBC Television's TV coverage of Formula One motor racing


== Video games ==
F-1 Sensation, the Japanese/European version of the cancelled NES game Grand Prix
Formula One Grand Prix (video game)
Grand Prix 2
Grand Prix 3
Grand Prix 4

Grand Prix (video game) by Activision
Grand Prix Challenge, by Atari
Grand Prix Circuit (video game), by Accolade
Grand Prix Legends (1967 season)
A gameplay mode in the Mario Kart series by Nintendo


== Other uses ==
Pontiac Grand Prix, an automobile formerly manufactured by General Motors