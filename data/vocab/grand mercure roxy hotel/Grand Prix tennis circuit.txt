The Grand Prix tennis circuit was a professional tennis tour for male players that existed from 1970 to 1989. It was the more prominent of two predecessors to the current tour for male players, the ATP Tour, the other being World Championship Tennis (WCT).


== Background ==
Prior to the Open Era popular professional tennis players were contracted to a Professional Promoter. Players such as Suzanne Lenglen and Vincent Richards were contracted to these promoters while amateur players followed their national (and international) federation. Later professional promoters, such as Bill Tilden and Jack Kramer, often convinced leading amateurs such as Pancho Gonzales and Rod Laver to join their tours with promises of good prize money, but these successes led to financial difficulties when players were paid too much and falling attendances resulted in reduced takings. In the late-1950s the professional tour began to fall apart. It only survived when the U.S. Pro Tennis Championships, having been unable to give prize money to its winner in 1962, received prize money from the First National Bank of Boston for the following year's tournament. At the same time the concept of "shamateurism" – amateur promoters paying players under the table to ensure they remained amateurs – had become apparent to Herman David, the chairman of The Wimbledon Championships at that time.
In 1967, David announced that a professional tournament would be held at Wimbledon after the Championships that year. The tournament was televised by the BBC and succeeded in gaining public support for professional tennis. In late 1967, the best of the remaining amateur players turned professional, paving the way for the first open tournament. Some professionals were independent at this time, such as Lew Hoad, Luis Ayala and Owen Davidson, but most of the best players came under contract to one of two professional tours:
The National Tennis League (NTL), run by George McCall and Fred Podesta.
Rod Laver, Roy Emerson, Ken Rosewall, Andrés Gimeno, Pancho Gonzales and Fred Stolle

World Championship Tennis (WCT), run by David F. Dixon, Albert G. Hill Jr and Lamar Hunt
Handsome Eight: John Newcombe, Tony Roche, Niki Pilić, Roger Taylor, Pierre Barthès, Butch Buchholz, Cliff Drysdale and Dennis Ralston

When the Open Era began in 1968, tournaments often found themselves deprived of either NTL or WCT players. The first Open tournament, the British Hard Court Championships at Bournemouth, was played without WCT players, as was that year's French Open. In 1970, NTL players did not play the Australian Open because their organization did not receive a guarantee.


== The formation of the Grand Prix ==
The manipulation of Grand Slams in particular by professional promoters at the start of the Open Era led Jack Kramer, the former No. 1 male tennis player in the world in the 1940s and 1950s and a promoter himself, to conceive the Grand Prix in 1969. He described it as "a series of tournaments with a money bonus pool that would be split up on the basis of a cumulative point system." This would encourage the best players to compete regularly in the series, so that they could share in the bonus at the end and qualify for a special championship tournament that would climax the year".
When only a few contract players showed up for the 1970 French Open, the International Lawn Tennis Federation (ILTF) approved Kramer´s Grand Prix proposal and in April 1970 its president Ben Barnett announced the creation of the Grand Prix circuit, on an experimental basis during its first year.


== ILTF—WCT rivalry and the Association of Tennis Professionals ==
The first WCT tournament was held in February 1-3, 1968 at Kansas City (USA) and the first NTL tournament in March 18-21, 1968 at Sao Paulo (Brazil). In July 1970 WCT absorbed the NTL.
In 1971, World Championship Tennis ran a twenty tournament circuit with the year-ending WCT Finals held in November. At the end of 1970, a panel of journalists had ranked the best players in the world, and the best thirty-two men based on this ranking were invited to play the 1971 WCT circuit: among these 32 players were Ilie Năstase, Stan Smith, Jan Kodeš, Željko Franulović and Clark Graebner. The Australian Open was part of the WCT circuit while the French Open, Wimbledon and the US Open were Grand Prix events. The conflict between the ILTF running the Grand Prix and WCT was so strong that Rosewall, Gimeno, Laver, Emerson and other WCT players did not enter that year's US Open. There was a third professional circuit that year with the U.S Indoor Circuit run by Bill Riordan, the future manager of Jimmy Connors.
In 1972, the struggle between ILTF and WCT ended with the ILTF banning contract professional players from January to July. Consequently the WCT contract pros were strictly forbidden to play the Grand Prix circuit, including the French Open and Wimbledon. At the US Open the players formed their own syndicate, the Association of Tennis Professionals (ATP), through the efforts of Jack Kramer, Donald Dell, and Cliff Drysdale.
In 1973, there were four rival professional circuits: the WCT circuit battled with the U.S. Indoor Circuit from January to April and the Grand Prix until July; both tours competed with the European Spring Circuit until June. In that same year the ATP created controversy by calling for a boycott of the 1973 Wimbledon Championships after one of its members, Niki Pilić, was suspended by the Yugoslav Tennis Federation for failing to play in a Davis Cup tie against New Zealand. The ATP boycott, despite negotiation, went ahead, with only three members of the organisation – Roger Taylor, Ilie Năstase and Ray Keldie – breaking the picket. They were later fined for this. The men's draws for that year were subsequently made up of second-string players, lucky losers and older players such as Neale Fraser, who reached the final of the men's doubles with fellow Australian John Cooper. The draw also showcased future talents such as Björn Borg, Vijay Amritraj, Sandy Mayer and John Lloyd, and record crowds helped to defy the boycott.


== Integration and the end ==
The WCT and Grand Prix circuits were separate until 1978, when the Grand Prix circuit integrated the WCT circuit. In 1982, the WCT circuit split from the Grand Prix again and created a more complex WCT ranking, similar to the ATP ranking. The split was short-lived, however, and in 1985 the Grand Prix absorbed the four remaining WCT tournaments.
During the 1988 US Open the ATP, led by then-World No. 1 Mats Wilander, staged an impromptu meeting known as the Parking Lot Press Conference during failed negotiations with the MTC over the organisation of the Grand Prix and key issues such as player fatigue. During the Conference the ATP declared that it would be starting its own tour in 1990, meaning that the 1989 Grand Prix would effectively be its last. The final event of the Grand Prix was the Nabisco Masters Doubles held at the Royal Albert Hall in London in the United Kingdom from December 6 through December 10, 1989. Its last champions were Jim Grabb and Patrick McEnroe, who beat John Fitzgerald and Anders Järryd.


== Governance ==
The governance of the Grand Prix was led by the Men's International Professional Tennis Council (MIPTC) from 1974 until 1989; its name was shortened to the Men's Tennis Council (MTC) in 1988. The Council's duties included imposing fines for violations of its Code of Conduct, drug testing and administrating the Grand Prix circuit. It also moved the Australian Open from its December date – which had been adopted in 1977 so that it could be included in the Grand Prix points system – to January for the 1987 edition so that the Grand Prix Masters could be held in December from 1986 onwards. However, it failed to reduce or maintain the number of tournaments on the Grand Prix circuit, with 48 Grand Prix events being held in 1974 compared to 75 in 1989.


== Sponsors and Grand Prix tour names ==
Based on USLTA Tennis Yearbooks and Guides and World of Tennis yearbooks the history of sponsors is as follows:
1970 Pepsi-Cola Grand Prix
1971 Pepsi-Cola Grand Prix
1972 Commercial Union Assurance Grand Prix
1973 Commercial Union Assurance Grand Prix
1974 Commercial Union Assurance Grand Prix
1975 Commercial Union Assurance Grand Prix
1976 Commercial Union Assurance Grand Prix
1977 Colgate-Palmolive Grand Prix
1978 Colgate-Palmolive Grand Prix
1979 Colgate-Palmolive Grand Prix
1980 Volvo Grand Prix
1981 Volvo Grand Prix
1982 Volvo Grand Prix
1983 Volvo Grand Prix
1984 Volvo Grand Prix
1985 Nabisco Grand Prix
1986 Nabisco Grand Prix
1987 Nabisco Grand Prix
1988 Nabisco Grand Prix
1989 Nabisco Grand Prix


== Formation of the ATP Tour ==
In 1990 the Association of Tennis Professionals, led by Hamilton Jordan, replaced the Men's Tennis Council as the sole governing body of men's professional tennis and the ATP Tour was born. The nine most prestigious Grand Prix tournaments became known as the Championship Series Single Week from 1990-1995. In 1996 Mercedes then began to sponsor these series of events named as the Super 9 until 1999. In 2000 they became known as the Tennis Masters Series until 2004, then the ATP Masters Series until 2009. Now called the ATP World Tour Masters 1000. Grand Prix tournaments below this level were originally called the Super Series, were retained by the ATP and renamed as the Championship Series. All remaining Grand Prix Tour events became part of the World Series.


== Grand Prix Season End rankings (Top 10) : 1970-1989 ==
NB: All rankings were calculated using the Grand Prix points system and do not necessarily reflect the ATP rankings at the same time.


== See also ==
World Championship Tennis
Association of Tennis Professionals
International Tennis Federation
History of tennis


== Notes ==
^ "ILTF agreement for Grand Prix tennis circuit to start". New York Times. 9 April 1970. Retrieved 20 August 2011. 
^ "How It All Began". ATP. 
^ Jack Kramer, with Frank Deford (1979). The Game : My 40 Years in Tennis. New York: Putnam. pp. 275–276. ISBN 978-0399123368. 
^ "Tennis Gets A Grand Prix". The Sydney Morning Herald. Apr 9, 1970. 
^ "Grand Prix Tennis European Circuit". The Lakeland Ledger. 26 January 1976. Retrieved 20 August 2011. 
^ The Daily Union Newspaper January 15 1989
^ "History". ITF. 
^ "Pepsi Cola Company Sponsorship". New York Times. 23 June 1970. Retrieved 20 August 2011. 
^ "Colgate Palmolive sponsorship". Sydney Morning Herald. 5 November 1976. Retrieved 20 August 2011. 
^ "Volvo Sponsorship". New York Times. 5 September 1988. Retrieved 20 August 2011. 
^ "Nabisco Sponsorship". New York Times. 28 September 1989. Retrieved 20 August 2011. 
^ "Newsbank Archive LA Times Reference to name". Los Angeles Times. 5 March 1990. Retrieved 20 August 2011. 
^ Chloe Francis (May 9, 2009). "Masters 1000 Tournaments: The Toughest Test?". Bleacher Report. 


== Further reading ==
Bud Collins, History of Tennis: An Authoritative Encyclopedia and Record Book, New Chapter Press, USA, 2nd Edition, 2010. ISBN 0942257707. Accessed 10/11/2010.