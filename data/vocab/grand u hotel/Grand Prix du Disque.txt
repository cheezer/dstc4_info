The Grand Prix du Disque is the premier French award for musical recordings. The award was inaugurated by Académie Charles Cros in 1948 and offers prizes in various categories. The categories vary from year to year, and multiple awards are often made in any one category in the same year. Awards are made in the following categories:
Ancient Music
Baroque Music
Blues
Chamber Music
Choral Music
First Recital
French Song
Instrumental and Symphonic Music
Instrumental Soloist (new talent)
Jazz
Lyric Music
Modern Music
Opera
Recordings for Children
Vocal Soloist (new talent)
World Music


== See also ==
Bïa Krieger


== External links ==
http://www.charlescros.org – Official Site