Greek may refer to anything of, from, or related to Greece, a country in Southern Europe.
Greeks, an ethnic group
Greek language, a branch of the Indo-European language family
Proto-Greek language, the assumed last common ancestor of all known varieties of Greek
Mycenaean Greek language, most ancient attested form of the language (16th to 11th centuries BC)
Ancient Greek, forms of the language used c. 1000–330 BC
Koine Greek, common form of Greek spoken and written during Classical antiquity
Medieval Greek or Byzantine Greek, language used between the Middle Ages and the Ottoman conquest of Constantinople
Modern Greek, varieties spoken in the modern era (from 1453 AD)

Greek alphabet, script used to write the Greek language
Greek Orthodox Church, several Churches of the Eastern Orthodox Church
Ancient Greece, the classical civilization centered in Greece


== Other uses ==
Greek may also refer to:
Greek (play), 1980 play by Steven Berkoff
Greek (opera), 1988 opera by Mark-Antony Turnage, based on Steven Berkoff's play
Greek (TV series), an ABC Family show
Greeks (finance), quantities representing the sensitivity of the price of derivatives
Greeking, style of displaying or rendering text or symbols in a computer display or typographic layout
Greek system or fraternities and sororities, social organizations for undergraduate students at North American colleges
Greek Theatre (Los Angeles), a theatre located at Griffith Park in Los Angeles, California
Greek love, a term referring variously to male bonding, homosexuality, pederasty and anal sex
The Greeks 1951 non-fiction book on classical Greece by HDF Kitto
The Greeks, a 1980 cycle of ten plays adapted by John Barton and Kenneth Cavander from the works of Homer, Euripides, Aeschylus and Sophocles


== See also ==
Greek dialects (disambiguation)
Hellenic (disambiguation)
Names of the Greeks, terms for the Greek people
Name of Greece, names for the country
Greek to me, an idiom for something not understandable