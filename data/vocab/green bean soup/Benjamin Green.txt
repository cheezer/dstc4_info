Benjamin Green, Ben Green, Benny Green or Bennie Green may refer to:


== Musicians ==
Bennie Green (1923–1977), American jazz trombonist
Benny Green (saxophonist) (1927–1998), English jazz saxophonist
Benny Green (pianist) (born 1963), American hard bop artist
Ben Green (composer) (born 1964), Israeli producer and songwriter


== Others ==
Benjamin Green (merchant) (1713–1772), Colonial American and Canadian administrator and judge
Benjamin Green (architect) (c. 1811–1858), English architect
Benny Green (footballer) (1883–1917), English inside-forward
Ben Charles Green (1905–1983), American jurist
Ben K. Green (1912–1974), American writer
Ben Green (comedian) (born 1973), English actor and comedian
Ben Green (mathematician) (born 1977), English mathematician
Benny Green, a character in BBC TV series Grange Hill played by Terry Sue-Patt


== See also ==
Benjamin Greene (disambiguation)