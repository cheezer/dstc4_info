The following is a list of products branded by Nokia.


== Current products and services ==


=== Products by Nokia Networks ===
Nokia Networks is a multinational data networking and telecommunications equipment company headquartered in Espoo, Finland and wholly owned subsidiary of Nokia Corporation.
The list of products is available here: Nokia Networks - Solutions, Services & Products


=== Products by Nokia Technologies ===
Nokia Technologies develops and licenses innovations. It also licenses Nokia brand to third party manufacturers.
The list of phones is:
Nokia N1


=== Products by Microsoft ===
After the sale of the Nokia mobile phones business to Microsoft, smartphones were transferred under the Microsoft brand. However, Microsoft still markets feature phones under the Nokia brand.
The list of phones is available here.


== Past products and services ==


=== Mobile phones ===
Boldface: Smartphones
S.: status, where
P indicates under production and
D indicates discontinued.
DCT (Digital Core Technology) 1, 2, 3 and 4 are generations of internal Nokia technology.


==== The Mobira/Nokia series (1982–1990) ====
The earliest phones produced by Nokia. These all used 1G networks.


==== Original series (1992–1999) ====
The earliest 2G phones by Nokia.


==== 4-digit series (1994–2010) ====


===== Nokia 1xxx – Ultrabasic series (1996–2010) =====
The Nokia 1000 series include Nokia's most affordable phones. They were mostly targeted towards developing countries and users who do not required advanced features beyond making calls and SMS text messages, alarm clock, reminders, etc.


===== Nokia 2xxx – Basic series (1994–2010) =====
Like the 1000 series, the 2000 series are entry-level phones. However, the 2000 series generally contained more advanced features than the 1000 series; many 2000 series phones feature color screens and some feature cameras, Bluetooth and even A-GPS, GPS such as in the case of the Nokia 2710.


===== Nokia 3xxx – Expression series (1997–2009) =====
The Nokia 3000 series are mostly mid-range phones targeted towards the youth market. Some of the models in this series are targeted towards young male users, in contrast with the more unisex business-oriented 6000 series and the more feminine fashion-oriented 7000 series.
Nokia 4xxx
The Nokia 4000 series was skipped as a sign of deference from Nokia towards East Asian customers.


===== Nokia 5xxx – Active series (1998–2010) =====
The Nokia 5000 series is similar in features to the 3000 series, but often contains more features geared toward active individuals. Many of the 5000 series phones feature a rugged construction or contain extra features for music playback.


===== Nokia 6xxx – Classic Business series (1997–2010) =====

The Nokia 6000 series is Nokia's largest family of phones. It consists mostly of mid-range to high-end phones containing a high amount of features. The 6000 series is notable for their conservative, unisex designs, which make them popular among business users.
Nokia 6136 UMA is the first mobile phone to include Unlicenced Mobile Access. Nokia 6131 NFC is the first mobile phone to include Near Field Communication.


===== Nokia 7xxx – Fashion and Experimental series (1999–2010) =====
The Nokia 7000 series is a family of Nokia phones with two uses. Most phones in the 7000 series are targeted towards fashion-conscious users, particularly towards women. Some phones in this family also test features. The 7000 series are considered to be a more consumer-oriented family of phones when contrasted to the business-oriented 6000 series.
The 7110 was the first Nokia phone with a WAP browser. WAP was significantly hyped up during the 1998–2000 Internet boom. However WAP did not meet these expectations and uptake was limited. Another industry first was the flap, which slid from beneath the phone with a push from the release button. Unfortunately the cover was not too durable. The 7110 was also the only phone to feature a navi-roller key.
The 7250i was a slightly improved version of the Nokia 7250. It includes XHTML and OMA Forward lock digital rights management. The phone has exactly the same design as the 7250. This phone is far more popular than the 7250 and has been made available on pre-paid packages and therefore it is very popular amongst youths in the UK and other European countries.
The 7510 Supernova was a phone exclusive to T-Mobile USA. Only some units of this model have Wi-Fi chips with UMA. The Wi-Fi adapter on this phone supports up to WPA2 encryption if present. This phone uses Xpress-On Covers.
The 7650 was the first Series 60 smartphone of Nokia. It was quite basic compared to smartphones, it didn't have MMC slot, but it had a camera.
The 7610 was Nokia's first smartphone featuring a megapixel camera (1,152x864 pixels), and is targeted towards the fashion conscious individual. End-users can also use the 7610 with Nokia Lifeblog. Other pre-installed applications include the Opera and Kodak Photo Sharing. It is notable for its looks, having opposite corners rounded off. It comes with a 64 MB Reduced Size MMC. The main CPU is an ARM compatible chip (ARM4T architecture) running at 123 MHz.
The 7710's 640x320 screen was a touch screen phone.


===== Nokia 8xxx – Premium series (1996–2007) =====
This series is characterized by ergonomics and attractiveness. The internals of the phone are similar to those in different series and so on that level offer nothing particularly different, however the physical handset itself offers a level of functionality which appeals to users who focus on ergonomics. The front slide keypad covers offered a pseudo-flip that at the time Nokia were unwilling to make. Materials used increased the cost and hence exclusivity of these handsets.
The only exception to the rule (there are many in different series) is the 82xx, 8310 which were very small and light handsets.


===== Nokia 9xxx – Communicator series (1996–2007) =====

The Nokia 9000 series was reserved for the Communicator series, but the latest Communicator, the E90 Communicator, is an Eseries phone.


==== Lettered series: C/E/N/X (2005–2011) ====


===== Cseries (2010–2011) =====

The Nokia Cseries is an affordable series optimized for social networking and sharing. The range includes a mix of feature phones running Series 40 and some smartphones running Symbian.
C1-00 and C2-00 are dual SIM phones, but with Nokia C1-00 both SIM cards cannot be utilized at the same time.


===== Eseries (2006–2011) =====

The Nokia Eseries is an enterprise-class series with business-optimized products. They are all smartphones and run on Symbian.


===== Nseries (2005–2011) =====

The Nseries are highly advanced smartphones, with strong multimedia and connectivity features and as many other features as possible into one device.
Note:
Although part of the Nseries, the Nokia N800 and N810 Internet Tablets did not include phone functionality. See the Internet Tablets section.
The N950 was meant to be the N9-00 with the old N9 'Lankku' being N9-01, however the N9-00 model number was used for the all touch 'Lankku' with the original design being the MeeGo developer-only N950.
Nokia N1 Tablet doesn't carries Nseries Logo.


===== Xseries (2009–2011) =====

The Nokia Xseries targets a young audience with a focus on music and entertainment. Like the Cseries, it is a mix of both Series 40 feature phones and Symbian smartphones.


==== Worded series: Asha/Lumia/X (2011–2015) ====


===== Asha (2011–2014) =====

The Nokia Asha series is an affordable series optimized for social networking and sharing, which Nokia market as "connecting the next billion" (referring to people in Third World nations who use the mobile internet for the first time). All phones run Series 40 except Asha 50x phones, which run on the Nokia Asha platform.


===== Lumia (2011–2015) =====

Lumia is a series of smartphones running Windows Phone. It also includes the Nokia Lumia 2520, a Windows RT-powered tablet computer.
The series was sold to Microsoft in 2014 who now brands these products under the name Microsoft. Devices with Microsoft branding are not listed here.


===== X (2014) =====
The Nokia X family is a range of Android smartphones from Nokia. These were the first ever Nokia phones to run on an Android OS.


==== 3-digit series (2011–present) ====
Lower first digit - entry-level, classic mobile phones platform(with relatively long work on battery). Higher first digit - high end mobile phones for ex. last Symbian, known for one of the best cameras that times - Nokia 808. All phones released since 2014 are manufactured by Microsoft.


==== Other phones ====


===== N-Gage – Mobile gaming devices (2003–2004) =====


===== PCMCIA Cardphones (2001–2003) =====


===== Concept phones =====

Nokia developed a phone concept, never realised as a working device, in the 2008 Nokia Morph.


=== Services ===
The list of services provided by Nokia.
After the sale of its mobile devices and services division to Microsoft, all of the below services were either discontinued, spinned off or re-branded to Lumia or Microsoft.


==== Consumer services ====


==== Nokia imaging apps ====


==== HERE-based ====


==== Desktop apps ====


==== Humanitarian services ====


==== Developer tools ====


==== Websites ====


==== Video gaming ====


=== Security ===
IP appliances run Nokia IPSO FreeBSD based operating system, work with Check Point's firewall and VPN products.
In 2004, Nokia began offering their own SSL VPN appliances based on IP Security Platforms and the pre-hardened Nokia IPSO operating system. Client integrity scanning and endpoint security technology was licensed from Positive Networks.
Nokia 50s
Nokia 105s
Nokia 500s


=== Internet Tablets ===

Nokia's Internet Tablets were designed for wireless Internet browsing and e-mail functions and did not include phone capabilities. The Nokia N800 and N810 Internet Tablets were also marketed as part of Nseries. See the Nseries section.
The Nokia N900, the successor to the N810, has phone capabilities and is not officially marketed as an Internet Tablet, but rather as an actual Nseries smartphone.


=== ADSL modems ===


=== GPS products ===


=== WLAN products ===


=== Digital television ===


=== Military communications and equipment ===
Nokia has developed the Sanomalaitejärjestelmä ("Message device system") for the Finnish Defence Forces. It includes:
Sanomalaite M/90
Partiosanomalaite
Keskussanomalaite
For the Finnish Defence forces Nokia manufactured also:
AN/PRC-77 portable combat-net radio transceiver (under licence, designated LV 217)
M61 gas mask


=== Telephone switches ===


=== TETRA ===

Nokia sold its Professional Mobile Radio business to Cassidian, a division of EADS.


=== Personal computers ===
In the 1980s, Nokia's personal computer division Nokia Data manufactured a series of personal computers by the name of MikroMikko.
Nokia's PC division was sold to the British computer company ICL in 1991. In 1990, Fujitsu had acquired 80% of ICL plc, which throughout the decade became wholly the part of Fujitsu. Personal computers and servers were marketed under the ICL brand; the Nokia MikroMikko line of compact desktop computers continued to be produced at the Kilo factories in Espoo, Finland. Components, including motherboards and Ethernet network adapters were manufactured locally, until production was moved to Taiwan. Internationally the MikroMikko line was marketed by Fujitsu as the ErgoPro.
In 1999, Fujitsu Siemens Computers was formed as a joint venture between Fujitsu Computers Europe and Siemens Computer Systems, wherein all of ICL's hardware business (except VME mainframes) was absorbed into the joint venture. On 1 April 2009, Fujitsu bought out Siemens' share of the joint venture, and Fujitsu Siemens Computers became Fujitsu Technology Solutions. Fujitsu continues to manufacture computers in Europe, including PC mainboards developed and manufactured in-house.


=== Mini laptops ===

On 24 August 2009, Nokia announced that they will be re-entering the PC business with a high-end mini laptop called the Nokia Booklet 3G. It was discontinued few years later.


=== Computer displays ===
Nokia was also known for producing very high quality CRT and early TFT LCD Multigraph displays for PC and larger systems application. The Nokia Display Products' branded business was sold to ViewSonic in 2000.


=== Others ===
Nokia has also produced below products.
During 1990s, Nokia divested itself of below industries to focus solely on telecommunications.


== See also ==
Nokian Footwear
Nokian Tyres
Nokia phone series
List of Motorola products
List of Sony Ericsson products


== References ==


== External links ==
Nokia – Phone Software Update
All Nokia DCT and BB generation list
Nokia museum (over 300 phones)
Nokia graphical timeline (1982 to 2006)
Nokia Mobile Reviews