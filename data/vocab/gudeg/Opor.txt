Opor is a type of dish cooked and braised in coconut milk from Indonesia, especially from Central Java. In Indonesia the term opor refer to the method of cooking in coconut milk. Opor is a popular dish for lebaran or Eid ul-Fitr, usually eaten with ketupat and sambal goreng ati (beef liver in sambal). In Yogyakarta chicken or egg opor often eaten with gudeg and rice.


== Ingredients ==
The main ingredient is meat (chicken, eggs or beef) cooked in coconut milk with a mixture of spices consisting of palm sugar, lemongrass, lemon leaf, ground garlic, shallot, candlenut, black pepper, galangal, coriander, and cooking oil. The meat is braised on a small flame until it's cooked and the coconut milk has thickened. Opor has a whitish color and a watery and almost soupy coconut milk-like consistence. Other similar dishes are sayur lodeh, gulai and kari, since all employ coconut milk. However, opor uses less spices, and the absence of turmeric and chilli pepper make opor maintain its whitish coconut milk-like color.


== Variants ==
The most common opor is opor ayam (chicken opor), although other ingredients are also can be cooked as opor. Opor daging (beef), opor bebek (duck), opor telur (hard boiled chicken egg), and opor tahu (tofu) are examples of opor variants.


== References ==