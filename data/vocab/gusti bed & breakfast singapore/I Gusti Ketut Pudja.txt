I Gusti Ketut Pudja (19 May 1908-4 May 1977) was an Indonesian Politician, and also first governor of Lesser Sunda. He was recognized as National Hero of Indonesia by Republic Indonesia Government on 7 November 2011.


== Early life ==
Pudja was born in 19 May 1908 in Singaraja, Bali. In 1934, he graduated from Rechtshoogeschool (Law High School) in Batavia. In 1935, he started working in Bali and Lombok Residency Office in Singaraja. A year later, he was placed in the Raad van Kerta, a court in Bali at that time.


== Independence struggle ==
Pudja was the member of the Panitia Persiapan Kemerdekaan Indonesia in 7 August 1945, as the continuation from Badan Penyelidik Usaha Persiapan Kemerdekaan Indonesia. This body was chaired by Soekarno. Independence was declared on August 17, 1945, two days after Japan surrendered to the Allied Forces in the end of Pacific War.


== Post-Independence ==
Pudja was appointed by the new created Republic of Indonesia as the first Governor of Lesser Sunda in 22 August 1945. He was also chairman of Supreme Audit Agency (Badan Pemeriksa Keuangan) in 1960-1968.


== Honour ==
Mahaputra Utama Medal (Bintang Mahaputra Utama), 1992
Indonesian National Hero, 2011


== References ==