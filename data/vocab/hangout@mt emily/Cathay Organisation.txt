Cathay Organisation Holdings Limited is one of Singapore's leading leisure and entertainment groups. It has the first THX cinema hall and digital cinema in Singapore. The group has operations in Singapore and Malaysia.


== History ==


=== The early years ===

Cathay Organisation was established on 18 July 1935 by Dato Loke Wan Tho and his mother, Mrs Loke Cheng Kim as Associated Theatres Ltd. In 1936, the company opened its first cinema in Kuala Lumpur, Malaysia, the Pavilion, with 1200 seats. The year 1939 saw the landmark Cathay Building open in Singapore with the Cathay Cinema premiering Sir Alexander Korda's Four Feathers.
Cathay was the country's first air-conditioned cinema i.e. Cathay Cinema and the first skyscraper, the tallest in Southeast Asia during that time. It was the first time patrons could watch movies air-conditioned and sitting in a comfortable armchair. It was also used as a landmark by pilots as a final approach before landing. Three years later, it showed its last show and was converted into a Red Cross casualty station. After the end of war in 1945, Cathay reopened and it was Singapore's first cinema to show American and British pictures as its first screening after closure. It signed a joint venture with another company to distribute mobile films to villages and estates in Malaysia. The group installed a new air-conditioning plant in 1948 and they started to acquire many cinemas in Singapore and Malaysia.


=== Studio operations ===
In 1953, Cathay-Keris Films was formed. The studio, situated in out in East Coast Road, produced its own films in the Malay. These were former barracks occupied by the Japanese and were converted into processing labs, sound studios, offices and even a canteen. That year, Cathay-Keris produced Singapore's first colour film, Buloh Perindu. More than 100 films were made at Cathay Keris's studios between 1953 to 1973 including five Pontianak movies; Pontianak (1957), Dendam Pontianak (1957), Sumpah Pontianak (1958), Pontianak Kembali (1963) & Pontianak Gua Musang (1964). Sadly the first two classic Pontianak films are long lost.
Dato Loke Wan Tho acquired Yung Hwa Studio in Hong Kong in 1955. In 1956, Motion Picture & General Investment Co Ltd (MP & GI) was formed. It took over the running of Yung Hwa Studios in Hong Kong. MP & GI was later renamed Cathay Organisation (HK). Some of MP & GI's most popular stars were Ge Lan also known as Grace Chang, You Min, Lin Dai, Peter Chen Ho, Chang Yang, Loh Ti, Lin Tsui and many more.
In the 50s and 60s, under Dato Loke Wan Tho, Cathay Organisation produced many award-winning and popular shows from both its Hong Kong and Singapore studios. Today, the Cathay Classic Film Library has more than 300 Mandarin, Cantonese and Malay titles.


=== Growth of the cinema empire ===
Associated Theatres Ltd. was renamed Cathay Organisation Pte Ltd in 1959. In 1964, Dato Loke Wan Tho died in a plane crash in Taiwan. His brother-in-law Choo Kok Leong took over the chairmanship of the company.
In 1965, Cathay Organisation established Singapore's first public bowling centre, Jackie's Bowl. Cathay Organisation, then a major shareholder of Jackie's Bowl, diversified into the bowling arena by acquiring the remaining shares in Jackie's Bowl and operating it under the name of Orchard Bowl. The company was subsequently renamed Cathay Bowl Pte Ltd in 1995.
Cathay's Jurong Drive-In cinema opened on 14 July 1971 and was, at that time, the first of its kind in Singapore and Malaysia, and the largest in Asia. The 5.6 hectare site was equipped with 899 speaker stands, 300 seats at the gallery for walk-in customers and the screen was elevated 25 feet from the ground. Cathay distributed Golden Harvest films and when Jurong Drive-In screened Bruce Lee's Big Boss, the line-up of cars was so long that many got out and walked. The show was scheduled for midnight but didn't start screening until 1am.
By the 70s, Cathay's cinema empire expanded to 75 cinemas in Singapore and Malaysia. It was also a reputable film distributor, with networks spanning Malaysia, Myanmar, Thailand, Vietnam, Hong Kong and Taiwan, and as far as Europe and Latin America.


=== Recession ===
Responding to the changing economic environment of the 1980s, Cathay Organisation rationalised its cinema business, selling out their cinema operations in Malaysia. In Singapore, Cathay closed the only drive-in cinema in Jurong and chose to move with the trends and operated only cineplexes. It converted its stand-alone cinemas into multi-screenswhere.


=== Revival of Cathay ===
In 1990, Cathay opened The Picturehouse, Singapore's first arthouse cinema and the following year, Cathay Cinema was converted into a 3 screen cineplex. The 1990s saw Cathay Bowl opening bowling alleys in major parts of the island. Cathay Properties was incorporated, providing property management and maintenance services for facilities operated by Cathay itself, as well as commercial and residential properties held by its affiliated companies in 1994. Orchard Cinema closed its doors in 1995 for redevelopment. In 1997, Cathay Cineleisure Orchard opened at the former Cathay Cinema site with the 6-screen Cathay Cineplex. That same year, Cathay opened its 6-screen cineplex, Mega Pavilion Cinema in Kompleks Bukit Jambul in Penang, Malaysia. A year later, Mega Pavilion Cinema opened at Megamal Pinang, Penang.

Cathay opened a 34 lane bowling alley in Endah Parade, Kuala Lumpur in 1998.
The Orchard Cineplex (Hall 3 & 6) and The Picturehouse were named "The Best Screens" in a survey conducted by The Straits Times in 1998. In a "Nominate your favourite cinema" contest conducted by Adpost, Cathay Cineplex Orchard was voted the runaway favourite. Mega Pavilion in Kompleks Bukit Jambul was voted the Second best cinema in Malaysia by listeners on a Malaysian radio channel.
In 1999, Cathay Organisation Holdings Ltd became a public-listed company on the SESDAQ with five wholly owned subsidiaries; Cathay Cineplexes Pte Ltd (cineplex operations), Cathay Bowl Pte Ltd (bowling centre operations), Cathay-Keris Films Pte Ltd (film acquisition & distribution), Cathay Cineleisure International Pte Ltd (entertainment centre management) and Cathay Properties Pte Ltd (property management services). The company also launched a new corporate identity.
Cathay Bowl became Singapore's first bowling centre operator to be awarded ISO 9002 certification by the Productivity and Standards Board (PSB). Cathay took a 19% stake in ACEL International Holdings Ltd, which offered educational enrichment programmes and it was renamed Cathay ACEL Pte Ltd.
In 1999, Singapore's first bowling alley to offer cosmic bowling was opened by Cathay Bowl; the 24-lane Cathay Bowl at SAFRA Tampines.
That same year, Cathay Cineplex Causeway Point opened marking Cathay Cineplexes' first foray into the suburban districts.


=== New millennium ===
Entering the new millennium, Cathay opened its five-screen Mega Pavilion Cinema at City Square in Johor Bahru, Malaysia in 2000.
Over in Singapore, Cathay received license from the Singapore Broadcasting Authority to operate video-on demand service in Singapore from June 2001. The company also launched its automated phone booking system for movie tickets while Cathay Cineplex Cineleisure Orchard underwent a facelift. Cathay Cineplexes became the first cineplex operator in Singapore to achieve ISO 9002 certification. That same year, the Cathay Building on Handy Road closed for re-development.
The following year, Cathay Cineplexes launched it e-Ticketing facility on its website for customers to purchase movie tickets online. Cathay Bowl opened 24 lanes at SAFRA Yishun and 22 lanes at Cathay Cineleisure Orchard. Cathay Bowl became, at that time, Singapore's largest bowling centre operations with 8 bowling centres and a total of 178 lanes. To promote the sport, the Cathay Bowling Academy was established with structured training programmes by qualified bowling coaches.
Also in 2001, Cathay opened two Mega Pavilion Cinemas in Penang; a four-screen cineplex at Prangin Mall and 5-screen cineplex at Island Plaza.

In 2002, Cathay Bowl opened 24 lanes at THE CHEVRONS club while Cathay Cineplexes introduced its drive-thru ticketing kiosk at Cathay Cineleisure Orchard, a self-service automated ticketing kiosk at Cathay Cineplex Orchard lobby and also introduced SMS ticketing.
The following year, the historical Chinatown Majestic Theatre was converted into a shopping centre. Cathay opened The Majestic in February.
Mega Pavilion Cineplex at Bukit Jambul in Malaysia went through a facelift. At the same time, in Singapore, EZ-Link payment was introduced at Cathay Cineplex Orchard and Cathay Bowl providing an alternative payment for customers.
That same year, Cathay Cineplex launched Singapore's 1st Digital Cinema. Cathay Cineplexes became the first to introduce commercial films in digital format in Singapore. It also opened three more halls at Orchard and became the first cineplex to introduce 24-hour movie screening in Singapore.
Hangout Hotels was incorporated in this same year and opened its first "no frills, just fun" concept hotel; hangout @ mt emily.
Cathay ACEL was renamed Cathay Ed-Ventures Pte Ltd in 2004 and shareholding was increased to 95% stake. Cathay Ad-House Pte Ltd was formed to provide advertising and marketing to the group's business units.
More new initiatives were introduced by Cathay Cineplexes. A 24-hour movie express automated ticketing kiosk was opened at Orchard MRT Station and dispenses movie tickets at time of purchase. Cathay Cineplexes also introduced the first PDA ticketing system in November 2004 for its cineplexes in Singapore.
Cathay hosted and organised the first Malaysian Film Festival in Singapore showcasing cutting edge Malaysian-made films to the Singapore audience. Received with much enthusiasm and support, this festival continues to be on Cathay's annual calendar of events.
Cathay-Keris Films formed a joint venture with Prime Movies International. The joint venture company, Cathay Prima Pte Ltd, was a collaboration to develop and produce theatrical and television films and series, including titles from cathay-Keris' classic film library.
In 2005, hangout @ mt emily becomes a licensed Hostelling International (HI) operator for the Youth Hostels Association Singapore, YHA(S), which is an associate organisation of the International Youth Hostel Federation (IYHF).
In a collaboration with F & N Coca Cola Singapore, the Coke Red Lounge, the first of its kind outside of the USA, opened at Cathay Cineplex Cinelesiure Orchard.
In July, Cathay Cineplex Cineleisure Orchard emerged tops in 10 of the 12 categories in The New Paper poll for best seats, best popcorn, best sound etc.
Cathay Cineplexes Sdn Bhd's fourth Mega Pavilion Cinemas in Malaysia was opened in Kota Kinabalu, the capital city of Sabah.
That same year, Cathay opened the largest cyber games centre E2Max @ Cine L9 at Cathay Orchard Cineleisure. E2Max @ Cine-L9 features over 200 PCs and consoles, including Xbox and internet surfing services, with different genres of games in varying configurations and levels for single or multi-player gameplay, a special ladies game area, 20 enclosed private cyber-gaming rooms, the Moto Music Zone and food & beverage facilities.
To complement E2Max @ L9 and to meet with consumer demands, Cathay Cineplexes opened three new E-cinemas at Cathay Cineleisure Orchard. These electronically enabled cinema halls allow for alternative showcase alternative forms of entertainment i.e. live sporting events like the World Cup, Premier League and Champions League matches, awards ceremonies like the Oscars and Golden Globes and even local entertainment events like Singapore Idol. With these 3 new screens, Cathay Cineplexes Cineleisure Orchard had largest number of screens under one roof.
On 23 March, redeveloped Cathay building, now known as The Cathay, hosted a private party to over 1,000 invited guests to mark its opening. The Cathay opened to the public the next day with 8 cinema halls, including The Grand Cathay & The Picturehouse. The Cathay also features retails and food and beverage outlets. Opening in the second phase are The Cathay Residences.
In May, Cathay Cineplexes hosted a party to mark the opening of its flagship cineplex at Cineleisure Damansara in Kuala Lumpur. Cathay Cineplex Damansara with 10 screens opened and in conjunction with the opening, Cathay Cineplexes effected its change in name from Mega Pavilion Cinemas to Cathay Cineplexes.
In June 2006, Cathay Organisation effected its voluntary delisting from SESDAQ.
In October 2006, The Cathay Gallery at The Cathay on Handy Road opened. The Cathay Gallery features the history of the Loke family and their various business interests in Malaysia and Singapore. A collection of memorabilia related to their businesses and personal interests are displayed in the gallery.
In November 2006, Cathay Organisation rationalised its business operations and sold its bowling operations to Family Leisure Pte Ltd.
Today, Cathay Organisation has subsidiaries in Singapore and Malaysia, in cinema operations, film distribution, film acquisitions, hotel management, investment holding, advertising, events management and property management.


== Subsidiaries ==
Cathay Cineplexes Pte Ltd
Cathay-Keris Films Pte Ltd
Cathay Cineleisure International Pte Ltd
Cathay Properties Pte Ltd
Cathay Consultancy Services Pte Ltd
Cathay Ed-Ventures Pte. Ltd
Cathay Leisure Holdings Pte. Ltd.
Hangout Hotels International Pte Ltd
Cathay Ad-House Pte. Ltd.
Cathay Organisation Holdings (M) Sdn. Bhd.
Cathay Cineplexes Sdn. Bhd
Camp Out Sdn. Bhd
Cathay Ad-House Sdn. Bhd.
E2Max Centre Pte Ltd


== Cathay Cineplexes ==
Cathay Cineplexes Singapore manages Cathay Cineleisure Orchard, The Cathay, Cathay Cineplex Causeway Point, Cathay Cineplex AMK Hub, Cathay Cineplex Downtown East, Cathay Cineplex West Mall and Cathay Cineplex Jem. Cathay Cineplexes Sdn Bhd in Malaysia manages cineplexes in Johor Bahru, Penang, Kuala Lumpur and Kota Kinabalu.
Cathay Cineplexes is not part of the Cinematograph Film Exhibitors Association.


=== Singapore ===
Cathay Cineplexes runs 60 screens in 7 locations in Singapore currently.


=== Malaysia ===
Mega Pavilion Cinemas, which was renamed Cathay Cineplexes Sdn Bhd in September 2004, became an integral part of Cathay Cineplexes' operations when its parent company, Cathay Organisation Holdings Ltd, acquired the entire share capital of Cathay Organisation Holdings (M) Sdn Bhd in 2002.
Cathay Cineplexes Sdn Bhd operates and manages 26 screens in Malaysia. Cathay Cineplex e@Curve (formally Cineleisure Damansara) has 16 screens while Cathay Cineplex City Square Johor Bahru has 10 screens. These cineplexes are all equipped with digital sound systems, plush cinema seats and a computerised ticketing system with seat selection features.


== Hangout Hotels International ==
Hangout Hotels is a budget hotel chain of Cathay Organisation.


== See also ==
List of cinemas in Malaysia
List of cinemas in Singapore


== References ==

Cathay Website Corporate History


== External links ==
Cathay Organisation Singapore Website