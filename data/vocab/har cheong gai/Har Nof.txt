Har Nof (Hebrew: הר נוף‎, lit. scenic mountain) is a neighborhood on a hillside on the western boundary of Jerusalem, Israel, with a population of 20,000 residents, primarily Orthodox Jews.


== History ==
In Talmudic times, Har Nof was an agricultural settlement that served Jerusalem. Remains of ancient wine presses, farmhouses and terraces built 1,500 years ago have been unearthed on the outskirts of Har Nof. The first homes in modern Har Nof were built in the early 1980s.


== Geography ==

Har Nof is a terraced neighborhood on the slopes of a mountain that sits 813 meters above sea level. Nearby are the remains of the Palestinian village of Deir Yassin, the site of the Deir Yassin massacre in April 1948 in which 100-120 Palestinian Arab villagers died in the attack by Lehi and Irgun. Due to the topography, many of the multi-storey apartment buildings have entrances on both sides of the building – one to reach the lower floors and another to reach the higher floors. Some streets are connected by long flights of stairs. At the foot of Har Nof lies the 1,200 dunam Jerusalem Forest (Yaar Yerushalayim), planted in the 1950s as a green lung around the city.


== Demography ==

The majority of the residents of Har Nof are Orthodox Jews, both Haredi and Dati Leumi. Many residents are recent olim. The neighborhood has a large community of English-speaking olim, including notable French-speaking and Spanish-speaking communities. There are also communities of Ger and Vizhnitz Hasidim, as well many Sephardi and Mizrahi Jews. The former Sefardic chief rabbi and leader of the Shas party, Rabbi Ovadia Yosef, lived in Har Nof. Spiritual leaders of the Ashkenazi Haredi community who reside in Har Nof are Rabbi Moishe Sternbuch of the Edah HaChareidis; the Bostoner Rebbe, Rabbi Mayer Alter Horowitz of Congregation Givat Pinchas (The Boston Shul); Rabbi Beryl Gershenfeld, Rosh Yeshiva of Har Nof's Machon Yaakov and Machon Shlomo yeshivas; and Rabbi Yitzchak Mordechai Rubin of Kehilat Bnei Torah.


== Synagogues and public institutions ==

Rabbi David Yosef is the head of the Yachveh Da'at Kollel and the chief rabbi of Har Nof. Har Nof has a large number of synagogues, yeshivas and Torah study institutions, among them the Central Synagogue Imrei Shefer, Heichal Hatorah, Yeshiva Pachad Yitzchok, Machon Shlomo, Yeshivat Lev Aharon and Machon Yaakov. The campuses of Neve Yerushalayim and She'arim College of Jewish Studies for Women are located in Har Nof, as is Yechaveh Da'at, Rabbi Ovadia Yosef's synagogue and spiritual headquarters.
On 18 November 2014, an attack occurred at the Kehilat Bnei Torah synagogue. Two Palestinian attackers from East Jerusalem entered the synagogue with knives, a meat cleaver and a pistol, inflicting heavy wounds on their victims who were at morning prayers, killing four and injuring eight - four of them seriously. In the ensuing gun battle, the two attackers were shot dead and one of the policemen who attended the scene later died of his wounds.


== Transportation ==
The neighborhood is linked to the city center by Kanfei Nesharim and Beit Hadfus Streets, with a number of bus lines providing public transportation.


== Economy ==
Har Nof has recently had an upswing in financial firms opening offices in the area. Har Nof has been described as a "financial hub" in the city of Jerusalem. Moville Finance started this trend by opening an office on Ibn Denan, with JP Morgan Chase following soon after.


== Communal activism ==
The residents of Har Nof founded Shomera, a non-profit environmental protection association to thwart the building of high-rise luxury towers that would block the view of the Jerusalem Forest. Emergency medical care in Har Nof is provided by the volunteer group Hachovesh. Em Habanim is a volunteer organization founded in 1995 by Malka Yarom, a Har Nof resident who opened her home to religious divorcees who had nowhere to take their children on the Sabbath. The organization now has a membership of 300 and offers support to single parent families in the Haredi sector.


== References ==


== External links ==
har nof website (English)