Harbour View is a community in Saint Andrew Parish, Jamaica. It is administered by the Kingston and St. Andrew Corporation and is served by the Kingston 17 Post Office.


== Geography ==
Harbour View is southeast of Long Mountain and south of Dallas Mountain. The centre of Harbour View is near the point where the Palisadoes spit meets the mainland, making Harbour View the eastern extreme of Kingston Harbour. To the east of this, also within Harbour View, is the mouth of the Hope River.


== Buildings ==

The Harbour View Stadium, home stadium of the Harbour View F.C., is toward the north of Harbour View.
Fort Nugent was built in the 18th century to the north west of Harbour View to defend Jamaica from invasion. The sole remnant of Fort Nugent is a Martello tower still standing on the site.
There is a Moravian church at the western end of Harbour View.


== Notable people from Harbour View ==
Donald Quarrie, gold-medal winner of the 200 metre track event at the 1976 Summer Olympics, is from Harbour View. The Donald Quarrie High School in Harbour View is named in his honour.


== Harbour View in popular culture ==
In the novel Doctor No by Ian Fleming, James Bond loses a car following him from Palisadoes Airport by turning right at Harbour View. His pursuers assume he has turned left, toward Kingston.


== References ==