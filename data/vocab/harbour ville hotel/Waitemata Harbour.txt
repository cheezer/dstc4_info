Waitematā Harbour is the main access by sea to Auckland, New Zealand. For this reason it is often referred to as Auckland Harbour and is crossed by the Auckland Harbour Bridge, despite the fact that it is one of two harbours surrounding the city. The Waitematā forms the north and east coasts of the Auckland isthmus. It is matched on the south by the shallower waters of Manukau Harbour.
With a size of 70 square miles, it connects the city's main port and the Auckland waterfront to the Hauraki Gulf, and the Pacific Ocean. It is sheltered from Pacific storms by Auckland's North Shore, Rangitoto Island and Waiheke Island.
The name is from the Māori language, with Wai te Mataa referring to obsidian glass. The 'sparkling waters' (a later translation of the meaning) of the harbour were said to glint like the volcanic glass prized by these early arrivals to the harbour.


== Overview ==

The harbour is an arm of the Hauraki Gulf, extending west for eighteen kilometres from the end of the Rangitoto Channel. Its entrance is between North Head and Bastion Point in the south. The westernmost ends of the harbour extend past Whenuapai in the northwest, and to Te Atatu in the west, as well as forming the estuarial arm known as the Whau River in the southwest.
The north shore of the harbour is formed by North Shore City, one of several separate cities within Greater Auckland. Suburbs located close to the shore here include Birkenhead, Northcote and Devonport (west to east). To the south is the heart of Auckland City, with the Auckland waterfront, as well as coastal suburbs such as Mission Bay, Parnell, Herne Bay and Point Chevalier (east to west), the latter of which lies on a short triangular peninsula jutting into the harbour.
The harbour is crossed at its narrowest point by the Auckland Harbour Bridge. To the east of its southern end lie the marinas of Westhaven, as well as the suburbs of Freemans Bay and the Viaduct Basin. Further east from these, and close to the harbour's entrance, lies the Port of Auckland.
There are other wharves and ports within the harbour, notable among them the Devonport Naval Base, and its accompanying ammunition dump at Kauri Point, Birkenhead, and the Chelsea Sugar Refinery wharf, all capable of taking ships over 500 gross register tons (GRT). Smaller wharves at Birkenhead, Northcote, Devonport and West Harbour offer commuter ferry services to the Auckland CBD.


== Geology ==
The harbour is in fact a drowned valley system in marine sediments deposited during the Miocene. Recent volcanism in the Auckland volcanic field has also shaped the coast, most obviously at Devonport and the Meola Reef (a lava flow which almost spans the harbour), but also in the explosion craters of Orakei Basin and in western Shoal Bay. In periods of low sea level, a tributary ran from Milford into the Shoal Bay stream. This valley provided the harbour with a second entrance when sea levels rose, until the Lake Pupuke volcano plugged this gap.
The current shore is strongly influenced by tidal rivers, particularly in the west and north of the harbour. Mudflats covered by mangroves flourish in these conditions, and salt marshes are also typical.


== History ==

The harbour has long been the main anchorage and port area for the Auckland area, even before European colonial times. Well sheltered not only by the Hauraki Gulf itself but also by Rangitoto Island, the harbour offered good protection in almost all winds, and lacked dangerous shoals or major sand bars (like on the Manukau Harbour) that would have made entry difficult. The harbour also proved a fertile area for encroaching development, with major land reclamation undertaken, especially along the Auckland waterfront, within a few decades of the city's European founding.
Taking the idea of the several Māori portage paths over the isthmus one step further, a potential Waitematā Harbour-Manukau Harbour canal was considered in the early 1900s, and legislation, the Auckland and Manukau Canal Act 1908, was passed that would allow authorities to take privately owned land where it was deemed required for a canal. However, no serious work (or land take) was undertaken. The act was repealed on 1 November 2010.


== References ==