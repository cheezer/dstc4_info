HarbourFront MRT Station (NE1/CC29) is an underground Mass Rapid Transit interchange of the North East Line and the Circle Line in Singapore. It is located in the south of Singapore next to the HarbourFront Centre, previously known as the Singapore World Trade Centre. It serves as a terminal station of the North East Line and the Circle Line. This station is also one of the most convenient in a sense that passengers do not have to walk long distances to interchange between lines.


== Location ==
HarbourFront station is located next to the HarbourFront Centre and VivoCity, and is connected to Sentosa by cable car. Buses to and from Sentosa do so from the HarbourFront Bus Interchange next to the station. When the Sentosa Express monorail line to Sentosa began service on 15 January 2007 as a virtual interchange, Sentosa became more effectively linked with most of the towns of Singapore. The station also provides public transport to the prominent tourist destination of Mount Faber, of which HarbourFront lies in its shadow, and lies within the Bukit Merah planning area.


== History ==
HarbourFront station opened on 20 June 2003 with the rest of the North East Line. The week after VivoCity opened, weekend passenger traffic doubled to more than 60,000 passengers each day, surpassing Dhoby Ghaut, formerly the busiest station on the North East Line. Due to the sudden jump in passenger traffic, the number of faregates facing the linkway to VivoCity had been increased by two from seven. The Circle Line section of the station began revenue service on 8 October 2011, replacing Marymount as a terminal station for the Circle Line which makes it a terminal station for two lines, becoming the first MRT station in Singapore to be a double-line terminus station.
The North East Line's overrun tunnel at this station will be extended by 50 metres, to the tune of an estimated S$8.2 million. Works has started and expected to be completed by the end of 2014. The station's operations are unaffected by the works as most of the enhancement works are done away from the station itself.


== Art in Transit ==
The station features two sets of artwork under the Art in Transit programme. The interior of the North East Line station and walkways to station exits feature the artwork Enigmatic Appearances by Ian Woo. Abstract images on blue enamel panels evoke a sense of open seas, a reference to the station's proximity to the sea. The second piece of artwork, Commuting Waves by Jason Ong, features in the Circle Line station. Using commuter traffic data of HarbourFront station on weekdays and weekends, the artist created two sets of two-dimensional glass waveforms resembling fishes, tying in with the station's water theme.


== Cultural references ==
The station was one of four stations featured in the Uniquely Singapore edition of Monopoly.


== Station layout ==


== Exits ==
A: HarbourFront Bus Interchange
B: HarbourFront Centre
C: VivoCity, Sentosa Express, Sentosa Boardwalk
D: Mount Faber Park
E: HarbourFront Centre, VivoCity


== Transport connections ==


=== Rail ===


== References ==


== External links ==
SBS Transit's HarbourFront MRT station official website
SMRT's HarbourFront MRT station official website
Changi Airport to HarbourFront MRT station Route Map