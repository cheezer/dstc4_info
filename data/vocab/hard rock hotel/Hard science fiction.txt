Hard science fiction is a category of science fiction characterized by an emphasis on scientific accuracy or technical detail, or on both. The term was first used in print in 1957 by P. Schuyler Miller in a review of John W. Campbell, Jr.'s Islands of Space in Astounding Science Fiction. The complementary term soft science fiction (formed by analogy to "hard science fiction") first appeared in the late 1970s. The term is formed by analogy to the popular distinction between the "hard" (natural) and "soft" (social) sciences. The science fiction critic Gary Westfahl argues that neither term is part of a rigorous taxonomy—instead they are approximate ways of characterizing stories that reviewers and commentators have found useful.
Stories revolving around scientific and technical consistency were written as early as 1870s, with the publication of Jules Verne's Twenty Thousand Leagues Under the Sea in 1870 and Around the World in 80 Days in 1873, among other stories. Although not always accurate, and though Verne himself denied writing as a scientist or seriously predicting machines and technology of the future, the attention to detail in his work became an inspiration for many future scientists and explorers.
Today, the term "soft science fiction" is also often used to refer to science fiction stories which lack any scientific focus or rigorous adherence to known science. The categorization "hard science fiction" represents a position on a broad continuum—ranging from "softer" to "harder".


== Scientific rigor ==

The heart of the "hard SF" designation is the relationship of the science content and attitude to the rest of the narrative, and (for some readers, at least) the "hardness" or rigor of the science itself. One requirement for hard SF is procedural or intentional: a story should try to be accurate, logical, credible and rigorous in its use of current scientific and technical knowledge about which technology, phenomena, scenarios and situations that are practically and/or theoretically possible. For example, the development of concrete proposals for spaceships, space stations, space missions, and a US space program in the 1950s and 1960s influenced a widespread proliferation of "hard" space stories. Later discoveries do not necessarily invalidate the label of hard SF, as evidenced by P. Schuyler Miller, who called Arthur C. Clarke's 1961 novel A Fall of Moondust hard SF, and the designation remains valid even though a crucial plot element, the existence of deep pockets of "moondust" in lunar craters, is now known to be incorrect.
There is a degree of flexibility in how far from "real science" a story can stray before it leaves the realm of hard SF. Some authors scrupulously avoid such technology as faster-than-light travel, while others accept such notions (sometimes referred to as "enabling devices", since they allow the story to take place) but focus on realistically depicting the worlds that such a technology might make possible. In this view, a story's scientific "hardness" is less a matter of the absolute accuracy of the science content than of the rigor and consistency with which the various ideas and possibilities are worked out.
Readers of "hard SF" often try to find inaccuracies in stories, a process which Gary Westfahl says writers call "the game". For example, a group at MIT concluded that the planet Mesklin in Hal Clement's 1953 novel Mission of Gravity would have had a sharp edge at the equator, and a Florida high-school class calculated that in Larry Niven's 1970 novel Ringworld the topsoil would have slid into the seas in a few thousand years. The same book famously featured a devastating inaccuracy: the eponymous Ringworld is not (in) a stable orbit and would crash into the sun without active stabilization. Niven fixed these errors in his sequel The Ringworld Engineers, and noted them in the foreword.


== Representative works ==

Arranged chronologically by publication year.


=== Short stories ===
Hal Clement, "Uncommon Sense" (1945)
James Blish, "Surface Tension" (1952), (Book 3 of The Seedling Stars [1957])
Tom Godwin, "The Cold Equations" (1954)
Poul Anderson, "Kyrie" (1968)
Frederik Pohl, "Day Million" (1971)
Larry Niven, "Inconstant Moon" (1971) and "The Hole Man" (1974)
Greg Bear, "Tangents" (1986)
Geoffrey A. Landis, "A Walk in the Sun" (1991)
Vernor Vinge, "Fast Times at Fairmont High" (2001)


=== Novels ===
Hal Clement, Mission of Gravity (1953)
John Wyndham, The Outward Urge (1959)
Arthur C. Clarke, A Fall of Moondust (1961), Rendezvous with Rama (1972)
Stanislaw Lem, Solaris (1961)
Poul Anderson, Tau Zero (1970)
Joe Haldeman, The Forever War (1974)
James P. Hogan, The Two Faces of Tomorrow (1979)
Robert L. Forward, Dragon's Egg (1980)
Robert Silverberg (editor), Murasaki (1992)
Stephen Baxter, Ring (1996)
Charles Sheffield, Between the Strokes of Night (1985)
Carl Sagan, Contact (1985)
Kim Stanley Robinson, The Mars trilogy (Red Mars (1992), Green Mars (1993), Blue Mars (1996))
Ben Bova, Grand Tour series (1992–2009)
Catherine Asaro, Primary Inversion (1995, 2012)
Linda Nagata, The Nanotech Succession (1995-1998)
Nancy Kress, Beggars in Spain (1993)
Greg Egan, Schild's Ladder (2002)
Alastair Reynolds, Pushing Ice (2005)
Paul J. McAuley, The Quiet War (2008)
Andy Weir, The Martian (2014)
Neal Stephenson, Seveneves (2015)


=== Films ===
2001: A Space Odyssey (1968)
Solaris (1968)
The Andromeda Strain (1971)
Silent Running (1972)
Stalker (1979)
2010: The Year We Make Contact (1984) — sequel to 2001
Contact (1997)
Gattaca (1997)
Eternal Sunshine of the Spotless Mind (2004)
Primer (2004)
Children of Men (2006)
The Man from Earth (2007)
Moon (2009)
Robot and Frank (2012)
Gravity (2013)
Her (2013)
Ex Machina (2015)


=== Anime / Manga ===
Yukinobu Hoshino, 2001 Nights (1984, 1986)
They Were Eleven (1986)
Royal Space Force: The Wings of Honnêamise (1987)
Patlabor 2: The Movie (1993)
Makoto Yukimura, Planetes (1999, 2004)
Flag (2006)
Pale Cocoon (2006)
Dennō Coil (2007)
Moonlight Mile (2007)
Eden of the East (2009)


=== Comics ===
Ringworld: The Graphic Novel, Part One (2014)
Ringworld: The Graphic Novel, Part Two (2015)
The Forever War, Vol. 1: Private Mandella (1990)
The Forever War 2: Lieutenant Mandella (2020-2203) (No. 2) (1991)
The Forever War 3 (No. 3) (1992)
Robot (The Sanatorium of Dr. Vliperdius/Mortal Engines); Timof Comics (based upon stories by Stanislaw Lem) (2013)


== See also ==
Hard fantasy
Hard science
Interstellar travel in fiction
Soft science fiction


== Notes ==


== References ==


== Further reading ==
On Hard Science Fiction: A Bibliography, originally published in Science Fiction Studies #60 (July 1993).
David G. Hartwell, "Hard Science Fiction,", Introduction to The Ascent of Wonder: The Evolution of Hard Science Fiction, 1994, ISBN 0-312-85509-5
Kathryn Cramer's chapter on hard science fiction in The Cambridge Companion to SF, ed. Farah Mendlesohn & Edward James.
Westfahl, Gary (1996-02-28). Cosmic Engineers: A Study of Hard Science Fiction (Contributions to the Study of Science Fiction and Fantasy). Greenwood Press. ISBN 0-313-29727-4. 
A Political History of SF by Eric Raymond
The Science in Science Fiction by Brian Stableford, David Langford, & Peter Nicholls (1982)
David N. Samuelson, "Hard SF", pp. 194–200, The Routledge Companion to Science Fiction, 2009.


== External links ==
Hard Science Fiction Exclusive Interviews
Kheper Realism scale
Science Fiction Stories with Good Astronomy & Physics: A Topical Index
The Ascent of Wonder by David G. Hartwell & Kathryn Cramer. Story notes and introductions.
The Ten Best Hard Science Fiction Books of all Time, selected by the editors of MIT's Technology Review, 2011
"Low-Level Science fiction: Sci-fi with hard science and a literary slant"