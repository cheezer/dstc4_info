Chin Haw or Chin Ho (Thai: จีนฮ่อ) are Chinese people who migrated to Thailand via Burma or Laos. Most of them were from Yunnan, the southern province of China.


== Migration ==
Generally, the Chin Haw can be divided into three groups according to the time of their migration.
In nineteenth century, the Qing army had sent troops to suppress the rebellion in Yunnan, known as the Panthay Rebellion, which caused up to 1,000,000 lives lost - both civilians and soldiers. During this time, many people fled to the Shan state in Burma, then to the north of Thailand.
There were Chinese merchants who traded between Yunnan, Burma and Lanna. Some of them decided to settle down along this trade route.
After the Chinese revolution in 1949 AD, the 93rd Corps, which supported the Kuomintang party, fled to Burma and to the north of Thailand


== Religion ==
The majority is Han Chinese and follows Chinese folk religion or Buddhism. And approximately one-third is Muslim, also known as Hui people or Hui Muslim.


== Activities ==
Muslim Chin Haw have links to triad secret societies in cooperating in the drug trade, working with other Chinese groups in Thailand like the Teo-Chiew and Hakka and the 14K Triad. They engaged in the heroin trade. Ma Hseuh-fu, from Yunnan province, was one of the most prominent Chin Haw heroin drug lords, his other professions included trading in tea and a hotelier.
The Muslim Chin Haw are the same ethnic group as the Panthay in Burma who are also descendants of Hui Muslims from Yunnan province, China.


== See also ==
Baan Haw Mosque
Attaqwa Mosque


== References ==