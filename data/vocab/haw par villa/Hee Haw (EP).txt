Hee Haw is the second release and first EP by the Australian post-punk band The Boys Next Door (later renamed The Birthday Party). The Hee Haw EP was released in 1979 by the independent label, Missing Link Records.


== Background ==
By 1978-9 The Boys Next Door were the most experienced and musically-mature of Melbourne’s punk and post-punk bands. The core members – Nick Cave, Mick Harvey, Phill Calvert and Tracy Pew – had been joined by Rowland S. Howard on guitar in 1978. The Boys Next Door released an album (Door, Door) on the Mushroom label in 1979 (recorded in separate sessions in mid-1978 and early 1979).
After the release of Door Door, The Boys Next Door transferred to the independent label, Missing Link Records, and took on label owner Keith Glass as their manager. Cave said, "We played [Hee Haw] to Michael Gudinski of Mushroom Records and he wasn't really interested." The tracks for the Hee Haw EP were recorded during July and August 1979 at Richmond Recorders in Melbourne, engineered by Tony Cohen. The five-track EP was released in December 1979. The artwork on the cover and insert sheet was done by Marcus Bergner.


== The music ==
The Hee Haw EP represents a musical departure from the relatively conventional punk/pop songs on Door Door. The EP introduces a more abrasive, rhythmic and exploratory sound that was further developed when the band re-located to the UK (and changed their name to The Birthday Party).
Two of the songs on the EP, "The Red Clock" and "The Hair Shirt," the former sung by Rowland S. Howard and the latter by Nick Cave, were included on the Birthday Party's self-titled album in 1980. The songs on the EP and The Birthday Party appeared on the 1988 compilation album, also titled Hee Haw.


== Track listing ==
"A Catholic Skin" (Nick Cave)
"The Red Clock" (Rowland S. Howard)
"Faint Heart" (Nick Cave)
"Death By Drowning" (Rowland S. Howard)
"The Hair Shirt" (Nick Cave)


== Re-release ==
The EP was reissued in Australia in December 1983 by Missing Link (ING008), credited to The Birthday Party. It was also reissued by the same label in 1987 credited to Boys Next Door. The tracks from the original Hee Haw EP were re-released in 1989 as part of a compilation of early recordings under the name of The Birthday Party; This album was also called Hee Haw.


== Band members ==
Nick Cave − vocals, sax
Mick Harvey − guitar, piano
Rowland S. Howard − guitar, vocals
Tracy Pew − bass
Phill Calvert − drums


== References ==


== External links ==
The Birthday Party lyrics from Nick Cave Online
Nick Cave discography pre-2000