Sharp Haw is a hill on the southern edge of the Yorkshire Dales, located just within the National Park. Being the last outpost of the hills before the broad Aire valley, and with a sharp summit from many angles, it is prominent and easily recognisable from much of the area to the south. The hill has an elevation of 357 metres (1,171 ft).
From the summit there are views over the Aire valley, to Gargrave, Skipton and the Leeds and Liverpool Canal, to the east into Wharfedale and to the north into Malhamdale.
Although Ordnance Survey maps do not show a path to the summit, there is a path from the public bridleway to the north: both are clear on the ground, and popular with cyclists.
The word Haw comes from the Old English hawian, and means view.


== References ==


== External links ==