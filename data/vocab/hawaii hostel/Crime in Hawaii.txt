This article refers to crime in the state of Hawaii.


== Statistics ==
In 2008 there were 49,516 crimes reported in Hawaii, including 25 murders, 46,004 property crimes, and 365 rapes.


== Capital punishment laws ==
Capital punishment is not applied in this state.


== References ==