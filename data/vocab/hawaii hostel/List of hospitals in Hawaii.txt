List of hospitals in Hawaii (U.S. state), sorted by hospital name.

Cancer Institute of Maui - Wailuku, Hawaii
Castle Medical Center - Kailua, Hawaii
Hawaii Pacific Health - Honolulu, Hawaii
Hawaii State Hospital - Kaneohe, Hawaii
Hilo medical center- Hilo,Hawaii
Kaiser Foundation Hospital - Honolulu, Hawaii
Kapi'olani Medical Center for Women & Children - Honolulu, Hawaii
Ka'u Hospital - Pahala, Hawaii
Kauai Veterans Memorial Hospital - Waimea, Hawaii
Kohala Hospital - Kapaau, Hawaii
Kona Community Hospital - Kealekekua, Hawaii
Kula Hospital - Kula, Hawaii
Kuakini Medical Center - Honolulu, Hawaii
Lanai Community Hospital - Lanai City, Hawaii
North Hawaii Community Hospital - Kamuela, Hawaii
Pali Momi Medical Center - Honolulu, Hawaii
The Queen's Medical Center - Honolulu, Hawaii
Samuel Mahelona Memorial Hospital - Kapaa, Hawaii
Shriners Hospital - Honolulu, Hawaii
Straub Clinic & Hospital - Honolulu, Hawaii
Tripler Army Medical Center - Honolulu, Hawaii
Wahiawa General Hospital - Wahiawa, Hawaii
Wilcox Memorial Hospital - Lihue, Hawaii; Kauai, Hawaii