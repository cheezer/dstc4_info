ALTIBASE HDB is a relational database management system manufactured by Altibase Corporation.


== Features ==
ALTIBASE HDB is a so-called "hybrid DBMS", meaning that it simultaneously supports access to both memory-resident and disk-resident tables via a single interface. It is compatible with Solaris, HP-UX, AIX, DEC, Linux, and Windows. It supports the complete SQL-92 standard, features MVCC (Multi-Version Concurrency Control), implements Fuzzy and Ping-Pong Checkpointing for periodically backing up memory-resident data, and ships with Replication and Database Link functionality.


== History ==
2000
Altibase acquired an in-memory database engine from the Electronics and Telecommunications Research Institute in February 2000, and commercialized the database in October of the same year.
2001
Altibase changed the name of the in-memory database product from "Spiner" to "Altibase".
2005
Altibase integrated the in-memory database with a disk-resident database to create a hybrid DBMS. Altibase claims that this is the world's first hybrid DBMS.
2011
Altibase renamed the hybrid DBMS "ALTIBASE HDB".
2012
Altibase released version 6.1.1 in April 2012.


== Utilities ==
Adapter for Oracle
For copying data that have been modified in ALTIBASE HDB to an Oracle database.
AdminCenter
A GUI for managing and monitoring ALTIBASE HDB
Audit
A tool for resolving replication conflicts
CheckServer
For monitoring the database server process and executing a script if it has terminated
iLoader
For downloading and uploading table data. Typically used during database migration tasks.
iSQL
A command-line utility for executing SQL statements
Migration Center
A GUI tool for migrating data from other database products to ALTIBASE HDB
Replication Manager
A GUI tool for simplifying replication management tasks


== Application Development Interfaces ==
ALTIBASE HDB includes the following application development interfaces:
JDBC
For connecting to ALTIBASE HDB via JDBC
PHP
For integrating PHP pages with ALTIBASE HDB via ODBC
Perl
A Perl DBD (Database Driver)
.NET Data Provider
For connecting to ALTIBASE HDB via applications based on the Microsoft .NET Framework
OLE DB Driver
For connecting to ALTIBASE HDB via the OLE DB interface
X/Open XA Interface
For integrating ALTIBASE HDB in a distributed transaction environment
iLoader API
An API for the bundled iLoader utility
CheckServer API
An API for the bundled CheckServer utility


== References ==
^ a b "Administrator's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ Park, Hee-Bom. "기술이전 성공사례 (Technology Transfer Success Story)" etnews October 16, 2002. Last retrieved on August 7, 2012.
^ "주목 e기업 – 알티베이스 (Noteworthy e-Business: Altibase)" Digital Times February 7, 2011. Last retrieved on August 7, 2012.
^ a b Kang, Dong-Sik. "알티베이스, 증권시장 90% 점유 국산 DBMS 강자 (Altibase, Korean database powerhouse, occupies 90% of domestic securities database market)" Digital Times February 6, 2011. Last retrieved on August 7, 2012.
^ "Adapter for Oracle User's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "Admin Center User's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "Audit User's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "Utilities Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "iLoader User's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "iSQL User's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "Migration Center User's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "Replication Manager User's Manual". Altibase Corporation. Retrieved July 25, 2012. 
^ "Application Program Interface User's Manual". Altibase Corporation. Retrieved July 25, 2012. 


== External links ==
ALTIBASE HDB product description on Altibase Website