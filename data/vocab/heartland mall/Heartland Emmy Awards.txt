Heartland Emmy Awards are a division of the National Academy of Television Arts and Sciences. The Highlands Ranch, Colorado division was founded in 1986. In addition to granting the Heartlands Emmy Awards, this division awards scholarships, honors industry veterans at the Silver Circle Celebration, conducts National Student Television Awards of Excellence, has a free research and a nationwide job bank. The chapter also participates in judging Emmy entries at the regional and national levels.


== Boundaries ==
The Heartland chapter was formed in 1986 after an intense effort to meet the high eligibility standards of the National Academy's national board, and serves the television industry in these markets/DMAs:


== References ==