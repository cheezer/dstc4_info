Galu Hanna (Persian: گلوحنا‎, also Romanized as Galū Ḩannā, Gelū Ḩanā, and Gelū Ḩennā’; also known as Galū, Hanā’, Ḩanā, Hana, Ḩannā’, and Ḩennā’) is a village in Mardehek Rural District, Jebalbarez-e Jonubi District, Anbarabad County, Kerman Province, Iran. At the 2006 census, its population was 321, in 61 families.


== References ==