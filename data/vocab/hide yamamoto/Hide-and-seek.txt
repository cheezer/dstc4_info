Hide-and-seek (British English or American English) or hide-and-go-seek (American English) is a popular children's game in which any number of players conceal themselves in the environment, to be found by one or more seekers. The game is played by one player chosen (designated as being "it") closing their eyes and counting to a predetermined number (such as counting to 10, 20, etc.) while the other players hide. After reaching the number (such as reaching 10, 20, etc.), the player who is "it" attempts to say, "Ready or not, here I come!" and then to locate all concealed players.
The game can end in one of several ways. In the most common variation of the game, the player chosen as "it" locates all players as the players are not allowed to move; the player found last is the winner and is chosen to be "it" in the next game. Another common variation has the seeker counting at "home base"; the hiders can either remain hidden or they can come out of hiding to race to home base; once they touch it, they are "safe" and cannot be tagged. But if the seeker tags another player before reaching home base, that person becomes "it."
The game is an example of an oral tradition, as it is commonly passed down and up by children to younger and older children.


== Variants ==
Different versions of the game are played around the world, under a variety of names. One derivative in game is called "Sardines", in which only one person hides and the others must find them, hiding with them when they do so. The hiding places become progressively more cramped, like sardines in a tin. The last person to find the hiding group is the loser. A. M. Burrage calls this version of the game "Smee" in his 1931 ghost story of the same name.
In some versions of the game, after the first player is caught or if no other players can be found over a period of time, "it" calls out a pre-agreed phrase (such as "Olly olly oxen free") to signal the other hiders to return to base for the next round. In another version, when players are caught they help the "it" seek out others.
In one variant, once all hiders have been located, the game then becomes a game of tag where the "it" chases after all the other players and the first person tagged becomes the "it".
In another, the hiders who are found help the "it" track down the remaining hiders, but the first person to be found becomes the next "it."
In Australia, the game is often called "44 Homes." The hiders hide until they are spotted by the seeker, who chants, "Forty, Forty, I see you" (sometimes shortened to "Forty, forty, see you"). Once spotted, the hider must run to "home base" (where the "it" was counting while the other players hid) and touch it before she or he is "tipped" (tagged, or touched) by the seeker. If tagged, that hider becomes the new "it."
In India, hide-and-seek is played differently - if any of the 'hiders' touch the seeker and says Dhappa, then the seeker has to count again. However, if the seeker sees the hider before they manage to touch him/her and say dhappa, then that hider will be 'it' the next round, unless some other hider manages to 'Dhappa' the seeker without being seen.
In Brazil and Russia, hide-and-seek has an extra step. The "it" starts counting with eyes closed and facing the wall while everyone hides. Once the "it" finds someone, they must race to the spot where the "it" was originally counting and facing the wall and whoever touches that spot first, wins the game.


== See also ==
Princess and monster game
Search game


== References ==