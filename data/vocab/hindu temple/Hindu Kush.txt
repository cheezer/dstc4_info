The Hindu Kush (/kʊʃ, kuːʃ/; Pashto, Persian and Urdu: هندوکش‎), also known in Sanskrit as Pāriyātra Parvata and in Ancient Greek as the Caucasus Indicus Ancient Greek: Καύκασος Ινδικός) or Paropamisadae (Ancient Greek: Παροπαμισάδαι), is an 800-kilometre-long (500 mi) mountain range that stretches between central Afghanistan and northern Pakistan. It is a western subrange of the Himalayas. It divides the valley of the Amu Darya (the ancient Oxus) to the north from the Indus River valley to the south.
The highest point in the Hindu Kush is Tirich Mir or Terichmir at 7,708 metres (25,289 ft) in the Chitral District of Khyber Pakhtunkhwa, Pakistan. To the east, the Hindu Kush buttresses the Pamir Mountains near the point where the borders of China, Pakistan and Afghanistan meet, after which it runs southwest through Pakistan and into Afghanistan, finally merging into minor ranges in western Afghanistan. The mountain range separates Central Asia from South Asia.


== Origin of name ==
The origins of the name Hindu Kush are uncertain, with multiple theories being propounded by different scholars and writers. In the time of Alexander the Great, the Hindu Kush range was referred to as the Caucasus Indicus or the "Caucasus of the Indus River" (as opposed to the Greater Caucasus range between the Caspian and Black Seas), and some past authors have considered this as a possible derivation of the name Hindu Kush. However, many other theories have been propounded by different scholars and writers for the origins of the modern name Hindu Kush. Hindū Kūh (ھندوکوه) and Kūh-e Hind (کوهِ ھند) are usually applied to the entire range separating the basins of the Kabul and Helmand Rivers from that of the Amu Darya, or, more specifically, to that part of the range lying northwest of Kabul. Sanskrit documents refer to the Hindu Kush as Pāriyātra Parvata.
The mountain range was called "Paropamisadae" by Hellenic Greeks in the late first millennium BC.
Other sources state that the term Hindu Kush originally applied only to the peak in the area of the Kushan Pass, which had become a center of the Kushan Empire by the first century.
The Persian-English dictionary indicates that the word 'koš' [kʰoʃ] is derived from the verb ('koštan' کشتن [kʰoʃˈt̪ʰæn]), meaning to kill. Although the derivation is only a possible one, some authors have proposed the meaning "Kills the Hindu" for "Hindu Kush", a derivation that is reproduced in Encyclopedia Americana:
The name Hindu Kush means "kills the Hindu", a reminder of the days when Indian slaves from the Indian subcontinent died in the harsh weather typical of the Afghan mountains while being transported to Central Asia. The World Book Encyclopedia states that "the name Kush ... means Death", while Encyclopædia Britannica says "The name Hindu Kush first appears in 1333 AD in the writings of Ibn Battutah, the medieval Berber traveller, who said the name meant 'Hindu Killer', a meaning still given by Afghan mountain dwellers who are traditional enemies of Indian plainsmen."
The word Koh or Kuh means "mountain" in some local language Khowar. According to Nigel Allan, Hindu Kush meant both "mountains of India" and "sparkling snows of India", as he notes, from a Central Asian perspective.


== History ==

The mountains have historical significance in the Indian subcontinent and China. There has been a military presence in the mountains since the time of Darius the Great. The Great Game of the 19th century often involved military, intelligence and/or espionage personnel from both the Russian and British Empires operating in areas of the Hindu Kush. The Hindu Kush were considered, informally, the dividing line between Russian and British areas of influence in Afghanistan.
During the Cold War the mountains again became militarized, especially during the 1980s when Soviet forces and their Afghan allies fought the mujahideen. After the Soviet withdrawal, Afghan warlords fought each other and later the Taliban and the Northern Alliance and others fought in and around the mountains.
The American and ISAF campaign against Al Qaeda and their Taliban allies has once again resulted in a major military presence in the Hindu Kush.
Alexander the Great explored the Afghan areas between Bactria and the Indus River after his conquest of the Achaemenid Empire in 330 BCE. It became part of the Seleucid Empire before falling to the Indian Maurya Empire around 305 BCE.

Alexander took these away from the Persians and established settlements of his own, but Seleucus Nicator gave them to Sandrocottus (Chandragupta), upon terms of intermarriage and of receiving in exchange 500 elephants.

Indo-Scythians expelled the Indo-Greeks by the mid 1st century BCE, but lost the area to the Kushan Empire about 100 years later.

Before the Christian era, and afterwards, there was an intimate connection between the Kabul Valley and India. All the passes of the Hindu-Kush descend into that valley; and travellers from the north as soon as they crossed the watershed, found a civilization and religion, the same as that which prevailed in India. The great range was the boundary in those days and barrier that was at times impassable. Hindu-Kuh—the mountain of Hind—was similarly derived.

Pre-Islamic populations of the Hindu Kush included Shins, Yeshkun, Chiliss, Neemchas Koli, Palus, Gaware, Yeshkuns, Krammins, Indo-Scythians, Bactrian Greeks, and Kushans.


== Mountains ==

The mountains of the Hindu Kush system diminish in height as they stretch westward: Toward the middle, near Kabul, they extend from 4,500 to 6,000 meters (14,800 to 19,700 ft); in the west, they attain heights of 3,500 to 4,000 meters (11,500 to 13,100 ft). The average altitude of the Hindu Kush is 4,500 meters (14,800 feet).
The Hindu Kush system stretches about 966 kilometres (600 mi) laterally, and its median north-south measurement is about 240 kilometres (150 mi). Only about 600 kilometres (370 mi) of the Hindu Kush system is called the Hindu Kush mountains. The rest of the system consists of numerous smaller mountain ranges including the Koh-e Baba, Salang, Koh-e Paghman, Spin Ghar (also called the eastern Safēd Kōh), Suleiman Range, Siah Koh, Koh-e Khwaja Mohammad and Selseleh-e Band-e Turkestan. The western Safid Koh, the Malmand, Chalap Dalan, Siah Band and Doshakh are commonly referred to as the Paropamise by western scholars, though that name has been slowly falling out of use over the last few decades.
Rivers that flow from the mountain system include the Helmand River, the Hari River and the Kabul River, watersheds for the Sistan Basin.
Numerous high passes ("kotal") transect the mountains, forming a strategically important network for the transit of caravans. The most important mountain pass is the Salang Pass (Kotal-e Salang) (3,878 m); it links Kabul and points south of it to northern Afghanistan. The completion of a tunnel within this pass in 1964 reduced travel time between Kabul and the north to a few hours. Previously access to the north through the Kotal-e Shibar (3,260 m) took three days. The Salang tunnel at 3,363 m and the extensive network of galleries on the approach roads were constructed with Soviet financial and technological assistance and involved drilling 1.7 miles through the heart of the Hindu Kush.
Before the Salang road was constructed, the most famous passes in the Western historical perceptions of Afghanistan were those leading to India. They include the Khyber Pass (1,027 m), in Pakistan, and the Kotal-e Lataband (2,499 m) east of Kabul, which was superseded in 1960 by a road constructed within the Kabul River's most spectacular gorge, the Tang-e Gharu. This remarkable engineering feat reduced travel time between Kabul and the Pakistan border from two days to a few hours.
Lataband Road

The roads through the Salang and Tang-e Gharu passes played critical strategic roles during the U.S. invasion of Afghanistan and were used extensively by heavy military vehicles. Consequently, these roads are in very bad repair. Many bombed out bridges have been repaired, but numbers of the larger structures remain broken. Periodic closures due to conflicts in the area seriously affect the economy and well-being of many regions, for these are major routes carrying commercial trade, emergency relief and reconstruction assistance supplies destined for all parts of the country.

There are a number of other important passes in Afghanistan. The Wakhjir Pass (4,923 m), proceeds from the Wakhan Corridor into Xinjiang, China, and into Northern Areas of Pakistan. Passes which join Afghanistan to Chitral, Pakistan, include the Baroghil (3,798 m) and the Kachin (5,639 m), which also cross from the Wakhan. Important passes located farther west are the Shotorgardan (3,720 m), linking Logar and Paktiya provinces; the Bazarak (2,713 m), leading into Mazari Sharif; the Khawak Pass (4,370 m) in the Panjsher Valley, and the Anjuman Pass (3,858 m) at the head of the Panjsher Valley giving entrance to the north. The Hajigak (2,713 m) and Unai (3,350 m) lead into the eastern Hazarajat and Bamyan Valley. The passes of the Paropamisus in the west are relatively low, averaging around 600 meters; the most well-known of these is the Sabzak between the Herat and Badghis provinces, which links the western and northwestern parts of Afghanistan.
These mountainous areas are mostly barren, or at the most sparsely sprinkled with trees and stunted bushes. Very ancient mines producing lapis lazuli are found in Kowkcheh Valley, while gem-grade emeralds are found north of Kabul in the valley of the Panjsher River and some of its tributaries. The famous 'balas rubies', or spinels, were mined until the 19th century in the valley of the Ab-e Panj or Upper Amu Darya River, considered to be the meeting place between the Hindu Kush and the Pamir ranges. These mines now appear to be exhausted.


=== Eastern Hindu Kush ===
The Eastern Hindu Kush range, also known as the High Hindu Kush range, is mostly located in northern Pakistan and the Nuristan and Badakhshan provinces of Afghanistan. The Chitral District of Pakistan is home to Tirich Mir, Noshaq, and Istoro Nal, the highest peaks in the Hindu Kush. The range also extends into Ghizar, Yasin Valley, and Ishkoman in Pakistan's Northern Areas.
Chitral is considered to be the pinnacle of the Hindu Kush region. The highest peaks, as well as countless passes and massive glaciers, are located in this region. The Chiantar, Kurambar, and Terich glaciers are amongst the most extensive in the Hindu Kush and the meltwater from these glaciers form the Kunar River, which eventually flows south into Afghanistan and joins the Bashgal, Panjshir, and eventually the much smaller Kabul River.


== Highest mountains ==


== Hindu Kush Peoples and Languages ==
The word "Hindu Kush" derived from (Sanskrit: पारियात्र पर्वत), and (accient Greek:Καυκάσου ινδόκοκκος), or (ancient Greek:Καύκασος Ινδικός), Caucasus Indicus, or Hindu Kush are the peoples of Chitral.


== See also ==
Tirich Mir
Paropamisadae
Himalayas
A Short Walk in the Hindu Kush
Geography of Afghanistan
Geography of Pakistan
Hindustan
List of highest mountains (a list of mountains above 7,200m)
List of mountain ranges
Mount Imeon
2002 Hindu Kush earthquakes
2005 Hindu Kush earthquake


== References ==


=== Bibliography ===
Biddulph, John. Tribes of the Hindoo Koosh (Sang-e-Meel, 2001)


=== Further reading ===
Drew, Frederic (1877). The Northern Barrier of India: A Popular Account of the Jammoo and Kashmir Territories with Illustrations. Frederic Drew. 1st edition: Edward Stanford, London. Reprint: Light & Life Publishers, Jammu, 1971
Gibb, H. A. R. (1929). Ibn Battūta: Travels in Asia and Africa, 1325–1354. Translated and selected by H. A. R. Gibb. Reprint: Asian Educational Services, New Delhi and Madras, 1992
Gordon, T. E. (1876). The Roof of the World: Being the Narrative of a Journey over the High Plateau of Tibet to the Russian Frontier and the Oxus Sources on Pamir. Edinburgh. Edmonston and Douglas. Reprint: Ch’eng Wen Publishing Company. Tapei, 1971
Leitner, Gottlieb Wilhelm (1890). Dardistan in 1866, 1886 and 1893: Being An Account of the History, Religions, Customs, Legends, Fables and Songs of Gilgit, Chilas, Kandia (Gabrial) Yasin, Chitral, Hunza, Nagyr and other parts of the Hindukush, as also a supplement to the second edition of The Hunza and Nagyr Handbook. And An Epitome of Part III of the author's 'The Languages and Races of Dardistan'. Reprint, 1978. Manjusri Publishing House, New Delhi. ISBN 81-206-1217-5
Newby, Eric. (1958). A Short Walk in the Hindu Kush. Secker, London. Reprint: Lonely Planet. ISBN 978-0-86442-604-8
Yule, Henry and Burnell, A. C. (1886). Hobson-Jobson: The Anglo-Indian Dictionary. 1996 reprint by Wordsworth Editions Ltd. ISBN 1-85326-363-X
A Country Study: Afghanistan, Library of Congress
Hindu Kush at Encyclopædia Iranica
Encyclopedia Britannica, 15 th Ed, Vol.21, pp. 54–55, 1987
An Advanced History of India, by R.C.Majumdar, H.C.Raychaudhuri, K.Datta, 2nd Ed., MacMillan and Co, London, pp. 336–37, 1965
Encyclopedia Britannica, 15 th Ed, Vol.21, p. 65, 1987
The Cambridge History of India, Vol.IV - The Mughul Period, by W.Haig & R.Burn, S.Chand & Co., New Delhi, pp. 98–99, 1963


== External links ==
Khyber Pass
Early Explorers of the Hindu Kush