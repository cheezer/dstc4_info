The Hindu is an English-language Indian daily newspaper. Headquartered at Chennai, The Hindu was published weekly when it was launched in 1878, and started publishing daily in 1889. It is the second most circulated English-language newspaper in India, with average qualifying sales of 1.39 million copies (as of December 2013). According to the Indian Readership Survey in 2012, it was the third most widely read English newspaper in India (after the Times of India and Hindustan Times), with a readership of 2.2 million people. The Hindu has its largest base of circulation in southern India, and is the most widely read English daily newspaper in Kerala and Tamil Nadu. Also, it is the most popular newspaper and the prime source of analytical views on socio-politico-economical, legal and administrative acts of Indian and other world institutions for Indian Administrative Service aspirants.
The enterprise employed over 1,600 workers and annual turnover reached almost $200 million in 2010. Subscription and advertisement are major sources of income. The Hindu became, in 1995, the first Indian newspaper to offer an online edition. As of October 2014, it is printed at 17 locations across eight states: Bengaluru, Chennai, Hyderabad, Thiruvananthapuram, Vijayawada, Kolkata, Coimbatore, Madurai, Noida, Visakhapatnam, Kochi, Mangaluru, Tiruchirappalli, Hubballi, Mohali, Lucknow, Allahabad, and Malappuram.


== History ==
The Hindu was founded in Madras on 20 September 1878 as a weekly, by what was known then as the Triplicane Six consisting of 4 law students and 2 teachers:- T. T. Rangachariar, P. V. Rangachariar, D. Kesava Rao Pantulu and N. Subba Rao Pantulu, led by G. Subramania Iyer (a school teacher from Tanjore district) and M. Veeraraghavachariar (a lecturer at Pachaiyappa's College). Started in order to support the campaign of Sir T. Muthuswamy Iyer for a judgeship at the Madras High Court and to counter the propaganda against him carried out by the Anglo-Indian press, The Hindu was one of the many newspapers of the period established to protest the discriminatory policies of the British Raj. About 80 copies of the inaugural issue were printed at Srinidhi Press, Georgetown on one rupee and twelves annas of borrowed money. Subramania Iyer became the first editor and Veeraraghavachariar, the first managing director of the newspaper.
The paper initially printed from Srinidhi Press but later moved on Scottish Press, then, The Hindu Press, Mylapore, and finally to the National Press on Mount Road. Started as a weekly newspaper, the paper became a tri-weekly in 1883 and an evening daily in 1889. A single copy of the newspaper was priced at four annas. The offices moved to rented premises at 100 Mount Road on 3 December 1883. The newspaper started printing at its own press there, named "The National Press," which was established on borrowed capital as public subscriptions were not forthcoming. The building itself became The Hindu's in 1892, after the Maharaja of Vizianagaram, Pusapati Ananda Gajapati Raju, gave The National Press a loan both for the building and to carry out needed expansion.
The Hindu was initially liberal in its outlook and is now considered left leaning. Its editorial stances have earned it the nickname, the 'Maha Vishnu of Mount Road'. "From the new address, 100 Mount Road, which was to remain The Hindu's home till 1939, there issued a quarto-size paper with a front-page full of advertisements—a practice that came to an end only in 1958 when it followed the lead of its idol, the pre-Thomson Times [London]—and three back pages also at the service of the advertiser. In between, there were more views than news." After 1887, when the annual session of Indian National Congress was held in Madras, the paper's coverage of national news increased significantly, and led to the paper becoming an evening daily starting 1 April 1889.
The partnership between Veeraraghavachariar and Subramania Iyer was dissolved in October 1898. Iyer quit the paper and Veeraraghavachariar became the sole owner and appointed C. Karunakara Menon as editor. However, The Hindu‍ '​s adventurousness began to decline in the 1900s and so did its circulation, which was down to 800 copies when the sole proprietor decided to sell out. The purchaser was The Hindu‍ '​s Legal Adviser from 1895, S. Kasturi Ranga Iyengar, a politically ambitious lawyer who had migrated from a Kumbakonam village to practise in Coimbatore and from thence to Madras. Kasturi Ranga Iyengar's ancestors had served the courts of Vijayanagar and Mahratta Tanjore. He traded law, in which his success was middling but his interest minimal, for journalism, pursuing his penchant for politics honed in Coimbatore and by his association with the 'Egmore Group' led by C. Sankaran Nair and Dr T.M. Nair. Since then the newspaper has been owned entirely by the members of the Kasturi Ranga Iyengar family.


== Modern history ==
In the late 1980s when its ownership passed into the hands of the family's younger members, a change in political leaning was observed. Worldpress.org lists The Hindu as a left-leaning independent newspaper. Joint managing director N. Murali said in July 2003, "It is true that our readers have been complaining that some of our reports are partial and lack objectivity. But it also depends on reader beliefs." N. Ram was appointed on 27 June 2003 as its editor-in-chief with a mandate to "improve the structures and other mechanisms to uphold and strengthen quality and objectivity in news reports and opinion pieces", authorised to "restructure the editorial framework and functions in line with the competitive environment". On 3 and 23 September 2003, the reader's letters column carried responses from readers saying the editorial was biased. An editorial in August 2003 observed that the newspaper was affected by the 'editorialising as news reporting' virus, and expressed a determination to buck the trend, restore the professionally sound lines of demarcation, and strengthen objectivity and factuality in its coverage.
In 1987–88, The Hindu's coverage of the Bofors arms deal scandal, a series of document-backed exclusives, set the terms of the national political discourse on this subject. The Bofors scandal broke in April 1987 with Swedish Radio alleging that bribes had been paid to top Indian political leaders, officials and Army officers in return for the Swedish arms manufacturing company winning a hefty contract with the Government of India for the purchase of 155 mm howitzers. During a six-month period, the newspaper published scores of copies of original papers that documented the secret payments, amounting to $50 million, into Swiss bank accounts, the agreements behind the payments, communications relating to the payments and the crisis response, and other material. The investigation was led by a part-time correspondent of The Hindu, Chitra Subramaniam, reporting from Geneva, and was supported by Ram in Chennai. The scandal was a major embarrassment to the party in power at the centre, the Indian National Congress, and its leader Prime Minister Rajiv Gandhi. The paper's editorial accused the Prime Minister of being party to massive fraud and cover-up.
In 1991, Deputy Editor N. Ravi, Ram's younger brother, replaced G. Kasturi as editor. Nirmala Lakshman, Kasturi Srinivasan's granddaughter and the first woman from the family to hold an editorial or managerial role, became Joint Editor of The Hindu and her sister, Malini Parthasarathy, Executive Editor.
In 2003, the Jayalalitha government of the state of Tamil Nadu, of which Chennai is the capital, filed cases against The Hindu for breach of privilege of the state legislative body. The move was perceived as a government's assault on freedom of the press. The paper garnered support from the journalistic community.
On 2 April 2013 The Hindu started "The Hindu in School" with S. Shivakumar as editor. This is a new edition for young readers, to be distributed through schools as part of The Hindu's "Newspaper in Education" programme. It covers the day's important news developments, features, sports, and regional news. On 16 September 2013, The Hindu group launched its Tamil edition with K. Ashokan as editor.
The newspaper has foreign bureaus in eleven locations – Islamabad, Colombo, Dhaka, Kathmandu, Beijing, Moscow, Paris, Dubai, Washington DC, London, and most recently Addis Ababa.


== Management ==
The Hindu is family-run. It was headed by G. Kasturi from 1965 to 1991, N. Ravi from 1991 to 2003, and by his brother, N. Ram, from 27 June 2003 to 18 January 2012. Other family members, including Nirmala Lakshman, Malini Parthasarathy, Nalini Krishnan, N Murali, K Balaji, K Venugopal and Ramesh Rangarajan are directors of The Hindu and its parent company, Kasturi and Sons. S Rangarajan, former managing director and chairman since April 2006, died on 8 February 2007. Ananth Krishnan, who is the first member of the youngest generation of the family to join the business worked as a special correspondent in Chennai and Mumbai from 2007, and foreign correspondent in Beijing from 2009. It now conducts very popular "The Hindu Young World Quiz', hosted by noted quizmaster V.V. Ramanan, that has now completed 12 years and is arguably the largest live, middle-school quiz competition.


== Managing-directors ==

M. Veeraraghavachariar (1878–1904)
S. Kasturi Ranga Iyengar (1904–1923)
K. Srinivasan (1923–1959)
G. Narasimhan (1959–1977)
N. Ram (1977–2011)
K. Balaji (2011 – 2013)
N. Ram (2013– incumbent)- Chairman of KSL & Publisher


== Editors ==
G. Subramania Iyer (1878–1898)
C. Karunakara Menon (1898–1905)
Kasturi Ranga Iyengar (1905–1923)
S. Rangaswami Iyengar (1923–1926)
K. Srinivasan (1926–1928)
A. Rangaswami Iyengar (1928–1934)
K. Srinivasan (1934–1959)
S. Parthasarathy (1959–1965)
G. Kasturi (1965–1991)
N. Ravi (1991–2003)
N. Ram (2003–2012)
Siddharth Varadarajan (2012–2013)
N. Ravi (2013–2015)- Editor-in-Chief 
Malini Parthasarathy (2013–2015)- Editor
Malini Parthasarathy (2015–incumbent)- Editor-in-chief


== Board of directors ==
The Hindu Group is managed by the descendants of Kasturi Ranga Iyengar. As of 2010, there are 12 directors in the board of Kasturi & Sons—N. Ram, N. Ravi and N. Murali (sons of G. Narasimhan); Nirmala Lakshman, Nalini Krishnan and Malini Parthasarathy (daughters of S. Parthasarathy); Ramesh Rangarajan, Vijaya Arun and Akila Iyengar (children of S. Rangarajan); K. Balaji, K. Venugopal and Lakshmi Srinath (children of G. Kasturi).


== Online presence ==
The Hindu was the first newspaper in India to have a website, launched in 1995.
On 15 August 2009, the 130-year-old newspaper, launched the beta version of its redesigned website at beta.thehindu.com. This was the first redesign of its website since its launch. On 24 June 2010 the beta version of the website went fully live at www.thehindu.com.
The new website retains its core values of independence, authenticity, and credibility while adopting contemporary web design principles, tools, and features.
The design is by Mario Garcia Jr., of Garcia Media, Tampa, Florida, USA. The workflow solution is by CCI Europe A/S, Denmark. The web publishing system is from Escenic A/S, Norway. The implementation was done in-house.


== Reviews ==
In 1965, The Times listed The Hindu as one of the world's ten best newspapers. Discussing each of its choices in separate articles, The Times wrote:- "The Hindu takes the general seriousness to lengths of severity... published in Madras, [it] is the only newspaper which in spite of being published only in a provincial capital is regularly and attentively read in Delhi. It is read not only as a distant and authoritative voice on national affairs but as an expression of the most liberal—and least provincial—southern attitudes... Its Delhi Bureau gives it outstanding political and economic dispatches and it carries regular and frequent reports from all state capitals, so giving more news from states, other than its own, than most newspapers in India... It might fairly be described as a national voice with a southern accent. The Hindu can claim to be the most respected paper in India."
In 1968, the American Newspaper Publishers' Association awarded The Hindu its World Press Achievement Award. An extract from the citation reads: "Throughout nearly a century of its publication The Hindu has exerted wide influence not only in Madras but throughout India. Conservative in both tone and appearance, it has wide appeal to the English-speaking segment of the population and wide readership among government officials and business leaders... The Hindu has provided its readers a broad and balanced news coverage, enterprising reporting and a sober and thoughtful comment... [It] has provided its country a model of journalistic excellence... [It] has fought for a greater measure of humanity for India and its people... [and] has not confined itself to a narrow chauvinism. Its Correspondents stationed in the major capitals of the world furnish The Hindu with world-wide news coverage... For its championing of reason over emotion, for its dedication to principle even in the face of criticism and popular disapproval, for its confidence in the future, it has earned the respect of its community, its country, and the world."


== Controversy ==

The Hindu has been accused of left-wing bias with its articles and editorials. N. Ram, the current chairman of Kasturi & Sons Limited, and the publisher of The Hindu, was the vice-president of the Students' Federation of India, the students wing of the CPI(M).
The Hindu has also been accused of pro-Sinhalese bias in its writing by many advocates for the rights of Sri Lankan Tamils. Prominent Sri Lankan political commentator David Jeyaraj claimed he was fired from The Hindu for exposing IPKF atrocities against the Sri Lankan Tamils. N.Ram was awarded the "Sri Lanka Rathna", the highest civilian award that can be conferred on a foreigner in Sri Lanka in November 2005.
In light of opinion pieces published by the former editor of The Hindu, N. Ram, extolling China's governance of Tibet and other perceived slights, many commentators, have claimed a Sinophilic bias in the writings of the paper. B. Raman director of the South Asia Analysis Group stated "Its sympathy for China and its policy in recent years of keeping out of its columns any report or article of a negative nature on China is well known." and went on to further claim that "its policy of placing its columns at the disposal of the Xinhua news agency of China" make it a mouthpiece of the Chinese government.
In his letter to his colleagues, N Ravi (Editor) says, "Ram seems bent on taking all the editorial directors into retirement with him with a scorched earth policy to ensure that no one in the family succeeds him".
On 21 July 2011, Siddharth Varadarajan, the national bureau chief of The Hindu, was appointed editor of The Hindu (made effective from 30 July 2011), a move that triggered the resignations of three members of the family from their senior editorial positions: N. Ravi resigned as editor, Malini Parthasarathy as executive editor and Nirmala Lakshman as the joint editor. A fourth member of the family, N. Murali, announced his retirement on attaining the age of 65 on 11 August 2011. They remain on the board of directors. Varadarajan was named by N. Ram, the editor-in-chief to succeed him.
On 21 October 2013, changes have been made in Editorial as well as business of The Hindu  N.Ravi has taken over as Editor-in-chief of The Hindu and Malini Parthasarathy as Editor of The Hindu. In consequence, Siddarth Varadarajan has submitted his resignation. N. Ram has become Chairman of Kasturi & Sons Limited and Publisher of The Hindu and Group publications; and N. Murali, Co-Chairman of the company.


== See also ==

The Hindu Business Line
Frontline magazine
The Hindu Group
List of newspapers in India
List of newspapers in India by readership
The Hindu Literary Prize
Lit for Life


== Citations ==


== Further reading ==
Merrill, John C. and Harold A. Fisher. The world's great dailies: profiles of fifty newspapers (1980) pp 162-69
125 years of The Hindu
N. Ram (13 September 2003). "Yesterday, Today, Tomorrow". The Hindu. Retrieved 20 April 2006. 
Looking Back: The history of The Hindu as told by historian S. Muthiah.
"Willing to strike and not reluctant to wound". 13 September 2003. Retrieved 28 April 2006. 
"Making news the family business". 13 September 2003. Retrieved 28 April 2006. 
"A clarion call against the Raj". 13 September 2003. Retrieved 28 April 2006. 
"Treading softly – but modernizing apace". 13 September 2003. Retrieved 28 April 2006. 
"Developing a paper for the new reader". 13 September 2003. Retrieved 28 April 2006. 

N. Murali (13 September 2003). "Core values and high quality standards". The Hindu. Retrieved 20 April 2006. 


== External links ==
Newspaper website - English
Official website - English
Mobile website - English
Online edition (ePaper) - English