The Persian Empire is any of a series of imperial dynasties centered in Persia (now Iran). The first of these was established by Cyrus the Great in 550 BC, with the Persian conquest of Media, Lydia and Babylonia. Persian dynastic history was interrupted by the Islamic conquest (651 AD) and later by the Mongol invasion. The main religion of ancient Persia was Zoroastrianism, but after the 7th century this was replaced by Islam. In the modern era, a series of Islamic dynasties ruled Persia independently of the universal caliphate. Since 1979 Persia (Iran) has been an Islamic republic.


== List of dynasties ==
Achaemenid Empire (550–330 BC), also called the "First Persian Empire"
Parthian Empire (247 BC–224 AD, also called the "Arsacid Empire"
Sasanian Empire (224–651), also called the "Neo-Persian Empire" and "Second Persian Empire"
Samanid Empire (819-999)
Safavid dynasty (1501–1736)
Afsharid dynasty (1736–1796)
Zand dynasty (1750–1794)
Qajar dynasty (1785–1925)
Pahlavi dynasty (1925–1979)


== See also ==
Iranian monarchy (disambiguation)
Persia (disambiguation)
Persian (disambiguation)
List of kings of Persia
Persia
Iranian people
Persian people