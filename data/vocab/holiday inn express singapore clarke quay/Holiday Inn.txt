Holiday Inn is a multinational brand of hotels, part of the LSE-listed InterContinental Hotels Group. Originally a U.S. motel chain, today it is one of the world's largest hotel chains, with 435,299 bedrooms in 3,463 hotels globally hosting over 100 million guest nights each year. The hotel chain is based in three cities: Atlanta, London and Rio de Janeiro.


== History ==


=== 1950s–1970s ===
Kemmons Wilson, a resident of Memphis, Tennessee, was inspired to build his own motel after being disappointed by poor quality and inconsistent roadside accommodations during a family road trip to Washington, D.C. The name Holiday Inn was given to the original hotel by his architect Eddie Bluestein as a joke, in reference to the 1942 Christmas-themed, musical film Holiday Inn, starring Bing Crosby and Fred Astaire. The first Holiday Inn opened at 4925 Summer Avenue in Memphis, the main highway to Nashville in August 1952 as Holiday Inn Hotel Courts. In the early 1990s, it was demolished, but there is a plaque commemorating the site.
Wilson partnered with Wallace E. Johnson to build additional motels on the roads entering Memphis. Holiday Inn's corporate headquarters was in a converted plumbing shed owned by Johnson. In 1953, the company built its next three hotels which, along with their first hotel, covered each approach to Memphis. The second motel was built on U.S. 51 South. It was followed by two more in 1953, one on Highway 51 North, and another on U.S. 61. On the occasion of Johnson's death in 1988, Wilson was quoted as saying, "The greatest man I ever knew died today. He was the greatest partner a man could ever have."
Together they started what Wilson would shepherd into Holiday Corporation, one of the world's largest hotel groups.
By the beginning of 1956, there were 23 Holiday Inns operating with seven more due to open by year's end. In 1957 Wilson began franchising the chain as Holiday Inn of America, mandating its properties be standardized, clean, predictable, family-friendly, and readily accessible to road travellers. The chain grew dramatically as a result, with 50 locations across the country by 1958, 100 by 1959, 500 by 1964, and the 1,000th Holiday Inn, in San Antonio, Texas, in 1968.
In 1965, the chain launched Holidex, a centralized reservation system where a visitor to any Holiday Inn could obtain reservations by teleprinter for any other Holiday Inn location. The only comparable systems at the time were operated by airlines (Sabre made its debut in 1963). Promoting itself as "your host from coast to coast", Holiday Inn added a call center after AT&T's introduction of +1-800 toll-free telephone number service in 1967 and updated its systems as desktop microcomputers, an invention of the 1970s, found their way into travel agencies.
Branded as "The Nation's Innkeeper", the chain put considerable financial pressure on traditional motels and hotels, setting the standard for competitors like Ramada Inn, Quality Inn, Howard Johnson's, and Best Western. By June 1972, with over 1,400 Holiday Inns worldwide, Wilson was featured on the cover of Time magazine and the motto became "The World's Innkeeper".
In the 1960s, Holiday Inn began franchising and opening campgrounds under the Holiday Inn Trav-L-Park brand. These recreational campgrounds were listed in the Holiday Inn directories.
In 1963, Holiday Inn signed a long-term deal with Gulf Oil Corporation where it agreed to accept Gulf credit cards to charge food and lodging at all of its US and Canadian hotels in return for Gulf building service stations on many Holiday Inn properties, particularly near major U.S. and Interstate highways. The arrangement was copied by competing lodging chains and major oil companies during the mid-to-late 1960s, but fell out of favor following the 1973 oil crisis. The Gulf/Holiday Inn arrangement ended around 1982.
The company later branched into other enterprises, including Medi-Center nursing homes, Continental Trailways, Delta Queen, and Show-Biz, Inc., a television production company that specialized in syndicated country music shows. Wilson also developed the Orange Lake Resort and Country Club near Orlando and a chain called Wilson World Hotels. The acquisition of Trailways in 1968 lasted until 1979, when Holiday Inn sold Trailways to private investor Henry Lea Hillman Sr of Pittsburgh, Pennsylvania. In the years during which Trailways was a subsidiary of Holiday Inn, television commercials for Holiday Inn frequently showed a Trailways bus stopping at a Holiday Inn hotel. Wilson retired from Holiday Inn in 1979. As of 2014, Wilson's family still operates hotels as part of the Kemmons Wilson Companies of Memphis.


=== The Great Sign ===

The "Great Sign" is the roadside sign used by Holiday Inn during their original era of expansion in the 1950s-1970s. It was perhaps their greatest advertising. It was extremely large and eye-catching, but was expensive to construct and operate. The manufacturer of the sign was Balton & Sons Sign Company. In shop sketch artists Gene Barber and Roland Alexander did the original design of the sign. Original engineering drawings were also done by Roland Alexander of Balton & Sons Sign Company. Wilson had very specific demands about how the sign should be designed. He wanted a sign that no road traveler could miss. It should be at least 50' high and be seen in both directions. He also wanted a marquee so the inn could welcome different groups to the hotel. The original sign cost $13,000. The story goes that the sign’s colors were selected because they were favorites of Wilson’s mother. The popularity of the sign led to many clones being produced, some of which remain to this day. In 1982, following Wilson's departure, the Holiday Inn board of directors made the decision to phase out the "Great Sign" in favor of a cheaper and less catchy backlit sign. The decision was not without controversy as it essentially signaled the end of the Wilson era and removed a widely recognized company icon. Wilson was angered about this, saying, "It was the worst mistake they ever made". Wilson so loved the sign that it was engraved on his tombstone. The majority of the signs were sold as scrap metal and recycled.
Several intact fragments of the famous sign have been restored and relit, mostly the Holiday Inn top section of the sign, and the marquee box. However, in 2006 a complete sign was finally found. The disassembled sign, complete with star, marquee box, and the sign base, was discovered in a backlot in Minnesota. On June 3, 2007 it was purchased by a neon sign restoration expert, in order to restore it to its 1950s appearance. It will be displayed at the National Save the Neon Signs Museum in Minot, North Dakota. Also, an intact sign that came from a Las Vegas location stands outside of the American Sign Museum in Cincinnati Ohio. Another intact and operating Holiday Inn Great Sign is at "The Henry Ford" Museum in Dearborn, Michigan and one with a private collector in Kentucky.


=== 1980s–present ===

Although still a healthy company, changing business conditions and demographics saw Holiday Inn lose its market dominance in the 1980s. Holiday Inns, Inc. was renamed "Holiday Corporation" in 1985 to reflect the growth of the company’s brands, including Harrah's Entertainment, Embassy Suites Hotels, Crowne Plaza, Homewood Suites, and Hampton Inn. In 1988, Holiday Corporation was purchased by UK-based Bass PLC (the owners of the Bass beer brand), followed by the remaining domestic Holiday Inn hotels in 1990, when founder Wilson sold his interest, after which the hotel group was known as Holiday Inn Worldwide. The remainder of Holiday Corporation (including the Embassy Suites Hotels, Homewood Suites, and Hampton Inn brands) was spun off to shareholders as Promus Companies Incorporated. In 1990, Bass launched Holiday Inn Express, a complementary brand in the limited service segment.

In 1997, Bass created and launched a new hotel brand, Staybridge Suites by Holiday Inn, entering the North American upscale extended stay market. In March 1998 Bass acquired the InterContinental brand, expanding into the luxury hotel market. In 2000 Bass sold its brewing assets (and the rights to the Bass name) and changed its name to Six Continents PLC. InterContinental Hotels Group (IHG) was created in 2003 after Six Continents split into two daughter companies: Mitchells & Butlers PLC to handle restaurant assets, and IHG to focus on soft drinks and hotels, including the Holiday Inn brand.
The brand name Holiday Inn is now owned by IHG, which in turn licenses the name to franchisees and third parties who operate hotels under management agreements.
The Wall Street Journal reported in 2002 that the company, led by Ravi Saligram, was producing a new 130-room "Next Generation" prototype hotel to rebuild the brand. It would include a bistro-like restaurant and an indoor pool. The first of these prototype hotels, the Holiday Inn Gwinnett Center, was built in Duluth, Georgia, in 2003.
On October 24, 2007, IHG announced a worldwide relaunch of the Holiday Inn brand, which spelled trouble for the remaining motels. The relaunch is "focused on delivering consistently best in class service and physical quality levels, including a redesigned welcome experience [and] signature bedding and bathroom products..." The first relaunched Holiday Inn opened in the US in spring 2008. Currently there are more than 2,500 relaunched Holiday Inn brand hotels around the world, and the Holiday Inn global brand relaunch process was completed by the end of 2010. By then, the majority of the HI motels were removed from the chain, with a few exceptions (In the 1980s and 1990s, HI hotels were built alongside the motel properties [i.e. Baton Rouge, Louisiana] in order to provide more amenities and newer rooms). When the relaunch occurred, these motels were either demolished or closed off, even if a full-service hotel was already on site. Today, less than 10 Holiday Inn motels still fly under the flag, others having been replaced by newer Holiday Inn Express locations or have switched to other chains.
In September 2008, IHG announced the creation of a new timeshare brand, Holiday Inn Club Vacations, a strategic alliance with The Family of Orange Lake Resorts.


== Criticism ==
The InterContinental Hotels Group became the target of an international boycott campaign in May 2013, over their plan to operate an Intercontinental-brand luxury hotel in Lhasa, Tibet. According to campaigners from the Free Tibet campaign, the hotel was a "PR coup for the Chinese government". IHG responded that "IHG's hotels create jobs and drive tourism income in the communities where they operate, thereby helping to increase living standards in Lhasa and wider Tibet." 
On March 22, 2015, European neo-Nazis (British National Party, British unity, League of Life, Britain First, German NPD, Italian Forza Nuova, Lega Lombarda, Spanish National Democracy, Greece's Golden Dawn, Party of the Swedes, Danskernes Parti (Denmark), National Independence Party (Finland), Jared Taylor (USA, author of the book "White Identity"), Russian Rodina, Fight for Donbass, ROD, New Russia, Russian Imperial Movement, Rusich and others) held meeting, the International Russian Conservative Forum, aimed at uniting nationally-orientated forces in Europe and Russia, at the St. Petersburg Holiday Inn. 


== Logos ==


== Brands ==

Holiday Inn – This is the most recognizable tier of service. There are two distinct types: high-rise, full-service plaza hotels and low-rise, full-service hotels. The former also included many high-rises with round, central-core construction, instantly recognizable from the 1970s. Both offer a restaurant, pools at most locations, room service, an exercise room, and functional but comfortable rooms.
Holiday Inn Hotel & Suites – The properties offer all the amenities and services of a regular Holiday Inn but consist of rooms mixed with suites.
Holiday Inn Resort – The properties also offer all the amenities and services of a full-service Holiday Inn; resorts are considered a more of an advertising branding than a completely different brand. Most Holiday Inn Resorts are located in high-leisure-tourism markets.

Holiday Inn Select – These upper-range full-service hotels cater to business travelers. In 2006 it was announced that Holiday Inn Select hotels would be discontinued. Existing hotels may continue to operate under the Holiday Inn Select flag until their existing license expires, however many are converting to Crowne Plaza or regular Holiday Inn hotels, with no further marketing or advertising based around the "Select" moniker. Several Select hotels still remain as of 2014.
Holiday Inn Sunspree Resorts (officially named SunSpree) –  The properties were in resort areas with full-service amenities and deluxe service. These are typically very large properties. The Holiday Inn SunSpree Resort in Montego Bay, Jamaica is the only hotel that still retains the moniker; others were incorporated as Holiday Inn Resorts.
Holiday Inn Club Vacations – These are resorts aimed at families and are only based in the U.S. The accommodations are mostly villas and suites.
Holiday Inn Garden Court – The properties exist only in Europe and South Africa and are designed to reflect the national culture.
Holiday Inn Express – The properties are smaller versions of Holiday Inn hotels with fewer amenities and services. They are very similar to competitors like La Quinta Inn and Hampton Inn in which they appeal to middle class customers.


=== Other ===
Although originally called "Holiday Inn Crowne Plaza", the Crowne Plaza moniker was split from Holiday Inn in 1994 to form a distinctive brand.
During the 1960s and 1970s, there were several Holiday Inn Jr. motels with just 44 to 48 guest rooms located in portables. Locations included Camden, Arkansas, Rantoul, Illinois. Cleveland, Mississippi, Sardis, Mississippi, Farmington, Missouri, Springfield, Tennessee, and Columbus, Texas. A traditionally constructed lobby building featured a Holiday Grill restaurant. The Camden location had just 32 rooms while the Rantoul location had 64 rooms.
Holiday Inn Magazine was a monthly publication for guests during the 1970s. It featured travel destination and attraction stories in addition to a featured hotel property.


== References ==


== External links ==
Official website
Holiday Inn Hotels review
Holiday Inn relaunch
Balton and Sons
Great Sign history
"Come Inn off the Highway" - USA Today article, May 24, 2002
I Remember JFK, a Baby Boomer's Pleasant Reminiscing Spot: Holiday Inns
New style Holiday Inn Review
Classic Classy Holiday Inn, A Facebook page commemorating The Great Sign and Holiday Inn
Pictures of the Great Sign