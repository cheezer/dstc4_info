A holiday is a day of observance.
Holiday may also refer to:
Holiday (employment) or annual leave, time off from a job
Christmas and holiday season or "the holidays", an annual festive period that surrounds Christmas and various other holidays
Vacation or holiday, a trip or leave of absence for the purpose of recreation or rest


== People ==
Holiday (surname)
Holiday Reinhorn (born 1964), American fiction writer known for her short stories


== Places ==
Holiday, Florida, U.S., a census-designated place
Holiday, a community in Zorra, Ontario, Canada


== Arts, entertainment, and media ==


=== Film ===
Holiday (1930 film), an adaptation of the play starring Ann Harding and Mary Astor
Holiday (1938 film), an adaptation of the play starring Katharine Hepburn and Cary Grant
Jour de fête or Holiday (1949), a French comedy film by Jacques Tati
Holiday (2006 film), a Bollywood film by Pooja Bhatt
The Holiday (2006), a film starring Cameron Diaz and Kate Winslet
Holiday (2010 film), a 2010 French crime comedy
Holidays (2010 film), a Malayalam film directed by M M Ramachandran
Holidays (2015 Film), a anthology film
Holiday: A Soldier Is Never Off Duty (2014), a Bollywood film by A. R. Murugadoss starring Akshay Kumar


=== Music ===


==== Artists and labels ====
The Holidays, an Australian indie rock band
Holiday Records, an American record label


==== Albums ====
Holiday (Alaska in Winter album), 2008
Holiday (America album), 1974
Holiday (Earth, Wind & Fire album), 2014
Holiday (Russ Freeman album), 1995
Holiday (The Magnetic Fields album), 1999
Holiday (Roberta Flack album), 1997
Holiday (Sammi Cheng album), 1991
Holiday! A Collection of Christmas Classics, by Crystal Lewis, 2000
The Holiday (EP), an EP by Brand New


==== Songs ====
"Holiday" (Bee Gees song)
"Holiday" (Dilana song)
"Holiday" (Dizzee Rascal song)
"Holiday" (Green Day song)
"Holiday" (Madonna song)
"Holiday" (Naughty by Nature song)
"Holiday" (Vampire Weekend song)
"Holiday" (Vanessa Amorosi song)
"Holiday", a song by The Birthday Massacre from Violet
"Holiday", a song by The Happy Mondays from Pills 'n' Thrills and Bellyaches
"Holiday", a song by Hilary Duff from Best of Hilary Duff
"Holiday", a song by Jet from Shine On
"Holiday", a song by Nazareth from Malice in Wonderland
"Holiday", a song by The Other Ones
"Holiday", a song by the Scorpions from Lovedrive
"Holiday", a song by Weezer from Weezer (1994)
"Holidays", a song by Miami Horror from Illumination
"Holiday", a song by Boys Like Girls from their self-titled, first album
"Holiday", a song by The Get Up Kids from Something to Write Home About
"Holiday", a song by the Kinks from Muswell Hillbillies
"Holiday", a song by Remady & Manu-L from The Original 2k13 Holiday
"Holiday", a song by DJ Antoine feat. Akon


=== Print and Stageplays ===
Holiday (comics), a fictional character in the Batman story The Long Halloween
Holiday (magazine), a 1928–1977 American travel magazine
Holiday (novel), a 1974 novel by Stanley Middleton
Holiday (play), a 1928 play by Philip Barry


=== Television ===


==== Series ====
Holiday (TV programme), a 1969–2007 UK travel review show


==== Episodes ====
"Holiday" (The Goodies), 1982
"Holiday" (Miranda)
"Holiday" (Stargate SG-1)
"Holiday" (Take a Letter, Mr. Jones)


== Companies and brands ==
Holiday Stationstores, a chain of convenience stores in the U.S.
Holiday, an appliance brand owned by Maytag


== Other uses ==
Holiday (horse) (foaled 1911), American Thoroughbred racehorse
Holiday, a Pastafarian holiday


== Transportation and vessels ==
MS Holiday, a cruise ship


== See also ==
Holliday (disambiguation)
Halliday