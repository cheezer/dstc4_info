The Holland Sentinel is a newspaper published six days a week in Holland, Michigan, United States, founded in 1896. It is published by GateHouse Media.
The newspaper covers most of Ottawa County, including Holland, Beechwood, Ferrysburg, Grand Haven, and Zeeland, as well as northwestern Allegan County, Michigan, including Douglas and Saugatuck.


== History ==
Originally an afternoon paper published six days a week, the Sentinel moved to Saturday mornings and then added a Sunday edition in the late 1980s. In the late 1990s, the paper adopted a morning format for all seven days.
Before adopting the name The Holland Sentinel, it was called The Holland Evening Sentinel (1928-1977), and before that the Holland Daily Sentinel.
The paper was formerly owned by Stauffer Communications, which was acquired by Morris Communications in 1994. Morris sold the paper, along with thirteen others, to GateHouse Media in 2007.
The Lakeshore Press, a publication of the Grand Rapids Press, was its main competitor, but has since shut down operations.
In the mid 1980s, the Sentinel was a satellite publishing site for the Chicago Tribune.
In the early 2000s, printing operations were moved to the Allegan, Michigan, facilities of The Flashes, a weekly advertising only newspaper that Morris Communications had purchased a few years prior.


== References ==


== External links ==
Official website
GateHouse Media