Holland Village (or Holland V) can refer to


== Places ==
Holland Village, Singapore
Holland Village in Gaoqiao, Shanghai, People's Republic of China
Holland Village in Shenyang, People's Republic of China, a development project of businessman Yang Bin


== Transportation ==
Holland Village MRT Station on the Circle MRT Line in Singapore's Mass Rapid Transit


== Arts and Entertainment ==
Holland V (TV series), a Singapore Chinese drama serial.