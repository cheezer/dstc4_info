Coming Home or Comin' Home may refer to:


== Film ==
Coming Home (1962 film), a South Korean film starring Choi Moo-ryong
Coming Home (1978 film), an American drama directed by Hal Ashby
Coming Home (2011 film), a film featuring Ankur Bhatia
Coming Home (2012 film), a French film directed by Frédéric Videau
Coming Home (2014 film), a Chinese film directed by Zhang Yimou


== Literature ==
Coming Home (novel), a novel by Lester Cohen
Coming Home, a novel by Rosamunde Pilcher
Coming Home (play), a 2009 play by Athol Fugard
Coming Home, a 2014 science-fiction novel by Jack McDevitt


== Music ==


=== Albums ===
Comin' Home (EP), the first EP from American country music singer Jessie James Decker
Coming Home (Faye Wong album)
Coming Home (Lionel Richie album), or the title song, "I'm Coming Home"
Coming Home (Lonestar album)
Coming Home (The Nadas album), or the title song
Coming Home (New Found Glory album), or the title song
Coming Home (The Soldiers album), or the title song (see below)
Coming Home (Boozoo Bajou album), an album by Boozoo Bajou
Coming Home (Danny Wood album), an album by Danny Wood (2008)
Coming Home (Nightmares on Wax album), an album by Nightmares on Wax
Coming Home (Leon Bridges album), an album by Leon Bridges
Coming Home, by Yungchen Lhamo
Coming Home, by Joe Grushecky


=== Songs ===
"Coming Home" by Miranda Cosgrove iCarly Cast
"Coming Home" (Alex Band song)
"Coming Home" (Alex Lloyd song)
"Coming Home", by Axel Rudi Pell from Shadow Zone
"Coming Home" (Cinderella song)
"Comin' Home" (City and Colour song)
"Coming Home" (Diddy-Dirty Money song)
"Coming Home" (Kaiser Chiefs song)
"Coming Home" (Lemar song)
"Coming Home" (Sigurjón's Friends song)
"Coming Home" (The Soldiers song)
"Major Tom (Coming Home)", by Peter Schilling
"Coming Home", by Alter Bridge from Blackbird
"Coming Home", by Aly & Fila feat. Jwaydan
"Coming Home", by Armin van Buuren from Mirage
"Comin' Home", by Bob Seger from The Distance
"Comin' Home", by Cheeseburger
"Coming Home", by DJ Tiësto from Parade of the Athletes
"Comin' Home", by Danger Danger from Screw It!
"Comin' Home", by Delaney & Bonnie from On Tour with Eric Clapton
"Comin' Home", by Deep Purple from Come Taste the Band
"Coming Home", by Eric Saade
"Coming Home", by Firelight (Eurovision 2014 - Malta)
"Coming Home", by Gwyneth Paltrow from the Country Strong film soundtrack
"Comin' Home", by Hum from Downward Is Heavenward
"Coming Home", by Iron Maiden from The Final Frontier
"Coming Home", by Iron Savior from Unification
"Coming Home", by James LaBrie from Static Impulse
"Coming Home", by John Legend from Once Again
"Coming Home", by King Diamond from Them
"Comin' Home", by Kiss from Hotter Than Hell
"Coming Home", by Little Richard from Pray Along with Little Richard
"Coming Home", by Leon Bridges
"Comin' Home", by Lynyrd Skynyrd from The Essential Lynyrd Skynyrd
"Coming Home", by Marit Larsen from Spark
"Coming Home", by The Mavis's from Rapture
"Coming Home", by Megadeth from The World Needs a Hero
"Coming Home", by Pixie Lott from Turn It Up Louder
"Comin' Home", by Prism from Armageddon
"Coming Home", by SWV from It's About Time
"Coming Home", by Saxon from Killing Ground
"Coming Home", by Scorpions from Love at First Sting
"Coming Home", by Stratovarius from Visions
"Comin' Home", by Talisman from Genesis
"Coming Home", by U.D.O. from Animal House
"Coming Home (Jeanny Part 2)", by Falco from Emotional
"Coming Home", by Avenged Sevenfold from Hail to the King


== Television ==
Coming Home (TV serial), a two-part 1998 ITV serial based on Rosamund Pilcher's novel (see below)
Coming Home (UK TV series), a BBC Wales documentary series
Coming Home (Lifetime TV series), a 2011 American reality series
Coming Home (TV miniseries), a 2003 miniseries produced by John Drimmer


=== Episodes ===
"Coming Home" (The Dead Zone)
"Coming Home" (Desperate Housewives)
"Coming Home" (Mobile Suit Gundam)
"Coming Home" (Xena: Warrior Princess)


== Other uses ==
Coming Home campaign, a UK program to aid military personnel returning from Afghanistan and Iraq


== See also ==
Homecoming (disambiguation)
Come Home (disambiguation)
Going Home (disambiguation)