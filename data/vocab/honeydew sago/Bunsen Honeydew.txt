Dr. Bunsen Honeydew, Ph.D. is a Muppet character from The Muppet Show, performed by Dave Goelz. He is a bald, yellow-skinned, bespectacled, lab-coated scientist who would do periodic science segments from "Muppet Labs, where the future is being made today." The character has no eyes, only completely transparent, lensless glasses, giving the appearance of a stereotypical absent-minded intellectual. His first name is borrowed from Robert Bunsen, after whom the Bunsen Burner was named. His last name is a reference to the honeydew melon, to which his head bears a striking resemblance. It is also a reference to Honeywell Labs, a technology company which aired TV commercials ("That someday is today ... at Honeywell") well-known at the time of the original Muppet Show.
Honeydew's experiments usually cause great harm to his very nervous and long-suffering assistant Beaker, a nearly mute Muppet with a shock of reddish hair. Honeydew worked alone in the first season of The Muppet Show, but from the second season onward, the luckless Beaker would always be present.
Some of the inventions that were created and tested included: edible paper clips, a gorilla detector, hair-growing tonic, banana sharpener, a robot politician (played by Peter Ustinov) and an electric nose warmer. In response to the ancient quest of alchemy to turn lead into gold, Honeydew created a device that turned gold into cottage cheese.


== Appearance ==
Dr. Bunsen Honeydew's moniker is derived from the traditional piece of lab equipment, the Bunsen burner, plus the fact that his head was shaped and colored like a honeydew melon. He was rumored to be modeled after Lew Grade, whose company ITC Entertainment produced The Muppet Show. Henson responded in a Judy Harris interview:

Bunsen Honeydew was not specifically Lew Grade when we did him. It would have been easy to make him much more like Lew Grade if we had tried to and, in retrospect, I wish that we had. The character that owns the Muppet theatre only appeared a couple of times and I always — in looking back — wished that I had made that to look just like Lew Grade because he's very caricaturable.

Some hold that the Muppet was based upon Jim Henson's children's high school chemistry teacher, Allan Abrahams. This has not been confirmed.
One of his most endearing features is his lack of eyes, despite the fact that he wears glasses. Occasionally, Honeydew removes his glasses to clean them, or lifts them as if to get a better look at things, which is something of a running gag.
Films where he appeared in major roles include The Muppet Movie (1979), The Muppet Christmas Carol (1992), Muppet Treasure Island (1996), Muppets from Space (1999),It's a Very Merry Muppet Christmas Movie (2002), and The Muppets' Wizard of Oz (2005) with Beaker. He also appeared in a small supporting role with Beaker in The Great Muppet Caper (1981) and A Muppets Christmas: Letters to Santa (2008), though in The Muppets Take Manhattan (1984) both Bunsen and Beaker were only background characters in the wedding scene. A scene with them was deleted.
In the Halloween 2011 episode of WWE Raw, Bunsen developed a specially formulated energy drink that provides strength, agility and fresher smelling breath to those who take it. He had Beaker go deliver it to Santino Marella for his match against Jack Swagger.
The duo appear in the 2011 feature film The Muppets, where they have been apparently working at CERN with the Large Hadron Collider after The Muppet Show ended. Their big CERN scene was deleted, as well as a scene with a bowling ball, leaving them with little screen time in the film. Both Bunsen and Beaker appear in the follow-up feature film Muppets Most Wanted (2014) with a substantially larger role including a featured invention of theirs, a big bomb-attracter vest.


== Biography ==
Bunsen graduated from Carnegie Mellonhead University (a spoof of Carnegie Mellon University) and was employed as an assistant to Dr. Pinhole Burns. He is the founder of Muppet Labs.
The two scientists were later incorporated into the Muppet Babies animated series. Howie Mandel and Dave Coulier voiced Bunsen, and Frank Welker provided Beaker's squeaky meeps. Bunsen made an appearance in Little Muppet Monsters voiced by Bob Bergen. Following Richard Hunt's death in 1992, the role of Beaker was taken over by Steve Whitmire.
In a 2004 Internet poll sponsored by the BBC and the British Association for the Advancement of Science, Beaker and Dr. Bunsen Honeydew were voted Britain's favourite cinematic scientists. They beat Mr. Spock, their closest rival, by a margin of 2 to 1 and won 33 percent of the 43,000 votes cast.


== Popular culture ==
Honeydew (along with Beaker) was featured on the #19 Dodge Dealers Dodge of Jeremy Mayfield in the 2002 Tropicana 400 in an advertising campaign in which he and his fellow Muppets were featured on a select few race cars.
Honeydew made an appearance in the Family Guy episode "Jungle Love", with Peter Griffin as an assistant rather than Beaker.


== Filmography ==
The Muppet Movie
The Great Muppet Caper
The Muppets Take Manhattan
Muppet*Vision 3D
The Muppet Christmas Carol
Muppet Classic Theater
Muppet Treasure Island
It's A Very Merry Muppet Christmas Movie
Muppets from Space
The Muppets' Wizard of Oz
A Muppet Family Christmas
Jim Henson's Muppet Babies
The Muppets at Walt Disney World
Studio DC: Almost Live
A Muppets Christmas: Letters to Santa
The Muppets
Lady Gaga and the Muppets' Holiday Spectacular
Muppets Most Wanted


== See also ==
Muppet Mobile Lab
Mad scientist


== References ==


== External links ==
Dr. Bunsen Honeydew on Muppet Wiki,  an external wiki

Official Muppets website