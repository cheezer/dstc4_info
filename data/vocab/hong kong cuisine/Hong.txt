Hong may refer to:
Hong (Chinese name) (洪), a Chinese surname, branch of Jiang (Jeung:Geung:Gang:Kang)
Hong, in Cantonese of surname Gang, Kang (康) from the Zhou Dynasty
Hong (Korean name), a Korean surname
Hong, hongsa or hamsa (mythical bird)
Hong Kong, a major Asian city
Hong (business), general term for a 19th–20th century trading company based in Hong Kong, Macau or Canton
Høng, a Danish municipality
Hong (rainbow-dragon), two-headed dragon in Chinese mythology
Hong, Nigeria, a Local Government Area
Lake Hong, in Hubei, China
Thirteen Factories (十三行), an area where Westerners lived and traded in the Qing Dynasty
Nam Pak Hong (南北行), a group of traditional Chinese medicine traders from North China
Hongmen (洪門), a Chinese fraternal organization
Hong (genus), a genus of ladybird