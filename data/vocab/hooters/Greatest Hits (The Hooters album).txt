Greatest Hits is a compilation album by the American rock band, The Hooters released in 1992.


== Background ==
Greatest Hits contains songs from The Hooters' three albums on Columbia Records: Nervous Night (1985), One Way Home (1987) and Zig Zag (1989).


== Track listing ==
"All You Zombies" (Rob Hyman, Eric Bazilian) – 5:57
"Satellite" (Rob Hyman, Eric Bazilian, Rick Chertoff) – 4:15
"And We Danced" (Rob Hyman, Eric Bazilian) – 3:48
"500 Miles" (Hedy West, additional lyrics by Rob Hyman, Eric Bazilian, Rick Chertoff) – 4:24
"Don't Knock It 'Til You Try It" (Rob Hyman, Eric Bazilian) – 4:17
"Day by Day" (Rob Hyman, Eric Bazilian, Rick Chertoff) – 3:25
"Where Do the Children Go" (Rob Hyman, Eric Bazilian) – 5:28
"Johnny B" (Rob Hyman, Eric Bazilian, Rick Chertoff) – 3:59
"Fightin' on the Same Side" (Rob Hyman, Eric Bazilian, Rick Chertoff) – 4:07
"Brother, Don't You Walk Away" (Rob Hyman, Eric Bazilian, Rick Chertoff) – 4:27
"Karla With a K" (The Hooters) – 4:40
"Nervous Night" (Rob Hyman, Eric Bazilian, Rick Chertoff) – 3:57
"Give The Music Back" (Rob Hyman, Eric Bazilian) – 5:15
"Mr. Big Baboon'" (Rob Hyman, Eric Bazilian, Rick Chertoff) – 3:54