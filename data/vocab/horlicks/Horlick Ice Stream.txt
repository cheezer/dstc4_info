Horlick Ice Stream (85°17′S 132°00′W) is a large ice stream on the featureless ice surface to the north of the main mass of the Horlick Mountains of Antarctica, draining west-southwestward, parallel to these mountains, to enter the lower portion of the Reedy Glacier. It was mapped by the U.S. Geological Survey from surveys and U.S. Navy air photos, 1960–64, and was named by the Advisory Committee on Antarctic Names in association with the Horlick Mountains.


== See also ==
List of glaciers in the Antarctic
List of Antarctic ice streams


== References ==

 This article incorporates public domain material from the United States Geological Survey document "Horlick Ice Stream" (content from the Geographic Names Information System).