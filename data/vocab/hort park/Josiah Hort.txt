Josiah Hort (c. 1674 – 14 December 1751), was an English clergyman of the Church of Ireland who ended his career as archbishop of Tuam (1742–1751).
Brought up as a Nonconformist, Hort went to school with the hymn writer Isaac Watts, who was his lifelong friend. He began as a Nonconformist minister, but then conformed to the Church of England, attending Clare College, Cambridge.
In 1709 Hort went to Ireland to serve as chaplain for Thomas Wharton, 1st Marquess of Wharton, Lord-Lieutenant, and obtained a parish there. After two deaneries (Cloyne (1718–1720) and Ardagh (1720–1721)) and two bishoprics (firstly of Ferns, then of Kilmore & Ardagh), he became Archbishop of Tuam. He also served for a period as a preacher and a volume of his sermons on "practical subjects" went through several editions. Because the rise of the English clergy was unpopular in Ireland, Dean Jonathan Swift, launched a violent attack on him in a satirical poem. Later on Swift became friendly toward Hort.
In his will he exorted his children to carry out his intentions "without having recourse to law and the subtility of lawyers," and in the case of difficulty to refer questions to "the decision of persons of known probity and wisdom, this being not only the most Christian, but the most prudent and cheap and summary way of deciding all differences."


== Preaching ==
Hort used his own personal experiences as prefaces to his sermons. After being disabled from preaching by an overstrain of his voice, he warned "all young preachers whose organs of speech are tender," and said, "Experience shows that a moderate Degree of Voice, with a proper and distinct Articulation, is better understood in all Parts of a Church than a Thunder of Lungs that is rarely distinct, and never agreeable to the Audience." His sermons were expressed in simple, dignified language.


== Family ==
He married the Lady Elizabeth Fitzmaurice, sister of Thomas FitzMaurice, 1st Earl of Kerry. Their second son, John, married a woman who belonged to a branch of the Butler family and was appointed consul-General at Lisbon in 1767. That same year he was made a baronet. Sir John Hort was the grandfather of the English theologian Fenton John Anthony Hort and great grandfather of Richard Garnons Williams, soldier and international rugby player.
Two of Josiah Hort's daughters married into well known Irish families of that day and not many years after Hort's migration to Ireland the Hort family had established a fair claim to be considered Irish.
Josiah Hort is the earliest of the family name of whom any record is preserved. His father, of whom little is known, lived near Bath, England.


== References ==
Life and Letters of Fenton John Anthony Hort, Volume I, by Arthur Hort, London Macmillan and Co. Ltd., New York: Macmillan & Co., 1896, chapter one.