Boutique hotel is a term used initially in North America and the United Kingdom to describe small hotels in unique settings with upscale accommodations. The same concept is used in hotels in South America, Asia, and Africa.


== History ==
Boutique hotels began appearing in the 1980s in major cities like London, New York, and San Francisco. The term was coined by Steve Rubell in 1984 when he compared the Morgans Hotel, the first hotel he and Ian Schrager owned, to a boutique.


== Description ==
Many boutique hotels are furnished in a themed, stylish and/or aspirational manner. The popularity of the boutique concept has prompted some multi-national hotel companies to try and capture a market share. In the United States, New York City remains an important centre for boutique hotels clustered about Manhattan. Some members of the hospitality industry are following the general "no-frill chic" consumer trend, with affordable or budget boutique hotels being created all around the world.  Boutique hotels are found in London, New York City, Miami, and Los Angeles. They are also found in resort destinations with exotic amenities such as electronics, spas, yoga and/or painting classes.


== References ==