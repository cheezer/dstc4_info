The Downunder Hostel Fire was a lethal fire on 17 September 1989, set shortly before 5:00 am in a backpackers hostel on Darlinghurst Road in the Kings Cross area of Sydney, Australia. Kings Cross is a very popular destination with international backpackers visiting Australia. The fire was the fifth most deadly disaster in Australia in 1989.


== Fire ==
Six young tourists died in a fire that started in the lobby of the hostel. Arson was suspected. The fire quickly swept up through the stairwell. All fatalities were caused by carbon monoxide poisoning or smoke inhalation in the room on the top floor. At least 60 were rescued, 16 of whom were injured. The victims were British, Canadian, New Zealand, Danish, Austrian and Swedish.


== Aftermath ==
Gregory Alan Brown, who had a history of mental illness and drug and alcohol abuse, confessed to setting this fire and some 500 more. He was sentenced to 18 years for manslaughter. He was convicted, despite his alleged unreliable confession and a suggestion that he was in Melbourne at the time. The court found diminished responsibility, sentencing him for manslaughter instead of murder. Alleged to have boasted to another prisoner "I love hearing people scream and watching them die," he was released in 2009 after serving his full sentence, refusing to apply for parole.


== Savoy Hotel fire ==
In December 1975 the Savoy Private Hotel burned down with the loss of 15 lives and 25 seriously injured after a fire set by Reginald John Little. It was next door to a building that housed the Pink Panther strip club and a brothel called the Kingsdore Motel. This building later became the Downunder Hostel. Both buildings were owned by alleged crime-boss Abe "the boss of the Cross" Saffron, who has been linked to seven other fires.


== Similar Incidents ==
Since 1981, another 52 people have died in fires in backpackers hostels and other low-end boarding facilities. Nineteen people died in 1981 in the Rembrandt Apartments, also in Kings Cross. Twelve died in a fire in Dungog, New South Wales in 1991 at the Palm Grove Hostel. In 2000 the Childers Palace Backpackers Hostel in Childers, Queensland, burned down, costing 15 lives. On top of all that, 130 backpackers were evacuated after an arson attack at the Ocean View Lodge in Fremantle, Western Australia in March 1993, 10 from the City Heart Hostel in Rockhampton, Queensland in August 1996 after another arson attack. On October 13, 2011, a fire was started in a bin outside Noah's Bondi Beach hostel in Bondi Beach, New South Wales, where 130 backpackers were sleeping,.


== Results ==
The Downunder fire eventually led to new stringent safety procedures. The coroner made recommendations about the number of fire exits, sprinkler systems, labelling of fire doors, fire extinguishers and other fire-fighting equipment and staffing. The local council formed a safety department but enforcement remains sporadic as local councils are not reimbursed for legal fees in Land and Environment Court.


== References ==


== External links ==
Sonya Zadel; Malcolm Brown (September 17, 1989). "'Sheer Panic' As Guests Escape". Sydney Morning Herald. 
"1989, September 17, Kings Cross fire". NSW Ministry of Police and Emergency Services. 
Pyromania : the fire at the Downunder Hostel in Kevin M. Waller (1994). Suddenly Dead : Ten Famous Cases Through The Eyes Of The Coroner. Ironbark, Chippendale, NSW. ISBN 9780330272582.