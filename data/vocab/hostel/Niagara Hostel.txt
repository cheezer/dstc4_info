The Niagara Hostel is a hostel in Niagara Falls, Ontario, Canada. The establishment provides inexpensive lodging for travellers and tourists, in particular backpackers. The hostel is close in proximity to the international border between Canada and the United States, allowing guests access to the Southern Ontario and Western New York areas.


== History ==
The hostel is sandwiched between the old Niagara Falls gaol (jail) and the Whirlpool Rapids Bridge, two landmarks in the history of U.S.-Canada relations. In the past, the building that houses today's modern hostel has seen service as a rooming house for rail workers from both Michigan Central and CP Rail. accommodations today feature a mixture of sunny rooms, creative community members and a breeze from the nearby Niagara Gorge.
Visitors can use distinctive amenities of the hostel such as an alternative library and a book exchange. The hostel has a full weekly schedule of events including jam sessions, international potluck events, New Moon meditation ceremonies, subculture sewing circles and regular demonstrations of fire spinning.
In 2006, a new Internet cafe was installed. In March, management of the Niagara Hostel began a new initiative to improve safety for guests and employees.


== Notable events ==


=== Niagara Drum Circle ===
The hostel has gained notoriety in the local media for a weekly drum circle and folk jam which has been used occasionally to educate travellers from around the world about traditional Canadian healing crafts.
The group has attracted musicians from both Canada and the U.S.A.


== See also ==
Hostel
Adventure travel
Backpacking (Canada)
Tourism in Canada


== External links ==
ACBB Hostel Niagara
Hostelling International
HI-Niagara Falls Hostel Website