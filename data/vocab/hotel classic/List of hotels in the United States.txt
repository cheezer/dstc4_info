This is a list of hotels in the United States, both current and defunct, organized by state. The list includes highly rated luxury hotels, skyscraper rated buildings, and historic hotels. It is not a directory of every chain or independent hotel building in the United States.
The navigation box below directs to the section for each state or territory.


== Alabama ==


=== Mobile ===


==== Birmingham ====


== Alaska ==


== Arizona ==


== Arkansas ==


== Colorado ==


=== Denver ===


== Connecticut ==


== Delaware ==


== Florida ==


=== Miami and Miami Beach ===


=== Orlando ===


==== Walt Disney World area ====


== Georgia ==


=== Atlanta ===


== Hawaii ==


== Idaho ==


== Illinois ==


=== Chicago ===


== Indiana ==


=== Indianapolis ===


== Iowa ==


== Kansas ==


== Kentucky ==


== Louisiana ==
Hilton Capitol Center
L'Auberge du Lac Resort
Sam's Town Hotel and Gambling Hall, Shreveport


=== New Orleans ===


== Maine ==


== Maryland ==


== Massachusetts ==


=== Boston ===


== Michigan ==


=== Detroit ===


== Minnesota ==


== Mississippi ==


== Missouri ==


== Montana ==


== Nebraska ==


=== Omaha ===


== Nevada ==


=== Las Vegas ===


=== Primm ===
Buffalo Bill's
Primm Valley Resort
Whiskey Pete's


=== Reno-Sparks ===


== New Hampshire ==


== New Jersey ==


== New Mexico ==


== New York ==


=== Long Island ===


=== New York City ===


==== Bronx ====


==== Brooklyn ====


==== Manhattan ====


==== Queens ====


=== Upstate New York ===


=== Westchester County ===


== North Carolina ==


== North Dakota ==


== Ohio ==


=== Cincinnati ===


=== Cleveland ===


== Oklahoma ==
Buffington Hotel
Price Tower


=== Oklahoma City ===
Colcord Hotel
Skirvin Hilton Hotel


=== Tulsa ===
Ambassador Hotel
Atlas Life Building
Mayo Hotel


== Oregon ==


=== Portland ===


== Pennsylvania ==


=== Philadelphia ===


=== Pittsburgh ===


== Rhode Island ==


== South Carolina ==


== South Dakota ==
Brown Palace Hotel (Mobridge, South Dakota)
Bullock Hotel
Calumet Hotel (Wasta, South Dakota)


== Tennessee ==


=== Memphis ===


== Texas ==


=== Austin ===
Driskill Hotel
Four Seasons Hotel Austin


=== Dallas ===


=== Houston ===


=== San Antonio ===


== Utah ==


== Vermont ==


== Virginia ==


=== Hampton Roads ===
The Chamberlin
The Monticello Hotel
Norfolk Waterside Marriott
Westin Virginia Beach Town Center


=== Richmond ===
Four Points by Sheraton Richmond
Four Points by Sheraton Richmond Airport
Exchange Hotel (Richmond, Virginia)
Jefferson Hotel (Richmond, Virginia)
Murphy's Hotel
Hotel Richmond


=== Staunton ===
American Hotel (Staunton, Virginia)
Stonewall Jackson Hotel and Conference Center
Valley Hotel
Virginia Hotel


== Washington ==


=== Seattle ===


== Washington, D.C. ==


== West Virginia ==


== Wisconsin ==


== Wyoming ==


== Territories ==


=== American Samoa ===
Rainmaker Hotel, Pago Pago
Tradewinds Hotel, Fagatogo


=== Guam ===
Hotel Nikko Guam


=== Northern Mariana Islands ===


=== Puerto Rico ===


=== U.S. Virgin Islands ===
Caneel Bay


== See also ==
Tourist attractions in the United States
List of hotels – indexed by country
Lists of hotels – an index of hotel list articles on Wikipedia
List of defunct hotel chains
List of motels


== References ==