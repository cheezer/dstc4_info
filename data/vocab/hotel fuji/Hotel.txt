A hotel is an establishment that provides lodging paid on a short-term basis. Facilities provided may range from a basic bed and storage for clothing, to luxury features like en-suite bathrooms. Larger hotels may provide additional guest facilities such as a swimming pool, business center, childcare, conference facilities and social function services. Hotel rooms are usually numbered (or named in some smaller hotels and B&Bs) to allow guests to identify their room. Some hotels offer meals as part of a room and board arrangement. In the United Kingdom, a hotel is required by law to serve food and drinks to all guests within certain stated hours. In Japan, capsule hotels provide a minimized amount of room space and shared facilities.
The precursor to the modern hotel was the inn of medieval Europe. For a period of about 200 years from the mid-17th century, coaching inns served as a place for lodging for coach travelers. Inns began to cater for richer clients in the mid-18th century. One of the first hotels in a modern sense was opened in Exeter in 1768. Hotels proliferated throughout Western Europe and North America in the 19th century, and luxury hotels began to spring up in the later part of the century.
Hotel operations vary in size, function, and cost. Most hotels and major hospitality companies have set industry standards to classify hotel types. An upscale full-service hotel facility offers luxury amenities, full service accommodations, on-site full service restaurant(s), and the highest level of personalized service. Full service hotels often contain upscale full-service facilities with a large volume of full service accommodations, on-site full service restaurant(s), and a variety of on-site amenities. Boutique hotels are smaller independent non-branded hotels that often contain upscale facilities. Small to medium-sized hotel establishments offer a limited amount of on-site amenities. Economy hotels are small to medium-sized hotel establishments that offer basic accommodations with little to no services. Extended stay hotels are small to medium-sized hotels that offer longer term full service accommodations compared to a traditional hotel.
Timeshare and Destination clubs are a form of property ownership involving ownership of an individual unit of accommodation for seasonal usage. A motel is a small-sized low-rise lodging with direct access to individual rooms from the car park. Boutique hotels are typically hotels with a unique environment or intimate setting. A number of hotels have entered the public consciousness through popular culture, such as the Ritz Hotel in London. Some hotels are built specifically as a destination in itself, for example at casinos and holiday resorts.
Most hotel establishments consist of a General Manager who serves as the head executive (often referred to as the "Hotel Manager"), department heads who oversee various departments within a hotel, middle managers, administrative staff, and line-level supervisors. The organizational chart and volume of job positions and hierarchy varies by hotel size, function, and is often determined by hotel ownership and managing companies.


== Etymology ==
The word hotel is derived from the French hôtel (coming from the same origin as hospital), which referred to a French version of a building seeing frequent visitors, and providing care, rather than a place offering accommodation. In contemporary French usage, hôtel now has the same meaning as the English term, and hôtel particulier is used for the old meaning, as well as "hôtel" in some place names such as Hôtel-Dieu (in Paris), which has been a hospital since the Middle Ages. The French spelling, with the circumflex, was also used in English, but is now rare. The circumflex replaces the 's' found in the earlier hostel spelling, which over time took on a new, but closely related meaning. Grammatically, hotels usually take the definite article – hence "The Astoria Hotel" or simply "The Astoria."


== History ==

Facilities offering hospitality to travellers have been a feature of the earliest civilizations. In Greco-Roman culture hospitals for recuperation and rest were built at thermal baths. During the Middle Ages various religious orders at monasteries and abbeys would offer accommodation for travellers on the road.
The precursor to the modern hotel was the inn of medieval Europe, possibly dating back to the rule of Ancient Rome. These would provide for the needs of travelers, including food and lodging, stabling and fodder for the traveler's horse(s) and fresh horses for the mail coach. Famous London examples of inns include the George and the Tabard. A typical layout of an inn had an inner court with bedrooms on the two sides, with the kitchen and parlour at the front and the stables at the back.
For a period of about 200 years from the mid-17th century, coaching inns served as a place for lodging for coach travelers (in other words, a roadhouse). Coaching inns stabled teams of horses for stagecoaches and mail coaches and replaced tired teams with fresh teams. Traditionally they were seven miles apart but this depended very much on the terrain.

Some English towns had as many as ten such inns and rivalry between them was intense, not only for the income from the stagecoach operators but for the revenue for food and drink supplied to the wealthy passengers. By the end of the century, coaching inns were being run more professionally, with a regular timetable being followed and fixed menus for food.
Inns began to cater for richer clients in the mid-18th century, and consequently grew in grandeur and the level of service provided. One of the first hotels in a modern sense was opened in Exeter in 1768, although the idea only really caught on in the early 19th century. In 1812 Mivart's Hotel opened its doors in London, later changing its name to Claridge's.
Hotels proliferated throughout Western Europe and North America in the 19th century, and luxury hotels, including Tremont House and Astor House in the United States, Savoy Hotel in the United Kingdom and the Ritz chain of hotels in London and Paris, began to spring up in the later part of the century, catering to an extremely wealthy clientele.


== International scale ==
Hotels cater to travelers from many countries and languages, since no one country dominates the travel industry.


== Types ==

Hotel operations vary in size, function, and cost. Most hotels and major hospitality companies that operate hotels have set widely accepted industry standards to classify hotel types. General categories include the following:


=== Upscale luxury ===
An upscale full service hotel facility that offers luxury amenities, full service accommodations, on-site full service restaurant(s), and the highest level of personalized and professional service. Luxury hotels are normally classified with at least a Four Diamond or Five Diamond status or a Four or Five Star rating depending on the country and local classification standards. Examples may include: InterContinental, Waldorf Astoria, Four Seasons, Conrad, Fairmont, and The Ritz-Carlton.


=== Full service ===
Full service hotels often contain upscale full-service facilities with a large volume of full service accommodations, on-site full service restaurant(s), and a variety of on-site amenities such as swimming pools, a health club, children's activities, ballrooms, on-site conference facilities, and other amenities. Examples may include: Starwood – Westin, Hilton, Marriott, and Hyatt hotels


=== Historic inns and boutique hotels ===
Boutique hotels are smaller independent non-branded hotels that often contain upscale facilities of varying size in unique or intimate settings with full service accommodations. Some historic inns and boutique hotels may be classified as luxury hotels.


=== Focused or select service ===
Small to medium-sized hotel establishments that offer a limited amount of on-site amenities that only cater and market to a specific demographic of travelers, such as the single business traveler. Most focused or select service hotels may still offer full service accommodations but may lack leisure amenities such as an on-site restaurant or a swimming pool. Examples include Courtyard by Marriott and Hilton Garden Inn.


=== Economy and limited service ===
Small to medium-sized hotel establishments that offer a very limited amount of on-site amenities and often only offer basic accommodations with little to no services, these facilities normally only cater and market to a specific demographic of travelers, such as the budget-minded traveler seeking a "no frills" accommodation. Limited service hotels often lack an on-site restaurant but in return may offer a limited complimentary food and beverage amenity such as on-site continental breakfast service. Examples include Hampton Inn, Aloft, Holiday Inn Express, Fairfield Inn, Four Points by Sheraton, and Days Inn.


=== Extended stay ===
Extended stay hotels are small to medium-sized hotels that offer longer term full service accommodations compared to a traditional hotel. Extended stay hotels may offer non-traditional pricing methods such as a weekly rate that cater towards travelers in need of short-term accommodations for an extended period of time. Similar to limited and select service hotels, on-site amenities are normally limited and most extended stay hotels lack an on-site restaurant. Examples include Staybridge Suites, Homewood Suites by Hilton, Home2 Suites by Hilton, Residence Inn by Marriott, Element, and Extended Stay Hotels.


=== Timeshare and destination clubs ===
Timeshare and Destination clubs are a form of property ownership also referred to as a vacation ownership involving the purchase and ownership of an individual unit of accommodation for seasonal usage during a specified period of time. Timeshare resorts often offer amenities similar that of a Full service hotel with on-site restaurant(s), swimming pools, recreation grounds, and other leisure-oriented amenities. Destination clubs on the other hand may offer more exclusive private accommodations such as private houses in a neighborhood-style setting. Examples of timeshare brands include Hilton Grand Vacations, Marriott Vacation Club International, Westgate Resorts, Starwood Vacation Ownership, and Disney Vacation Club.


=== Motel ===
A motel is a small-sized low-rise lodging establishment similar to that of a limited service hotel, but with direct access to individual rooms from the car park. Common during the 1950s and 1960s, motels were often located adjacent to a major road, where they were built on inexpensive land at the edge of towns or along stretches of highways .
New motel construction is rare as hotel chains have been building economy limited service franchised properties at freeway exits which compete for largely the same clientele, largely saturating the market by the 1990s. They are still useful in less populated areas for driving travelers, but the more populated an area becomes the more hotels fill the need. Many of the motels which remain in operation have joined national franchise chains, rebranding themselves as hotels, inns or lodges.


== Management ==

Hotel management is a globally accepted professional career field and academic field of study. Degree programs such as hospitality management studies, a business degree, and/or certification programs formally prepare hotel managers for industry practice.
Most hotel establishments consist of a General Manager who serves as the head executive (often referred to as the "Hotel Manager"), department heads who oversee various departments within a hotel, middle managers, administrative staff, and line-level supervisors. The organizational chart and volume of job positions and hierarchy varies by hotel size, function, and is often determined by hotel ownership and managing companies.


== Unique and specialty hotels ==


=== Historic Inns and boutique hotels ===

Boutique hotels are typically hotels with a unique environment or intimate setting. Some hotels have gained their renown through tradition, by hosting significant events or persons, such as Schloss Cecilienhof in Potsdam, Germany, which derives its fame from the Potsdam Conference of the World War II allies Winston Churchill, Harry Truman and Joseph Stalin in 1945. The Taj Mahal Palace & Tower in Mumbai is one of India's most famous and historic hotels because of its association with the Indian independence movement. Some establishments have given name to a particular meal or beverage, as is the case with the Waldorf Astoria in New York City, United States where the Waldorf Salad was first created or the Hotel Sacher in Vienna, Austria, home of the Sachertorte. Others have achieved fame by association with dishes or cocktails created on their premises, such as the Hotel de Paris where the crêpe Suzette was invented or the Raffles Hotel in Singapore, where the Singapore Sling cocktail was devised.

A number of hotels have entered the public consciousness through popular culture, such as the Ritz Hotel in London, through its association with Irving Berlin's song, 'Puttin' on the Ritz'. The Algonquin Hotel in New York City is famed as the meeting place of the literary group, the Algonquin Round Table, and Hotel Chelsea, also in New York City, has been the subject of a number of songs and the scene of the stabbing of Nancy Spungen (allegedly by her boyfriend Sid Vicious).


=== Resort hotels ===

Some hotels are built specifically as a destination in itself to create a captive trade, example at casinos and holiday resorts. Though of course hotels have always been built in popular destinations, the defining characteristic of a resort hotel is that it exists purely to serve another attraction, the two having the same owners.
On the Las Vegas Strip there is a tradition of one-upmanship with luxurious and extravagant hotels in a concentrated area. This trend now has extended to other resorts worldwide, but the concentration in Las Vegas is still the world's highest: nineteen of the world's twenty-five largest hotels by room count are on the Strip, with a total of over 67,000 rooms.
In Europe Center Parcs might be considered a chain of resort hotels, since the sites are largely man-made (though set in natural surroundings such as country parks) with captive trade, whereas holiday camps such as Butlins and Pontin's are probably not considered as resort hotels, since they are set at traditional holiday destinations which existed before the camps.


=== Other speciality hotels ===

The Burj al-Arab hotel in Dubai, United Arab Emirates, built on an artificial island, is structured in the shape of a boat's sail.
The Library Hotel in New York City, is unique in that each of its ten floors is assigned one category from the Dewey Decimal System.
The Jailhotel Löwengraben in Lucerne, Switzerland is a converted prison now used as a hotel.
The Luxor, a hotel and casino on the Las Vegas Strip in Paradise, Nevada, United States is unusual due to its pyramidal structure.
The Liberty Hotel in Boston, used to be the Charles Street Jail.
Hotel Kakslauttanen in Finland, a collection of glass igloos in Lapland that allow you to watch the Northern Lights
Built in Scotland and completed in 1936, The former ocean liner RMS Queen Mary in Long Beach, California, United States uses its first-class staterooms as a hotel, after retiring in 1967 from Transatlantic service.
The Wigwam Motels used patented novelty architecture in which each motel room was a free-standing concrete wigwam or teepee.
Various Caboose Motel or Red Caboose Inn properties are built from decommissioned rail cars.
Throughout the world there are several hotels built from converted airliners.


=== Bunker hotels ===
The Null Stern Hotel in Teufen, Appenzellerland, Switzerland and the Concrete Mushrooms in Albania are former nuclear bunkers transformed into hotels.


=== Cave hotels ===
The Cuevas Pedro Antonio de Alarcón (named after the author) in Guadix, Spain, as well as several hotels in Cappadocia, Turkey, are notable for being built into natural cave formations, some with rooms underground. The Desert Cave Hotel in Coober Pedy, South Australia is built into the remains of an opal mine.


=== Cliff hotels ===

Located on the coast but high above sea level, these hotels offer unobstructed panoramic views and a great sense of privacy without the feeling of total isolation. Some examples from around the globe are the Riosol Hotel in Gran Canaria, Caruso Belvedere Hotel in Amalfi Coast (Italy), Aman Resorts Amankila in Bali, Birkenhead House in Hermanus (South Africa), The Caves in Jamaica and Caesar Augustus in Capri.


=== Capsule hotels ===

Capsule hotels are a type of economical hotel first introduced in Japan, where people sleep in stacks of rectangular containers.


=== Ice, snow and igloo hotels ===

Igloo Village in Kakslauttanen,the Ice Hotel in Jukkasjärvi, Sweden is the first ice hotel in the world, built in 1990, and the Hotel de Glace in Duschenay, Canada, melt every spring and are rebuilt each winter; the Mammut Snow Hotel in Finland is located within the walls of the Kemi snow castle; and the Lainio Snow Hotel is part of a snow village near Ylläs, Finland.


=== Garden hotels ===
Garden hotels, famous for their gardens before they became hotels, include Gravetye Manor, the home of garden designer William Robinson, and Cliveden, designed by Charles Barry with a rose garden by Geoffrey Jellicoe.


=== Referral hotel ===

A referral hotel is a hotel chain that offers branding to independently-operated hotels; the chain itself is founded by or owned by the member hotels as a group. Many former referral chains have been converted to franchises; the largest surviving member-owned chain is Best Western.


=== Railway hotels ===

Frequently, expanding railway companies built grand hotels at their termini, such as the Midland Hotel, Manchester next to the former Manchester Central Station, and in London the ones above St Pancras railway station and Charing Cross railway station. London also has the Chiltern Court Hotel above Baker Street tube station, there are also Canada's grand railway hotels. They are or were mostly, but not exclusively, used by those traveling by rail.


=== Straw bale hotels ===
The Maya Guesthouse in Nax Mont-Noble in the Swiss Alps, is the first hotel in Europe built entirely with straw bales. Due to the insulation values of the walls it needs no conventional heating or air conditioning system, although the Maya Guesthouse is built at an altitude of 1,300 metres (4,300 ft) in the Alps.


=== Transit hotels ===

Transit hotels are short stay hotels typically used at international airports where passengers can stay while waiting to change airplanes. The hotels are typically on the airside and do not require a visa for a stay.


=== Treehouse hotels ===
Some hotels are built with living trees as structural elements, for example the Treehotel near Piteå, Sweden, the Costa Rica Tree House in the Gandoca-Manzanillo Wildlife Refuge, Costa Rica; the Treetops Hotel in Aberdare National Park, Kenya; the Ariau Towers near Manaus, Brazil, on the Rio Negro in the Amazon; and Bayram's Tree Houses in Olympos, Turkey.


=== Underwater hotels ===
Some hotels have accommodation underwater, such as Utter Inn in Lake Mälaren, Sweden. Hydropolis, project in Dubai, would have had suites on the bottom of the Persian Gulf, and Jules' Undersea Lodge in Key Largo, Florida requires scuba diving to access its rooms.


== Records ==


=== Largest ===

In 2006, Guinness World Records listed the First World Hotel in Genting Highlands, Malaysia, as the world's largest hotel with a total of 6,118 rooms. The Izmailovo Hotel in Moscow has the most rooms, with 7,500, followed by The Venetian and The Palazzo complex in Las Vegas (7,117 rooms) and MGM Grand Las Vegas complex (6,852 rooms).


=== Oldest ===
According to the Guinness Book of World Records, the oldest hotel in operation is the Nisiyama Onsen Keiunkan in Yamanashi, Japan. The hotel, first opened in 707 A.D. has been operated by the same family for forty-six generations. The title was held until 2011 by the Hoshi Ryokan, in the Awazu Onsen area of Komatsu, Japan, which opened in the year 718, as the history of the Nisiyama Onsen Keiunkan was virtually unknown.


=== Highest ===
The Ritz-Carlton Hong Kong claims to be the world's highest hotel. It is located on the top floors of the International Commerce Centre in Hong Kong, at 484 metres (1,588 ft) above ground level.


=== Most expensive purchase ===
In October 2014, the Anbang Insurance Group, based in China, purchased the Waldorf Astoria New York in Manhattan for US$1.95 billion, making it the world's most expensive hotel ever sold.


== Living in hotels ==
A number of public figures have notably chosen to take up semi-permanent or permanent residence in hotels.
Fashion designer Coco Chanel lived in the Hôtel Ritz, Paris, on and off for more than 30 years.
Inventor Nikola Tesla lived the last ten years of his life at the New Yorker Hotel until he died in his room in 1943.
Larry Fine (of The Three Stooges) and his family lived in hotels, due to his extravagant spending habits and his wife's dislike for housekeeping. They first lived in the President Hotel in Atlantic City, New Jersey, where his daughter Phyllis was raised, then the Knickerbocker Hotel in Hollywood. Not until the late 1940s did Larry buy a home in the Los Feliz area of Los Angeles.
The Waldorf-Astoria Hotel and its affiliated Waldorf Towers has been the home of many famous persons over the years including former President Herbert Hoover who lived there from the end of his presidency in 1933 until his death in 1964. General Douglas MacArthur lived his last 14 years in the penthouse of the Waldorf Towers. And the composer Cole Porter also spent the last 25 years of his life in an apartment at the Waldorf Towers.
Millionaire Howard Hughes lived in hotels during the last ten years of his life (1966–76), primarily in Las Vegas, as well as Acapulco, Beverly Hills, Boston, Freeport, London, Managua, Nassau, Vancouver, and others.
Vladimir Nabokov and his wife Vera lived in the Montreux Palace Hotel in Montreux, Switzerland (1961-his death in 1977).
Actor Richard Harris lived at the Savoy Hotel while in London. Hotel archivist Susan Scott recounts an anecdote that, when he was being taken out of the building on a stretcher shortly before his death in 2002, he raised his hand and told the diners "it was the food."
Egyptian actor Ahmed Zaki lived his last 15 years in Ramses Hilton Hotel – Cairo.
British entrepreneur Jack Lyons lived in the Hotel Mirador Kempinski in Switzerland for several years until his death in 2008.
American actress Elaine Stritch lived in the Savoy Hotel in London for over a decade.


== See also ==
List of hotels
Lists of hotels
List of chained-brand hotels
List of defunct hotel chains
Casino hotel

List of casino hotels

Niche tourism markets


=== Industry and careers ===


=== Human habitation types ===


== References ==


== Further reading ==