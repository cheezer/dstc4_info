Hayes Hotel is a hotel in Libery Square Thurles, County Tipperary, Ireland. In 1884 the Gaelic Athletic Association was founded in the billiards room of the hotel.


== History ==
The hotel traded under the name 'The Star and Garter' in the 18th Century. The hotel was purchased in the 1830s by William Boyton and became known as 'Boyton's Hotel'. In the 1870s the hotel was purchased by Miss Eliza J. Hayes and thus became known as Hayes' Commercial and Family Hotel.
On the 1 November 1884, a group of Irishmen gathered in the Hotel billiard room to formulate a plan and establish an organisation to foster and preserve Ireland's unique games and athletic pastimes. And so was founded one of the world's greatest amateur associations, the GAA. The architects and founding members were Michael Cusack of County Clare, Maurice Davin, John K. Bracken, George McCarthy, P.J. Ryan of Tipperary, John Wise-Power, and John McKay.
The hotel is a popular venue on the day of the Munster final when it is held in Thurles, especially when the final is between traditional rivals Tipperary and Cork.
In April 2013, Hayes' Hotel, went into receivership. The hotel will remain open following the appointment of receivers, however the hotel currently doesn't offer room sales.
In October 2014, Fethard native Jack Halley took ownership of the Hotel, buying it at Auction or €650,000, and on 22 November 2014 the hotel launched their newly refurbished nightclub the HH Club which includes a music venue and nightclub.


== See also ==
History of the Gaelic Athletic Association


== References ==


== External links ==
Official Website (Archived)