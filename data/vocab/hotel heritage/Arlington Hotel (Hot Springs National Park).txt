The Arlington Resort Hotel & Spa is a resort in the Ouachita Mountains of Hot Springs National Park, Arkansas, home of Oaklawn Race Track and the Arkansas Derby. The Arlington's sister hotel was the Baker Hotel in Mineral Wells, Texas. The hotel is located at the north end of "Bathhouse Row".


== History ==

Samuel W. Fordyce and two other entrepreneurs financed the construction of the first luxury hotel in the area, the first Arlington Hotel, which opened in 1875. After almost 20 years of use, it was razed to build a new hotel.
When it was rebuilt in 1892-93, the hotel was known as the New Arlington, and boasted of its Spanish Renaissance architecture. With 300 rooms in four stories of red brick, it had corner towers. This second Arlington burned to the ground on April 5, 1923, killing one fireman and causing an estimated $1.6 million in damage (1923 dollars).

Those buildings were at the north end of Bathhouse Row, where the Arlington Park then was created. The third Arlington Hotel, designed by Mann and Stern in 1925, is the current hotel at the "Y" intersection at the corner of Central Avenue and Fountain Street. The building's huge size, Spanish-Colonial Revival style, and placement at the terminus of the town's most important vista made the building a key Hot Springs landmark. The original site became a park at the north end of Bathhouse Row.
In the 1930s, the Arlington Hotel was a favorite vacation spot for Al Capone.
The Lobby Bar of the Arlington is on Esquire Magazine's list of the best bars in America. [1]


== References ==


== External links ==
Official Site