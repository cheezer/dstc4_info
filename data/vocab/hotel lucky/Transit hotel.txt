A transit hotel is a short-stay hotel typically used at international airports in the transit zone where passengers on extended waits between planes (typically a minimum of six hours) can stay. The hotel is within the or airside security/passport checkpoints and close to the airport terminals.
No entry visa into the country is required to stay.


== References ==