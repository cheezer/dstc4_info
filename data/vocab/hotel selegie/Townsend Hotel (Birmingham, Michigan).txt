The Townsend Hotel is a luxury boutique hotel located in the Metro Detroit city of Birmingham, Michigan. The hotel contains the Ruby Grille restaurant. In addition, the hotel ballroom can accommodate conferences of up to 500 people. The hotel architecture is in the English manor house style.


== Reception ==
The hotel has received several awards including the AAA Four Diamond Award and Travel and Leisure Magazine's World's Best Award.


== References ==

Cantor, George (2005). Detroit: An Insiders Guide to Michigan. University of Michigan Press. ISBN 0-472-03092-2.