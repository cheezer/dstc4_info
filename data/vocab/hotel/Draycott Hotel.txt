Draycott Hotel is a 5-star hotel in London, England.


== Location ==
The Draycott Hotel is located in a 19th-century townhouse at 26 Cadogan Gardens in Knightsbridge,within the borough of Kensington & Chelsea. It is situated near Sloane Square.


== Notable Guests ==
Previous guests have included the Queen of Denmark, Gary Oldman and Pierce Brosnan.


== References ==


== External links ==
Official site