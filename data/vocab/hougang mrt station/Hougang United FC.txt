Hougang United Football Club is a professional football club in Singapore's professional league S.League.


== History ==
Hougang United Football Club was founded as Marine Castle United Football Club, which was formed by Newcastle United fans in the Marine Parade area. Upon successful entry into the S.League in 1998, the club struggled in its early foray in the first few years, finishing in the bottom two for the next four seasons.
Marine Castle changed its name to Sengkang Marine Football Club in 2002 and finished in 8th position consecutively, its highest ever finish in its short history.
Financial difficulties then forced Sengkang Marine out of the S.League in 2004, and Paya Lebar-Punggol Football Club took its place in 2005, finishing the season as wooden-spoonist. The two clubs then merged their resources from 2006 to 2010, to form Sengkang Punggol Football Club, finishing no higher than 10th.
On 1 January 2011, the chairman of Sengkang Punggol Football Club, Mr. Bill Ng, announced the changes that began the rewriting of another chapter of this football club. With improved financial status and a change in name to the present Hougang United Football Club, there was renewed optimism among the Hougang fan base around the club's home stadium since its inception, Hougang Stadium. The club has a group of supporters known as the HOOLS (Hougang Only One Love). The club also had its fair share of ‘marquee players’ in the earlier days like Grant Holt (formerly Norwich City), Michael Currie (formerly Queens Park Rangers) and Kim See-man (former South Korea youth international).
In November 2014, it was announced that Hougang United and Woodlands Wellington will merge for the 2015 season. With the announcement that Hougang United will retain its name after the merger and assimilation of most of Woodlands staff into Hougang's structure, including players and head coach Salim Moin, Woodlands Wellington ceased to exist in the S.League from 2015.


=== Partnership ===
On 22 November 2014, Hougang United announced the partnership with Global Football Academy for the 2015 S.League season. 


=== Scholarship ===
Hougang United FC Scholarship was launched in May 2015, the aim of the scholarship is to support and facilitate the academic development of young non-professional footballing talents. 


== Seasons ==
2003 saw the introduction of penalty shoot-outs if a match ended in a draw in regular time. Winners of penalty shoot-outs gained two points instead of one.
Sengkang Marine sat out the 2004 S.League season. They merged with Paya Lebar Punggol to form Sengkang Marine on their return to the S.League in 2006.
Hougang United deducted 5 point for a pre-match brawl with Etoile during the 2011 season.
Last updated on 15 May 2014


== Players ==


=== Current Squad ===
As of 28 June 2015 
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== Prime League Squad ===
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


== Managers ==
 Trevor Morgan (2005-2007)
 Aide Iskandar (interim) (June 25, 2009–Dec 31, 2009)
 Aide Iskandar (Jan 1, 2010–Dec 31, 2011)
 Nenad Bacina (Dec 1, 2011–Nov 30, 2012)
 Johana Bin Johari (Nov 30, 2012–1?)
 Alex Weaver (Jan 1, 2013–March 31, 2013)
 Johana Bin Johari (April 2013 – August 2013)
 Amin Nasir (Aug 21, 2013 – Dec 31, 2014)
 Salim Moin (Jan 1, 2015 – )


== Sponsors ==
Main Sponsor: ESW Manage
Kit Supplier: Vonda


== References ==


== External links ==
Official Hougang United FC Website
S.League website page on Hougang United FC
Official Hougang United FC Facebook page
Official Hougang United Twitter
Official Hougang United Instagram