Baden-Powell House, colloquially known as B-P House, is a Scouting hostel and conference centre in South Kensington, London, which was built as a tribute to Lord Baden-Powell, the founder of Scouting. The house, owned by The Scout Association, hosts a small exhibition relating to Scouting in its current form and a granite statue by Don Potter.
The building committee, chaired by Sir Harold Gillett, Lord Mayor of London, purchased the site in 1956, and assigned Ralph Tubbs to design the house in the modern architectural style. The foundation stone was laid in 1959 by World Chief Guide Olave, Lady Baden-Powell, and it was opened in 1961 by Queen Elizabeth II. The largest part of the £400,000 cost was provided by the Scout Movement itself. Over the years, the house has been refurbished several times, so that it now provides modern and affordable lodging for Scouts, Guides, their families and the general public staying in London. The building also hosts conference and event space for hire.


== History ==
Acting on a 1942 initiative by Chief Scout Lord Somers, a formal Baden-Powell House Committee was established by The Scout Association in 1953 under the direction of Sir Harold Gillett, later Lord Mayor of London. The committee's directive was to build a hostel to provide Scouts a place to stay at reasonable cost while visiting London. For this purpose, in 1956 the committee purchased a bombed-out property at the intersection of Cromwell Road and Queen's Gate at a cost of £39,000.
The Scout Movement raised the major part of the funding of £400,000 for building and furnishing the building between 1957 and 1959. Scouts throughout the Country collected 'ship' halfpennies, and this raised the bulk of the money for the building. Money was also raised through public appeals supported by publication in Scout Movement magazines, a collection of donations in 15,000 brick-shaped boxes, and 5,000 appeal letters signed personally by then Chief Scout Lord Rowallan. Scouts representing every county were present at the opening.
In a celebration on 17 October 1959 the foundation stone was laid by the World Chief Guide (Olave Baden-Powell), in the presence of Lord Mayor Sir Harold Gillett, the new Chief Scout Sir Charles Maclean, and 400 other guests. A casket was buried under the foundation stone which held 1959 Scout mementoes, stamps, coins, photographs, etc., and a programme of the ceremony.
With 142 Queen's Scouts as Guard of Honour, and live broadcast by the BBC (commentator Richard Dimbleby), Baden-Powell House was opened on 12 July 1961 by Queen Elizabeth II. Afterwards, she toured the house with the Chief Scout and the president of The Scout Association, her uncle Prince Henry, Duke of Gloucester. A black marble panel with gold lettering was put on the balcony in the hall to commemorate the event.


== Modern architecture ==

The house was designed by the architect Ralph Tubbs in 1956, whose works included the Dome of Discovery, the highlight of the 1951 Festival of Britain. Tubbs' floor plans and a model of his design were displayed during a fundraising campaign and exhibition on 21 February 1957 in the Egyptian Hall of the Mansion House.
The six storied Baden-Powell House is designed in the modern architectural style, as pioneered by the Swiss architect Le Corbusier from the late 1920s onwards, and predominating in the 1950s. At Baden-Powell House, Tubbs made the first floor overhang the ground floor, a Le Corbusier architectural design choice to free the building from the ground, such as seen in his Pavillon Suisse at the Cité Internationale Universitaire in Paris. Additionally, Le Corbusier's Sainte Marie de La Tourette priory in Lyon shows two floors of monk's cells with small windows, cantilevered over the more open floors below, another design choice used by Tubbs in the facade of Baden-Powell House. While Tubbs created Baden-Powell House in the modern architectural style of Le Corbusier, he used more architectural restraint in his own design choices. For example, he made the main visible building component brick rather than concrete. This heavier evolution of Le Corbusier's style was popular in England throughout the post-war years until replaced by the Brutalist style in the later 1960s.
Baden-Powell House was built to Tubbs' design by Harry Neal Ltd, for which they received the 1961 Gold Medal of the Worshipful Company of Tylers and Bricklayers. At the opening, the house received the building design award for 'The building of most merit in London.'
Thirty-five years after its opening, Baden-Powell House was refurbished in a six-month £2 million programme, providing all modern amenities such as private facilities for all rooms, double glazing, and air conditioning, as well as enhancing conference facilities for large and small events. Upon completion of the programme, the house was opened by the president of The Scout Association, Prince Edward, Duke of Kent on 5 June 1997. In 2002 a Starbucks coffee (discontinued before 2015) and sandwich bar was opened, as well as an outdoor roof garden adjacent to the meeting conference rooms on the second floor.


== Baden-Powell collection ==
Although it has since been replaced with a number of smaller displays available to the public in the reception area showing some traditional Scouting skills, a notable collection of Baden-Powell memorabilia has been on display in the past for visitors in 'The story of B-P' exhibition. This included many drawings and letters by Baden-Powell himself, such as the original of his Last Message to Scouts, Laws for me when I am old and several first editions of his books. The former exhibition also displayed the original painting by David Jagger, as presented to Baden-Powell on 29 August 1929 at the 'Coming of Age' 3rd World Scout Jamboree. This painting, a personal favourite of Baden-Powell, is often used in publications throughout the Scout movement. The Baden-Powell memorabilia has since been moved to the headquarters for Scouting in the UK, Gilwell Park.
As an introductory part of the collection, a nearly 3 meter high statue of Baden-Powell has been erected in front of Baden-Powell House, the only granite statue in London. The sculptor was Baden-Powell's personal friend Don Potter. It was unveiled on 12 July 1961 by the Duke of Gloucester, as part of the official opening of the house.


== 21st century: Hostel and Conference centre ==

From 1974 to 2001, Baden-Powell House was the headquarters of The Scout Association, for which a dedicated extension to the house was completed in 1976. In April 2001, the headquarters formally moved to new accommodation at Gilwell Park. As the owner of Baden-Powell House, The Scout Association receives a net income out of the revenues of approximately £1.5 million.
Baden-Powell House provides a hostel for people visiting London. In the period 2004–2006 the hostel participated in the Youth Hostel Association, after which the Scout Association entered into an agreement with German company Meininger City Hostels. The building is still owned by The Scout Association, but it is run by Meininger. As part of the arrangement with this company Scout members from the UK and abroad are able to stay at a reduced rate. It is also a conference and event space. Baden-Powell House is rated Four Star by the Visit Britain Quality Assurance, and Mobility Level 1; also recent visitors rate it on average 4 out of 5.
The hostel and conference centre is entered through a wide glazed atrium which serves as a large foyer containing the cafe and some Scouting displays. From the atrium the large hall is reached which can serve as an auditorium with seating for up to 300 people. The first floor has a restaurant seating 100 guests; the second floor has meeting rooms, and conference facilities for groups up to 80 delegates per room. The upper floors contain 180 hostel bedrooms. In an average year, 30 thousand people spend the night, and 100 thousand meals are served in the restaurant.


== See also ==

Baden-Powell International House, 25-story hotel of The Scout Association of Hong Kong in Kowloon, Hong Kong
Ellsworth Augustus Scout House, the Boy Scouts of America hostel, in Mendham, New Jersey
Jamboree on the Air, call sign for Baden-Powell House is GB3BPH
Kandersteg International Scout Centre


== References ==


== External links ==

Official website of the Baden-Powell House conference centre
Official website of the Baden-Powell House youth hostel, Meininger Hotel London Hyde Park
Location of Baden-Powell House on Streetmap
Visit London City Guide, details on Baden-Powell House (Meininger)