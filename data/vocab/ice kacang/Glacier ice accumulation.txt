Glacier ice accumulation occurs through accumulation of snow and other frozen precipitation, as well as through other means including rime ice (freezing of water vapor on the glacier surface), avalanching from hanging glaciers on cliffs and mountainsides above, and re-freezing of glacier meltwater as superimposed ice. Accumulation is one element in the glacier mass balance formula, with ablation counteracting. With successive years in which accumulation exceeds ablation, then a glacier will experience positive mass balance, and its terminus will advance.


== Accumulation zones ==
Glaciologists subdivide glaciers into glacier accumulation zones, based on the melting and refreezing occurring. These zones include the dry snow zone, in which the ice entirely retains subfreezing temperatures and no melting occurs. Dry snow zones only occur within the interior regions of the Greenland and Antarctica ice sheets. Below the dry snow zone is the percolation zone, where some meltwater penetrates down into the glacier where it refreezes. In the wet snow zone, all the seasonal snow melts. The meltwater either percolates into the depths of the glacier or flows down-glacier where it might refreeze as superimposed ice. A glacier's equilibrium line is located at the lower limit of the wet snow zone.


== References ==