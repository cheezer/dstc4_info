Ice is the solid form of water, or, more generally, any frozen volatile.
Ice or ICE may also refer to:


== Computing ==
In Case of Emergency, Emergency numbers stored on a mobile or cellular phone.
Digital ICE, Image Correction and Enhancement, for removing surface defects from a scanned photo/image
ICE (cipher), a block cipher in cryptography
IceWM, The Ice Window Manager
In-circuit emulator, a computer debugging hardware device
Information and Content Exchange, an XML protocol for content syndication
Integrated collaboration environment
Interactive Connectivity Establishment, a mechanism for NAT traversal
Interactive Customer Evaluation, form technologies for collecting software user feedback
Internal Compiler Error, a type of compilation error
Internal consistency evaluators, a set of tools for validating Windows Installer packages
Internet Communications Engine, a computer software middleware platform developed by ZeroC
Microsoft Research Image Composite Editor, a panorama stitching program
Inter-Client Exchange, an X Window System protocol framework
Interactive Creative Environment, a visual programming platform for Autodesk Softimage


== Media ==


=== Books ===
Ice (comics), a DC comic book superheroine
Ice (Dukaj novel), by Jacek Dukaj
Ice (Durst novel), a 2009 novel by Sarah Beth Durst, a modernization of the fairy tale "East of the Sun and West of the Moon"
Ice (Johnson novel), by Shane Johnson
Ice (Sorokin novel), by Vladimir Sorokin
Ice (webcomic)
Intrusion Countermeasures Electronics (ICE), a phrase used in cyberpunk literature
Ice, a 1978 novel by James Follett
Ice, a 1983 novel by Ed McBain in the 87th Precinct series


=== Music ===
Ice Records, a record label
Ice (band), a British band who recorded in the 1990s
Ice, a France-based American funk band who also recorded as Lafayette Afro Rock Band and as Krispie and Company
International Contemporary Ensemble
"Ice" (Lights song) by Lights from the album The Listening
"Ice" (Kelly Rowland song), a 2012 single by Kelly Rowland
"Ice", a song by Camel from I Can See Your House from Here
“Ice”, a song by Sarah McLachlan from Fumbling Towards Ecstasy
“Ice”, a song by The Rasmus from Playboys


=== Television and film ===
"Ice" (The X-Files), an episode of the television series The X-Files
"Ice", an episode of the television series Alias
"I.C.E", an episode of the television series Sirens
ICE (anime), a 2007 original video animation
Ice (2003 film), a 2003 Tamil film directed by Raghuraj
Ice (1998 film), a made-for-TV disaster movie
Ice (2011 miniseries), a British miniseries


=== Sports ===
"Ice", nickname of American basketball player George Gervin


== Organizations ==
iCE Advertisements or Insane Creators Enterprise, a digital art group
ICE - International Currency Exchange, a UK-based currency exchange provider
Independent Commission of Experts, former name of the investigation into assets moved to Switzerland around the Second World War
Independent Crown entities, part of New Zealand's State sector
Information Council on the Environment, a public relations organization in the USA
Institution of Civil Engineers, a British professional association
Iron Crown Enterprises, game-producing company from Virginia, USA
Institute for Credentialing Excellence, an institute for accreditation
Institute of Culinary Education, in New York City, New York, USA
Instituto Costarricense de Electricidad, the Costa Rican Institute of Electricity
IntercontinentalExchange, an Atlanta-based market for futures in energy and commodities and for other derivatives
The Ice Organisation, a UK sustainable rewards programme, also known as MyIce
U.S. Immigration and Customs Enforcement, a U.S. government agency


== Science, technology and medicine ==
Ice-ice, a disease condition of seaweed
Caspase 1, or Interleukin-1 beta Converting Enzyme
ICE (chemotherapy), a treatment for some kinds of cancer
ICE table, Initial, Change, Equilibrium, for tracking chemical reactions
Internal combustion engine, a fuel engine
International Cometary Explorer, a spacecraft for studying interaction between the Earth's magnetic field and solar wind
4-Methylaminorex, nicknamed "ice" in North America
Methamphetamine, nicknamed "ice" in Australia, Hawaii
Interference cancellation equipment in radio equipment such as cellular repeaters
In astronomy, any volatile chemical, such as water, ammonia, or carbon dioxide, not necessarily in solid form.


== Transportation ==
In car entertainment, hardware installed into cars, to provide audio/visual entertainment
InterCity Express (Citytrain), Australian interurban train fleet in Brisbane
Intercity-Express, German high-speed train
Iowa, Chicago and Eastern Railroad, USA


== Other uses ==
Ice, Kentucky
Inventory of Conflict and Environment, a research project at American University's School of International Service
International Corpus of English, a set of text corpora representing varieties of English around the world
In case of emergency, a programme encouraging people to list emergency contacts in their phone under "ICE"


== See also ==
Ices (disambiguation)