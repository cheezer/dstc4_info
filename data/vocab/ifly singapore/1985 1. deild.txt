Statistics of 1. deild in the 1985 season.


== Overview ==
It was contested by 8 teams, and B68 Toftir won the championship.


== League standings ==


== Results ==
The schedule consisted of a total of 14 games. Each team played two games against every opponent in no particular order. One of the games was at home and one was away.
Source: www.faroesoccer.com
1 ^ The home team is listed in the left-hand column.Colours: Blue = home team win; Yellow = draw; Red = away team win.


== Top goalscorers ==


== References ==