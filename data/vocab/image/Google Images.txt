Google Images is a search service owned by Google and introduced in July 2001, that allows users to search the Web for image content. The keywords for the image search are based on the filename of the image, the link text pointing to the image, and text adjacent to the image. When searching for an image, a thumbnail of each matching image is displayed. When the user clicks on a thumbnail, the image is displayed in a box over the website that it came from. The user can then close the box and browse the website, or view the full-sized image.


== Expansion and user interface (2001-2011) ==
The Google President, Eric Schmidt, stated that Google Images search was created because of the desire to view Jennifer Lopez in her exotic green Versace dress. In 2000, Google Search results were limited to simple pages of text with links, but the developers worked on developing this further, realising that an image search was required to answer "the most popular search query" they had seen to date: Jennifer Lopez's green dress. As a result of this, Google Image Search was born.
In 2001, 250 million images were indexed. In 2005, this grew to 1 billion. By 2010, the index reached 10 billion images. As of July 2010, the service receives over one billion views a day. Google introduced a sort by subject feature for a visual category scheme overview of a search query in May 2011.
In early 2007 Google implemented an updated user interface for the image search, where information about the image, such as resolution and URL, was hidden until the user moved the mouse over the thumbnail. This was discontinued after a few weeks.
On October 27, 2009, Google Images added a feature to its image search that can be used to find similar images.
On July 20, 2010, Google updated the user interface again, hiding image details until mouse over, like before. This feature can be disabled by pressing "Ctrl + End" on one's keyboard and clicking "Switch to basic version".
In June 2011, Google Images began to allow for reverse image searches directly in the image search-bar (that is, without a third-party add on, such as the one previously available for Mozilla Firefox). This feature allows users to search by dragging and dropping an image into the search-bar, uploading an image, selecting a URL, or 'right-clicking' on an image.


== Search by image ==

Google Images has a Search by Image feature for performing reverse image searches. Unlike traditional image retrieval, this feature removes the need to type in keywords and terms into the Google search box. Instead, users search by submitting an image as their query. Results may include similar images, web results, pages with the image, and different resolutions of the image. For example, if you search using a picture of a beach, you might get results including similar beaches, information about the same beach, and websites that use the same beach picture.
The precision of Search by Image's results is higher if the search image is more popular. Additionally, Google Search by Image will offer a "best guess for this image" based on the descriptive metadata of the results.
You can access Search by Image in four ways:
Upload an image Click the camera icon in the Google search box. Select "Upload an image" and choose an image from your computer to upload.
Drag and drop Click and drag an image from your computer into the Google search box. Not all browsers support this feature; use Chrome or Firefox 4+.
Search using image URL Click the camera icon in the Google search box. Copy your image URL into the box labeled "Paste image URL".
Right-click an image In Chrome or Firefox, right-click an image and select "Search Google with this image". Firefox requires users to first download Google's Search by Image add-on.


== Search by image algorithm ==
The general steps that Search by Image takes to get from your submitted image to returning your search results are as follows:
Analyze image The submitted image is analyzed to find identifiers such as colors, points, lines, and textures.
Generate query Those distinct features of the image are used to generate a search query.
Match image The query is matched against billions of images in Google's back end.
Return results Google's search and match algorithms return matching and visually similar images as results to you.


== New algorithm and accusations of censorship (2012-present) ==
On December 11, 2012, Google Images' search engine algorithm was changed once again, in the hopes of preventing pornographic images from appearing when non-pornographic search terms are used. According to Google, pornographic images would still appear as long as the term searched for was specifically pornographic. While explicitly stating that they were "not censoring any adult content", it was immediately noted that even when entering terms such as "blow job", "boob," or even the word "pornography" itself, no explicit results were shown. The only alternative option is to turn on an even stricter filter which will refuse to search for the aforementioned terms whatsoever. It has also been noted that users can no longer exclude keywords from their search as before.


== See also ==
TinEye
Picsearch
Yahoo Image Search
Bing Images
Image search


== References ==


== External links ==
Google Image Search website
Google Image Labeler Beta
The Official Google Blog