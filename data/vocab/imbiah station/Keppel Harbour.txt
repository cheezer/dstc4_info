Keppel Harbour (Chinese: 吉宝港口; pinyin: Jíbǎo Gǎngkǒu) is a stretch of water in Singapore between the mainland and the southern islands of Pulau Brani and Sentosa (formerly Pulau Blakang Mati) . Its naturally sheltered and deep waters was to meet the requirements of British colonists attempting to establish a Far East maritime colony in that part of the world, and thereby setting the stage for the eventual formation of Singapore as a successful independent state.


== Etymology and history ==

The harbour was first noticed in August 1819 by William Farquhar, who reported his discovery of a "new harbour" inhabited by orang laut (sea Gypsies) living in boats to Sir Stamford Raffles the following month.
In the 1830s, the Straits Settlements, consisting of Singapore, Malacca and Penang, was a pirates' haven. By 1832, Singapore had become the busy centre of government for the three areas.
It was also at this time that Captain Henry Keppel came to Singapore and helped to clear the Straits of pirates. Whilst based at Singapore, he discovered the deep water anchorage that came to be called by his name. Keppel first sailed to Singapore as a midshipman in 1832 and took part in the Naning (Malacca) expedition, and came again later in 1842 to help with the suppression of piracy in the Malay Archipelago. Keppel had a long association with Singapore, having visited the island on several occasions up to 1903. He surveyed the new harbour of Singapore, which was formed based on his plans. The harbour was completed in 1886.
In 1855, Captain William Cloughton, William Paterson and William Wemyss Ker purchased Pantai Chermin from the Temenggong of Johor. In 1859, Cloughton built the first dry dock known as Number 1 Dock. In 1868, the second dock, Victoria Dock, was inaugurated by Sir Harry George Ord, Governor of the Straits Settlements. The Albert Dock was opened in 1879.

For a while, the harbour was simply known as New Harbour but it was renamed Keppel Harbour by the Acting Governor, Sir Alexander Swettenham, on 19 April 1900 when Admiral Keppel visited Singapore at the age of 92.
New Harbour Road was also renamed Keppel Road. This reportedly pleased Admiral Keppel very much. The Chinese names for Keppel Road and Keppel Harbour were sin kam kong chu u or "Kampong Bahru dock", and sek lat moi or "selat passage" (selat is Malay for straits).


== Plans ==
Singapore's largest conglomerate, the Keppel group, has announced plans to build exclusive villas on the 5.3 hectare Keppel Island that it owns in Keppel Bay at Keppel Harbour — home to a shipyard until 2000.
The group will also launch a condominium designed by renowned architect Daniel Libeskind, who is designing the masterplan for New York's ground zero site. The 1,200-unit waterfront condominium — known as Keppel Bay phase two — will be launched in early 2007. It will sit on about 84,000 square metres of land on the mainland opposite Keppel Island, with a shoreline of 750 metres. It will have six high-rise blocks and some spacious low-rise apartments.
The condominium will be part of the 4,860,000 square feet (452,000 m2) Keppel Bay mega development, which is 70 per cent owned by Keppel Corp and 30 per cent by its unit Keppel Land. About 2,800 homes are set to be built, including the existing 969-unit Caribbean at Keppel Bay.
The most exclusive homes of the lot are likely to be reserved for Keppel Island, where the Marina@Keppel Bay, a separate development, will be completed by late 2007. The marina will have high-end restaurants and tentative plans are for high-end villas and possible condominium units on the island.
A 250 metre Keppel Bay Bridge, a cable-stayed bridge links Keppel Island to the mainland.
Keppel also has two other smaller plots for condominium on the other side of the Caribbean. Corals condominium is located beside Caribbean, just beside King's Dock (Second largest dock in the world since 20 Aug 1923) with 366 units. One is 3.4 ha in size while the other, a joint venture with Mapletree, is 2.9 ha.


== References ==
Victor R Savage, Brenda S A Yeoh (2004), Toponymics - A Study of Singapore Street Names, Eastern University Press, ISBN 981-210-364-3
^ Joyce Teo, "Keppel to build more waterfront homes", The Straits Times, 22 November 2006