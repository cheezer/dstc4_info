Madame Tussauds is a wax museum in Singapore. It officially opened in 2014, with Katy Perry taking part in the opening festivities. It is located at Imbiah Lookout in Sentosa. 


== Wax figures ==

The wax museum has many wax figures of notable political icons, famous superstars, sports icons and others.Those includes Yusof Ishak, Soekarno, Lee Kuan Yew, David Beckham, Sharukh Khan, Angelina Jolie, Johnny Depp and many more. It recently added a figure of the Singapore Girl, its second one of Mohamed Ambiah, an iconic fight attendant for Singapore Airlines.


=== Shows ===
At Madame Tussauds in Singapore, there are two main attractions:
Show – Images of Singapore
Boat ride – Spirit of Singapore


== Location ==
Madame Tussauds Singapore is located at Imbiah Lookout , Sentosa Island, Singapore.


== Entrance ==
Standing at the entrance is a statue wax figure of Lady Gaga.


== Ticketing ==
Ticketing are cost at $28SGD for children age 3-12 years, $30SGD for senior citizens, age 60 and above, while an adult ticket cost $38SGD.


== References ==