The imm Cologne (internationale möbelmesse) is an international, public open furniture trade show held at koelnmesse exhibition centre in Cologne, Germany, every year in January.
The exhibition's primary focus is contemporary furniture and interior design, but also showcases innovative materials and fabrics and latest architectural lighting design technology. Along the Salone del Mobile at Milan, the imm Cologne is regarded as a leading market place for related industries, i.e. furniture designers, furniture companies, furniture retailers, architects and interior designers. The 2010 show had some 1,500 exhibitors and attracted over 100,000 visitors.
The Cologne Furniture Fair is organised by the Verband der Deutschen Möbelindustrie e.V. and was first held in 1949. Since the late 90s, the City of Cologne stages several events in conjunction with imm, namely the Cologne Design Week and Interior Design Week. Other groups include Design Champs and the international D3 Design Talents, with workshops for both young design professionals and design students.


== See also ==
Art Cologne
Interior Design Education
Light+building


== Gallery ==


== References ==
^ "Facts and Figures 2010" (PDF). imm-cologne.com. Retrieved 16 September 2010. 


== External links ==
Official site (German) (English)
imm Interior innovation award (German) (English)
Verband der Deutschen Möbelindustrie e. V. (German)