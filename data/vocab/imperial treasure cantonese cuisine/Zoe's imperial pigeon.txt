The Zoe's imperial pigeon (Ducula zoeae), also known as the banded imperial pigeon, is a species of bird in the Columbidae family. It is found in Indonesia and Papua New Guinea. Its natural habitats are subtropical or tropical moist lowland forests, subtropical or tropical mangrove forests, and subtropical or tropical moist montane forests.


== References ==