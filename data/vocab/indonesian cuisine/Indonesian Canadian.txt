An Indonesian Canadian is a Canadian citizen of Indonesian descent or an Indonesia-born person who resides in Canada. They are one of the smaller Asian minorities in Canada.


== Demography ==
In the early post-World War II period, most migrants from Indonesia to Canada were Indo people of mixed Dutch and pribumi ancestry. Many did not come directly from Indonesia, but rather went to the Netherlands and then re-migrated due to racial prejudice they faced there. Community members believe that perhaps 3,000 live in the Ontario area. These migrants tend to be fluent in Dutch, and may also speak Indonesian, or more commonly Indonesian-based trade languages. However, Indonesians of Chinese descent formed the main group in the stream of migration which began in the late 1960s and early 1970s. They have come to comprise an estimated 80% of Canada's population of Indonesian background. Most do not speak any variety of Chinese.
7,610 respondents to the 1991 census stated their place of birth as "Indonesia". Around half of those were settled in the Greater Toronto Area. Data from the 2006 Census suggested that 14,320 people of Indonesian ethnic origin reside in Canada (3,225 single responses, 11,095 in combination with other responses), primarily in Ontario (6,325, or 44%), British Columbia (4,640, or 32%), and Alberta (1,920, or 13%).


== Religion ==
More than half of Indonesian Canadians are believed to be Christians, with roughly equal numbers of Catholics and Protestants. In contrast, although Islam is Indonesia's majority religion, Muslims are estimated to make up only about 10% of Indonesian Canadians. An Indonesian Catholic congregation, the Ummat Katholik Indonesia, has been meeting in the Toronto area since 1979; it was first headed by a Dutch priest who had previously lived in Indonesia, and later by a Javanese theology student from the University of Toronto. An inter-demoninational Protestant fellowship, the Indonesian Christian Fellowship, also emerged in Toronto in the 1980s.


== Community organisations ==
The first Indonesian community organisations in Canada, the Indonesian-Canadian Association and the Canadian-Indonesian Society, were founded in 1969 in Toronto and Vancouver, respectively. Indonesian consuls' wives also set up branches of Dharma Wanita, a women's group, in various cities. A credit union, Indoka (Indonesian Credit Union) was established in the early 1970s. A group of Muslim and Christian women formed Sanggar Budaya (Culture Workshop), a dance and music group, a few years later. There is also an Indonesian Catholic Organisation comprising mainly ethnic Chinese, as well as INCASEC (Indonesian Canadian Senior Citizens).


== Notable people ==
I Am Robot and Proud, electronic musician from Toronto
Diyan Achjadi (artist and professor).


== Footnotes ==


== Bibliography ==
Nagata, Judith (1988), "Religion, Ethnicity, and Language: Indonesian Chinese Immigrants in Toronto", Southeast Asian Journal of Social Science 16 (1): 116–131, doi:10.1163/080382488X00072 
Nagata, Judith (1999), "Indonesians", in Magocsi, Paul R., Encyclopedia of Canada's Peoples, University of Toronto Press, pp. 722–726, ISBN 0-8020-2938-8 
Schryer, Frans J. (1998), "Dutch Indonesians in Ontario", The Netherlandic presence in Ontario: pillars, class and Dutch ethnicity, Wilfrid Laurier University Press, pp. 154–162, ISBN 978-0-88920-262-7 
"Ethnic origins, 2006 counts, for Canada, provinces and territories - 20% sample data", Ethnocultural Portrait of Canada, Census 2006, Statistics Canada, retrieved 2010-01-27 


== External links ==
Indonesian Canadian Community Association (ICCA)
United Indonesian Canadian Society