Afrodisiac is a funk/soul album by The Main Ingredient. Released in 1973 by RCA Victor, the album features several songs written or co-written by Stevie Wonder.


== Track list ==
"Superwoman" - (Stevie Wonder) 3:15
"Where Were You When I Needed You" - (Stevie Wonder) 4:09
"I Am Yours" - (Stevie Wonder, Syreeta Wright) 4:08
"Work To Do" - (The Isley Brothers) 3:18
"Girl Blue" - (Stevie Wonder, Syreeta Wright) 4:21
"You Can Call Me Rover" -(J.R. Bailey, Ken Williams, Mel Kent) 3:26
"Something 'Bout Love" - (Stevie Wonder) 4:22
"Love Of My Life" - (George Clinton) 3:30
"Something Lovely" -(Stevie Wonder, Syreeta Wright) 3:12
"Goodbye My Love" -(Luther Maxwell) 4:50


== Charts ==


=== Singles ===


== External links ==
The Main Ingredient-Afrodisiac at Discogs


== References ==