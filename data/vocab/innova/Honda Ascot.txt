The Honda Ascot is a compact sedan manufactured by Honda and marketed only in Japan from 1989 to 1997. The first generation produced two versions based on the Honda Accord CB series called the Ascot and from 1993 to 1996 a "pillared hardtop" called the Ascot Innova. The Innova shared much of its mechanicals with the European-market Accord manufactured at the Honda UK facility in Swindon, England, and was essentially the badge engineered Rover 600. The second generation was a platform improvement, shared with the Japan-only sedan called the Honda Rafaga. The "Ascot" name was chosen with reference to the Ascot Racecourse and Ascot tie, in order to add the model an alleged air of class and elegance.


== First generation (CB1/2/3/4) ==
The first Honda Ascot was borne out Honda's strategy to diversify its sales channels in Japan. In 1985, two separate dealer networks were established, under the names of Honda Clio and Honda Primo, in addition to the already existing Honda Verno network. While the Primo stores handled kei cars, as well as the Honda Civic, the Clio stores focused on larger models, including the top-of-the-line Honda Legend. With the arrival of the fourth-generation Honda Accord (CB) its sales were assigned solely to Honda Clio.
That meant, however, that the Honda Primo network needed an Accord-based compact sedan for the dealership channel. Honda adopted a rather simple solution of creating a "sister car" to Accord, the Honda Ascot. Technically, the Ascot was identical to the Accord sedan, and the interior, including the dashboard, was shared with the Accord. The exterior sheetmetal was unique to the Ascot, with cosmetic differences provided for a different look, befitting the Ascot's upmarket role as the top-of-the-line model of Honda Primo, which sold small, entry-level products. The headlight units were borrowed from the Honda Inspire, adding to its luxurious appearance. The top level FTB-i included four-wheel anti-lock disc brakes, four-wheel steering, and four-wheel double wishbone suspension, with the 2.0 Liter PGM-FI fuel injection engine. The fuel economy for the top level engine was rated at 10.4 km/L (29 mpg-imp; 24 mpg-US) according to figures quoted in sales materials. Here's a Honda promotional video introducing the Ascot FTB-i.
The Ascot had a six-light greenhouse compared to the regular Accord's four-light layout, and featured a different front end with a more formal grille, as well as a revised rear end with an Ascot specific rear tail light treatment, installing the rear license plate indented into the rear bumper. The styling approach was also duplicated on the smaller Honda Concerto, a platform shared with the Honda Civic of the same time period.
The Ascot was launched on September 13, 1989, and the television commercials initially featured the "Take the A-Train" jazz standard A-train commercial. Later on, Honda decided to change the marketing image of the car, employing Eric Clapton to do the commercials. In August 1991, a rehashed Ascot was presented, allegedly "refined to better suit the Japanese taste". In March 1992, Honda presented an all-new Honda Ascot Innova (see below), while the regular Ascot was replaced by an all-new model in 1993.


== Ascot Innova (CB3/4) ==
Launched on March 5, 1992, the (Japanese: Ascot Innova) was also based on the CB Accord underpinnings, but was given an all-new, modern-looking and rounded body, with styling similar to the Honda Prelude BB4 series (1992–1996). It was the result of a joint effort with the Rover Group that provided Rover with the 600 series, and an Accord that was unique to Europe. The Innova retained the original Ascot's six-light greenhouse layout and horizontal taillights, as well as long, sleek and low body proportions (as opposed to the second-generation Ascot's upright stance and more Accord CF-like rear end). The Innova was fitted with frameless side glazing to provide for the "pillared hardtop" look, thus being Honda's answer to cars like the Toyota Carina ED/Toyota Corona EXiV, Nissan Bluebird ARX and Mitsubishi Emeraude. The Ascot Innova was sold in conjunction to the Ascot, and did not replace it.
The Ascot Innova was available in three four-cylinder engine choices: the 2.0-litre F20A unit, producing 135 PS in the less expensive 2.0iC and 2.0i versions with SOHC, 150  in the 2.0Si trim with DOHC, and the H-series 2.3-litre H23A engine fitted in the export versions of the Prelude, producing 165 HP (the 2.3-litre versions were designated 2.3Si-Z). Interestingly, while the 2.0-litre versions maintained the 1695 mm width which allowed them to remain in the favorable tax class, the 2.3-litre Innovas were 1710 mm wide, as the engine displacement didn't allow them to remain in the lower tax band, and buyers in Japan were also liable for a higher annual road tax bill also.
A 4-wheel steering system like the one in the Prelude was made available for the 2.0i, 2.0Si and 2.3Si-Z versions. Apart from the cheapest 2.0iC version, which came with a 5-speed manual transmission, all Ascot Innovas came with a 4-speed automatic. The Ascot Innova range started at ¥1,558,000 for the 2.0iC in the Tokyo sales area, while the most expensive 2.3Si-Z fetched ¥2,992,000 in Sapporo area, not including extra charges for options such as 4-wheel steering, moonroof, passenger airbag, cornering lamps or ABS. The television advertising campaign was built around the slogan "Hardtop Innovation" and featured the American actress Geena Davis.
The Innova itself remained a JDM-only model, and a Honda Primo exclusive, with no JDM sister cars. It was the result of shared efforts with British partner Rover Group, and the car was sold as the European Accord, which was made in Swindon, UK and marketed in Europe as Honda Accord instead of the North American version. The European Accord and the Ascot Innova differ only slightly, with the Euro-Accord being wider and featuring framed windows. Normally, this type of vehicle with a sports car influence would have been sold at Honda Verno, however because of its strong similarity to the Prelude, and Honda Verno already had a luxury sports sedan called the Honda Vigor, the Ascott Innova was assigned to Honda Primo instead, as Honda Clio had the Honda Legend.
The European Accord was in turn the base for the Rover 600 saloon, developed under Honda's long-standing relationship with the British Rover Group. The Rover 600 and the Swindon Accord also shared two engine options not available for the Ascot Innova - Honda's F18A 1.8-litre unit and Rover's 2.0-litre L-series turbodiesel. The Ascot Innova remained in production until 1996, while the European Accord saloon continued until 1998, with a facelifted model in 1997. In 1998, it was replaced with an all-new model.


=== Trim Levels ===
2,000cc SOHC F20A engine (135 PS)
2.0i･C
2.0i
2.0i･4WS

2,000cc DOHC F20A engine (150 PS)
2.0Si
2.0Si･4WS

2,300cc DOHC H23A engine (165 PS)
2.3Si-Z
2.3Si-Z･4WS
2.3Si-Z･TCV
2.3Si-Z･TCV･4WS


== Second generation (CE) ==
The next-generation (CD) Accord grew in size considerably, mostly to satisfy North American market efforts, and thus became too wide to fit within the favorable Japanese tax class. Therefore, Honda needed a slightly smaller compact sedan to cater to the JDM market, and this duty was assigned to the new Ascot
Japanese TV commercial for the Honda Ascot.
Rather than being directly based on the now larger Accord, the new Ascot was given its own platform, with the codenames CE4/5, which was a platform improvement of the previous generation. Like the Inspire, Vigor and the even larger Honda Legend, the CE Ascot had its engines mounted longitudinally, contrary to the Accord and most other FWD cars, employing the transverse setup, thereby placing the weight of the engine aligned and behind the front axle and improving vehicle weight distribution between the front and rear axles. In Japan, the smaller G20A engine used regular grade fuel, while the larger G25A engine used premium grade fuel.
The new Ascot had a sloping appearance resembling a coupé, rendering the vehicle shorter and taller than both the CD Accord, Ascot Innova and the Inspire, thus continuing the first Ascot's role as a premium entry-level formal sedan. It was fitted with the 5-cylinder inline Honda G engine, also employed by the Inspire and Vigor, in two displacement versions - 2.0-litre and 2.5-litre, with the gearbox slotted behind it. The wheelbase of the Ascot was shorter than the slightly longer Inspire and Vigor, which measured at 2,805 mm (110.4 in), a difference of 35 mm (1.4 in), which didn't leave much room for rear seat passengers. Furthermore, due to the lengthwise installation of the five-cylinder engine, and the requirement that the overall length of the car comply with Japanese government regulations concerning cars classified as "compact", compromises were made with regards to rear passenger accommodations, and sales suffered as a result. The Ascot also spawned a sister car, the Honda Rafaga, which was sold at Honda Verno stores, which differed from the Ascot only by different external trim details. Both models were replaced by the CF series Accord platform twin Honda Torneo in 1997.


== References ==
^ http://www.honda.co.jp/news/1992/4920303.html Honda Ascot Innova (Japanese)
^ http://www.honda.co.jp/factbook/auto/ASCOT/19931014/as293-019.html


== External links ==
Honda Ascot timeline at Honda's homepage (Japanese)
1992 Honda Ascot Innova press release (Japanese)
Summary of Honda Ascot/Ascot Innova TV ad campaigns, with screen captures (Japanese)