Intercontinental Cup may refer to:
Intercontinental Cup (football), a competition organised jointly by UEFA and CONMEBOL
Beach Soccer Intercontinental Cup
Intercontinental Cup (baseball), a competition sanctioned by the International Baseball Federation
FIRS Intercontinental Cup, a roller hockey competition between the winners of the CERH European League and the CSP South American Club Championship
FIBA Intercontinental Cup, a basketball competition between each of the European Cup winners from each of the European nations cup competition winners
ICC Intercontinental Cup, a first-class cricket competition run by the International Cricket Council for 12 of its associate members.
Intercontinental Le Mans Cup, a sports car racing competition organised by the Automobile Club de l'Ouest