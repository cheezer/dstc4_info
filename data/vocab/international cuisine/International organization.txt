An international organization is an organization with an international membership, scope, or presence. There are two main types:
International nongovernmental organizations (INGOs): non-governmental organizations (NGOs) that operate internationally. These include international non-profit organizations and worldwide companies such as the World Organization of the Scout Movement, International Committee of the Red Cross and Médecins Sans Frontières.
Intergovernmental organizations, also known as international governmental organizations (IGOs): the type of organization most closely associated with the term 'international organization', these are organizations that are made up primarily of sovereign states (referred to as member states). Notable examples include the United Nations (UN), Organisation for Economic Co-operation and Development (OECD), Organization for Security and Co-operation in Europe (OSCE), Council of Europe (CoE), and World Trade Organization (WTO). The UN has used the term "intergovernmental organization" instead of "international organization" for clarity.
The first and oldest intergovernmental organization is the Central Commission for Navigation on the Rhine, created in 1815 by the Congress of Vienna.
The role of international organizations are helping to set the international agenda, mediating political bargaining, providing place for political initiatives and acting as catalysts for coalition- formation. International organizations also define the salient issues and decide which issues can be grouped together, thus help governmental priority determination or other governmental arrangements.


== See also ==
List of intergovernmental organizations
List of regional organizations by population
List of trade blocs
Regional Economic Communities
Regional integration
Regional organization
Supranational union


== Notes and references ==


== External links ==
Headquarters of International Organisation List of International Organisation and their Headquarters
Procedural history and related documents on the Articles on the Responsibility of International Organizations in the Historic Archives of the United Nations Audiovisual Library of International Law
World News related documents on the World News related documents