In physics and chemistry, an ion is an atom or group of atoms with a net electric charge.
Ion may also refer to:


== Ancient Greece ==
Ion (dialogue), a dialogue by Plato, between Socrates and Ion, a reciter of epic poems
Ion (mythology), the son of Xuthus and Creüsa, daughter of Erechtheus
Ion (play), a play by Euripides on the relationship between humans and the gods, in which Ion is instead the son of Apollo
Ion of Chios, a writer and philosopher


== Arts and entertainment ==
Ion (DC Comics), a creature in the DC Comics universe
Ion (Marvel Comics), a super villain in the Marvel Comics universe
I.O.N, a Japanese science fiction manga by Arina Tanemura
Ion Television, a television network
Fon Master Ion, a character from the video game Tales of the Abyss


== Brands ==
ION Audio, a brand of audio equipment manufacturer Numark Industries
Ion (chocolate), a Greek chocolate brand
Alesis Ion, an analog-modeling synthesizer keyboard made by Alesis
Ion, a defunct communication network formerly advertised by Sprint Nextel
Ion (paintball marker), made by Smart Parts
Saturn Ion, an automobile formerly sold by General Motors
Mitsubishi i-MiEV, sold in Europe as the Peugeot iOn
Nvidia Ion, an Nvidia platform for the Intel Atom CPU
Ion, a lighting control console by Electronic Theatre Controls
Ion rapid transit, a proposed system in Waterloo Region, Ontario, Canada
Ion at Home, A sub-brand from Sally Beauty


== Places ==
Ion, a hamlet near to Lower Gravenhurst, Bedfordshire, England
Ion River, Romania
Ion, Iowa, an unincorporated community in the United States
I'On, Mount Pleasant, South Carolina, a neighborhood


== Institutions ==
Instituto Oncologico Nacional, Panama City
Institute of Navigation


== Other ==
Ion (name), both a given name and a surname
Ion (window manager), in computing a window manager for the X Window System
IATA code for Impfondo Airport, Republic of the Congo
ION (satellite) (Illinois Observing Nanosatellite), a satellite from the University of Illinois


== See also ==
Ionic (disambiguation)