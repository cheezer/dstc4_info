Westgate (Chinese: 西城) is a lifestyle and family shopping mall in Jurong East, Singapore. It is the only mall with direct connections to both Jurong East MRT station and Jurong East Bus Interchange.
Located at the heart of Jurong Gateway, the integrated retail and office development compromises of a 7-level lifestyle and family shopping mall and a 20-level office tower known as Westgate Tower.
Isetan Supermarket together with wine shop called Bacchus is located at Basement 2, the lowest level in Westgate. There will also be Westgate Wonderland on Level 4, a gym with swimming pool and a childcare centre with Kids Club at Level 5.


== Anchor tenants ==
Courts
Food Republic
Isetan
Royce'
Samsung Concept Store
Suit Select
Sushi Express
KOI Café
Tim Ho Wan
Fitness First
My First Skool


== References ==


== External links ==
Official website