The Palladium-Item is the daily morning newspaper for Richmond, Indiana and surrounding areas. The paper is a merger of two older papers, the Richmond Palladium and the Richmond Item and traces its history back to 1831, making it the oldest continuous businesses in Richmond. Notable writers from the paper's staff include Mike Lopresti who is now a sports columnist for the Gannett News Service and is published in many of their papers.


== External links ==

Palladium-Item website
Official mobile website
Website for owner Gannett