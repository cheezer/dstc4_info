European route E 57 is an intermediate E-road connecting Sattledt - Liezen - St. Michael - Graz in Austria and further Maribor - Ljubljana in Slovenia
In Austria, it follows the A9 motorway, also called Pyhrnautobahn. In Slovenia it follows the A1 motorway.
It includes the second longest tunnel in Austria, the Plabutschtunnel, 10.0 km (6.2 mi) long. It also includes the Gleinalm tunnel, 8.3 km (5.2 mi) long, and the Bosruck tunnel, 5.5 km (3.4 mi) long.


== Itinerary ==
The E 57 routes through four European countries:


=== Austria ===


=== Slovenia ===