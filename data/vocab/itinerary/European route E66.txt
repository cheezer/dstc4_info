European route E 66 is a part of the inter-European road system. This Class A intermediate west-east route runs 651 kilometres (405 mi) from Franzensfeste in Italy to Székesfehérvár in Hungary, connecting the Alps with the Pannonian Plain.


== Itinerary ==
The E 66 routes through three European countries:
Italy
Strada Statale 49 della Pusteria: Franzensfeste (with connection to European route E45) - Innichen - Winnebach

Austria
Drautal Straße (B100): Arnbach - Silian - Lienz - Oberdrauburg - Spittal an der Drau
Tauern Autobahn (A10): Spittal an der Drau - Villach
Süd Autobahn (A2): Villach - Klagenfurt - Graz - Fürstenfeld
Gleisdorfer Straße (B65): Fürstenfeld - Heiligenkreuz im Lafnitztal

Hungary

8-as főút (planned M8): Rábafüzes/Szentgotthárd - Körmend - Veszprém - Székesfehérvár.

Hungary requested in October 2011 that E66 should be extended from Székesfehérvár via Dunaújváros - Kecskemét to Szolnok. The expectation is that this will be valid and signposted in 2013 or 2014.[1]