Kolak (or kolek) is an Indonesian dessert based on palm sugar or coconut sugar, coconut milk, and pandanus leaf (P. amaryllifolius). A variation in which banana is added, is called kolak pisang or banana kolak. Other variations may add ingredients such as pumpkins, sweet potatoes, jackfruit, plantains, cassava, and tapioca pearls. It is usually served warm or at room temperature.
In Indonesia, kolak is a popular iftar dish during the holy month of Ramadan.


== Gallery ==


== External links ==
Kolak - Banana, Sweet Potatoes, and Tapioca in Coconut Milk - description and recipe.
Kolak - Banana Compote in Coconut Milk - Recipe and step-by-step photos