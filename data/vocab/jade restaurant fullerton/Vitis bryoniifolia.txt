Vitis bryoniifolia is a prolific and adaptable, polygamo-dioecious species of climbing vine in the grape family native to China, where it is known as ying yu, or hua bei pu tao (North China grape). The variant form ternata is known as san chu ying yu, meaning three-foliolate, or -leaflet ying yu. Ying yu translates to mean "hard jade".
V. bryoniifolia is found in a wide variety of tree-established habitats including forests and shrublands, or fields and valleys where trees are present, especially along the banks of streams; and can be found both in highlands and low- (100–2500 meters above sea-level). It has a long growing season, flowering from April to August, and bearing its fruit (rosy, plum-colored berries, 5–8 mm. in diameter) from June to October; and also a broad distributional range, being reported from 15 of China's 27 provinces and autonomous regions (Anhui, Fujian, Guangdong, Guangxi, Hebei, Hubei, Hunan, Jiangsu, Jiangxi, Shaanxi, Shandong, Shanxi, Sichuan, Yunnan and Zhejiang).


== References ==


== External links ==
Largish JPGs of Vitis bryoniifolia:
Plant crawling horizontally along rocks (1354.6 kb)
Erect stem with leaves and flower buds (1264.2 kb)
Erect stem with leaves and flower buds (922.6 kb)
Plant crawling along the forest floor between rocks (2125.0 kb)