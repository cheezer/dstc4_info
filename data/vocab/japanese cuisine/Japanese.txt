Japanese may refer to:
Something from or related to Japan, an island country in East Asia
Japanese language, spoken mainly in Japan
Japanese people, the ethnic group that identifies with Japan through culture or ancestry
Japanese diaspora, Japanese emigrants and their descendants around the world
Foreign-born Japanese, naturalized citizens of Japan

Japanese writing system, consisting of kanji and kana
Japanese cuisine, the food and food culture of Japan


== See also ==
List of Japanese people
All pages beginning with "Japanese"
Japonica (disambiguation)
Japonicum (disambiguation)
Japanese studies