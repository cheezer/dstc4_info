This is a list of films produced in Japan in year order ordered by decade on separate pages. For an A-Z of films see Category:Japanese films. Also see cinema of Japan.


== 1905-1919 ==
List of Japanese films: Pre 1920


== 1920s ==
Japanese films of the 1920s


== 1930s ==
Japanese films of the 1930s


== 1940s ==
Japanese films of the 1940s


== 1950s ==
Japanese films of the 1950s


== 1960s ==
Japanese films of the 1960s


== 1970s ==
Japanese films of the 1970s


== 1980s ==
Japanese films of the 1980s


== 1990s ==
Japanese films of the 1990s


== 2000s ==
Japanese films of the 2000s


== 2010s ==
Japanese films of the 2010s


== See also ==
Cinema of Japan


== External links ==
Top 5 Japanese Films on Listar
Japanese film at the Internet Movie Database