Singapore has sent athletes to most Summer Olympic Games held since 1948, when it was established as a separate British Crown Colony from the Straits Settlements just over three months before the commencement of the 1948 Summer Olympics. It continued to send a team to the Games until 1964 when Singapore was part of Malaysia, which sent a combined team. Upon Singapore's full independence from Malaysia in 1965, the country continued to participate in all subsequent editions of the Summer Games except in 1980 when the country participated in a large Olympic boycott. No athlete from Singapore has competed in any Winter Olympic Games. Singapore is making a bid to participate in the Winter Olympic Games in 2014 via construction of its first Olympic-sized ice skating rink in April 2012.
The country has won four Olympic medals, the first at the 1960 Summer Games, the second at the 2008 Summer Games and the third and fourth at the 2012 Summer Games.
The first medal was won by Tan Howe Liang, who won a silver medal in lightweight weightlifting in 1960.
In table tennis, Jing Jun Hong and Li Jiawei came close to winning medals by finishing in fourth place at the 2000 Sydney Olympics and 2004 Athens Olympics respectively. Singapore sent a big contingent to the Beijing Olympics, and many felt that with many 'home' players in Singapore's squad, this was their best chance of winning a medal since 1960 - they were proven right.
In the 2008 Beijing Olympics, Li Jiawei, together with Feng Tianwei and Wang Yuegu, beat the South Korea Women's Table Tennis team, composed of Dang Ye-Seo, Kim Kyung-Ah and Park Mi-Young 3-2 in the semi-finals, assuring Singapore of at least a silver medal and ending Singapore's 48-year Olympic medal drought. Singapore faced host China in the gold medal final.
In the 2012 London Olympics, Feng Tianwei beat Kasumi Ishikawa from Japan 4-0 in the Table Tennis Women's Singles Bronze Medal Match, winning Singapore's first individual Olympic medal in 52 years. In the Table Tennis Women's Team Bronze Medal Match, Li Jiawei, together with Feng Tianwei and Wang Yuegu, beat the South Korea team composing Dang Ye-Seo, Kim Kyung-Ah and Seok Ha-Jung 3-0, winning another bronze medal.
The two bronze medals won at the 2012 London Olympics marks the first time in Singapore's history that more than one medal were won consecutively in a single Olympic Games.
To date, athletes from Singapore have won a total of 4 medals at the Olympics. No athlete from Singapore has ever won a gold medal.


== List of medalists ==


=== Medals by sport ===


== References ==