Birmingham School of Jewellery, founded in 1890, is a jewellery school in Birmingham, England. Located on Vittoria Street in the city's Jewellery Quarter, it is the largest jewellery school in Europe. It is part of the Birmingham Institute of Art and Design (BIAD), a faculty of Birmingham City University.
Its portfolio of courses includes jewellery, silversmithing, horology, and gemmology. It is close to the Jewellery Industry Innovation Centre (JIIC), which offers professional expertise in industry-related techniques including Computer Aided Design (CAD), rapid prototyping and surface finishing.


== History ==
By the mid-19th century, the jewellery trade was considered the most lucrative in Birmingham with jewellers being some of the best paid workers in the city. There were also more people employed in the trade than any other in the city. Apprentices generally did not require any qualifications but style became a study within the industry and one jeweller's firm required all apprentices to attend the Birmingham School of Art.
The Birmingham School of Jewellery and Silversmithing was established in 1890 as a branch of the School of Art when Martin & Chamberlain converted a goldsmith's factory, built in 1865 to a design by J. G. Bland. The top storey was added in 1906 by Cossins, Peacock & Bewlay who also designed the south extension in 1911. The school was acquired by Birmingham Polytechnic (now Birmingham City University) in 1989, along with an adjoining site.
The university commissioned Associated Architects who designed a further south extension which was constructed between 1992 and 1993. They also redesigned much of the interior, creating a full-height atrium with gallery access to workshops. The reception area can also be used as exhibition space. The building itself consists of a Lombardo-Gothic front, whilst the 1911 extension is of red brick mottled with blue. The project won the 1995 RIBA Architecture Award and the 1996 Civic Trust Award.


== References ==


== External links ==
Official website
BCU website