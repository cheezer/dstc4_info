The Straits Chinese Jewellery Museum (Malay: Muzium Perhiasan Cina Selat; Chinese: 海峡华人珠宝博物馆) is a museum in Malacca City, Malacca, Malaysia.


== History ==
Before it was opened as the museum, the building used to be a house of a prominent Peranakan Chinese. The Straits Chinese Jewellery Museum was then opened in October 2012. Collections at the museum is the work of 30 years long of collection.


== Architecture ==
The museum is housed in a heritage house building that represents the Peranakan culture. The house consists of living room, two open-space courtyards, dining room etc.


== Exhibitions ==
The museum displays the furniture and jewellery of the Peranakan culture, ranging from brooches, shoes, porcelain, rings etc which are influenced by Chinese design and motifs and created by Chinese, Indian and Sri Lankan craftsmen. Besides the various types of jewellery which numbers up to 400, the museum also houses the jewellery making equipment and the lifestyle gallery.


== Opening time ==
The museum opens everyday from 10.00 a.m. to 5.00 p.m. on weekdays and to 6 p.m. on weekends.


== See also ==
List of museums in Malaysia
List of tourist attractions in Malacca


== References ==


== External links ==
Facebook Page - Straits Chinese Jewellery Museum