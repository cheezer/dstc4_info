The jing erhu (Chinese: 京二胡; pinyin: jīng èrhú) is a Chinese two-stringed bowed musical instrument in the huqin family of instruments, similar to the erhu. It is so named because it is used in jing xi, or Beijing opera. It is lower in pitch than the jinghu, which is the leading melodic instrument in the Beijing opera orchestra, and is considered a supporting instrument to the jinghu.
The jing erhu has a wooden body and neck. It is played vertically, with the body resting on the player's left thigh and the horsehair of the bow passing between the two strings. It previously used silk strings, but since the 1960s has more commonly used steel strings.
The jing erhu was popularized in the 1920s by Wang Shaoqing (王少卿), a musician in the troupe of Mei Lanfang.


== External links ==
erhu music - Listen to relaxing erhu music
Jing erhu page


== See also ==
Erhu
Jinghu
Huqin
List of Chinese traditional instruments