Shennong Bencaojing (also The Classic of Herbal Medicine and Shen-nung Pen-tsao Ching; simplified Chinese: 神农本草经; traditional Chinese: 神農本草經; pinyin: Shénnóng Běncǎo Jīng; Wade–Giles: Shen2-nung2 Pen3-ts'ao3 Ching1) is a Chinese book on agriculture and medicinal plants. Its origin has been attributed to the mythical Chinese sovereign Shennong, who was said to have lived around 2800 BC. Researchers believe the text is a compilation of oral traditions, written between about 200 and 250 AD (Unschuld 1986: 17). The original text no longer exists but is said to have been composed of three volumes containing 365 entries on medicaments and their description.


== Content ==
The first volume of the treatise included 120 drugs harmless to humans, the "stimulating properties": reishi, ginseng, jujube, the orange, Chinese cinnamon, Eucommia bark, or the root of liquorice (Glycyrrhiza uralensis) . These herbs are described as "noble" or "upper herbs" (上品).
The second volume is devoted to 120 therapeutic substances intended to treat the sick, but have toxic, or potentially toxic properties of varying degrees. In this category, we find the ginger, peonies and cucumber. The substances of this group are described as "human," "commoner," or "middle herbs" (中品).
In the last volume there are 125 entries corresponding to substances which have a strong or violent action on physiological functions and are often poisonous. Rhubarb, different pitted fruits and peaches are among those featured. These herbs are referred to as "low herbs" (下品).


== References ==

Jean-Baptiste Du Halde SJ (1735). "Description Géographique, Historique, Chronologique, Politique et Physique de l'Empire de la Chine et de la Tartarie chinoise" (in French). Retrieved September 3, 2010. 
Unschuld, Paul Ulrich (1986), Medicine in China, A History of Pharmaceutics, 本草, University of California Press.
Yang, Shou-zhong. The Divine Farmer's Materia Medica: A Translation of the Shen Nong Ben Cao Jing. USA, Blue Poppy Press, 2007 Google Books May. 2011