Sweatpants are a casual variety of soft trousers intended for comfort or athletic purposes, although they are now worn in many different situations. In Britain, Australia, New Zealand, and South Africa they are known as tracksuit bottoms or jogging bottoms. In Australia, they are also commonly known as trackpants or tracky daks.


== History ==
The first pair of sweatpants was introduced in the 1920s by Émile Camuset. He was the founder of Le Coq Sportif. The sweatpants were simple knitted gray jersey pants that allowed athletes to stretch and run comfortably.


== Design ==
Sweatpants are usually made from cotton or polyester, often of a heavy knit. They often have elastic in the waist, and may or may not have pockets. Sweatpants are traditionally ash gray in color but are now available in most colors. They are usually quite "baggy" and loose, making them easy to put on and take off; moreover they have flexibility and comfort. In addition, this design traps less heat than most conventional, lightweight trousers, which may be a disadvantage in some conditions such as cold temperatures and an advantage in others such as body heat-increasing aerobic activities. Warmer varieties can be made from polar fleece.
Once these practical pants were only worn for sporting events and at home. Now, they are available in many fashionable styles and are worn in a variety of public situations. Because of their comfort and fashion, they have become a popular choice of clothing. The sweatpant variations described below are made from many different materials and in many forms.


== Variations ==

There are many variations on the sweatpant design that have evolved to define their own categories of athletic pants. These variations include fashion pants, windpants, tearaway pants, and muscle pants.


=== Fashion pants ===
Fashion pants typically refers to "fashion conscious" sportswear. These pants are often made from a variety of materials, like velvet or satin, and in many color combinations or patterns. One distinguishing characteristic is that fashion pants generally lack the elastic band at the ankles.


=== Windpants ===
Windpants are similar to sweatpants but are lighter and shield the wearer from cold wind rather than insulate. Windpants are typically made of polyester or nylon, with a liner made of cotton or polyester. The nylon material's natural friction against both itself and human legs makes "swooshing" sounds during walking. Windpants often have zippers on each ankle, letting athletes unzip the end of each leg allowing the pants to be pulled over their footwear.


=== Tearaway pants ===
Tearaway pants, also known as breakaway pants, rip-off pants, or (in the UK) popper pants, are closely related to windpants. Tearaway pants are windpants with Snap fasteners running the length of both legs. The snaps allow athletes to remove their tearaway pants in a timely manner to compete in some sports. Basketball and track and field are the two sports most commonly associated with tearaway pants and windpants.


== References ==
^ Lisa Pryor (25 October 2002). "In her tracky daks, a Hollywood star turns invisible". Sydney Morning Herald. Retrieved 13 March 2013. Naomi Watts: 'I look like a completely different person when I just wake up and get my tracky daks on' 
^ "Sweatpants History: The Comfortable Choice". ASI. 29 May 2007. Retrieved 30 March 2015.