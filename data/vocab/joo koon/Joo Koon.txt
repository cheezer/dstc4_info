Joo Koon is an industrial area in the western region of Singapore. Joo Koon is served by the Joo Koon MRT Station, which is the end point of the East West MRT Line. East of Joo Koon is Lok Yang and South is Gul. Joo Koon Consists of mostly factories.
It is bounded by Upper Jurong Road, the Pan Island Expressway, Jalan Ahmad Ibrahim and Benoi Road.


== Amenities and landmarks ==
Surrounding Joo Koon industrial town are Pasir Laba Camp, SISPEC, SAFTI, Jurong Camp, Singapore Discovery Centre and Arena Country Club.
There is a food centre located along Joo Koon Way. Additionally there are ATMs, a convenience store, eatery and clinic at Joo Koon MRT Station.


== Residential ==
There is a domitory called Jurong Apartments located near Joo Koon MRT Station.


== Mass rapid transit ==
Joo Koon MRT Station is located on Joo Koon Circle in the eastern side of the industrial estate.


== Bus services ==
The below bus service plies within Joo Koon Industrial Estate:
In addition, SBS Transit bus service 192 plies along the bounded roads of Benoi Road and Jalan Ahamd Ibrahim (AYE) and 182, 182M and 193 plies along the bounded road of Upper Jurong Road.


== Future developments ==
A 16 floors multi purpose building which consists of the future Joo Koon Bus Interchange, NTUC Fairprice warehouse and office units are under construction in front of Joo Koon MRT Station.


== Neighbouring areas ==