The Jumbo Mark II Class ferries were built for Washington State Ferries between 1997 and 1999, at Todd Pacific Shipyards in Seattle. Each ferry can carry up to 2500 passengers and 202 vehicles, making them the largest ferries in the fleet, and the second longest double-ended ferries in the world. They all have full galley service and a "quiet room" upstairs.
Ferries in this class include:
Puyallup
Tacoma
Wenatchee


== References ==


== External links ==
Washington State Ferries class information