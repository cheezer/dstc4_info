Junction may refer to:


== Electricity ==
Electrical junction
Thermoelectricity junction, a metal–metal junction
Metal–semiconductor junction
p–n junction, or semiconductor–semiconductor junction

Magnetic tunnel junction
Rectifying junction
Ohmic contact, a non-rectifying junction
Heterojunction
Homojunction
Depletion region, also called junction region
Junction voltage

Junction diode
Junction transistor (disambiguation)
p–n junction isolation
Junction box, a container for the protection of electrical connections


== Finance ==
Junction (investment platform), a online investment platform that allows accredited investors to purchase limited partnership interests


== Science and technology ==
Cell junction, in biology
NTFS junction point, in computing
Perl 6#Junctions, construct of the computer language Perl 6 called Junctions


== Transport ==
Junction (traffic) where traffic routes cross
Junction (road), a road junction
Intersection (road), an at-grade road junction
Interchange (road), a grade-separated road junction

Junction (canal), a canal junction
Junction (rail), a railroad/railway junction

Junctions, a traffic simulation software package


== Places ==
United States
Junction, California (disambiguation)
Junction, Illinois
Junction, Texas
Junction, Utah
Junction, Wisconsin
England
Junction, North Yorkshire


== Popular culture ==
Junction (film), a 2012 film
Junction (Dune), a fictional planet in the Dune universe
Final Fantasy VIII, a video game that has a Guardian Force junction or status junction
Junction (manga), a manga (Japanese comic book or cartoon) by Toshiki Yui
Junction (EP), an EP by Basement Jaxx


== See also ==
The Junction (disambiguation)
Junction City (disambiguation)
Grand Junction (disambiguation)