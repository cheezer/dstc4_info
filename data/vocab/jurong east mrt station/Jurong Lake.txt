Jurong Lake (Chinese: 裕廊湖 ; Malay: Tasik Jurong ; Tamil: ஜூரோங் ஏரி) is a 70ha freshwater lake and reservoir located in the western region of Singapore formed with the damming of Sungei Jurong further downstream. The lake serves as a reservoir contributing to the water supply of the country. It lies next to the Lakeside MRT Station, which derived its name from this geographical feature. The lake is surrounded by parkland, which serves as a recreational ground for nearby residents in Jurong East and Jurong West New Towns.
A landscaped sanctuary called Jurong Lake Park was built around the perimeter of the lake and work was completed in January 2006. Also, a 2.8 kilometre water promenade along Jurong Lake Park would allow residents to participate in watersports. The inner running track starting from Chinese garden bridge and back is 3.4 kilometers in length.
As is the case for most other reservoirs in Singapore, swimming is currently illegal in the lake, although this may change in line with the Singapore government's liberalisation of the use of bodies of water in the republic. Fishing is now allowed in some designated spots around the lake where only artificial baits can be used for fishing. However, the water is noted to be green in colour during dry weather and a murky brown after a downpour, the likely result of runoff from its urban catchment area.


== Tourist attractions ==
There are several tourist attractions located near or even within the Lake, including a Chinese Garden and a Japanese Garden, which are located on their respective islands within the lake, as well as the nearby Tang Dynasty Village, which has since closed down.
The Science Centre, Singapore, a family-oriented attraction in Jurong East, is located in the area, together with Jurong Country Club, which includes an 18-hole par-72 golf course and driving range.


== Redevelopment plan ==
On 4 April 2008, the National Development Minister Mah Bow Tan announced a plan to develop the Jurong into a commercial hub outside the Central Business District (CBD). The new Jurong Lake District will offer a potential development area of 360 hectares, roughly the size of Marina Bay. The Urban Redevelopment Authority said that some 750,000m2 of land will be set aside at Jurong Gateway, expected to attract billions of dollars in development, for offices, hotels, food and beverage and entertainment uses. However, analysts say that the short 10–15 years time frame may be a little tight, due to the amount of projects in progress and a dampened global economic climate.
The Urban Redevelopment Authority announces a masterplan on 4 April 2008 to transform the area around Jurong Lake to a unique lakeside destination for business and leisure in the next 10 to 15 years.
A new district will be created, named, Jurong Lake District which consists of two precincts, Jurong Gateway and Lakeside. These refer to the areas around the Jurong East MRT Station and Jurong Lake in the west region of Singapore. Jurong Lake and the area around it, known as Lakeside, is the other area in the Jurong Lake District.
New developments around Jurong Lake include:
A public park will be developed next to Lakeside MRT Station
New Science Centre will be built next to Chinese Garden MRT Station
Lakeside village
Enhanced Chinese Garden and Japanese Garden


== References ==


== External links ==
See a diagrammatic bird's eye view of Jurong Lake at the Singapore Pagenation map:[1]