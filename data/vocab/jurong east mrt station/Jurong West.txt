Jurong West is a planning area in Singapore, bounded by the Pan Island Expressway, the eastern edge of Jurong Camp, Boon Lay Way, (Walking distance) Corporation Road, Fourth Chin Bee Road, International Road, Corporation Road, Ayer Rajah Expressway, Yuan Ching Road, Boon Lay Way, along a Canal leading into Jurong Lake, and back up to the Pan-Island Expressway. It has the second largest resident population in Singapore.


== Sub-zones ==

There are 9 sub-zones in Jurong West Planning Area, namely
Boon Lay
Chin Bee
Hong Kah
Central
Kian Teck
Safti
Taman Jurong
Bulim
Yunnan


=== Neighborhoods ===
There are 9 neighborhoods (N) in Jurong West.
N1 - Taman Jurong (South)
N2 - Bulim and Boon Lay
N3 - Taman Jurong (North)
N4 - Hong Kah (East)
N5 - Hong Kah (West)
N6 - Central
N7 - Yunnan (South East)
N8 - Yunnan (North East)
N9 - The entire area West of Pioneer Road North, consisting of the Yunnan (West)


== History ==
The development of Jurong started in the 1970s when estates such as Boon Lay, Taman Jurong, Bukit Batok, Bukit Gombak, Hong Kah, Teban Gardens and Yuhua were built, mostly due to the resettlement of Hong Kah (present-day Tengah) and surrounding villages. Boon Lay, Taman Jurong and Hong Kah formed Jurong West.
In the 1950s, Jurong West was mainly dominated by swamps with low hills covered by shrubs and a thick jungle. It was developed into an industrial estate in the 1960s, supported by low-cost housing. Amenities such as government dispensaries, a private hospital, creches, hawker centres and banks were built in the 1970s during efforts to develop Singapore economically.
Up to the late 1980s, only part of the Jurong West housing estate had been developed, specifically the area between Boon Lay estate and Jurong East. In the early 1990s, a new section of Pioneer Road North was built to connect the present Jurong West Extension to Upper Jurong Road. This signalled the start of the development of Jurong West Extension. Today, the area is also served by the PIE which was extended to Tuas from Corporation Road, also in the early 1990s.
In 1982, Jurong West New Town started expanding as Jurong West Extension, which saw the realignment of the PIE to make it go through the southern boundary of present-day NTU, while converting the former section into Jurong West Avenue 2 and renaming the original Upper Jurong Road into Jurong West Avenue 4. Pioneer Road was extended North wards from present-day Upper Jurong Road as Pioneer Road North to the new PIE exit, which signalled the start of the development of Jurong West Extension (Nanyang, Jurong West Central and Gek Poh). The N9 estate was the first to be built and the N6 estate was the last, in the early 2000s. The MRT Line was extended from Lakeside to Boon Lay in 1990 and again to Pioneer in 2009.
Though a single neighborhood, Jurong West is divided into 3 GRCs (Chua Chu Kang, Jurong, and West Coast) and 1 SMC (Hong Kah North) and with 3 Town Councils managing different parts of the neighborhood, namely the Jurong Town Council, West Coast Town Council and Chua Chu Kang Town Council.
Nonetheless, though divided into smaller constituencies, the neighborhood is collectively managed by the PAP.
Located next to the Jurong Industrial Estate, managed by JTC, it is common to find foreign workers hanging out in the neighborhood and influx of foreign workers and foreign expatriates in the region has been a concern among the residents in the area.


== Transport ==


== Tourism ==
Singapore Discovery Centre
Singapore Army Museum
Jurong Bird Park


== Retail ==

Jurong Point Shopping Mall
The largest suburban mall in Singapore, Jurong Point Shopping Centre is well-accessible by bus services from all the residential precincts of Jurong except Yuhua and Teban Gardens. This mall targets the young adults and family populations with stalls selling fashion, food and beverage, sports, entertainment and lifestyle products, and the elderly population with body-wellness stores and healthcare centres. It is well linked to the Boon Lay MRT Station and Boon Lay Bus Interchange.
There are other shopping malls in Jurong but are well-known only within the precinct they are in, such as 'Hong Kah Point' in Hong Kah and 'Taman Jurong Shopping Centre' in Taman Jurong. Therefore, it is recommended that one should read the pages of the various precincts to understand more about the shopping centres in Jurong.


== Education ==
There is the Nanyang Technological University in Jurong West. It is not only a major university in Singapore, but in entire Asia and beyond.
There are 12 Primary schools and 12 Secondary schools in Jurong West:
Boon Lay Sec, Boon Lay Garden Pri, Corporation Pri, Frontier Pri, Fuhua Sec, Hong Kah Sec, Hua Yi Sec, Jurong Sec, Jurong West Pri, Jurong West Sec, Juying Pri, Juying Sec, Lakeside Pri, Pioneer Pri, Pioneer Sec, Rulang Pri, Shuqun Pri, West Grove Pri, Westwood Pri, Westwood Sec, Xingnan Pri, Yuan Ching Sec and Yuhua Sec.
The A Level course is offered at Jurong JC and as part of the 6-Year Integrated Programme at River Valley High.


== External links ==
Jurong Point shopping centre
Singapore Discovery Centre
Southwest CDC Website
Jurong West EC Website


== References ==