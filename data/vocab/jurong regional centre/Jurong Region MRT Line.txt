The Jurong Region MRT Line (JRL) will be the seventh Mass Rapid Transit (MRT) line in Singapore. It is currently under planning and evaluation.
Expected to begin revenue service in 2025, this line will complement the existing East West Line and North South Line and to serve future residents of Tengah, students of Nanyang Technological University, future workers of CleanTech Park and the Jurong Industrial Estate.


== History ==
It was first announced in 2001, under the Concept Plan 2001, as an expected LRT line to serve residents in Jurong living beyond the end point of the original East West Line terminus, Boon Lay. However, plans for this line were put on hold indefinitely in 2008 when the Land Transport Authority announced that no new LRT lines would be built unless a new township is heavily developed, as the population of Jurong and total yearly demand from NTU deemed that the line was not economically sustainable to be built.
In 2011, under the NTU Master Plan 2011, the line was re-proposed as a private light-rail serving NTU, that would run from Pioneer and into the campus.
On 17 January 2013, this line was confirmed to be built, by 2025. It will be part of the MRT network instead of the initial proposal as a LRT line, serving Tengah and Choa Chu Kang, besides just Jurong when originally announced in 2001.


== Stations ==
Based on the Ministry of National Development Land Use Plan, Boon Lay and Choa Chu Kang are expected to be the interchanges of the line.


== External links ==
Jurong Region Line


== References ==