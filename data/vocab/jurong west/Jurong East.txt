Jurong East is a planning area of Singapore. It is bounded by Sungei Pandan, the Pan Island Expressway, Jurong Canal Drive, Boon Lay Way, Yuan Ching Road, the Ayer Rajah Expressway, Jurong Port Road, Jalan Buroh, Jurong Pier Road and the Northern border of Selat Jurong.


== History ==
The development of Jurong started in the 1970s when estates such as Boon Lay, Taman Jurong, Bukit Batok, Bukit Gombak, Hong Kah, Teban Gardens and Yuhua were built, mostly due to the resettlement of Hong Kah (present-day Tengah) and surrounding villages. Yuhua, Teban Gardens, Bukit Batok and Bukit Gombak formed Jurong East.


== Sub-zones ==
There are 10 sub-zones in Jurong East,
Jurong Port
Penjuru Crescent
Jurong River
Teban Gardens
Jurong Lake
Jurong Regional Centre
International Business Park
Toh Guan
Yuhua
Boon Lay (West of Yuhua)


== Housing ==


=== Neighborhoods ===
There are 4 neighborhoods in Jurong East
N1 - Yuhua (South) and area of Jurong Regional Centre along Jurong Gateway Road.
N2 - Yuhua (North), Toh Guan and area of Jurong Regional Centre along Boon Lay Way.
N3 - Boon Lay (Jurong East)
N4 - Teban Gardens


=== Politics ===
The Northern section is under Jurong GRC and Southern section under West Coast GRC.


=== Education ===
There are 3 Primary schools and 4 Secondary schools in Jurong East.
Commonwealth Sec
Crest Sec
Fuhua Primary School (which Pandan School merged into)
Jurong Primary School
Jurongville Sec
Shuqun Sec
Yuhua Primary School


== Recreation ==


=== Sports ===
Jurong East Sports and Recreation Centre


=== Leisure ===
Jurong Lake Park
Pandan Gardens Park Connector
Jurong Park Connector
Toh Guan Neighborhood Park
Pandan Gardens Leisure Park
Pandan Reservoir Fitness Corner
Jurong Country Club
Hong Kah East Neighborhood Park
Yuhua Village Neighborhood Park


=== Tourist Attractions ===
There are 3 tourist attractions in Jurong East:
Science Centre
Snow City
Chinese Garden


== Transport ==


=== Road ===
Jurong East is connected to the rest of Singapore with the Pan Island Expressway(PIE) and the Ayer Rajah Expressway(AYE).
Jurong Town Hall Road interconnects the two expressways with Boon Lay, Yuhua, Jurong Regional Centre, International Business Park, Jurong Lake and Teban Gardens.
Toh Guan is connected to the PIE via Toh Guan Road, while Jurong Canal Road provides an alternative at Boon Lay for traffic to (Tuas) and from (Changi) the PIE.
From the AYE, arterial roads Jurong Pier Road, Jurong Port Road and Penjuru Road (along with Minor Arterial road Teban Gardens Crescent) carry the traffic to Jurong River, Penjuru Crescent and Jurong Port.
Boon Lay Way and Jalan Buroh are the two other arterial roads in Jurong East, which provides inter-connectivity across the various sub-zones in the area.


=== Public Transport ===


==== Mass Rapid Transit ====
Jurong East is served by 2 MRT stations – Jurong East and Chinese Garden.


==== Bus ====
Most of the bus routes in Jurong East originate from Jurong East Bus Interchange, which is adjacent to Jurong East MRT Station.


==== International Railway ====
Jurong East was chosen as the Singapore terminus of the Kuala Lumpur–Singapore High Speed Rail on the 5th May 2015.


== Economy ==
At Jurong Port, Jurong River, Penjuru Crescent, and parts of Toh Guan and Teban Gardens, there is land allocated for business activities.


=== Jurong Lake District ===
Consisting of Jurong Lake, Jurong Regional Centre, International Business Park and the southern section of Toh Guan, the Jurong Lake District is a prime regional centre serving as an inviting place for commercial development remote from the Central Area, to meet the various demands of business and provide employment opportunities nearer to people staying in the Western region of Singapore.


==== International Business Park ====


==== Commercial ====


===== Shopping malls =====
At Jurong Regional Centre and Toh Guan, there are 5 Shopping malls,
IMM
JCube
JEM
Westgate
Big Box


===== Town Centre =====
The Town Centre of Jurong East is located at Jurong Regional Centre.


== External links ==
Jurong Regional Library
Chinese and Japanese Garden
Science Centre, Singapore
Southwest CDC Website


== References ==