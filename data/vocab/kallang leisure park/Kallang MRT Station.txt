Kallang MRT Station is an above-ground Mass Rapid Transit station on the East West Line in Singapore. It is in yellow livery and located just next to the Kallang River, which is famous for its water sports, and serves the urban planning area of Kallang. This is the last above-ground station until Redhill. Westward of the station towards Lavender, the tracks lose elevation until they level out when sufficiently underground. This is because of the space limitations as there is no such elevation of tracks in the CBD.
The station is within the vicinity of Boon Keng, Stadium and the planned Geylang Bahru. This station is next to the Lorong 1 Geylang Bus Terminal and near to Kallang Riverside Park.
When Stadium Station was under construction, Kallang was crowded with people, when either the Singapore Indoor Stadium or the National Stadium had an event.


== Platform screen doors ==
As with most of the above-ground stations along the East West Line, it was built without platform screen doors. Installation of half-height platform screen doors, to prevent commuters from falling onto the train tracks, was started on 19 December 2010 and started operation on 25 February 2011 with Kembangan.


== High volume, low speed (HVLS) fans ==
Station installed with Rite Hite Revolution High Volume, Low Speed (HVLS) fans and in operations since 28 July 2012.


== Station layout ==


== Exits ==
A: Sims Avenue, Geylang Lorong 1 Bus Terminal
B: Lorong 1 Geylang, Boon Keng Road, Geylang West CC, Kallang River, Kallang Park Connector, Kallang Sector Industrial Estate, Sims Avenue Industrial Estate, Kallang Trivista


== Transport connections ==


=== Rail ===


== References ==


== External links ==
Official website