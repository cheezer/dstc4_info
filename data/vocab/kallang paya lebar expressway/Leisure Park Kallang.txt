Leisure Park Kallang (or Kallang Leisure Park) is an entertainment centre cum shopping mall in central Singapore. It is served by Stadium MRT Station and is within the vicinity of Tanjong Rhu. It is located in Kallang and it is sited next to the Singapore Sports Hub, Kallang Theatre and Singapore Indoor Stadium


== History ==
Leisure Park Kallang was built in 1982 as the country's first purpose-built entertainment centre. At the time of completion, it featured a bowling alley, a three-screen cinema and an ice skating rink. Located next to the National Stadium, it served the crowds after major events there, and its popularity peaked in the 1990s. The mall began to lose popularity in 2002, when construction works for Stadium MRT commenced. Eventually, having seen businesses take a sharp decline, it was sold to Jack Investments in 2003. It shut its doors in 2004 for redevelopment works. The mall was rebuilt in 2007 and it currently has old timers such as a six-screen multiplex, an ice rink and a bowling alley. Recently, Leisure Park Kallang has become more convenient due to the opening of Stadium MRT and the Kallang Wave Mall at the rebuilt National Stadium.


== Anchor Tenants ==
Cold Storage
Filmgarde Cineplex
Kallang Bowl
Kallang Ice World


== References ==