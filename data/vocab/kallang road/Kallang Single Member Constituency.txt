Kallang Single Member Constituency is a former defunct Single Member Constituency spanning Kallang of Singapore. It was there since 1959 to 1991, until it absorbed into Jalan Besar GRC.


== Members of Parliament ==
Suppaniah D (1976 - 1991)


== Candidates and results ==


== References ==
1959 GE's result
1963 GE's result
1968 GE's result
1972 GE's result
1976 GE's result
1980 GE's result
1984 GE's result
1988 GE's result
1991 GE's result