Kallang Bahru is an estate in Kallang for both housing and industrial use. It is served by the future Downtown Line Stage 3's Kallang Bahru MRT Station. Kallang Bahru (estate name) is near to Kallang River and the Kallang MRT Station as well as Boon Keng MRT Station.
It is also the name of the road that serves the area.


== See also ==
Kallang
Kallang MRT Station
Kallang Bahru MRT Station


== External links ==
 Media related to Kallang Planning Area at Wikimedia Commons