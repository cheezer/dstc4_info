Peter Kam Pui-Tat (Chinese: 金培達) is a music composer for Hong Kong films including The Warlords,Bodyguards and Assassins and Wu Xia.
Kam is an eight-time winner at the Hong Kong Film Awards.


== Partial filmography ==
Full Alert (1997)
Kung Fu Jungle (2014)


== External links ==
Peter Kam at the Internet Movie Database