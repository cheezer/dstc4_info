Kampong may refer to:


== General use ==
Kampong (village), from kampung, an Indonesian and Malaysian word for a hamlet or village


== Places ==
Cambodia
Kampong Cham Province, province in Eastern Cambodia, bisected by the Mekong River
Kampong Chhnang Province, also known as "Port of Pottery", province in Central Cambodia
Kampong Speu Province, province in southwestern Cambodia
Kampong Thom Province, province on the banks of the Stung Saen River in northwestern Cambodia, bordering the Tonie Sap
Singapore
Kampong Glam, a neighbourhood in Singapore
United States
The Kampong, a large tropical garden in Miami, Florida
Australia
Kampong, another name for the northern settlement area on Christmas Island, Flying Fish Cove


== Other uses ==
Kampong (field hockey club), a Dutch field hockey club in Utrecht
Kampong, common name for Oroxylum indicum, a tree native to South-East Asia