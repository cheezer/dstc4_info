Kampong Thom (Khmer: កំពង់ធំ, IPA: [kɑmpɔːŋ tʰom] "Grand Port") is a province (khaet) of Cambodia. It borders the provinces of Siem Reap to the northwest, Preah Vihear to the north, Stung Treng to the northeast, Kratie to the east, Kampong Cham, Kampong Chhnang to the south, and the Tonle Sap to the west.

The provincial capital is Kampong Thom City, a town of approximately 30,000 people on the banks of the Stung Sen River.
Kampong Thom is Cambodia's second largest province by area. There are a number of significant Angkorian sites in the area, including Prasat Sambor Prei Kuk and Prasat Andet temples. As one of the nine provinces bordering Tonle Sap Lake, Kampong Thom is part of the Tonlé Sap Biosphere Reserve.


== Administration ==

The province is subdivided into 8 districts.
0601 Baray
0602 Kampong Svay
0603 Stung Sen
0604 Prasat Balang
0605 Prasat Sambour
0606 Sandan
0607 Santuk
0608 Stoung


== History ==
The previous name of the province was Kampong Pous Thom (Port/City of the Great Snakes). According to local legend, at a lakeside dock near the Sen River, a pair of large snakes inhabited a nearby cave. On every Buddhist holiday, the snakes would make appearances to the people nearby who then began to refer to the area as Kampong Pous Thom. Eventually the snakes disappeared and the name was shortened to Kampong Thom. During the Colonial Cambodia period, the French divided Cambodian territory into provinces and named most of them according to the local popular names for the respective areas.
Kampong Thom was a powerful capital in Southeast Asia during the Funan period. Prasat Sambor Prei Kuk, dating from the Chenla Era, is in Kampong Thom Province.


== Geography ==
Two of the three core areas in Tonlé Sap Biosphere Reserve are located in Kampong Thom.
Boeng Chhmar (14,560 hectares), and
Stung Saen (6,355 hectares).


== Economy ==

Kampong Thom Province is rich in tourism potential, attracting tourists with its exotic lakes, rivers, forests, mountains and more than 200 ancient temples.
Much of Kampong Thom is located on the floodplain of the Tonlé Sap lake. In 2003–04, it was a significant producer of wild fish (18,800 tons) and the fourth largest producer of fish through aquaculture in Cambodia (1,800 tons). Most of the fish-raising is done by home production, with a growing segment devoted to rice field aquaculture.
Kampong Thom is also one of the largest producers of cashew nuts in Cambodia, with 6,371 hectares under production in 2003–04.


== References ==


== External links ==