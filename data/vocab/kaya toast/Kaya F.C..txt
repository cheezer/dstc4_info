Kaya Futbol Club is a Filipino professional association football club based in Makati City in the National Capital Region. They are currently playing in the first division of the United Football League, the highest level of football in the Philippines.
The name of the club comes from the Filipino word káya, which means "we can". In Old Tagalog, the word is closely defined as susi ng kapatiran (key to brotherhood). Both of these definitions provide the basis for Kaya FC's team spirit and vision as a club. The club is owned by Santiago Araneta, the CEO and the owner of LBC Express. The club's main rival is the Loyola Meralco Sparks.


== History ==
In the late 1980s, one evening a week, players of all ages and from different backgrounds would get together on a basketball court for indoor football. The enjoyment of the weekly practices grew and the players' skill improved rapidly. They began to join outdoor 7-a-side football tournaments playing against different teams. Frequently the Kaya team members found themselves on the winner’s podium receiving medals and trophies.
After a few years and multiple awards, they realized that many of their teammates were some of the best in the country and the team needed to set a foundation. The name Kaya Football Club was then created in July 1996 and the beginning of the Kaya leadership on and off the field for football in the NCR and the Philippines became more prominent.
In the late 1990s Kaya FC participated in official and more challenging 11-a-side football tournaments organized by the National Capital Region Football Association. The team was eventually recognized as one of the only club teams capable of defeating the "big three", composed of the Philippine Army, Philippine Air Force and Philippine Navy football clubs.
Between 2000 and 2009 Kaya FC defeated the armed forces teams in two separate championships and was the Champion of the United Football League (UFL) on three occasions (2009 the UFL became the LBC-UFL and invited the armed forces to join the league wherein Kaya FC placed 2nd).
In the last few years a number of Kaya FC players represented the Philippines in international tournaments.


== Image and Fanbase ==
The colors of Kaya's badge, red, green, yellow and black, are mainly based on the pan-African colors that are associated with rastafarianism. This is mainly due to the heavy influence of Reggae on the club's founding members lives. Even the club's name, aside from its original Filipino meaning, are also derived from a famous Filipino reggae song from the pinoy band, Pinikpikan. As a result of this strong influence on the clubs members, the club itself is mainly associated with this movement.
In addition to this, Kaya boasts of having the most fans in the UFL, and fangroups such as the famous Ultras Kaya, are known for their loud and fierce support of the club.
Kaya has had rivalries with many teams in the UFL, the most prominent ones today being with fellow NCR-based Loyola Meralco Sparks F.C. and with Global F.C.. These three clubs are also amongst the most supported and successful in the UFL today making every clash between the three very intense. In recent years, however, the Sparks have become Kaya's main rivals, mainly due to the fact that both are the most successful teams in the NCR, with Kaya haling from Makati in the south of Metro Manila and Loyola being based in Quezon City in the north. The rivalry started during the 2011 UFL Cup semi-finals clash between the two, in which Kaya went up to lead the game by 3–0 only to lose to the Sparks by 4–5 after an enthralling comeback of the Sparks. The loss has always been painful for Kaya fans. In the 2010s they also had a fierce rivalry with Union Internacional Manila F.C. but since the club dissolved and decided to be relegated to Division 2, this rivalry has mostly died out.
Viewed as the pioneers in ultras culture in the Philippines, the Ultras Kaya also inspired the creation of other ultra goups such as the PAF ultras, the supporters of Philippine Air Force F.C., with whom the Kaya ultras share a close friendship. Coincidentally, Kaya and the Airmen have the same rival in the Sparks ever since Airforce beat Loyola in the Finals of the 2011 UFL Cup.


== First team squad ==
As of 24 January 2015
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


== Reserve Team (Kaya F.C. Elite) ==
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


== Coaching staff ==


== Manager ==


== Sponsors ==
1Major shirt sponsor (names located at the front of the shirt).
2Secondary sponsor (names mostly located at the back of the shirt).


== Records ==
Key
Tms. = Number of teams
Pos. = Position in league
TBD = To be determined
DNQ = Did not qualify
Note: Performances of the club indicated here was after the UFL created (as a semi-pro league) in 2009.


== Honors ==
United Football League Division 1
Runners-up (2): 2010, 2012

UFL FA Cup
Runners-up: 2014

PFF National Men's Club Championship

Third place (1): 2013

Adidas Football Festival
– Champions: 1996
Chines-Pilipino League
Soccer Ventures Football Festival
Nomads International 6-Aside
Alaska Cup
– Champions: 2000, 2003, 2004, 2009
NCRFA Summer League Division 1
NCRFA League Division
NCRFA League Division 1
Chris Monfort Cup
Adidas National Lighting Cup
PFF 7-Aside Cup
PLDT 7-Aside Cup
UCFA 6-Aside Cup
Kia Cup


== References ==


== External links ==
Official website