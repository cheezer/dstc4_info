Kaya may refer to:


== People ==
Kaya (given name)
Kaya (surname)


== Places ==
Kaya, Burkina Faso, a town in Burkina Faso, capital of the department
Kaya Airport, serving the town
Kaya Department, a department or commune of Sanmatenga Province in central Burkina Faso
Kaya, Fethiye, a village in Muğla Province, Turkey
Kaya, Hopa, a village in Artvin Province, Turkey
Kaya, Kyoto, a town located in Yosa District, Kyoto Prefecture, Japan
Kaya, South Sudan, a town in South Sudan
Skiu-Kaya, adjoining villages in Ladakh, India


== Popular culture ==
Kaya (film), a 1969 Yugoslav film
Kaya FM, a radio station in Johannesburg, South Africa
Kaya (TV series), a scripted MTV drama television series


=== Anime ===
Kaya (One Piece), a fictional character in the anime and manga One Piece
Kaya (Princess Mononoke), a character from the anime movie Princess Mononoke


=== Music ===
Kaya (album), an album by Bob Marley and the Wailers (1978)
Kaya Tour, a concert tour organised to support the album

Kaya (Canadian singer) (born 1968), real name Francis Martin Lavergne
Kaya (Japanese musician), former member of Schwarz Stein
Kaya (Mauritian musician) (1960–1999), singer and creator of Seggae
Kaya, a vocal group in the Australian version of The X Factor
Kaya (Bulgarian band) (1999–present)


== Other uses ==
Gaya confederacy, a confederacy in southern Korea (42–562 CE)
Kaya Airlines, a Mozambican airline
Kaya F.C., a Filipino association football club
Kaya identity, an equation relating factors that determine the level of human impact on climate, in the form of emissions of the greenhouse gas carbon dioxide
Kaya (jam), a type of coconut egg jam popular in Malaysia and Singapore
Kaya (Mijikenda), a sacred forest site of the Mijikenda peoples in Kenya
Kaya-no-miya, the seventh oldest collateral branch (ōke) of the Japanese Imperial Family
Kaya toast, a popular snack amongst Malaysians and Singaporeans
Kaya (tree), Torreya nucifera, or Japanese Nutmeg tree
Kaya (ship), several Japanese ships
Kaya Yei, a traditional Ghanaian porter


== See also ==
All pages with titles containing "Kaya"
All pages beginning with "Kaya"
Kayah (disambiguation)