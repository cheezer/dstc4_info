Döner kebab (/ˈdɒnər kəˈbæb/, /ˈdoʊnər/; Turkish: döner or döner kebap, [døˈnɛɾ̝̊ ceˈbap]) is a Turkish dish made of meat cooked on a vertical rotisserie, normally lamb but sometimes beef, or chicken.
The sliced meat of a döner kebab may be served wrapped in a flatbread such as lavash or pita or as a sandwich instead of being served on a plate. It is a common fast-food item not only in Turkey but also in the Middle East, Europe, Canada and Australia. Seasoned meat in the shape of an inverted cone is turned slowly against a vertical rotisserie, then sliced vertically into thin, crisp shavings. On the sandwich version, the meat is generally served with tomato, onion with sumac, pickled cucumber and chili.


== HistoryEdit ==

Before taking its modern form, as mentioned in Ottoman travel books of the 18th century, the döner used to be a horizontal stack of meat rather than vertical, like the cağ kebabı of Erzurum. Grilling meat on horizontal skewers has an ancient history in the Eastern Mediterranean, but it is unknown when slices of meat, rather than chunks, were first used.
In his own family biography, İskender Efendi of 19th century Bursa writes that "he and his grandfather had the idea of roasting the lamb vertically rather than horizontally, and invented for that purpose a vertical mangal". Since then, Hacı İskender is known as the inventor of Turkish döner kebap. With time, the meat took a different marinade, got leaner, and eventually took its modern shape.


== EtymologyEdit ==
A döner kebab is sometimes spelled döner kebap (the Turkish spelling), lit. "rotating roast", or can be shortened to döner (Turkish: döner), lit. "turn around", also spelled "doener", "donar", "donair", "doner", or sometimes "donner".
In Greece, döner kebab is called gyros. The most common form of gyros is prepared with pork, due to its broad availability and low price in Greece. The name comes from Greek γύρος ("turn"), a calque of the Turkish name döner kebap; the dish was formerly called ντονέρ [doˈner] in Greece as well. Today, ντονέρ refers to gyros prepared with lamb or beef.


== Döner in TurkeyEdit ==
There are many variations of döner in Turkey:
Porsiyon ("portion", döner on a slightly heated plate, sometimes with a few grilled peppers or broiled tomatoes on the side)
Pilavüstü ("over rice", döner served on a base of pilaf rice)
İskender (specialty of Bursa, served in an oblong plate, atop a base of thin pita, with a dash of pepper or tomato sauce and boiling fresh butter) "Kebapçı İskender" is trademarked by Yavuz İskenderoğlu, whose family still runs the restaurant in Bursa.
Dürüm, wrapped in a thin lavaş that is sometimes also grilled after being rolled, to make it crispier. It has two main variants in mainland Turkey:Soslu dürüm or SSK (sos, soğan, kaşar; in English: sauce, onion, cheese) (specialty of Ankara, contains İskender kebap sauce, making it juicier)
Kaşarlı dürüm döner (speciality of Istanbul, grated kaşar cheese is put in the wrap which is then toasted to melt the cheese and crisp up the lavaş)

Tombik or gobit (literally "the chubby", döner in a bun-shaped pita, with crispy crust and soft inside, and generally less meat than a dürüm)
Ekmekarası ("between bread", generally the most filling version, consisting of a whole (or a half) regular Turkish bread filled with döner)


== Regional variationsEdit ==


=== Caucasus, Middle East and AsiaEdit ===


==== ArmeniaEdit ====
In Armenia Ġarsi khorovats, šaurma or in the Armenian diaspora, "Tarna" (literally, "it turns"); it is usually lamb, pork or chicken on a vertical rotisserie, sliced and wrapped in lavaş, served with tahini, yogurt or garlic sauce and with a side dish of pickled vegetables or tourshi.


==== AzerbaijanEdit ====
In Azerbaijan, döner is called Shaurma (Aze: Şaurma) or Döner (Aze: Dönər). Şaurma is made with chicken and always include garlic sauce, whereas döner can be made with either chicken or beef, and does not include garlic sauce. Both can be served in bread, lavaş or in plate. Döner also can be served in tandoor bread. The most popular variety is Turkish döner.


==== JapanEdit ====

In Japan, döner kebabs are now quite common, especially in Tokyo. They are predominantly made of chicken but occasionally beef, and are often sold from parked vans. Called simply "kebab", they have been simplified to suit Japanese tastes; the salad is usually omitted in favour of shredded cabbage, usually with a choice of sauces such as regular (often just a mix of mayonnaise and ketchup), spicy, and garlic, and often a slice of tomato.


==== South KoreaEdit ====
Döner kebab is available throughout much of Seoul, particularly in the foreigner-dominated neighborhood of Itaewon. There are two main varieties: the first, sold from street carts, is modified to suit Korean tastes, with chicken rather than lamb, shredded white cabbage, and honey mustard; the second is offered at permanent takeaways such as Ankara Picnic, Mr. Kebab and Sultan Kebab, and features a lamb option along with more traditional sauces.


==== ThailandEdit ====
There is at least one döner kebab shop on the island of Koh Samui. There are many kebab shops around the Nana area on Sukhumvit Road in Bangkok, and in Pattaya on Walking Street.


==== VietnamEdit ====
Döner kebab is increasingly becoming popular in Vietnam. Throughout Hanoi and Ho Chi Minh City many döner kebab stalls can be found. Bánh mỳ Döner Kebab, the Vietnamese version of the döner kebab, has some fundamental differences with the original döner kebab. First of all, pork is used instead of beef and lamb. Second, the meat is served in a Vietnamese baguette. Thirdly, the meat is topped with sour vegetables and chili sauce. In contrast to many other countries in Asia, the döner kebab in Vietnam has been localized and is primarily consumed by the locals, while in other countries in the Far East, kebabs are primarily sold to expats, tourists and the middle class, and the original recipe is used.


=== EuropeEdit ===


==== AustriaEdit ====

Döner kebab shops can be found in all cities across Austria. Kebabs (rarely referred to as "Döner") outsell burgers or the traditional Würstel (sausage). The range of döner is similar to other German-speaking countries, but chicken kebabs are more likely to be found in central Vienna than lamb or beef kebab.


==== BelgiumEdit ====
Döner kebab restaurants and food stands can be found in almost all cities and smaller towns in Belgium, where they are known as dürüm when served in a wrap. The variety served is similar to that of Germany and the Netherlands. However, it is not uncommon to see döner served with French fries in Belgium, often stuffed into the bread itself (similar to the German "Kebab mit Pommes"). This is probably done to suit local taste, as fries are still the most common Belgian fast food. Many different sauces are typically offered, including plain mayonnaise, aioli, cocktail sauce, sambal oelek or harissa paste, andalouse sauce, "américaine" sauce and tomato ketchup or curry ketchup. Another basic ingredient of the typical Belgian döner kebab is two or three green, spicy, Turkish peppers.


==== FinlandEdit ====

In Finland, döner kebabs have gained a lot of popularity since Turkish immigrants opened restaurants and imported their traditional food. Kebabs are generally regarded as fast food, often served in late-night restaurants also serving pizza, as well as shopping malls. There are over 1000 currently active restaurants that serve kebab foods in Finland, making one kebab restaurant for every 5000 people in mainland Finland.


==== FranceEdit ====
Most kebab shops (themselves known simply as kebabs) are generally run by North African immigrants in France. The basic kebab consists of either "pain de maison" (Turkish soft bread) or "pain arabe" (unleavened flatbread) stuffed with grilled lamb shavings, onions and lettuce, with a choice of sauce from sauce blanche (yogurt sauce with garlic and herbs), harissa (spicy red sauce originally from North Africa), ketchup, or several others. Kebabs are usually served with french fries, often stuffed into the bread itself. This variation is called Sandwich grec ("Greek sandwich"). Other variations include beef, turkey, chicken, veal, and replacing the Turkish bread with pita bread or baguette.


==== GermanyEdit ====
Turkish immigrants in Berlin developed a version to suit German tastes; this regional variation has become one of Germany's most popular fast food dishes. Annual sales in Germany amount to €2.5 billion. Veal, chicken, and becoming increasingly more popular, turkey ("Truthahn"), are widely used instead of lamb, particularly by vendors with large ethnic German customer bases, for whom lamb is traditionally less preferred.

Typically, along with the meat, a salad consisting of chopped lettuce, cabbage, onions, cucumber, and tomatoes is offered, as well as a choice of sauces like hot sauce, herb sauce, garlic sauce, or yogurt sauce. The filling is served in a thick flatbread that is usually toasted or warmed. A German variety of döner presentation is achieved by placing the döner meat and the add-ons on a lahmacun and then rolling the ingredients inside the dough into a tube that is eaten out of a wrapping of usually aluminum foil, sometimes called "Turkish Pizza". When plain dough is used instead of Lahmacun the rolled fast food is called "Dürüm Döner" or "Yufka Döner."
Tarkan Taşyumruk, president of the Association of Turkish Döner Producers in Europe (ATDID), provided information in 2010 that, every day, more than 400 tonnes of döner kebab meat is produced in Germany by around 350 firms. At the same ATDID fair, Taşyumruk stated that, "Annual sales in Germany amount to €2.5 billion. That shows we are one of the biggest fast-foods in Germany." In many cities throughout Germany, döner kebabs are at least as popular as hamburgers or sausages, especially with young people.
In 2011 there were over 16,000 establishments selling döner kebabs in Germany, with yearly sales of €3.5 billion.


==== NetherlandsEdit ====

Döner kebab is very popular and widely available in the Netherlands. As a snack, it is usually served in or with a pita as a "broodje döner" (döner sandwich) with lettuce, onion, tomato slices and sauces, mainly garlic and sambal.
A new form of serving, called a "kapsalon", is increasing in popularity. It is a metal tray filled with French fries with a layer of döner (sometimes a layer of sauce) over them, topped by a layer of young cheese. This goes into the oven until the cheese melts. Then a freshly sliced salad is put on top of that. The kapsalon is finished with a large amount of garlic sauce and a bit of sambal.
The Dutch television programme Keuringsdienst van Waarde analyzed döner kebab sandwiches advertised as lamb and found that only one of them contained 100% lamb meat, while most consisted of mixes of lamb and beef. Some consisted of 100% beef, chicken, turkey or pork.


==== United KingdomEdit ====

Introduced by Turkish immigrants, the döner kebab with salad and sauce is a very popular dish in the United Kingdom, especially after a night out. The typical kebab shop or roadside van in the UK will offer hot chilli sauce and garlic yoghurt-style sauce, and may also offer lemon juice, mayonnaise, or perhaps a mint sauce similar to raita. Döner kebabs are most commonly served in a pita bread in the UK, but are sometimes also wrapped in other types of bread - naan bread or roti, for example. Döner meat is also sometimes served as a pizza topping or simply with a side order of chips. In many kebab shops a chicken döner kebab is served as well, being cooked in the same fashion next to the lamb döner. The first döner kebab shop opened in the UK in 1966 in London, and was called the Hodja Nasrettin and was owned by Cetin Bukey.


=== AmericasEdit ===


==== CanadaEdit ====
A variation known as "donair" was introduced in Halifax, Nova Scotia, Canada in the early 1970s. Peter Kamoulakos immigrated to Canada in 1959. When he failed in his attempt to sell traditional gyros, Kamoulakos adapted the dish to local tastes. He substituted beef for lamb and created a sweet sauce. He claimed he invented the donair in 1972 and that it debuted at King of Donair's Quinpool Road location in 1973, but this cannot be confirmed.

"The Original" Mr. Donair is a company originally started by Peter Kamoulakos to further popularize donair via retail sales and food service.
In the summer of 2008, after numerous cases of E. coli related food poisoning due to the consumption of undercooked donair meat in Alberta, the federal government came out with a set of guidelines for the preparation of donairs. The principal guideline is that the meat should be cooked at least twice: once on the spit, and then grilled as the donair is being prepared.


==== United StatesEdit ====
In the United States, döner kebab is becoming more popular especially in cities with Mideastern immigrant communities, such as New York, Chicago, Detroit, Omaha, Seattle, San Diego, and Los Angeles.


==== MexicoEdit ====
Al pastor is a probable variation of döner kebab. Literally "in the style of the shepherd", it references the lamb often used in döner kebab, Greek gyro and Arab shawarma.


=== OceaniaEdit ===


==== AustraliaEdit ====

In Australia, döner and other kebabs are very popular due to immigration from Turkey and the Middle East, as well as postwar immigration from Greece which introduced the "yiros" to some areas of Australia. Döner (usually referred to in Australia as a "kebab") is sometimes considered to be a healthier alternative to traditional fast food. In Australian shops or stalls, beef, lamb, chicken and falafel kebabs can be found in all major cities where many suburbs have take-away shops that offer them. They are typically served with a salad consisting of lettuce, tomato, onion and tabouli, and optionally with grated cheese, on pita (also known in some areas as Lebanese bread), with one or more sauces or spreads, such as hummus, garlic yoghurt, chilli or BBQ. In New South Wales, the kebabs are often briefly toasted in a sandwich press after construction.
In Adelaide, kebabs are called "yiros". The pita bread is toasted on a hotplate before rolling, and is not further toasted after rolling. Cheese is not usually added.
The "dodgy kebab", often blamed for food poisoning has become more rare since New South Wales food safety best practice recommended a second cooking of kebab meat. Most stores have adopted this measure and it is now common practice in Australia. Second cooking requires that meat sliced from the doner is cooked on the hotplate/grill to 60 °C just before serving. Previously, "dodgy kebab" meat was often sliced from the doner, including some not yet fully heated/cooked meat, at the time of ordering or meat that had been sliced and sat waiting at the bottom of the doner for an unknown length of time.


== Health concernsEdit ==
Döner kebab is popular in many countries in the form of fast food, often as an end to a night out when preceded by the consumption of an excess of alcohol. Health concerns surrounding döner kebab in the UK and Western Europe, including the hygiene involved in overnight storage and re-heating of partially cooked meat, as well as high salt and fat levels, have been reported in the European media. However, Simon Langley-Evans, a professor of human nutrition at Nottingham University states that döner is a healthier choice of fast food, as it brings together meat, wholemeal bread and vegetables.


== See alsoEdit ==
Cağ kebab
Dürüm
Iskender kebap
Street food


== ReferencesEdit ==


== Further readingEdit ==
Cardin, Geoff (July 29, 2011). "The Dish: Döner Kabob". Feast Magazine. Retrieved 2013-04-18. 


== External linksEdit ==
 Döner kebab at Wikibook Cookbooks
 Media related to Döner kebab at Wikimedia Commons