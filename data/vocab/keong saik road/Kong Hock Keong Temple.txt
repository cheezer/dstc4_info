Kong Hock Keong Temple (廣福宮) also Kuan Ying Teng Temple (觀音寺) is a Taoist and Buddhist temple in Georgetown, Penang built in 1800 by early Chinese settlers of the Hokkien and Cantonese communities. It is located in north-west Malaysia. It is dedicated to the worship of Guanyin. It is located on Jalan Kapitan Keling.


== Protected From Destruction ==
The temple has come through threats, unscathed, time and again. Not only did it survive a bomb dropped by the Japanese invasion of the Second World War, but also other threats through the years, including the fire of 1846, the terrorist granade attack on the Penang power sub-stations at Pitt Lane and the Chinese Town Hall next door to the temple, and the middle-of-the-night blaze that destroyed the nearby stalls selling oil and joss paper.


== References ==


== External links ==
 Media related to Kong Hock Keong Temple, Penang at Wikimedia Commons