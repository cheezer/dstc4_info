Keppel Bay is a broad bay in Central Queensland, Australia at the mouth of the Fitzroy River. Cape Keppel is at the Eastern end of the bay.
The bay and the nearby Keppel Islands (part of Keppel Bay Islands National Park) were named by Captain Cook when he was there on 27 May 1770, after Admiral Augustus Keppel of the British Royal Navy.


== View ==


== See also ==

Great Keppel Island
Cape Manifold
Pumpkin Island


== References ==


== External links ==
University of Queensland:Queensland Places: Keppel Bay Area