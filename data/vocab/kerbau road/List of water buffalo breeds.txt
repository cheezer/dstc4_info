This is a list of domestic water buffalo breeds and their uses.
The domestic buffalo, Bubalus bubalis, is descended from the wild water buffalo (Bubalus arnee), now designated an endangered species. Buffalo have been bred, predominantly in Asia, for thousands of years for use by humans. Their main domestic uses are as draught animals and for the production of milk and meat. Two types are recognised, the river type and the swamp type. Note: except where otherwise indicated, the reference for all entries is DAD-IS.


== See also ==

List of cattle breeds
 Media related to Water buffalos at Wikimedia Commons


== References ==