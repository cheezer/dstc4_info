Tekka Centre is a multi-use building complex comprising a wet market, food centre and shops, located in the northern corner of Bukit Timah Road and Serangoon Road, in Little India, Singapore.


== Etymology and history ==
The case of Tekka Centre is often used to illustrate the complexities of Chinese language romanisation in Singapore. The market was originally known as Kandang Kerbau (or just KK), Malay for "buffalo pens", referring to the slaughterhouses operating in the area until the 1920s, and the name still lives on in the nearby Kandang Kerbau Women's and Children's Hospital, Kandang Kerbau Police Station and the Kandang Kerbau Post Office. In Hokkien, the market was known as Tek Kia Kha, literally meaning "foot of the small bamboos", as bamboo plants once grew on the banks of the Rochor Canal. This was adapted into the popular name Tekka Pasar (笛卡巴刹), where pasar is Malay for "market".
The original market was built in 1915, and was located across the street between Hastings Road and Sungei Road. When it was torn down in 1982 and relocated at its present site, the new multi-use complex was named Zhujiao Centre (竹脚中心), the pinyin version of Tek Kia. However, to locals, especially non-Chinese, the new word Zhujiao was both hard to read and pronounce and bore no resemblance to Tekka. Eventually, the complex was officially renamed Tekka Centre in 2000 as it better reflected the history of the place. The market was closed for a significant renovation in 2008, reopening in 2009.
Little India's first air-conditioned mall, Tekka Mall (later renamed The Verge), was built on the original site of the market in 2003.


== Facilities ==

Today, Tekka Centre remains a landmark in Little India, where different ethnic communities congregate. There are Chinese stallholders who speak Tamil, and vice versa. Shops sell traditional Indian costumes and inexpensive casual clothes. Some of the more notable shops include those selling Taoist and Buddhist paraphernalia, hardware shops, and tailors who can alter clothes in minutes.

On the ground floor is a hawker centre with stalls which sell Indian vegetarian meals, served on banana leaves or on stainless steel platters, besides Chinese vegetarian, North Indian and Malay food.
At the wet market which is on the same level, stalls sell fresh seafood, especially crabs from Sri Lanka, and vegetables. There are also many Chinese stalls selling vegetables that are specially flown in from India.
The centre is served by the adjacent Little India MRT station. There are also an underground car park and two taxicab stands. Amenities nearby include The Verge and Little India Arcade.


== References ==