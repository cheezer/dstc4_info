Ketupat (in Indonesian and Malay), Kupat (in Javanese and Sundanese) or Tipat (in Balinese) is a type of dumpling made from rice packed inside a diamond-shaped container of woven palm leaf pouch. It is commonly found in Indonesia, Malaysia, Brunei, Singapore, and the Philippines (where it is known by the name pusô in Cebuano, bugnóy in Hiligaynon, patupat in Kapampangan and Pangasinan, or ta’mu in Tausug). It is commonly described as "packed rice", although there are other types of similar packed rices such as lontong and bakchang.
Ketupat is cut open, its skin (woven palm leaf) being removed, the inner rice cake is cut in pieces, and served as staple food, as the replacement of plain steamed rice. It usually eaten with rendang, opor ayam, sayur labu (chayote soup), sambal goreng ati (liver in sambal) or served as an accompaniment to satay (chicken or beef or lamb in skewers) or gado-gado (mixed vegetables with peanut sauce). Ketupat is also the main element of certain dishes such as ketupat sayur (ketupat in jicama soup with tofu and boiled egg) and kupat tahu (ketupat and tofu in peanut sauce).


== History ==
The use of woven young palm leaves (janur) fronds as a pouch to cook food is widespread in Maritime Southeast Asia, from Indonesia, Malaysia, to the Philippines. Ketupat is made from rice that has been wrapped in a woven palm leaf pouch and boiled. As the rice cooks, the grains expand to fill the pouch and the rice becomes compressed. This method of cooking gives the ketupat its characteristic form and texture of a rice dumpling.

Local stories passed down through the generations have attributed the creation of this style of rice preparation to the seafarers' need to keep cooked rice from spoiling during long sea voyages. The coco leaves used in wrapping the rice are always shaped into a triangular or diamond form and stored hanging in bunches in the open air. The shape of the package facilitates moisture to drip away from the cooked rice while the coco leaves allow the rice to be aerated and at the same time prevent flies and insects from touching it.
In Java and most of Indonesia, ketupat is linked to Islamic tradition of lebaran (Eid ul-Fitr). The earliest connection of ketupat with Islamic lebaran tradition is believed to be originated in 15th-century Sultanate of Demak. Nevertheless ketupat is also known in non-Muslim communities, such as Hindu Balinese and people of the Philippines, which suggested that the weaving of coconut fronds has pre-Islamic origin. It was linked to the local Hindu ritual on venerating Dewi Sri, the Javanese goddess of rice. The Balinese Hindus still weaved the Cili fronds effigy of Dewi Sri as an offering, as well as weaving tipat fronds during Kuningan Balinese Hindu holy day.
According to Javanese traditions, the Indonesian lebaran tradition was first started when Sunan Bonang, one of Wali Songo of Tuban in 15th-century Java, calls for the Muslims to elevate the perfection of their Ramadhan fast by asking forgiveness and forgiving others' wrongdoings. The tradition on preparing and consuming ketupat or kupat in Javanese language during lebaran is believed to be introduced by Raden Mas Sahid or Sunan Kalijaga, one of Wali Songo (nine Muslim saints) that spread Islam in Java. Sunan Kalijaga introduced the lebaran ketupat ritual on 8 Shawwal, a week after Eid ul-Fitr and a day after six day Shawwal fast. It is believed that it contains appropriate symbolism; kupat means ngaku lepat or "admitting one's mistakes" in Javanese language, in accordance to asking for forgiveness tradition during lebaran. The crossed weaving of palm leaves symbolizes mistakes and sins committed by human beings, and the inner whitish rice cake symbolize purity and deliverance from sins after observing Ramadhan fast, prayer and rituals. Other than Java, the tradition on consuming ketupat during Eid ul-Fitr is also can be found throughout Indonesia; from Sumatra, Kalimantan, Sulawesi, Nusa Tenggara.


== Cultural significance ==

In various places in Indonesia, there is a ceremony called Lebaran Ketupat, which is observed after the conclusion of an extra six days of fasting following Idul Fitri. In Lombok, West Nusa Tenggara, thousands of Muslims celebrated Lebaran Ketupat — or Lebaran Topat as it is locally called — by visiting the graves of Muslim ulamas before partaking in communal ceremonial activities, which includes music performances, ketupat cooking competitions, to shared meals where ketupat was served as the main dish. Side dishes at the events varied, ranging from plecing kangkung (stir-fried water spinach) to the local dish of Ayam Taliwang. In Central Java, Lebaran Ketupat is called Bada Kupat, and was celebrated by cooking and serving ketupat and lepet (steamed sticky rice cooked in plaited palm leaves) in Semarang. In Colo, Kudus Regency, a parade of gunungan (cone-shape offering) made of ketupat, lepet and other food items on the slope of Mount Muria near the grave of noted Muslim preacher Sunan Muria, was held to celebrate Bada Kupat, while on the slope of Mount Merapi in Boyolali Regency,the celebration featured a parade of livestock decorated with ketupat.
Among Hindu communities in Bali and Banyuwangi in East Java, ketupat is part of the offering and ritual of Kuningan festive celebration to conclude the Galungan holy days. During Galungan, Hindu families would create and erected penjor pole made of janur (young palm leaves), and then made some offerings to the Pura. Ten days after Galungan, the ceremony of Kuningan would be observed to conclude the religious holy days. To celebrate Kuningan, Balinese Hindu families would made tipat or ketupat first as offering, and then they would consume some ketupat afterwards.
There are some striking similarities between Javanese Muslim Lebaran and Balinese Hindu Galungan-Kuningan holy days, which ketupat is one of them. For example, the families would paid a visit to the grave of their family or ancestors prior of observing the holy day, and they would consume ketupat to conclude the religious festival. Although today in contemporary Indonesia, ketupat is strongly associated with Muslim celebration of Idul Fitri, this parallel phenomena suggested the pre-Islamic native origin of ketupat, as Native Indonesian ways to shows gratitude and to celebrate festivities by making and consuming certain kind of food.


== Varieties ==

There are many varieties of ketupat, with two of the more common ones being ketupat nasi and ketupat pulut. Ketupat nasi is made from white rice and is wrapped in a square shape with coconut palm leaves while ketupat pulut is made from glutinous rice is usually wrapped in a triangular shape using the leaves of the fan palm (Licuala). Ketupat pulut is also called "ketupat daun palas" in Malaysia.
Ketupat is also traditionally served by Indonesian and Malays at open houses on festive occasions such as lebaran or Idul Fitri (Hari Raya Aidilfitri). During Idul Fitri in Indonesia, ketupat is often served with either opor ayam (chicken in coconut milk), chicken or beef curry, rendang, sambal goreng ati (spicy beef liver), krechek (buffalo or beef skin dish), or sayur labu Siam (chayote soup). Ketupat or lontong is also used as the replacement of plain steamed rice in gado-gado, karedok, or pecel.
Among the Moro (Muslim) groups of the Philippines, ketupat is served with an array of dishes including tiyulah itum, rendang, ginataang manok, kurma and satay. It is served during special occasions such as Eid'l Fitr, Eid'l Adha and weddings.
Among Filipinos, pusô, as ketupat is locally known, is also traditionally used as a pabaon or a packed lunch, traditionally brought by workers, served with any selection of stews. Pusô is also widely eaten in the side streets of Cebu with pork or chicken skewers and other grilled selections.


== Derivative dishes ==
Other than replacing steamed rice or lontong in certain dishes, such as satay, gado-gado, and ketoprak, ketupat is also forming the essential part of other derivative dishes with certain recipes developed from it.


=== Ketupat sayur ===

One of popular street food in Indonesian cities is Ketupat sayur which literary means "ketupat in vegetables soup". Ketupat sayur is known in two popular versions; the Betawi version from Jakarta and the Padang version from West Sumatra. Ketupat sayur is popular as breakfast fare in Jakarta and Padang. It consist of ketupat served with sliced labu siam (chayote) and unripe jackfruit gulai in thin and spicy coconut milk soup, topped with cooked tofu and telur pindang (spiced boiled egg), and krupuk crackers. If lontong was used in the identical recipe, it is called lontong sayur instead.


=== Kupat tahu ===

Ketupat also used as main ingredient in Sundanese and Javanese dish kupat tahu, which is ketupat, tahu goreng (fried tofu), and bean sprouts served in peanut sauce topped with crispy krupuk crackers. Popular variants of kupat tahu includes Kupat tahu Kuningan from Kuningan Regency in West Java, Kupat Tahu Magelang from Magelang Regency, Central Java, and Kupat tahu Gempol from Surabaya, East Java.
Its Balinese version is called tipat cantok, which is sliced ketupat, vegetables, bean sprout, cucumber, and fried tofu mixed in peanut sauce which is made from ground fried peanuts, garlic, chili pepper, salt and tauco fermented soy paste.


== Other uses ==

In Hindu-majority Bali, ketupat is used as one of the temple offerings. In Java, among traditional Muslim abangan community, the woven empty or uncooked ketupat skin is often hung as an amulet to symbolize wealth and prosperity.
Because in Indonesia ketupat is strongly linked to Islamic Eid ul-Fitr, it is also used as decorations. The empty ketupat skin woven from colorful ribbons are used as decorations to signify this festive occasions, in the same fashions as bells to signify Christmas. Colorful ribbon ketupat are often used to decorate shopping malls, offices, or as decorations of gift parcels.


== Similar dishes ==
In Cambodia, a similar dish of pounded sticky rice wrapped in a pentagonal woven palm leaves is called katom (កាតំ) in Khmer. It is a non-traditional variant of num kom which uses banana leaves instead of palm. In Indonesia, similar dish of compressed rice in leaf container includes lepet, lontong, lemper, arem-arem and bacang.


== See also ==
Lontong
Onigiri
Zongzi


== References ==