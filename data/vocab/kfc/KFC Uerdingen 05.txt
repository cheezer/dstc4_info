KFC Uerdingen 05 is a German football club in the Uerdingen district of the city of Krefeld, North Rhine-Westphalia. The one time Bundesliga side enjoyed its greatest successes in the 1980s but now currently plays in the 5th division, Oberliga Niederrhein.


== History ==
The club was founded on 17 November 1905 as Fußball-Club Uerdingen 05. On 1 August 1919, following World War I, FC was joined by Sportvereinigung des Realgymnasiums Urdingen. During World War II from 1941–45 the club played as part of the combined wartime side Kriegspielgemeinschaft Uerdingen alongside VfB 1910 Uerdingen (which was known from 1910–19 as Sport-Club Preussen Uerdingen). That partnership continued after the war with the two clubs playing as Spielvereinigung Uerdingen 05. On 20 February 1948, VfB became independent again and in 1950 SpVgg resumed their original identity as FC Uerdingen 05.
In 1953, the club merged with Werkssportgruppe Bayer AG Uerdingen, the local worker's sports club of the chemical giant Bayer AG, becoming FC Bayer 05 Uerdingen. Bayer withdrew its sponsorship of the football team in 1995 at which time the club took on the name Krefelder Fußball-Club Uerdingen 05. Bayer continues to support the non-footballing departments of the club as Sport-Club Bayer 05 Uerdingen.

Uerdingen played in the amateur local leagues throughout their early history. By the early 1960s they had advanced as far as the Amateurliga Niederrhein (III) where they would play until 1971 when they stepped up into the Regionalliga West (II). The club then enjoyed a succession of strong finishes: a second-place result in 1974–75 earned them promotion to the top flight Bundesliga, where they finished dead last. After three seasons in the second tier 2. Bundesliga Nord, another second-place finish returned Uerdingen to the Bundesliga in 1979, this time for a two-year stay. The club would go on to enjoy its most successful years through the 1980s. They returned to the Bundesliga in 1983 and earned a best-ever third-place result there in 1986. Uerdingen also captured the DFB-Pokal (German Cup) in 1985 with a 2:1 victory over Bundesliga champions Bayern Munich in Berlin's Olympiastadion.
Legendary in the club's history from this time is their victory over Dynamo Dresden in the quarter-finals of the 1986 European Cup Winners Cup. Down 0–2 after the first leg away and behind 1–3 by half-time at home in the return leg, Uerdingen came storming back with six unanswered goals to win 7–3.
In 1987, Uerdingen also became the first club to win both the German under 19's and under 17's championship in the same season.
The team spent the first half of the 1990s as an "elevator crew" bouncing up and down between the Bundesliga and 2. Bundesliga. After the 1995 season Bayer withdrew its sponsorship of the football team which has suffered chronic financial difficulties ever since. Uerdingen took up their final year of play in the Bundesliga in the 1995–96 season as Krefelder Fußball-Club Uerdingen 05. By the turn of the millennium they had slipped through the second division and into third division play. The club's persistent financial problems led the DFB to deny them a license for play in the Regionalliga Nord (III) in 2003 despite a mid-table finish and they were relegated to the Oberliga Nordrhein (IV).
Veteran manager and Fortuna Düsseldorf legend Aleksandar Ristić was put in charge of the team as German football was reorganised with the introduction the new 3. Liga in 2008–09. KFC attempted to qualify for the restructured Regionalliga (IV), but failed in its attempt and was instead relegated to the Verbandsliga (VI) after finishing 13th.
In 2010–11, the club won the Verbandsliga and thus gained promotion to the NRW-Liga (V). It was the first promotion in 17 years. KFC finished 8th NRW-Liga and missed second consecutive promotion to Regionalliga West due to finishing behind VfB Hüls. After the disbanding of the NRW-Liga, KFC qualified for the Oberliga Niederrhein. It won a league championship at this level in 2013 and was promoted to the Regionalliga West but relegated back to the Oberliga again in 2015.
KFC has struggled with financial difficulties in recent years, and its efforts to raise money included auctioning on eBay the right to coach the squad for one match and inviting childhood fan Pete Doherty to a league match.


== Honours ==
The club's honours:
DFB-Pokal
Winners: 1985

UEFA Intertoto Cup
Participation: 1988, 1990, 1991, 1992

European Cup Winners' Cup
Semi-finals 1986

Verbandsliga Niederrhein
Champions: 2010–11

Oberliga Niederrhein
Champions: 2012-13

Lower Rhine Cup
Winners: 2001


=== Youth ===
German Under 19 championship
Champions: 1987

German Under 17 championship
Champions: 1987


== Recent seasons ==


== Current squad ==
As of 31 July 2015
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


== Manager history ==
07.01.1970 – 06.30.1977 Klaus Quinkert
07.01.1977 – 06.30.1979 Siegfried Melzig
07.01.1979 – 06.30.1981 Horst Buhtz
07.01.1981 – 06.30.1983 Werner Biskup
07.01.1983 – 06.30.1984 Friedhelm Konietzka
07.01.1984 – 06.30.1987 Karl-Heinz Feldkamp
07.01.1987 – 12.01.1987 Horst Köppel
12.08.1987 – 06.30.1989 Rolf Schafstall
07.01.1989 – 11.25.1990 Horst Wohlers
11.26.1990 – 06.02.1991 Friedhelm Konietzka
06.03.1991 – 05.13.1996 Friedhelm Funkel
05.14.1996 – 06.30.1996 Armin Reutershahn
07.01.1996 – 06.30.1997 Hans-Ulrich Thomale
07.01.1997 – 09.29.1998 Jürgen Gelsdorf
09.30.1998 – 03.28.1999 Henk ten Cate
03.28.1999 – 06.30.1999 Ernst Middendorp
07.01.1999 – 10.31.1999 Herbert Schäty
11.01.1999 – 06.30.2000 Peter Vollmann
07.01.2000 – 06.30.2002 Jos Luhukay
07.01.2002 – 05.13.2004 Claus-Dieter Wollitz
07.01.2004 – 06.30.2006 Wolfgang Maes
07.01.2006 – 06.30.2007 Jürgen Luginger
07.01.2007 – 03.22.2008 Aleksandar Ristić
03.24.2008 – 06.30.2008 Klaus Berge
07.01.2008 – 08.11.2008 Richard Towa
08.11.2008 – 18.09.2009 Uwe Weidemann
18.09.2009 – 30.03.2010 Wolfgang Maes
31.03.2010 – 17.05.2010 Edgar Schmitt
10.06.2010 – 05.11.2011 Peter Wongrowitz
15.11.2011 – 24.05.2012 Jörg Jung
26.05.2012 – 31.06.2012 Erhan Albayrak & Ronny Kockel
01.07.2012 – 28.03.2014 Eric van der Luer
28.03.2014 – 14.04.2014 Erhan Albayrak
14.04.2014 – 22.04.2014 Ersan Tekkan
22.04.2014 – 18.05.2015 Murat Salar
Since 18.05.2015 Horst Riege


== References ==


== External links ==
Official website
The Abseits Guide to German Soccer