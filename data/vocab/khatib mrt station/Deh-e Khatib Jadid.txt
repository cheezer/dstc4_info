Deh-e Khatib Jadid (Persian: ده خطيب جديد‎, also Romanized as Deh-e Khaţīb Jadīd; also known as Deh-e Khaţīb and Deh Khaţīb) is a village in Keybar Rural District, Jolgeh Zozan District, Khvaf County, Razavi Khorasan Province, Iran. At the 2006 census, its population was 467, in 102 families.


== References ==