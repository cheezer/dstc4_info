In Islam, a khatib or khateeb (Arabic خطيب khaṭīb) is a person who delivers the sermon (khuṭbah) (literally "narration"), during the Friday prayer and Eid prayers.
The khatib is usually the Imam (prayer leader), but the two roles can be played by different people. There are no requirements of eligibility to become a khatib, although the person must be a male who has attained the age of puberty. It is also required that the khatib be in a state of physical purity (wuḍūʼ).


== See also ==
Khattab
Khutba


== References ==