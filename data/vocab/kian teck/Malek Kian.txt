Malek Kian (Persian: ملك كيان‎; also Romanized as Malek Kīān, Malek Keyān, Maliakian, and Malyakan) is a city in the Central District of Tabriz County, East Azerbaijan Province, Iran. At the 2006 census, its population was 1,066, in 269 families.


== References ==