Kim may refer to:


== Names ==
Kim (given name)
Kim (surname), including, but not limited to:
Kim (Korean name)
Kim Dynasty (disambiguation), several dynasties

Kim, Vietnamese form of Jin (surname)


== Places ==
Kim, Colorado, United States
Camp Kim, Seoul, South Korea, a military facility
Kimberley Airport, IATA code KIM
Kim, a town near Surat, Gujarat, India


== Media ==
Kim (novel), by Rudyard Kipling
Kim (1950 film), a 1950 American adventure film based on the novel
Kim (1984 film), a 1984 British film based on the novel

"Kim" (M*A*S*H), a 1973 episode of the American television show M*A*S*H
"Kim" (song), by Eminem


== Other uses ==
Khalifa Islamiyah Mindanao, Filipino terrorist organization
Kim (food)
Kim people
Kim language
Tropical Storm Kim (disambiguation), eight tropical Pacific Ocean cyclones
KIM-1, a microcomputer
Kingdom Identity Ministries, a Christian organisation


== See also ==
Kim's Game
Kem (disambiguation)
Khim
KIMM (disambiguation)
Kimberly (disambiguation)