The arc kite or twinskin kite is a type of traction kite designed and patented by Peter Lynn. It is a very stable, safe and secure type of powerkite. It can be used for all kinds of kite powered sports, for example: kiteboarding, landboarding, kite buggying or snowkiting. The shape of the kite is similar to a C shaped leading edge inflatable kite, however the construction is similar to a foil kite. These kites also fall into a category of foils called "closed-cell inflatables", meaning that the ram-air inlets on the leading edge of the kite are normally closed by flaps that act as one-way valves to maintain internal air pressure. It is this feature that makes the kite useful for kitesurfing since, unlike standard open-cell foils, if the kite crashes on the water, it will stay inflated and float long enough for the rider to recover and re-launch.


== History ==
1999-2000 S-Arc
2002 F-Arc
2003 Guerilla & Phantom
2004 Bomba & Guerilla II
2005 Venom
2006 Venom II & Vortex
2007 Scorpion
2008 Synergy
2009 Charger
2012 Phantom II
2013 Charger 2013


== Styles ==
Depending on the specific style of the kite its suitable better for different usages, however, each of the kites can be used on all terrain.


=== Land based kites (highest aspect ratio) ===
Kites which are not as easy to water relaunch, but have better depower and upper wind range.
Phantom
Scorpion
Phantom II


=== Water based low aspect ratio ===
S-Arc
Bomba
Vortex


=== Water based high aspect ratio ===
Better low wind performance,
F-Arc
Guerila I & Guerilla II
Venom I & Venom II
Synergy
Charger


== References ==
Official web site