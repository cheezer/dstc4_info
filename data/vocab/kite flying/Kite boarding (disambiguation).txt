Kite boarding encompasses various sports which make use of a kite for power and a board for support.
Kitesurfing, a water based, kite powered sport
Snowkiting, a snow based, kite powered sport
Kite boarding, a snow based, kite powered sport using a snowboard
Kite skiing, a snow based, kite powered sport using snow skis

Kite landboarding, a land based, kite powered sport using a four wheeled board


== See also ==
Windsport