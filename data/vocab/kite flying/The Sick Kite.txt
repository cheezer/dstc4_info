The Sick Kite is one of Aesop's Fables and is numbered 324 in the Perry Index.


== Versions ==
Greek versions of this fable are told of the raven (κοραξ) while it is called a kite (milvus) in Mediaeval Latin sources. The bird is ill and asks its grieving mother to pray in the temples on its behalf. The mother replies that since it was a robber of the sacrifices there, religious observance would now be of no use. The fable appears in the collection of William Caxton and in many others, generally with a reflection on the uselessness of death-bed repentance.
In the 1546 edition of the Emblemata by Andrea Alciato the story is modified. There the bird vomits and is told by its parent that it is losing nothing of its own, since all it has eaten was stolen. The fable is used to illustrate the Latin proverb male parta, male dilabuntur (ill-gotten, ill-spent). A sceptical variation on the theme directed against religious observance later appeared in Gotthold Ephraim Lessing's collection of prose fables (1759). 'The fox, observing that the raven plundered the altars of the gods, and that he supported himself from their sacrifices, said to himself: I should like to know whether the raven partakes of their sacrifices because he is a prophetical bird; or whether he is deemed a prophetical bird because he is so insolent as to partake of the sacrifices?'


== References ==


== External links ==
Book illustrations from the 16th-19th centuries