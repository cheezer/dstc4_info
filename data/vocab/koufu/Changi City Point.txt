Changi City Point (Chinese: 樟城坊) is a shopping mall located in the vicinity of Changi Business Park, Singapore directly opposite Expo MRT station. This mall is renowned for containing factory outlets of various brands such as Adidas, Nike, Lacoste and Pedro. There is also a landscaped themed rooftop garden together with a wet and dry playground on the topmost floor on level 3. Anchor tenants do include Cold Storage, Koufu and Challenger.


== §References ==


== §External links ==
Official website