Wu Yi (Chinese: 武乙) was king of the Shang dynasty of ancient China from 1147 to 1112 BC. His given name was Qu (瞿). According to the Bamboo Annals, his capital was at Yin. He was a son of his predecessor Geng Ding and father of King Wen Ding.
In the 21st year of his reign, the Zhou leader Koufu (口父) died.
In the 24th year of the regime of Wu Yi, Zhou attacked Cheng (程) at Bi (毕) and defeated Bi.
In the 30th year, Zhou attacked Yiqu (义渠) and captured the king of Yiqu. According to Sima Qian, the King of Yiqu has two sons by different mothers; after the king died, they fought each other for throne only to have Zhou defeat them both and absorb the territory of Yiqu.
In the 34th year of Wǔ Yǐ’s reign, King Ji of Zhou came to the capital to worship and was rewarded with 30 pieces of jade and 10 horses.
In the 35th year of Wǔ Yǐ’s reign, Ji attacked the Guirong (鬼戎) at Xiluo (西落). According to Sima Qian, he captured 20 kings of that tribe. In the same year, Wu Yi went hunting between the Yellow and Wei Rivers and was killed by lightning. According to the Book of Documents, this was blamed on Wu Yi’s grave impiety. Once, he was supposed to have carved a wooden statue of "the God of Heaven" and had one of the priests to represent it 令人為行. He played liubo with the idol and had it lose three times in a row, then destroyed it. Another time, he filled a leather bag with blood and hung it high in the air, then shot it with arrows; he called this "shooting Heaven". He then blasphemed by saying the god of thunder and lightning was nothing.


== References ==