Heartland Mall (Chinese: 心邻坊) is a shopping mall in Singapore. A relatively small shopping mall in comparison with other malls, it serves the neighbourhood of Kovan. It consists of four floors, with the fourth level occupied only by tuition centres. It is located at Block 205 Hougang Street 21, just next to Kovan MRT station. The mall is situated in an area known as Kovan City (Chinese: 高文城) which used to be Hougang Town Centre, but the town centre was moved to Hougang Central when neighbourhoods sprung up further north.


== Shops ==
Notable shops in Heartland Mall include Popular, Old Chang Kee, Prima Deli, KFC, Sukiya, Watson's and Sakae Sushi. The majority of the first floor is occupied by Cold Storage, a local supermarket chain.


== References ==


== External links ==