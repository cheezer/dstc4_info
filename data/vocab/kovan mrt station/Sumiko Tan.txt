Sumiko Tan (born 1963) is the news editor of the Home Section for The Straits Times, a Singapore-based broadsheet newspaper. She was formerly the editor of The Sunday Times and magazines team.
Tan's father is Chinese Teochew and her mother is Japanese. She completed her GCE 'A'-Level education at Anglo-Chinese Junior College in 1981, later describing her two years there as "very happy ones", and went on to attend the National University of Singapore. On graduating with a B.A., she joined The Straits Times as a reporter in 1985. Writing in The Sunday Times in 2013, Tan said that she had lived in the Kovan neighbourhood all her life, though the area was not popularly known as Kovan in her earliest years: "People called it the Hougang 6th Milestone in Teochew." 
Tan married her junior college school mate, Quek Suan Shiau, an electrician and chess teacher based in Wales, United Kingdom, in 2010. Referred to as "H", he has featured in a number of Tan's newspaper articles.


== Works ==
Sumiko Tan is the author of thirteen non-fiction books, including some co-authored:
2013: 48 Values from the News: The Straits Times Guide to Building Character (editor; co-editor Serene Goh), Straits Times ISBN 9789810739263
2005: Ngee Ann Kongsi: Into the Next Millennium Ngee Ann Kongsi ISBN 9810411405
2000: The Singapore Parliament: The House We Built Parliament of the Republic of Singapore ISBN 9812321446
1999: Home, Work, Play Urban Redevelopment Authority, Singapore ISBN 9810417063
1998: Lee Kuan Yew: the Man and His Ideas (with Han Fook Kwang and Warren Fernandez), Singapore Press Holdings ISBN 9812040498
1996: Kim Seng: A Reflection of Singapore's Success Kim Seng Publication Committee, Singapore ISBN 9810085761
1994: First and Foremost: training technologists for the nation - forty years of the Singapore Polytechnic Singapore Polytechnic ISBN 981005419X
1993: Chai Chee Revisited (co-author Michael Liew) Kampong Chai Chee CCC, Singapore
1992: The Winning Connection: 150 Years of Racing in Singapore Bukit Turf Club, Singapore ISBN 9810031777
1992: Sisters in Crime Times Books International, Singapore ISBN 9812043152
1991: Hijack! SQ117: the untold story Heinemann Asia ISBN 9971642476
1990: Streets of Old Chinatown, Singapore Page Media, Singapore ISBN 9810017693
1990: True Crime: Murder & Rape from the Police Files Times Books International, Singapore ISBN 9812041788


== References ==