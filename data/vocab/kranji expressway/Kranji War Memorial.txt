The Kranji War Memorial (Chinese: 克兰芝阵亡战士公坟; Malay: Tanah Perkuburan Perang Kranji; Tamil: கிராஞ்சி போர் நினைவு) is located at 9 Woodlands Road, in Kranji in northern Singapore. Dedicated to the men and women from United Kingdom, Australia, Canada, Sri Lanka, India, Malaya, the Netherlands and New Zealand who died defending Singapore and Malaya against the invading Japanese forces during World War II, it comprises the War Graves, the Memorial Walls, the State Cemetery, and the Military Graves.


== Overview ==
The War Memorial represents the three branches of the military - the Air Force, Army and Navy. The columns represent the Army, which marches in columns, the cover over the columns is shaped after of the wings of a plane, representing the Air Force, and the shape at the top resembles the sail of a submarine, representing the Navy.
The Memorial's walls inscribe over 24,000 names of allied servicemen whose bodies were never found, spread over both sides of 12 columns of the war memorial itself. On the Kranji War Memorial the names of 191 Canadian airmen are inscribed.
The grounds of the memorial is set on a hilly terrain with views around the largely undeveloped landscape, although signs of urbanity are clearly visible further afield. The modern skyline of Johor Bahru in Malaysia is also clearly visible. The grounds are immaculately maintained by the Commonwealth War Graves Commission, and accessible only from Woodlands Road, the same road that the invading Japanese Imperial Guards had marched down on 9 February 1942.
Kranji War Memorial and Cemetery has been included into the photographic archive by the War Graves Photographic Project in association with the Commonwealth War Graves Commission. Each individual grave has been recorded together with each and every column on the memorials.


== Kranji War Cemetery ==

The War Cemetery is the final resting place for 4,458 allied servicemen in marked graves laid out in rows on maintained and manicured lawns. Over 850 of these graves are unidentified.
Towards the north end of the cemetery grounds is the State Cemetery and burial site of Inche Yusuf bin Ishak and Benjamin Henry Sheares, the first and second Presidents of Singapore. To the west are the Military Graves for Commonwealth soldiers who died during the Konfrontasi and Malayan Communist Insurgency periods. 69 Chinese servicemen who served as members of the Commonwealth forces and who were killed by the Japanese in February 1942 were buried at the Chinese Memorial. There are also 64 burials for World War I, including special memorials for three men who were buried in civil cemeteries in Singapore and Saigon, and whose graves were impossible to locate till this day.
Previously a hospital burial ground during the Japanese Occupation period, it became a military cemetery at the end of the war. Military servicemen buried elsewhere in Singapore were exhumed and reburied at the memorial. The Indian Soldiers' were not buried at the Kranji War Memorial as they were supposed to be cremated.Instead their names were engraved on the memorial walls.


== Gallery ==


== See also ==
The Cenotaph, Singapore
Former Indian National Army Monument
Civilian War Memorial


== References ==


== External links ==
Tourism board information
Peter Chou's PBase Gallery Kranji War Memorial
Kranji War Cemetery at Find a Grave
Kanji Military Cemetery at Find a Grave