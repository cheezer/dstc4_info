The Singapore Racecourse is a venue for Thoroughbred horse racing in the Kranji area of Singapore. Built and operated by the Singapore Turf Club, it opened in 2000 as a modern replacement for the Bukit Timah Race Course.
The Singapore Racecourse hosts a number of important domestic races as well as two major international events, the Singapore Airlines International Cup and the KrisFlyer International Sprint.


== References ==
Race details at the Singapore Turf Club official website.