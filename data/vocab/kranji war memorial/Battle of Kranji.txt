The Battle of Kranji was the second stage of the Empire of Japan's plan for the invasion of Singapore during the Second World War. On 9 February 1942 the Imperial Japanese Army assaulted the north-western front of the British colony of Singapore. Their primary objective was to secure a second beachhead after their successful assault at Sarimbun Beach on 8 February, in order to breach the Jurong-Kranji defence line as part of their southward thrust towards the heart of Singapore City. Defending the shoreline between the Kranji River and the Johor–Singapore Causeway was the Australian 27th Brigade, led by Brigadier Duncan Maxwell, and one irregular company. On 10 February the Japanese forces suffered their heaviest losses while moving up the Kranji River, which caused them to panic and nearly aborted the operation. However, a series of miscommunications and withdrawals by Allied forces in the ensuing battles allowed the Japanese to swiftly gain strategic footholds, which eventually led to the fall of Singapore on 15 February 1942.


== Background ==
The terrain around Kranji was primarily mangrove swamps and tropical forest intersected by streams and inlets. The shoreline between the Kranji River and the Johor–Singapore Causeway, nearly four kilometers long, was defended by the Australian 27th Brigade, led by Australian Brigadier Duncan Maxwell. The 27th Infantry Brigade consisted of three battalions—the 2/30th, 2/29th, and 2/26th and was supported by the 2/10th Field Artillery Regiment, as well as one platoon from the 2/4th Machine Gun Battalion.
They were supported by one company from Dalforce (named after its commander, Lieutenant-Colonel John Dalley of the Malayan Police Special Branch), a local Chinese militia consisting of Communists, Nationalist supporters, and other volunteers. As the war intensified, the Dalforce volunteers were given only three to four days of training and sent to the war front with elementary weapons. Lacking uniforms, the volunteers improvised by wearing a red triangle on their blue shirts to avoid being mistaken for Japanese by the Australians.
The Allied forces at Kranji were to be assaulted by the Imperial Guards Division led by Major General Takuma Nishimura. 400 Imperial Guards had landed and taken Pulau Ubin, an island in the north-east of Singapore, in a feint attack on 7 February, where they encountered minimal resistance.


== Battle ==


=== 9 February 1942: Japanese landings ===
On 9 February, two divisions of the Japanese Twenty Fifth Army, led by Lieutenant General Tomoyuki Yamashita, landed on the northwestern coast of Singapore, in the Sarimbun area. Yamashita's headquarters (HQ) was in the Sultan of Johor's palace on Istana Bukit Serene, which offered him and his officers a bird's eye view of virtually every key target in the northern sector of Singapore Island, only 1.6 kilometres (one mile) across the Straits of Johor. Sultan Ibrahim's palace was not fired upon by the British because any damage caused would have extensive repercussions for British-Johor ties.
The primary objective of the Japanese at Kranji was to capture Kranji village; this would let them repair the demolished Causeway in order to facilitate easy flow of reinforcements and supplies down the roads of Woodlands and Mandai, and to the rest of the island for their vanguard force. Once the leading wave of Japanese was safely ashore, the massed Japanese artillery switched their fire to the defensive positions at Kranji. Telegraph and telephone communications were destroyed in the bombardment and communications between the front line and command HQ were broken. At 8:30pm that night, the men of the Imperial Guards Division began the crossing from Johor in special armoured landing-crafts, collapsible boats and by swimming.


=== 10 February 1942: Heavy losses ===

In the early hours of 10 February, Japanese forces suffered their heaviest losses during the Battle of Singapore. While moving up the Kranji River, advance landing parties from the 4th Regiment of the Imperial Guard Division found themselves under heavy fire from Australian machine gunners and mortar teams. They also found themselves surrounded by oil slicks, which had been created by Allied personnel emptying the nearby Woodlands oil depot, to prevent its capture. A scenario feared by Yamashita came to pass by accident; the oil was set alight by Allied small arms fire, causing many Japanese soldiers to be burnt alive. Sustaining heavy losses, Nishimura requested permission to abandon the operation. However, Yamashita denied the request.
Maxwell, who had limited communications with his division headquarters, was concerned that his force would be cut off by fierce and chaotic fighting at Sarimbun and Jurong to the south west, involving the Australian 22nd Brigade. Maxwell's force consequently withdrew from the seafront. This allowed the Japanese to land in increasing strength and take control of Kranji village. They also captured Woodlands, and began repairing the causeway, without encountering any Allied attacks.
Japanese light tanks, which had good buoyancy, were towed across the straits to Lim Chu Kang Road where they joined the battle at dusk. With reinforced troops and tanks advancing down Choa Chua Kang Road, the Australian troops were no match for the tanks and fled to the hills of Bukit Panjang. The 5th Division (Imperial Japanese Army) captured Bukit Timah village by the evening of 11 February.


==== Jurong-Kranji defence line ====

Lieutenant-General Arthur Percival, General Officer Commanding of HQ Malaya Command, drew a defence perimeter covering Kallang aerodrome, MacRitchie and Peirce reservoirs and the Bukit Timah supply depot area to ensure the integrity of the city's defence. One line of the north-western defence perimeter was the Jurong-Kranji defence line, a narrow ridge connecting the sources of the Jurong and the Kranji Rivers, forming a natural defence line protecting the north-west approach to the Singapore City. (Its counterpart was the Serangoon Line, which was sited between Kallang Airfield and Paya Lebar village on the eastern part of Singapore). The troops were to defend this Line strongly against the invading Japanese force. The Line was defended by the 44th Indian Infantry Brigade which covered milestone 12 on Jurong Road, the 12th Indian Infantry Brigade and the reinforced 22nd Australian Brigade which guarded the northern part of the Line and maintained contact with the 44th Indian Brigade. The 15th Indian Infantry Brigade was re-positioned near Bukit Timah Road to guard the island's vital food and petrol supplies. A secret instruction to protect this area was issued to Percival's generals.


==== Miscommunication ====
Percival's secret orders to withdraw to the last defence line around the city only if necessary were misunderstood by Maxwell, who took this to be an order for an immediate withdrawal to the Line. As a result, the 44th Indian Infantry Brigade, the 12th Indian Infantry Brigade and the 22nd Australian Brigade, reinforced after their withdrawal from Sarimbun beach in the north-west, abandoned the Line on 10 February. Fearing that the large supplies depot would fall into Japanese hands should they make a rush for Bukit Timah too soon, General Archibald Wavell, Allied commander-in-chief of the Far East sent an urgent message to Percival:

It is certain that our troops in Singapore Island heavily outnumber any Japanese who have crossed the Straits. We must destroy them. Our whole fighting reputation is at stake and the honour of the British Empire. The Americans have held out in the Bataan Peninsula against a far heavier odds, the Russians are turning back the picked strength of the Germans. The Chinese with an almost lack of modern equipment have held the Japanese for four and a half years. It will be disgraceful if we yield our boasted fortress of Singapore to inferior enemy forces.


== Aftermath ==

By 11 February, the Jurong-Kranji Defence Line was left undefended which allowed the Japanese forces to sweep through the Line to attack Bukit Timah. On the same day, Percival finally moved his Combined Operations Headquarters in Sime Road to the underground bunker, The Battle Box at Fort Canning.
Despite their fighting spirit, the Dalforce fighters suffered from poor training and the lack of equipment. A further blow was delivered when the 27th Australian Brigade withdrew southwards. As a result, the Japanese established a stronghold in the northern Woodlands area and secured a relatively easy passage into the island. General Wavell left Singapore for Java early on 11 February and sent a cable to British Prime Minister Winston Churchill in London on his assessment of the war front in Singapore:

Battle for Singapore is not going well... I ordered Percival to stage counter-attack with all troops possible... Morale of some troops is not good and none is as high as I should like to see... The chief troubles are lack of sufficient training in some reinforcing troops and an inferior complex which bold Japanese tactics and their command of the air have caused. Everything possible is being done to produce more offensive spirit and optimistic outlook. But I cannot pretend that these efforts have been entirely successful up to date. I have given the most categorical orders that there is to be no thought of surrender and that all troops are to continue fighting to the end...

By 12 February, the Imperial Guards had captured the reservoirs and Nee Soon village. The defending troops, by this time, were badly shaken. Thousands of exhausted and frightened stragglers left the fighting to seek shelter in large buildings. On the same night, British forces in the east of the island had begun to withdraw towards the city.
On 13 February, the Japanese 5th Division continued its advance and reached Adam and Farrer Roads to capture the Sime Road Camp. Yamashita moved his HQ forward to the bomb-damaged Ford Factory in Bukit Timah. Heading southwards, the Japanese 18th Division advanced into Pasir Panjang, where the last major battle of Singapore would be fought with the Malay Regiments at Bukit Chandu.


=== Commemoration ===
In 1995, the former battle sites of Kranji and the defence line were gazetted by the National Heritage Board as two of the eleven World War II sites of Singapore.


== See also ==

Malayan Campaign
Malaya Command
Japanese order of battle during the Malayan Campaign
Kent Ridge Park
Ee Hoe Hean Club


== References ==


=== Notes ===


=== Bibliography ===