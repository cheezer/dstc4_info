Kranji Reservoir (Chinese: 克兰芝蓄水池; Malay: Empangan Air Kranji) is a reservoir in the northern part of Singapore, near the Straits of Johor. It was a former freshwater river that flowed out into the sea that was dammed at its mouth to form a freshwater reservoir. It can also be classified as an estuary. The dam has a road bridging the two banks, and now prevents the sea from coming in, and is home to a marsh. The former Kranji River has three main tributaries - Sungei Peng Siang, Sungei Kangkar and Sungei Tengah.


== Historical Significance ==
Although known as a place for fishing and picnicking, the Kranji Reservoir Park is a historical site. A war memorial plaque tells visitors of the historical and violent past of this place. It was here that the Battle of Kranji took place. The Japanese army invaded Kranji in their plan to take Singapore during the Second World War.


== Recreation ==
In 1985 it became permissible to fish in the Kranji Reservoir Park. The Park now has two fishing areas, named A and B. The greenery around the park has made it a favorite haunt of picnickers.


== Incidents ==
In 2002, a mother and son were picking shells from the shores of Kranji Reservoir when the PUB opened up the barrage to let water into the reservoir from the sea. Both of them died. PUB later clarified that a sound was made before the barrage was opened to warn people not to get too near the reservoir.


== References ==