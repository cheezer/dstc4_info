The Kranji Expressway (Abbreviation: KJE; Chinese: 克兰芝高速公路; pinyin: Kèlánzhī Gāosù Gōnglù; Tamil: கிராஞ்சி விரைவுச்சாலை; Malay: Lebuhraya Kranji) in Singapore connects from the BKE in Bukit Panjang and travels south-west to join with the PIE in Jurong West. Construction of the expressway started in 1990 and was completed in 1994. The expressway is also the second shortest of all the expressways at about 8 kilometres (5.0 mi).
It replaces some roads - Hong Kah Road, Hong Kah Lane, Jalan Beka, Jalan Pelawan, Jalan Jelawi, Jalan Dedali, Lorong Merawan, Lorong Kerubut, Jalan Beras, Jalan Bungar, Jalan Buey, part of Jalan Sabit, Lorong Puyu, Lorong Dengkes, Jalan Ara, Jalan Chapa, Lorong Jelubu and Kadlin Lane.


== List of exits ==


== External links ==
Traffic camera monitoring the KJE
Infopedia article on Kranji Expressway