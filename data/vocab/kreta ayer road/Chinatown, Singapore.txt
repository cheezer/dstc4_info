Singapore's Chinatown is an ethnic neighbourhood featuring distinctly Chinese cultural elements and a historically concentrated ethnic Chinese population. Chinatown is located within the larger district of Outram.
As the largest ethnic group in Singapore is Chinese, Chinatown is considerably less of an enclave than it once was. (Note: The name Chinatown is given by the British and continues to be used by Singaporeans today, although the area retains the Chinese name Niu Che Shui.) However, the district does retain significant historical and cultural significance. Large sections of it have been declared national heritage sites officially designated for conservation by the Urban Redevelopment Authority.


== Geography ==
Singapore's Chinatown is composed of several precincts – Kreta Ayer, Telok Ayer, Tanjong Pagar, Bukit Pasoh and Ann Siang Hill.
Kreta Ayer – Kreta Ayer (meaning "water cart" similar to the Chinese name of "Niu Che Shui"; would be spelled "kereta air" in modern Malay) is considered by many to be the heart of Singapore's Chinatown. With attractions such as Chinatown Heritage Centre, Chinatown Food Street and Chinatown Night Market, and Kreta Ayer Wet Market, Kreta Ayer is both a popular tourist destination as well as a favoured location for local food.
Telok Ayer – The original focal point of settlement in Chinatown, Telok Ayer is home to many Chinese temples as well as Muslim mosques that have been around since the first days of Chinatown. More recently, restaurants and drinking holes have begun popping up on Telok Ayer's streets.
Tanjong Pagar – Once the center of operations for rickshaw pullers, Tanjong Pagar today is known for the large number of bridal salons that have set up shop along her rows of preserved pre-World War II shophouses. Singapore's tallest Housing and Development Board (HDB) flat, The Pinnacle@Duxton, also resides in Tanjong Pagar.
Bukit Pasoh – Known also as the "Street of Clans", Bukit Pasoh is the historic, and in many cases, current, home of several Chinese cultural and clan associations – unusual neighbours to the slew of boutique hotels and international restaurants that have sprung up.
Ann Siang Hill – Named after a wealthy Hokkien Chinese sawmiller who acquired the area to serve as his house and estate in the 1800s, the slopes of Ann Siang Hill hosts several European restaurants making it a popular hangout spot for foreigners working in the central business district nearby.


== History ==


=== Development ===

Wang Dayuan visited Singapore (then called Temasek or Dan Ma Xi) in 1330 and recorded that there was a Chinese community. This would make Singapore one of the oldest Chinatowns, as well as the largest.
Under the Raffles Plan of Singapore, the area originally was a division of colonial Singapore where Chinese immigrants tended to reside. Although as Singapore grew, Chinese immigrants settled in other areas of the island-city, Chinatown became overcrowded within decades of Singapore's founding in 1819 and remained such until many residents were relocated at the initiation of Singapore's governmental Housing Development Board in the 1960s.
In 1822, Sir Stamford Raffles wrote to Captain C.E. Davis, President of the Town Committee, and George Bonham and Alex L. Johnson, Esquires, and members, charging them with the task of "suggesting and carrying into effect such arrangements on this head, as may on the whole be most conducive to the comfort and security of the different classes of inhabitants and the general interests and welfare of the place..."
He went on to issue instructions, as a guide to the Committee, which included a general description of Singapore Town, the ground reserved by the government, the European town and principal mercantile establishments and the native divisions and "kampongs". These included areas for Bugis, Arabs, Marine Yard, Chulias, Malays, Markets and Chinese Kampongs, the present-day Chinatown. Raffles was very clear in his instructions and his guidelines were to determine the urban structure of all subsequent development. The "five-foot way", for example, the continuous covered passage on either side of the street, was one of the public requirements.
Raffles foresaw the fact that "it may be presumed that they (the Chinese) will always form by far the largest portion of the community". For this reason, he appropriated all of the land southwest of the Singapore River for their accommodation but, at the same time, insisted that the different classes and the different provinces be concentrated in their separate quarters and that these quarters, in the event of fire, be constructed of masonry with tiled roofs.
This thus resulted in the formation of a distinct section titled Chinatown. However, only when parcels of land were leased or granted to the public in and after 1843 for the building of houses and shophouses, did Chinatown's physical development truly begin.


=== Effects ===

The effects of diversity of Chinatown are still present. The Hokkiens (Fukiens) are associated with Havelock Road, Telok Ayer Street, China Street and Chulia Street, and the Teochew merchants are mostly in Circular Road, River Valley Road, Boat Quay and South Bridge Road. The ubiquitous Cantonese are scattered around South Bridge Road, Upper Cross Street, New Bridge Road and Bukit Pasoh Road. These days, the Hokkiens and Teochews have largely scattered to other parts of the island, leaving the Cantonese as the dominant dialect group in Chinatown.
The Chinese names of Pickering Street are Kian Keng Khau (mouth of the gambling houses) or Ngo Tai Tiahn Hok Kiong Khau (mouth of the five generations of the Tian Hok Temple).
Guilds, clans, trade unions and associations were all referred to as kongsi, a kind of Chinese mafia, although the literal meaning of the word is "to share". The so-called mafia is better translated as the secret and sinister hui. However, these secret societies, the triads, who themselves had suffered under the Manchus in China, provided support to the later immigrants to Singapore by paying their passage and permitting to pay it off by working.
There were the letter writers of Sago Street—the Chinese called this street Gu Chia Chwi Hi Hng Cheng (front of Kreta Ayer Theatre), but it was mainly associated with death—the sandalwood idols of Club Street and the complicated and simple food of Mosque Street; all rang to the sound of the abacus. Old women could be seen early in the mornings topping and tailing bean sprouts, the skins of frogs being peeled, the newly killed snakes being skinned and the centuries-old panaceas being dispensed by women blessed with the power of curing.
In the heart of this diverse Chinese community is an important temple for Singaporean Indians, the Sri Mariamman Hindu Tamil Temple, and mosques, Al-Abrar Mosque at Telok Ayer Street and Jamae Mosque at Mosque Street, as well as the Fukien Thian Hock Keng Chinese Temple of 1830 to 1842. These catered to the pockets of non-Chinese residents in the area and shows that despite efforts to segregate the early immigrants, they had no qualms living together, and side by side.


=== Etymology ===

The name Chinatown was used by the British while the locals went by these names: In Chinese, Singapore's Chinatown is known as Niu che shui (Chinese: 牛车水; pinyin: Niú chē shuǐ; literally: "bull-cart water") as a result of the fact that, because of its location, Chinatown's water supply was principally transported by animal-driven carts in the 19th century. The name is also echoed in the Malay name, Kreta Ayer, with the same meaning.


==== Street name origins ====

Mosque Street is named after Jamae Mosque, located on the South Bridge Road end of the street. The mosque was completed in 1830 by the Chulia Muslims from the Coromandel coast of South India but also used by the Malay Muslims living in the area. In the early years, Mosque Street was the site of ten stables.
Pagoda Street takes its name from the Sri Mariamman Temple. During the 1850s and 1880s, the street was one of the centres of slave traffic. It also had its share of coolie quarters and opium smoking dens. One of the traders was Kwong Hup Yuen who, it is thought, occupied No. 37, and after whom Pagoda Street is often referred to today.
Sago Lane and Sago Street got their name because in the 1840s there were a number of sago factories located there. Sago is taken from the pith of the rumbia palm and made into flour that is used for making cakes both sweet and savoury.Dead bodies are taken to Sago Lane.
Smith Street was probably named after Sir Cecil Clementi Smith, who was the Governor of the Straits Settlements between 1887 and 1893.
Temple Street refers to the Sri Mariamman Temple, which is located at the South Bridge Road end of the street. It was formerly known as Almeida Street after Joaquim d'Almeida, son of José D'Almeida, who owned some land at the junction of Temple Street and Trengganu Street. In 1908, the Municipal Commissioners changed its name to Temple Street to avoid confusion with other streets in Singapore which were also named after D'Almeida.
Trengganu Street, described as "the Piccadilly of Chinese Singapore" in the past, now forms the heart of the tourist belt in Chinatown. In Chinese, it is called gu chia chui wah koi, or "the cross street of Kreta Ayer". The crossing of streets refers to Smith Street and Sago streets. The street name is derived from Terengganu, a state in present day Peninsular Malaysia.


== Architecture ==

The street architecture of Chinatown's buildings, the shophouses especially, combine different elements of baroque architecture and Victorian architecture and do not have a single classification. Many of them were built in the style of painted ladies, and have been restored in that fashion. These styles result in a variety of different colours of which pastel is most dominant. Trengganu Street, Pagoda Street and Temple Street are such examples of this architecture, as well as development in Upper Cross Street and the houses in Club Street. Boat Quay was once a slave market along the Singapore River, Boat Quay has the most mixed-style shophouses on the island.
In 1843, when land titles were issued, the terraces in Pagoda Street (now with additions, mostly three-story) were born. They were originally back to back, an arrangement which made night soil collection difficult, but lanes were developed in between following the Singapore Improvement Trust (SIT) backlane orders of 1935.
The architectural character of many of the terraces in Chinatown is much more Italianate in style than those of, for instance Emerald Hill or Petain Road. Windows often appear as mere slits with narrow timber jalousies (often with adjustable slats). Fanlights over the windows are usually quite decorative and the pilasters and balconies and even the plasterwork and colours seem to be Mediterranean in flavour. The style was probably introduced by those early Chinese immigrants (both China-born and Straits-born) who had knowledge of the Portuguese architecture of Macau, Malacca and Goa. The Chettiars and Tamils from Southern India would also have been familiar with the European architecture there, although it is difficult to imagine how these people would have had a particularly strong influence on building in Chinatown.


== Transportation ==
The Mass Rapid Transit MRT serves the area at Chinatown MRT Station on the North East and Downtown Lines, in the middle of pedestrian-only Pagoda Street, and serves the vicinity, as well as several public bus routes which integrates it into Singapore's transportation system. Also nearby are the Tanjong Pagar MRT Station on the East West MRT Line; Outram Park MRT Station, an interchange between the East West Line and North East Line; and Clarke Quay MRT Station on the North East Line, as well as a bus terminal called New Bridge Road Bus Terminal.


== Politics ==
Chinatown is mainly in the Kreta Ayer-Kim Seng division of Tanjong Pagar Group Representation Constituency whose Member of Parliament is John Lim. Some other parts are under the Tanjong Pagar-Tiong Bahru division whose Member of Parliament is Ang Leng Yuk.


== Gallery ==


== See also ==
Chinatowns in Asia
Sylvia Sherry, author of Street of The Small Night Market set in Chinatown, Singapore.


== References ==

Norman Edwards, Peter Keys (1996), Singapore – A Guide to Buildings, Streets, Places, Times Books International, ISBN 9971-65-231-5
Victor R Savage, Brenda S A Yeoh (2003), Toponymics – A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1


== External links ==
Official Singapore Chinatown website
Chinatown Heritage Centre
 Chinatown, Singapore travel guide from Wikivoyage
Kreta Ayer on Visitsingapore.com
360° X 360° interactive VR of Chinatown
Chinatownology: Singapore Chinatown
Kreta Ayer Community Centre website
The Streets of Chinatown from Think Singapore
Living Wellness @ Chinatown Pagoda Street