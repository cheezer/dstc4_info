The 164th Division was a unit of the Wehrmacht in World War II. It was initially created as the 164th Infantry Division in November 1939 and took part in the invasion of Greece in April 1941. In January 1942, consolidating Axis seizure of the island during the Battle of Crete, the 164th was reorganized as Fortress Division Kreta (FDK). In August 1942 the division was split to form the smaller Fortress Brigade Kreta and the 164th Light Afrika Division (German: Leichte Afrika Division), with the former remaining in Crete and the latter sent to North Africa.
The 164th Light Afrika Division fought at El Alamein and took heavy casualties in the westward retreat. It was sent to Tripoli for rehabilitation, but the necessary resources were not available, so it was sent to Tunisia to build fortifications. It was almost entirely destroyed there, and the remnants were lost in general Axis surrender in May.


== Commanding officers ==


=== 164th infantry division ===
Generalleutnant Konrad Haase, 1 December 1939 – 10 January 1940
Generalleutnant Josef Folttmann, 10 January 1940 – 1 January 1942


=== 164th light infantry division ===
Generalleutnant Josef Folttmann, 18 July 1942 – 10 August 1942
Generalleutnant Carl-Hans Lungershausen, 10 August 1942 – 15 January 1943
Generalmajor Kurt Freiherr von Liebenstein, 15 January 1943 – 12 May 1943


== See also ==
Fortress Crete
Western Desert Campaign, North African Campaign
Division (military), Military unit
Wehrmacht, List of German divisions in World War II