Lily Tirtasana Neo (Chinese: 梁莉莉; born August 12, 1953) is a Singaporean medical practitioner and politician. A member of the People's Action Party (PAP), Neo is a Member of Parliament (MP) for the Kreta Ayer - Kim Seng Ward within Tanjong Pagar Group Representation Constituency (GRC). Born in Indonesia, Neo relinquished her Indonesian citizenship and has been a Singaporean for more than 35 years. Neo is married with two grown-up children who are both medical practitioners.


== Early life and education ==
Lily Tirtasana Neo was born in Medan, Indonesia on August 12, 1953.  Neo studied at Primary Thamrin Methodist School in Medan from 1961 to 1969. Thereafter, she attended Methodist High School in Penang and furthered her studies in Australia before graduating with MB Bch BAO; LRCPO and LRCSI Bachelor of Medicine, Bachelor of Surgery from the Royal College of Surgeons in Ireland in 1980. 


== Career ==
After obtaining her medical degree, Neo became a self-employed medical practitioner, a job she has kept since 1982. She is also a member of the People's Action Party. 
Neo entered politics in 1996 and was elected to Parliament as a Member of Parliament (MP) for Kim Seng within Kreta Ayer-Tanglin Group Representation Constituency (GRC) during the Singapore General Election in January 1997. Subsequently, she served as an MP for Kreta Ayer - Kim Seng in Jalan Besar Group Representation Constituency for two consecutive terms.
At the 2011 General Elections, she moved to Tanjong Pagar GRC and was re-elected to serve as an MP for Kreta Ayer - Kim Seng as there was no electoral opposition in Tanjong Pagar. 
Neo currently serves as Chairperson for Tanjong Pagar Town Council  and Deputy Chairperson for the Social and Family Development Government Parliamentary Committee.  Her current positions include serving as a Member for the Parliament House Committee and Transport Government Parliamentary Committee. 
Neo previously served as Chairperson for various Government Parliamentary Committees, Chairperson for Jalan Besar Town Council and Treasurer for PAP Women's Wing. 
Neo has supported social measures to support the low-income, particularly the elderly and young children. 


== Personal life ==
Neo is married to obstetrician and gynaecologist Ben Neo.  They have two adult children and three grandchildren. Neo is a Christian and enjoys playing the piano, swimming and reading.  


== See also ==
List of Singapore MPs
List of current Singapore MPs


== References ==