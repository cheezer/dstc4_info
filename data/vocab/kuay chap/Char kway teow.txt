Char kway teow, literally "stir-fried ricecake strips", is a popular noodle dish in Malaysia, Singapore, Brunei and Indonesia. The dish is considered a national favourite in Malaysia and Singapore.
It is made from flat rice noodles (河粉 hé fěn in Chinese) or kway teow (粿条 guǒ tiáo in Chinese) of approximately 1 cm or (in the north of Malaysia) about 0.5 cm in width, stir-fried over very high heat with light and dark soy sauce, chilli, a small quantity of belachan, whole prawns, deshelled blood cockles, bean sprouts and chopped Chinese chives. The dish is commonly stir-fried with egg, slices of Chinese sausage and fishcake, and less commonly with other ingredients. Char kway teow is traditionally stir-fried in pork fat, with crisp croutons of pork lard, and commonly served on a piece of banana leaf on a plate.
Char kway teow has a reputation of being unhealthy due to its high saturated fat content. However, when the dish was first invented, it was mainly served to labourers. The high fat content and low cost of the dish made it attractive to these people as it was a cheap source of energy and nutrients. When the dish was first served, it was often sold by fishermen, farmers and cockle-gatherers who doubled as char kway teow hawkers in the evening to supplement their income.


== Etymology ==
The term "char kway teow" is a transliteration of the Chinese characters 炒粿條, pronounced chhá-kóe-tiâu? in Min Nan (also known as Hokkien). The word kóe-tiâu (literally meaning "ricecake strips") generally refers to flat rice noodles, which are the usual ingredient in West Malaysia and Singapore. In East Malaysia, on the other hand, actual sliced ricecake strips are used to make this dish.
In popular transliterations, there is no fixed way of spelling chhá-kóe-tiâu, and many variants can be found: e.g., "char kueh teow", "char kueh tiao", etc.
Owing to the dish's popularity and spread to Cantonese-speaking areas, the term "char kway teow" has been corrupted into "炒貴刁" in Cantonese. This is pronounced as caau2 gwai3 diu1? in Cantonese and cháo guìdiāo ? in Mandarin. The term "貴 刁" has no real meaning, but its pronunciation in Cantonese and Mandarin is similar to "粿條" in Min Nan.
In Hong Kong, "char kway teow" is often known as "Penang char kway teow" (檳城炒粿條 or 檳城炒貴刁), indicating the dish's origin.


== Variations ==

As the dish has become increasingly popular, many cooks have come up with their own interpretations of the same basic main ingredient of ricecake strips/flat rice noodles fried with anything from eggs (chicken or duck), onions, garlic, prawns, cockles, Chinese sausage, chives, etc.
In the past, it was usual to stir-fry char kway teow in pork fat without eggs (which were, however, available on request). More recently, ordinary cooking oil is commonly used for health or religious reasons, and eggs have become a standard ingredient in the dish.
Versions of char koay teow prepared by Muslims in Malaysia will exclude pork fat and may include extra soy sauces and spices and the use of broader-width flat rice noodles. There are also vegetarian varieties that may or may not include eggs.
There are also "gourmet" versions of char kway teow, commonly found in Ipoh and Penang, where the dish may be prepared with more seafood, with crab meat and with duck eggs.
Char kway teow is also popular at takeaways in Australia and New Zealand.
In Myanmar, a variety called the Beik Kut kyae kaik (the Beik Scissor Cut) exists. It is popular in the southern coastal regions around the town of Myeik ("Baik" is the Burmese pronunciation) and in Yangon, the largest city in the country. It uses more pepper and seafood compared to the kway teow of Singapore and Malaysia. The rice noodles are slightly thinner and are stir-fried with boiled yellow peas, bean sprouts, squid and prawns, spring onions and dark sweet soy sauce. After being stir-fried, the noodles are cut with scissors (kut kyae in Burmese), thus its name. In many Asian fusion restaurants in America, such as the popular Cafe Asia chain, this dish is offered under the name Gway Tiao.
Many Southeast Asian restaurants in Hong Kong offer char kway teow as a Malay speciality although it is of Southeast Asian Chinese origin. The char kway teow served in Hong Kong is an entirely different dish: stir-fried Chinese-style flat rice noodles with prawns, char siu, onions, and bean sprouts, seasoned with curry and bright yellow in colour. In some places this is known as Fried "Good Dale", a transliteration of the characters "炒貴刁".
In Indonesia, there is a similar dish known as kwetiau goreng (Indonesian: fried flat rice noodles) and is served in Chinese restaurants, street side tent warung, and by traveling street hawkers' carts. This Indonesian version tastes mildly sweet with generous addition of kecap manis (sweet soy sauce), has spicier and stronger flavor with addition of sambal condiment, less oily, mostly halal which means uses no lard or pork, and normally incorporates beef or chicken to cater to the majority Indonesian Muslim population. However, some Chinese restaurants in Indonesia that mainly serve non-Muslim customers might use pork and pork fat.
In Vietnamese cuisine, a similar stir-fried noodle dish is called hủ tiếu xào. Thai cuisine has its own version called phat si-io.
In Singapore, some of best 'old school' char kway teow can be found in hawker centres. There are also healthier versions with more vegetables and less oil. Furthermore, the greens and bean sprouts gives off a fresh, crunchy texture that makes the dish taste even more unique from other dishes of the cuisine. This version is also common in Perth, Western Australia, which has a large expatriate Singaporean population.


== See also ==

Beef chow fun
Chinese noodles
Rice noodles
Shahe fen


== References ==
^ a b "Char Kway Teow". Tourism Malaysia. Archived from the original on 12 October 2014. Retrieved 9 March 2015. 
^ "Char Kway Teow – Penang – Sister". sigmatestudio.com. Retrieved 28 March 2010. 
^ "Fried Good Dale: A Translation Run Amok". DCist. 
^ "Singapore Food – YourSingapore". yoursingapore.com. 


== External links ==