Clorot, celorot, cerorot, or jelurut is a traditional sweet snack (kue or kuih) of sweet and soft rice flour cake with coconut milk, wrapped with janur or young coconut leaf in cone shape. It is a popular traditional sweet snack commonly found in Indonesia, Malaysia and Brunei.
In Java, it is known as clorot or celorot, and commonly associated with Javanese traditional jajan pasar (market munchies). In Bali and Lombok islands of Indonesia, it is known as cerorot. In Brunei and in the Malaysian states of Sabah in East Malaysia, it is known as jelurut.


== Ingredients and cooking method ==
Gula jawa (palm sugar), pandan leaf, salt and water are boiled until done and mixed with coconut milk. This sweet liquid then being poured upon rice flour and sago or tapioca flour, and mixed evenly. The janur or young coconut leaf rolled to form a long cone, similar to a small trumpet, secured and arranged upright. The thick-liquid sweet dough then filled into this coconut leaf cones until three-quarter full. Then the top section is filled with the mixture of coconut milk, rice flour and salt. These filled cones then being steamed for about 15 minutes until the dough inside the cone are cooked and hardened.


== References ==
^ a b c "Dari Celorot Sampai Engkak Ketan" (in Indonesian). Femina. Retrieved 19 June 2015. 
^ Abd. Latip Talib (2006). Beraraklah awan pilu (in Malay). Utusan Publications. pp. 127–. ISBN 978-967-61-1899-8. Retrieved 17 August 2013. 
^ a b "Clorot (Jawa)" (in Indonesian). Sajian Sedap. 15 December 2010. Retrieved 19 June 2015. 
^ "Kebudayaan (Brunei)" (in Malay). Papar District Office. Retrieved 17 August 2013. 


== External links ==
Clorot recipe
Video about Clorot in Javanese culture
Longest Jelurut Kuih