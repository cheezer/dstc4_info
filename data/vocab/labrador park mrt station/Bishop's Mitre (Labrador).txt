Bishop's Mitre is a mountain located 3 km (9,843 ft) east of Brave Mountain on the northern coast of Labrador in the Kaumajet Mountains. Noteworthy for the river carved down its middle, its appearance is more like a steep tower, which lies on the north point of Grimmington Island, between Seal Bight and Cod Bag Harbour.


== References ==