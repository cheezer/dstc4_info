Lakeside is a census designated place (CDP) in San Diego County, California. The population was 20,648 at the 2010 census, up from 19,560 as of the 2000 census.


== History ==

Lakeside was founded in 1886 when 6,600 acres of land surrounding the naturally occurring Lindo Lake were purchased by the El Cajon Valley Land Company, who immediately began to promote the new land as a town and built an 80-room Victorian-style inn, the Lakeside Hotel, at a cost of $50,000 (approximately $1,220,000 today). Three years later, in 1889, Lakeside became connected to the railroad system, and small businesses began to spring up, firmly establishing Lakeside as a bustling community. In 1904, John H. Gay bought the Lakeside Hotel and fenced off the park surrounding Lindo Lake, claiming both as part of his estate. He then proceeded to construct an automobile and horse racetrack around the lake, which became famous when Barney Oldfield set a new land speed record when visiting for the track's opening in 1907. The inn and racetrack became a popular gathering place for millionaires and celebrities and consistently drew large crowds by train to watch the races held there, but both were demolished upon Gay's wishes at his death in 1920.
Lakeside has long held a reputation as a "cowboy town" and "rodeo town," due to the rural setting, the prevalence of ranches and the abundant horse ownership in the area, as well as hosting an 8-acre (32,000 m2) permanent rodeo facility, the Lakeside Rodeo Grounds. The venue is manned and maintained by the El Capitan Stadium Association, an all-volunteer group who aims to assist and support the youth of Lakeside by donating all proceeds from facility rentals to local sports and service, as well as education grants. Notable rodeos include the PRCA-sanctioned Lakeside Rodeo, part of their California circuit and usually occurring on the last weekend in April, regularly drawing over 20,000 observers to the arena, as well as the Lakeside Optimists' Bulls Only Rodeo in July.
During the middle of the 20th century, Lakeside was home to significant Native American, Spanish-speaking and Filipino populations.
Many social and service groups are very active in Lakeside including VFW, Elks, Optimist, Soroptimist, 4-H, FFA, Boys and Girls Club, Boy Scouts, Girl Scouts, PLAY, United States Naval Sea Cadet Corps, youth soccer, baseball and football leagues, and the Cactus Park BMX track. The U.S. Navy maintains two large housing sites for military dependents in the town.
Today, protection of the town's history falls to the Lakeside Historical Society, which works primarily to retain and preserve the buildings in the historic former downtown along Maine Avenue, now removed from the central business hub.


== Recreation ==
While Lakeside was first named for the presence of the small, natural lake it was founded around, it is now home to a trio of large reservoirs in Lake Jennings, El Capitan Reservoir and San Vicente Reservoir. All three are kept stocked by the California Department of Fish and Wildlife with various types of fish ranging from largemouth bass to catfish and rainbow trout, and are popular fishing destinations for local and county residents. The latter two are also open to recreational water activities such as wakeboarding and jetskiing; however, San Vicente is currently closed to the public until at least 2014.
There are a number of parks with outdoor trails for hiking, biking and equestrian riding, most notably the River Park situated along the bank and riverbed of the San Diego River, the park surrounding Lindo Lake and Stelzer and El Monte County Parks. Also located in Lakeside is the trailhead for the climb to the summit of El Cajon Mountain, nicknamed locally as "El Capitan" for its resemblance to the famous Yosemite cliff, which dominates the view of the mountains northeast of Lakeside.
As well as hosting outdoor recreational opportunities, the town is also in close proximity to a number of Native American casino operations, the most notable of which is Barona Resort & Casino a few miles to the north. A full resort featuring hotel, buffet and golfing facilities, Barona is notable for having a gambling age restriction lower than most California casinos at 18 years old,. Other Native American casinos nearby include Viejas Casino, located to the east of Lakeside on Interstate 8, as well as the Golden Acorn casino further out along the interstate.


== Geography ==
Lakeside is in the western foothills of the Cuyamaca Mountains.
According to the United States Census Bureau, Lakeside has a total area of 7.3 square miles (19 km2). 6.9 square miles (18 km2) of it is land and 0.4 square miles (1.0 km2) of it (5.22%) is water.


=== Climate ===
According to the Köppen Climate Classification system, Lakeside has a warm-summer Mediterranean climate, abbreviated "Csa" on climate maps.


== Demographics ==


=== 2000 ===
As of the census of 2000, there were 19,560 people, 6,849 households, and 5,150 families residing in the census-designated place (CDP). The population density was 3,420.7 inhabitants per square mile (1,320.3/km²). There were 7,047 housing units at an average density of 1,232.4 per square mile (475.7/km²). The racial makeup of the CDP was 89.83% White, 0.75% African American, 1.10% Native American, 1.27% Asian, 0.28% Pacific Islander, 3.28% from other races, and 3.49% from two or more races. Hispanic or Latino of any race were 11.52% of the population.
There were 6,849 households out of which 39.4% had children under the age of 18 living with them, 57.9% were married couples living together, 12.4% had a female householder with no husband present, and 24.8% were non-families. 18.7% of all households were made up of individuals and 7.3% had someone living alone who was 65 years of age or older. The average household size was 2.83 and the average family size was 3.22.
In the CDP the population by age was 29.0% under the age of 18, 8.4% from 18 to 24, 30.8% from 25 to 44, 21.7% from 45 to 64, and 10.1% who were 65 years of age or older. The median age was 35 years. For every 100 females there were 96.5 males. For every 100 females age 18 and over, there were 94.2 males.
The median income for a household in the CDP was $48,910, and the median income for a family was $55,336. Males had a median income of $41,258 versus $29,375 for females. The per capita income for the CDP was $20,100. About 6.9% of families and 8.5% of the population were below the poverty line, including 10.4% of those under age 18 and 9.5% of those age 65 or over.


=== 2010 ===
The 2010 United States Census reported that Lakeside had a population of 20,648. The population density was 2,836.0 people per square mile (1,095.0/km²). The racial makeup of Lakeside was 17,545 (85.0%) White, 235 (1.1%) African American, 181 (0.9%) Native American, 351 (1.7%) Asian, 53 (0.3%) Pacific Islander, 1,327 (6.4%) from other races, and 956 (4.6%) from two or more races. Hispanic or Latino of any race were 3,627 persons (17.6%).
The Census reported that 20,465 people (99.1% of the population) lived in households, 81 (0.4%) lived in non-institutionalized group quarters, and 102 (0.5%) were institutionalized.
There were 7,347 households, out of which 2,761 (37.6%) had children under the age of 18 living in them, 3,878 (52.8%) were married, 1,086 (14.8%) had a female householder with no husband present, 457 (6.2%) had a male householder with no wife present. There were 477 (7.3%) unmarried. 1,433 households (19.5%) were made up of individuals and 660 (9.0%) had someone living alone who was 65 years of age or older. The average household size was 2.79. There were 5,421 families (73.8% of all households); the average family size was 3.16.
The population was spread out with 5,050 people (24.5%) under the age of 18, 1,996 people (9.7%) aged 18 to 24, 4,914 people (23.8%) aged 25 to 44, 6,030 people (29.2%) aged 45 to 64, and 2,658 people (12.9%) who were 65 years of age or older. The median age was 39.1 years. For every 100 females there were 96.6 males. For every 100 females age 18 and over, there were 93.2 males.
There were 7,776 housing units at an average density of 1,068.0 per square mile (412.4/km²), of which 5,066 (69.0%) were owner-occupied, and 2,281 (31.0%) were occupied by renters. The homeowner vacancy rate was 2.9%; the rental vacancy rate was 6.6%. 13,773 people (66.7% of the population) lived in owner-occupied housing units and 6,692 people (32.4%) lived in rental housing units.


== Economy ==
David Jeremiah's Turning Point for God is based in Lakeside.


== Government ==
In the California State Legislature, Lakeside is in the 38th Senate District, represented by Republican Joel Anderson, and the 71st Assembly District, represented by Republican Brian Jones.
In the United States House of Representatives, Lakeside is in California's 50th congressional district, represented by Republican Duncan D. Hunter.


== Notable people ==
Joan Embery, animal and environmental advocate, and regular guest on The Tonight Show.
Sean Pendergrass, storyboard artist for the TV series' Rugrats, As Told by Ginger, and The Wild Thornberrys.
Carl C. Rasmussen (1901–52), Los Angeles City Council member, owned a hardware store in Lakeside.
Frederick W. Sturckow, a United States Marine Corps officer and a NASA astronaut.
Scott Wilson, professional bodybuilder, former Mr. America and Mr. International.
Doug Ingle, founding member and original vocalist of the band Iron Butterfly.


== References ==


== External links ==
Lakeside Chamber of Commerce