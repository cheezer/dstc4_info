A landmark is a recognizable natural or artificial feature used for navigation, a feature that stands out from its near environment and is often visible from long distances.
In modern use, the term can also be applied to smaller structures or features, that have become local or national symbols.


== Etymology ==
In old English the word landmearc (from land + mearc (mark)) was used to describe an "object set up to mark the boundaries of a kingdom, estate, etc.". Starting from approx. 1560, this understanding of landmark was replaced by a more general one. A landmark became a "conspicuous object in a landscape". A landmark literally meant a geographic feature used by explorers and others to find their way back or through an area. For example the Table Mountain near Cape Town, South Africa is used as the landmark to help sailors to navigate around southern tip of Africa during the Age of Exploration. Artificial structures are also sometimes built to assist sailors in naval navigation. The Lighthouse of Alexandria and Colossus of Rhodes are ancient structures built to lead ships to the port.
In modern usage, a landmark includes anything that is easily recognizable, such as a monument, building, or other structure. In American English it is the main term used to designate places that might be of interest to tourists due to notable physical features or historical significance. Landmarks in the British English sense are often used for casual navigation, such as giving directions. This is done in American English as well.
In urban studies as well as in geography, a landmark is furthermore defined as an external point of reference that helps orienting in a familiar or unfamiliar environment. Landmarks are often used in verbal route instructions and as such an object of study by linguists as well as in other fields of study.


== Types of landmarks ==
Landmarks are usually classified as either natural landmarks or man-made landmarks, both are originally used to support navigation on finding directions. A variant is a seamark or daymark, a structure usually built intentionally to aid sailors navigating featureless coasts.
In modern sense, landmarks are usually referred to as monuments or distinctive buildings, used as the symbol of a certain area, city, or nation, such as the Statue of Liberty in New York City, Eiffel tower in Paris, Big Ben in London, Christ the Redeemer in Rio de Janeiro, The Obelisk in Buenos Aires, Sydney Harbour Bridge in Sydney, or Fernsehturm in Berlin. Church spires are often very tall and visible from many miles around, thus often serve as built landmarks. Also town hall towers and belfries often have a landmark character.
Natural landmarks can be characteristic features, such as mountains or plateaus. Trees also serve as local landmarks, such as jubilee oaks or conifers. Some landmark trees may be nicknamed, examples being Queen's Oak, Hanging Oak or Centennial Tree.


== See also ==
Cultural heritage management
National landmark (disambiguation)
National symbol


== References ==


== External links ==
 Media related to Landmarks at Wikimedia Commons