Latin pop (Spanish: Pop latino) refers to pop music that contains sound or influence from Latin America but it can also mean pop music from any Spanish-speaking world. Latin pop usually combines upbeat Latin music with American pop music. Latin pop is commonly associated with Spanish-language pop, rock, and dance music.


== History ==
Latin Pop is one of the most popular Latin music genres today. However, before the arrival of artists like Shakira and Ricky Martin, Latin Pop first reached a global audience through the work of bandleader Sergio Mendes in the mid-1960s; in later decades, was defined by the romantic ballads that legendary artists such as Julio Iglesias or Roberto Carlos produced back in the 1970s.


=== Influences and development ===
Latin Pop became the most popular form of Latin music (genre) in the United States during the 1980s and 1990s, even achieving massive crossover success among non-Latino listeners during the late 1990s. While not restricted to America by any means, Latin pop was profoundly affected by production techniques and other styles of music—both Latin and otherwise—that originated primarily in the United States. Tejano music, centered in Texas and the U.S./Mexico border region, had begun to introduce synthesizers, slicker production, and a more urban sensibility to formerly rootsy styles like norteño and conjunto. Moreover, New York and Miami were home to thriving Latin club scenes, which during the 1980s led to the rise of Latin freestyle, a club-oriented dance music that was rooted in Latin rhythms but relied on synthesizers and drum machines for most of its arrangements. Both of these sounds influenced the rise of Latin pop, which retained Latin rhythms in its uptempo numbers but relied more on mainstream pop for its melodic sense. Latin pop's first major crossover star was Gloria Estefan, who scored a succession of non-club-oriented dance-pop hits during the mid- to late 1980s, but who eventually became known more as an adult contemporary diva with an affinity for sweeping ballads. This blend of Latinized dance-pop and adult contemporary balladeering dominated Latin pop through the 1990s; most of its artists sang in Spanish for Latino audiences, although Latin pop's similarity to the mainstream helped several performers score crossover hits when they chose to record in English. Jon Secada landed several pop hits during the mid-1990s, and Tejano pop star Selena's album Dreaming of You actually debuted posthumously at number one on the album charts upon its 1995 release.


== See also ==

Pop music
Grammy Award for Best Latin Pop Album
Latin music
Latin Pop Songs
Latin Pop Albums


== References ==


== External links ==
Univision musica (Univision musica)
RITMOSON MUSIC NEWS