This page lists direct English translations of common Latin phrases, such as veni, vidi, vici and et cetera. Some of the phrases are themselves translations of Greek phrases, as Greek rhetoric and literature were highly regarded in Ancient Rome when Latin rhetoric and literature were still maturing.
The Latin letter i may be used either as a vowel or a consonant. When used as a consonant, it often is replaced by the letter j, which originally, was simply an orthographic "long i" that was used in initial positions and when it occurred between two other vowels. This convention from Medieval Latin is preserved most commonly in Latin legal terminology—hence phrases such as de iure often are spelled de jure.

To view all phrases on a single, lengthy document, see:
List of Latin phrases (full)
The list also is divided alphabetically into twenty pages:


== See also ==


== External links ==
Database of Latin phrases, searchable in Latin or English
Latin sayings & Latin phrases, organized by topic
Notable idioms and concepts in Latin
Commonly used Latin phrases
Latin abbreviations
Over 1000 Latin terms and phrases