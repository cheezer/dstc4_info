Lau or LAU may refer to:
Lau Chan, fictional character in video game Virtua Fighter Series
Lau (band), a British folk music group
Lambda Alpha Upsilon, a Greek letter intercollegiate fraternity
Lebanese American University, a university in Lebanon
Local administrative unit, a low level administrative division of a country, ranked below a province, region, or state
Lẩu, Vietnamese Hot pot
Lauinger Library, the main library at Georgetown University, which is commonly known as "Lau"


== People ==
Lau (surname)
Liu (劉/刘), a common Chinese family name transliterated Lau in Cantonese
Lau clan, one of the Saraswat Brahmin clans of Punjab


== Places ==
Lau, Estonia, a village in Estonia
Lau, Gotland, a village in Gotland, Sweden
Lau, Nigeria, a local government area
Lau Islands, Fiji
Lau Province, Fiji
Lau River, a tributary of the Gave d'Ossau in south west France
Laurel (Amtrak station), a passenger train station in Laurel, Mississippi, United States
LAU, IATA code for Manda Airport, a public airport on Manda Island, Kenya


== Languages ==
Lauan language, also called Lau, spoken in Fiji, ISO 639-3: llx
Lau language (Malaita), spoken in the Solomon Islands, ISO 639-3: llu