Andy Lau Tak-wah MH, JP (born 27 September 1961) is a Hong Kong Cantopop singer-songwriter, actor, presenter, and film producer. Lau has been one of Hong Kong's most commercially successful film actors since the mid-1980s, performing in more than 160 films while maintaining a successful singing career at the same time. In the 1990s, Lau was branded by the media as one of the Four Heavenly Kings of Cantopop.
Lau was entered into the Guinness World Records for the "Most Awards Won by a Cantopop Male Artist". By April 2000, he had already won a total unprecedented 292 awards. In 2005, Lau was awarded "No.1 Box office Actor 1985–2005" of Hong Kong, yielding a total box office of HKD 1,733,275,816 for shooting 108 films in the past 20 years. In 2007, Lau was also awarded the "Nielsen Box Office Star of Asia" by the Nielsen Company (ACNielsen).


== Biography ==


=== Early life ===
Andy Lau was born in Tai Po, Hong Kong. A son of a fireman (劉禮), as a young boy Lau had to fetch water for his family up to eight times a day because their house was not equipped with plumbing. He later graduated from a Band One secondary school, Ho Lap College in San Po Kong, Kowloon. He also practices Chinese calligraphy. Lau converted to Buddhism in the 1980s. He was raised in a nominally Buddhist household and is now a follower of the Lingyan Mountain Temple in Taiwan.


=== Personal life ===
In 2008, Lau married Malaysian Chinese Carol Chu after 24 years of speculation over their relationship. The couple registered their marriage in Clark County, Nevada, in the United States. On 9 May 2012, Chu gave birth to his first child, a daughter named Hanna.


== Career ==


=== Dramatic acting ===
In 1981, Lau signed up for the artist training program offered by TVB, which is where his acting career began. The lead role which led to his initial popularity was in the 1982 TVB series The Emissary. In 1983, Lau's role as "Yang Guo" in the TVB wuxia series The Return of the Condor Heroes further increased his popularity. From then on, he would take on many of the lead roles in many of the TVB series.
In September 1983, TVB was looking to increase their ratings in competition with Korea and Japan for the best variety show. The station then created the show TVB All-star challenge (星光熠熠勁爭輝) featuring almost the entire line-up of the most popular actors and singers at the time. Lau along with Tony Leung, Michael Miu, Felix Wong, and Kent Tong were branded as "TVB's Five Tigers" (無線五虎將) due to their popularity on the show.
In the late 80s, Lau left TVB due to contract problems. TVB wanted to bind him to an exclusive five-year contract which Lau refused to sign, so TVB blacklisted him. He then focused on his film career.


=== Film acting ===

In 1981, Lau made a guest appearance in one of Susanna Kwan's music video and caught the eye of the manager Teddy Robin. Teddy Robin then gave Lau a chance to play a small role in the movie Once Upon a Rainbow. This was the first step in Lau's film acting career. He was then given a role in Ann Hui's 1982 film, Boat People. Later in 1983 he had his first leading role in a Shaw Brothers film called On the Wrong Track.
One of his early leading roles included the more serious 1988 film The Truth (法內情). However, Lau is best known in movies for his (often) recurring roles as a "Heroic Gangster" such as Wong Kar-wai's 1988 film, As Tears Go By and Johnnie To's 1990 film, A Moment of Romance.
Though a respectable actor, Lau in the early days was known more for his good looks. The people he works with say he is an idol, but he has claimed to be an artist. Lau has proved his acting skills in many of his movies. His first major acting prize came with A Fighter's Blues, which was his first Golden Bauhinia Award for Best Actor. He would win the Hong Kong Film Award for Best Actor award that year for Running out of Time. In 2004, he won the prestigious Golden Horse Award for his performance in Infernal Affairs III, the sequel to the popular Infernal Affairs. Western audiences may also be familiar with his performance in the House of Flying Daggers.
In 2005, Lau was awarded "No.1 Box office Actor 1985–2005" of Hong Kong, yielding a total box office of HKD 1,733,275,816 for shooting 108 films in the past 20 years. The aforementioned figure is as compared to the first runner-up Stephen Chow's (HKD 1,317,452,311) and second runner-up Jackie Chan's (HKD 894,090,962). "I've never imagine that it would be as much as 1.7 billion!" he told the reporters. For his contributions, a wax figure of Lau was unveiled on 1 June 2005 at the Madame Tussauds Hong Kong. In 2007, Lau was also awarded the "Nielsen Box Office Star of Asia" by the Nielsen Company (ACNielsen).


=== Film producer ===
In 1991 Lau has set up his own film production company called Teamwork Motion Pictures Limited, which in 2002 was renamed to Focus Group Holdings Limited. His contributions in the film industry as well as his involvement in nurturing new talents in the Asian film industry led him to being awarded the "Asian Filmmaker of the Year" in the Pusan International Film Festival in 2006. Some of the films he produced include the award-winning Made in Hong Kong and the mainland digital film Crazy Stone.


=== Musical career ===

Lau released his first album "Only Know that I Still Love You" (只知道此刻愛你) under Capital Artists in 1985. However, his first album was not a big hit. Despite having a voice not traditionally associated with popular music, his hard work and perseverance resulted in him being one of the most successful singers in Cantopop. His singing career reached stellar status in 1990 with the release of the album entitled "Would It Be Possible" (可不可以), and his subsequent releases only solidified his status as a marketable singer. For that song, he would win his first 1990 RTHK Top 10 Gold Songs Awards. He would then win at least one RTHK award category every year consecutively until the year 2007.
From Jade Solid Gold Top 10 Awards, he has won the "Most Popular Hong Kong Male Artist" award 7 times and the "Asia Pacific Most Popular Hong Kong Male Artist" award 15 times. He also entered into Guinness World Records for "Most Awards Won by a Cantopop Male Artist". By April 2000, he had already won a total unprecedented 292 awards.
Many of his songs quickly topped the music charts, not only in Hong Kong, but also in Taiwan, Mainland China, and in many different parts of Asia. Some of the most notable hits by Lau include "The Days We Spent Together" (一起走過的日子), "If You Are My Legend" (如果你是我的傳說), "The Tide" (潮水), "Forget Love Potion" (忘情水), "True Forever" (真永遠), "Chinese people" (中國人), "Love You Forever" (愛你一萬年), "You Are My Woman" (你是我的女人), "Secret Admiration" (暗裡著迷). Besides singing in Cantonese and Mandarin, he also sang in other languages, such as English, Japanese, Malay, and Taiwanese. One example of a Hokkien song was (世界第一等).
Since the early 1990s, Lau, along with Jacky Cheung, Aaron Kwok and Leon Lai have been referred by the Chinese media as the Cantopop Four Heavenly Kings.
Lau sang alongside Jackie Chan during a part of the 2008 Summer Olympics closing ceremony on 24 August 2008. In addition Lau, who has been supporting the disabled athletes in Hong Kong for more than a decade, was appointed as the Goodwill ambassador for the 2008 Summer Paralympics. He led other performers in singing and performing the song "Everyone is No.1" at the Beijing National Stadium just a few hours before the 2008 Paralympics opening ceremony began to show his support for the disabled athletes. He also sang the theme song "Flying with the Dream" with Han Hong during the Paralympics opening ceremony on 6 September 2008.


=== Appearance in other media ===
Lau is alleged to have been featured as a non-player character (NPC) as a random pedestrian in a sandbox-style action video game called Prototype. On 6 July, the Hong Kong local newspapers Headline Daily and Sing Tao Daily reported on Lau's cameo.


=== Community ===
In 1994, Lau established the Andy Lau Charity Foundation which helps people in need and promotes a wide range of youth education services. In 1999, he was awarded the Ten Outstanding Young Persons of the World, being the third person from Hong Kong at that time to bestow this distinguished honour. In 2008, Lau took a main role in putting together the Artistes 512 Fund Raising Campaign for donation relief toward the victims of the 2008 Sichuan earthquake. It was one of the largest and most ambitious charity event ever assembled in the territory.
Lau has been noted for his active involvement in charity works throughout his 30 years of showbiz career and was honoured "Justice of Peace" by the Hong Kong SAR government in 2008. In May 2010, he received the "World Outstanding Chinese" award and an "honorary doctorate" from the University of New Brunswick, Canada.


== Awards and nominations ==
Hong Kong Film Awards


=== Won ===
1998 Best Film for Made in Hong Kong (Producer)
2000 Best Actor for Running Out of Time
2003 Best Original Film Song for Infernal Affairs (Singer with Tony Leung)
2004 Best Actor for Running on Karma
2008 Best Supporting Actor for Protégé
2011 Best Film for Gallants (Producer)
2012 Best Actor and Best Film for A Simple Life (Producer)


=== Nominations ===


==== Best Actor ====
1989 Best Actor Nomination for As Tears Go By
1992 Best Actor Nomination for Lee Rock
1996 Best Actor Nomination for Full Throttle
2001 Best Actor Nomination for A Fighter's Blues
2002 Best Actor Nomination for Love on a Diet
2003 Best Actor Nomination for Infernal Affairs
2006 Best Actor Nomination for Wait 'Til You're Older
2008 Best Actor Nomination for The Warlords


==== Best Original Film Song ====
1992 Best Original Film Song Nomination for Casino Raiders II (Singer)
1995 Best Original Film Song Nomination for Tian Di (Singer)
1996 Best Original Film Song Nomination for Full Throttle (Singer/Lyricist)
1998 Best Original Film Song Nomination for Island of Greed (Singer)
1999 Best Original Film Song Nomination for The Longest Summer (Singer)
1999 Best Original Film Song Nomination for A True Mob Story (Singer/Lyricist)
2002 Best Original Film Song Nomination for Shaolin Soccer (Singer/Lyricist)
2006 Best Original Film Song Nomination for Wait 'Til You're Older (Singer/Lyricist)
2008 Best Original Film Song Nomination for Brothers (Singer with Eason Chan/Lyricist)
2012 Best Original Film Song Nomination for Shaolin (Singer/Lyricist)
Golden Horse Film Festival
2011 Best Actor (A Simple Life)
2004 Best Actor (Infernal Affairs III : End Inferno)
2003 Best Actor Nomination (Infernal Affairs)
2001 Best Actor Nomination (Love on a Diet)
1990 Best Supporting Actor Nomination (The Last Princess of Manchuria)
Golden Bauhinia Awards
2007 Best Actor Nomination (Battle of Wits)
2006 Best Actor Nomination (Wait Til You're Older)
2005 Best Actor Nomination (A World Without Thieves)
2004 Best Actor Nomination (Infernal Affairs III : End Inferno)
2004 Best Actor Nomination (Running on Karma)
2003 Best Actor Nomination (Infernal Affairs)
2001 Best Actor (A Fu)
2000 Best Actor Nomination (Running Out of Time)


==== Other Nominations ====
1983 Best New Performer Nomination for Boat People
1999 Best Film Nomination for The Longest Summer (Producer)
2007 Best Asian Film Nomination for Crazy Stone (Producer)
(11 Best Actor Nominations, 11 Best Original Film Song Nominations, 4 Best Film Nominations, 1 Best Supporting Actor Nomination, 1 Best New Performer Nomination, 1 Best Asia Film Nomination.)


== References ==


== External links ==

Andy Lau's Official Website (Chinese)
Andy Lau at the Hong Kong Movie DataBase
Andy Lau at the Internet Movie Database