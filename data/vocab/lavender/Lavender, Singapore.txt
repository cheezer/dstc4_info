Lavender is a cluster of Housing and Development Board (HDB) flats in Kallang, Singapore. Named after Lavender Street, the area is near the Rochor River and is served by Lavender MRT Station.
Its main landmarks include Jalan Besar Stadium, inaugurated in 1929, and Tai Pei Buddhist Centre which was founded in 1937.
The district has not changed a lot in a city known for its frenetic urban planning, and as a source puts it, "entering Lavender is like entering Singapore in the late 1980s", where old public housing is occupied by an elderly community, with small automotive shops, hardware resellers and temples.
Due to its proximity to the heart of the city, the Lavender area also attracts many backpackers who can book a room at one the many youth hostels in the vicinity.
A trend of gentrification began around 2014, mainly along Jalan Besar, the main street which separates Lavender on the north from the Little India, Singapore district. There are now many small coffee shops and music studios in a rather vintage-looking district.


== References ==