Le Mans Football Club (French pronunciation: ​[ləmɑ̃]; commonly referred to as Le MUC or simply Le Mans) was a French association football club based in Le Mans. The club was founded in 1985 as a result of a merger under the name Le Mans Union Club 72. In 2010, Le Mans changed its name to Le Mans FC to coincide with the re-modeling of the club, which includes moving into a new stadium, MMArena, which opened in January 2011. The club currently plays in Ligue 2, the second level of French football having suffered relegation from Ligue 1 following the 2009–10 season. Due to financial difficulties, the club lost its professional status in 2013.


== History ==
Le Mans Sports Club were founded in 1900, but it wasn't until 1908 that a football club existed within it. In 1910, Le Mans qualified for the Championnat de la France in 1910, but were heavily overturned by Saint-Servan. Gaining a huge reputation up to World War I, Le Mans SC plunged into obscurity by World War II before joining the a war league in 1942.
The football section of Union Sportive du Mans was founded in 1903.
The club was formed as a result of a merger between Union Sportive du Mans and Le Mans Sports Club, on 12 June 1985. Upon its foundation, former football player Bernard Deferrez was installed as manager. Le Mans UC spent the majority of its infancy in Ligue 2. In the 2003–04 season, the club achieved promotion to Ligue 1 for the first time, but were immediately relegated. Le Mans returned to the first division for the 2004–05 season and successfully remained in the league for the next four seasons. The club suffered relegated back to Ligue 2 in the 2009–10 season. Midway through the campaign, on 2 December 2009, Le Mans announced that it was changing its name from Le Mans Union Club 72 to Le Mans FC.


== Players ==


=== Last squad ===
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== Last reserve squad ===
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== Notable players ===
Below are the notable former players who have represented Le Mans and its predecessors in league and international competition since the club's foundation in 1985. To appear in the section below, a player must have played in at least 100 official matches for the club.
For a complete list of Le Mans players, see Category:Le Mans FC players


== Former managers ==


== Honours ==
Division d'Honneur Quest
Winners (2): 1961, 1965

Coupe Gambardella
Winners (1): 2004


== References ==


== External links ==
Official Website (in French)