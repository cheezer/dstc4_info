Kwan Swee Lian (born 1933) is a Malaysian chef and businessperson. Known as the "nasi lemak queen", she is recognised for establishing Sakura, a restaurant famous for its nasi lemak and beef rendang. Sakura prompted the opening of Madam Kwan's, which is now led by her son Rudy.


== §Early life ==
The seventh child of the family, Kwan was born 1933 in Kluang, Johor, Malaysia. Kwan's father was a labourer and her mother was a homemaker. She studied at Kuan Cheng Girls' School in Kuala Lumpur, where her family relocated to later. After two years of secondary education, Lwan decided to pursue a professional career. Starting out as a salesperson, she established Sakura, a hair salon, around 1958. Sakura was subsequently converted into a restaurant some two decades later.


== §Career ==
Due to unspecified bad choices, Kwan had to sell Sakura around the year 2000. The new owners of the restaurant then sacked her, leaving Kwan with just a knife and little cash. Deciding to work from a rented food stall, Kwan suffered another setback when no profits were made. Instead, large losses were incurred due to the high rent and the cost of the ingredients. As a "tribute" to Kwan, her youngest son Rudy decided to forgo his career in the stock market and establish Madam Kwan's, a Malaysia-based restaurant selling similar food items as Sakura does, in 1999. The rendang sold at Madam Kwan's is personally cooked by Kwan, alongside other food items. The restaurant is recognised for its beef rendang and nasi lemak, the latter selling around a thousand plates a day. Kwan also serves as a food taster for Madam Kwan's.


== §Personal life ==
Kwan wed Foo Lum Choon, a medical doctor, in 1962. Born 1917, Foo died in 2007, leaving Kwan a widow. They had four children: three sons and a daughter. Among the three sons, two of them have died as of 2013. The surviving son, Rudy, is an accountant and the head of Madam Kwan's. Their daughter Shirley is a homemaker. Kwan is an avid hat collector with about twenty in her collection. She labels herself as a "perfectionist".


== §References ==