Rough lemon (Citrus jambhiri Lush.) refers to the fruit and the tree of a citrus hybrid related to the citron and the lemon, with character traits similar to rangpur or mandarin orange.
Rough lemon is a cold-hardy citrus and can grow into a big tree.
There are several cultivars of rough lemon that use as a citrus rootstock, among them are: florida, schaub, and Vangassay rough lemon.


== References ==


== External links ==
The Gardener
FruitiPedia
National Tropical Botanical Garden
MirriamWebster