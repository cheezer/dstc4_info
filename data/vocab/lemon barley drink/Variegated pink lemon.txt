The variegated pink lemon, also called the variegated Eureka lemon, or pink-fleshed Eureka lemon is a cultivar of lemon (Citrus × limon) with unique pink flesh, and variegated foliage. It was discovered as a sport on an ordinary Eureka lemon tree in Burbank, California circa 1930.


== References ==
Variegated pink at the Citrus Variety Collection