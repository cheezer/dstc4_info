"Lento" (English: "Slow") is the second single from the third album by Mexican singer Julieta Venegas, called Sí. The song was named the 5th best song of the 2000s decade by Latin music website Club Fonograma.


== Song information ==
The song was written by Coti Sorokin and Julieta Venegas. Julieta plays the accordion, drum machine and keyboards on the track. It made the Billboard chart, and reached positions 31 and 13 on the Hot Latin Songs and Latin Pop Airplay respectively. Julieta performed a new version on piano for her MTV Unplugged in 2008. Erika Martins Brazilian singer performed a cover of this song in Portuguese in 2009, and sings a few bars of the song in Spanish position in the lists of popularity in Brazil.


== Live performance ==
The performance of the MTV Video Music Awards Latinoamérica 2004 in Miami, Florida performs with the Mexican hip hop group Cartel de Santa


== Music video ==
The music video was filmed in Japan, and featured her twin sister Yvonne.
The video begins with Julieta Venegas walking the streets of Japan and following her sister Yvonne. The two are dressed in the same clothes and have the same hairstyle (except that Yvonne has shorter hair). Animated flowers and plants begin to grow each time Julieta goes through a street and on anything she touches. The video ends with the sisters playing a crane game.


== Track listing ==
CD Single
"Lento" — 4:03


== Charts ==


=== Weekly charts ===


== References ==


== External links ==
Full lyrics of this song at MetroLyrics