Lentor is an area located on the North side of Singapore situated between Yishun and Yio Chu Kang. Lentor is the home to many private housings and there is no public housing in the area. Lower Seletar Reservoir is also located along Lentor Avenue which is the only major road in Lentor. The stretch of the North South MRT line between Khatib MRT Station and Yio Chu Kang MRT Station also plies along Lentor Avenue. Lentor can be also accessed through Seletar Expressway (Exit 3). All the military training areas in Lentor will become new sites for North-South Expressway, residential and bus depot respectively.


== Transport ==
There are a lot of bus routes which pass through Lentor Avenue, however 529 and 825 from Yio Chu Kang Bus Terminal comes to Lentor Estate area.