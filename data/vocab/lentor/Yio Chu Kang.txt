Yio Chu Kang is a sub-urban area in the northeast of Singapore, with proximity to the Ang Mo Kio, Seletar and Serangoon areas. Deriving its name from the Yio Chu Kang Village, it is still known for lush greenery and low-density housing with high-rise public housing in its southern fringes.


== Educational institutions ==


=== Primary schools ===
Anderson Primary School


=== Secondary schools ===
Presbyterian High School *Yio Chu Kang Secondary School


=== Tertiary Institutions ===
Nanyang Polytechnic
Anderson Junior College
ITE College Central


== Private Housing Estates ==
Teacher's Housing Estate
Thomson Hills Estate
The Calrose
Castle Green
Seasons Park
Nuovo
Cactus Green
Sunrise Garden
Seletar Garden
Grande Vista


== Other Amenities ==
Hospitals
Ang Mo Kio- Thye Huan Kwan Hospital

Community Centres/Clubs
The Grassroots Club
Yio Chu Kang Community Club

Sports Facilities
Yio Chu Kang Sports And Recreation Centre
Yio Chu Kang Stadium
Yio Chu Kang Swimming Complex


== Transportation ==
Situated at the northern portion of Ang Mo Kio, Yio Chu Kang is served by bus services originating from its bus interchange, Ang Mo Kio Bus Depot, Ang Mo Kio Bus Interchange and other parts of the island. Yio Chu Kang MRT Station also serves the area, providing residents a direct link to the relatively distant Central Area.
Buses that ply along Yio Chu Kang:


== Electoral division ==
Yio Chu Kang became a Single Member Constituency in 2006 although it was part of the Ang Mo Kio Group Representative Constituency since 1991, during the 2010 elections the SMC was dissolved and absorbed again back into Ang Mo Kio GRC