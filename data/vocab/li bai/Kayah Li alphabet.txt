The Kayah Li alphabet (Kayah Li: ꤊꤢ꤬ꤛꤢ꤭ ꤜꤟꤤ꤬) is used to write the Kayah languages Eastern Kayah Li and Western Kayah Li, which are members of Karenic branch of the Sino-Tibetan language family. They are also known as Red Karen and Karenni. Eastern Kayah Li is spoken by about 26,000 people, and Western Kayah Li by about 100,000 people, mostly in the Kayah and Karen states of Myanmar, but also by people living in Thailand.


== History ==
Kayah Li script was devised by Htae Bu Phae in March 1962, in part in response to the appearance of Latin-based orthographies which had appeared after 1950. It is taught in schools in refugee camps in Thailand. Kayah Li’s relation to Brahmic scripts can be seen in its ordering and the shapes of some of its letters, although the shapes of most of them were developed independently. At least nine of its characters bear a relation to characters in the Myanmar script.


== Description ==
Unlike the Myanmar script, the Kayah Li script is an alphabet proper as the consonant letters don't have any subsequent vowel. Four of the vowels are written with separate letter, the others are written using a combination of the letter for a and a diacritic marker. The diacritics can also be used in combination with the letter for ơ to represent sounds occurring in loanwords. There is also a set of three diacritics that are used to indicate tone. Unlike the vowel diacritics, that are written above the letter, these are written under.


=== Vowels ===


==== Vowels used for loanwords ====


=== Tone markers ===


=== Consonants ===


=== Digits ===


== Unicode ==

Kayah Li was added to the Unicode Standard in April, 2008 with the release of version 5.1.
The Unicode block for Kayah Li is U+A900–U+A92F:


== See also ==
Kayah languages
Karen people


== External links ==
Page on Omniglot about Kayah Li
Unicode Kayah Li chart
GNU FreeFont, a Unicode font with support for Kayah Li
Different Kayah Li Fonts


== References ==
Everson, Michael (2006-03-09). "Proposal for encoding the Kayah Li script in the BMP of the UCS" (pdf). Working Group Document. International Organization for Standardization.