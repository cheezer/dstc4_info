Later Liang may refer to the following states in Chinese history:
Later Liang (Sixteen Kingdoms) (後涼; 386–403), one of the Sixteen Kingdoms
Western Liang or Later Liang (後梁; 555–587), a remnant of the Liang dynasty during the Southern and Northern Dynasties period
Later Liang (Five Dynasties) (後梁; 907–923), a state during the Five Dynasties and Ten Kingdoms period


== See also ==
Liang (disambiguation)