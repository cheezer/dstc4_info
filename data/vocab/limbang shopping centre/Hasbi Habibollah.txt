Haji Hasbi Habibollah (born 2 January 1963) is a Malaysian politician. He is the Member of the Parliament of Malaysia for the Limbang constituency in Sarawak, representing the Parti Pesaka Bumiputera Bersatu (PBB) in the governing Barisan Nasional coalition.
Hasbi was elected to Parliament by a narrow margin in the 2008 election for the newly created seat of Limbang. He was previously an engineer.


== Election results ==


== References ==