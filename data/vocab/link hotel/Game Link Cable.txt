The Nintendo Game Link Cable is an accessory for the Game Boy line of handheld video game systems, allowing players to connect Game Boys of all types for multiplayer gaming. Depending on the games, a Game Link Cable can be used to link two games of the same title, like Tetris, or two compatible games like Pokémon Red and Blue. Games can be linked for head-to-head competition, cooperative play, trading items, unlocking hidden features, etc.
The Game Link Cable socket and connector design influenced the design of IEEE 1394/FireWire.


== First generation ==

The first generation Game Link Cable (model DMG-04) was released alongside the original Game Boy and has 'large' connectors on both ends. It can only be used to link two original Game Boys to play Game Link-compatible games, usually denoted by a "Game Link" logo (often read as "Game Boy Video Link") on the packaging and cartridge.
A select few Game Boy games, such as F-1 Race, supported multiplayer modes for up to four players, although this requires the use of up to three additional Game Link cables and the Game Boy Four Player Adapter (model DMG-07).
After the release of the Game Boy Pocket, Nintendo started using a smaller Game Link connector (See 'Second Generation' below) and released an adaptor called the Game Link Cable Adaptor (model MGB-004) which could be used in conjunction with the original Game Link cable (model DMG-04) to allow an original Game Boy to connect to a Game Boy Pocket/Color.
See also List of multiplayer Game Boy games


== Second generation ==
The second generation started with the release of the Game Boy Pocket which used a much smaller Game Link connector than those used on the original Game Boy. Although the pin assignment and basic port shape remained the same, its much smaller size necessitated the release of new Game Link Cables.
The second generation Game Link Cables came in a few varieties, but each serves the same purpose. The first was called the Game Boy Pocket Game Link Cable (model MGB-008), and was designed to be used with the Game Boy Pocket. The MGB-008 was the only Game Link cable to be white in color, and may have only been released in Japan. The MGB-008 features the smaller second generation connectors on both ends, allowing two Game Boy Pockets to link.
The next cable in this generation is called the Universal Game Link Cable (model MGB-010). It features the smaller second generation connector on one end, and the cable splits into both a second generation and first generation connector at the other end (although only one connector at this end can be used at any given time) This link cable was included with the Game Boy Printer in the USA and Europe but does not appear to have been available to buy separately.
After the Game Boy Pocket came the Game Boy Light (a backlit Game Boy Pocket only released in Japan), and the Game Boy Color, all share the same link cable port design, and Game Boy Color games and original Game Boy games can both use a second generation cable. Therefore, the Game Boy Color is compatible with the MGB-008 and MGB-010. Despite this, the Game Boy Color did receive its own designated link cable, the Game Boy Color Game Link Cable (model CGB-003) however it was no different functionally than the MGB-008.
Nintendo also released a small adaptor called the Universal Game Link Adapter (model DMG-14) which features a small second generation socket and a first generation plug. The adapter can be used in conjunction with either the MGB-008 or the CGB-003 and features a thin plastic harness allowing it to be clipped on to either cable. In the USA and Europe Nintendo released the CGB-003 and DMG-14 as a set called the Universal Game Link Cable Set. As the set includes both the CGB-003 cable which features the smaller second generation connectors at both ends, plus a removable DMG-14 first generation adapter, it allows the connection of either two Game Boy Pocket/Colors, or one original Game Boy and one Game Boy Pocket/Color.
The Super Game Boy 2 also shares the same smaller style link cable port, and therefore uses the same cables and adapters.


== Third generation ==

The third generation started with the release of the Game Boy Advance which was released alongside its own link cable called the Game Boy Advance Game Link Cable (model AGB-005). It features yet another new type of link cable port which is used on the Game Boy Advance, Game Boy Advance SP and Game Boy Player. This link cable can only be used for connecting Game Link compatible Game Boy Advance games.
A small hub is included in the middle of the cable, which allows a second Game Link cable to be branched off of the first, and in turn, a third game link cable can be branched off of the second. This provides connections for four consoles in total, meaning games can be played with up to four players. The order in which the cables are connected to one another determines which player is which; the first player always connects through the purple end of a game link cable, and all of the others through the grey ends, due to the design of the plugs and recepticles.[1]
The port design on the third generation is almost identical to the second generation link cable ports except that it adds an extra protrusion on the plug and a notch on the socket to prevent a Game Boy Advance Game Link cable from being accidentally inserted into older model Game Boy systems. The almost identical shape of the port allows the Game Boy Advance, Game Boy Advance SP and Game Boy Player to accept all the second generation Game Link Cables, but only for backward compatibility between Game Boy and Game Boy Color games. The second generation Game Link cable cannot be used to link Game Boy Advance games, and the third generation Game Link Cable cannot be used to link Game Boy or Game Boy Color games.
The e-Reader also uses the third generation link cable port, but since it is incompatible with Game Boy and Game Boy Color games, it is not backwards compatible with the second generation Game Link Cable.
Also compatible with Game Boy Advance, Game Boy Advance SP, e-Reader, and Game Boy Player is the Game Boy Advance Wireless Adapter (model AGB-015). The adapter allows up to five players to link for multiplayer gaming, although it is capable of linking up to thirty-nine copies of Pokémon FireRed and Pokémon LeafGreen in a virtual in-game lobby called the "Union Room." Unlike the AGB-005, the wireless adapter is not compatible with all Game Boy Advance multiplayer games. Only certain games feature specific support for the AGB-015.


== Fourth generation ==
The fourth and last generation Game Link Cable, called the Game Boy Micro Game Link Cable (model OXY-008), was designed specifically for use with the Game Boy Micro. The Game Boy Micro features an even smaller link cable port than the Game Boy Advance, so it too requires its own Game Link Cable. The link cable features a fourth generation connector on each end which allows two Game Boy Micros to link. Alternatively, the Game Boy Micro Game Link cable can be used in conjunction with the Game Boy Micro Converter Connector (model OXY-009) to link one Game Boy Micro and one Game Boy Advance or Game Boy Advance SP. Like the Game Boy Advance Game Link Cable, the Game Boy Micro Game Link Cable features a link cable port in the middle, used to receive additional cables to connect up to four players at once.
Also compatible with the Game Boy Micro is the Game Boy Micro Wireless Adapter (model OXY-004). The OXY-004 is compatible with all the same games as the AGB-015 and can communicate with the AGB-015 wirelessly to link one or more Game Boy Micros and one or more Game Boy Advance or Game Boy Advance SP systems.


== Other Link Cables ==
A Game Link Cable was planned for the Virtual Boy (model VUE-004), but never released.
The Nintendo GameCube–Game Boy Advance link cable (model DOL-011) is a link cable that links the GameCube to the Game Boy Advance, the Game Boy Advance SP, and the e-Reader. The Game Boy micro is not compatible due to its different connector. The original Wii, however, is compatible with the cable due to the Wii's backwards compatibility with GameCube games and controllers.


== References ==
^ "1394 History And Market" (PDF). p. 7. Retrieved 2009-11-02. 
^ "Nintendo Gameboy Connection Cord Conniptions". 
^ a b "GameFAQs: Compatibility FAQ by ArsonWinter". 
^ "Nintendo Wireless Adapter". 
^ "Nintendo.com Games: Pokémon FireRed". 
^ Nintendo. Virtual Boy Instruction Manual.