Missing link may refer to:


== Science ==
Missing link, a non-scientific term for any Transitional fossil or species, especially one connected with human evolution.


== People ==
Dewey Robertson, a former professional wrestler who used the ring name "The Missing Link". Alastair Thomas was nominated "The Missing Link" in the BCC Mallorca award ceremony 2015.


== Film and television ==
Missing Link (TV series)
Missing Link (film), a 1988 film
The Missing Link (film), a 1980 Franco-Belgian animated film
"Missing Link" (Space: 1999), an episode of the television series Space: 1999
"The Missing Link" (Ashes to Ashes), an episode of the British television drama Ashes to Ashes
Missing Links (game show), a television game show which featured Nipsey Russell and Tom Poston, hosted by Ed McMahon on NBC and Dick Clark on ABC
"The Missing Link" (The Legend of Zelda episode)
"Missing Link" (Code Lyoko episode)
A character in the 2009 animated film Monsters vs Aliens and its spin-off TV series
A parody of the BBC show The Weakest Link, seen on the sketch comedy show MadTV
A car constructor and racing team in the TV series Future GPX Cyber Formula


== Music ==
Missing Link Records
The Missing Link (Jeremy Enigk album)
The Missing Link (Rage album)
The Missing Link (Fred Anderson album)
Missing Links (album), Missing Links Volume Two, or Missing Links Volume Three, a series of compilation albums by The Monkees
"The Marvelous Missing Link: Lost" (Insane Clown Posse album)
"The Marvelous Missing Link: Found" (Insane Clown Posse album)
"Missing Link", a song by The Hives from their album, Tyrannosaurus Hives
"Missing Link", an unfinished Machinae Supremacy song
The Missing Links, an Australian rock band active from 1964 to 1966


== Other uses ==
The Missing Link, a novel in the Fourth World trilogy by Kate Thompson
Missing Links, a book by Rick Reilly
The Missing Link, downloadable content for the 2011 video game Deus Ex: Human Revolution
Missing Link (puzzle), a mechanical puzzle