Little is a surname in the English language. The name is ultimately derived from the Middle English littel, and the Old English lȳtel, which mean "little". In some cases the name was originally a nickname for a little man. In other cases, the name was used to distinguish the younger of two bearers of the same personal name. Early records of the name include: Litle, in 972; Litle, in about 1095; and le Lytle, in 1296. The surname has absorbed several non English-language surnames. For example, Little is sometimes a translation of the Irish Ó Beagáin, meaning "descendant of Beagán". Little can also be a translation of the French Petit and Lepetit, as well as other surnames in various languages with the same meaning ("little"), especially the German name Klein during World War II.


== People ==
Andrew Little (disambiguation), several people
Angela Little (disambiguation), several people
Ann Little (1891–1984), American silent film actress
Anna Little (disambiguation), several people
Annie Little, American singer
Arthur Dehon Little (1863–1935), American chemical engineer, founder of consulting company Arthur D. Little
Bentley Little (born 1960), American author
Betty Little (born 1940), New York senator
Booker Little (1938–1961), American jazz trumpeter
Brian Little (disambiguation), several people
Bryan Little (disambiguation), several people
Carlo Little (1938–2005), British rock and roll drummer
Chad Little (born 1963), NASCAR driver
Cleavon Little (1939–1992), American film and theatre actor
David Little (disambiguation), several people
Donald Little, Canadian judge
Emily Little (born 1994), Australian gymnast
Elbert Luther Little, (1907 - 2004), American botanist
Floyd Little (born 1942), American professional footballer
Frances Little (1863–1941), pseudonym of American author Fannie Caldwell
Frank Little (disambiguation), several people
George Little (disambiguation), several people
Gerry P. Little, New Jersey politician
Grady Little (born 1950), former Major League Baseball manager
Henry Little (disambiguation), several people
Ian Little, record producer
Ian Little (footballer) (born 1973), Scottish football player and manager
Jack Little (disambiguation), several people
James Little (disambiguation), several people
Jason Little (disambiguation), several people
Jean Little (born 1932), Canadian author
Joan Little (born 1953), African-America acquitted of murder
John Little (disambiguation), several people
Joseph James Little (1841–1913), US representative from New York, printer and publisher
Ken Little (born 1947), modernist American sculptor
Lawson Little (1910–1968), American professional golfer
Lewis Henry Little (1817–1862), Confederate brigadier general during the American Civil War
Malcolm Little (1925–1965), better known as Malcolm X
Mark Little (disambiguation), several people
Neil Little (born 1971), Canadian former ice hockey player
Nicky Little (born 1976), Fijian rugby union player
Ralf Little (born 1980), English actor and comedian
Rich Little (born 1938), Canadian-American impressionist and voice actor
Richard Little (disambiguation), several people
Ricky Little (born 1989), Scottish footballer
Robert Little (disambiguation), several people
Russell M. Little (1809–1891), New York politician
Sally Little (born 1951), South African LPGA Golfer and multiple winner
Stephen Little (born 1954), American Asian art scholar, museum administrator and artist
Steve Little (disambiguation), several people
Syd Little (born 1942), English comedian
Tandy Little (born 1921), American politician
Tasmin Little (born 1965), English violinist
Thomas Little (disambiguation), several people
Tony Little (born 1956), American TV fitness personality
Walter Little (disambiguation), several people
William Little (disambiguation), several people
Zarah Little (born 1981), American actress


== Fictional characters ==
Bingo Little, in P. G. Wodehouse stories
Stuart Little, fictional mouse in many of E. B. White's short stories, and in some films


== See also ==
Clan Little, a Scottish Border clan
Chicken Little (2005 film)
List of people known as the Little
Lyttle, a surname


== References ==