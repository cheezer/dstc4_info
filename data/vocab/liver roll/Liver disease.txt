Liver disease (also called hepatic disease) is a type of damage to or disease of the liver.


== Associated medical conditions ==


=== Types ===

There are more than a hundred kinds of liver disease, these are some of the most common:
Fascioliasis, a parasitic infection of liver caused by a Liver fluke of the Fasciola genus, mostly the Fasciola hepatica.
Hepatitis, inflammation of the liver, is caused by various viruses (viral hepatitis) also by some liver toxins (e.g. alcoholic hepatitis), autoimmunity (autoimmune hepatitis) or hereditary conditions.
Alcoholic liver disease is a hepatic manifestation of alcohol overconsumption, including fatty liver disease, alcoholic hepatitis, and cirrhosis. Analogous terms such as "drug-induced" or "toxic" liver disease are also used to refer to disorders caused by various drugs.
Fatty liver disease (hepatic steatosis) is a reversible condition where large vacuoles of triglyceride fat accumulate in liver cells. Non-alcoholic fatty liver disease is a spectrum of disease associated with obesity and metabolic syndrome.
Hereditary diseases that cause damage to the liver include hemochromatosis, involving accumulation of iron in the body, and Wilson's disease. Liver damage is also a clinical feature of alpha 1-antitrypsin deficiency and glycogen storage disease type II.
In transthyretin-related hereditary amyloidosis, the liver produces a mutated transthyretin protein which has severe neurodegenerative and/or cardiopathic effects. Liver transplantation can give a curative treatment option.
Gilbert's syndrome, a genetic disorder of bilirubin metabolism found in a small percent of the population, can cause mild jaundice.
Cirrhosis is the formation of fibrous tissue (fibrosis) in the place of liver cells that have died due to a variety of causes, including viral hepatitis, alcohol overconsumption, and other forms of liver toxicity. Cirrhosis causes chronic liver failure.
Primary liver cancer most commonly manifests as hepatocellular carcinoma and/or cholangiocarcinoma; rarer forms include angiosarcoma and hemangiosarcoma of the liver. (Many liver malignancies are secondary lesions that have metastasized from primary cancers in the gastrointestinal tract and other organs, such as the kidneys, lungs.)
Primary biliary cirrhosis is a serious autoimmune disease of the bile capillaries.
Primary sclerosing cholangitis is a serious chronic inflammatory disease of the bile duct, which is believed to be autoimmune in origin.
Budd–Chiari syndrome is the clinical picture caused by occlusion of the hepatic vein.


== Mechanism ==
Liver disease can occur through several mechanisms. A common form of liver disease is viral infection. Viral hepatitides such as Hepatitis B virus and Hepatitis C virus can be vertically transmitted during birth via contact with infected blood. According to a 2012 NICE publication, "about 85% of hepatitis B infections in newborns become chronic". In occult cases, Hepatitis B virus is present by HBV DNA, but testing for HBsAg is negative. High consumption of alcohol can lead to several forms of liver disease including alcoholic hepatitis, alcoholic fatty liver disease, cirrhosis, and liver cancer. In the earlier stages of alcoholic liver disease, fat builds up in the liver's cells due to increased creation of triglycerides and fatty acids and a decreased ability to break down fatty acids. Progression of the disease can lead to liver inflammation from the excess fat in the liver. Scarring in the liver often occurs as the body attempts to heal and extensive scarring can lead to the development of cirrhosis in more advanced stages of the disease. Approximately 3-10% of individuals with cirrhosis develop a form of liver cancer known as hepatocellular carcinoma.


== Diagnosis ==
A number of liver function tests (LFTs) are available to test the proper function of the liver. These test for the presence of enzymes in blood that are normally most abundant in liver tissue, metabolites or products. serum proteins, serum albumin, serum globulin, alanine transaminase, aspartate transaminase, prothrombin time, partial thromboplastin time.


== Treatment ==

Anti-viral medications are available to treat infections such as hepatitis B. Other conditions may be managed by slowing down disease progression, for example:
By using steroid-based drugs in autoimmune hepatitis.
Regularly removing a quantity of blood from a vein (venesection) in the iron overload condition, hemochromatosis.
Wilson’s disease, a condition where copper builds up in the body, can be managed with drugs which bind copper allowing it to be passed from your body in urine.
In cholestatic liver disease ,(where the flow of bile is affected due to cystic fibrosis) a medication called ursodeoxycholic acid (URSO, also referred to as UDCA) may be given.


== References ==


== Further reading ==
Friedman, Lawrence S.; Keeffe, Emmet B. (2011-08-03). Handbook of Liver Disease. Elsevier Health Sciences. ISBN 1455723169.