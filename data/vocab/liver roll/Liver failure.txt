Liver failure or hepatic insufficiency is the inability of the liver to perform its normal synthetic and metabolic function as part of normal physiology. Two forms are recognised, acute and chronic.


== Acute ==

Acute liver failure is defined as "the rapid development of hepatocellular dysfunction, specifically coagulopathy and mental status changes (encephalopathy) in a patient without known prior liver disease".:1557
The diagnosis of acute liver failure is based on physical exam, laboratory findings, patient history, and past medical history to establish mental status changes, coagulopathy, rapidity of onset, and absence of known prior liver disease respectively.:1557
The exact definition of "rapid" is somewhat questionable, and different sub-divisions exist which are based on the time from onset of first hepatic symptoms to onset of encephalopathy. One scheme defines "acute hepatic failure" as the development of encephalopathy within 26 weeks of the onset of any hepatic symptoms. This is sub-divided into "fulminant hepatic failure", which requires onset of encephalopathy within 8 weeks, and "subfulminant", which describes onset of encephalopathy after 8 weeks but before 26 weeks. Another scheme defines "hyperacute" as onset within 7 days, "acute" as onset between 7 and 28 days, and "subacute" as onset between 28 days and 24 weeks.:1557


== Chronic ==

Chronic liver failure usually occurs in the context of cirrhosis, itself potentially the result of many possible causes, such as excessive alcohol intake, hepatitis B or C, autoimmune, hereditary and metabolic causes (such as iron or copper overload, Steatohepatitis or non-alcoholic fatty liver disease).


== References ==