Lloyds Bank plc is a British retail and commercial bank with branches across England and Wales. It has traditionally been considered one of the "Big Four" clearing banks. Originally founded in Birmingham in 1765, the bank expanded during the nineteenth and twentieth centuries and took over a number of smaller banking companies. In 1995 it merged with the Trustee Savings Bank and traded as Lloyds TSB Bank plc between 1999 and 2013.
The bank is the principal subsidiary of Lloyds Banking Group, which was formed in January 2009 by the acquisition of HBOS by the then-Lloyds TSB Group. That year, following the UK bank rescue package, the British Government took a 43.4% stake in Lloyds Banking Group. As a condition imposed by the European Commission regarding state aid, the group later announced that it would create a new standalone retail banking business, made up of a number of Lloyds TSB branches and those of Cheltenham & Gloucester. The new business began operations on 9 September 2013 under the TSB brand. Lloyds TSB was subsequently renamed Lloyds Bank on 23 September 2013.
Lloyds Bank has an extensive network of branches and ATM in England and Wales (as well as an arrangement for its customers to be serviced by Bank of Scotland branches in Scotland, Halifax branches in Ireland and vice versa) and offers 24-hour telephone and online banking services. As of 2012 it has 16 million personal customers and small business accounts.
It has its operational Headquarters in London with other offices in Wales and Scotland. It also operates a number of office complex, brand headquarters and data centres in Yorkshire including Leeds, Sheffield and Halifax.


== History ==


=== Origins ===

The origins of Lloyds Bank date from 1765, when button maker John Taylor and iron producer and dealer Sampson Lloyd II set up a private banking business in Dale End, Birmingham. The first branch office opened in Oldbury, some six miles (10 km) west of Birmingham, in 1864. The symbol adopted by Taylors and Lloyds was the beehive, representing industry and hard work. The black horse device dates from 1677, when Humphrey Stokes adopted it as sign for his shop. Stokes was a goldsmith and "keeper of the running cashes" (an early term for banker) and the business became part of Barnett, Hoares & Co. When the bank took over that bank in 1884, it retained the black horse as its symbol.
The association with the Taylor family ended in 1852 and, in 1865, Lloyds & Co. converted into a joint-stock company known as Lloyds Banking Company Ltd.
"LLOYDS BANKING COMPANY LIMITED - Authorized Capital £2,000,000. FOUNDED ON The Private Banks of Messrs. Lloyds & Co. and Messrs. Moilliet and Sons, with-which have subsequently been amalgamated the Banks of Messrs. P. H. Williams, Wednesbury, and Messrs. Stevenson, Salt, & Co., Stafford and Lichfield. {They had an office at 20 Lombard St., London}
Your Directors have the satisfaction to report that they have concluded an agreement with the well-known and old-established firm of Messrs. Stevenson, Salt & Company for the amalgamation with this Company of their Banking Business at Stafford, Lichfield, Rugeley, and Eccleshall, and that this agreement has had the unanimous approval of the Extraordinary General Meeting held on 31st January last. It will be again submitted to you for final confirmation after the close of the Ordinary General Meeting.
TIMOTHY KENRICK, Chairman. BIRMINGHAM, 9th February 1866. [First Report of the Company - 1865]"
Two sons of the original partners followed in their footsteps by joining the established merchant bank Barnett, Hoares & Co. which later became Barnetts, Hoares, Hanbury and Lloyd— based in Lombard Street, London. Eventually, this became absorbed into the original Lloyds Banking Company, which became Lloyds, Barnetts and Bosanquets Bank Ltd. in 1884. and, finally, Lloyds Bank Limited in 1889.


=== Expansion ===

Through a series of mergers, including Cunliffe, Brooks in 1900, the Wilts. and Dorset Bank in 1914 and, by far the largest, the Capital and Counties Bank in 1918, Lloyds emerged to become one of the "Big Four" clearing banks in the United Kingdom. By 1923, Lloyds Bank had made some 50 takeovers, one of which was the last private firm to issue its own banknotes—Fox, Fowler and Company of Wellington, Somerset. Today, the Bank of England has a monopoly of banknote issue in England and Wales. In 2011, the company founded SGH Martineau LLP.
In 1968, a failed attempt at merger with Barclays and Martins Bank was deemed to be against the public interest by the Monopolies and Mergers Commission. Barclays finally acquired Martins the following year. In 1972, Lloyds Bank was a founding member of the Joint Credit Card Company (with National Westminster Bank, Midland Bank and Williams & Glyn's Bank) which launched the Access credit card (now MasterCard). That same year it introduced Cashpoint, the first online cash machine to use plastic cards with a magnetic stripe. In popular use, the Cashpoint trademark has become a generic term for an ATM in the United Kingdom.
Under the leadership of Sir Brian Pitman between 1984 and 1997, the bank's business focus was narrowed and it reacted to disastrous lending to South American states by trimming its overseas businesses and seeking growth through mergers with other UK banks. During this period, Pitman tried unsuccessfully to acquire The Royal Bank of Scotland in 1984, Standard Chartered in 1986, and Midland Bank in 1992. Lloyds Bank International merged into Lloyds Bank in 1986, since there was no longer an advantage in operating separately. In 1988, Lloyds merged five of its businesses with the Abbey Life Insurance Company to create Lloyds Abbey Life.


=== Lloyds TSB ===

The bank merged first with the newly demutualised Cheltenham & Gloucester Building Society (C&G), then with the TSB Group in 1995. The C&G acquisition gave Lloyds a large stake in the UK mortgage lending market. The TSB merger was structured as a reverse takeover; Lloyds Bank Plc was delisted from the London Stock Exchange and TSB Group plc was renamed Lloyds TSB Group plc on 28 December, with former Lloyds Bank shareholders owning a 70% equity interest in the share capital, effected through a scheme of arrangement. The new bank commenced trading in 1999, after the statutory process of integration was completed. On 28 June, TSB Bank plc transferred engagements to Lloyds Bank Plc which then changed its name to Lloyds TSB Bank plc; at the same time, TSB Bank Scotland plc absorbed Lloyds' three Scottish branches becoming Lloyds TSB Scotland plc. The combined business formed the largest bank in the UK by market share and the second-largest to Midland Bank (now HSBC) by market capitalisation.
Lloyds Abbey Life became a wholly owned subsidiary of the group in 1996, absorbing Hill Samuel in 1997, before closing to new business in 2000. In 2007, Abbey Life was sold to Deutsche Bank for £977 million.
In 1999, the group agreed to buy the Scottish Widows Fund and Life Assurance Society for £7 billion. The society demutualised in 2000, shortly before the acquisition was completed. In 2001, Lloyds TSB made a bid to acquire Abbey National; however, the bid was blocked by the Competition Commission, who ruled that a merger would be against the public interest.
In October 2011, Lloyds TSB's credit rating was reduced by Moody's from Aa3 to A1. The action was taken in the light of a shift in government policy to move risk from taxpayers to creditors by reducing the level of support offered to financial institutions.
Lloyds TSB was the first Official Partner for the 2012 Summer Olympics in London.


=== Divestment and return to Lloyds Bank ===

After the 2008 rescue of HBOS, Lloyds TSB Group was renamed Lloyds Banking Group. In 2009, following the liquidity crisis, HM Government took a 43.4% stake in Lloyds Banking Group. The European Commission ruled that the group must sell a portion of its business by November 2013, as it categorised the stake purchase as state aid.
On 24 April 2013, it was confirmed that a number of Lloyds TSB branches in England and Wales would be combined with the branches of Cheltenham & Gloucester and the business of Lloyds TSB Scotland to form a new bank operating under the TSB brand and divested by the group. The selected Lloyds TSB branches and those of Cheltenham & Gloucester were transferred to Lloyds TSB Scotland plc, which was renamed TSB Bank plc and is due to be divested through an initial public offering in 2014. The new bank began operating on 9 September 2013 as a separate division within Lloyds Banking Group. The remaining business of Lloyds TSB returned to the Lloyds Bank name on 23 September 2013.
In October 2014, the bank announced that it planned to cut 9,000 jobs and close some branches in light of an increase in the number of customers using online banking services.


== Services ==

The bank offers a full range of banking and financial services, through a network of 1,300 branches in England and Wales. Branches in Jersey, Guernsey and the Isle of Man are operated by Lloyds Bank International Limited, while Lloyds Bank (Gibraltar) Limited operates in Gibraltar; both are wholly owned subsidiaries and trade under the Lloyds Bank brand. Lloyds Bank is authorised by the Prudential Regulation Authority and regulated by both the Financial Conduct Authority and the Prudential Regulation Authority. It is a member of the Financial Ombudsman Service, the Financial Services Compensation Scheme, UK Payments Administration and of the British Bankers' Association. The bank uses the following series of sort codes:—
The Lloyds Bank Foundation funds local, regional and national charities working to tackle disadvantage across England and Wales. There are separate foundations covering Scotland, Northern Ireland and the Channel Islands.


== Overseas operations ==
By the early 1990s, Lloyds Bank had offices in 30 countries, from Argentina to the United States of America. See Lloyds Bank International for history of these operations and businesses.


== Controversies ==


=== Payment protection insurance ===
In November 2005 an investigation by the Financial Services Authority (FSA) highlighted a lack of compliance controls surrounding payment protection insurance (PPI). A second investigation in October 2006 identified further evidence of poor compliance and major PPI providers including Lloyds were fined for not treating customers fairly. In January 2011 a High Court case began which in the following April ruled against the banks, on 5 May 2011 Lloyds withdrew from the legal challenge. In 2012, Lloyds announced that they had set aside £3.6 billion to cover the cost of compensating customers who were mis-sold PPI.
In March 2014 it was reported that Lloyds had been reducing the compensation they offered by using a regulatory provision called "alternative redress" to assume that customers wrongly sold single-premium PPI policies would have bought a cheaper, regular premium PPI policy instead.
In June 2015 the Lloyds Banking Group was fined £117m for mishandling payment protection insurance claims including many claims being "unfairly rejected".


=== Links to arms trade ===
In December 2008 the British anti-poverty charity War on Want released a report documenting the extent to which the UK high street banks invest in, provide banking services for and lend to arms companies. The report stated that Lloyds TSB is the only high street bank whose corporate social responsibility policy does not mention the arms industry, yet is that industry's second largest shareholder among high street banks.


=== Tax evasion ===
In 2009, the BBC's Panorama alleged that Lloyds TSB Offshore in Jersey, Channel Islands was encouraging wealthy customers to evade tax. An employee of Lloyds was filmed telling a customer how several mechanisms could be used to make their transactions invisible to the UK tax authorities. This action is also in breach of money laundering regulations in Jersey. Lloyds subsequently claimed that this was an isolated incident which they were investigating.


=== Retail conduct failings ===
In December 2013, Lloyds Banking Group had been fined £28m for "serious failings" in relation to bonus schemes for sales staff. The Financial Conduct Authority said it was the largest fine that it or the former Financial Services Authority had imposed for retail conduct failings. The bonus scheme pressured staff to hit sales targets or risk being demoted and have their pay cut, the FCA said. Lloyds Bank has accepted the regulator's findings and apologised to its customers. In February 2014, the Twitter profile @LloydsBankHell began tracking disquiet with consumers towards the bank.


=== Divestment of government-owned shares ===
Based on figures from the National Audit Office, George Osborne's sale of a 6% tranche of Lloyds shares in autumn 2013—despite his claims that the sale had netted a profit—worked out at a loss of at least £230m for UK taxpayers.


=== Credit cards ===
Lloyds refused to issue credit cards to two over-75 year olds with impeccable credit history on the basis that they were too old. Only after Telegraph Money contacted Lloyds, the bank apologised and issued the cards.


=== Libor rate manipulations ===
In July 2014, U.S. and UK regulators slapped a combined £218 million ($370 million) in fines on Lloyds and a number of subsidiaries over the bank's part in the global Libor rate fixing scandal, and other rate manipulations and false reporting.


== See also ==
Lloyds Bank Ltd v Bundy
Lloyds Bank plc v Rosset


== References ==


=== Bibliography ===


== External links ==
Official website
Lloyds TSB Foundation
Lloyds Banking Group
TSB