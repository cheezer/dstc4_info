Local education authorities (LEAs) are the local councils in England and Wales that are responsible for education within their jurisdiction. The term is used to identify which council (district or county) is locally responsible for education in a system with several layers of local government. Local education authorities are not usually ad hoc or standalone authorities, although the former Inner London Education Authority was one example of this.


== Responsible local authority ==


=== England ===
England has several tiers of local government and the relevant local authority varies. Within Greater London the 32 London borough councils and the Common Council of the City of London are the local authorities responsible for education; in the metropolitan counties it is the 36 metropolitan borough councils; and in the non-metropolitan counties it is the 27 county councils or, where there is no county council, the councils of the 55 unitary authorities. The Council of the Isles of Scilly is an education authority. Since the Children Act 2004 each local education authority is also a children's services authority and responsibility for both functions is held by the director of children's services. There are 152 local education authorities in England.


=== Wales ===
In Wales the councils of the counties and county boroughs are responsible for education. Since 5 May 2010, the terms local education authority and children's services authority have been repealed and replaced by the single term 'local authority' in both primary and secondary legislation.


== Functions ==
Local education authorities have some responsibility for all state schools in their area.

They are responsible for distribution and monitoring of funding for the schools
They are responsible for co-ordination of admissions, including allocation of the number of places available at each school
They are the direct employers of all staff in community and VC schools
They have a responsibility for the educational achievement of looked-after children, i.e. children in their care
They have attendance and advisory rights in relation to the employment of teachers, and in relation to the dismissal of any staff
They are the owners of school land and premises in community schools.
Until recently, local education authorities were responsible for the funding of students in higher education (for example undergraduate courses and PGCE) whose permanent address is in their area, regardless of the place of study. Based on an assessment of individual circumstances they offer grants or access to student loans through the Student Loans Company.


== History ==


=== Creation ===
The term was introduced by the Education Act 1902 (2 Edw.7, c. 42). The Act designated each local authority; either county council and county borough council; would set up a committee known as a local education authority (LEA). The councils took over the powers and responsibilities of the school boards and technical instruction committees in their area. Municipal boroughs with a population of 10,000 and urban districts with a population of 20,000 were to be local education authorities in their areas for elementary education only. The LEAs' role was further expanded with the introduction of school meals in 1906 and medical inspection in 1907.
In 1904 the London County Council became a local education authority, with the abolition of the London School Board. The metropolitan boroughs were not education authorities, although they were given the power to decide on the site for new schools in their areas, and provided the majority of members on boards of management.


=== Reform ===
The system continued unchanged until 1965, when the London County Council was replaced by the Greater London Council. The twenty outer London boroughs became local education authorities, while a new Inner London Education Authority, consisting of the members of the GLC elected for the inner boroughs covering the former County of London was created.
In 1974 local government outside London was completely reorganised. In the new metropolitan counties of England, metropolitan boroughs became LEAs. In the non-metropolitan counties the county councils were the education authorities, as they were throughout Wales.
In 1986, with the abolition of the Greater London Council, a directly elected Inner London Education Authority was formed. This however only existed until 1990, when the twelve inner London boroughs assumed responsibility for education.
In 1989, under the Education Reform Act 1988, the LEAs lost responsibility for higher education, with all polytechnics and colleges of higher education becoming independent corporations.
A further wave of local government reorganisation during the 1990s led to the formation of unitary authorities in parts of England and throughout Wales, which became local education authorities.
A local educational authority award is an award given to the local educational authority, as opposed to an award given by the LEA.


== List of local authorities responsible for education ==
There are currently 152 local education authorities in England and 22 in Wales. Below they are listed alphabetically by region.


== See also ==
Special education
Dyslexia support in the United Kingdom


== References ==


== External links ==
DfES: The Role of the Local Education Authority in School Education