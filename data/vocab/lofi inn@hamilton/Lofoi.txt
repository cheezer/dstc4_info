Lofoi (Greek: Λόφοι) is a village in the Florina peripheral unit, Greece, located 15 km east of the city of Florina. Its name in Macedonian Slavic is Заб'рдени (Zab'rdeni). It was also known as Ζαπύρδανη (Zapyrdani) or Ζαμπύρδανη (Zampyrdani) in Greek, until it was renamed by Greek authorities in 1926.


== History ==
First mentioned in an Ottoman defter of 1481, the village, then known as Zabrdani, had eighty households and produced vines, walnuts, and honey. In Ottoman tax registers of the non-Muslim population of the wilayah Filorine from 1626–1627, the village is marked under the name Zaburdani with 62 households.
The population of the village was affiliated with the Bulgarian Exarchate church in the early 20th century. According to Dimitar Mishev, the secretary of the Exarchate, there were 344 Bulgarians in Zabrdani in 1905 and there was a Bulgarian school that functioned in the village.
The village was incorporated into Greece in 1913 after the Balkan Wars.


== Demography ==
According to the 2001 census, the population of Lofi is 443 people. The population is largely from a Macedonian Slavic-speaking background.


== References ==