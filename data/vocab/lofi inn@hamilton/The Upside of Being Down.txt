The Upside of Being Down is the debut studio album by US blues band Marcy Levy Band, fronted by Marcella Detroit. The album was released by Detroit's independent label Lofi Records in July 2006, following their debut EP Button Fly Blues in 2003.


== Track listing ==


== Personnel ==
Adapted from album booklet:
Rick Reed - Bass on track 2
Max Bangwell - Drums
Roby Duron - Engineer & Mixer
Job Striles - Guitar
Marcy Levy - Guitar & Vocals
Micheal Fell - Harmonica
Brad Vance - Mastering
Marcy Levy, Michael Fell - Producers
Lance Aston - Artwork
Recorded at The Blues Bunker, 2005


== References ==