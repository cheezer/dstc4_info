Pekan Air Panas is a small town in the district of Segamat, Johor, Malaysia, which is named after the hot springs available there. It is located about 14 km from Labis via Jalan Ayer Panas (Johor State Route J151). The town is also known as Tenang (not to be confused with Tenang Station, another town also located in Segamat District). A majority of the residents of the town are Chinese and Malay communities.
Pekan Air Panas is famed for its hot springs. Besides the hot springs, the town also houses several aborigine settlements (Perkampungan Orang Asli) including Bekok and also has mata kucing farms. Mata kucing (aka longan) is a small lychee-like local fruit with brown skin.


== Etymology ==
Pekan Air Panas is named after the hot springs available near the town. The town is also known as Bukit. The settlement was opened on Thursday, 3 December 1806. The name Tenang comes from settler who first opened the settlement, Tuan Haji Muhammad Arifin bin Aladin when he found the new settlement area, which was so serene (hence the Malay word Tenang). After he perform his prayer, he decided to name this place with name Kampung Tenang (next to Pekan Air Panas), he also known as Tok Tenang. But most of business and facilities was located at Pekan Air Panas.


== Education ==
Educational institutions in Pekan Air Panas encompass only primary education. There are 1 national primary schools (Sekolah Kebangsaan) whose medium of instruction is Malay language and 1 Chinese primary schools (Sekolah Jenis Kebangsaan).
SJK (C) Tenang (登能华小)
SK Pekan Air Panas


== More photos ==


== See also ==
Segamat
Labis
Therapeutic lure of hot spring- Allmalaysia.info - Pekan Air Panas.
Geographic Coordinate


== References ==