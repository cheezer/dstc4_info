Masjid Ahmad, or Ahmad Mosque is a mosque in Singapore located in the Buona Vista area, at the junction of South Buona Vista Road and Lorong Sarhad. Originally a typical kampung mosque built in 1934 on a piece of land given by a Mr Ahmat Yahya, the mosque has been redeveloped with better facilities. It serves the residents and workers around the area.


== See also ==
Islam
Timeline of Islamic history
List of mosques
Religion in Singapore
Islam in Singapore
List of mosques in Singapore


== External links ==
Ahmad Mosque
Majlis Ugama Islam Singapura, MUIS (Islamic Religious Council of Singapore)
List of Mosques in Singapore managed by MUIS : Masjid Ahmad
IslamicEvents.SG, Portal for Islamic events, courses and classes in Singapore
GoogleMaps StreetView of Masjid Ahmad