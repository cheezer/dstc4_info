Buangkok is an area located in the North-East Region of Singapore. It encompasses an area of Hougang New Town and the medical hub of Buangkok Green Medical Park that includes the Institute of Mental Health north of Buangkok Green, and the public residential areas in the neighbourhood of Compassvale in Sengkang New Town north of Buangkok Drive. It was once filled and overcrowded by kampongs and villages that have been demolished just in 2009.


== Etymology and history ==
Chinese farmers settled on the land in this vicinity in the early twentieth century. The land belonged partly to the state and partly to Singapore United Rubber Plantation Limited. The namesake road Lorong Buang Kok was named buang kok, meaning "united", after the rubber plantation by the settlers. In 1967, a track off Lorong Buang Kok was named Lorong Buang Kok Kechil.
When Sengkang New Town was developed in the late 1990s, a large section of Lorong Buang Kok was removed for the development of the housing new town. Today, Lorong Buang Kok is truncated into two sections — one located at the west end of Buangkok with its entrance near Yio Chu Kang Road, and the other much shorter section located within Sengkang New Town near Punggol Road.


== Public housing estates ==
The public housing estates in Buangkok include a cluster of apartment blocks at Buangkok Link in the northern part of Hougang New Town and the southern part of Compassvale in Sengkang New Town.
The housing estates in Compassvale are as follows:
Compassvale Arcadia, Compassvale Link - Block nos. 267A-B, 268A-D & 269A-D
Aspella, Compassvale Link - Block nos. 275A-D & 277A-D
Atrina, Sengkang Central & Compassvale Link - Block nos. 272A-D & 273A-D
Coris 1 & 2, Compassvale Bow & Compassvale Link - Block nos. 264A-F, 265A-E & 266A-C
Tivela, Sengkang Central - Block nos. 270A & 271A-C


== Private Condominium ==
There are a few private condominiums around buangkok area:
1. the Quartz is a 99 years leasehold which developed by GuocoLand and T.O.P in 2010
2. Jewel @ Buangkok, new development by CDL T.O.P. in 2017, also have 99 years leasehold tenure


== Transport ==
Buangkok MRT Station (NE15) on the North East Line serves the residents of Buangkok. It was opened on 15 January 2006 due to popular demand.


== References ==
Victor R Savage, Brenda S A Yeoh (2003), Toponymics - A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1