Seputeh is an area and parliamentary constituency in Kuala Lumpur, Malaysia. Mid Valley Megamall, Malaysia's largest shopping complex is located here. The current Member of Parliament representing the constituent is Teresa Kok Suh Sim from Democratic Action Party. One of the well known residential condominium, Le Chateau I is also located here at Lorong Syed Putra Kiri.


== Townships ==
Bukit Jalil
Taman Salak Selatan
Kampung Baru Salak Selatan
Taman Desa
Taman Danau Desa
Taman Abadi Indah
Taman Gembira (Happy Garden)
Taman Overseas Union
Taman Bandaraya
Sungai Besi
Bandar Baru Seri Petaling