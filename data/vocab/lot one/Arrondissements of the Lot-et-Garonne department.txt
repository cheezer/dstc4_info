The 4 arrondissements of the Lot-et-Garonne department are:
Arrondissement of Agen, (prefecture of the Lot-et-Garonne department: Agen) with 71 communes. The population of the arrondissement was 102,162 in 1990, and was 105,380 in 1999, an increase of 3.15%.
Arrondissement of Marmande, (subprefecture: Marmande) with 98 communes. The population of the arrondissement was 77,806 in 1990, and was 76,169 in 1999, a decrease of 2.1%.
Arrondissement of Villeneuve-sur-Lot, (subprefecture: Villeneuve-sur-Lot) with 92 communes. The population of the arrondissement was 88,574 in 1990, and was 86,850 in 1999, a decrease of 1.95%.
Arrondissement of Nérac, (subprefecture: Nérac) with 58 communes. The population of the arrondissement was 37,447 in 1990, and was 36,981 in 1999, a decrease of 1.24%.