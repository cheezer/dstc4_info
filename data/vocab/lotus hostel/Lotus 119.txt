Lotus 119 was a box car built by Lotus Cars to compete at the Soapbox Challenge that took place at the 2002 Goodwood Festival of Speed. It is believed to be the fastest box car built capable of 200 mph (320 km/h) on a 45 degree slope. Several types were built, with the 119c at present being the fastest.


== References ==
^ LOTUS
^ Lotus Gravity Racers or Soap Box Derby Cars
^ Lotus 119c