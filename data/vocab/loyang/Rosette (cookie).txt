A rosette (called struva in Swedish) is a thin, cookie-like deep-fried pastry of Scandinavian (Swedish and Norwegian) origin. Rosettes are traditionally made during Christmas time. They are made using intricately designed irons. The iron is heated to a very high temperature in oil, dipped into the batter, then re-immersed in the hot oil to create a crisp shell around the metal. The iron is immediately removed and the rosette is separated from the iron. Usually, the edges of the rosette are dipped into frosting or sugar. Rosette recipes are popular in the United States among families with Scandinavian ancestry.
In Finland, rosettes may be served at May Day (Vappu) celebrations as an alternative to funnel cakes (tippaleipä).
Rosettes are a traditional pastry in Turkey, where they are known as demir tatlısı (iron dessert, in reference to the moulds, which are made of cast or sheet iron). These pastries are also made in Iran, where they are called nan panjara, Mexico where they're called buñuelos and Colombia where they are known as "solteritas". They are also made in the southern state of India. The Christian community of the southern state Kerala make achappam during Christmas and special occasions. A similar form is available in Sri Lanka as well, which is called kokis.
In Malaysia, a similar pastry is called kuih loyang (brass cakes, named for the brass moulds), kuih ros (rose cakes), 蜂窝饼 (beehive or honeycomb) cookies.  Perhaps influenced by the Dutch colonials, the Malaysian version includes coconut milk in addition to flour, sugar and eggs, similar to the Sri Lankan kokis.


== References ==
^ Isin, Priscilla Mary (2008), Gülbeşeker: Türk Tatlıları Tarihi", YKY, 16-17
^ "Beehive Cookies (Kuih Rose) 蜂窝饼 - Chinese New Year Series". February 5, 2013. Retrieved March 28, 2014. 


== See also ==
Kokis
Bamiyeh
Krumkake
Cuisine of Norway
Cuisine of Sweden
Lefse
Christmas cookies