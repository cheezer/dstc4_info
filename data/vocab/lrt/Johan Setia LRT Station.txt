The Johan Setia LRT station is designated to be an elevated rapid transit station in Johan Setia, Klang, Selangor, Malaysia, forming part of the LRT 3 or Bandar Utama - Klang LRT line. 
The station is marked as Station No. 25 which is also the terminus for the RM 9 billion LRT 3 line project. The Bandar Utama-Klang Line's maintenance depot is located next to the Johan Setia LRT station. The Johan Setia LRT station is expected to be operational in August 2020. 


== Locality landmarks ==
Bandar Parklands (Bandar Bukit Tinggi 3)
Bandar Bestari (Canary Garden)
Kota Bayuemas
SAZEAN Business Park


== Gallery ==


== External links ==
Official LRT 3 project website
LRT 3 project video
Prasarana Malaysia Berhad, LRT 3 operator


== References ==