Lucky Man may refer to:


== Music ==
Lucky Man (Dave Koz album) (1993)
Lucky Man (Charlie Major album) (1995)
"Lucky Man" (Emerson, Lake & Palmer song) (1970)
"Lucky Man" (The Verve song) (1997)
"Lucky Man" (Montgomery Gentry song) (2007)
Lucky Man, an album by Hal Ketchum
"Lucky Man", a song by the Steve Miller Band from Sailor
"Lucky Man", a song by Bruce Springsteen from Tracks
"Lucky Man", a song by Arashi from How's It Going?
"Lucky Man", a 2003 song by Lynyrd Skynyrd from Vicious Cycle


== Other uses ==
Lucky Man (film), a 1995 Tamil film starring Karthik Muthuraman
Lucky Man: A Memoir, a 2002 autobiography by Michael J. Fox


== See also ==
Tottemo! Luckyman, a 1993 Japanese manga and anime series
O Lucky Man!, a 1973 film