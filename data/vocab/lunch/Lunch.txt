Lunch is a midday meal of varying size depending on the culture. The origin of the words lunch and luncheon relate to a small meal originally eaten at any time of the day or night, but during the 20th century gradually focused toward a small or mid-sized meal eaten at midday. Lunch is the second meal of the day after breakfast.


== History ==
The abbreviation lunch, in use from 1823, is taken from the more formal Northern English word luncheon, which the Oxford English Dictionary (OED) reports from 1580 as describing a meal that was inserted between more substantial meals. It may also mean a piece of cheese or bread.
In medieval Germany, there are references to similariar, a sir lunchentach according to the OED, a noon draught – of ale, with bread – an extra meal between midday dinner and supper, especially during the long hours of hard labour during haying or early harvesting. In general, during the Middle Ages the main meal for almost everyone took place at midday where there was no need for artificial lighting. During the 17th and 18th century the dinner was gradually pushed back into the evening, leaving a wider gap between it and breakfast that came to be filled in with lunch. A formal evening meal, artificially lit by candles, sometimes with entertainment, was a "supper party" as late as the Regency era.
Up until the early 19th century, luncheon was generally reserved for the ladies, who would often have lunch with one another when their husbands were out. As late as 1945, Emily Post wrote in the magazine Etiquette that luncheon is "generally given by and for women, but it is not unusual, especially in summer places or in town on Saturday or Sunday, to include an equal number of men" – hence the mildly disparaging phrase, "the ladies who lunch". Lunch was a ladies' light meal; when the Prince of Wales stopped to eat a dainty luncheon with lady friends, he was laughed at for this effeminacy.

Afternoon tea supplemented this luncheon at four o'clock, from the 1840s. Mrs Beeton's Book of Household Management (1861) – a guide to all aspects of running a household in Victorian Britain, edited by Isabella Beeton – had much less to explain about luncheon than about dinners or ball suppers:
The remains of cold joints, nicely garnished, a few sweets, or a little hashed meat, poultry or game, are the usual articles placed on the table for luncheon, with bread and cheese, biscuits, butter, etc. If a substantial meal is desired, rump-steaks or mutton chops may be served, as also veal cutlets, kidneys... In families where there is a nursery, the mistress of the house often partakes of the meal with the children, and makes it her luncheon. In the summer, a few dishes of fresh fruit should be added to the luncheon, or, instead of this, a compote of fruit or fruit tart, or pudding.


=== The modern lunch ===
With the onset of industrialization in the 19th century, male workers began to work long shifts at the factory, severely disrupting the age-old eating habits of the rural life. Initially, workers were sent home for a brief dinner provided by their wives, but as the workplace was removed farther from the home, working men took to providing themselves with something portable to eat at a break in the schedule during the middle of the day.
The lunch meal slowly became 'institutionalised' in England when workers with long and fixed hour jobs at the factory were eventually officially given an hour off of work as a lunch-break, to give them strength for the afternoon shift. Stalls and later chop houses near the factories began to provide mass-produced food for the working class and the meal soon became an established part of the daily routine, remaining so to this day.
In many countries and regions lunch remains the dinner or main meal. (See following section.) Prescribed lunchtimes allow workers to return to their homes to eat with their families. Consequently, where lunch is the customary main meal of the day, businesses will close during lunchtime. Lunch also becomes dinner for special days, such as holidays or special events, such as Christmas dinner and harvest dinners such as Thanksgiving, which are usually served in early afternoon. Among Christians, the main meal on Sunday is "Sunday dinner," at a restaurant or home, after morning church services. Whenever dinner is the midday meal, the evening meal, or supper, assumes the function of the light-meal lunch.


== Around the world ==


=== Asia ===
A traditional Bengali lunch is a seven-course meal. The first course being shukto, which is a mix of vegetables cooked with few spices and topped with coconut icing. The second course consists of rice, dal, and a vegetable curry. The third course consists of rice and fish curry. The fourth course is that of rice and meat curry (generally chevon, mutton, chicken or lamb). The fifth course contains sweet preparations like rasgulla, pantua, rajbhog, sandesh, etc. The sixth course consists of payesh or mishti doi (sweet yogurt). The seventh course is that of paan, which acts as a mouth freshener.
In China today lunch is not nearly as complicated as it was before its industrialization. Rice, noodles and other mixed hot foods are often eaten, either at a restaurant or brought in a container. However, Western cuisine is not uncommon either. It is called 午餐 or 午饭 in most areas.


=== Europe ===

In Bosnia and Herzegovina, it is the main meal of the day. It is traditionally a substantial hot meal, sometimes adding additional courses like soup and dessert. It is usually a savoury dish, consisting of protein (e.g., meat), starchy foods (e.g., potatoes) and vegetables or salad. It is usually eaten around 2:00 PM.
In Denmark, lunch consists of a light meal. Often it would be rye bread with different toppings like liver pâté, herring and cheese.
In Finland, lunch is a full hot meal, served as one course optionally with small salads and desserts. Dishes are diverse, ranging from meat or fish courses to soups heavy enough as standalone meals.
In France, the midday meal is taken between noon and 2:00 p.m.
In Germany it is the main meal of the day. It is traditionally a substantial hot meal, sometimes adding additional courses like soup and dessert. It is usually a savoury dish, consisting of protein (e.g., meat), starchy foods (e.g., potatoes) and vegetables or salad. Casseroles and stews are popular as well. There are a few sweet dishes like Germknödel or rice pudding that can serve as a main course, too. In German lunch is called Mittagessen – literally "midday's food".
In Poland the main meal of the day (called "obiad") is traditionally eaten between 12:00 and 16:00, and consists of a soup and a main dish. Most Poles equate the English word "lunch" with "obiad" because it is the second of the three main meals of the day; śniadanie (breakfast),obiad (lunch/dinner) and kolacja (dinner/supper). There is another meal eaten by some called "drugie śniadanie", which means "second breakfast". "Drugie śniadanie" is eaten around 10.00 o'clock and is a light snack, usually consisting of sandwiches and / or salad or a thin soup.
In Hungary, lunch is traditionally the main meal of the day following a "leves", soup.
In the Netherlands, Belgium and Norway, it is common to eat sandwiches for lunch: slices of bread that people usually carry to work for eating in the canteen, in school or at the work place. The slices of bread are usually filled with sweet or savoury foodstuffs such as chocolate sprinkles (vlokken), apple syrup, peanut butter, slices of meat, cheese or kroket. The meal typically includes coffee, milk or juice, and sometimes yogurt, some fruit or soup. It is eaten around noon, during the (most of the time) 30 minute lunch break.
In Portugal, lunch (almoço in Portuguese) consists of a full hot meal, similar to dinner, normally with soup, a meat or fish course, and dessert and takes place between noon and 2:00 p.m. It is the main meal of the day throughout the country with the exceptions of the Metropolitan areas of Lisbon and Porto, where lighter meals or snacks are not uncommon. The Portuguese word lanche derives from the English word lunch, but it refers to a lighter meal or snack taken during the afternoon (around 5 p.m.) due to the fact that traditionally Portuguese dinners occur at a later hour than in English-speaking countries.
In Romania, lunch (prânz in Romanian) is the main hot meal of the day. It is usually eaten at 12:00, and never later than 15:00. The lunch usually consists of two dishes. Usually the first course is a light soup and the second course, the main course, usually consists of potatoes, rice or pasta with a garnish. Traditionally, people used to bake and eat desserts, but nowadays it is less common. On Sundays, the lunch is more consistent and is usually accompanied by an appetizer or salad.
In Russia, the midday meal is taken in the afternoon. Usually lunch is the biggest meal and consists of the first course, which is a soup and a second course which would be meat and a garnish. Tea is standard.
In Spain, lunch takes place between 1:00 p.m and 3:00 p.m., in contrast, supper does not usually begin until between 8:30 p.m. and 10:00 p.m. It is nonetheless the main meal of the day everywhere, and usually consists of a three course meal similar to a dinner. The first course usually consists of an appetizer (yet rarely a soup); the main course of a more elaborate dish, usually meat or fish based; the dessert of something sweet, often accompanied by a coffee or small amounts of spirits. Workhouses have a complete restaurant with one-hour minimum break. Spanish schools have a complete restaurant as well and students have minimum one-hour break. Three courses are common practice at home, workplace and schools. Most small shops close for between two to four hours – usually between 1:30 p.m. to 4:30 p.m. – to allow to go home for a full lunch.
In Sweden, lunch may consist of sandwiches containing cheese, meat, and fish.
In the United Kingdom, lunch is often a small meal, designed to stave off hunger until they return home from work and eat dinner. It is usually eaten early in the afternoon.


=== Middle East ===
In the Middle East and in most Arab countries, lunch is eaten between 1:00 p.m. and 4:00 p.m. and is the main meal of the day. It usually consists of meat, rice, vegetables and sauces and is sometimes but not always followed by dessert.


=== North America ===
In the United States and Canada, lunch is usually a moderately sized meal eaten at some point between 11:00 a.m. and 3:00 p.m., with 12:00 being the most-common lunch time in the US. North Americans generally eat a quick lunch which often includes some type of sandwich during the work week. Children often bring packed lunches to school, which might consist of a sandwich such as bologna (or other cold cut) and cheese, tuna, chicken, or peanut butter and jelly, or savoury pie in Canada, as well as some fruit, chips, dessert and a drink such as juice, milk, or water. Adults often leave work to go out for a quick lunch, which might include some type of hot or cold sandwich such as a hamburger or "sub" sandwich. Salads and soups are also common, as well as tacos, burritos, sushi, bento boxes, and pizza. Some individuals may pack left overs for lunch. Canadians and Americans generally do not go home for lunch, and lunch rarely lasts more than an hour. Business lunches are common and may last longer. Children generally have a break in the middle of the day to eat lunch. Public schools often have a cafeteria where children can buy lunch or eat a packed lunch. Boarding schools (including universities) often have a cafeteria where lunch is included in tuition.
In Mexico, lunch is usually the main meal of the day, and normally take place between 2:00 p.m. and 4:00 p.m.. There are usually three or four courses: the first is an entrée of rice, noodles or pasta, but also may include a soup or salad. The second course consists of a main dish called guisado served with one or two side dishes, consisting of refried beans, cooked vegetables, rice or salad. The main dish is accompanied with tortillas or a bread called bolillo. The third time is a combination of a traditional dessert or sweet, café de olla and a digestif. During the meal it is usual to drink aguas frescas, although soft drinks have gained ground in recent years. See also List of Mexican dishes.


=== Oceania ===
In Australia, the time between 10:30 a.m. and noon is considered brunch, and lunch from midday to around 3:00 p.m. A typical Australian brunch may consist of fruit or cereal product although it often includes other food as well since it is really just a merge of lunch and breakfast, while the most popular lunch meals are hot foods. These may include burgers, sandwiches and other generic café foods. If a meal during the late afternoon is not considered to be lunch, it can be referred to as "afternoon tea". While afternoon tea is commonly recognised, the meal portions are usually significantly smaller, with many afternoon teas consisting of nothing more than coffee or other beverages.


=== South America ===
In Argentina, lunch is usually the main meal of the day, and normally takes place between noon and 2:00 p.m. People usually eat a wide variety of foods like chicken, beef, pasta, salads and a drink such as water, soda or wine and some dessert. Although at work, people usually take a fast meal which can consist on some kind of sandwich brought from home or bought in a fast food place.
In Brazil, lunch is the main meal of the day, taking place between 11:30 a.m. and 2:00 p.m. Brazilian basically eat rice with beans, salad and meat, but the kind of food may vary from region to region. In the Northern areas (home to the Amazon basin and other large rivers) most people eat fish, but there is also beef, rice, beans, and farofa. Fried chicken is also widely consumed. During the weekend Brazilians usually eat churrasco (barbecue) and Feijoada. In Bahia, it´s very common to eat fish with dende oil with acarajé. On the other hand, in São Paulo it is very common to eat Italian and Japanese food, mixed with barbecue and other native dishes. Brazilians also like to eat tapioca and cheesebread (known as pão de queijo), both made with cassava powder, and can substitute wheat breads for people with coeliac disease, because it has no gluten. There is an enormous variety of brazilian fruits, such as cupuaçu, cashew fruit and nut, fruta do conde, amora, maracujá, papaya, jabuticaba, cajá and others, and many dishes made with them, such as açaí. Guaraná soft drink, coffee, caipirinha, beer and coconut water and a candy called brigadeiro are also popular.


== Working lunches and lunch breaks ==

Since lunch typically falls in the early-middle of the working day, it can either be eaten on a break from work, or as part of the workday. The difference between those who work through lunch and those who take it off could be a matter of cultural, social class, bargaining power, or the nature of the work. Also, to simplify matters, some cultures refer to meal breaks at work as "lunch" no matter when they occur – even in the middle of the night. This is especially true for jobs that have employees rotate shifts.


== See also ==

Break (work)
Brown bag seminar
Lunch counter
Mahlzeit (German salutation)


== Notes ==


== References ==


== External links ==
 Chisholm, Hugh, ed. (1911). "Luncheon". Encyclopædia Britannica (11th ed.). Cambridge University Press. 
Wikibooks Cookbook