Lychee Mini Fruity Gels are a type of sweet or candy made with konjac, a gummy binding agent made from the tubers of the konnyaku root. The product is made by AP Frozen Foods Ltd, Thailand.


== Deaths and injury ==
Lychee Mini Fruity Gels have been implicated in a number of deaths and choking injuries in the US and Pacific Rim countries. While choking on candy and other foodstuffs is not uncommon, the gel nature means that these candies totally block the airway and the konnyaku gel is "ten times stronger" than gelatin. Research shows that "hard, round foods with high elasticity or lubricity properties, or both, pose a significant level of risk," especially to children under three years old. The candies can resist attempts to dislodge with the Heimlich manoeuvre.


== Legal status ==
The foodstuff has been banned in the US (by the FDA) and the European Union.


== References ==