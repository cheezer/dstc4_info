The  Goldmark  (officially just Mark, sign: ℳ) was the currency used in the German Empire from 1873 to 1914. The Papiermark refers to the German currency from 4 August 1914 when the link between the Mark and gold was abandoned.


== History ==
Before unification, the different German states issued a variety of different currencies, though most were linked to the Vereinsthaler, a silver coin containing 16⅔ grams of pure silver. Although the Mark was based on gold rather than silver, a fixed exchange rate between the Vereinsthaler and the Mark of 3 Mark = 1 Vereinsthaler was used for the conversion. Southern Germany had used the Gulden as the standard unit of account, which was worth 4⁄7 of a Vereinsthaler and, hence, became worth 1.71 (1+5⁄7) Mark in the new currency. Bremen had used a gold based Thaler which was converted directly to the Mark at a rate of 1 gold Thaler = 3.32 (3+9⁄28) Mark. Hamburg had used its own Mark prior to 1873. This was replaced by the Goldmark at a rate of 1 Hamburg Mark = 1.2 Goldmark.

From January 1, 1876 onwards, the Mark became the only legal tender. The name Goldmark was created later to distinguish it from the Papiermark (papermark) which suffered a serious loss of value through hyperinflation following World War I (see inflation in the Weimar Republic). The goldmark was on a gold standard with 2790 Mark equal to 1 kilogram of pure gold (1 Mark = 358 mg). From 1900 to 1933, the United States adhered to a gold standard as well, with the value of the dollar being fixed at a price of approximately one-twentieth ounce (troy weight) of gold (one troy ounce of gold was actually valued at US$20.67). The goldmark therefore had a value of approximately U.S. $0.25. The monetary hegemon of the time when the goldmark was in use, however, was the Pound Sterling, with £1 being valued at 20.43 goldmarks.
World War I reparations owed by Germany were stated in goldmarks in 1921, 1929 and 1931; this was the victorious Allies' response to their fear that vanquished Germany might try to pay off the obligation in papermarks. The actual amount of reparations that Germany was obliged to pay out was not the 132 billion marks cited in the London Schedule of 1921 but rather the 50 billion marks stipulated in the A and B Bonds. The actual total payout from 1920 to 1931 (when payments were suspended indefinitely) was 20 billion German goldmarks, worth about $5 billion US dollars or £1 billion British pounds. Most of that money came from loans from New York bankers.
Following the Nazi seizure of power in 1933, payments of reparations were officially abandoned. West Germany after World War II did not resume payment of reparations as such, but did resume the payment of debt that Germany had acquired in the inter-war period to finance its reparation payments, paying off the principal on those debts by 1980. The interest on those debts was paid only after German reunification in 1990; the last payment of interest to the United States was made by Germany in 2010, and it expects to make interest payments on this debt to other debtor nations until 2020, over 100 years after the end of World War I.


== Coins ==

Coins of denominations between 1 Pfennig and 1 Mark were issued in standard designs for the whole Empire, whilst those above 1 Mark were issued by the individual states, using a standard design for the reverses (the Reichsadler, the eagle insignia of the German Empire) with a design specific to the state on the obverse, generally a portrait of the monarch, with the free cities of Bremen, Hamburg and Lübeck each using its municipal coat of arms. Occasionally Commemorative coins were minted, in which cases the obverse and (much more rarely) the reverse designs might depart from the usual pictorial standards. Many of the smaller states, issued coins in very small numbers. Also, in general all states' coinage became very limited after World War I began. Well preserved examples of such low mintage coins can be rare and valuable. The Principality of Lippe was the only state not to issue any gold coins in this period.


=== Base metal coins ===
1 Pfennig (Copper: 1873–1916, aluminium: 1916–1918)
2 Pfennig (Copper: 1873–1916)
5 Pfennig (Cupro-nickel: 1873–1915, iron: 1915–1922)
10 Pfennig (Cupro-nickel: 1873–1916, iron and zinc: 1915–1922)
20 Pfennig (Cupro-nickel, 1887–1892)
25 Pfennig (Nickel, 1909–1912)


=== Silver coins ===
Silver coins were minted in .900 fineness to a standard of 5 grams silver per Mark. Production of 2 and 5 Mark coins ceased in 1915 while 1 Mark coins continued to be issued until 1916. A few 3 Mark coins were minted until 1918, and ½ Mark coins continued to be issued in silver until 1919.
20 Pfennig, 1.1111 g (1 g silver), only until 1878
½ Mark or 50 Pfennig, 2.7778 g (2.5 g silver)
1 Mark, 5.5555 g (5 g silver)
2 Mark, 11.1111 g (10 g silver)
3 Mark, 16.6667 g (15 g silver), from 1908 onwards
5 Mark, 27.7778 g (25 g silver)
The 3 Mark coin was introduced as a replacement for the Vereinsthaler coins of the previous currency, whose silver content was slightly more than that of the 3 Mark coin.
The 5 Mark coin, however, was significantly closer in value to older Thalers (and other such crown-sized coins).


=== Gold coins ===
Gold coins were minted in .900 fineness to a standard of 2790 Mark = 1 kilogram of gold. Gold coin production ceased in 1915. 5 Mark gold coins were minted only in 1877 and 1878.
5 Mark, 1.9912 g (1.7921 g gold)
10 Mark, 3.9825 g (3.5842 g gold)
20 Mark, 7.965 g (7.1685 g gold)


== Banknotes ==

Banknotes were issued by the Imperial Treasury (known as "Reichskassenschein") and the Reichsbank, as well as by the banks of some of the states. Imperial Treasury notes were issued in denominations of 5, 10, 20 and 50 Mark, whilst Reichsbank notes were produced in denominations of 20, 50, 100 and 1000 Mark. The notes issued after 1914 are referred to as Papiermark.


== Currency signs ==
In Unicode, the Mark sign is U+2133 ℳ script capital m. The Pfennig is U+20B0 ₰ german penny sign.


== Notes ==


== References ==