Jim Macpherson (born June 23, 1966) is a United States percussionist who has played with well-known rock groups, including Guided by Voices, The Breeders, The Moping Swans, The Raging Mantras, Killjoys and The Amps. Macpherson also played in Kattie Dougherty's and bassist Sharon Gavlick's project band Real Lulu, among a collection of Dayton, Ohio drummers.
Macpherson started drumming at the age of fourteen, but drumming was mostly a hobby for the full-time handyman. The Breeders provided his most popular gig, introducing Macpherson's talent to a large audience - largely as the opening act for Nirvana's 1992 European tour.
At the end of Guided by Voices' 2000 European tour, Macpherson left the band to spend more time with his family. GBV vocalist/songwriter Robert Pollard was disappointed in Jim's resignation, though unsurprised as Pollard himself remained the band's only original member throughout many lineup changes.
Macpherson resides in Lewisburg, Ohio.


== External links ==
Recording credits from the Guided by Voices database