The MacPherson strut is a type of car suspension system which uses the top of a telescopic damper as the upper steering pivot. It is widely used in the front suspension of modern vehicles and is named for Earle S. MacPherson, who developed the design.


== History ==
Earle S. MacPherson was appointed the chief engineer of Chevrolet's Light Car project in 1945, to develop new smaller cars for the immediate post-war market. This gave rise to the Chevrolet Cadet. By 1946 three prototypes of the Cadet design had been produced. These incorporated the first MacPherson struts, giving independent suspension both front and rear. The Cadet project was cancelled in 1947 and the disgruntled MacPherson was enticed away to Ford. Patents were filed in both 1947 for GM and 1949 for Ford, the 1949 patent citing designs by Guido Fornaca of FIAT in the mid-1920s. The strut suspension of the pre-war Stout Scarab would also have been a likely influence and like Stout's own influences, the widespread use of long-travel struts in aircraft landing gear was well known by this time. It is possible that MacPherson was inspired by the suspension on the French Cottin-Desgouttes that used the same design, but with leaf springs. Cottin-Desgouttes front suspension was in turn inspired by J. Walter Christie's 1904 design and he was inspired by plants.
MacPherson originally created the design for use at all four wheels, but it is more commonly used for the front suspension only, where it provides a steering pivot as well as a suspension mounting for the wheel.
Following MacPherson's arrival at Ford, the first production car to feature MacPherson struts was the British-built 1950 Ford Consul and later Zephyr.
The first production car is widely thought to be the French 1949 Ford Vedette, but this was already developed before MacPherson with an independent front suspension based on wishbones and an upper coil spring. Only in 1954, after the Vedette factory had been purchased by Simca, did the revised Simca Vedette switch to using front struts.


== Design ==
A MacPherson strut uses a wishbone, or a substantial compression link stabilized by a secondary link, which provides a bottom mounting point for the hub carrier or axle of the wheel. This lower arm system provides both lateral and longitudinal location of the wheel. The upper part of the hub carrier is rigidly fixed to the bottom of the outer part of the strut proper; this slides up and down the inner part of it, which extends upwards directly to a mounting in the body shell of the vehicle. The line from the strut's top mount to the bottom ball joint on the control arm gives the steering axis inclination. The strut's axis may be angled inwards from the steering axis at the bottom, to clear the tyre; this makes the bottom follow an arc when steering.
To be really successful, the MacPherson strut required the introduction of unibody (or monocoque) construction, because it needs a substantial vertical space and a strong top mount, which unibodies can provide, while benefiting them by distributing stresses. The strut will usually carry both the coil spring on which the body is suspended and the shock absorber, which is usually in the form of a cartridge mounted within the strut (see coilover). The strut can also have the steering arm built into the lower outer portion. The whole assembly is very simple and can be preassembled into a unit; also by eliminating the upper control arm, it allows for more width in the engine compartment, which is useful for smaller cars, particularly with transverse-mounted engines such as most front wheel drive vehicles have. It can be further simplified, if needed, by substituting an anti-roll bar (torsion bar) for the radius arm. For those reasons, it has become almost ubiquitous with low cost manufacturers. Furthermore, it offers an easy method to set suspension geometry.


=== Pseudo MacPherson strut ===
The pseudo MacPherson strut is similar to a regular MacPherson strut, except the lower control arm is replaced by a wishbone. A torsion (anti-roll) bar is optional and if present is attached by a rod to the spring-damper.


== Advantages and disadvantages ==
Although it is a popular choice, due to its simplicity and low manufacturing cost, the design has a few disadvantages in the quality of ride and the handling of the car. Geometric analysis shows it cannot allow vertical movement of the wheel without some degree of either camber angle change, sideways movement, or both. It is not generally considered to give as good handling as a double wishbone or multi-link suspension, because it allows the engineers less freedom to choose camber change and roll center.
Another drawback is that it tends to transmit noise and vibration from the road directly into the body shell, giving higher noise levels and a "harsh" feeling to the ride compared with double wishbones, requiring manufacturers to add extra noise reduction or cancellation and isolation mechanisms.
Despite these drawbacks, the MacPherson strut setup is still used on high performance cars such as the Porsche 911, several Mercedes-Benz models and lower BMWs models (including the new Mini but excluding the 2007 X5, 2009 7-series, 2011 5-series and 5-series GT).
The Porsche 911 up until the 1989 model year (964) use MacPherson strut designs that do not have coil springs, using a torsion bar suspension instead.


== See also ==
Chapman strut
Double wishbone suspension
Strut bar


== References ==


== External links ==
AutoZine Technical School site
MacPherson Geometry Calculator