Robert Macpherson (or MacPherson) may refer to:
Robert Turnbull Macpherson (photographer) (b. 1814), Scottish artist and photographer who worked in Rome
Robert George Macpherson (b. 1866), Canadian politician and pharmacist
Robert Macpherson (Canadian politician) (1853 – 19??), Canadian builder and politician
Robert MacPherson (mathematician) (b. 1944), American mathematician
Robert MacPherson (BMX racer) (b. 1971), American bicycle motocross (BMX) racer
Robert Macpherson (actor) (b. 1984), English-born actor