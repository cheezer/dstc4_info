The Indian Institute of Technology Madras (IIT Madras) or IIT-M is an autonomous public engineering and research institution located in Chennai (formerly Madras), Tamil Nadu, India. It is recognised as an Institute of National Importance by the Government of India. Founded in 1959 with technical and financial assistance from the former government of West Germany, it was the third Indian Institute of Technology that was established by the Government of India through an Act of Parliament, to provide education and research facilities in engineering and technology.
IIT Madras is a residential institute that occupies a 2.5 km² campus that was formerly part of the adjoining Guindy National Park. The institute has nearly 550 faculty, 8,000 students and 1,250 administrative and supporting staff. Growing ever since it obtained its charter from the Indian Parliament in 1961, much of the campus is a protected forest, carved out of the Guindy National Park, home to large numbers of chital (spotted deer), black buck, monkeys, and other rare wildlife. A natural lake, deepened in 1988 and 2003, drains most of its rainwater.


== History ==

In 1956, the West German Government offered technical assistance for establishing an institute of higher education in engineering in India. The first Indo-German agreement was signed in Bonn, West Germany in 1959 for the establishment of the Indian Institute of Technology at Madras. IIT Madras was started with technical, academic and financial assistance from the Government of West Germany and was at the time the largest educational project sponsored by the West German Government outside their country. This has led to several collaborative research efforts with universities and institutions in Germany over the years. Although official support from the German government has ended, several research efforts involving the DAAD programme and Humboldt Fellowships still exist.
The institute was inaugurated in 1959 by Prof Humayun Kabir, the then Union Minister for Scientific Research and Cultural Affairs. In 1961, the IITs were declared to be Institutions of National Importance that include the seven Institutes of Technology located at Kharagpur (established 1951), Mumbai (established 1958), Chennai (established 1959), Kanpur (established 1959), Delhi (established 1961), Guwahati (established 1994) and Roorkee (established 1847, renamed to an IIT in 2001). IIT Madras celebrated its Golden Jubilee in 2009. Seven more IITs have been set up since 2008.


=== Campus ===
The main entrance of IIT Madras is on Chennai's Sardar Patel Road, flanked by the residential districts of Adyar and Velachery. The campus is close to the Raj Bhavan, the official seat of the Governor of Tamil Nadu. Other entrances are located in Velachery (near Anna Garden MTC bus stop, Velachery Main Road), Gandhi Road (known as Krishna Hostel gate or Toll Gate) and Taramani gate (close to Ascendas Tech Park).
The campus is located 10 km from the Chennai Airport, 12 km from the Chennai Central Railway station, and is well connected by city buses. Kasturba Nagar is the nearest station on the Chennai MRTS line.
Two parallel roads, Bonn Avenue and Delhi Avenue, cut through the faculty residential area, before they meet at the Gajendra Circle, near the Administrative Block. Buses and electric mini buses regularly ply between the Main Gate, Gajendra Circle, the Academic Zone, and the Hostel Zone.


=== Location in context ===


== Organisation and Administration ==

The Indian Institute of Technology, Madras, is an autonomous statutory organisation functioning within the Institutes of Technology Act. The sixteen IITs are administered centrally by the IIT Council, an apex body established by the Government of India. The Minister of Human Resource and Development, Government of India, is the Chairman of the Council. Each institute has a Board of Governors responsible for its administration and control.
The Senate comprises all professors of the Institute and decides its academic policy. It controls and approves the curriculum, courses, examinations, and results. It appoints committees to examine specific academic matters. The Director of the institute serves as the Chairman of the Senate. The Director from 2001 to 2011 was Dr. M. S. Ananth, who stepped down at the end of July 2011. As of September 2011, Prof. (Dr.) Bhaskar Ramamurthi has taken over as Director.
Three Senate Sub-Committees - The Board of Academic Research, The Board of Academic Courses and The Board of Students - help in academic administration and in the operations of the Institute. The Finance Committee advises on matters of financial policy, while the Building and Works Committee advises on buildings and infrastructure. The Board of Industrial Consultancy and Sponsored Research addresses industrial consultancy and the Library Advisory Committee oversees library matters.


== Departments ==
Aerospace Engineering
Applied Mechanics
Biotechnology
Chemical Engineering
Chemistry
Civil Engineering
Computer Science and Engineering
Electrical Engineering
Engineering Design
Humanities and Social Sciences
Mechanical Engineering
Management Studies
Metallurgical and Materials Engineering
Mathematics
Ocean Engineering
Physics


== Academics ==
IIT Madras offers undergraduate, postgraduate and research degrees across 16 disciplines in Engineering, Sciences, Humanities and Management. About 360 faculty belonging to science and engineering departments and centres of the Institute are engaged in teaching, research and industrial consultancy.
The institute has 16 academic departments and advanced research centres across disciplines of engineering and pure sciences, with nearly 100 laboratories. The academic calendar is organised around the semester. Each semester provides a minimum of seventy days of instruction in English. Students are evaluated on a continuous basis throughout the semester. Evaluation is done by the faculty, a consequence of the autonomous status granted to the Institute. Research work is evaluated on the basis of the review thesis by peer examiners both from within the country and abroad. Ordinances that govern the academic programme of study are prepared by the Senate, the highest academic body within the institute.


=== Admission tests ===
For the undergraduate curriculum, admission to the BTech and Dual Degree (BTech + MTech) programme is done through the Joint Entrance Examination - Advanced (JEE-Advanced) . Admission to the five-year integrated Master of Arts (MA) programme is through the Humanities and Social Sciences Entrance Examination (HSEE), an IIT Madras specific exam.
For the postgraduate curriculum, admission to the MTech and MS programmes are through the Graduate Aptitude Test in Engineering (GATE). The Joint Admission Test to MSc (JAM) is the entrance exam for the two-year M.Sc. programme, and other post BSc programmes. MBA candidates are accepted through the Common Admission Test (CAT).


=== Rankings ===
Internationally, IIT Madras was ranked #312 in the QS World University Rankings of 2012, 53 in the QS Asian University Rankings and 17 in the QS BRICS University Rankings of 2014.
The Times Higher Education World Rankings ranked it at 76 and 44 in Asia and BRICS countries respectively in 2014.
In India, among engineering colleges, it ranked 4 by India Today in 2012, 5 by Outlook India in 2012, and 2 by Dataquest in 2011. In the Mint Government Colleges survey of 2009 it ranked 5.


=== Grading System and Student Evaluation ===
The Indian Institutes of Technology are under control of the Government of India and therefore have strict rules for grades. Depending on the course the evaluation is based on participation in class, attendance, quiz, exam and/or paper. Continuous evaluation is done by course instructors. The Evaluation System of IIT Madras which is also used in other IITs is the Cumulative Grade Point Average with a scale from 0 to 10 which is converted to letters:
CGPA then gets calculated as the cumulative credit-weighted average of the grade points: CGPA = (Σ Ci • GPi) / (Σ Ci) where: N is the number of courses Ci is credits for the ith course GPi is grade points for the ith course CGPA is the cumulative grade point average
The CGPA is not the same as the one commonly used in the United States. In India some credits might be awarded during Bachelor studies for Co-curricular and Extra-curricular Activities, while during the Master Programme this is not allowed. Through agreements with numerous international organisations, IIT grades are accepted from many international organisations like NTU, NUS and DAAD.
Additionally, the attendance of the students is evaluated with VG for very good (always present), G for good (not present every lecture) and P for poor (student was present less than 85% of lectures).


== Other academic activities ==


=== Academic research programmes ===
The institute has departments and advanced research centres across the disciplines of engineering and the pure sciences, and nearly 100 laboratories.
Research programmes concern work undertaken by faculty members or specific research groups within departments that award an MS or PhD degree. Research is carried out by scholars admitted into these departmental programmes, under the guidance of their faculty. Each department makes known its areas of interest to the academic community through handbooks, brochures and bulletins. Topics of interest may be theoretical or experimental. IIT Madras has initiated 16 inter-disciplinary research projects against identified focus areas.
The rigours of academic study at each level are balanced with co-curricular activities. Lectures on topics of academic relevance are held under the Extramural Lecture Series. Conferences, symposiums and workshops are organised by the faculty, attracting scholarly participation from around the world. In the past, several well known dignitaries from across the world and from various spheres of life have delivered their lectures in the Extramural Lecture Series programme.


=== Partnership with other universities ===
The institute maintains academic friendship with educational institutes around the world through faculty exchange programmes. The institute has signed Memoranda Of Understanding (MOUs) with foreign universities, resulting in cooperative projects and assignments.


=== Industrial Consultancy and Sponsored Research ===
Through industrial consultancy, faculty and staff undertake assignments for industry that may include project design, testing and evaluation, or training in new areas of industrial development. Industries and organisations request the IIT faculty to undertake assignments channelled through the Centre For Industrial Consultancy and Sponsored Research (ICSR).
National organisations sponsor programmes of research by funding projects undertaken by the faculty. Such research is time bound and allows project participants to register for a degree. Project proposals are usually prepared by the IIT faculty and forwarded to interested organisations, based on the nature of their research and their interest to fund such projects.
Sponsored projects are often vehicles for new resources within departments, and often permit their project staff to register for academic degrees in the institute. All sponsored research activities at the institute are coordinated by ICSR.


=== IIT Madras Research Park ===
The IIT Madras Research Park is modeled along the lines of research parks at Stanford and MIT. It focuses not just on incubation efforts but also on propelling innovation in established R&D focused companies. IIT Madras Research Park facilitates a collaborative relationship between tenants/clients and IIT Madras.
The research park is adjacent to IIT Madras (within walking distance) on an 4.5 hectare campus. The facilities include 1,50,000 m2 of office space with attendant services in three towers of 37,000 m2 each. Each floor plate of the 12 storey towers is about 3,300 m2 with the smallest office space module being around 280 m2. Other facilities include incubation modules, shops, cyber cafes, restaurants, food courts, guest rooms, conference facilities, exhibition space, terrace gardens and landscaped front and back yards.


=== National Programme on Technology Enhanced Learning (NPTEL) ===
To improve the quality of higher education in India, IIT Madras has come up with an initiative called NPTEL (National Programme on Technology Enhanced Learning) in the year 1999. As per this initiative, all the IITs, along with the IISc Bangalore would come up with a series of lectures across all the streams of engineering. These videos are being used by several institutes as part of their programmes. This initiative has gained wide popularity in India and the lectures are being used by several engineering students from across India.


== Student activities ==


=== Shaastra ===

Shaastra is the annual technical festival of IIT Madras. It is typically held in the second week of January and is the first ISO 9001:2000 certified student festival in the world. It is known for its organisation and activities. Forums include the symposia, workshops, video conferences, lectures, demonstrations, and technical exhibitions. Competitive activities cover design events, programming, simulations, quizzes, applied engineering, robotics, junk-yard wars and contraptions.
The Shaastra Lecture Series draws attendance from various distinguished people in various technical fields from all over the world for giving guest lectures either in person or via video conference. These lectures and demonstrations aim to introduce students to the cutting-edge research happening in various fields, and serve as educational forums that encourage interaction between collegiate students and academia and industry.


=== Saarang ===

Saarang is the annual social and cultural festival of IIT Madras. It is a five-day-long event held towards the end of January every year and attracts a crowd of 40,000 students and young people from across the country, making it one of the largest such fests in India. Saarang events include speaking, dancing, thespian, quizzing and word games, professional shows (nicknamed proshows) and workshops on music, fashion, art, and dance. Saarang has been awarded ISO 9001:2008 certificate recently.
Saarang, is the new name of the festival that was once called "Mardi Gras". It was changed in the early 1990s in effort to reflect the cultural and environmental roots of this festival.


=== Department festivals ===
Several departments organise department festivals. Samanvay, Biofest, ExeBit, Wavez, Mechanica, CEA Fest, Chemclave, Amalgam and Forays are some of the festivals organised by the Department of Management Studies, Computer Science and Engineering, Ocean Engineering, Mechanical Engineering, Civil Engineering, Chemical Engineering, Metallurgical and Materials Engineering and Maths departments respectively.Department of Humanities and Social Sciences hosts Annual Academic Conference.


=== Student Hostels ===
Most students at IIT Madras reside in the hostels, where extracurricular activities complement the academic routine. The campus has 18 hostels, of which three, Sharavati, Sarayu and the recently constructed Sarayu Extension (at the beginning of the Academic year 2011-'12 to accommodate more girls students of various PG programmes) are exclusively for women. In earlier times each hostel had attached dining facilities but many of them have been closed down. Sharavati, Sarayu Extension and four seven-storeyed men's hostels do not have mess halls. Dining facilities are provided in two centralised halls dubbed 'Vindhya' and 'Himalaya'. The hostels may accommodate undergraduate and graduate students, though they tend to keep the two apart. Students are assigned to hostels at the time of admission, where they usually spend their entire stay at the Institute.
The hostels are named after the principal rivers of India and the campus dining facilities are named after mountains, resulting in an epigram about IIT Madras that it is the only place where the mountains move and the rivers remain still.

The halls of IITM are:
Sindhu, Pampa, Mahanadhi and Tamiraparani are seven-storeyed whereas all the other (older, classic) hostels are three or four storeyed. These four hostels can accommodate more than 1,200 students. The older hostels were all three-storeyed till the early 2000s when extra rooms in the form of an extra floor and rooms above the common room were added. An additional new floor in the three-storeyed hostels which generally house the undergraduate students and a new block in place of the mess halls of these hostels have been constructed to accommodate for the increased intake of the students. These new blocks could be used as entrances for these hostels.


=== Extracurricular activities ===

The Sustainability Network (S-Net) is an alumni-student-faculty initiative launched in May 2009 to sensitise and highlight the need to preserve the unique niche of one of the best educational campus in India. S-Net was envisioned to work towards development and deployment of solutions for making a self-sustaining campus (focusing on energy/electricity, water, and waste management), which could eventually be replicated across the country through tie-ups with other educational institutions.

The Fifth Estate is the official media body of IIT Madras and gives an insight into the happenings inside the campus and important news related to the institute. The Open Air Theatre hosts the weekly movie, a Saturday night tradition, besides other activities. It seats over 7,000.
The National Service Scheme (NSS) in IIT Madras has been noted for taking up socially relevant initiatives, taken up as individual projects to create an impact on the society as well as the students. The wing of NSS at IITM has over 400 students every year, contributing to the cause of the scheme. Since its inception, NSS at IITM has achieved many milestones in its history as a unique, student-run organisation. Linked with several NGOs and social organisations both within and outside Chennai. By working out projects from Braille magazines to technology interventions, from teaching children in urban slums to educational video content, NSS (IITM) seeks to challenge the mediocre thinking, and reach out into the darkness, to pull a hand into the light.
Hobby clubs include the speaking club, the astro club, dramatics, music and robotics.
Student bodies such as Vivekananda Study Circle (VSC), Islamic Study Circle, IIT Christian Fellowship, Genesis and Reflections focus on spiritual discussions.
The campus has evolved a slang, attracting a published Master's thesis at a German University. A mix of English, Hindi, Telugu (Gult), Malayalam (Mallu) and Tamil (Tam), aspects of the campus slang have been adopted by some other Chennai colleges.
Unlike its sister institutions, IIT Madras has no single Indian language used among its students: Tamil, Telugu, Malayalam, Marathi, Kannada, English and Hindi are all very commonly used. Consequently, all student participatory activities like debating, dramatics, short-film making, and others are held in English. This is even reflected in the slang that uses more of English and other Indian regional languages than Hindi, unlike in IIT-M's northern counterparts.


== Facilities ==
IIT Madras provides residential accommodation for its students, faculty, administrative and supporting staff, and their families. The residential houses employ private caterers. The self-contained campus includes two schools (Vanavani and Kendriya Vidyalaya), three temples (Jalakanteshwara, Durga Peliamman and Ganapathi temple), three bank branches (SBI, ICICI, Canara Bank), a hospital, shopping centres, food shops, a gym, a swimming pool, cricket, football, hockey and badminton stadiums. Internet is available in the academic zone and the faculty and staff residential zone. Earlier Internet was limited in hostel-zone from 2:00 pm till midnight and from 5:00 am to 8:00 am, but increasing demand during academic semester lead to full day Internet service.
IIT Madras has the fastest super computing facility among educational institutions in India. The IBM Virgo Super Cluster installed with 97 teraflops was also ranked 364 among the top 500 in world in the Top500 November 2012 list. Apart from this, the institution already has a super computer with 20 teraflops.


== IIT Madras Heritage Centre ==

The Heritage Centre was formally inaugurated by Dr Arcot Ramachandran, former Director IIT Madras on 3 March 2006. The Centre is located on the ground floor of the administration building. The actual idea of a Heritage Centre was mooted in the year 2000 and it has become a reality due to the efforts of the Professor-in-charge Dr Ajit Kumar Kolar and his team. The Centre will function as a repository of material of heritage value and historical significance of various facets of the Institute.
The exhibits include photographs, documents, publications, paintings, portraits, products developed and other articles. Information regarding important events, laboratory development, visits of important dignitaries, Indo-German cooperative activities, and academic achievements of faculty and students also are included. Aspects of IITM campus features and development, campus life and student activities are also included, thus broadening the scope of the Centre in the future to non-academic activities also.
The activities of the Heritage Centre will be of a continuous nature from now on and hence the support and cooperation of all IITians (students, faculty and staff, past and present) is very essential in making the Centre meet its goal of preserving IITM history and culture for the future generations of IITians. By the very nature of the task, the role of the Alumni is crucial in establishing and furtherance the Centre.


== Notable alumni ==

Anand Rajaraman, Founder of Junglee; Currently Heading Kosmix.com with Venky Harinarayan
Anant Agarwal, Professor of Electrical Engineering and Computer Science at MIT
Arvind Raghunathan, Managing Director, Deutsche Bank
Arun Sundararajan, Professor at Stern School of Business, New York University
B. N. Suresh, Director of IISST
B Muthuraman, Managing Director of Tata Steel
Bhaskar Ramamurthi, Director, IIT Madras
Gururaj Deshpande,Founder of Sycamore Networks
Jai Menon, IBM Fellow, CTO and VP, Technical Strategy - IBM Systems and Technology Group
Kris Gopalakrishnan, Co-chairman and co-founder of Infosys
Krishna Bharat, Creator of Google News, Principal Scientist, Google
L. Mahadevan, Professor at Harvard, MacArthur Fellow 2009
Marti G. Subrahmanyam, Professor of Finance, Stern School of Business at New York University
Prabhakar Raghavan, Vice President of Engineering, Google and Consulting Professor at Stanford University
R. Prasanna, Guitarist / Carnatic Music
Prem Watsa, Billionaire; Founder, chairman, and chief executive of Fairfax Financial Holdings, which owns BlackBerry
Vic Gundotra, Senior Vice President Google, creator of google plus and MIT technology Review top innovators in world
Ramanathan V. Guha, Inventor of RSS feed technology
Raghu Ramakrishnan, Technical Fellow and CTO, Information Services Microsoft
Raju Narayana Swamy, IAS Officer
Ramayya Krishnan, Dean of the Heinz College at Carnegie Mellon University
S. Sowmya, Carnatic Vocalist
Shashi Nambisan, Director of the Centre for Transportation Research and Education at Iowa State University
Sridhar Tayur, Ford Distinguished Research Chair and Professor of Operations Management at Carnegie Mellon University; founder, SmartOps and OrganJet
Subra Suresh, President of Carnegie Mellon University, former Director of the National Science Foundation, former Dean of the MIT School of Engineering
Venkat Rangan, Co-founder and CTO at Clearwell Systems
Venkatesan Guruswami, Associate Professor, Department of Computer Science, Carnegie Mellon University
Venky Harinarayan, Kosmix cofounder
Vinay Nair, visiting professor at The Wharton School and Founding Principal of Ada Investments


== See also ==
List of universities in India
Universities and colleges in India
Education in India


== References ==


== External links ==
Official website