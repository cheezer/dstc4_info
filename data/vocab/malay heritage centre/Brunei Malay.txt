Brunei Malay (Bahasa Melayu Brunei), or Kedayan (Kadaian), sometimes conflated as Brunei-Kadaian, is the national language of Brunei and a lingua franca in parts of East Malaysia. It is not the official language of Brunei, which is standard Malay, but is socially dominant and is replacing minority languages. It is quite divergent from standard Malay and is mostly mutually unintelligible with it.
Dialects are Brunei Malay, Kedayan, and Kampong Ayer, which are all close. The name Brunei Malay is used for the numerically and politically dominant Brunei people, who traditionally lived on water. Kedayan is the used for the land-dwelling farmers, and Kampong Ayer is used for the inhabitants of the river in and north of the capital.
Brunei Malay has a three-vowel system, with the merger of /a/ and /ə/. Final /k/ is released, and there is a non-phonemic glottal stop at the ends of vowel-final words.


== Pidgin ==

A pidginised variant of Brunei Malay, known as Sabah Malay, is a local trade language.


== Examples ==
"Ia tu bini-bini." = She is a lady.
"Sudah ko makan?" = Have you eaten?
"Awda mendapat cabutan bertuah" = You received a lucky draw.
"Ko" = You ( short version of Malay Standard 'Kau' )
"Awda" = (A combination of AWang and DAyang (Equivalent to Mr. and Miss)) = You. Generally used to address the public.
"Bini-bini" = lady ("bini" is also used in Malaysian Malay, bini however means wife. However in Malaysian and Singaporean Malay, this is not considered a polite word either refer to someone's wife or to refer to one's own wife to friends, relatives, strangers etc. In Malaysia and Singapore, the word ' isteri ' is used in polite company. 'Orang Rumah' is also acceptable, the term literally means ' Person of the House'. In Indonesia, 'istri' is used. )
"Baiktah" ("Baik saja" in Malay) = might as well
"tarus" = straight ahead, immediately (spelled and pronounced 'terus' in standard Malay)
"Kitani" = We ( might be corrupted from ' Kita Ini ' - meaning ' Us Here ' in Malay )
"Karang" = later
"Ani" = this (' ini 'in Malay Standard)
"Awu" = yes
"Inda" = no
"Kita" = us (might be referring to older person as ' you ')
"Manada" = No way (denial words, also used in Malaysia)
"Orang Putih" = Literally mean White People, to refer the Westerner or any foreigner with white skin.
"Kaling" = referring to any Indian ethnics that live and works in Brunei (however, it is consider impolite word in Malaysia and Singapore which is "Keling")


== Studies ==
The vocabulary of Brunei Malay has been collected and published by several western explorers in Borneo including Pigafetta in 1521, De Crespigny in 1872, Charles Hose in 1893, A. S. Haynes in 1900, Sidney H. Ray in 1913, H. B. Marshall in 1921, and G. T. MacBryan in 1922, and some Brunei Malay words are included in "A Malay-English Dictionary" by R. J. Wilkinson.


== References ==


== Further reading ==
De Crespigny, C. 1872. On Northern Borneo. Proceedings of the Royal Geographical Society XVI. 171-187. [2] [3] [4] [5] [6] [7] [8]
"Brunei Low Dialect" [9] [10]

Haynes, A. S. “A List of Brunei-Malay words.” JSBRAS 34 ( July 1900): 39—48. [11]
Hose, Charles. No. 3. "A Journey up the Baram River to Mount Dulit and the Highlands of Borneo". The Geographical Journal. No. 3. VOL. I. (March, 1893)
MacBryan, G.T. 1922. Additions to a vocabulary of Brunei-Malay. JSBRAS. 86:376-377. [12]
Marshall, H.B. and Moulton, J.C. 1921, "A vocabulary of Brunei Malay", in Journal of the Straits Branch, Royal Asiatic Society. [13] [14] [15]
Marshall, H.B. 1921. A vocabulary of Brunei Malay. JSBRAS. 83:45-74.
Ray, Sidney H. 1913. The Languages of Borneo. The Sarawak Museum Journal. 1,4:1-196.
Roth, Henry Ling. 1896. The Natives of Sarawak and British North Borneo. 2 vols. London: Truslove and Hanson. Rep. 1980. Malaysia: University of Malaya Press. [16] VOL I. [17] VOL II. VOL II. [18] [19]
http://www.jstor.org/stable/41561363?
A Vocabulary of Brunei Malay H. B. Marshall Journal of the Straits Branch of the Royal Asiatic Society No. 83 (APRIL, 1921), pp. 45–74 Published by: Malaysian Branch of the Royal Asiatic Society Stable URL: http://www.jstor.org/stable/41561363 Page Count: 30


== External links ==
Brunei Malay
Lexicography in Brunei Darussalam: An Overview
Majority and minority language planning in Brunei Darussalam
Dominant Language Transfer in Minority Language Documentation Projects: Some Examples from Brunei