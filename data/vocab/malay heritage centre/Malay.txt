Malay may refer to:


== Language ==
Malay language or Bahasa Melayu, a major Austronesian language spoken in Malaysia, Indonesia, Brunei, and Singapore
Old Malay, the Malay language from the 4th to the 14th century

Malay languages, a group of closely related languages in the Malay Archipelago
Malay trade and creole languages, a set of pidgin languages throughout the Malay Peninsula and Malay Archipelago
Brunei Malay, an unofficial national language of Brunei distinct from standard Malay
Kedah Malay, a variety of the Malaya languages spoken in Malaysia and Thailand
Sri Lankan Creole Malay language, a language spoken by the Malay ethnic minority in Sri Lanka
Indonesian language, the official form of the Malay language in Indonesia


== Ethnic groups ==
Malay race, a racial category used in the late 19th and early 20th century to describe Austronesian peoples
Ethnic Malays, the ethnic group located primarily in the Malay peninsula, and parts of Sumatra and Borneo
Bruneian Malay people, ethnic Malays in Brunei Darussalam
Cape Malays, an ethnic group or community in South Africa
Cocos Malays, the predominant ethnic group of the Cocos (Keeling) Islands, now part of Australia
Kedahan Malay, a large subgroup of ethnic Malays
Malaysian Malay, a constitutionally defined group of Muslim Malaysian citizens
Malays in Singapore, a broad group defined by the Singaporean government
Malay Indonesian, ethnic Malays in Indonesia
Overseas Malays, people of Malay ancestry living outside ethnic Malay home areas
Sri Lankan Malays, an ethnic group in Sri Lanka of Indonesian and Malaysian ancestry
Thai Malays, ethnic Malays in Thailand


== Individual people ==
Malay (record producer) (born 1978), American music producer
Jessi Malay (born 1986), American singer
Malay Ghosh (born 1937), Indian statistician and Distinguished Professor at the University of Florida
Malay Roy Choudhury (born 1939), Bengali poet and novelist who founded the "Hungryalist Movement" in the 1960s


== Places ==
Malay, Azerbaijan, a village in Azerbaijan
Malay, Aklan, a municipality in the Philippines
Malay, Saône-et-Loire, a commune in the Saône-et-Loire département of France
Malay Archipelago, the group of islands located between mainland Southeast Asia and Australia
Malay-le-Grand, a commune in the Yonne département of France
Malay-le-Petit, a commune in the Yonne département of France
Malay Peninsula, the geographic area containing Malaysia and Singapore, as well as parts of Myanmar (Burma) and Thailand
Malay Sheykh-e Ginklik, a village in Golestan Province, Iran
Malay Town, the unofficial name for an area of Cairns in Australia


== Nation-states ==
Malay states, a group of nine states of Malaysia (all located in West Malaysia) which have hereditary rulers
Malaysia, the modern country that encompasses most of the ancient Malay states
Melayu Kingdom, a 7th-century classical Southeast Asian kingdom


== Animals ==
Malay (chicken), a breed of chicken originating in Asia
Malayan tiger (Panthera tigris jacksoni), a subspecies of tiger from the Malay peninsula


== Ships ==
SS Malay, a Norwegian cargo ship in service from 1959 to 1961
USS Malay (SP-735), a United States Navy patrol vessel in commission from 1917 to 1919


== Other uses ==
Malay alphabet, the more common of the two alphabets used today to write the Malay language
Malay cuisine, the cuisine of Malay people of Malaysia, Indonesia, Singapore, Brunei, Mindanao and Southern Thailand
Malay Village, a museum in Geylang, Singapore
Malay world, the Malay-speaking countries of Southeast Asia, or the homeland of the Austronesian people


== See also ==
Malay grammar, the body of rules that describe the structure of expressions in the Malay language
Malay Democrats of the Philippines, a political party of the Philippines
Malay Falls, Nova Scotia, a community in Canada
Malaya (disambiguation)
Melee (disambiguation)
All pages beginning with "Malay"