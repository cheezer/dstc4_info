Holland-Bukit Timah Group Representation Constituency (Traditional Chinese: 荷蘭-武吉知馬集選區;Simplified Chinese: 荷兰-武吉知马集选区) is a four-member Group Representation Constituency located in the central, western and northern areas of Singapore. The Group Representation Constituency has not received a contest since it was formed in 2006.
Originally known as the Holland-Bukit Panjang Group Representation Constituency, it was renamed to its present name due to the Bukit Panjang ward of the GRC being carved out to form a Single Member Constituency. The Bukit Timah Single Member Constituency was absorbed and became part of the GRC.
Holland-Bukit Timah GRC is led by Minister for the Environment and Water Resources Dr Vivian Balakrishnan.
A large portion of Holland-Bukit Timah GRC is made up of jungle and nature reserve, namely the Bukit Timah Nature Reserve and the Central Catchment Nature Reserve.


== Operational responsibility ==
Cashew - Chestnut Drive housing estate, Cashew Road and Dairy Farm Road
Ulu Pandan - Clementi Neighbourhood 1, Sunset Way housing estate, Sixth Avenue, Bukit Timah Road, Old Holland Road, Mount Sinai, Ghim Moh and Farrer Road
Zhenghua - Central Water Catchment, Mandai, a portion of Bukit Panjang, MacRitchie Reservoir, Old Upper Thomson Road, Binjai Park
Bukit Timah - Clementi, a portion of Jurong East, International Business Park, Toh Tuck, Eng Kong Gardens and Faber Heights, where it is near to the Upper Bukit Timah Road DTL station.


== Members of Parliament ==


== Candidates and results ==


=== Elections in 2010s ===


=== Elections in 2000s ===


== References ==
2011 General Election's result
2006 General Election's result