Mandai Road (Chinese: 万礼路; Malay: Jalan Mandai) is a heritage road located in Mandai in the northern area of Singapore. The road starts from Woodlands Road and ends at Upper Thomson Road. The road was built in 1855 in a jungle and appeared in the Franklin and Jackson Plan of Singapore (1828) as a river indicated as "R. Mandi". It has been said the name of the road comes from a tree known as the "Mandai tree".
As the road is the main access to the Singapore Zoo and the Night Safari, it is common to refer to the zoos collectively as the "Mandai Zoo". The Mandai Crematorium and Columbarium is also located in the area, and is the resting places for those honourable leaders, such as Lee Kuan Yew, the former and first Prime Minister of Singapore.
The Upper Seletar Reservoir is also located in the vicinity.


== See also ==
Mandai


== References ==
Victor R. Savage; Brenda S. A. Yeoh (2004), Toponymics: A Study of Singapore Street Names, Singapore: Eastern University Press, ISBN 978-981-210-364-2 .