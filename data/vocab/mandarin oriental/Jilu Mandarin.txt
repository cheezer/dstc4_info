Jilu or Ji–Lu Mandarin (simplified Chinese: 冀鲁官话; traditional Chinese: 冀魯官話; pinyin: Jì–Lǔ Guānhuà), formerly known as Beifang Mandarin "Northern Mandarin", is a dialect of Mandarin Chinese spoken in the Chinese provinces of Hebei (Jì) and Shandong (Lǔ). Its name is a combination of the abbreviated names of the two provinces, which derive from ancient local provinces. The names are combined as Ji–Lu Mandarin.
Although these areas are near Beijing, Ji–Lu has a different accent and many lexical differences from the Beijing dialect, which is the basis for Standard Chinese, the official national language. There are three dialect groups, Bao–Tang, Shi–Ji, and Cang–Hui.


== Dialect groups ==
Bao–Tang dialect (保唐片), incorporating a large part of Tianjin and northern part of Hebei province.
Tianjin dialect (天津話)
Baoding dialect (保定話)
Tangshan dialect (唐山話)

Shi–Ji dialect 石济片, incorporating a large part of central Hebei province including the capital Shijiazhuang and western part of Shandong province, including the capital Jinan
Xingtai dialect (邢台话)
Shijiazhuang dialect (石家莊話)
Jinan dialect

Cang–Hui dialect (沧惠片), incorporating the coastal region of the Yellow River Delta (including the prefecture-level city of Cangzhou, from which it takes its name)
The Bao–Tang dialect shares the same tonal evolution of the inner tone from Middle Chinese as Beijing Mandarin and Northeastern Mandarin. Moreover, the popularization of Standard Chinese in the two provincial capitals has induced changes in the Shi–Ji dialect causing the former to shift rapidly towards the standard language.


== References ==