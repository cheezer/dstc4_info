The Rosigold (or Rosy Gold) mango is a named, early season mango cultivar that was selected in south Florida.


== History ==
Rosigold is of Southeast Asian heritage, and may have been a seedling of a Saigon-type mango. A 2005 pedigree analysis estimated that Rosigold was a seedling of the Ono mango.
Due to its low growth habit, Rosigold has been promoted in Florida as a mango for home growers with limited space, as well those who desire an early fruiting variety. Rosigold is now sold as nurserystock and often marketed as a 'condo mango' because it can be grown and maintained in a pot.
Rosigold trees are planted in the collections of the University of Florida's Tropical Research and Education Center in Homestead, Florida as well as the Miami-Dade Fruit and Spice Park, also in Homestead.


== Description ==
The fruit averages under a pound in weight and is oblong in shape with a smooth surface. The apex is bluntly pointed and the fruit lacks a beak. At maturity the skin is yellow in color, sometimes containing an orange-red blush. The flesh is orange-yellow in color, fiberless, and has a rich, sweet flavor. It contains a polyembryonic seed. Rosigold's fruit production is considered good. The fruit begin ripening in March in Florida, making Rosigold one of the earliest ripening cultivars.
The trees have a small growth habit and can be maintained at 8 feet in height with pruning.


== References ==