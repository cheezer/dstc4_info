Garcinia livingstonei (African mangosteen, lowveld mangosteen, Livingstone's garcinia or imbe) is a species of Garcinia, native to a broad area of tropical Africa, from Côte d'Ivoire east to Somalia, and south to South Africa.
It is an evergreen small tree, growing to 6–18 m tall. The leaves are borne in opposite pairs or whorls of 3–4, each leaf blue-green, oval, 6–11 cm long and 3–5.5 cm broad. The flowers are produced in clusters on the stems. The fruit is a small, bright, orange, thin-skinned berry 1–4 cm diameter, with one single large seed; the small yield of edible fruit is pleasantly sweet, yet acidic, but also containing a latex that some people find unpleasant.


== Cultivation and uses ==
A traditional food plant in Africa, this little-known fruit has potential to improve nutrition, boost food security, foster rural development and support sustainable landcare.
It is mainly grown as an ornamental fruit, but is sometimes eaten. The juice is known for staining very badly. Mostly eaten fresh, it is also used in drinks. It can be grown in southern Florida.
Both a male and female plant are needed in order to obtain fruit, although both sexes can be grafted onto the same plant to achieve the same effect.


== References ==
^ Germplasm Resources Information Network: Garcinia livingstonei
^ PlantsZAfrica: Garcinia livingstonei
^ University of Pretoria: Garcinia livingstonei
^ National Research Council (2008-01-25). "Baobab". Lost Crops of Africa: Volume III: Fruits. Lost Crops of Africa 3. National Academies Press. ISBN 978-0-309-10596-5. Retrieved 2008-07-17. 


== External links ==
Garcinia livingstonei in West African plants – A Photo Guide.