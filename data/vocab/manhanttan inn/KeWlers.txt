keWlers is a Finnish demogroup, formed in the early 1990s, originally called Mewlers. After smaller releases such as Tripfish 2 or Another High-Caliber Mechanical Complication, their breakthrough came in 2002 when they released their critically acclaimed demo Variform at Assembly 2002. Despite only achieving third place, the demo became one of the most widely known and top rated demos ever released, winning three Scene.org awards. Further major demos to be released later included Protozoa (ranked 6th at Breakpoint 2003) and A Significant Deformation Near The Cranium, released at Assembly 2003. Kewlers have been nominated for at least one Scene.org Award every year, except for 2004. The group disbanded for a while in 2006 directly after the release of their final demo, 1995. But since 2010 Kewlers have returned from their hiatus, with possibly Variform 2, released at Revision 2012, as their most influential demo since their comeback.
keWlers are infamous for their outspokenness – their logo contains a stylized middle finger and their official slogan is "Kewlers suck!" – likely to be originating from crowd cheers at the Scene.org Awards gala at Breakpoint 2003 – which they now use and prefer as a friendly greeting. It is also common for other demogroups to, instead of sending greetings, send "fuckings" to Kewlers as their sign of gratitude and respect. Most of the members' handles are in fact "Kewlers-nicks" besides their original, possibly more known demoscene-handle.
Since 2004, the number of Kewlers demos has decreased, and after the release of We Cell at Assembly 2004 the group hinted on their website to cease any further activities. With the release of their Assembly 2006 demo 1995 (in cooperation with mfx), the group officially acclaimed to call it quits.
However, in 2010, Kewlers returned to the Assembly demo competition, releasing 'Devolution', which earned 4th place. In 2011 they released 'Fermion' at Assembly 2011, which also received 4th place.
At the Assembly 2012 wild demo competition, Kewlers released their first Android based demo called Rrhoid, which earned 3rd place. In 2013 Kewlers kept on releasing mostly Android demos, with most recently NVite, an invitation for NVScene organised by nVidia and scene.org


== Members ==


=== Active members ===
Actor Dolban – musician, graphic artist
Albert Huffman – coder
Curly Brace – coder, 3D artist
DashNo – graphic artist
Fred Funk – musician, graphic artist
Tron Jeremy – 3D artist, graphic artist
Giorgio Moore – musician
Little Bitchard – musician
Math Daemon – coder
Mel Funktion – musician
Scootr Lovr – coder, 3D artist


=== Inactive members ===
Actor Cobain
Cookie Crumb
Helmut Kool
Justin Case
Lady Manhanttan
Ronnie Raygun
Uncle Ben
Orlando Boom


== Awards ==
Kewlers have received numerous awards and award nominations.


=== 2006 Scene.org Awards ===
Aesterozoa, nominated for Best 64k Intro
1995, winner Best Soundtrack [1] and Public Choice [2]


=== 2004 Scene.org Awards ===
We Cell, nominated for Best Demo, Best Effects, Best Soundtrack, and Public Choice
X-Mix 2004 (with mfx), nominated for Best Demo, Best Soundtrack, and Most Original Concept


=== Bitfilm Festival 2004 ===
Protozoa, Winner of the Demoshow [3]


=== 2003 Scene.org Awards ===
A Significant Deformation Near The Cranium, winner of Best Music, nominated for Best Demo, Best Effects, and Best Graphics
Protozoa, nominated for Best Demo, Best Soundtrack, Most Original Concept, and Public Choice


=== 2002 Scene.org Awards ===
Variform, winner Best Demo, Best Effects, and Best Music


== Releases ==
Kopio – finished 1st at Lobotomia 2000
Tripfish 2 – finished 7th at Assembly 2001
AH-CMC – finished 8th at lobotomia 2002
974 mites – finished 6th at Assembly 2002
Variform – finished 3rd at Assembly 2002
Deutsche Telekom – finished 3rd at buenzli 11
The Jackson Five – finished 4th at Lamerfest 2002
The Jackson Five remix – finished 3rd at Dot 2002
Jellied Veal – finished 8th at Alternative Party 2003
Protozoa – finished 7th at Breakpoint 2003
GLitch – finished 1st at Stream 2003
Oddjobb – finished 5th at Assembly 2003
A Significant Deformation Near The Cranium – finished 6th at Assembly 2003
X-MIX 2004: Ion Traxx – finished 4th at Breakpoint 2004
Typo Graphics – finished 1st at Scene Event 2004
We Cell – finished 5th at Assembly 2004
Variform Remixed – musicdisk
Aesterozoa – finished 3rd at Assembly 2006's 64k intro compo
1995 – finished 3rd at Assembly 2006
Isoljator remixed – finished 8th at Breakpoint 2010
Bootjam – Used in the Scenebooth at Assembly demo party
Devolution – finished 4th at Assembly 2010
MF Real – finished 1st at Stream Demoparty 2010
Circus Extreme – finished 1st at Stream Demoparty 2011
Fermion – finished 4th at Assembly 2011
Proton-K – finished 1st at Icons Demoparty 2012
Variform 2 – Sequel to Variform, finished 16th at Revision Demoparty 2012
Mono part 1 – finished 1st at Stream Demoparty 2012
Rrhoid – finished 3rd at Assembly 2012
1 Finger – finished 5th at Revision 2013
Nocake – finished 1st at Wedding Party 2013
Spongiforma Squarepantsii – finished 5th at Assembly 2013
Payback 2014 invitation – finished 4th at Kindergarten 2013
NVite – finished 2nd at the Ultimate Meeting 2013


== References ==


== External links ==
Kewlers website
Kewlers releases on Pouet