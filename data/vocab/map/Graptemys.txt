Graptemys is a genus of aquatic, freshwater turtles known commonly as map turtles or sometimes sawback turtles, endemic to North America.


== Geographic range ==
They are found throughout the eastern half of the United States and northward into southern Canada.


== Description ==
They superficially resemble many other species of aquatic turtles, including sliders (Trachemys) and cooters (Pseudemys). However, they are distinguished by a keel that runs the length of the center of the carapace. They also typically grow to a smaller size at maturity. They are given the common name "map turtle" due to the map-like markings on the carapace.


== Courtship behavior ==
Adult Graptemys males have greatly elongated claws on the front feet, which are used in courtship behavior. The male faces the considerably larger female and "fans" her face, vibrating his foreclaws against her head to induce her to cooperate in mating.


== Longevity ==
Average life expectancy of map turtles ranges from 15 to 100 years, depending on species.


== Pet trade ==
Throughout the pet trade, Mississippi, common, and Ouachita map turtles were bred and hatched out by the thousands in the 1970s. Various turtles other were available, but as the salmonellosis Four-inch Law was established, map turtles and others slowly decreased in popularity. Today, these same three still hold the title for most common among the pet trade. Other species being captive-bred more often include the Texas map turtle, Cagle's map turtle, and the black-knobbed map turtle. Some harder-to-find map turtles include the yellow-blotched map turtle and the Pearl River map turtle.


== Species ==
(Listed alphabetically)
Graptemys barbouri Carr & Marchland, 1942 – Barbour's map turtle
Graptemys caglei Haynes & McKown, 1974 – Cagle's map turtle
Graptemys ernsti Lovich & McCoy, 1992 – Escambia map turtle
Graptemys flavimaculata Cagle, 1954 – yellow-blotched map turtle or yellow-blotched sawback
Graptemys geographica ( Le Sueur, 1817) – northern map turtle, formerly known as the common map turtle
Graptemys gibbonsi Lovich & McCoy, 1992 – Pascagoula map turtle
Graptemys nigrinoda Cagle, 1954 – black-knobbed map turtle
Graptemys oculifera (Baur, 1890) – ringed map turtle
Graptemys ouachitensis Cagle, 1953 – Ouachita map turtle
Graptemys pearlensis Ennen, Lovich, Kreiser, Selman, Qualls, 2010 – Pearl River map turtle
Graptemys pseudogeographica (Gray, 1831) – false map turtle
G. p. kohnii (Baur, 1890) – Mississippi map turtle
G. p. pseudogeographica Gray, 1831 – false map turtle

Graptemys pulchra Baur, 1893 – Alabama map turtle
Graptemys versa Stejneger, 1925 – Texas map turtle


== References ==


== External links ==
An Overview of Map Turtles in the United States, Reptile & Amphibian Magazine, November/December 1993, p. 6-17.
ARKive - images and videos of the Cagle's map turtle (Graptemys caglei)
Ouachita map turtle - Graptemys ouachitensis Species account from the Iowa Reptile and Amphibian Field Guide.
False map turtle - Graptemys pseudogeographica Species account from the Iowa Reptile and Amphibian Field Guide.
Common map turtle - Graptemys geographica Species account from the Iowa Reptile and Amphibian Field Guide.


== Further reading ==
Agassiz, L. Contributions to the Natural History of the United States of America. Vol. I. Little, Brown and Company. Boston. li + 452 pp. (Genus Graptemys, p. 252.)
Smith, H.M., and E.D. Brodie, Jr. 1982. Reptiles of North America: A Guide to Field Identification. Golden Press. New York. 240 pp. ISBN 0-307-13666-3 (paperback). (Genus Graptemys, p. 48, including identification key to species.)