Marinas in Turkey refer to Turkey's ports of call for international and local yachtsmen equipped with modern services routinely expected in recreational boating industry, and they are presently found either in or near Istanbul or İzmir, the two largest port cities of the country whose economies are focused on tourism in the Aegean Sea or the Mediterranean Sea, with a particular concentration in southwest Anatolia.
Although the history of modern services geared to recreational boating reaches back to only slightly more than three decades in Turkey, the country's increasing popularity in nautical tourism is advantaged by its spectacular coastline and a past noted for the rich seafaring literature, some of whose references are part of everyday culture, as is the case for the Blue Cruise, and the search for the Golden Fleece. It is noteworthy to recall that, apart from the larger installations listed below, there are also countless points of stop and supply which offer the advantages inherent to smaller enterprises, sometimes in a family environment, at the same time as putting the geography of the Turkish coasts to good use.
Since recent years, these installations offer the modern infrastructure and facilities that are considered as requirements with increasing rapidity and sophistication, catering a whole range of services. Considerable momentum has been accumulated in terms of infrastructure, know-how and human resources, promising a still more active future for the industry. Sizable investments by non-Turkish investors have been made in some of the marina installations below and prominent Turkish private sector groups view marinas as an attractive investment that also enhances their prestige, and thus have built or acquired one to include in their overall portfolio.
While it has two well-integrated marinas (in Girne/Kyrenia and Gazimağusa/Famagusta), particular emphasis on the Marinas in the Turkish Republic of Northern Cyprus need to be put on a number of points in relation to its state of isolation susceptible to evolve in line with the Foreign relations of the Turkish Republic of Northern Cyprus and on which political haggling between Turkey and the European Union continues.


== List of facilities ==
There are close to three dozen marinas currently (2010) operational in Turkey and the Turkish Republic of Northern Cyprus, as listed below. Many of these were initially constructed through public investment, and later purchased and fine-tuned by the private sector. The dry dock areas are not included in the capacity counts.


== Profiles of operating companies ==


=== Marinturk Marinas & Boatyards ===
Located in two most important yachting locations of Turkey, Marinturk Marinas & Boatyards offer berth up to 80 meter super yachts. Marinturk's Istanbul City Port Marina has the biggest capacity in Turkey in terms of berth numbers, ranging from 10 meters up to 80.
Marinturk is effectuating the second chain marinas in Turkey in terms of operational integrity along with these attributions. At the moment company has three marinas located in Istanbul and Gocek(2). Among facilities, it is possible to find; floating pontoons designed by the systems endowed with the highest standards in Mediterranean, shore power connection up to 400 amps, wireless internet connection, travel lifts capable of hoisting yachts up to 200 tons, high technological systems for disposal of all the wastes without causing any harms to the environment, 24 hours electronic security systems via camera control and all the other services that may be required. Marinturk Marinas is the only marina offers sunshades over VIP pontoons.


=== Palmarina Bodrum ===
PALMARINA Bodrum, with the only high capacity of Mega & Giga Yacht mooring of Turkey, together with the entertainment world it offers, is providing the opportunity for 69 Mega Yachts that are 40 meters and above to dock and to moor throughout the years well as for sail boats and motor yachts of various lengths. It also provides technical services for 140 boats up to 45 meters in length maximum on its fully equipped boatyard on the land. PALMARINA Bodrum, which is getting ready to provide its services throughout the 12 months with all of its brands, is putting forward its difference as the most secure and the only Mega Yacht marina of Turkey. Besides, it is the one and only marina that has been certified with ISO 9001 – 2008 issued by Lloyd`s Register of Shipping (LRQA).


=== Local specialist firms ===
Alaçatı Marina and İzmir's Levent Marina are operated by locally based enterprises and Ataköy Marina by a select yacht club. Alaçatı Marina is located within the compound of the ongoing and large-scale Port Alaçatı construction project intended for international holiday home-owners.


=== Setur Marinas ===
Setur is one of the prominent names in Turkey's tourism industry and is part of Koç Holding, Turkey's top conglomerate. The company Setur was founded in 1964 within the group originally to operate duty-free shops and expanded into the travel agency business in the 1970s. Its first venture into marina ownership and management was in 1978 with the construction of the marina in Çeşme's Altınyunus tourism complex. The company's further acquisitions of marinas, in Antalya and Finike, were initially leased from the municipalities of these cities in 1991 and 1997, and Ayvalık and Kuşadası marinas were taken over from another company the same year. This was followed in 1998 by the take-over of Kalamış and Fenerbahçe Marinas in İstanbul. The company portfolio was brought to its present state with the purchase of the majority actions of Netsel Marina in Marmaris in 2005, bringing the number of marinas owned and operated to eight. In 2000, the company, "Setur Marinas", was set up to operate as an entity distinct from "Setur" self, which continues to concentrate on the travel agency business. At the same time, the different marinas which were, until then, under the structures of separate companies were united within the single framework of Setur Marinas as based in Kalamış, İstanbul. Setur Yalova Marina the newest of Setur Marinas group, has started operation in 2010.


=== International investments ===
Port Göcek is managed by the British Camper and Nicholsons (widely recognised to be the world's oldest and most prestigious yachting business name) marina division CNMarinas, in a joint-venture with the owner, Turkey's Turkon Holding. The marina is part of an integrated leisure compound, at the tip of a natural park and complete with a Swissotel and the beach gravel brought over from Canada. In 2006, Camper and Nicholson also won the bid, together with IC Holding, for the completion works and the operating of Çeşme harbour marina [1], and it will be the second built in this city considered as a highly precious part of the whole marina privatization package (Çeşme's present-day marina operated by Setur is situated outside the city, within Altınyunus compound, Turkey's showpiece tourism investment in the 1970s owned today by Yaşar Holding.


=== As prestige investments ===
A number of large Turkish groups started by constructing a single marina to date and/or still has only one, with obvious intentions of prestige enhancement. One name in this segment is Doğuş Holding's recently (late 1990s) constituted Doğuş Marina or D Marin. They started with a large marina in Bodrum's depending municipality of Turgutreis and a second one in Didim was completed in 2009, with yet another one in Dalaman at contract stage.
Similarly, Park Kemer Marina in Kemer is owned by Turgay Çiner's Park Group and is the group's only marina at present. Yet another example is Martı Marina in Marmaris, which was formerly part of a larger holding that also included a vast textile and clothing branches. Since the sale of these last businesses in 1999, the marina remains part of the four up-market vacation villages still operated by Turkey's well-known figure of business and politics Halit Narin's Martı Group.


=== In the Turkish Republic of Northern Cyprus ===

There are number of specific points to be addressed in relation to the marinas situated along the shores of Northern Cyprus, such as the need to make a call in a Turkish port beforehand.


== Under construction ==
Turkey's priority agenda item in the field of nautical tourism remains the expansion of the zone with proper infrastructure and especially the introduction of her Black Sea coast to international yachting and developing this region's virtually unexplored potential. To date, marina development in Turkey's Black Sea coast, adjusted to serve modern-day Argonauts, lags very much behind Turkey's other regions.
The infrastructure for a 175-boat capacity marina is actually fully laid in Trabzon and Turkey's Ministry of Transport is seeking private investors for the complementary works and the operational commitments. Other investments in the same stage of readiness are the two marinas (500- and 200-boat capacity) within Mersin metropolitan area, two in Antalya Province (Alanya with 425-boat and Gazipaşa with 250-boat capacities), two more tendered and concluded investments in İzmir Province each with 400-boat capacities, in Sığacık and inside Çeşme harbour, and finally two small marinas for up to 100 boats in the district of Burhaniye in the Aegean Sea (neighboring Ayvalık) and in Kumburgaz coast of İstanbul's Silivri district, in the Sea of Marmara. Finally, the compound of İzmir's historic Konak Pier building is intended to be enlarged to include Turkey's largest marina (in Konak district in the heart of İzmir metropolitan area) once the suitable adjacent area is made available. The shopping mall part of Konak Pier is already in service since two years and houses up-market stores.


== See also ==

Tourism in Turkey
Turkish Riviera
Blue Cruise (Mavi Yolculuk)
Gulet
Taka
List of ports in Turkey


== References ==


== External links ==