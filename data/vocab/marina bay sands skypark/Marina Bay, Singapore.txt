Marina Bay is a bay near Central Area in the southern part of Singapore, and lies to the east of the Downtown Core. The area surrounding the bay itself, also called Marina Bay, is a 360 hectare extension to the adjacent Central Business District.


== Master Plan ==
The URA Master Plan for Marina Bay aims to encourage a mix of uses for this area, including commercial, residential, hotel and entertainment.
The Singapore government also spent $35 million to complete the 3.5 km Waterfront Promenade around Marina Bay. It includes a new eco-friendly visitor centre and The Helix linking Bayfront to Marina Centre where the Youth Olympic Park is located. The Promontory @ Marina Bay (formerly Central Promontory Site) will be used as an interim event space and public space used for activities such as theatres and carnivals.


== History ==

In the 1970s, land reclamation was carried out at Marina Bay, forming what is today the Marina Centre and Marina South areas. In the reclamation process, Telok Ayer Basin and Inner Roads was removed from the map by reclaiming land, while the Singapore River's mouth now flows into the bay instead of directly into the sea. In 2008, Marina Barrage was built, converting the basin into a new downtown freshwater Marina Reservoir, providing water supply, flood control and a new lifestyle attraction.


== Events at Marina Bay ==

The inaugural Singapore Grand Prix took place on 28 September 2008 on a street circuit through Marina Bay. It was also the first ever Formula One Grand Prix to be staged at night, with the track fully floodlit. Since its inception, The Float@Marina Bay has hosted events such as the National Day Parade, New Year’s Eve Countdown, Singapore Fireworks Celebrations, as well as served as a spectator stand for the inaugural Formula 1 Singapore Grand Prix. The world’s largest floating stadium played host to the Opening and Closing Ceremonies of the inaugural 2010 Summer Youth Olympics.


== Infrastructure ==


=== Common Services Tunnel ===
Singapore is the second Asian country after Japan to implement a comprehensive Common Services Tunnel system to distribute various utility services to all city developments. The Marina Bay network of purpose-built tunnels houses water pipes, electrical and telecommunication cables and other utility services underground. CST not only improves reliability of services supplies and allows easy maintenance and new installations, it also has 100% emergency backup services and the capacity for expansion to meet changing utility needs.


=== Water management ===
In 2004, the Public Utilities Board publicly announced plans to construct a new downtown reservoir by damming the Marina Channel. This barrage was completed in 2008. Known as the Marina Barrage, it turned Marina Bay and the Kallang Basin into a confined freshwater reservoir with limited access to marine transportation to regulate the water quality. The new reservoir provides another source of drinking water for Singapore, as well as a stable water level for a variety of water activities and events. The barrage will also prevent flooding in the Chinatown area.


=== Transportation ===
There are currently 7 rail stations: City Hall, Raffles Place, Marina Bay, Bayfront, Downtown, Esplanade and Promenade serving Marina Bay. By 2020, the 360 hectares Marina Bay will boast a comprehensive transport network as Singapore's most rail-connected district. The first three new MRT lines will open between 2012 and 2014. By 2018, the Marina Bay district will have more than six MRT stations, all no more than five minutes of each other. A comprehensive pedestrian network including shady sidewalks, covered walkways, underground and second-storey links will ensure all-weather protection and seamless connectivity between developments and MRT stations. Within greater Marina Bay, water taxis will even double up as an alternative mode of transportation.


== Key developments ==


=== In Marina Bay ===
ArtScience Museum
Asia Square
Bayfront Bridge
Circle Line
Clifford Pier
Common Services Tunnel
Downtown Line
Esplanade – Theatres on the Bay
F1 Pit Building
Gardens by the Bay
ITE College South
Marina Bay Street Circuit
The Fullerton Heritage Precinct: Customs House, The Fullerton Hotel Singapore, Fullerton Waterboat House, One Fullerton, Fullerton Bay Hotel
The Helix Bridge
Marina Barrage
Marina Bay Cruise Centre Singapore
Marina Bay Financial Centre
Marina Bay Golf Course
Marina Bay Link Mall
Marina Bay Mall
Marina Bay Sands
Marina Bay Suites
Marina Coastal Expressway
Marina East New Town
Marina South New Town
Marina South Pier
Marina South Pier MRT Station
Millenia Walk
One Marina Boulevard
One Raffles Quay
One Shenton Way
OUE Bayfront
Singapore Flyer
Suntec City
The Float at Marina Bay
The Lawn @ Marina Bay
The Promontory @ Marina Bay (formerly Central Promontory Site)
The Sail @ Marina Bay
Thomson-East Coast Line
Youth Olympic Park


=== Other places of interest ===
The 101-hectare Gardens by the Bay site is made up of Bay South Garden, Bay East Garden, and Bay Central Garden across the mouth of the Singapore River. All three gardens will be interconnected via a series of pedestrian bridges to form a larger loop along the whole waterfront and linked to surrounding developments, open public spaces, transport nodes and attractions.
A 3.5 km waterfront promenade linking the attractions at the Marina Centre, Collyer Quay and Bayfront areas was completed in 2010.


== Gallery ==


== See also ==
Downtown Core
Future developments in Singapore
Marina Centre
Marina South


== References ==


== External links ==
Official website
Marina Bay Facebook Page
Urban Redevelopment Authority Official Website
Marina Bay Photo Gallery