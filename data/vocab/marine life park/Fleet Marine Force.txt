The United States Fleet Marine Forces (FMF) are combined general and special purpose forces within the United States Department of the Navy that are designed in engaging offensive amphibious or expeditionary warfare and defensive maritime employment. The Fleet Marine Forces provide the National Command Authority (NCA) with a responsive force that can conduct operations in any spectrum of conflict around the globe.


== Organization ==
The Fleet Marine Force consists of both combative naval fleets and Marine Corps' forces components that would entirely make up the Fleet Marine Forces on the Pacific and Atlantic coasts, or within its "designate(s)". While it serves directly under the Marine Corps organization, the FMF personnel, Marines and Sailors, are subject to the operational control of naval fleet commanders; the Commandant of the Marine Corps (CMC) retains administrative and training control.
The Commanding General of the Fleet Marine Force; either its Pacific (CG FMFPAC) or Atlantic (CG FMFLANT) command, are responsible for the administration and training of all of the subordinate units of the Marine Corps Forces (MARCORFOR). The subordinate units of the Fleet Marine Forces come under the operational control of the commanders, U.S. Fleet Force Command (formerly Atlantic Fleet) or United States Pacific Fleet, when deployed.
The commanders of Marine Forces Command (MARFORCOM) and Pacific (MARFORPAC) serve as Marine Corps component commanders to their respective combatant commanders and may also serve as commanding generals of Fleet Marine Forces (FMFs) Atlantic, or Pacific.
The operating forces of the Marine Corps are currently organized into two Fleet Marine Forces (FMF):
 Fleet Marine Force, Pacific (FMFPAC) with headquarters in Honolulu, Hawaii
 Fleet Marine Force, Atlantic (FMFLANT) with headquarters in Norfolk, Virginia.
Each FMF is equivalent to a U.S. Navy type command and reports to its respective Fleet Commander-in-Chief. The commanding general, a lieutenant general may be either an aviator or a ground officer. His deputy commanding general is from the other community.
Marine Corps forces are organized into warfighting units of combined arms known as Marine Air Ground Task Forces (MAGTFs) and are either employed as part of naval expeditionary forces or separately as part of larger joint or combined forces.
Each FMF consists of at least one Marine Expeditionary Force (MEF) consisting of at least one Marine Division (MARDIV), one Marine Aircraft Wing (MAW), one Marine Logistics Group (MLG) (formerly the Force Service Support Group [FSSG]), and a MEF Headquarters Group. Other miscellaneous supporting units may be attached. In addition to one or more MEF(s), each FMF is further organized into one or more intermediate-sized MAGTFs named Marine Expeditionary Brigades (MEBs) and smaller MAGTFs called Marine Expeditionary Units (MEUs).


== History ==

Its predecessor was the Advanced Base Force in the early 20th century. The history of the FMF dates to December 7, 1933, when Secretary of the Navy Claude A. Swanson issued General Order 241 defining the Fleet Marine Force.
Momentarily before and during World War II, the Fleet Marine Force had FMF units also in western Pacific assigned to the United States Asiatic Fleet, establishing the Fleet Marine Force, Western Pacific (FMFWestPac).


== Navy Personnel in the Fleet Marine Forces ==

For service in the Fleet Marine Force, the United States Department of the Navy issues the FMF Enlisted Warfare Specialist Insignia and the FMF Qualified Officer Insignia (formerly, the Fleet Marine Force Ribbon was issued). Naval Fleet Marine Force personnel, usually Corpsmen or Naval Gunfire Liaison Officers who participate in amphibious assaults, are also eligible to receive the FMF Combat Operations Insignia to certain service medals and ribbons. Such Naval personnel are authorized to wear the Marine Corps utility uniform with Navy insignia, and must conform to all physical requirements of the U.S. Marines.


== See also ==

Naval Expeditionary Combat Command
1st Naval Construction Division
United States Navy Hospital Corpsman
Marine Air-Ground Task Force Reconnaissance
United States Navy Amphibious Reconnaissance Corpsman


== References ==


== External links ==
http://www.globalsecurity.org/military/library/report/1989/SJH.htm
http://www.globalsecurity.org/military/library/report/1995/MJS.htm