The World Register of Marine Species (WoRMS) is a database that aims to provide an authoritative and comprehensive list of names of marine organisms.


== Contents ==
The content of the registry is edited and maintained by scientific specialists on each group of organism. These taxonomists control the quality of the information, which is gathered from malacological journals and several regional and taxon-specific databases. WoRMS maintains valid names of all marine organisms, but also provides information on synonyms and invalid names. It will be an ongoing task to maintain the registry, as new species are constantly being discovered and described by scientists. In addition, the nomenclature and taxonomy of existing species is often corrected or changed as new research is constantly being published.


== History ==
WoRMS was founded in 2008 and grew out of the European Register of Marine Species. It is primarily funded by the European Union and hosted by the Flanders Marine Institute in Ostend, Belgium. WoRMS has established formal agreements with several other biodiversity projects, including the Global Biodiversity Information Facility and the Encyclopedia of Life. In 2008, WoRMS stated that it hoped to have an up-to-date record of all marine species completed by 2010, the year in which the Census of Marine Life was completed.
As of February 2015, WoRMS contained listings for 227,979 valid marine species, of which 218,759 were checked. Their goal is to have a listing for each of the approximately 240,000 marine species.


== See also ==
Census of Marine Life (CoML)
Ocean Biogeographic Information System (OBIS)
Catalog of Fishes


== References ==


== External links ==
World Register of Marine Species