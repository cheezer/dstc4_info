For Archdeacons of Halifax before 1927, see Archdeacon of Pontefract.
The Archdeacon of Halifax is the priest in charge of the archdeaconry of Halifax, an administrative division of the Church of England Diocese of Leeds (formerly in the Diocese of Wakefield.)


== History ==
From the creation of the Diocese of Wakefield in 1888 until its reorganisation in 1927, the archdeaconry of Huddersfield comprised all but the northwestern corner of that diocese. In 1927, the archdeaconry was renamed to that of Halifax and its borders moved to cover the western half of the diocese (the old Halifax archdeaconry became the archdeaconry of Pontefract).
The (second) archdeaconry of Halifax has constituted the western half of the diocese since the 1927 reorganisation, covering the deaneries of Calder Valley, Halifax, Brighouse and Elland, Huddersfield, Almondbury and Kirkheaton. Since the creation of the Diocese of Leeds on 20 April 2014, the archdeaconry forms the new Huddersfield episcopal area.


== List of archdeacons ==
For Archdeacons of Halifax before 1927, see Archdeacon of Pontefract.
Archdeacons of Huddersfield
1888–1892 (res.): Norman Straton
April 1892–1913 (ret.): William Donne
1914–1927: Richard Harvey (became Archdeacon of Halifax)
With the diocesan reorganisation of 1927, the archdeaconry was renamed to Halifax.
Archdeacons of Halifax
1927–1935 (ret.): Richard Harvey (previously Archdeacon of Huddersfield)
1935–1946 (ret.): Albert Baines
1946–1949 (res.): Arthur Morris
1949–1961 (res.): Eric Treacy
1961–1972 (res.): John Lister
1972–1984 (ret.): John Alford (afterwards archdeacon emeritus)
1985–1989 (res.): Alan Chesters
1989–1994 (res.): David Hallatt
1995–2003 (res.): Richard Inwood (became Bishop suffragan of Bedford)
2003–28 October 2011 (res.): Robert Freeman
22 January 2012–present: Anne Dawtry


== References ==
^ a b Diocese of Wakefield – Archdeaconry of Halifax
^ The London Gazette: no. 25876. pp. 6279–6281. 20 November 1888. Retrieved 31 January 2012.
^ The London Gazette: no. 33248. pp. 1029–1034. 15 February 1927. Retrieved 31 January 2012.
^ The Church of England – Synod approves new Diocese of Leeds for West Yorkshire and The Dales
^ Moving towards a new diocese for West Yorkshire and the Dales (Accessed 9 July 2013)
^ Straton, Rt. Rev. Norman Dumenil John. Who Was Who. 1920–2008 (December 2007 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Donne, Ven. William. Who Was Who. 1920–2008 (December 2007 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Harvey, Ven. Richard Charles Musgrave. Who Was Who. 1920–2008 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Baines, Ven. Albert. Who Was Who. 1920–2008 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Morris, Rt Rev. Arthur Harold. Who Was Who. 1920–2008 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Treacy, Rt Rev. Eric. Who Was Who. 1920–2008 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Lister, Very Rev. John Field. Who Was Who. 1920–2008 (December 2007 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Alford, Ven. John Richard. Who Was Who. 1920–2008 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Chesters, Rt Rev. Alan David. Who's Who 2013 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Hallatt, Rt Rev. David Marrison. Who's Who 2013 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Penrith, Bishop Suffragan of,. Who's Who 2013 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013. 
^ Huddersfield Parish Church – Archdeacon Anne Dawtry's Welcome in St Peter’s
^ Dawtry, Ven. Dr Anne Frances. Who's Who 2013 (December 2012 online ed.). A & C Black, an imprint of Bloomsbury Publishing plc. Retrieved 15 May 2013.