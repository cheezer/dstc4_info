Marymount University is a coeducational, four-year Catholic university that has its main campus located in Arlington, Virginia. Marymount offers bachelor’s, master’s, and doctoral degrees in a wide range of disciplines and has a diverse and welcoming academic community with approximately 3,600 students representing approximately 40 states and 70 countries.


== History ==
Marymount was founded in 1950 by the Religious of the Sacred Heart of Mary (RSHM) as Marymount College, a two-year women's school. It was a member school of the Marymount colleges operated by the sisterhood in New York, California and several other states. The campus was located on the former estate of Admiral Presley Marion Rixey, Naval Surgeon General and personal physician to Presidents Theodore Roosevelt and William McKinley. Classes and activities were centered around the former Rixey Mansion, renamed as the university's "Main House".
Marymount became a four-year college in 1973, added master's degree programs in 1979, and its first doctoral program, the Doctor of Physical Therapy, in 2004. Its first male students were admitted into the nursing program in 1972 and the college became fully coeducational and moved to university standing in 1986.
The university established the Center for Ethical Concerns in 1993. In 2002, Marymount began offering an honors track for exceptional incoming freshman and transfers.
In October 2010, Marymount celebrated its 60th anniversary with the grand opening and ribbon cutting ceremony for Caruthers Hall, a 52,000-square-foot (4,800 m2) academic facility focused on the sciences and health sciences, and Rose Benté Lee Ostapenko Hall, a 77,000-square-foot (7,200 m2) residence hall providing apartment-style housing for 239 students. The Malek Plaza is a gathering area between the two buildings and features a statue of Sister Majella Berg, RSHM, who was president of Marymount from 1960 to 1993. A sky bridge connects the new buildings to the rest of the campus. 


== Catholic tradition ==
While the University has evolved in many ways through the years, it remains firmly committed to its Catholic mission and identity. Marymount provides a values-based education that promotes both intellectual and spiritual development. All undergraduates take religious studies, theology, and philosophy courses as part of their core curriculum.
Through liturgy, faith discussion groups, spiritual retreats, a strong music ministry, and a wide range of service opportunities, Campus Ministry encourages individuals to examine and deepen their faith and to live ethically responsible lives. And Marymount students are known for their commitment to making a positive difference by serving others. To encourage a spirit of openness and respect, all Campus Ministry activities and programs are open to everyone in the Marymount community, regardless of one’s faith tradition and beliefs.
Catholic colleges have a long tradition of welcoming people of all backgrounds and faiths, and Marymount is no exception – providing a hospitable, diverse community where dialogue and friendship promote understanding. At Marymount, each individual is recognized as a unique child of God whose contributions enrich the whole community.


== Academics and accreditation ==
Marymount University is accredited by the Commission on Colleges of the Southern Association of Colleges and Schools to award doctoral, master’s, and bachelor’s degrees. Additionally, many programs have specialized accreditations and approvals.
The school grants bachelor's, master's and doctoral degrees, undergraduate and graduate certification, and pre-professional programs in teaching, law, medicine and physical therapy through the Schools of Arts and Sciences, Business Administration, Education and Human Services, and the Malek School of Health Professions.
In addition, Marymount is a member of the Consortium of Universities of the Washington Metropolitan Area, which allows students to take courses at any of the other 13 member institutions and to borrow books from their libraries. Member institutions include Georgetown University, The George Washington University, The Catholic University of America, and the Corcoran College of Art and Design.
Marymount offers dozens of Study Abroad Programs that give students opportunities to accomplish course work while experiencing another culture. There are three categories of programs: semester programs, short term courses, and summer programs.
The School of Business Administration offers graduate programs that provide a strong foundation in business principles, as well as instruction in the latest technological advances. Graduate students can take advantage of full- or part-time study options, with an emphasis placed on evening and Saturday classes that are convenient for the working professional. Faculty advising, peer networking, and a cutting-edge education afford a wide array of professional opportunities in the Washington, DC, region.


== Campuses ==


=== Main campus ===
Marymount's main campus is located on 21 acres (85,000 m2) in the North Arlington area of Arlington, Virginia. The campus includes six residence halls: Rose Benté Lee Ostapenko Hall, Rowley Hall, Butler Hall, St. Joeseph's Hall, Berg Hall, and Gerard Phelan Hall; three academic buildings: Rowley Academic Center, Caruthers Hall, and Gailhac Hall; St. Joeseph's Hall computer labs; the Rose Benté Lee Center which includes two gyms, the campus bookstore, Bernie's Cafe, mail facilities, and recreational and meeting spaces; the Gerard Phelan Cafeteria; the Emerson G. Reinsch Library and Auditorium; the Lodge; Ireton Hall; the Main House; the Chapel of the Sacred Heart of Mary; and the synthetic-turf practice field.


=== Ballston Center ===
Marymount's Ballston Center is 2 miles (3.2 km) away from the main campus in Arlington and houses the School of Business Administration and the Counseling, Forensic Psychology, and Physical Therapy programs. The center also includes the Truland Auditorium; the Ballston Center library extension; computer labs; the Ballston Center Cafeteria; the Ballston Center chapel; the Career and Internship Center; and the Study Abroad Office. The Ballston-MU Metro Station is within walking distance from the Ballston Campus. Additionally, free shuttle services operate between the Ballston Metro Station, Main Campus, and Ballston Campus.


=== Reston Center ===
Marymount also has a Reston center. Its mission is to provide undergraduate and graduate programs that help adult learners further their education and achieve their personal and professional goals.


== Athletics ==
Marymount's teams, known athletically as the Saints, compete in NCAA Division III in the Capital Athletic Conference. Sports available for men and women include Basketball, Cross-Country, Lacrosse, Soccer, Cheerleading, and Swimming and Volleyball. Golf is available for men. In 2014, the school will begin sponsoring varsity baseball. In September 2012, it hired Frank Leoni, former Rhode Island and William & Mary coach, as the baseball program's first head coach.
The Marymount women's basketball team has a long and rich history both on the court and in the classroom. They have won eleven Capital Athletic Conference titles (in 20 years) including the first seven in the CAC's history and most recently, in 2010. The Saints have advanced to the NCAA post-season tournament 14 times, moving on to the Final Four in 2002, the Elite 8 in 2004, and the Sweet 16 seven other times. The Saints are one of only three teams (of 425 in Division III) to advance to at least the Sweet 16 in the four seasons from 2001-2004. They also excel in the classroom with a team GPA over 3.0 for the past nine consecutive years and have had two CAC Scholar Athletes of the Year and one NCAA Post-Graduate Scholarship winner.
In 2003, the Marymount cheerleading squad won a Capital Athletic Conference championship.


== Clubs and student organizations ==
Marymount University has over 40 clubs and student organizations. The majority of these clubs are managed by the Office of Student Activities while others, like the Student Government Association, fall under the direction of other university offices. Many departments on campus offer specialized clubs based on academic interests, like the Fashion or Psychology clubs.
Students have the opportunity to become involved in groups dedicated to community service or philanthropy like the Campus Ministry Association and the Global Charity Project or participate in a career-related club like the Fashion Club, Interior Design Association, and Students for Free Enterprise. The Student Government Association, Co-Curricular Council, and Activities Programming Board provide students with additional campus leadership opportunities.
The student government has outlawed tobacco use. Throughout university are signs that say "We are proud to be a tobacco free campus."


== Awards and acknowledgments ==
Recognized in The Templeton Guide: Colleges that Encourage Character Development.
Recognized for Quality and Diversity in U.S. News & World Report’s 2009 Edition of America’s Best Colleges.
Recognized by U.S. News & World Report as the 40th Best Master's College (South).


== References ==


== External links ==
Marymount University
Marymount Study Abroad
Marymount University Athletics
Reinsch Library