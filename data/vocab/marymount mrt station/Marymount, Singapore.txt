Marymount is an area located at Bishan, Singapore. It has precincts of Neighbourhood 3. The nearest MRT station is Marymount MRT Station. Its etymology is that it is also known as Shunfu.
Bus 410 usually stops at Marymount (but it was amended to cover the whole Shunfu Road on 30 January 2011).


== References ==