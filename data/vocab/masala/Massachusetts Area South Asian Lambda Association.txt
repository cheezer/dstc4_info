The Massachusetts Area South Asian Lambda Association (MASALA) is a Boston-based organization for "Gay, Lesbian, Bi, Trans, and Questioning" people of South Asian ethnicity. It was founded in 1992 and is the only organization designed specifically for all queer South Asians in New England. For the purposes of MASALA, "South Asian" is defined as being from "Afghanistan, Bangladesh, Bhutan, Burma, India, Maldives, Nepal, Pakistan, Sri Lanka, and Tibet; and from the global South Asian Diaspora".
It is a frequent sponsor of film screenings, lectures and other cultural events. It also hosts an annual party in the fall. "Our aim is to increase the awareness of our presence in the social spaces that we inhabit and welcome alliances with like-minded individuals and groups regardless of their ethnic/cultural/sexual identities." MASALA has connections with many of the colleges and universities in New England. The group also holds monthly potlucks at various locations throughout Boston.


== §See also ==

List of LGBT organizations


== §References ==


== §External links ==
Interview with Dr. Aida Khan
Friendster profile
Myspace profile
Jacobs, Ethan, "MASALA Mela gets political", Bay Windows, Wednesday Oct 8, 2008 [1]
Jacobs, Ethan, "Domestic violence orgs. reach out", Bay Windows, Thursday Apr 24, 2008 [2]
Interview with Film maker Sarav Chidambaram