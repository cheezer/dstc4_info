John Maxwell may refer to:


== Arts ==
John Maxwell (actor) (1916–1982), American actor of the 1940s and 1950s
John Maxwell (artist) (1905–1962), Scottish artist
John Maxwell (producer), British film producer associated with British International Pictures
John Maxwell (1824–1895), London-based publisher, husband of Mary Elizabeth Braddon
John Maxwell (writer) (born 1944), American writer
John Alan Maxwell (1904–1984), American artist and illustrator
John C. Maxwell (born 1947), American author and leadership coach
Johnny Maxwell, fictional character in Terry Pratchett novels


== Politics ==
John Maxwell, 1st Baron Farnham (1687–1759), Irish peer and politician
John Maxwell, 2nd Earl of Farnham (1760–1823), Irish Representative peer and politician
John Patterson Bryan Maxwell (1804–1845), New Jersey House Representative for the Whig Party
Sir John Maxwell, 7th Baronet (1768–1844), British Member of the UK Parliament for Paisley
Sir John Maxwell, 8th Baronet, British Member of the UK Parliament for Lanarkshire and Renfrewshire
John Waring Maxwell, Member of the UK Parliament for Downpatrick


== Sports ==
John Maxwell (American football), American football player for John Heisman's Clemson Tigers
John Maxwell (golfer) (1871–1906), American golfer and Olympic silver medalist
John Maxwell (sport shooter), Australian Olympic sport shooter
J. Rogers Maxwell (1875–?), yachtsman; son of John Rogers Maxwell, Sr.


== Other ==
John Maxwell, 4th Lord Maxwell (died 1513), Scottish nobleman and head of the Border family of Maxwell
John Maxwell (bishop) (died 1647), Scottish prelate, Archbishop of Tuam, Bishop of Ross
Vice Admiral John Robert Maxwell, British sailor and Governor of The Bahamas
General Sir John Maxwell (British Army officer) (1859–1929), British Army officer and colonial governor
John Maxwell (Confederate agent), secret agent during the American Civil War
John Maxwell (Medal of Honor) (1841–1931), American Medal of Honor recipient
Sir John Maxwell (police officer) (1882–1968), Chief Constable of Manchester, 1927–1942
John Hall Maxwell (1812–1866), Scottish agriculturist
John Preston Maxwell (1871–1961), Presbyterian obstetric missionary to China
John Rogers Maxwell, Sr. (1846–1910), American railroad executive and yachtsman
John Maxwell, 8th Lord Maxwell (1553–1593), Scottish Catholic nobleman
John Maxwell, 9th Lord Maxwell (died 1613), Scottish Catholic nobleman
John Maxwell (Australian businessman), a Royal Australian Air Force Flight Lieutenant seconded to Royal Air Force, and later businessman