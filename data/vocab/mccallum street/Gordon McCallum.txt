Gordon McCallum (26 May 1919 – 10 September 1989) was an American-born English sound engineer. He won an Academy Award for Best Sound and was nominated for three more in the same category. He worked on over 300 films between 1944 and 1985.


== Selected filmography ==
McCallum won an Academy Award and was nominated for three more:
Won
Fiddler on the Roof (1971)
Nominated
Ryan's Daughter (1970)
Diamonds Are Forever (1971)
Superman (1978)


== References ==


== External links ==
Gordon McCallum at the Internet Movie Database