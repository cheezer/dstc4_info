The McDonald's Sign, also known as McDonald's Store #433 Sign, in Pine Bluff, Arkansas, United States, is the only known surviving example of a single-arch McDonald's sign in Arkansas. The sign was erected in 1962 and remained at its original location until 2007. In 2007 McDonald's Store #433 moved and the sign was renovated and moved to the new location. The McDonald's sign was added to the U.S. National Register of Historic Places in 2006.


== History ==
Before McDonald's Store #433 was built on South Main Street in Pine Bluff, the neighborhood was mostly residential. The area began to change as the mid-20th century approached and commercial enterprises began to appear in the area. The McDonald's was constructed at 1300 South Main Street in 1962 and followed the standard corporate design for the era: a red, white and yellow motif with arches projecting through the roof. Owned by Mike Retzer.
McDonald's Store #433 opened on July 3, 1962, and remained in operation until it was announced in August 2007 that the store would close by the end of the year. A new location was constructed in the 2800 block of South Olive Street, about two miles from the original location. Initially, it was unknown what the sign's fate would be; by September, it was announced that the sign would move to the new location.
As the date of closure approached in September the sign was dismantled and transported to Skylite Sign & Neon in Mabelvale, Arkansas, for renovation work which lasted through October 2007.


== Design ==
The McDonald's sign in Pine Bluff represents a transition between the "Speedee" sign and the now-ubiquitous double golden arches. The design featured in Pine Bluff was used for one year and was relatively rare even when it was being used, even more so today. The first double golden arches sign appeared just three months after the Pine Bluff store opened.
The Pine Bluff sign has several features which are typical of the single-arch style from the early years of McDonald's. They are generally back-lit and made up of plastic panels situated in a metal frame. The single golden arch and red advertising space midway up the sign are hallmarks of the single-arch style McDonald's sign found in Pine Bluff. The sign was manufactured by Sign Crafters of Evansville, Indiana and the plastic sheeting was made by Rohm & Haas Company of Philadelphia.
The design of the single-arch McDonald's sign was heavily influenced by an image of the McDonald family crest shown to Ray Kroc. Kroc then decided to incorporate the crest into the road sign. The arch itself evokes modernism. In Orange Roofs, Golden Arches: The Architecture of American Chain Restaurants Phillip Langdon stated the arch was symbolic of a: "buoyant spirit: a feeling of skyward momentum, symbolic of an aerospace age in which man could hurtle himself into the heavens." Langdon goes on to state that the purpose of the McDonald's arch was to bring a sense of structural modernism in a roadside hamburger stand.


== Historic significance ==

The U.S. National Register of Historic Places added the Pine Bluff McDonald's sign to its listings on August 21, 2006, as McDonald's Store #433 Sign. As the only known surviving example of an early single arch McDonald's sign anywhere in the state of Arkansas, the Pine Bluff sign has statewide significance. McDonald's has used a variety of designs for its signs; consequently, early examples are exceedingly rare. The sign in Pine Bluff is also a good example of a rare early back lit plastic sign. Back lit plastic signs were popularized post-World War II and revolutionized the sign industry through the 1950s and 60s.
It remains unclear exactly how many single-arch McDonald's signs remain nationwide. Prior to Hurricane Katrina in 2005, one was still in use in Biloxi, Mississippi. The store on MacArthur Boulevard in Springfield, Illinois also had a single arch up until 2006. Others exist in Lancaster, Pennsylvania, Magnolia, New Jersey [1], Green Bay, Wisconsin [2], St Clair Shores, Michigan; Warren, Michigan; Huntsville, Alabama; Independence, Missouri; Winter Haven, Florida, Belleville, Illinois; and Muncie, Indiana. The sign in Pine Bluff is the only such sign that has its own listing on the National Register, though a few others may exist as contributing properties within historic districts. It is likely that the majority of single-arch signs were incorporated into private collections and it is rare to see them in use.


== See also ==

Golden Arches
Oldest McDonald's restaurant in Downey, California


== References ==


== Further reading ==
Auer, Michael J. "The Preservation of Historic Signs", Preservation Brief #25, October 1991, National Park Service, accessed April 22, 2008.
Langdon, Philip. Orange Roofs, Golden Arches: The Architecture of American Chain Restaurants, (Google Books), Joseph, 1986, (ISBN 0-7181-2788-9).