Mee siam, which means "Siamese noodle" in Malay, is a dish of thin rice vermicelli popular in Singapore and Malaysia. It is said to have originated either from the Malay and Peranakan communities and is hard to discern as many Peranakan dishes are of Malay origin. As the name suggests, it is inspired or adapted from Thai flavours.
In Singapore, it is served with spicy, sweet and sour light gravy. The gravy is made from a rempah spice paste, tamarind and taucheo (salted soy bean). Mee Siam is typically garnished with shredded omelette, scallions, bean sprouts, garlic chives, and lime wedges. In Malaysia a "dry" version is more commonly found, which is essentially stir frying the rice noodles with the same ingredients used in the Singaporean version.
In Thailand a very similar dish is known as mi kathi (noodles with coconut milk), a noodle dish popularly eaten as lunch in the Central Region. It is made by stir frying rice vermicelli noodles with a fragrant and thick sauce that has a similar taste profile as Mee Siam. The sauce is made from coconut milk mixed with minced pork, prawns, firm bean curd, salted soy bean, bean sprouts, garlic chives, and tamarind. It is served with thinly sliced egg omelet, fresh bean sprouts, fresh garlic and banana blossom.


== See also ==
Malay cuisine


== References ==