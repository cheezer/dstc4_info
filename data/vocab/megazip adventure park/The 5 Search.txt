The 5 Search premiered on MediaCorp Channel 5 on 4 January 2015. It is a Singaporean reality-acting-hosting competition programme that finds the next face of the broadcasting channel, Channel 5. The judges are actress and Dim Sum Dollies member Selena Tan, actor Tay Ping Hui and host Bryan Wong. Class 95FM radio personality Jean Danker was employed as the host of the show.
Each week, there would be two segments of competition – one with regards to acting, and another for hosting – which all comes with a particular theme. The contestants on the show are required to outperform their fellow competitors in these segments to survive elimination for the week. They are also mentored by local industry veterans and get the opportunity to act or host alongside with them.
On 1 March 2015, Kayly Loh was announced the winner of the The 5 Search, with Shrey Bhargava and Aiken Chia as the runners-up. She received a two-year artiste management contract from MediaCorp, and a cash prize of $30,000.


== Top 15 finalists ==


== Finals ==
Color key:


=== Week 1 – Top 15 ===
Guest judge: Najip Ali
The show kicked off with a hosting challenge, which the contestants were tasked to pitch a product that was picked out by random to the judges within one minute. The winner of this challenge received immunity and was immediately saved from elimination for the week. For the acting challenge, the contestants were split into three groups of five, and were tasked to work on a scene from the local situation comedy Spouse for House. The resident judges also acted as the mentors for the contestants.


=== Week 2 – Top 12 ===
Guest judge: Irene Ang
This week, the hosting challenge of the show required the contestants to host under circumstances that were out of their comfort zones. They were tasked to deliver an opening link, conduct an interview with an interviewee, perform an ad-lib, as well as a closing link. They were judged based on their poise and professionalism despite the discomfort. The winner of this challenge received immunity and was immediately saved from elimination for the week. For the acting challenge, the contestants paired up with one another to act out a two-minute scene, and all of them were given acting roles that were out of their personal comfort zones.
Two-minute scene titles and plots
^1 Ron Teh & Michelle Wong: The Interrogation – An intelligent cop and a psychotic killer pit their wits against each other
^2 Aiken Chia & Gina Lin: Unfilial – A retrenched factory supervisor pleads with her well-doing brother for money to support their parents
^3 Caryn Cheng & Esther Goh: Frenemies – Two former schoolmates meet and the snob treats the other with disdain
^4 Fadhli Abdullah & Laanya Asogan: Love Hurts – An abusive husband punishes his wife for scratching his car
^5 Jeevan Kularetnam & Kayly Loh: Broken Vows – A wife confronts her cheating husband about his affair
^6 Shrey Bhargava & Leroy Yap: Amateur Killers – Two bumbling friends plan to dispose a body after killing an acquaintance accidentally


=== Week 3 – Top 9 ===
Guest judge: Michelle Chong
Guest mentor: Darren Foong
This week, the show started off with the acting challenge. The contestants were split into three groups of three, and required to come up with a two-minute skit on the spot in the improvisational theatre format. Unlike the previous weeks, the winners of this challenge received an advantage for the following hosting challenge, instead of an immunity. For the hosting challenge, the contestants were required to conduct an interview with one of the following local celebrities – Pierre Png, Aileen Tan, and Judee Tan, who acted as the "difficult guests" on a talk show. The winners of the acting challenge were given the opportunity to chat with their interviewees for 10 minutes, while the rest were given five minutes to get ready at the studio. As all contestants performed well during the acting challenge, the results for this week were solely based on the hosting challenge.
Two-minute improvisational theatre skit plot
^1 Shrey is an overeager real estate agent who is trying to sell a haunted house to newlyweds Aiken and Kayly.
^2 Esther is a lost traveller who has been invited into the home of an elderly couple played by Leroy and Caryn, and they want to eat her.
^3 Fadhli and Gina are mourning the lost of their mother, played by Michelle, when she rises from the dead as a vampire.


=== Week 4 – Top 8 ===
Guest judges: Georgina Chang and Kumar
Guest mentor: Robb Weller
This week, the show commenced with the hosting challenge. The contestants were required to host in pairs on a food show, and were assessed on their adaptability to new elements and last-minute changes in the course of their hosting. The winner of the hosting challenge was given the opportunity to join the hosts of the morning show on 987FM on the following day. For the acting challenge, the contestants were paired up once again and were tasked to act on the set of long-running drama series 118. The winners of the acting challenge were given the chance to watch and meet the casts of the musical Cats.
Bryan Wong did not appear on the judging panel due to work commitments with a travel show and was replaced by MediaCorp Radio's vice-president for English programming Georgina Chang.


=== Week 5 – Top 7 ===
Guest judge: Chua En Lai
This week, the show commenced with the acting challenge. The contestants were required to work with the creative and executive producer of comedy television series The Noose Prem Anand to create a "noose-worthy" comedy character. They were required to perform their own research on the possible characters that they would like to play, and later pitch their ideas to Anand who then assisted them in the scripting and developing of their characters The winner of the challenge was given the opportunity to appear in one episode of the upcoming season of The Noose. For the hosting challenge, the contestants were tasked to host an entertainment segment about the Celebrate SG50 event. They were given 24 hours to research about the event, and the celebrities that they were going to hold the backstage interviews with. They were judged based on their prep work, quality of the interview question, interaction with the celebrity interviewees, and ability to convey the sense of excitement and party atmosphere to the television audience.


=== Week 6 – Top 6 (first week) ===
Guest judge: Terence Cao
Guest mentor: Sunny Pang
This week, the show commenced with the acting challenge. The contestants were required to work with MediaCorp actor Terence Cao on an acting sequence, and they were all given the same script and scenes to act. The winner of the challenge was given the opportunity to attend a-week-long of action classes conducted by Sunny Pang to perfect their skills. For the hosting challenge, the contestants were required to work in pairs together with a charity organisation. Each pair had to present a story, and helping their respective charity organisation with their day-to-day operations.
It was revealed that Aiken Chia and Michelle Wong were in the bottom 2. However, the two contestants were then declared safe by Bryan Wong, who stated that there was no elimination for the week.


=== Week 7 – Top 6 (second week) ===
Guest judge: Sebastian Tan
Guest mentor: Dim Sum Dollies
This week, the show commenced with the hosting challenge. The contestants were required to hold a live interview with various celebrity interviewees on the set of The 5 Show. The winner of the challenge was given the opportunity to host a video segment on The 5 Show. For the acting challenge, the contestants were required to work in pairs, and reinterpret local musical numbers.


=== Week 8 – Final: Top 5 ===
Guest judges: Georgina Chang and Eric Khoo
Group performance: "Wishing on a Star" / "Superstar" / "Starships"
Musical guest: Secondhand Serenade ("Heart Stops (By the Way)" with Veronica Ballestrini and "Fall for You")
Prize presenter: Rebecca Lim
There were four challenges during the final – drama acting challenge, comedy acting challenge, pre-recorded hosting challenge and live hosting challenge. In the drama acting challenge, the contestants all acted in a pre-recorded short skit together with MediaCorp actress Priscelia Chan. In the comedy acting challenge, each of the contestants collaborated with a comedy actor and put up a live comedy sketch. In the pre-recorded hosting challenge, each of the contestants hosted their own show that came with a celebrity guest appearance. For the live hosting challenge, the contestants paired up with Jean Danker and presented their fellow contestants and celebrities who appeared in the comedy acting challenge.
The judges' scores made up 50% of the overall results, and the remaining 50% were determined by public votes via the Toggle Now application or text messaging.


== Elimination chart ==