Apocalypse Memories is an original novel based on the U.S. television series Buffy the Vampire Slayer.


== Plot summary ==
Willow has arrived back in Sunnydale after spending time with Giles in England. She is terrified of using her magic powers again for fear of a return to dark magic consuming her. However, suddenly Sunnydale once again becomes a center of the weirdness when an angel named Michael brings on signs of the Apocalypse. The angel insists that this is not an artificial one, the type Buffy had stopped before, this is the natural and long-established end of the world.
Willow must find a way to overcome her fear of magic in order to perform one of the most dangerous spells known to mankind; the Belial Siphon, which has not been performed before. Meanwhile, Buffy is trying to stop an Apocalypse of her own accord, yet Buffy cannot seem to fight what is thrown at her.


== Continuity ==
Supposed to be set during seventh season of Buffy the Vampire Slayer, shortly after "Same Time, Same Place".


=== Canonical issues ===

Buffy books such as this one are not usually considered by fans as canonical. Some fans consider them stories from the imaginations of authors and artists, while other fans consider them as taking place in an alternative fictional reality. However unlike fan fiction, overviews summarising their story, written early in the writing process, were 'approved' by both Fox and Joss Whedon (or his office), and the books were therefore later published as officially Buffy/Angel merchandise.
The book involves Buffy driving people around Sunnydale. As far as we know from canon, Buffy had limited driving capabilities. She tried and failed to obtain a licence during season 3.
This marks one of the few occasions where overt christian mythology is incorporated into the core Buffy mythology. Such instances only appear outside of the core canon.


== External links ==


=== Reviews ===
Litefoot1969.bravepages.com - Review of this book by Litefoot
Teen-books.com - Reviews of this book
Nika-summers.com - Review of this book by Nika Summers
Shadowcat.name - Review of this book