The Miami-Dade County Public Schools district has 58 middle schools. This page shows first a list of names and then a more descriptive list of some middle schools.


== Simple list of middle schools in the district ==
These middle schools serve grades 6-8 with exceptions as noted (names in boldface are the subject of sections of this article):


== Middle schools without their own articles ==


=== Highland Oaks Middle School ===
It is one of two hurricane animal shelters in the county.


=== Ponce De Leon Middle School ===
Ponce De Leon Middle School is a magnet school in Coral Gables, Florida. The school was founded in 1924 by Robertson Olsen. It was originally a high school and later became a middle school.


=== Ruben Dario Middle School ===
Ruben Dario Middle School (RDMS) is a public school in Miami, Florida. It has around 1000 students. The school mascot is the cougar.


=== Shenandoah Middle School ===
Shenandoah Middle School is a public-magnet middle school in the historic Shenandoah section of Miami. It offers a Museum Magnet program for grades 6-8. The curriculum of a Museum Magnet is based on close collaboration with museums for educational expertise and resources.
Shenandoah partners with these museums:


=== W.R Thomas Middle School ===
W.R Thomas Middle School is a certified Cambridge Magnet middle school located in Miami, Florida.


== Notes ==
^ http://shenandoahmiddle.dadeschools.net/museums_magnet.asp