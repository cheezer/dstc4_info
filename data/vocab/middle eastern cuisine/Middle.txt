Middle or The Middle may refer to:
Centre (geometry), the point equally distant from the outer limits.


== PlacesEdit ==
Middle (sheading), a subdivision of the Isle of Man
Middle Township (disambiguation)
Middle Lake (disambiguation)
Middle River (disambiguation)
Middle Creek (disambiguation)
Middle Brook (Raritan River)
Middle Island (disambiguation)
Middle Bay (disambiguation)
Middle Mountain, California
Middle Peninsula, Chesapeake Bay, Virginia
Middle Rocks, two rocks at the eastern opening of the Straits of Singapore


== MusicEdit ==
"Middle", a song by Rocket from the Crypt from their 1995 album Scream, Dracula, Scream!
"The Middle" (song), by Jimmy Eat World
"The Middle", song by Demi Lovato from her debut album Don't Forget


== Other usesEdit ==
The Middle (TV series), an ABC TV series that made its debut in September 2009
Middle Road (disambiguation)


== See alsoEdit ==
All pages beginning with "Middle"
Core (disambiguation)
Seed