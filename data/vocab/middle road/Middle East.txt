The Middle East (also called the Mid East) is a eurocentric description of a region centered on Western Asia and Egypt. The corresponding adjective is Middle-Eastern and the derived noun is Middle-Easterner. Formerly, the eurocentric synonym Near East (as opposed to Far East) was commonly used. Arabs, Azeris, Kurds, Persians, and Turks constitute the largest ethnic groups in the region by population, while Armenians, Assyrians, Circassians, Copts, Druze, Jews, Maronites, Somalis, and other denominations form significant minorities.
The history of the Middle East dates back to ancient times, and the region has generally been a major center of world affairs. However, in the context of its ancient history, the term Near East was used in reference to the Eastern Mediterranean/Ottoman Empire regions, while the term Middle East was restricted to the area between the aforementioned Near— and Far East (Mesopotamia to India). Several major religions have their origins in the Middle East, including Judaism, Christianity, and Islam; the Baha'i faith, Mandaeism, Unitarian Druze, and numerous other belief systems were also established within the region. The Middle East generally has a hot, arid climate, with several major rivers providing irrigation to support agriculture in limited areas such as the Nile Delta in Egypt, the Tigris and Euphrates watersheds of Mesopotamia, and most of what is known as the Fertile Crescent. Most of the countries that border the Persian Gulf have vast reserves of crude oil, with the dictatorships of the Arabian Peninsula in particular benefiting from petroleum exports. In modern times the Middle East remains a strategically, economically, politically, culturally and religiously sensitive region.


== Terminology ==
The term "Middle East" may have originated in the 1850s in the British India Office. However, it became more widely known when American naval strategist Alfred Thayer Mahan used the term in 1902 to "designate the area between Arabia and India". During this time the British and Russian Empires were vying for influence in Central Asia, a rivalry which would become known as The Great Game. Mahan realized not only the strategic importance of the region, but also of its center, the Persian Gulf. He labeled the area surrounding the Persian Gulf as the Middle East, and said that after the Suez Canal, it was the most important passage for Britain to control in order to keep the Russians from advancing towards British India. Mahan first used the term in his article "The Persian Gulf and International Relations", published in September 1902 in the National Review, a British journal.

The Middle East, if I may adopt a term which I have not seen, will some day need its Malta, as well as its Gibraltar; it does not follow that either will be in the Persian Gulf. Naval force has the quality of mobility which carries with it the privilege of temporary absences; but it needs to find on every scene of operation established bases of refit, of supply, and in case of disaster, of security. The British Navy should have the facility to concentrate in force if occasion arise, about Aden, India, and the Persian Gulf.

Mahan's article was reprinted in The Times and followed in October by a 20-article series entitled "The Middle Eastern Question," written by Sir Ignatius Valentine Chirol. During this series, Sir Ignatius expanded the definition of Middle East to include "those regions of Asia which extend to the borders of India or command the approaches to India." After the series ended in 1903, The Times removed quotation marks from subsequent uses of the term.
Until World War II, it was customary to refer to areas centered around Turkey and the eastern shore of the Mediterranean as the "Near East", while the "Far East" centered on China, and the Middle East then meant the area from Mesopotamia to Burma, namely the area between the Near East and the Far East. In the late 1930s, the British established the Middle East Command, which was based in Cairo, for its military forces in the region. After that time, the term "Middle East" gained broader usage in Europe and the United States, with the Middle East Institute founded in Washington, D.C. in 1946, among other usage.


=== Criticism and usage ===

The term Middle East has been criticized as implicitly Eurocentric. In contemporary English-language academic and media venues, the term is used by both Europeans and non-Europeans.
The description Middle has also led to some confusion over changing definitions. Before the First World War, "Near East" was used in English to refer to the Balkans and the Ottoman Empire, while "Middle East" referred to Iran, the Caucasus, Afghanistan, Central Asia, and Turkestan. In contrast, "Far East" referred to the countries of East Asia (e.g. China, Japan, Formosa, Korea, Hong Kong, etc.)
With the disappearance of the Ottoman Empire in 1918, "Near East" largely fell out of common use in English, while "Middle East" came to be applied to the re-emerging countries of the Islamic world. However, the usage "Near East" was retained by a variety of academic disciplines, including archaeology and ancient history, where it describes an area identical to the term Middle East, which is not used by these disciplines (see Ancient Near East).
The first official use of the term "Middle East" by the United States government was in the 1957 Eisenhower Doctrine, which pertained to the Suez Crisis. Secretary of State John Foster Dulles defined the Middle East as "the area lying between and including Libya on the west and Pakistan on the east, Syria and Iraq on the North and the Arabian peninsula to the south, plus the Sudan and Ethiopia." In 1958, the State Department explained that the terms "Near East" and "Middle East" were interchangeable, and defined the region as including only Egypt, Syria, Israel, Lebanon, Jordan, Iraq, Saudi Arabia, Kuwait, Bahrain, and Qatar.
The Associated Press Stylebook says that Near East formerly referred to the farther west countries while Middle East referred to the eastern ones, but that now they are synonymous. It instructs:

Use Middle East unless Near East is used by a source in a story. Mideast is also acceptable, but Middle East is preferred.

At the United Nations, the numerous documents and resolutions about the Middle East are in fact concerned with the Arab–Israeli conflict, in particular the Israeli–Palestinian conflict, and, therefore, with the four states of the Levant. The term Near East is occasionally heard at the UN when referring to this region.


=== Translations ===
There are terms similar to Near East and Middle East in other European languages, but since it is a relative description, the meanings depend on the country and are different from the English terms generally. In German the term Naher Osten (Near East) is still in common use (nowadays the term Mittlerer Osten is more and more common in press texts translated from English sources, albeit having a distinct meaning) and in Russian Ближний Восток or Blizhniy Vostok, Bulgarian Близкия Изток, Polish Bliski Wschód or Croatian Bliski istok (meaning Near East in all the four Slavic languages) remains as the only appropriate term for the region. However, some languages do have "Middle East" equivalents, such as the French Moyen-Orient, Swedish Mellanöstern, Spanish Oriente Medio or Medio Oriente, and the Italian Medio Oriente.
Perhaps because of the influence of the Western press, the Arabic equivalent of Middle East (Arabic: الشرق الأوسط ash-Sharq al-Awsaṭ), has become standard usage in the mainstream Arabic press, comprehending the same meaning as the term "Middle East" in North American and Western European usage. The designation, Mashriq, also from the Arabic root for east, also denotes a variously defined region around the Levant, the eastern part of the Arabic-speaking world (as opposed to the Maghreb, the western part). Even though the term originated in the West, apart from Arabic, other languages of countries of the Middle East also use a translation of it. The Persian equivalent for Middle East is خاورمیانه (Khāvar-e miyāneh), the Hebrew is המזרח התיכון (hamizrach hatikhon) and the Turkish is Orta Doğu.


== Territories and regions ==


=== Traditional definition of the Middle East ===
Traditionally included within the Middle East are Iran (Persia), Asia Minor, Mesopotamia, the Levant, the Arabian Peninsula, and Egypt. In terms of modern-day countries, these are:
 Bahrain
 Cyprus
 Egypt
 Iran
 Iraq
 Israel
 Jordan
 Kuwait
 Lebanon
 Oman
 Palestine
 Qatar
 Saudi Arabia
 Syria
 Turkey
 United Arab Emirates
 Yemen


=== Other definitions of the Middle East ===

Various concepts are often being paralleled to Middle East, most notably Near East, Fertile Crescent and the Levant. Near East, Levant and Fertile Crescent are geographic concepts, which refer to large sections of the modern defined Middle East, with Near East being the closest to Middle East in its geographic meaning.
The countries of the South Caucasus—Armenia, Azerbaijan, and Georgia—are occasionally included in definitions of the Middle East.
The Greater Middle East is a political term coined by the second Bush administration in the first decade of the 21st century, to denote various countries, pertaining to the Muslim world, specifically Iran, Turkey, Afghanistan and Pakistan. Various Central Asian countries are sometimes also included.


== History ==

The Middle East lies at the juncture of Eurasia and Africa and of the Mediterranean Sea and the Indian Ocean. It is the birthplace and spiritual center of religions such as Christianity, Islam, Judaism, Manichaeism, Yezidi, Druze, Yarsan and Mandeanism, and in Iran, Mithraism, Zoroastrianism, Manicheanism, and the Bahá'í Faith. Throughout its history the Middle East has been a major center of world affairs; a strategically, economically, politically, culturally, and religiously sensitive area.
The world's earliest civilizations, Mesopotamia (Sumer, Akkad, Assyria and Babylonia) and ancient Egypt, originated in the Fertile Crescent and Nile Valley regions of the ancient Near East. These were followed by the Hittite, Greek and Urartian civilisations of Asia Minor, Elam in pre-Iranian Persia, as well as the civilizations of the Levant (such as Ebla, Ugarit, Canaan, Aramea, Phoenicia and Israel), Persian and Median civilizations in Iran, North Africa (Carthage/Phoenicia) and the Arabian Peninsula (Magan, Sheba, Ubar). The Near East was first largely unified under the Neo Assyrian Empire, then the Achaemenid Empire followed later by the Macedonian Empire and after this to some degree by the Iranian empires (namely the Parthian and Sassanid Empires), the Roman Empire and Byzantine Empire. However, it would be the later Arab Caliphates of the Middle Ages, or Islamic Golden Age which began with the Arab conquest of the region in the 7th century AD, that would first unify the entire Middle East as a distinct region and create the dominant Islamic ethnic identity that largely (but not exclusively) persists today. The Mongols, the Turkish Seljuk and Ottoman Empires, the Safavids and the British Empire would also later dominate the region.
The modern Middle East began after World War I, when the Ottoman Empire, which was allied with the Central Powers, was defeated by the British Empire and their allies and partitioned into a number of separate nations, initially under British and French Mandates. Other defining events in this transformation included the establishment of Israel in 1948 and the eventual departure of European powers, notably Britain and France by the end of the 1960s. They were supplanted in some part by the rising influence of the United States from the 1970s onwards.
In the 20th century, the region's significant stocks of crude oil gave it new strategic and economic importance. Mass production of oil began around 1945, with Saudi Arabia, Iran, Kuwait, Iraq, and the United Arab Emirates having large quantities of oil. Estimated oil reserves, especially in Saudi Arabia and Iran, are some of the highest in the world, and the international oil cartel OPEC is dominated by Middle Eastern countries.
During the Cold War, the Middle East was a theater of ideological struggle between the two superpowers and their allies: NATO and the United States on one side, and the Soviet Union and Warsaw Pact on the other, as they competed to influence regional allies. Of course, besides the political reasons there was also the "ideological conflict" between the two systems. Moreover, as Louise Fawcett argues, among many important areas of contention, or perhaps more accurately of anxiety, were, first, the desires of the superpowers to gain strategic advantage in the region, second, the fact that the region contained some two thirds of the world's oil reserves in a context where oil was becoming increasingly vital to the economy of the Western world [...] Within this contextual framework, the United States sought to divert the Arab world from Soviet influence. Throughout the 20th and 21st centuries, the region has experienced both periods of relative peace and tolerance and periods of conflict particularly between Sunnis and Shiites.


== Demographics ==


=== Ethnic groups ===

The Middle East is today home to numerous long established ethnic groups, including; Arabs, Armenians, Assyrians, Azeris, Balochs, Bengalis, Circassians, Copts, Crimean Tatars, Druze, Filipinos, Gagauz, Georgians, Greeks, Hindus, Jews, Kurds, Lurs, Maltese, Mandaeans, Maronites, Mhallami, Ossetians, Pakistanis, Pashtuns, Persians, Punjabis, Roma, Samaritans, Shabaks, Sikhs, Sindhis, Somalis, Sri Lankans, Tats, Turks, Turcomans, Yazidis, and Zazas.


=== Migration ===
According to the International Organization for Migration, there are 13 million first-generation migrants from Arab nations in the world, of which 5.8 reside in other Arab countries. Expatriates from Arab countries contribute to the circulation of financial and human capital in the region and thus significantly promote regional development. In 2009 Arab countries received a total of 35.1 billion USD in remittance in-flows and remittances sent to Jordan, Egypt and Lebanon from other Arab countries are 40 to 190 per cent higher than trade revenues between these and other Arab countries. In Somalia, the Somali Civil War has greatly increased the size of the Somali diaspora, as many of the best educated Somalis left for Europe, North America and other Middle Eastern countries.
Non-Arab Middle Eastern countries such as Turkey, Israel and Iran are also subject to important migration dynamics.
A fair proportion of those migrating from Arab nations are from ethnic and religious minorities facing racial and or religious persecution and are not necessarily ethnic Arabs, Iranians or Turks. Large numbers of Kurds, Jews, Assyrians, Greeks and Armenians as well as many Mandeans have left nations such as Iraq, Iran, Syria and Turkey for these reasons during the last century. In Iran, many religious minorities such as Christians, Baha'is and Zoroastrians have left since the Islamic Revolution of 1979.


=== Religions ===

The Middle East is very diverse when it comes to religions, many of which originated there. Islam is the largest religion in the Middle East, but other faiths that originated there, such as Judaism and Christianity, are also well represented. Christians represent 41% of Lebanon, where the President, Army General and Central Bank Governor are required to be Christian. There are also important minority religions like the Bahá'í Faith, Yazdânism, Zoroastrianism, Mandeanism, Druze, Yarsan, Yazidism and Shabakism, and in ancient times the region was home to Mesopotamian Religion, Canaanite Religion, Manicheanism, Mithraism and various Monotheist Gnostic sects.


=== Languages ===
The five top languages, in terms of numbers of speakers, are Arabic, Persian, Turkish, Berber, and Kurdish. Arabic and Berber represent the Afro-Asiatic language family. Persian and Kurdish belong to the Indo-European language family. Turkish belongs to Turkic language family. About 20 minority languages are also spoken in the Middle East.
Arabic, with all its dialects, are the most widely spoken languages in the Middle East, with Literary Arabic being official in all North African and in most West Asian countries. Arabic dialects are also spoken in some adjacent areas in neighbouring Middle Eastern non-Arab countries. It is a member of the Semitic branch of the Afro-Asiatic languages.
Persian is the second most spoken language. While it is primarily spoken in Iran and some border areas in neighbouring countries, the country is one of the region's largest and most populous. It belongs to the Indo-Iranian branch of the family of Indo-European languages.
The third-most widely spoken language, Turkish, is largely confined to Turkey, which is also one of the region's largest and most populous countries, but it is present in areas in neighboring countries. It is a member of the Turkic languages, which have their origins in Central Asia.
Other languages spoken in the region include Semitic languages such as Hebrew and Aramaic dialects spoken mainly by Assyrians and Mandeans. Also to be found are Armenian, Azerbaijani, Somali, Berber which is spoken across North Africa, Circassian, smaller Iranian languages, Kurdish languages, smaller Turkic languages (such as Gagauz), Shabaki, Yazidi, Roma, Georgian, Greek, and several Modern South Arabian languages such as Geez. Maltese is also linguistically and geographically a Middle Eastern language.
English is commonly taught and used as a second language, especially among the middle and upper classes, in countries such as Egypt, Jordan, Israel, Iran, Kurdistan, Iraq, Qatar, Bahrain, United Arab Emirates and Kuwait. It is also a main language in some Emirates of the United Arab Emirates.
French is taught and used in many government facilities and media in Lebanon. It is taught in some primary and secondary schools of Egypt, Israel and Syria.
Hindustani (Urdu and Hindi) is widely spoken by migrant communities in many Middle Eastern countries, such as Saudi Arabia (where 20-25% of the population is South Asian), the United Arab Emirates (where 50-55% of the population is South Asian), and Qatar, which have large numbers of Pakistani and Indian immigrants.
The largest Romanian-speaking community in the Middle East is found in Israel, where as of 1995 Romanian is spoken by 5% of the population. Russian is also spoken by a large portion of the Israeli population, because of emigration in the late 1990s. Amharic and other Ethiopian languages are spoken by Ethiopian minorities.


== Economy ==

Middle Eastern economies range from being very poor (such as Gaza and Yemen) to extremely wealthy nations (such as Qatar and UAE). Overall, as of 2007, according to the CIA World Factbook, all nations in the Middle East are maintaining a positive rate of growth.
According to the World Bank's World Development Indicators database published on July 1, 2009, the three largest Middle Eastern economies in 2008 were Turkey ($794,228,000,000), Saudi Arabia ($467,601,000,000) and Iran ($385,143,000,000) in terms of Nominal GDP. Regarding nominal GDP per capita, the highest ranking countries are Qatar ($93,204), the UAE ($55,028), Kuwait ($45,920) and Cyprus ($32,745). Turkey ($1,028,897,000,000), Iran ($839,438,000,000) and Saudi Arabia ($589,531,000,000) had the largest economies in terms of GDP-PPP. When it comes to per capita (PPP)-based income, the highest-ranking countries are Qatar ($86,008), Kuwait ($39,915), the UAE ($38,894), Bahrain ($34,662) and Cyprus ($29,853). The lowest-ranking country in the Middle East, in terms of per capita income (PPP), is the autonomous Palestinian Authority of Gaza and the West Bank ($1,100).
The economic structure of Middle Eastern nations are different in the sense that while some nations are heavily dependent on export of only oil and oil-related products (such as Saudi Arabia, the UAE and Kuwait), others have a highly diverse economic base (such as Cyprus, Israel, Turkey and Egypt). Industries of the Middle Eastern region include oil and oil-related products, agriculture, cotton, cattle, dairy, textiles, leather products, surgical instruments, defence equipment (guns, ammunition, tanks, submarines, fighter jets, UAVs, and missiles). Banking is also an important sector of the economies, especially in the case of UAE and Bahrain.
Except for Cyprus, Turkey, Egypt, Lebanon and Israel, tourism has been a relatively undeveloped area of the economy, in part because of the socially conservative nature of the region as well as political turmoil in certain regions of the Middle East. In recent years, however, countries such as the UAE, Bahrain, and Jordan have begun attracting greater number of tourists because of improving tourist facilities and the relaxing of tourism-related restrictive policies.
Unemployment is notably high in the Middle East and North Africa region, particularly among young people aged 15–29, a demographic representing 30% of the region's total population. The total regional unemployment rate in 2005, according to the International Labor Organization, was 13.2%, and among youth is as high as 25%, up to 37% in Morocco and 73% in Syria.


== Gallery ==


== See also ==


== Notes ==


== References ==


=== General ===
Adelson, Roger (1995). London and the Invention of the Middle East: Money, Power, and War, 1902-1922. Yale University Press. ISBN 0-300-06094-7. 
Anderson, R; Seibert, R; Wagner, J. (2006). Politics and Change in the Middle East (8th ed.). Prentice-Hall. 
Barzilai, Gad; Aharon, Klieman; Gil, Shidlo (1993). The Gulf Crisis and its Global Aftermath. Routledge. ISBN 0-415-080029. 
Barzilai, Gad (1996). Wars, Internal Conflicts and Political Order. State University of New York Press. ISBN 0-7914-2943-1. 
Beaumont, Peter; Blake, Gerald H; Wagstaff, J. Malcolm (1988). The Middle East: A Geographical Study. David Fulton. ISBN 0-470-21040-0. 
Goldschmidt, Arthur Jr (1999). A Concise History of the Middle East. Westview Press. ISBN 0-8133-0471-7. 


=== Citations ===


== Further reading ==
Cressey, George B. (1960). Crossroads: Land and Life in Southwest Asia. Chicago, IL: J.B. Lippincott Co. xiv, 593 p., ill. with maps and b&w photos.
Freedman, Robert O. (1991). The Middle East from the Iran-Contra Affair to the Intifada, in series, Contemporary Issues in the Middle East. 1st ed. Syracuse, NY: Syracuse University Press. x, 441 p. ISBN 0-8156-2502-2 pbk.


== External links ==

"Middle East - Articles by Region" - Council on Foreign Relations: "A Resource for Nonpartisan Research and Analysis"
"Middle East - Interactive Crisis Guide" - Council on Foreign Relations: "A Resource for Nonpartisan Research and Analysis"
Middle East Department University of Chicago Library
Middle East Business Intelligence since 1957: "The leading information source on business in the Middle East" - MEED.com
Carboun - advocacy for sustainability and environmental conservation in the Middle East
Middle East at DMOZ
Middle East News from Yahoo! News
Middle East Business, Financial & Industry News — ArabianBusiness.com