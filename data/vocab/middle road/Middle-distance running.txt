Middle-distance running events are track races longer than sprints, up to 3000 metres. The standard middle distances are the 800 metres, 1500 metres and mile run, although the 3000 metres may also be classified as a middle-distance event. The 880 yard run, or half mile, was the forebear to the 800 m distance and it has its roots in competitions in the United Kingdom in the 1830s. The 1500 m came about as a result of running three laps of a 500 m track, which was commonplace in continental Europe in the 20th century.


== Events ==


=== 600 yards ===
This was a popular distance, particularly indoors, when imperial distances were common. In 1882, American Lon Myers set what was then a world record at 600 yards (548.64 metres), running it in 1:11.4. The event was a common event for most American students, because it was one of the standardized test events as part of the President's Award on Physical Fitness. In the early 1970s, Martin McGrady was unsuccessful at longer or shorter races but made his reputation, set world records and drew a lot of fans to the arenas to watch him race elite Olympians at this odd distance.


=== 600 m ===
This middle distance length is rather uncommon, and is mainly run by sprinters wishing to test their endurances at a longer distance. Like other middle distance races, it evolved from the 600 yard race. The 600 m is also used as an early season stepping stone by 800 m runners before they have reached full race fitness. The record at this distance is for men:

Johnny Gray (United States) 1:12.81 Santa Monica 24 May 1986

For women:

Ana Fidelia Quirot (Cuba) 1:22.63 Guadalajara 25 July 1997


=== 800 m ===

The 800 m consists of two laps around a standard 400 m track, and has always been an Olympic event. It was included in the first women's track programme in 1928, but suspended until 1960 because of shock and the exhaustion it caused the competitors. Without the benefits of modern training, men of the era were, in contrast, expected to run themselves to complete exhaustion during competitions.
The current record is for men:

David Rudisha (Kenya) 1:40.91 London 9 August 2012

For women:

Jarmila Kratochvílová (Czechoslovakia) 1:53.28 Munich, 26 July 1983


=== 1000 m ===
This distance is not commonly raced, though it is more common than the 500 m event is for sprinters. This is commonly raced as an indoor men's heptathlon event, or as an indoor high school event. In 1881, Lon Myers set what was then a world record at 1000 yards, running it in 2:13.0. The record at this distance for men is:

Noah Ngeny (Kenya) 2:11.96 Rieti 5 September 1999

For women:

Svetlana Masterkova (Russia) 2:28.98 Brussels 23 August 1996

See also 1000 metres world record progression


=== 1200 m ===
Three laps. A distance seldom raced on its own, but commonly raced as part of the Distance Medley Relay.
There is no recorded world records or world bests however the mens is believed to be set by Hicham El Guerrouj
Hicham El Guerrouj (Morocco) 2:44.75 Rieti 2002 


=== 1500 m ===

Also known as the metric mile, this is a premier middle-distance race, covering three and three-quarter laps around a standard Olympic-sized track. In recent years, races over this distance have become more of a prolonged sprint, with each lap averaging 55 seconds for the world record performance by Hicham El Guerrouj of Morocco in 1998 at Rome (two 1:50 s 800 m performances back to back). Thus, speed is necessary, and it seems that the more aerobic conditioning, the better. This is a difficult distance at which to compete mentally, in addition to being one of the more tactical middle-distance track events. The distance is often witness to some of the most tactical, physical races in the sport, as many championship races are won in the final few metres. The record at this distance for men is:

Hicham El Guerrouj (Morocco) 3:26.00 Rome 14 July 1998

For women:

Qu Yunxia (China) 3:50.46 Beijing 11 September 1993


=== 1600 m ===
At exactly four laps of a normal 400 m track, this distance is raced as a near replacement for the mile (it is, in fact, 9.344 m, about 30.6 feet, shorter; however, it is still colloquially referred to as "the mile"). The 1600 meters is the official distance for this range of races in US High Schools. The 1500 m, however, is the most common distance run at the college and international levels.
The final leg of a Distance medley relay is 1600 metres. While that race is rarely run outside high school and collegiate invitational competition, it has been held at the international level.
An accurate way to run an actual mile on a metric track is to run the additional 9.344 meters before starting the first marked 400 meter lap. Many tracks, especially high-level tracks, will have a waterfall starting line drawn 9.344 meters back for this purpose. Otherwise, on a metric track, there will be a relay zone 10 meters before the common start/finish line, frequently marked by a triangle pointed toward the finish. In many configurations, that triangle is about half a meter wide, making its point extremely close to the mile start line, which would be slightly less than two feet from the marked relay zone (the widest part of the triangle, or line).


=== Mile ===

This length of middle-distance race, 1760 yards, (1609.344 metres), is very common in countries that do not use the metric system, and is still often referred to as the "Blue Riband" of the track.
When the International Amateur Athletic Federation decided in 1976 to recognize only world records for metric distances, it made an exception for the mile and records are kept to this day.
Historically, the mile took the place that the 1500 m has today. It is still raced on the world class level, but usually only at select occasions, like the famous Wanamaker Mile held annually at the Millrose Games. Running a mile in less than four minutes is a famously difficult achievement, long thought impossible by the scientific community. The first man to break the four-minute barrier was Englishman Roger Bannister at Oxford in 1954. The record at this distance for men is:

Hicham El Guerrouj (Morocco) 3:43.13 Rome 7 July 1999

For women:

Svetlana Masterkova (Russia) 4:12.56 Zürich 14 August 1996


=== 2000 m ===
Another event that is rarely run, a miler's speed will generally allow him/her to prevail at this distance over less balanced challengers. The record at this distance for men is:

Hicham El Guerrouj (Morocco) 4:44.79 Berlin September 7, 1999

For women:

Sonia O'Sullivan (Ireland) 5:25.36 Edinburgh July 8, 1994


=== 3000 m ===

Truly on the borderline between middle and longer distances, the 3000 m (7.5 laps) is a standard race in the United States, though it is not raced at the outdoor IAAF World Championships. This race requires decent speed, but a lack of natural quickness can be made up for with superior aerobic conditioning and supporting race tactics. The record at this distance for men is:

Daniel Komen (Kenya) 7:20.67 Rieti 1 September 1996

For women:

Junxia Wang (China) 8:06.11 Beijing 13 September 1993


=== 3200 m ===
At exactly 8 laps on a standard 400 m track, this event is typically run only in American high schools along with the 1600 m. It is colloquially called the "two-mile", as the distance is only 18.688 metres shorter than two miles. In college, the typical runner of this event would convert to the 5,000 metre run (or potentially the 3,000 metre run during indoor season). It should be noted that in most Eastern-American high schools, colleges, and middle schools, this event is usually considered a long distance event, depending on the region. It is the longest track distance run in most high school competitions.


=== Two Miles ===

This length of long middle-distance or short long-distance race was 3520 yards, (3218.688 metres).
Historically, the 2 mile took the place that the 3000 m and the 3200 m have today. The first man to break the four-minute barrier on both miles for a total of less than 8 minutes was Daniel Komen in 1997, and his time of 7:58.61 remains a world record. The record at this distance for men is:

Daniel Komen (Kenya) 7:58.61 Hetchel, Belgium 19 July 1997

For women:

Meseret Defar (Ethiopia) 8:58.58 Brussels, Belgium 14 September 2007


=== 2,000 metre steeplechase ===
Another race only run in high school or Masters meets. The typical specialist in this event would move up to the 3000m steeplechase in college.


=== 3,000 metre steeplechase ===

The 3,000 metre steeplechase is a distance event requiring greater strength, stamina, and agility than the flat 3,000 metre event. This is because athletes are required to jump over five barriers per lap, after a flat first 200 m to allow for settling in. One barrier per lap is placed in front of a water pit, meaning that runners are also forced to deal with the chafing of wet shoes as they race. The world record for men is:

Saif Saeed Shaheen (Qatar) 7:53.63 Brussels 3 September 2004

For women:

Gulnara Samitova (Russia) 8:58.81 Beijing 17 August 2008


== United States and Japan youth running ==
In the United States, the 3000 m is more common at the high school and collegiate levels (along with the US two mile). In Japan, the 800, 1500 and 3000 metre events are competed in both genders for junior high school and high school, except that high school boys jump to 5000 metres. Both 3000 and 5000 metre distances are sometimes described as long distance but also frequently as middle distance, depending on the context. From the perspective of a longer race like a half marathon, marathon or relays such as the ekiden relay, the 5000 metre race might be viewed as middle distance.
The tables below do not focus on record times but rather on the performance of many runners in a given year (in this case, 2007 and 2008). These are the top 100 (or even 500) junior high school and high school runners in Japan and the USA.


=== 800 metres ===


=== 1500 metres ===
A few states of the USA use this distance, among them Oregon, Florida, Massachusetts, and Rhode Island.


=== 1600 metres ===


=== 3000 metres ===
A few states of the USA use this distance, among them Oregon, Massachusetts, Florida, and Rhode Island.


=== 3200 metres ===


=== 2000 metre steeplechase ===
In the USA, the steeplechase is still relatively uncommon in high school. One example is New York state, where high school boys compete in the 2000 metre steeplechase.


=== 3000 metre steeplechase ===


=== 5000 metres ===
(Not a middle distance event)
Japanese secondary school boys regularly run 5000 metres on the track rather than 3000 meters. USA high school boys rarely run this distance except during cross country.


== See also ==
Long-distance running
The Flying Finns
Beer mile


== Notes and references ==

(Japanese) Track and Field Magazine (Rikujou kyougi magazine); contact Baseball Magazine Company, Chiyoda-ku Tokyo 102-0073; sportsclick.jp/track.
Milesplit. US, as of 2008, represents results primarily from the eastern states of USA.
Athletic.net, as of 2008, represents results from all states of USA, but especially the western states.
The centralized collection of high school and especially middle school data in the USA is relatively new, and there is more 2008 data than 2007 data. For both high school and middle school, many good performances may not have been reported to the various agencies.
Middle school often implies the students are one year younger than junior high. Japanese junior high corresponds with USA 7th, 8th and 9th grades.