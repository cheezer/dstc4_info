Moni Aizik is an Israeli-born martial artist. Aizik has a jujutsu and judo background and is the founder of Commando Krav Maga. One of Aizik's advertisements has been disallowed by the UK's Advertising Standards Authority due to being unable to provide proof of some of his claims.


== Israel ==
At the age of eight, under Opa Schutte, Aizik started training in judo and jujutsu. He won national title in judo seven times.
Aizik states that he was asked, shortly after the Yom Kippur War, to improve upon the army's existing hand-to-hand combat system due to his knowledge of martial arts, including various Israeli fighting systems, jujitsu and judo. Aizik says that he collaborated with Imi Lichtenfeld in 1973–1974.
After his military service he trained at a Maccabi Tel Aviv club in Israel. One of Aizik's students was Yael Arad, an 1992 Olympic silver medalist in judo.


== North America ==
Moving to North America in the 1980s, Aizik started teaching CKM at the Jewish Community Center in Toronto. He also established the "Samurai Club" in Canada.
Former UFC Welterweight Champion Carlos Newton was one of his students in Canada for a short time.


== United Kingdom ==
On October 28, 2008, the British Advertising Standards Authority stated that Aizik can not use "Ex-Israeli Special Forces Commando" or "Counter Terrorism Expert" in his advertising there since those claims can not be substantiated. Aizik says such documentation is classified.


== Family ==
His son Tal "Fly" Aizik (טל אייזיק) is an Israeli professional Dota 2 player who is currently playing for compLexity Gaming.


== References ==


== External links ==
Moni Aizik official page on Facebook