Petit Moni (プッチモニ), also known as Pucchi Moni, was the second official subgroup of the Japanese pop idol group Morning Musume.
Since 2009, Petitmoni has been revived as Petitmoni V, their latest performance being in 2013, however, it is unknown if Petitmoni V will have further activity following Erina Mano's graduation in 2013.


== History ==
Founded November 1999, the group initially consisted of Morning Musume members Kei Yasuda, Sayaka Ichii, and Maki Goto. Shortly after their formation, the group released their first single, "Chokotto Love," which sold more than 1 million copies.
After the release of the first single, on May 21, 2000, Sayaka Ichii graduated from both Morning Musume and Petitmoni. In order to fill the third member's position, their producer Tsunku placed 4th generation member Hitomi Yoshizawa into the group, forming the second generation. Petitmoni went on to release three more singles, with their only album, Zenbu! Petitmoni, released in August 2002. The album was a collection of all of their singles and B-side tracks, along with three extra tracks.
Goto graduated from both Morning Musume and Petitmoni in 2002. Shortly thereafter, Yasuda's graduation was announced for 2003. This sparked yet another formation change, in which Ayaka Kimura (of Coconuts Musume) and 5th generation member Makoto Ogawa were added. Yoshizawa became the new leader, but the new lineup never released a single; only a song entitled "Wow Wow Wow" and a remake of "Chokotto Love," which were included on later Petit Best compilation CDs.
The group went on an indefinite hiatus in 2003, and was effectively disbanded in subsequent years with Ogawa leaving to study English abroad in 2006, Yoshizawa graduating from Morning Musume in 2007, and Kimura leaving Hello! Project and Up-Front Agency in 2008.
The 2009 Hello! Project album Chanpuru 1: Happy Marriage Song Cover Shū featured a track by Petitmoni V, initiating a revival of the disbanded group. Its new lineup consisted of Saki Nakajima, Mai Hagiwara, and Erina Mano, and the "V" in its name is for "victory." This revived group appeared for the first time at the Hello! Project Summer 2009 concert (minus Mai Hagiwara, who was ill and could not participate), singing a new song, "Pira! Otome no Negai" (later released on the compilation album Petit Best 10).


== Members ==


=== First generation ===
Kei Yasuda
Sayaka Ichii
Maki Goto


=== Second generation ===
Kei Yasuda (Leader)
Maki Goto
Hitomi Yoshizawa


=== Third generation ===
Hitomi Yoshizawa (Leader)
Makoto Ogawa
Ayaka Kimura (from Coconuts Musume)


=== Revival (PetitmoniV) ===
Saki Nakajima (from Cute)(leader)
Mai Hagiwara (from Cute)
Erina Mano (Hello! Project soloist)(Graduated Hello! Project February 23, 2013)


== Discography ==


=== Singles ===


=== Albums ===


=== Compilation tracks ===
Together! - "Chokotto Love (2001 Version)"
Petit Best 3 - "Chokotto Love (2003 Version)"
Petit Best 4 - "Wow Wow Wow"
Chanpuru 1: Happy Marriage Song Cover Shū - "Kimi ga Iru Daki de"
Petit Best 10 - "Pira! Otome no Negai"


== Live Only Songs ==
Chokotto LOVE (Petitmoni V)
BABY! Koi ni KNOCK OUT (Petitmoni V)
Uwaki na Honey Pie (Petitmoni V)
WOW WOW WOW (Petitmoni V)


== References ==


== External links ==
Petitmoni discography on the Up-Front Works website (Japanese)
Kpopwiki:Petitmoni (English)