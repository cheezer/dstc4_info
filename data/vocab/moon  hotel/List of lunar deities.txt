In mythology, a lunar deity is a god or goddess associated with or symbolizing the moon. These deities can have a variety of functions and traditions depending upon the culture, but they are often related to or an enemy of the solar deity. Even though they may be related, they are distinct from the solar deity. Lunar deities can be either male or female, but are usually held to be the opposite sex of the corresponding solar deity.


== Moon in religion and mythology ==
The monthly cycle of the moon, in contrast to the annual cycle of the sun's path, has been implicitly linked to women's menstrual cycles by many cultures, as evident in the links between the words for menstruation and for moon in many resultant languages. Many of the most well-known mythologies feature female lunar deities, such as the Greek goddesses Phoebe, Artemis, Selene, and Hecate as well as the Chinese goddess Chang'e.
Male lunar gods are also frequent, such as Sin of the Mesopotamians, Mani of the Germanic tribes, the Japanese god Tsukuyomi. These cultures usually featured female Sun goddesses. There are also many lunar deities that were prevalent in Greek and Egyptian civilizations. For example, Ibis and Chonsu of Thebes were both lunar deities. Thoth was also a lunar deity, but his character is considerably more complex than Ibis and Chonsu.
In fact, the original Proto-Indo-European lunar deity appears to have been a male god. In subsequent traditions, the number of male moon deities (or words for "moon" with a male gender) seem to vastly outnumber female ones, which appear to be an exclusively eastern Mediterranean invention. Several goddesses, like Hecate or Artemis, did not originally have lunar aspects, and only acquired them late in antiquity, due to syncretism with Selene/Luna, the de facto Greco-Latin lunar deity. In traditions with male gods, there is little evidence of such syncretism, though the Greco-Roman Hermes has been equated with male Egyptian lunar gods like Thoth. In Greece proper, remnants of male moon gods are also seen with Menelaus.
Also of significance is that many religions and societies are oriented chronologically by the Moon as opposed to the sun. One common example is Hinduism in which the word Chandra means Moon and has religious significance during many Hindu festivals (e.g. Karwa Chauth, Sankasht Chaturthi and during the eclipses).
The moon is also worshipped in witchcraft, both in its modern form and in Medieval times, for example, in the cult of Madonna Oriente.
While many Neopagan authors and feminist scholars claim that there was an original Great Goddess in prehistoric cultures that was linked to the moon and formed the basis of later religions, the Great Goddess figure is highly speculative and not a proven concept. It may be noted that most of the oldest civilizations mentioned above had male lunar deities and it was only later cultures, the classical ones most people are familiar, that featured strong female moon goddesses.
The moon features prominently in art and literature and also the purported influence of the moon in human affairs remains a feature of astrology and theology.


=== North and South America ===
Goddess Menily (Cahuilla mythology)
Goddess Huitaca (Chibcha mythology)
Goddess Chia (Colombian mythology)
God Coniraya (Incan mythology)
Goddess Mama Quilla (Incan mythology)
Goddess Ka-Ata-Killa (Incan mythology)
God Alignak (Inuit mythology)
God Igaluk (Inuit mythology)
God Tarqiup Inua (Inuit mythology)
Goddess Yoolgai Asdza´a´ (Diné Bahane'/Navajo)
Goddess Pah (Pawnee mythology)
Goddess Jaci (Tupi mythology)
Ari (Tupi mythology)
God Abaangui (Guarani mythology)


=== Mesoamerica ===
Goddess Coyolxauhqui (Aztec mythology)
Goddess Metztli (Aztec mythology)
God Tecciztecatl see Metztli (Aztec mythology)
Goddess and God Awilix (K'iche' Maya mythology)
God Ixbalanque (Maya mythology)
Maya moon goddess (Maya mythology)


=== Ancient Near East ===
God Men (Phrygian mythology)
God Ta'lab (Arabian mythology)
God Wadd (Arabian mythology)
Goddess Nikkal (Canaanite mythology)
God Yarikh (Canaanite mythology)
God Napir (Elamite mythology)
God Baal-hamon
God Kaskuh (Hittite mythology)
God Kusuh (Hurrian mythology)
God Sin (Mesopotamian mythology)
God Aglibol (Palmarene mythology)
Goddess Selardi (Urartian mythology)


=== Europe ===
Goddess Ilargi (Basque mythology)
Goddess Artume (Etruscan mythology)
Goddess Losna (Etruscan mythology)
Goddess Kuu (Finnish mythology)
Goddess Achelois (Greek mythology)
Goddess Phoebe (Greek mythology)
Goddess Artemis / Diana (Greco-Roman mythology)
Goddess Selene / Luna (Greco-Roman mythology)
Goddess Hecate / Trivia (Greco-Roman mythology)
God Elatha (Irish Mythology)
God Meness (Latvian mythology)
Goddess Ataegina (Lusitanian mythology)
God Mani (Norse mythology)
Goddess Mano (Sami mythology)
God Jarilo (Slavic mythology)
Goddess Kazza (Arturian mythology)
Goddess Bendis (Thracian mythology)
Goddess Arianrhod (Welsh mythology)


=== Asia ===
Goddess Chup Kamui (Ainu mythology)
Goddess Chang'e (Chinese mythology)
Goddess Su'e (Chinese mythology)
Goddess Han Ying (Chinese mythology)
Goddess Queen Jiang (Chinese mythology)
Goddess Changxi (Chinese mythology)
God Jie Lin (Chinese mythology)
God Tsukuyomi (Japanese mythology)
Goddess Anumati (Hindu mythology)
God Chandra or 'Indu' (Hindu mythology)
Goddess Ratih (Indonesian mythology)
Goddess Silewe Nazarate (Indonesian mythology)
Goddess Mayari or 'Bulan' (Philippine mythology)
Goddess Dae-Soon (Korean mythology)
Goddess Neang Vimean Chan (Cambodian mythology)
God Ay Ata (Turkic mythology)


=== Africa ===
Goddess Gleti (Dahomean mythology)
God Khonsu (Egyptian mythology)
God Iah (Egyptian mythology)
God Thoth (Egyptian mythology)
God Arebati (Mbuti mythology)
God Kalfu (Vodun mythology)
Goddess Yemaya (Yoruba mythology)


=== Oceania ===
God Kidili (Mandjindja mythology)
Papare (Orokolo mythology)
God Avatea (Polynesian mythology)
God Fati (Polynesian mythology)
Goddess Hina (Polynesian mythology)
Goddess Lona (Hawaiian mythology)
Goddess Mahina (Polynesian mythology)
God Marama (Polynesian mythology)
God Ngalindi (Yolngu mythology)


== See also ==
Great Goddess
Man in the Moon
Moon idol
Moon rabbit
Nature worship
Solar deity
The White Goddess
Triple Goddess


== References ==
^ Harding, Esther M., 'Woman's Mysteries: Ancient and Modern', London: Rider, 1971, p. 24.
^ Thoth, the Hermes of Egypt: a study of some aspects of theological thought in ancient Egypt, page 75
^ Dexter, Miriam Robbins. Proto-Indo-European Sun Maidens and Gods of the Moon. Mankind Quarterly 25:1 & 2 (Fall/Winter, 1984), pp. 137–144.
^ Walker, Barbara G., The Woman's Encyclopedia of Myths and Secrets, San Francisco: Harper, 1983, p. 669.