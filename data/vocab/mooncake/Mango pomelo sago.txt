Mango pomelo sago is a Hong Kong dessert. That said to be invented in 1984. It is composed of mango, pomelo, sago, coconut milk, cream and sugar. 
Mango pomelo sago is not only a dessert or drink, it is also a flavor for cake, ice-cream, ice pop and mooncake.


== Gallery ==


== See also ==
Mango pudding


== References ==