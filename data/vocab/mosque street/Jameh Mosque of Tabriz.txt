This article is about Jameh Mosque of Tabriz; for similar uses, see Jameh Mosque (disambiguation).
The Jāmeh Mosque (Persian: مسجد جامع تبریز‎ - Masjid-e-Jāmeh Tabrīz) is a large, congregational mosque (Jāmeh) in Tabrīz city, within the East Azerbaijan Province of Iran. It is located in the Bazaar suburb of Tabriz next to the Grand Bazaar of Tabriz and the Constitutional House of Tabriz.


== Photo gallery ==


== See also ==
 Media related to Masjed-e Jomeh, Tabriz at Wikimedia Commons
Blue Mosque, Tabriz
Saheb ol Amr Mosque


== References ==
Editorial Board, East Azarbaijan Geography, Iranian Ministry of Education, 2000
http://www.eachto.ir