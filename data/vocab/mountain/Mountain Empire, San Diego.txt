The Mountain Empire is a rural area in southeastern San Diego County, California. The Mountain Empire subregion consists of the backcountry communities in southeastern San Diego County. The area is also sometimes considered part of the East County region of San Diego County.


== Geography ==
The Mountain Empire occupies the largely hilly, rugged terrain of the Laguna Mountains and foothills between Interstate 8 and the U.S.-Mexico border east of Otay Mountain and west of Imperial County. The Pacific Crest Trail has its southern terminus in Mountain Empire, along the international border just south of the town of Campo. Portions of the Mountain Empire are located in the Descanso Ranger District of the Cleveland National Forest.
California State Route 94 and Interstate 8 are the primary highways through the region. Historic U.S. Route 80 also passes through the Mountain Empire, as does the San Diego and Arizona Eastern Railway.


== Communities ==
Boulevard
Buckman Springs
Campo
Descanso
Guatay
Jacumba
Lake Morena Village
Pine Valley
Potrero
Tecate
Tierra del Sol


== Education ==
The region is served by the Mountain Empire Unified School District, which consists of six elementary schools and Mountain Empire High School. The Mountain Empire Unified School District is geographically the largest school district in California, occupying over 600 square miles (1,600 km2).


== Media ==
The Mountain Empire is served by countywide publications such as the San Diego Union-Tribune and the San Diego Reader. Regionally, it is served by East County Magazine, and locally by the Back Country Messenger.


== References ==


== External links ==
Mountain Empire Subregional Plan
Back Country Messenger website
East County Magazine website