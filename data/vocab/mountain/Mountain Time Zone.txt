The Mountain Time Zone of North America keeps time by subtracting seven hours from Greenwich Mean Time, during the shortest days of autumn and winter (UTC−7), and by subtracting six hours during daylight saving time in the spring, summer, and early autumn (UTC−6). The clock time in this zone is based on the mean solar time at the 105th meridian west of the Greenwich Observatory. In the United States, the exact specification for the location of time zones and the dividing lines between zones is set forth in the Code of Federal Regulations at 49 CFR 71.
In the United States and Canada, this time zone is generically called Mountain Time (MT). Specifically, it is Mountain Standard Time (MST) when observing standard time (fall and winter), and Mountain Daylight Time (MDT) when observing daylight saving time (spring and summer). The term refers to the fact that the Rocky Mountains, which range from northwestern Canada to the US state of New Mexico, are located almost entirely in the time zone. In Mexico, this time zone is known as the Pacific Zone.
In the United States and Canada, the Mountain Time Zone is one hour ahead of the Pacific Time Zone and one hour behind the Central Time Zone.
In some areas, starting in 2007, the local time changes from MST to MDT at 2 am MST to 3 am MDT on the second Sunday in March and returns at 2 am MDT to 1 am MST on the first Sunday in November.
Sonora in Mexico and most of Arizona in the United States do not observe daylight saving time, and during the spring, summer, and autumn months they are on the same time as Pacific Daylight Time. The Navajo Nation, most of which lies within Arizona, does observe DST, although the Hopi Nation, as well as some Arizona state offices lying within the Navajo Nation, do not.
The largest city in the Mountain Time Zone is Ciudad Juárez, Chihuahua. The Phoenix metropolitan area is the largest metropolitan area in the zone; the largest metropolitan area that observes Mountain Daylight Time is the binational El Paso–Juárez area, closely followed by Denver, Colorado. TV broadcasting in the Mountain Time Zone is typically tape-delayed one hour, so that shows match the broadcast times of the Central Time Zone (i.e. prime time begins at 7 pm MT following the same order of programming as the Central Time Zone).


== Canada ==

The following provinces and areas are part of the Mountain Time Zone:

Alberta
the Northwest Territories (except for Tungsten)
most of the Kitikmeot Region of Nunavut
portions of southeastern and northeastern British Columbia *however, these areas stay on Mountain Standard Time year-round, thus are on the same time as Pacific time zone areas for the majority of each year
the area of Saskatchewan immediately surrounding the city of Lloydminster


== Mexico ==

The following states have the same time as Mountain Time Zone:

Baja California Sur
Chihuahua
Nayarit: Except from the Bahía de Banderas municipality which uses the Central Time Zone.
Sonora – no daylight saving time, always on MST.
Sinaloa
Revillagigedo Islands (Colima): three of the four islands have the same time as Mountain Time Zone, Isla Socorro, San Benedicto Island and Roca Partida.


== United States ==

The following states or areas are part of the Mountain Time Zone:

Arizona – no daylight saving time, always on MST (winter time), except on the Navajo Nation.
Colorado
Idaho – southern half, south of the Salmon River
Kansas – only the counties of Sherman, Wallace, Greeley and Hamilton, all of which border Colorado. The remaining three counties that border Colorado, Cheyenne, Morton and Stanton, observe Central Time, as do all other Kansas counties.
Montana
Nebraska – western third
Nevada – West Wendover, on the Utah border, is the only location in the state which legally observes Mountain Time. However, the towns of Jackpot, Jarbidge, Mountain City and Owyhee, while all legally within the Pacific Time Zone, locally observe the Mountain Time Zone due to proximity to and stronger connections with towns in nearby Idaho.
New Mexico
North Dakota – southwestern quadrant, southwest of the (Missouri River)
Oregon – most of Malheur County, on the Idaho border
South Dakota – western half
Texas – the two westernmost counties (Hudspeth, El Paso)
Utah
Wyoming

Also, the unincorporated community of Kenton, Oklahoma, located in the extreme western end of the Oklahoma Panhandle, unofficially observes Mountain Time (as the nearest sizable towns are located in Colorado and New Mexico, both of which are in the Mountain Time Zone). However, the entire state of Oklahoma is officially in the Central Time Zone. Additionally, western Culberson County, Texas unofficially observes Mountain Time.


== See also ==
Effects of time zones on North American broadcasting


== Notes ==


== References ==

World time zone map
U.S. time zone map
History of U.S. time zones and UTC conversion
Canada time zone map
Time zones for major world cities
The official U.S. time for the Mountain Time Zone (except Arizona)
The official U.S. time for the Mountain Time Zone (Arizona)
Official times across Canada
Official times across Mexico