Patricia Edwina Victoria Knatchbull, 2nd Countess Mountbatten of Burma CBE MSC CD JP DL (born 14 February 1924) is a British peer and the third cousin of Queen Elizabeth II. She is the elder daughter of Louis Mountbatten, 1st Earl Mountbatten of Burma and his wife, the heiress Edwina Ashley, a patrilineal descendant of the Earls of Shaftesbury, first ennobled in 1661. She is the elder sister of Lady Pamela Hicks, and first cousin to Prince Philip, Duke of Edinburgh.
Mountbatten succeeded her father when he was assassinated in 1979, as his peerages had been created by the Crown with special remainder to his daughters and their heirs male. This inheritance accorded her the title of countess and a seat in the House of Lords, where she remained until 1999, when the House of Lords Act 1999 removed most hereditary peers from the House.


== Marriage and children ==
On 26 October 1946 she married John Knatchbull, 7th Baron Brabourne (9 November 1924 – 23 September 2005), at the time an aide to her father in the Far East. The wedding took place at Romsey Abbey in the presence of members of the Royal Family. Her bridesmaids were Princess Elizabeth, Princess Margaret, Lady Pamela Mountbatten, the bride's younger sister, and Princess Alexandra, daughter of the Duke and Duchess of Kent.
Later they became one of the few married couples each of whom held a peerage in their own right, and whose descendants are slated to inherit titles through both. They had eight children:
Norton Louis Philip Knatchbull, 8th Baron Brabourne (born 8 October 1947), married Penelope Eastwood (born 16 April 1953) and have three children.
The Hon. Michael-John Ulick Knatchbull (born 24 May 1950), married Melissa Clare Owen (born 12 November 1960) on 1 June 1985 and had one daughter Kelly, who is a god-daughter of Princess Anne, The Princess Royal; they divorced in 1997. Married Susan Penelope Jane Coates (born 23 October 1959) on 6 March 1999 and had one daughter Savannah. Divorced 13 February 2006
The Hon. Anthony Knatchbull (born and died 6 April 1952)
Lady Joanna Edwina Doreen Knatchbull (born 5 March 1955), married Baron Hubert Pernot du Breuil (2 February 1956 – 6 September 2004) on 3 November 1984 and had one daughter; divorced in 1995; married Azriel Zuckerman (born 18 January 1943) on 19 November 1995 and had one son.
Lady Amanda Patricia Victoria Knatchbull (born 26 June 1957), married Charles Vincent Ellingworth (born 7 February 1957) on 31 October 1987 and has three sons.
The Hon. Philip Wyndham Ashley Knatchbull (born 2 December 1961), married Atalanta Cowan, daughter of John Cowan, (born 20 June 1962) on 16 March 1991 and had one daughter; married Wendy Amanda Leach (born 20 July 1966) on 29 June 2002 and had two sons.
The Hon. Nicholas Timothy Charles Knatchbull (18 November 1964 – 27 August 1979), murdered, aged 14, by an IRA bomb.
The Hon. Timothy Nicholas Sean Knatchbull (born 18 November 1964), married Isabella Julia Norman (born 9 January 1971), a great-great-granddaughter of the 4th Earl of Bradford, on 11 July 1998 and had two sons and three daughters.


== Activities ==
Mountbatten was educated in Malta, England, and New York. In 1943, at age 19, she entered the Women's Royal Naval Service as a Signal Rating and served in Combined Operations bases in Britain until being commissioned as a third officer in 1945 and serving in the Supreme Allied Headquarters, South East Asia. This is where she met Lord Brabourne, who was an aide to her father. In 1973 was appointed Deputy Lieutenant for the County of Kent; she is also a serving magistrate and is involved with numerous service organisations including SOS Children's Villages UK, of which she is Patron; the Order of St John, of which she is a Dame; and the Countess Mountbatten's Own Legion of Frontiersmen of the Commonwealth, of which she is Patron.
On 15 June 1974, she succeeded her cousin Lady Patricia Ramsay, formerly HRH Princess Patricia of Connaught, as Colonel-in-Chief of Princess Patricia's Canadian Light Infantry, for whom the regiment was named when Princess Patricia's father, the Duke of Connaught, was Governor General of Canada during the First World War. Despite her succeeding to an earldom in her own right as Countess Mountbatten of Burma on the death of her father in 1979, she preferred that the officers and men of her regiment address her as Lady Patricia. She was succeeded by The Right Honourable Adrienne Clarkson on 17 March 2007. On 28 August 2007, the Governor General of Canada presented her with the Canadian Meritorious Service Cross for her services as Colonel-in-Chief of Princess Patricia's Light Infantry.
Brabourne was in the boat which was blown up by the IRA off the shores of Sligo in August 1979, killing her fourteen-year-old son Nicholas; her father; her mother-in-law, the Dowager Baroness Brabourne; and fifteen-year-old Paul Maxwell, a boat-boy from County Fermanagh. She, her husband, and their son Timothy were injured but survived the attack. Following this loss the Countess became Patron and later, President of The Compassionate Friends, a self-help charitable organisation of bereaved parents in the UK.
In June 2012, at the time of Queen Elizabeth II's first visit to the Republic of Ireland, Mountbatten said the Queen had her full support for meeting Mr. McGuinness, a former IRA commander who was allegedly part of the terrorist group at the time of Lord Mountbatten's murder. "I think it's wonderful," she said. "I'm hugely grateful that we have come to a point where we can behave responsibly and positively." In September 2012 Mountbatten unveiled a memorial to the work of the Combined Operations Pilotage Parties at Hayling Island in Hampshire.


== Daughter's involvement with Prince Charles ==
As Lady Brabourne during her father's lifetime, her immediate family became closely involved in the consideration of a future consort for her first cousin once-removed, Charles, Prince of Wales. In early 1974, Lord Mountbatten began corresponding with the eldest son of Queen Elizabeth II and Prince Philip about a potential marriage to Lady Brabourne's daughter, Amanda. Charles wrote to Lady Brabourne (who was also his godmother), about his interest in her daughter, to which she replied approvingly, though suggesting that a courtship was premature. Amanda Knatchbull declined the marriage proposal of Charles in 1980, following the assassination of her grandfather.


== Portrait ==
In October 2009 TCF Canada Inc. presented the Countess with a portrait of herself by the noted Canadian artist Christian Cardell Corbet of which the oil sketch resides in the Canadian Portrait Academy Permanent Collection.


== Titles and honours ==


=== Titles and styles ===
14 February 1924 – 23 August 1946: Miss Patricia Mountbatten
23 August – 26 October 1946: The Honourable Patricia Mountbatten
26 October 1946 – 27 August 1979: The Right Honourable The Lady Brabourne
27 August 1979 – present: The Right Honourable The Countess Mountbatten of Burma
Mountbatten was born the daughter of a younger son of a marquess (1st Marquess of Milford Haven, formerly Prince Louis Alexander of Battenberg) and thus had no courtesy title. She became the daughter of a peer (when her father was created a viscount), and thus obtained the courtesy prefix Honourable. When she married a baron, she obtained her husband's precedence, which happened to be higher than that of a viscount's daughter. When her father was raised to an earldom, however, her precedence remained the same, because the higher courtesy rank of an earl's daughter cannot be claimed by the wife of a man who ranks as a peer in his own right. When her father died and she succeeded him as countess by special remainder, Patricia Mountbatten became a peeress in her own right. Since her peerage was higher than her husband's, she was entitled to enjoy its higher title and precedence. By contrast, her younger sister's rank as an earl's daughter outranked her husband's status as a commoner from August 1946 to August 1979 because when a peer's daughter marries a commoner rather than a peer, she is allowed to retain the rank derived from her parent.


=== Honours ===
Dame of the Order of St. John of Jerusalem (DStJ)
Commander of the Order of the British Empire (CBE)
Meritorious Service Cross (MSC)
Canadian Forces Decoration (CD)


=== Colonelcies-in-chief ===
The Loyal Edmonton Regiment (4th Battalion, Princess Patricia's Canadian Light Infantry)
Countess Mountbatten's Own Legion of Frontiersmen of the Commonwealth
Princess Patricia's Canadian Light Infantry (formerly, now The Right Honourable Adrienne Clarkson)


== Ancestry ==


== Bibliography ==
Dimbleby, Jonathan (1994). The Prince of Wales: A Biography. New York: William Morrow and Company. 


== References ==


== External links ==
Hansard 1803–2005: contributions in Parliament by the Countess Mountbatten of Burma
Regiments.org on Princess Patricia's Canadian Light Infantry
mountbattenofburma.com - Tribute & Memorial web-site to Louis, 1st Earl Mountbatten of Burma
About Us - Patrons of The Compassionate Friends