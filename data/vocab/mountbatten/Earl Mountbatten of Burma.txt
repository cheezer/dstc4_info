Earl Mountbatten of Burma is a title in the Peerage of the United Kingdom. It was created in 1947 for Rear Admiral Louis Mountbatten, 1st Viscount Mountbatten of Burma, the last Viceroy of India. The letters patent creating the title specified the following special remainder,

...to his eldest daughter Patricia Edwina Victoria, Baroness Brabourne...and the heirs male of her body lawfully begotten; and in default of such issue to every other daughter lawfully begotten of the said Louis Francis Albert Victor Nicholas, Viscount Mountbatten of Burma, successively in order of seniority of age and priority of birth and to the heirs male of their bodies lawfully begotten...

As a result, Lord Mountbatten's eldest daughter Patricia succeeded as the Countess Mountbatten of Burma upon the former's death. Should the legitimate male line of descent of the 2nd Countess Mountbatten of Burma become extinct, the peerages will be inherited by her sister, Lady Pamela Hicks, and her legitimate heirs male. Should the legitimate male line of both sisters become extinct, the peerages will become extinct.
The subsidiary titles of the Earldom are: Viscount Mountbatten of Burma, of Romsey in the County of Southampton (created 1946), and Baron Romsey, of Romsey in the County of Southampton (1947). Both of these titles, in the Peerage of the United Kingdom, have the same special remainder as the Earldom. Lord Romsey was the courtesy title by which Lady Mountbatten of Burma's eldest son and heir was known until he succeeded his father as 8th Lord Brabourne.
The family seat is Newhouse Manor, near Ashford, Kent.


== Earls Mountbatten of Burma (1947) ==
The heir apparent is the present holder's eldest son Norton Louis Philip Knatchbull, 8th Baron Brabourne (b. 1947).


== Line of succession ==
Norton Knatchbull, 8th Baron Brabourne (b. 1947), eldest son the 2nd Countess (previously known as Lord Romsey from the time of his mother's succession to the title until his succession to his father's title)
The Hon. Nicholas Louis Charles Norton Knatchbull (b. 1981), only son of the 8th Baron Brabourne
The Hon. Michael John Ulick Knatchbull (b. 1950), second son of the 2nd Countess
The Hon. Philip Wyndham Ashley Knatchbull (b. 1961), fourth son of the 2nd Countess
Frederick Michael Hubert Knatchbull (b. 2003), elder son of the Hon. Philip Wyndham Ashley Knatchbull
John Robin Rocky Knatchbull (b. 2004), younger son of the Hon. Philip Wyndham Ashley Knatchbull
The Hon. Timothy Nicholas Sean Knatchbull (b. 1964), fifth son of the 2nd Countess Mountbatten of Burma
Milo Columbus John Knatchbull (b. 2001), elder son of The Hon. Timothy Nicholas Sean Knatchbull
Ludovic David Nicholas Knatchbull (b. 2003), second son of The Hon. Timothy Nicholas Sean Knatchbull
Lady Pamela Carmen Louise Hicks (b. 1929), younger daughter of the 1st Earl Mountbatten of Burma
Ashley Louis David Hicks (b. 1963), only son of Lady Pamela Hicks


== References ==


== Sources ==
mountbattenofburma.com – Tribute & Memorial web-site to Louis, 1st Earl Mountbatten of Burma
Mountbatten Medal