Mountbatten, Singapore is an estate within Kallang near to Marine Parade. Its total area size is 161 hectares.
The estate is named after Lord Louis Mountbatten, the Supreme Allied Commander of the South East Asia Command, Governor General of India and British Military Administrator of Malaya from 1945 to 1946.
The area is served by Mountbatten MRT station on the Circle Line.
Mountbatten Road is a major thoroughfare stretches all the way from Guillemard Road (where Kallang Airport Way merges with Sims Way) in Kallang to Haig Road in Katong area (becoming E. Coast Road).


== See also ==
Mountbatten Centre Singapore - private school found along Mountbatten Road.
Mountbatten Square - a commercial building found along Mountbatten Road.


== External links ==
Infopedia website