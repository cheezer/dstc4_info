Spider-Man is a Marvel Comics superhero.
Spider-Man or Spiderman may also refer to:

.


== Entertainment ==
Spider-Man (comic book), former name of the comic book Peter Parker: Spider-Man
Spider-Man (Miles Morales), after Peter Parker of the Ultimate Universe is killed the mantle passed to Miles Morales
Spider-Man: Turn Off the Dark, the Broadway musical with music by Bono and The Edge
Spider-Man, one of several alternative versions of Spider-Man


=== Film ===
Spider-Man (1977 film), the pilot for the 1970s live action series
Spider-Man (2002 film), the first film in the series directed by Sam Raimi and starring Tobey Maguire
Spider-Man (soundtrack), the soundtrack album for the 2002 film

Spider-Man 2, the second film in the Sam Raimi series
Spider-Man 3, the third film in the Sam Raimi series


=== Television ===
Spider-Man (1967 TV series)
"Spider-Man" (theme song)

Spider-Man (Toei), a Japanese live action series
Spider-Man (1981 TV series)
Spider-Man (1994 TV series), also known as Spider-Man: The Animated Series
Spider-Man Unlimited, a 1999 television series
Spider-Man: The New Animated Series, the 2003 television series also known as MTV Spider-Man


=== Video games ===
Spider-Man: The Video Game, a 1991 arcade title from Sega
Spider-Man (2000 video game), a title released by Activision
Spider-Man (2002 video game), an Activision title based on the movie
Spider-Man: Shattered Dimensions, a Spider-Verse game


=== Music ===
Spiderman, a creature described in The Cure's "Lullaby" song lyrics
Spider Man (album), a 1965 album by jazz vibraphonist Freddie McCoy


== People ==
Spider-Man (nickname), a list of people


== See also ==
The Amazing Spider-Man (disambiguation)
"Spydermann", a single by Another Bad Creation
The Spider Man, a novel by Filipino author F. Sionil José