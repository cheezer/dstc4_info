The 100 Scariest Movie Moments is a television documentary miniseries that first aired in late October 2004 on Bravo. Aired in five 60-minute segments, the miniseries counts down what producer Anthony Timpone, writer Patrick Moses, and director Kevin Kaufman have determined as the 100 most frightening and disturbing moments in the history of movies. Each segment includes interviews from horror genre experts and other celebrities who experienced the listed films, as well as film clips and movie stills from the films covered. This 2004 offering was followed up in 2006 by the two-part sequel, 30 Even Scarier Movie Moments, as well as another countdown, 13 Scarier Movie Moments.


== Summary ==
The countdown included 100 films, mainly from the 1970s to 2000s, although films from the 1960s and earlier are also featured. While the list mainly comprises horror and thriller films, science fiction films such as The Terminator and Jurassic Park are included. Even children's films such as The Wizard of Oz and Willy Wonka & the Chocolate Factory made their way onto the countdown.
The top five films included on the list were predominantly from the 1960s and 1970s. The Texas Chain Saw Massacre ranked fifth for the scene in which Leatherface bashes Kirk's skull in with a sledgehammer. Psycho ranked number four for the death scene of the private investigator Milton Arbogast. The Exorcist ranked third for the scene where the possessed Regan MacNeil's head spins clockwise during the exorcism, Alien ranked second for the chestburster sequence, and Jaws was placed at the number one spot for the opening scene in which the unseen shark devours Chrissie Watkins during a midnight swim.


== Sequels ==
Produced by Sharp Entertainment, 30 Even Scarier Movie Moments, a two-part sequel to this title, aired on Bravo and Chiller in October 2006. This was followed by Sharp Entertainment's 13 Scarier Movie Moments which aired in October 2009.


== References ==


== External links ==
The 100 Scariest Movie Moments at the Internet Movie Database
100 Scariest Movie Moments (via Internet Archive)