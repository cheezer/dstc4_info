The Manila Mass Rapid Transit Line 7 (MRT-7) is a proposed rapid transit line in Metro Manila. If completed, the line could be 23 km long with 14 stations. The line has been projected running in a northeast direction, traversing Quezon City and a part of Caloocan City in Metro Manila before ending at the City of San Jose del Monte in Bulacan province.
Under the proposal, the project will have a combined 45-km of road and rail transportation project from the Bocaue exit of the North Luzon Expressway (NLEX) to the intersection of North Avenue and EDSA. The 22-km, 6-lane asphalt road will connect the NLEX to the major transportation hub development in San Jose del Monte.


== History ==
Universal MRT Corporation, composed of a consortium of the Tranzen Group, EEI Corporation and SM Prime Holdings and led by former Finance Secretary Roberto de Ocampo submitted an unsolicited proposal to the Philippine Department of Transportation and Communications in 2002. In June 2007, DOTC presented a Swiss Challenge in which four business firms submitted their counter proposal. In January 2008, DOTC announced that the ULC proposal emerged as winner and the contract was signed. The San Miguel Corporation owns a majority stake in Universal LRT Corporation (ULC).
In May 2009, The Investment Coordination Committee (ICC) of the National Economic and Development Authority (NEDA) approved the MRT-7 project. Construction of MRT-7 should have commenced in January 2010, but has been postponed several times since then.
The proposed stations (listed south to north) are:


== See also ==


== References ==