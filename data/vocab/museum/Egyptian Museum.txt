The Museum of Egyptian Antiquities, known commonly as the Egyptian Museum or Museum of Cairo, in Cairo, Egypt, is home to an extensive collection of ancient Egyptian antiquities. It has 120,000 items, with a representative amount on display, the remainder in storerooms. As of April 2015, it is open to the public.


== History ==

The Egyptian Museum of Antiquities contains many important pieces of ancient Egyptian history. It houses the world’s largest collection of Pharaonic antiquities. The Egyptian government established the museum, built in 1835 near the Ezbekeyah Garden and later moved to the Cairo Citadel. In 1855 Archduke Maximilian of Austria was given all of the artifacts by the Egyptian government; these are now in the Kunsthistorisches Museum, Vienna.
A new museum was established at Boulaq in 1858 in a former warehouse, following the foundation of the new Antiquities Department under the direction of Auguste Mariette. The building lay on the bank of the Nile River, and in 1878 it suffered significant damage in a flood of the Nile River. In 1892, the collections were moved to a former royal palace, in the Giza district of Cairo. They remained there until 1902 when they were moved, for the last time, to the current museum in Tahrir Square.
During the Egyptian Revolution of 2011, the museum was broken into, and two mummies were reportedly destroyed. Several artifacts were also shown to have been damaged. Around 50 objects were lost. Since then 25 objects have been found. Those that were restored were put on display in September 2013 in an exhibition entitled Damaged and Restored. Among the displayed artifacts are two statues of King Tutankhamen made of cedar wood and covered with gold, a statue of King Akhenaton, Ushabtis statues that belonged to the Nubian kings, a mummy of a child and a small plychrome glass vase.


== Interior design ==
There are two main floors in the museum, the ground floor and the first floor. On the ground floor there is an extensive collection of papyrus and coins used in the Ancient world. The numerous pieces of papyrus are generally small fragments, due to their decay over the past two millennia. Several languages are found on these pieces, including Greek, Latin, Arabic, and ancient Egyptian. The coins found on this floor are made of many different metals, including gold, silver, and bronze. The coins are not only Egyptian, but also Greek, Roman, and Islamic. This has helped historians research the history of Ancient Egyptian trade.
Also on the ground floor are artifacts from the New Kingdom, the time period between 1550 and 1069 BC. These artifacts are generally larger than items created in earlier centuries. Those items include statues, tables, and coffins (sarcophagi).
On the first floor there are artifacts from the final two dynasties of Egypt, including items from the tombs of the Pharaohs Thutmosis III, Thutmosis IV, Amenophis II, Hatshepsut, and the courtier Maiherpri, as well as many artifacts from the Valley of the Kings, in particular the material from the intact tombs of Tutankhamun and Psusennes I. Two special rooms contain a number of mummies of kings and other royal family members of the New Kingdom.


== Gallery ==


== See also ==
Egyptian Museum of Turin
Egyptian Museum of Berlin
Grand Egyptian Museum
List of museums with major collections of Egyptian antiquities
Coptic Museum


== References ==


== Further reading ==
Brier, Bob (1999). The Murder of Tutankhamen: A True Story. ISBN 0-425-16689-9.
Montet, Pierre (1968). Lives of the Pharaohs. World Publishing Company.
Wafaa El-Saddik. The Egyptian Museum. Museum International. (Vol. 57, No.1-2, 2005).
Egyptian Treasures from the Egyptian Museum in Cairo, Francesco Tiradritti, editor, Araldo De Luca, photographer. 1999, New York: Abrams ISBN 0-8109-3276-8. Also published, with variant titles, in Italy and the UK. Reviews US ed.


== External links ==
Egyptian Museum official website
Photographic archive of Art and Architecture
The Cairo Museum
Gallery of Items in the Egyptian Museum