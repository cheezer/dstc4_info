Magudam is a 1992 Tamil drama film written and directed by Prathap Pothan. It stars Sathyaraj, Bhanupriya and Gouthami in the lead roles.


== Plot ==
Muthuvelu(satyaraj) was brought up by his grandmother(manoramma). His grandmother won't allow him to talk with girls. One night he meets thilakavathi(gowtami) and he gets fear thinking that she is a ghost as she was wearing white dress. Then he understoods that she is a human. When she tries to drink poisson he stops it and ask her flashback. She says that she was a medical student and on a fine day her chief asked to find the correct reason for a lady's suicide. But she proved that it was a murder and that was done by her husband thillainathan who has a big background. For this thillainathan spoils thilaka's name and killed her mother and trying to kill her also. She wanted not to die on their hands.So she thought to commit suicide. By saying this she completes.Muthuvelu want to help her.Soon after they two fall in love. After some scenes he convinces his grandma and his grandmother says thilaka should continue her studies, for which velu will help.That time muthu was arrested by police for a wrong case framed by thillainathan. In that period he loses his land by thillainathan.When he returns from jail he thought of taking revenge on thillainathan. He forms a group and he becomes a local rowdy. There comes bhavani(bhanupriya),a mischief girl falls in love with velu and troubles him to marry her.Meanwhile thillainathan tries to kill muthu by shoting him, but bhavani saves him and got hurt as he shot her.At the hospital docter says that she is going to die and asked her brother to fulfil her last wish.She says that she want to die as muthu's wife and muthu marries bhavani. Unfortunately she was saved by thilaka and thilaka's heart was broken on knowing that muthu was bhavani's husband.She stops her marriage and his grandma also hates him without knowing the situation of his marriage and goes with thilaka. Thilaka maintains good friendship with bhavani and solves her family problems. Meanwhile thillainathan tries to kill muthu in many ways.Bhavani gives a birth to a child.Finally thillainathan kidnaps muthu's child,grandma,bhavani,and thilakavathi.Muthu and bhavani's brother comes to rescue them. In this process bhavani was shot by thillainathan,saying last words that muthu should marry thilaka and thilaka should be the mother of her baby. Then she pushes thillainathan from a rock and she also falls down.They both died. Finally muthu marries thilaka.


== Cast ==
Sathyaraj as muthuvelu
Bhanupriya as bhavani
Gouthami as thilakavathi
Manorama as muthuvelu's grandmother
Charanraj as bhavani's brother
Salim Ghouse as thilainathan
Goundamani as thanisami
Senthil as manasthan


== Production ==
Initially Kanaka was offered the role of bhavani and Sukanya was offered the role of thilaka.then it was replaced by Bhanupriya and Gouthami.


== Reception ==
The film performed well at the box office. A couple of songs became radio hits.


== External links ==
[1]