Vedha Muthu Mukandar (died 6 December 1855), (also known as Velu Muthu Mukandar), was the first Protestant Christian in the Megnanapuram Circle. He was a Hindu who converted to Christianity through the influence of missionary, the Rev. C. T. E. Rhenius. Vedha Muthu is buried at St Stephen’s Church, Jebagnanapuram, Solaikudiyiruppu.
Vedha Muthu Mukandar was born into the Nadar caste in Solaikudiriruppu, an oasis village in the Kudhremozhi Theri, part of the Thoothukudi district of Tamil Nadu state, and close to Korkai, the site of the capital of the ancient Pandyan Kingdom. He became a Mukandar of Megnanapuram village, through his apparent prophetic intuition, and wisdom in solving disputes, an ancient title originally bestowed by Arab rulers of the Vijayanagar Empire.


== §Conversion to Christianity ==

The conversion of Vedha Muthu has an historical perspective. Before British supremacy was established in the region it had been part of the Vijayanagara Empire, whose appointed Nayaks, (governors), had restricted the life of the members of Vedha Muthu's Nadar caste, regardless of status, and denied them access to the Thiruchendur Murugan Temple. Consequently, many Nadar's considered the arrival of the British as an opportunity to remove this discrimination, and had become receptive to Christianity.
In 1823, after performing Christian conversions at Sathankulam, the Rev. C T E Rhenius, (the first Church Mission Society missionary in India), visited Solaikudiyiruppu. Through the encouragement and preaching of Rhenius, Vedha Muthu converted and was baptised with the new name, Velu Muthu, becoming the village's first Protestant Christian. Rhenius converted many in Solaikudiyiruppu, and by 1825 had persuaded Vedha Muthu and his fellow converts to build a church. After the mysterious death of the Nadar Christians' leader Chinnamuth Sundaranandam David, Rev. Rhenius looked for another to share missionary work, and turned to Vedha Muthu to take the place of Sundaranandam David, whereupon Vedha Muthu joined an initiative to establish a Christian satellite village at Neduvillai. In 1830, Neduvillai's name was changed to Megnanapuram, meaning a "Place of True Wisdom".


== §Schism and reconciliation ==
When the Church Mission Society terminated the services of Rev Rhenius in 1835, Vedha Muthu and his supporters stopped attending its Church at Solaikudiyiruppu, and the now Holy Trinity Cathedral of the Diocese of Tirunelveli at Palayamkottai. Alternative Prayer Halls were established, one by Vedha Muthu close to his home at Jebagnanapuram, Solaikudiyiruppu. This schism continued after the death of Rev. Rhenius.
Vedha Muthu's Prayer Hall was taken under the Tinnevelly Diocese by D. Navamani Gnanayutham, one of Vedha Muthu's great-grandsons, following a call for unification by Bishop V S Azariah, the First Indian Anglican Bishop. It was dedicated on 2 November 1940 by Anglican Bishop the Rt. Rev. Stephen Neill as St Stephen’s Church, Jebagnanapuram, Solaikudiyiruppu. Currently the church is part of the M. Santhapuram Pastorate (Megnanapuram Circle) of the C.S.I. Nazareth-Thootukudi Diocese.


== §The tomb of Vedha Muthu Mukandar ==

Vedha Muthu married Thiraviyam Nadathi; they are buried together at St Stephen’s Church, Jebagnanapuram, Solaikudiyiruppu.
Tamil inscriptions state that Vedha Muthu died on the twenty first day of the Tamil month Karthigai in the year 1031 (Gregorian date 6 December 1855), and Thiraviyam Nadathi on the twelfth day of the Tamil month Avani in the year 1021 (Gregorian date 27 or 28 August 1845).
The congregation of St Stephen’s Church and the Christian Megnananapuram Circle considers Vedha Muthu Mukandar and Thiraviya Muthu Nadathi as its Patriarchal Parents. Each year an Asanam (Love-Feast) is organised the day after Christmas in their memory.
Their grandson, G Devasahayam, was better known as Challiyar.


== §References ==
^ Colonel Yule, Concerning the City of Cail, II, 30
^ Jaffur Shurreef, Dr G A Herklots, Qanoon-E-Islam - Customs of the Mussulmans of India, Second Edition, 1863, 296 pages, Madras, J Higginbothams, p. 190
^ Robert L. Hardgrave, Jr.(1969) The Nadars of Tamilnad; the political culture of a community in change. British records from 1800 British records to 1968
^ S S Kavitha “Unique in its own way - NAMMA MADURAI Mottai Vinayagar is popular and powerful”, Metro Plus Madurai, The Hindu, 4 August 2007, [1]
^ A Ganesan & S Ramchandran, Researchers, South Indian Social History Research Institute, Chennai, “Tamil Nadars”, Deccan Herald, 1 March 2007
^ Stephen Neill; Eleanor M Jackson, Dr., God's apprentice: the autobiography of Stephen Neill, 1991, 349 pages, Hodder & Stoughton, London, ISBN 0-340-54490-2, p.p. 82-83
^ Robert Eric Frykenberg & Alaine M. Low, Christians and missionaries in India edited by Robert Eric Frykenberg. Grand Rapids, Mich.: Wm. B. Eerdmans Publishing Company (1 February 2003) Paperback: 432 pages, ISBN 0-8028-3956-8 & ISBN 978-0-8028-3956-5 p. 51
^ Norman Etherington, Missions and Empire, Oxford University Press, USA; illustrated edition (20 October 2005), 352 pages, ISBN 0-19-925347-1 & ISBN 978-0-19-925347-0 p. 113
^ Tinnevelly Diocese Property Records - 1935


== §Further reading ==
Etherington, Norman; Missions and empire. Published by Oxford University Press, 2005, ISBN 0-19-925347-1, ISBN 978-0-19-925347-0, 332 pages
Lee Robert, Dana; Converting colonialism: visions and realities in mission history, 1706–1914. Published by Wm. B. Eerdmans Publishing, 2008, ISBN 0-8028-1763-7, ISBN 978-0-8028-1763-1, 304 pages
Reformer in India 1793-1833, Published by CUP Archive