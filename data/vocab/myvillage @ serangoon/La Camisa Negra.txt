"La Camisa Negra" (English: "The Black Shirt") is a Spanish rock song written by Colombian singer-songwriter Octavio Mesa and recorded by Juanes for his third studio album Mi Sangre. In Latin America, the track was released in 2005 as the third single from Mi Sangre, and in Europe, it was released in 2006 as the album's lead single.
The song received mixed reviews from critics and generated controversy when it was used to support neo-fascism in Italy. The single was very successful in Latin America, topping most record charts.


== Music and structure ==
"La Camisa Negra" is written in common time in the key of F-sharp minor. The song is carried by an acoustic guitar strum at a moderate 100 beats per minute, with an accompanying melody on the electric guitar. The lyrics are organized in the common verse-chorus form, and Juanes' range spans around an octave and a half, from C#4 to F#5.


== Critical reception ==
The song received mixed reviews from critics. Contactmusic.com stated that the track "is a good intro into the world of Juanes as it fully exhibits his guitar style and absorbing voice." MyVillage gave the song two out of five stars, commenting that it "has a certain charm about it, but I certainly won't mind missing the boat on this occasion." IndieLondon called the song "a supremely slick acoustic ballad", stating that "the rolling guitar licks…provide a wonderful accompaniment." OMH gave the song a mixed review, stating that "the powerful chorus has a rather catchy vocal melody to it" but that "it's like being promised a culinary banquet only to be served a few chicken nuggets."


== Controversy ==
The song was used in Italy in support of neo-fascism because of the association of "black shirt" with the Fascist Blackshirts of Benito Mussolini, and many nightclub attendees from the far right raised an arm in the fascist salute when the song was played. In response, left-wing media network Indymedia called for a boycott of the song. Juanes later stated that "'La Camisa Negra' has got nothing to do with fascism or Mussolini...People can interpret music in all kinds of ways I guess." The song was also banned in the Dominican Republic for its sexual undertones. It was also implicated in controversy at his August 2009 performance in Havana.


== Chart performance ==
In the United States, the song performed quite well for a Spanish language recording reaching number eighty-nine on the Billboard Hot 100. It performed much better on the Latin charts, topping the Hot Latin Tracks for eight non-consecutive weeks, topping the Latin Pop Airplay, and reaching number two on the Latin Tropical Airplay. Billboard listed the song at number two on the 2005 year-end Hot Latin Songs chart, behind fellow Colombian singer-songwriter Shakira's "La Tortura". The song performed well in Europe, topping the charts in Austria, Denmark, France, Germany, Italy, Spain, and Switzerland and reaching the top twenty in Belgium, Finland, Ireland, the Netherlands, and Norway. The single was certified gold in Switzerland and it is one of the Top 10 singles in the country's history. As of August 2014, it is the 51st best-selling single of the 21st century in France, with 395,000 units sold.


== Music video ==
A music video for the song was released in 2005. In the video, Juanes arrives in a town, accompanied by two women and an older man. The man and women exit the car, and the man begins playing a guitar while the two dance. Various people are shown in realist street scenes. A wave comes out of the guitar, and aside from Juanes, everyone through whom the wave passes is frozen in time, performing the same motion repeatedly in forward and reverse. During the last chorus, the wave reverses direction, and the people of the town disappear. The song and video appear in the video game Dance Dance Revolution Hottest Party 3 and Dance Dance Revolution X2.


== Cover versions ==
In 2007, Mexican singer Lucero included a live version with mariachi of this song on her album Lucero En Vivo Auditorio Nacional. Live cover version from Kylie Minogue in Bogotá 2008, KylieX2008.
In 2009, the University of Rochester Midnight Ramblers recorded a contemporary a cappella version of the song for their 8th studio album, "Ca C'est Bon".
Fox's 2009 TV Show Glee in episode 3 "Acafellas," a cover version of this song was heard in background.
There is also an Iranian version of the song entitled "Beverley Hills" by group Barobax
Dutch band 3JS covered the song in 2011 as a b-side for their Toen Ik Jou Vergat single.


== Formats and track listings ==
12" maxi single (House Remixes)
"La Camisa Negra" [Main Mix]
"La Camisa Negra" [Duro Mix]
"La Camisa Negra" [T.U.&G! Remix]
CD single #1
"La Camisa Negra" [Album Version] - 3:36
"La Camisa Negra" [Remix por Toy Hernández] - 4:36
CD single #2
"La Camisa Negra" [Album Version] - 3:36
"La Camisa Negra" [Sonidero Nacional Remix] - 3:36
Maxi single (July 24, 2005)
"La Camisa Negra" [Album Version] - 3:36
"La Camisa Negra" [Sonidero Nacional Remix] - 3:36
"Fotografía" [feat. Nelly Furtado] - 3:58
"La Camisa Negra" [Video] [Bonus]
An Introduction to Juanes [Bonus]


== Charts and certifications ==


== External links ==
"La Camisa Negra" music video


== References ==