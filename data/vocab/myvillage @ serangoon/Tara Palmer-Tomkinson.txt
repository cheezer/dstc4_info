Tara Palmer-Tomkinson (born 23 December 1971) also known as T P-T, is an English socialite, "it girl", television presenter, columnist, model and charity patron. Her activities have been well-covered by the British tabloid press.


== Early life ==
Her parents are Charles and Patricia Palmer-Tomkinson (née Dawson). Palmer-Tomkinson's father has represented his country as a skier at Olympic level. Tara grew up on her parents' estate in Dummer, Hampshire, and was educated first at Hanford School, then at Sherborne School for Girls in Dorset. After she left school she worked briefly in the City of London for Rothschilds bank.


== Writing career ==
In the mid to late 1990s a weekly column for The Sunday Times appeared under her name. However, this was actually ghostwritten by author Wendy Holden based on Palmer-Tomkinson's 'phoned in description of her activities during the preceding week. She subsequently similarly "contributed" to The Spectator, The Mail on Sunday, GQ, Eve, Harpers and Queen, Tatler, InStyle and The Observer sporadically.
In September 2007 her book The Naughty Girl's Guide to Life, co-authored with Sharon Marshall, was published by Sphere. It was serialised in The Sunday Times Style magazine.
In October 2010 her first novel, Inheritance, was published by Pan Books. However, this also was ghostwritten.


== Television appearances ==
In 2002, she made an appearance on the British television series I'm a Celebrity... Get Me Out of Here!, finishing runner up. This included being gunged in the "Jungle Shower", one of the first 'bush tucker' trials. In November 2005, Palmer-Tomkinson presented her third 'behind the scenes' series on ITV2 for the hit show I'm a Celebrity... Get Me out of Here! Now.
She has also appeared on the reality shows Spelling Bee and Cold Turkey, which followed her attempts to quit smoking with Sophie Anderton, celebrity specials of A Place in the Sun and Blind Date and in episodes of Tabloid Tales, With a Little Help from my Friends, Russian Roulette, Celebrities Under Pressure and Project Catwalk. Palmer-Tomkinson also appeared on Top Gear in 2002 as their "star in a reasonably-priced car".
Palmer-Tomkinson's presenting credits include Animals Do the Funniest things with Tony Blackburn, Junior Eurovision, The British Comedy Awards...Party On, What Kids Really Think, Popworld, Top of the Pops, SM:TV Live, Company Magazine Bachelor of the Year, Dumb Britain, Extreme, a role as a team captain on Bognor or Bust which was hosted by Angus Deayton and work for GMTV, Five, LBC radio, the music channel The Hits and the Living TV programme Dirty Cows.
Palmer-Tomkinson played herself in the film Mad Cows and an episode of Footballers Wives, has acted in a film version of An Ideal Husband and was for a period the face of Walkers Crisps replacing Victoria Beckham. She appeared on an episode of Airline as a member of EasyJet Cabin Crew for a day.
In 2014, Palmer-Tomkinson appeared on the celebrity special edition of The Jeremy Kyle Show in which she revealed the depths of her cocaine addiction and the truth behind her reported relapse.
Palmer-Tomkinson has been a contestant on Comic Relief Does Fame Academy for the BBC. She gave away tickets to see her compete in the show to "ordinary people" who had helped her out (the other contestants generally giving their free tickets to other celebrities). She invited the policeman who found her stolen car, the locksmith who helped when she was locked out of her house and her parents' local shopkeepers. On Friday 16 March 2007 (Red Nose Day – Comic Relief) Tara won Comic Relief Does Fame Academy, beating Tricia Penrose in the final. She was a guest on the BBC's Would I Lie to You?, a comedy quiz which was aired in the spring of 2007 and August 2008, alongside regulars Lee Mack and David Mitchell.


== Other work ==
Palmer-Tomkinson plays the piano, as was demonstrated at events at the Royal Festival Hall with the National Symphony Orchestra, at the Royal Albert Hall with Mozart, and at The Coliseum during a Leonard Bernstein Tribute. She was also the host of the Classic FM Gramophone Awards 2005.
From November 2013 Palmer-Tomkinson was Patron of Scottish charity Speur Ghlan for a year. Speur Ghlan delivers early intervention for young children diagnosed with autism or developmental delays. The appointment garnered media attention for having been facilitated through social media. She stated it was a "huge honour" to have been chosen as patron of the Speur-Ghlan charity and said that she admired the "tireless work" undertaken by the charity. The following year Palmer-Tomkinson herself was diagnosed with "a high degree of autism". According to Palmer-Tomkinson the diagnosis "was a shock but could explain why I've always lived my life at such a frantic level".


== Bibliography ==


=== Novels ===
Inheritance (2010)
Infidelity (2012)


=== Other Books ===
The Naughty Girl's Guide To Life (2007) (written with Sharon Marshall)


== Popular culture ==

In 2004, Paul Harvey's painting of Palmer-Tomkinson was exhibited in The Stuckists Punk Victorian show at the Walker Art Gallery for the Liverpool Biennial.
She had a waxwork model in Madam Tussauds in London.


== Personal life ==
Palmer-Tomkinson hails from a family of landowners and Olympians. Her paternal great-great-grandfather was the landowner, and Liberal politician, James Tomkinson. His wife, Emily Frances, was the daughter of Sir George Palmer, 3rd Baronet. Palmer-Tomkinson's grandfather James Palmer-Tomkinson, uncle Jeremy Palmer-Tomkinson and father Charles have all competed at multiple Winter Olympic Games. Palmer-Tomkinson is the youngest of three children. She has a brother James and a sister, Santa Montefiore (née Palmer-Tomkinson). Her brother-in-law is the historian Simon Sebag Montefiore.
Palmer-Tomkinson's family have a close relationship with the British Royal Family. Her parents are friends of The Prince of Wales and Duchess of Cornwall. She attended the wedding of Prince William and Catherine Middleton.
In 1999, she was treated at the Meadows clinic in Arizona for a cocaine addiction, and since her recovery has supported various drugs charities.
In 2006, Palmer-Tomkinson received extensive publicity after her septum nasi collapsed due to her former £400-a-day addiction to cocaine. Pictures were printed in several British tabloids. She underwent cosmetic surgery to have it rebuilt, at a cost of £6,000. Some sources claim the surgery was carried out by cranio-facial surgeon Martin Kelly, the late husband of actress Natascha McElhone.
On 22 December 2014, Palmer-Tomkinson was arrested at Heathrow airport. This followed her reaction to being refused access to a first-class lounge. Following her arrest she said that a panic attack triggered her behaviour. She stated: "I wasn't drunk, there was no disorderly. I was cautioned, I saw a doctor, they were nice to me", before flying to Switzerland to celebrate her 43rd birthday.


== See also ==


== References ==


== External links ==
Speur Ghlan charity
Tara Palmer-Tomkinson at the Internet Movie Database
MyVillage biography