Naan Mahaan Alla (English: I'm Not Saint) is a Tamil film is directed by S. P. Muthuraman. The film during production was referred to as Naan Gandhi Alla. The film was a remake of Hindi film Vishwanath.


== Plot ==
Viswanath (Rajinikanth), a famous lawyer, stays with his widowed mother (Vijayakumari) and physically handicapped sister (Uma). He sends Jegan, a rich magnate's son (Sathyaraj) and Lokaiya, two criminals to jail on the charge of rape and murder of an innocent girl. Jagan’s father, GMK (M N Nambiar) uses his influence and starts creating trouble in Viswanath’s life. GMK succeeds in getting Viswanath imprisoned on a trumped up charge. His heartbroken mother dies. Once out of jail, Viswanath sets out to punish the culprits when he temporarily loses his eyesight…


== Cast ==
Rajinikanth
Radha
M. N. Nambiar
V.K.Ramasamy
Cho Ramaswamy
Senthamarai
Sathyaraj
Sangili Murugan
K. Kannan
A. R. S.
C. R. Vijayakumari
Uma


== Soundtrack ==
Music composed by Ilaiyaraja and lyrics were penned by Vaali.


== References ==


== External links ==
Naan Mahaan Alla at the Internet Movie Database