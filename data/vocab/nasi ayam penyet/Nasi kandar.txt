Nasi kandar is a popular northern Malaysian dish, which originates from Penang. It is a meal of steamed rice which can be plain or mildly flavoured, and served with a variety of curries and side dishes.


== EtymologyEdit ==
The word Nasi kandar, came about from a time when nasi [rice] hawkers or vendors would [balance] a kandar pole on their shoulder with two huge containers of rice meals. The name has remained and today the word Nasi Kandar is seen on most Tamil Muslim or "Malaysian Mamak" restaurants and Indian-Muslim stall meals.


== DescriptionEdit ==
The rice for a nasi kandar dish is often placed in a wooden container about three feet high, giving it a distinctive aroma. The rice is accompanied by side dishes such as fried chicken, curried beef spleen, cubed beef, lamb, fish roe, fried prawns or fried squid. The vegetable dish would usually be (brinjal or "terong") (aubergine), okra (lady fingers or "bendi") or bitter gourd. A mixture of curry sauces is poured on the rice. This is called 'banjir' (flooding) and imparts a diverse taste to the rice.
Traditionally, nasi kandar is always served with its side dishes on a single plate. Nowadays, small melamine bowls are used for the side dishes. Nevertheless, the curry sauce mix is always poured directly onto the rice.
In recent years, several chain restaurants have appeared such as Nasi Kandar Subaidah, Nasi Kandar Nasmir, Pelita Nasi Kandar, and Kayu Nasi Kandar. Purists have disputed its taste compared to the original Penang versions. In Perlis, the rice is coloured yellow with herbs and the dish is referred to as "nasi ganja", though in fact no "ganja" (cannabis) is actually used in its preparation.


== See alsoEdit ==
Mamak stall


== ReferencesEdit ==


== External linksEdit ==
Nasi Kandar
Nasi Kandar Guide