The depressor septi (Depressor alœ nasi) arises from the incisive fossa of the maxilla.
Its fibers ascend to be inserted into the nasal septum and back part of the alar part of nasalis muscle.
It lies between the mucous membrane and muscular structure of the lip.


== Action ==
The depressor septi is a direct antagonist of the other muscles of the nose, drawing the ala of the nose downward, and thereby constricting the aperture of the nares.
Works like the alar part of the nasalis muscle.


== Additional images ==


== References ==
This article incorporates text in the public domain from the 20th edition of Gray's Anatomy (1918)