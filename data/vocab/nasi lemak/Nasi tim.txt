Nasi tim is a Chinese-Indonesian steamed chicken rice. In Indonesian language nasi means (cooked) rice and tim means steam. The ingredients are chicken, mushroom and hard boiled egg. These are seasoned in soy sauce and garlic, and then placed at the bottom of a tin bowl. This tin bowl is then filled with rice and steamed until cooked. This dish is usually served with light chicken broth and chopped leeks.


== Variations ==

Although it commonly uses chicken, some variants also use pork, fish or beef in place of chicken. Nasi tim for babies are often made from red rice and chicken liver.


== Serving ==
The serving method is as follows: nasi tim in metal bowls (made from tin, aluminium or stainless steel) are usually kept in a steamer to keep warm. It is then served by placing the tin bowl against a plate and the bowl's content will be printed upon the plate. Because this food is always served hot — just like chicken soup — nasi tim is known as comfort food in Chinese Indonesian culture.
The soft texture of rice and boneless chicken also make this dish suitable for young children or adults in convalescence.


== Bibliography ==
Tan, Mely G. (2002), "Chinese Dietary Culture in Indonesian Urban Society", in Wu, David Y. H. & Cheung, Sidney C. H., The Globalization of Chinese Food, Honolulu, H.I.: University of Hawaii Press, pp. 152–169, ISBN 978-0-8248-2582-9. 


== See also ==

List of chicken dishes
Hainanese chicken rice
Duck rice


== References ==


== External links ==
Nasi tim recipe (in Indonesian)