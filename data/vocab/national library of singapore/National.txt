National may refer to:
Nation or country
Nationality – a national is person who is subject to a nation, regardless of whether the person has full rights as a citizen
National (distribution), a type of product or publication that is distributed across an entire nation, e.g., a national magazine

Places
National, Maryland, census-designated place
National, Utah, ghost town
National, West Virginia, unincorporated community
Other uses
Championnat National (also known as National), French football league competition
National (brand), a brand name of electronic goods from Panasonic
National Benzole (or simply known as National), former petrol station chain in the UK, merged with BP
National Car Rental, an American rental car company
National (Cymanfa Ganu), a Welsh festival in Wales, England and North America
National String Instrument Corporation, a guitar company formed to manufacture the first resonator guitars
Their successor companies:
National Dobro Corporation
National Reso-Phonic Guitars

In the context of stringed instruments, the tricone and biscuit designs promoted by the National String Instrument Corporation

SK Nationalkameratene, Norwegian sports club
TIL National, Norwegian sports club


== See also ==
All pages beginning with "National"
All pages with titles containing "National"
Le National (disambiguation), various newspapers and a television program
Nacional (disambiguation)
Nationals (disambiguation)
The National (disambiguation)