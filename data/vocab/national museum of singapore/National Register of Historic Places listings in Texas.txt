These historic properties and districts in the state of Texas are listed in the National Register of Historic Places. Properties and/or districts are listed in most of Texas's 254 counties.
The tables linked below are intended to provide a complete list of properties and districts listed in each county. The locations of National Register properties and districts with latitude and longitude data may be seen in a Google map by clicking on "Map of all coordinates".
The names on the lists are as they were entered into the National Register; some place names are uncommon or have changed since being added to the National Register.

This National Park Service list is complete through NPS recent listings posted July 31, 2015.


== Current listings by county ==
The following are approximate tallies of current listings by county, based on entries in the National Register Information Database as of April 24, 2008 and new weekly listings posted since then on the National Register of Historic Places web site. Frequent additions to the listings and occasional delistings are made, so the counts here are approximate and not official. New entries are added to the official Register on a weekly basis. Also, the counts in this table exclude boundary increase and decrease listings which modify the area covered by an existing property or district and which carry a separate National Register reference number. The numbers of NRHP listings in each county are documented by tables in each of the individual county list-articles.


== See also ==
List of bridges on the National Register of Historic Places in Texas
List of National Historic Landmarks in Texas
Recorded Texas Historic Landmark


== References ==