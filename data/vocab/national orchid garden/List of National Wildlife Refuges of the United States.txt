As of 13 December 2011, there were 555 National Wildlife Refuges in the United States. Refuges that have boundaries in multiple states are listed only in the state where the main visitor entrance is located. The newest refuge established is the Cherry Valley National Wildlife Refuge in Pennsylvania.[1]
The United States is divided into seven regions for administrative purposes:


== Alabama ==
Bon Secour National Wildlife Refuge
Choctaw National Wildlife Refuge
Eufaula National Wildlife Refuge
Mountain Longleaf National Wildlife Refuge
Cahaba River National Wildlife Refuge

Wheeler National Wildlife Refuge
Fern Cave National Wildlife Refuge
Key Cave National Wildlife Refuge
Sauta Cave National Wildlife Refuge
Watercress Darter National Wildlife Refuge


== Alaska ==
Alaska Maritime National Wildlife Refuge
Alaska Peninsula National Wildlife Refuge
Arctic National Wildlife Refuge
Becharof National Wildlife Refuge
Innoko National Wildlife Refuge
Izembek National Wildlife Refuge
Kanuti National Wildlife Refuge
Kenai National Wildlife Refuge
Kodiak National Wildlife Refuge
Koyukuk National Wildlife Refuge
Nowitna National Wildlife Refuge
Selawik National Wildlife Refuge
Tetlin National Wildlife Refuge
Togiak National Wildlife Refuge
Yukon Delta National Wildlife Refuge
Yukon Flats National Wildlife Refuge


== Arizona ==
Bill Williams River National Wildlife Refuge
Buenos Aires National Wildlife Refuge
Cabeza Prieta National Wildlife Refuge
Cibola National Wildlife Refuge
Havasu National Wildlife Refuge
Imperial National Wildlife Refuge
Kofa National Wildlife Refuge
Leslie Canyon National Wildlife Refuge
San Bernardino National Wildlife Refuge


== Arkansas ==
Cache River National Wildlife Refuge
Bald Knob National Wildlife Refuge

Felsenthal National Wildlife Refuge
Overflow National Wildlife Refuge
Pond Creek National Wildlife Refuge

Holla Bend National Wildlife Refuge
Logan Cave National Wildlife Refuge

Wapanocca National Wildlife Refuge
Big Lake National Wildlife Refuge

White River National Wildlife Refuge


== California ==
Coachella Valley National Wildlife Refuge
Castle Rock National Wildlife Refuge
Grasslands Wildlife Management Area
Hopper Mountain National Wildlife Refuge Complex
Bitter Creek National Wildlife Refuge
Blue Ridge National Wildlife Refuge
Hopper Mountain National Wildlife Refuge
Guadalupe-Nipomo Dunes National Wildlife Refuge

Humboldt Bay National Wildlife Refuge
Lanphere Dunes

Kern National Wildlife Refuge
Klamath Basin National Wildlife Refuges Complex (CA and OR)
Clear Lake National Wildlife Refuge
Lower Klamath National Wildlife Refuge
Tule Lake National Wildlife Refuge

Modoc National Wildlife Refuge
North Central Valley Wildlife Management Area
Pixley National Wildlife Refuge
Sacramento National Wildlife Refuge Complex
Butte Sink National Wildlife Refuge
Butte Sink Wildlife Management Area
Colusa National Wildlife Refuge
Delevan National Wildlife Refuge
Sacramento National Wildlife Refuge
Sacramento River National Wildlife Refuge
Sutter National Wildlife Refuge

San Diego Bay National Wildlife Refuge
San Diego National Wildlife Refuge
San Francisco Bay National Wildlife Refuge Complex
Antioch Dunes National Wildlife Refuge
Don Edwards San Francisco Bay National Wildlife Refuge
Ellicott Slough National Wildlife Refuge
Farallon National Wildlife Refuge
Marin Islands National Wildlife Refuge
Salinas River National Wildlife Refuge
San Pablo Bay National Wildlife Refuge

San Luis National Wildlife Refuge Complex
San Luis National Wildlife Refuge
Merced National Wildlife Refuge
San Joaquin River National Wildlife Refuge
Grasslands Wildlife Management Area

Seal Beach National Wildlife Refuge
Sonny Bono Salton Sea National Wildlife Refuge
Stone Lakes National Wildlife Refuge
Tijuana Slough National Wildlife Refuge
Willow Creek-Lurline Wildlife Management Area


== Colorado ==
Alamosa National Wildlife Refuge
Arapaho National Wildlife Refuge
Baca National Wildlife Refuge
Browns Park National Wildlife Refuge
Rocky Flats National Wildlife Refuge
Monte Vista National Wildlife Refuge
Rocky Mountain Arsenal National Wildlife Refuge
Two Ponds National Wildlife Refuge


== Connecticut ==
Stewart B. McKinney National Wildlife Refuge


== Delaware ==
Bombay Hook National Wildlife Refuge
Prime Hook National Wildlife Refuge
Killcohook National Wildlife Refuge (status revoked by Congress in October 1998)


== District of Columbia ==
None


== Florida ==
Arthur R. Marshall Loxahatchee National Wildlife Refuge
Hobe Sound National Wildlife Refuge

Chassahowitzka National Wildlife Refuge
Crystal River National Wildlife Refuge
Egmont Key National Wildlife Refuge
Passage Key National Wildlife Refuge
Pinellas National Wildlife Refuge

Florida Panther National Wildlife Refuge
J.N. 'Ding' Darling National Wildlife Refuge
Caloosahatchee National Wildlife Refuge
Island Bay National Wildlife Refuge
Matlacha Pass National Wildlife Refuge
Pine Island National Wildlife Refuge

Lake Woodruff National Wildlife Refuge
Lower Suwannee National Wildlife Refuge
Cedar Keys National Wildlife Refuge

Merritt Island National Wildlife Refuge
Archie Carr National Wildlife Refuge
Lake Wales Ridge National Wildlife Refuge
Pelican Island National Wildlife Refuge
St. Johns National Wildlife Refuge

National Key Deer Refuge
Crocodile Lake National Wildlife Refuge
Great White Heron National Wildlife Refuge
Key West National Wildlife Refuge

Okefenokee National Wildlife Refuge (administered in Georgia)
St. Marks National Wildlife Refuge
St. Vincent National Wildlife Refuge
Ten Thousand Islands National Wildlife Refuge


== Georgia ==
Eufaula National Wildlife Refuge
Okefenokee National Wildlife Refuge
Banks Lake National Wildlife Refuge

Piedmont National Wildlife Refuge
Bond Swamp National Wildlife Refuge

Savannah Coastal Refuges Complex
Blackbeard Island National Wildlife Refuge
Harris Neck National Wildlife Refuge
Savannah National Wildlife Refuge
Wassaw National Wildlife Refuge
Wolf Island National Wildlife Refuge


== Hawaii ==
Big Island National Wildlife Refuge Complex
Hakalau Forest National Wildlife Refuge
Kona Forest National Wildlife Refuge

Hawaiian Islands National Wildlife Refuge
Kauai National Wildlife Refuge Complex
Hanalei National Wildlife Refuge
Huleia National Wildlife Refuge
Kilauea Point National Wildlife Refuge

Maui National Wildlife Refuge Complex
Kakahaia National Wildlife Refuge
Kealia Pond National Wildlife Refuge

Oahu National Wildlife Refuge Complex
James Campbell National Wildlife Refuge
Oahu Forest National Wildlife Refuge


== Idaho ==
Deer Flat National Wildlife Refuge
Kootenai National Wildlife Refuge
Southeast Idaho National Wildlife Refuge Complex
Bear Lake National Wildlife Refuge
Camas National Wildlife Refuge
Grays Lake National Wildlife Refuge
Minidoka National Wildlife Refuge

Oxford Slough Waterfowl Production Area


== Illinois ==
Chautauqua National Wildlife Refuge
Crab Orchard National Wildlife Refuge
Cypress Creek National Wildlife Refuge
Emiquon National Wildlife Refuge
Hackmatack National Wildlife Refuge
Mark Twain National Wildlife Refuge Complex
Meredosia National Wildlife Refuge
Middle Mississippi River National Wildlife Refuge
Two Rivers National Wildlife Refuge


== Indiana ==
Big Oaks National Wildlife Refuge
Muscatatuck National Wildlife Refuge
Patoka River National Wildlife Refuge and Management Area


== Iowa ==
DeSoto National Wildlife Refuge
Driftless Area National Wildlife Refuge
Iowa Wetland Management District
Neal Smith National Wildlife Refuge
Port Louisa National Wildlife Refuge
Union Slough National Wildlife Refuge


== Kansas ==
Flint Hills National Wildlife Refuge
Kirwin National Wildlife Refuge
Marais des Cygnes National Wildlife Refuge
Quivira National Wildlife Refuge


== Kentucky ==
Clarks River National Wildlife Refuge


== Louisiana ==
Bayou Cocodrie National Wildlife Refuge
Cat Island National Wildlife Refuge
Catahoula National Wildlife Refuge
East Cove National Wildlife Refuge
Lake Ophelia National Wildlife Refuge
Grand Cote National Wildlife Refuge

North Louisiana Refuge Complex
Black Bayou Lake National Wildlife Refuge
D'Arbonne National Wildlife Refuge
Handy Brake National Wildlife Refuge
Louisiana Wetland Management District
Red River National Wildlife Refuge
Upper Ouachita National Wildlife Refuge

Shell Keys National Wildlife Refuge
Southeast Louisiana National Wildlife Refuge Complex
Atchafalaya National Wildlife Refuge
Bayou Sauvage National Wildlife Refuge
Bayou Teche National Wildlife Refuge
Big Branch Marsh National Wildlife Refuge
Bogue Chitto National Wildlife Refuge
Breton National Wildlife Refuge
Delta National Wildlife Refuge
Mandalay National Wildlife Refuge

Southwest Louisiana National Wildlife Refuge Complex
Cameron Prairie National Wildlife Refuge
Lacassine National Wildlife Refuge
Sabine National Wildlife Refuge

Tensas River National Wildlife Refuge


== Maine ==
Aroostook National Wildlife Refuge
Carlton Pond Waterfowl Production Area
Maine Coastal Islands National Wildlife Refuge
Cross Island National Wildlife Refuge
Franklin Island National Wildlife Refuge
Petit Manan National Wildlife Refuge
Pond Island National Wildlife Refuge
Seal Island National Wildlife Refuge

Moosehorn National Wildlife Refuge
Rachel Carson National Wildlife Refuge
Sunkhaze Meadows National Wildlife Refuge


== Maryland ==
Chesapeake Marshlands National Wildlife Refuge Complex
Blackwater National Wildlife Refuge
Eastern Neck National Wildlife Refuge
Martin National Wildlife Refuge
Susquehanna River National Wildlife Refuge

Patuxent Research Refuge [2]


== Massachusetts ==
Assabet River National Wildlife Refuge
Great Meadows National Wildlife Refuge
Mashpee National Wildlife Refuge
Massasoit National Wildlife Refuge
Monomoy National Wildlife Refuge
Nantucket National Wildlife Refuge
Nomans Land Island National Wildlife Refuge
Oxbow National Wildlife Refuge
Parker River National Wildlife Refuge
Silvio O. Conte National Fish and Wildlife Refuge
Thacher Island National Wildlife Refuge


== Michigan ==
Detroit River International Wildlife Refuge
Harbor Island National Wildlife Refuge
Huron National Wildlife Refuge
Kirtlands Warbler Wildlife Management Area
Michigan Wetland Management District
Michigan Islands National Wildlife Refuge
Seney National Wildlife Refuge
Shiawassee National Wildlife Refuge


== Minnesota ==
Agassiz National Wildlife Refuge
Big Stone National Wildlife Refuge
Big Stone Wetland Management District
Crane Meadows National Wildlife Refuge
Detroit Lakes Wetland Management District
Fergus Falls Wetland Management District
Glacial Ridge National Wildlife Refuge
Hamden Slough National Wildlife Refuge
Litchfield Wetland Management District
Mille Lacs National Wildlife Refuge
Minnesota Valley National Wildlife Refuge
Minnesota Valley Wetland Management District
Morris Wetland Management District
Northern Tallgrass Prairie National Wildlife Refuge
Rice Lake National Wildlife Refuge
Rydell National Wildlife Refuge
Sherburne National Wildlife Refuge
Tamarac National Wildlife Refuge
Upper Mississippi River National Wildlife and Fish Refuge
Windom Wetland Management District


== Mississippi ==
Bogue Chitto National Wildlife Refuge
Coldwater River National Wildlife Refuge
Mississippi Sandhill Crane National Wildlife Refuge
Grand Bay National Wildlife Refuge

Mississippi Wetlands Management District
Dahomey National Wildlife Refuge
Tallahatchie National Wildlife Refuge

Noxubee National Wildlife Refuge
St. Catherine Creek National Wildlife Refuge
Theodore Roosevelt National Wildlife Refuge Complex
Hillside National Wildlife Refuge
Holt Collier National Wildlife Refuge
Mathews Brake National Wildlife Refuge
Morgan Brake National Wildlife Refuge
Panther Swamp National Wildlife Refuge
Theodore Roosevelt National Wildlife Refuge
Yazoo National Wildlife Refuge


== Missouri ==
Big Muddy National Fish and Wildlife Refuge
Clarence Cannon National Wildlife Refuge
Great River National Wildlife Refuge
Mingo National Wildlife Refuge
Ozark Cavefish National Wildlife Refuge
Pilot Knob National Wildlife Refuge
Squaw Creek National Wildlife Refuge
Swan Lake National Wildlife Refuge


== Montana ==
Benton Lake National Wildlife Refuge
Benton Lake Wetland Management District

Bowdoin National Wildlife Refuge
Black Coulee National Wildlife Refuge
Creedman Coulee National Wildlife Refuge
Hewitt Lake National Wildlife Refuge
Lake Thibadeau National Wildlife Refuge

Charles M. Russell National Wildlife Refuge
Hailstone National Wildlife Refuge
Halfbreed Lake National Wildlife Refuge
Lake Mason National Wildlife Refuge
War Horse National Wildlife Refuge
UL Bend National Wildlife Refuge

Lee Metcalf National Wildlife Refuge
Medicine Lake National Wildlife Refuge
Lamesteer National Wildlife Refuge

National Bison Range Complex
National Bison Range
Lost Trail National Wildlife Refuge
Ninepipe National Wildlife Refuge
Northwest Montana Wetland Management District
Pablo National Wildlife Refuge
Swan River National Wildlife Refuge

Red Rock Lakes National Wildlife Refuge


== Nebraska ==
Boyer Chute National Wildlife Refuge
Crescent Lake National Wildlife Refuge
DeSoto National Wildlife Refuge
Fort Niobrara National Wildlife Refuge
John and Louise Seier National Wildlife Refuge
North Platte National Wildlife Refuge
Rainwater Basin Wetland Management District
Valentine National Wildlife Refuge


== Nevada ==
Anaho Island National Wildlife Refuge
Desert National Wildlife Refuge Complex
Ash Meadows National Wildlife Refuge
Desert National Wildlife Refuge
Moapa Valley National Wildlife Refuge
Pahranagat National Wildlife Refuge

Fallon National Wildlife Refuge
Ruby Lake National Wildlife Refuge
Stillwater National Wildlife Refuge
Sheldon-Hart Mountain National Wildlife Refuge Complex (NV and OR)
Sheldon National Wildlife Refuge


== New Hampshire ==
Great Bay National Wildlife Refuge
John Hay National Wildlife Refuge
Umbagog National Wildlife Refuge
Wapack National Wildlife Refuge


== New Jersey ==
Cape May National Wildlife Refuge
Edwin B. Forsythe National Wildlife Refuge
Great Swamp National Wildlife Refuge
Supawna Meadows National Wildlife Refuge
Wallkill River National Wildlife Refuge (shared with NY)


== New Mexico ==
Bitter Lake National Wildlife Refuge
Bosque del Apache National Wildlife Refuge
Grulla National Wildlife Refuge
Las Vegas National Wildlife Refuge
Maxwell National Wildlife Refuge
San Andres National Wildlife Refuge
Sevilleta National Wildlife Refuge
Valle de Oro National Wildlife Refuge


== New York ==
Iroquois National Wildlife Refuge
Long Island National Wildlife Refuge Complex
Amagansett National Wildlife Refuge
Conscience Point National Wildlife Refuge
Elizabeth A. Morton National Wildlife Refuge
Oyster Bay National Wildlife Refuge
Sayville National Wildlife Refuge
Seatuck National Wildlife Refuge
Target Rock National Wildlife Refuge
Wertheim National Wildlife Refuge

Montezuma National Wildlife Refuge
Shawangunk Grasslands National Wildlife Refuge
Wallkill River National Wildlife Refuge (shared with NJ)


== North Carolina ==
Alligator River National Wildlife Refuge
Pea Island National Wildlife Refuge

Mackay Island National Wildlife Refuge
Currituck National Wildlife Refuge

Mattamuskeet National Wildlife Refuge
Cedar Island National Wildlife Refuge
Swanquarter National Wildlife Refuge

Pee Dee National Wildlife Refuge
Pocosin Lakes National Wildlife Refuge
Roanoke River National Wildlife Refuge


== North Dakota ==
Arrowwood National Wildlife Refuge Complex
Arrowwood National Wildlife Refuge
Johnson Lake National Wildlife Refuge (easement refuge)

Arrowwood Wetland Management District
Chase Lake National Wildlife Refuge
Chase Lake Wetland Management District
Halfway Lake National Wildlife Refuge (easement refuge)

Valley City Wetland Management District
Hobart Lake National Wildlife Refuge (easement refuge)
Sibley Lake National Wildlife Refuge (easement refuge)
Stoney Slough National Wildlife Refuge (easement refuge)
Tomahawk National Wildlife Refuge (easement refuge)

Audubon National Wildlife Refuge Complex
Audubon National Wildlife Refuge
Camp Lake National Wildlife Refuge (easement refuge)
Hiddenwood National Wildlife Refuge (easement refuge)
Lake Nettie National Wildlife Refuge (easement refuge)
Lake Otis National Wildlife Refuge (easement refuge)
Lake Patricia National Wildlife Refuge (easement refuge)
Lost Lake National Wildlife Refuge (easement refuge)
McLean National Wildlife Refuge (easement refuge)
Pretty Rock National Wildlife Refuge (easement refuge)
Sheyenne Lake National Wildlife Refuge (easement refuge)
Stewart Lake National Wildlife Refuge (easement refuge)
White Lake National Wildlife Refuge (easement refuge)

Lake Ilo National Wildlife Refuge

Des Lacs National Wildlife Refuge Complex
Crosby Wetland Management District
Lake Zahl National Wildlife Refuge

Des Lacs National Wildlife Refuge
Lostwood National Wildlife Refuge
Lostwood Wetland Management District
Shell Lake National Wildlife Refuge (easement refuge)

Devils Lake Wetland Management District
Ardoch National Wildlife Refuge (easement refuge)
Brumba National Wildlife Refuge (easement refuge)
Kellys Slough National Wildlife Refuge
Lake Alice National Wildlife Refuge
Lambs Lake National Wildlife Refuge (easement refuge)
Little Goose National Wildlife Refuge (easement refuge)
Pleasant Lake National Wildlife Refuge (easement refuge)
Rock Lake National Wildlife Refuge (easement refuge)
Rose Lake National Wildlife Refuge (easement refuge)
Silver Lake National Wildlife Refuge (easement refuge)
Snyder Lake National Wildlife Refuge (easement refuge)
Stump Lake National Wildlife Refuge (easement refuge)
Sullys Hill National Game Preserve
Wood Lake National Wildlife Refuge (easement refuge)

J. Clark Salyer National Wildlife Refuge
Buffalo Lake National Wildlife Refuge (easement refuge)
Cottonwood Lake National Wildlife Refuge (easement refuge)
Lords Lake National Wildlife Refuge (easement refuge)
Rabb Lake National Wildlife Refuge (easement refuge)
School Section Lake National Wildlife Refuge (easement refuge)
Willow Lake National Wildlife Refuge (easement refuge)
Wintering River National Wildlife Refuge (easement refuge)

J. Clark Salyer Wetland Management District
Kulm Wetland Management District
Bone Hill Creek National Wildlife Refuge (easement refuge)
Dakota Lake National Wildlife Refuge (easement refuge)
Maple River National Wildlife Refuge (easement refuge)

Long Lake National Wildlife Refuge
Appert Lake National Wildlife Refuge (easement refuge)
Canfield Lake National Wildlife Refuge (easement refuge)
Florence Lake National Wildlife Refuge
Hutchinson Lake National Wildlife Refuge (easement refuge)
Lake George National Wildlife Refuge (easement refuge)
Long Lake Wetland Management District
Springwater National Wildlife Refuge (easement refuge)
Slade National Wildlife Refuge
Sunburst Lake National Wildlife Refuge (easement refuge)

Tewaukon National Wildlife Refuge
Storm Lake National Wildlife Refuge (easement refuge)
Wild Rice Lake National Wildlife Refuge (easement refuge)

Upper Souris National Wildlife Refuge


== Ohio ==
Ottawa National Wildlife Refuge Complex
Cedar Point National Wildlife Refuge
Ottawa National Wildlife Refuge
West Sister Island National Wildlife Refuge


== Oklahoma ==
Deep Fork National Wildlife Refuge
Little River National Wildlife Refuge
Optima National Wildlife Refuge
Ozark Plateau National Wildlife Refuge
Salt Plains National Wildlife Refuge
Sequoyah National Wildlife Refuge
Tishomingo National Wildlife Refuge
Washita National Wildlife Refuge
Wichita Mountains Wildlife Refuge


== Oregon ==
Julia Butler Hansen Refuge for the Columbian White-Tailed Deer (WA and OR)
Klamath Basin National Wildlife Refuges Complex (CA and OR)
Bear Valley National Wildlife Refuge
Klamath Marsh National Wildlife Refuge
Upper Klamath National Wildlife Refuge
Lower Klamath National Wildlife Refuge (CA and OR)

Lewis and Clark National Wildlife Refuge
Malheur National Wildlife Refuge
Mid-Columbia River National Wildlife Refuge Complex
Cold Springs National Wildlife Refuge
McKay Creek National Wildlife Refuge
Umatilla National Wildlife Refuge

Oregon Coast National Wildlife Refuge Complex
Bandon Marsh National Wildlife Refuge
Cape Meares National Wildlife Refuge
Nestucca Bay National Wildlife Refuge
Oregon Islands National Wildlife Refuge
Siletz Bay National Wildlife Refuge
Three Arch Rocks National Wildlife Refuge

Sheldon-Hart Mountain National Wildlife Refuge Complex (NV and OR)
Hart Mountain National Antelope Refuge

Tualatin River National Wildlife Refuge
Willamette Valley National Wildlife Refuge Complex
Ankeny National Wildlife Refuge
Baskett Slough National Wildlife Refuge
William L. Finley National Wildlife Refuge


== Pennsylvania ==
Cherry Valley National Wildlife Refuge
Erie National Wildlife Refuge
John Heinz National Wildlife Refuge at Tinicum


== Rhode Island ==
Rhode Island National Wildlife Refuge Complex
Block Island National Wildlife Refuge
John H. Chafee National Wildlife Refuge
Ninigret National Wildlife Refuge
Sachuest Point National Wildlife Refuge
Trustom Pond National Wildlife Refuge


== South Carolina ==
Cape Romain National Wildlife Refuge
Carolina Sandhills National Wildlife Refuge
Ernest F. Hollings ACE Basin National Wildlife Refuge
Santee National Wildlife Refuge
Savannah Coastal Refuges Complex (administered in Georgia)
Pinckney Island National Wildlife Refuge
Savannah National Wildlife Refuge
Tybee National Wildlife Refuge

Waccamaw National Wildlife Refuge


== South Dakota ==
Huron Wetland Management District
Lacreek National Wildlife Refuge
Bear Butte National Wildlife Refuge

Lake Andes National Wildlife Refuge Complex
Lake Andes National Wildlife Refuge
Lake Andes Wetland Management District
Karl E. Mundt National Wildlife Refuge

Madison Wetland Management District
Sand Lake National Wildlife Refuge
Waubay National Wildlife Refuge
Waubay Wetland Management District


== Tennessee ==
Cross Creeks National Wildlife Refuge
Hatchie National Wildlife Refuge
Tennessee National Wildlife Refuge
West Tennessee National Wildlife Refuge Complex
Chickasaw National Wildlife Refuge
Lake Isom National Wildlife Refuge
Lower Hatchie National Wildlife Refuge
Reelfoot National Wildlife Refuge


== Texas ==
Anahuac National Wildlife Refuge
Aransas National Wildlife Refuge
Attwater Prairie Chicken National Wildlife Refuge
Balcones Canyonlands National Wildlife Refuge
Big Boggy National Wildlife Refuge
Brazoria National Wildlife Refuge
Buffalo Lake National Wildlife Refuge
Caddo Lake National Wildlife Refuge
Grulla National Wildlife Refuge
Hagerman National Wildlife Refuge
Laguna Atascosa National Wildlife Refuge
Lower Rio Grande Valley National Wildlife Refuge
McFaddin National Wildlife Refuge
Muleshoe National Wildlife Refuge
Neches River National Wildlife Refuge
San Bernard National Wildlife Refuge
Santa Ana National Wildlife Refuge
Texas Point National Wildlife Refuge
Trinity River National Wildlife Refuge


== Utah ==
Bear River Migratory Bird Refuge
Fish Springs National Wildlife Refuge
Ouray National Wildlife Refuge


== Vermont ==
Missisquoi National Wildlife Refuge


== Virginia ==
Back Bay National Wildlife Refuge
Chincoteague National Wildlife Refuge
Eastern Shore of Virginia National Wildlife Refuge
Eastern Virginia Rivers National Wildlife Refuge Complex
James River National Wildlife Refuge
Plum Tree Island National Wildlife Refuge
Presquile National Wildlife Refuge
Rappahannock River Valley National Wildlife Refuge

Fisherman Island National Wildlife Refuge
Great Dismal Swamp National Wildlife Refuge
Nansemond National Wildlife Refuge
Potomac River National Wildlife Refuge Complex
Elizabeth Hartwell Mason Neck National Wildlife Refuge
Featherstone National Wildlife Refuge
Occoquan Bay National Wildlife Refuge

Wallops Island National Wildlife Refuge


== Washington ==
Columbia National Wildlife Refuge
Conboy Lake National Wildlife Refuge
Copalis National Wildlife Refuge
Dungeness National Wildlife Refuge
Flattery Rocks National Wildlife Refuge
Franz Lake National Wildlife Refuge
Grays Harbor National Wildlife Refuge
Julia Butler Hansen Refuge for the Columbian White-Tailed Deer (WA and OR)
Little Pend Oreille National Wildlife Refuge
McNary National Wildlife Refuge
Nisqually National Wildlife Refuge
Pierce National Wildlife Refuge
Protection Island National Wildlife Refuge
Quillayute Needles National Wildlife Refuge
Ridgefield National Wildlife Refuge
Saddle Mountain National Wildlife Refuge
San Juan Islands National Wildlife Refuge
Steigerwald Lake National Wildlife Refuge
Toppenish National Wildlife Refuge
Turnbull National Wildlife Refuge
Umatilla National Wildlife Refuge
Willapa National Wildlife Refuge


== West Virginia ==
Canaan Valley National Wildlife Refuge
Ohio River Islands National Wildlife Refuge


== Wisconsin ==
Driftless Area National Wildlife Refuge
Fox River National Wildlife Refuge
Gravel Island National Wildlife Refuge
Green Bay National Wildlife Refuge
Horicon National Wildlife Refuge
Leopold Wetland Management District
Necedah National Wildlife Refuge
St. Croix Wetland Management District
Trempealeau National Wildlife Refuge
Upper Mississippi River National Wildlife and Fish Refuge
Whittlesey Creek National Wildlife Refuge


== Wyoming ==
Managed under Arapaho National Wildlife Refuge (in Colorado)
Bamforth National Wildlife Refuge
Hutton Lake National Wildlife Refuge
Mortenson Lake National Wildlife Refuge
Pathfinder National Wildlife Refuge

National Elk Refuge
Seedskadee National Wildlife Refuge
Cokeville Meadows National Wildlife Refuge


== Insular areas ==


== American Samoa ==
Rose Atoll National Wildlife Refuge


== Guam ==
Guam National Wildlife Refuge


== Northern Mariana Islands ==
None


== Puerto Rico ==
Caribbean Islands National Wildlife complex (PR, UM and VI)
Cabo Rojo National Wildlife Refuge
Culebra National Wildlife Refuge
Desecheo National Wildlife Refuge
Laguna Cartagena National Wildlife Refuge
Vieques National Wildlife Refuge


== US Minor Outlying Islands ==
Caribbean Islands National Wildlife complex (PR, UM and VI)
Navassa Island National Wildlife Refuge

Managed under Hawaiian Islands National Wildlife Refuge (in Hawaii)
Midway Atoll National Wildlife Refuge

United States Pacific Island Wildlife Refuges
Baker Island National Wildlife Refuge
Howland Island National Wildlife Refuge
Jarvis Island National Wildlife Refuge
Johnston Atoll National Wildlife Refuge
Kingman Reef National Wildlife Refuge
Palmyra Atoll National Wildlife Refuge
Wake Atoll National Wildlife Refuge


== US Virgin Islands ==
Caribbean Islands National Wildlife complex (PR, UM and VI)
Buck Island National Wildlife Refuge
Green Cay National Wildlife Refuge
Sandy Point National Wildlife Refuge


== See also ==
List of largest National Wildlife Refuges


== References ==
US Fish and Wildlife Service list of National Wildlife Refuges