Phaguwala is a village located 19 kilometers east of the city of Sangrur and 40 kilometers from Patiala on NH-64 in the district of Sangrur in state of Punjab (India). Phaguwala is surrounded by lush green fields and most fertile land of Indo-Gangetic Plains. This is a village of great freedom fighter and M.L.A Jathedar Jangir Singh Phaguwalia. People of different castes and religions live with brotherhood and peace which includes Jatt Sikh families, Sikh Ravidasias, Sikh Valmikies, Muslims etc. Main surnames are: Ghumans, Tiwanas, Dhaliwals, Behlas etc. The famous Gurudwara Patshahi Naumi is situated here on Sunam road towards south of village. The village has a branch of Punjab & Sind Bank. ATM facility of PNB and P&S Bank are available.


== Gurudwara Sahib Patshahi Navmi Shri Guru Teg Bahadur Ji ==

According to local tradition Phaguwala had not been founded when the Sikh guru Tegh Bahadur Ji passed through this area whilst on his journey from Bhawanigarh to Sunam. According to this tradition the guru met a brahmin peasant ploughing his fields near Bhawanigarh; the peasant served the Guru Ji with food and drink before the guru continued on his journey. To honor the Guru's visit, the Brahmin is said to have constructed a platform and began worshiping it as a sacred site. The site is one kilometer north of the present village.
After sometime the construction of the original platform, a room was constructed and a fair was held to celebrate Basant Panchmi - the fifth day of the light half of the Hindu lunar month of Magh (January–February), the first day of spring. This shrine was eventually developed into a proper gurdwara and was called Gurudwara Sahib Patshahi Naumi "The gurdwara of the ninth master" during the time of Maharaja Narinder Singh of Patiala (1846–62CE) who is also said to have presented a copy of the Guru Granth Sahib for installation here. The present buildings comprising a semi-octagonal sanctum with a domed room above it and a square hall in front, and other ancillaries, were constructed during the 1960s. A 100 feet (30 m) square sarovar has also been added since. The gurdwara owns 14 acres (57,000 m2) of land. It is managed by the Shiromani Gurdwara Prabandhak Committee under Section 87 of the Gurdwaras Act.
The main congregation is held on the fifth day of the light half of each lunar month. Largely attended religious fairs are held on this day falling in the lunar months of Jeth (21 May – 22 June) and Magh to coincide with the martyrdom anniversary of Guru Arjan Dev Ji and Basant Panchmi respectively. All Panchmies are also celebrated at Gurudwara sahib.


== Jathedar Jangir Singh ==

Phaguwala feels proud on name of great freedom fighter and M.L.A Jathedar Jangir Singh, the first president of Pepsu Akali Dal. His picture got placed in Sikh museum Shri Amritsar sahib due to his contributions for Sikh and Punjab issues. After his death, the government school is renamed to his name. Sardarni Joginder Kaur, his wife was elected for SGPC member for 15 consecutive years. His son Davinder singh is retired Principal.


== Politics ==
Gram (Village) Panchayat of Phaguwala is local body responsible for governing, developing and managing the village. It includes Sarpanch and members. Present Sarpanch is Mrs. Arvinder Kaur. Now Phaguwala is divided into 9 wards and each member is elected from each ward. Phaguwala comes under Lok Sabha and Vidhan Sabha constituency seat of Sangrur.


== Project Green 2012 ==

Project Green works to enhance and beautify green spaces. The Project of construction two village parks is under progress. Trees were planted across the boundary of village and at common places under this project.


== Education ==

Government Senior Secondary School
Satya Bharti School


== Notable Persons ==
Jathedar Jangir Singh - Former M.L.A
Nahar Singh Ghuman - Sr. Advocate (PB. and Haryana High Court, Chandigarh)
Rajinder Singh Ghuman - I.P.S(former DIG)
Ujjagar Singh Ghuman - Politician SAD(B) and former Sarpanch
Jathedar Joga Singh - Politician SAD(B) and former SGPC Member


== External links ==
Official website