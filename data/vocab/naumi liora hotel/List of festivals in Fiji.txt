Public holidays in Fiji reflect the country's cultural diversity. Each major religion in Fiji has a public holiday dedicated to it. Also Fiji's major cities and towns hold annual carnivals, commonly called festivals, which are usually named for something relevant to the city or town, such as the Sugar Festival in Lautoka, as Lautoka's largest and most historically important industry is sugar production.
Public Holidays that fall on the weekend are usually moved to either the Friday of the preceding week or the Monday of the following week. This includes religious holidays as well, though in essence they are celebrated on the actual day.


== List of important festivals and days in Fiji ==