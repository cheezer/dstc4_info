Neo-Dada is a minor audio and visual art movement that has similarities in method or intent to earlier Dada artwork. While it revived some of the objectives of dada, it put "emphasis on the importance of the work of art produced rather than on the concept generating the work". It is the foundation of Fluxus, Pop Art and Nouveau réalisme. Neo-Dada is exemplified by its use of modern materials, popular imagery, and absurdist contrast. It also patently denies traditional concepts of aesthetics.
The term was popularized by Barbara Rose in the 1960s and refers primarily, although not exclusively, to a group of artwork created in that and the preceding decade.
Artists linked with the term include Genpei Akasegawa, John Chamberlain, Jim Dine, Kommissar Hjuler, Jasper Johns, Yves Klein, George Maciunas, Yoko Ono, Nam June Paik, Allan Kaprow, George Brecht, Wolf Vostell, Joseph Beuys, Dick Higgins, Ushio Shinohara, Buster Cleveland, Robert Rauschenberg and Doo Kim.


== See also ==
Anti-art


== Footnotes ==


== References ==
Susan Hapgood, Neo-Dada: Redefining Art, 1958-62. Universe Books and American Federation of Arts (1994)
Cecilia Novero, "Antidiets of the Avant-Garde: From Futurist Cooking to Eat Art." (University of Minnesota Press, 2010)
Owen Smith, Fluxus: The History of an Attitude. San Diego State University Press (1998)
Lynn University Art Appreciation