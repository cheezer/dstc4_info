"Neo-Pentecostalism" redirects here. "Neo-Pentecostalism" refers to both the Charismatic Movement and neocharismatic churches.
Neo-charismatic movement are a category of evangelical churches who teach about the gifts of the Spirit, Spiritual warfare and Power evangelism. The Charismatic Christianity incorporates Pentecostal, Evangelical charismatic movement, and neo-charismatic churches. Neo-charismatic represent the "Third Wave" but are broader—now more numerous than Pentecostals (first wave) and charismatics (second wave) combined, owing to the remarkable growth of postdenominational and independent charismatic groups. The origin of the movement is in 1980, in USA.


== Defining characteristics ==
Neo-charismatics, like Apostolics, Pentecostals and charismatics, believe in and stress the post-Biblical availability of gifts of the Holy Spirit, including glossolalia (speaking in tongues), healing, and prophecy. They practice laying on of hands and seek the "infilling" of the Holy Spirit. However, a specific experience of baptism with the Holy Spirit may not be requisite for experiencing such gifts. No single form, governmental structure, or style of church service characterizes all neo-charismatic services and churches. The general definition is somewhat negative in calling them "Christian bodies with pentecostal-like experiences that have no traditional pentecostal or charismatic denominational connections, (and sometimes only very slender—if any—historical connections)".


== Adherents and denominations ==
Some 19,000 denominations or groups, with approximately 295 million individual adherents, are identified as neo-charismatic. Neo-charismatic tenets and practices are found in many independent, nondenominational or post-denominational congregations, with strength of numbers centered in the African independent churches, among the Han Chinese house-church movement, and in South American (especially Brazilian) churches.


=== Examples of Churches ===


== See also ==
Charismatic Christianity
Born Again Movement


== References ==