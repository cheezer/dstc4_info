The New Year Honours is a part of the British honours system, with New Year's Day, 1 January, being marked by naming new members of orders of chivalry and recipients of other official honours. A number of other Commonwealth realms also mark this day in this way.
The awards are presented by or in the name of the reigning monarch, currently Queen Elizabeth II or her vice-regal representative. British honours are published in supplements to the London Gazette.
Honours have been awarded at New Year since at least 1890, in which year a list of Queen Victoria's awards was published by the London Gazette on 2 January. There was no honours list at New Year 1902, as a list had been published on the new King´s birthday the previous November, but in January 1903 a list was again published, though including only Indian orders until 1909 (while the other orders were announced on the King´s birthday in November).
Australia has discontinued New Year Honours, as it now announces its honours on Australia Day, 26 January, and the Queen's Official Birthday holiday, in early June.


== See also ==
Honours Committee
Orders, decorations, and medals of Australia
Orders, decorations, and medals of Canada
Orders, decorations, and medals of New Zealand
Orders, decorations, and medals of the United Kingdom


== References ==
^ The London Gazette: no. 26008. pp. 1–2. 1 January 1890.
^ "Court circular" The Times (London). Tuesday, 31 December 1901. (36652), p. 4.