The newton (symbol: N) is the International System of Units (SI) derived unit of force. It is named after Isaac Newton in recognition of his work on classical mechanics, specifically Newton's second law of motion.


== Definition ==
1 Newton is the force needed to accelerate 1 kilogram of mass at the rate of 1 metre per second squared.
In 1946, Conférence Générale des Poids et Mesures (CGPM) resolution 2 standardized the unit of force in the MKS system of units to be the amount needed to accelerate 1 kilogram of mass at the rate of 1 metre per second squared. The 9th CGPM, held in 1948, then adopted the name "newton" for this unit in resolution 7. This name honors the English physicist and mathematician Isaac Newton, who laid the foundations for most of classical mechanics. The newton thus became the standard unit of force in le Système International d'Unités (SI), or International System of Units.
Newton's second law of motion states that F = ma, where F is the force applied, m is the mass of the object receiving the force, and a is the acceleration of the object. The newton is therefore:
1 N = 1 kg⋅m/s2
where the following symbols are used for the units:
N: newton
kg: kilogram
m: metre
s: second.
In dimensional analysis:

where
F: force
M: mass
L: length
T: time.
This SI unit is named after Isaac Newton. As with every International System of Units (SI) unit named for a person, the first letter of its symbol is upper case (N). However, when an SI unit is spelled out in English, it should always begin with a lower case letter (newton)—except in a situation where any word in that position would be capitalized, such as at the beginning of a sentence or in material using title case. Note that "degree Celsius" conforms to this rule because the "d" is lowercase.— Based on The International System of Units, section 5.2.


== Examples ==
1 N is the force of Earth's gravity on a mass of about 102 g = (1⁄9.81 kg).
On Earth's surface, a mass of 1 kg exerts a force of approximately 9.81 N [down] (or 1.0 kilogram-force; 1 kgf = 9.80665 N by definition). The approximation of 1 kgf corresponding to 10 N (1 decanewton or daN) is sometimes used as an approximation in everyday life and in engineering.
The force of Earth's gravity on (= the weight of) a human being with a mass of 70 kg is approximately 686 N.
The dot product of force and distance is mechanical work. Thus, in SI units, a force of 1 N exerted over a distance of 1 m is 1 N⋅m of work. The Work-Energy Theorem states that the work done on a body is equal to the change in energy of the body. 1 N⋅m = 1 J (joule), the SI unit of energy.
It is common to see forces expressed in kilonewtons or kN, where 1 kN = 1,000 N. See SI prefix.


== Common use of kilonewtons in construction ==
Kilonewtons are often used for stating safety holding values of fasteners, anchors, and more in the building industry. They are also often used in the specifications for rock climbing equipment. The safe working loads in both tension and shear measurements can be stated in kilonewtons. Injection moulding machines, used to manufacture plastic parts, are classed by kilonewton (i.e., the amount of clamping force they apply to the mould).
On the Earth's surface, 1 kN is about 101.97162 kilogram-force of load, so multiplying the kilonewton value by 100 (i.e. using a slightly conservative and easier to calculate value) is a good approximation.


== Conversion factors ==


== See also ==


== Notes and references ==