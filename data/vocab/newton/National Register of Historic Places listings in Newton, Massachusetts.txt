List of Registered Historic Places in Newton, Massachusetts was transferred from List of Registered Historic Places in Middlesex County, Massachusetts, and is an integral part of that list—which in turn is an integral part of List of National Register of Historic Places entries. There are over 180 places listed in Newton.
This list is sortable by village. The 13 villages are:
Auburndale
Chestnut Hill
Newton Centre (spelled Newton Center by the M.B.T.A., but not by the city)
Newton Corner
Newton Highlands
Newton Lower Falls
Newton Upper Falls
Newtonville
Nonantum
Oak Hill
Thompsonville
Waban
West Newton

This National Park Service list is complete through NPS recent listings posted July 31, 2015.


== Current listings ==


== Notes on Zip Codes used ==
Most villages have their own Zip Codes, but some do not. To further add to the confusion, the Zip Codes do not always coincide with the village boundaries which are "unofficial" according to the city. Most residents, though, seem to know exactly where the village lines are.
The following ZIP Codes are present:
a Chestnut Hill Zip Code 02467 extends into Brookline. There are two non-contiguous parts of Newton in this Zip Code but they are joined together by the Brookline part.
b Nonantum does not have its own Zip Code. It uses Newton 02458, which is for Newton Corner. Historic Place listings for Nonantum are assigned to Newton Corner.
c Oak Hill does not have its own Zip Code, It uses Newton 02459, which is for Newton Centre.
d Thompsonville does not have its own Zip Code. It uses Newton 02459 from Newton Centre (or Center), but it also on the border of Chestnut Hill Newton. No listings have been determined to be in Thompsonville, but it is possible that one or more Newton Centre listings may actually be in Thompsonville.


== References ==