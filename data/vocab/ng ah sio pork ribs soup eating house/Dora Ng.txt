Dora Ng Lei-Lo (吳里璐) is a Hong Kong film costume and make up designer.


== Filmography ==
An Empress and the Warriors (2008)
CJ7 (2008)
The Secret of the Magic Gourd (2007)
The Matrimony (2007)
Happy Birthday (2007)
Re-cycle (2006)
Perhaps Love (2005)
Seoul Raiders (2005)
Dumplings and Three... Extremes (2004)
Three (2002)
Tokyo Raiders (2000)
Lavender (2000)
Skyline Raiders (2000)
Purple Storm (1999)
Gorgeous (1999)
Metade Fumaca (1999)
Hot War (1998)
Hero (1997)
Comrades: Almost a Love Story (1996)
Who's the Woman, Who's the Man? (1996)
Peace Hotel (1995)
He's a Woman, She's a Man (1994)


== External links ==
IMDb entry
Interview with Dora Ng
HK cinemagic entry