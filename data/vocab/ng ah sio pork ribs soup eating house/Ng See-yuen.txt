Ng See-yuen, born 1944 in Shanghai is a Chinese director of independent film companies in Hong Kong. He has worked in the Hong Kong film industry since 1970, particularly in Hong Kong action cinema, with roles including film director, producer and screenwriter. Ng is the chairman of the Federation of Hong Kong Filmmakers.


== History ==


=== Filmmaking ===
His career in the industry began at Shaw Brothers Studio, where his official title was "Executive". The first film he was involved in was The Chinese Boxer (1970), on which he worked as assistant director to film director Jimmy Wang Yu. In 1975, he founded Seasonal Films Corporation. The first film produced by the company was Secret Rivals in 1976, which Ng also directed.
Ng produced and co-wrote Snake in the Eagle's Shadow (1978) and Drunken Master, which were both the first films directed by Yuen Woo-ping, and Jackie Chan's first real successes at the domestic box office.
In 1985, Ng was the first Hong Kong producer to make a film in the USA that successfully showed the Hong Kong style of action, when he worked with Corey Yuen on No Retreat, No Surrender, which starred then unknowns Kurt McKinney and Jean-Claude Van Damme.
Other notable films that Ng See-yuen worked on include Ninja in the Dragon's Den (as co-writer and producer) and Legend of a Fighter (as producer and writer), both in 1982. He also co-produced Jackie Chan's 1992 film Twin Dragons, and four of the Once Upon a Time in China series.
Still working in the industry, he produced Alfred Cheung's romantic comedy Contract Lover in 2007, and is credited as presenter for the 2008 film Legendary Assassin.


=== Other industry roles ===
Ng See-yuen is an Honorary Advisor and jury member for the Asian Film Awards. He holds several additional titles including Chairman of the Federation of Hong Kong Filmmakers, Honorary Permanent President of the Hong Kong Film Directors' Guild, and Advisor of the Hong Kong International Film Festival. In April 2007, he became an official member of the Hong Kong Film Development Council. In this capacity, he has been a vocal advocate for the introduction of a motion picture rating system in China and has spoken out on issues such as Mainland China's policies toward the co-production of films with other nations and censorship.
Ng is also the founder of "UME International Cineplex," one of the largest cinema chains in China, with five-star cineplexes in Guangzhou, Shanghai, Chongqing, Hangzhou and an IMAX cineplex in Beijing. "UME" is an acronym for Ultimate Movie Experience.


== References ==

Some additional film information taken from Ng See-yuen's entry at HK Cinemagic.


== External links ==
The Hong Kong Film Development Council
The Hong Kong Film Services Office
Official site of UME Cineplex