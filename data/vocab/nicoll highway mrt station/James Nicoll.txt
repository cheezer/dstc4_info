James Davis Nicoll (born March 18, 1961) of Kitchener, Ontario, is a freelance game and speculative fiction reviewer, former role-playing game store owner, and also works as a first reader for the Science Fiction Book Club. As a Usenet personality, Nicoll is known for writing a widely quoted epigram on the English language, as well as for his accounts of suffering a high number of accidents, which he has narrated over the years in Usenet groups like rec.arts.sf.written and rec.arts.sf.fandom. He is now a blogger on LiveJournal and Facebook. On September 12, 2014, he also started his own website, jamesdavisnicoll.com, dedicated to his book reviews of works old and new.


== Influence on SF genre ==
In addition to his influence as a first reader for the Science Fiction Book Club, and as a book reviewer for Bookspan, Publishers Weekly and Romantic Times, Nicoll often offers ideas and concepts to other writers, primarily through the medium of Usenet. After winning the 2006 Locus Award for his novella Missile Gap, Charles Stross thanked him, writing that Nicoll "came up with the original insane setting — then kindly gave me permission to take his idea and run with it."


== "The Purity of the English Language" ==
In 1990, in the Usenet group rec.arts.sf-lovers, Nicoll wrote the following epigram on the English language:
The problem with defending the purity of the English language is that English is about as pure as a cribhouse whore. We don't just borrow words; on occasion, English has pursued other languages down alleyways to beat them unconscious and riffle their pockets for new vocabulary.
(A followup to the original post acknowledged that the spelling of 'riffle' was a misspelling of 'rifle'.)
Over the years it has spread over the internet, often misattributed to other individuals including Booker T. Washington and a nineteenth-century painter also named James Nicoll. In recent years however the epigram has also been quoted, with proper attribution, in books by professor of rhetoric and communication design Randy Harris. Amateur linguists Jeremy Smith, Richard Lederer, and Anu Garg have also referenced Nicoll's quote.
Professional linguists who have referenced the quotation online include Professor of Linguistics Mark Liberman of the University of Pennsylvania and Language Log; Associate Professor of Linguistics Suzanne Kemmer of Rice University, who also posted her research into the quote at the LINGUIST mailing list; and Second Language Acquisition Ph.D. student Rong Liu. There are also amateur philologists who have used the quote, including journalist Suw Charman and journalist Vale White.


== 'Nicoll Events' ==
Nicoll relates a number of life– and/or limb-threatening accidents that have happened to him, which he has told and retold on various science fiction fandom related newsgroups. Over the years these stories have also been collected into Cally Soukup's List of Nicoll events.
Inspired by Nicoll's collection of accidents, as well as his tendency to take in any stray cat that comes knocking, fantasy author Jo Walton wrote him a poem in 2002, available at her Livejournal.


== "Brain eater" ==
A post on soc.history.what-if credits Nicoll with coining the phrase "brain eater" which is supposed to "get" certain writers such as Poul Anderson and James P. Hogan. Nicoll claims the 'brain eater' affected Hogan, because of Hogan's expressions of belief in Immanuel Velikovsky's version of catastrophism, and his advocacy of the hypothesis that AIDS is caused by pharmaceutical use rather than HIV (see AIDS denialism). The term has been adopted by other Usenet posters,   as well as elsewhere on the Internet and use of the term within Usenet has been criticised.


== Nicoll-Dyson Laser ==
Nicoll proposed the Nicoll-Dyson Laser concept where the satellites of a Dyson swarm act as a phased array laser emitter capable of delivering their energy to a planet-sized target at a range of millions of light years.
E. E. Smith first used the general idea of concentrating the sun's energy in a weapon in the Lensman series when the Galactic Patrol developed the sunbeam (in Second Stage Lensmen); however, his concept did not extend to the details of the Nicoll-Dyson Laser. The 2012 novel The Rapture of the Nerds by Cory Doctorow and Charles Stross uses the Nicoll-Dyson Laser concept by name as the means by which the Galactic Federation threatens to destroy the Earth.


== Science-Fictional Lysenkoism ==
In a discussion on rec.arts.sf.written about why Golden Age science fiction so often uses aliens said to derive from short-lived but well-known stars such as Rigel whose lifespan is probably too brief to ever allow the rise of life due to the long-established mass-luminosity relationship for main-sequence stars, Nicoll identified what he termed the "SFnal Lysenkoist Tendency: when actual, tested science contradicts some detail in an SF story, attack the science."


== Awards ==
Nicoll was one of five nominees for the 2010 and 2011 Hugo Awards for Best Fan Writer. He has also been Fan Guest of Honor (GoH) at SF conventions including 2013's ConFusion (convention) in Detroit  and 2014's Arisia in Boston.


== References ==


== External links ==
More Words, Deeper Hole, James Nicoll's LiveJournal weblog
James Davis Nicoll, James Nicoll's public review site
Nicoll's Usenet posts in Google Groups: since 2000, 2000–1, 1996–9, 1990-2
Millennial Reviews A series of reviews by James Nicoll of science fiction books set in the year 2000.