Bruce Baird Nicoll (3 October 1851 – 18 September 1904) was an Australian politician.
He was born in Sydney to shipwright George Robertson Nicoll and Sarah Baird. When he was six years old his family moved to Scotland, and Nicoll was educated at Dundee before returning to Sydney around 1864. He worked in the family shipping office, and from 1871 ran his own service in the Northern Rivers district. On 1 March 1873 he married Jane Ann Zahel, with whom he had three sons. In 1889 he was elected to the New South Wales Legislative Assembly as the Protectionist member for Richmond. Re-elected in 1891, he was defeated in 1894. Nicoll died at Dulwich Hill in 1904.


== References ==