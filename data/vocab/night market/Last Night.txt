Last Night, The Last Night or Last Nite may refer to:


== Film ==
Last Night (1964 film), an Egyptian film
Last Night (1998 film), a Canadian film by Don McKellar
Last Night, a 2004 short starring Frances McDormand
Last Night (2010 film), a dramatic romance starring Keira Knightley and Sam Worthington


== Literature ==
Last Night, a collection of short stories by James Salter, or the title story


== Music ==


=== Albums ===
Last Night (His Name Is Alive album), 2002
Last Nite (Larry Carlton album), 1986
Last Night (Moby album), 2008
Last Nite (P-Square album), 2003


=== Songs ===
"Last Night", by Little Walter
"Last Night" (Az Yet song)
"Last Night" (Diddy song)
"Last Night" (Good Charlotte song)
"Last Night" (Ian Carey song)
"Last Night" (Lucy Spraggan song)
"Last Night" (Mar-Keys composition)
"Last Night" (The Vamps song)
"Last Night", by The Crickets from The "Chirping" Crickets
"Last Night", by Justin Timberlake from Justified
"Last Night", by Motion City Soundtrack from Even If It Kills Me
"Last Night", by Paris Hilton featuring Lil Wayne
"Last Night", by The Partridge Family from Shopping Bag
"Last Night", by the Rhymefest from El Che
"Last Night", by the Traveling Wilburys from Traveling Wilburys Vol. 1
"Last Night", by Vanessa Hudgens from Identified
"Last Night (Kinkos)", by Omarion from Ollusion
Last Nite
"Last Nite", instrumental by Muddy Waters
"Last Nite", by The Strokes
"Last Nite", song by The New Colony Six W. Kemp
The Last Night
"The Last Night", by Bon Jovi from Lost Highway
"The Last Night" (Aya Matsuura song)
"The Last Night" (Skillet song)


== See also ==
The Last Knight, a 2005 book by Norman Cantor
The Last Knight, a 2007 novel by Hilari Bell