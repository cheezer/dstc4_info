Night or nighttime (sp. night-time or night time) is the period of time between the sunset and the sunrise when the Sun is below the horizon.
This occurs after dusk. The opposite of night is day (or "daytime" to distinguish it from "day" as used for a 24-hour period). The start and end points of time of a night vary based on factors such as season, latitude, longitude and timezone.
At any given time, one side of the planet Earth is bathed in light from the Sun (the daytime) and the other side of the Earth is in the shadow caused by the Earth blocking the light of the sun. This shadow is called the darkness of night. Natural illumination is still provided by a combination of moonlight, planetary light, starlight, diffuse zodiacal light, gegenschein, and airglow. In some circumstances, bioluminescence, aurorae, and lightning can provide some illumination. The glow provided by artificial illumination is sometimes referred to as light pollution because it can interfere with observational astronomy and ecosystems.


== Duration and geography ==
Nights are shorter than days on average due to two factors. Firstly, the sun is not a point, but has an apparent size of about 32 arc minutes. Secondly, the atmosphere refracts sunlight so that some of it reaches the ground when the sun is below the horizon by about 34 arc minutes. The combination of these two factors means that light reaches the ground when the center of the sun is below the horizon by about 50 arc minutes. Without these effects, day and night would be the same length at the autumnal (autumn/fall) and vernal (spring) equinoxes, the moments when the sun passes over the equator. In reality, around the equinoxes the day is almost 14 minutes longer than the night at the equator, and even more towards the poles. The summer and winter solstices mark the shortest and the longest night, respectively. The closer a location is to either the North Pole or the South Pole, the larger the range of variation in the night's length. Although equinoxes occur with a day and night close to equal length, before and after an equinox the ratio of night to day changes more rapidly in high latitude locations than in low latitude locations. In the Northern Hemisphere, Denmark has shorter nights in June than India has. In the Southern Hemisphere, Antarctica has longer nights in June than Chile has. The Northern and Southern Hemispheres of the world experience the same patterns of night length at the same latitudes, but the cycles are 6 months apart so that one hemisphere experiences long nights (winter) while the other is experiencing short nights (summer).
Between the pole and the polar circle, the variation in daylight hours is so extreme that for a portion of the summer, there is no longer an intervening night between consecutive days and in the winter there is a period that there is no intervening day between consecutive nights.


== On other celestial bodies ==

The phenomenon of day and night is due to the rotation of a celestial body about its axis, creating an illusion of the sun rising and setting. Different bodies spin at very different rates, however. Some may spin much faster than Earth, while others spin extremely slowly, leading to very long days and nights. The planet Venus rotates once every 224.7 days – by far the slowest rotation period of any of the major planets. In contrast, the gas giant Jupiter's sidereal day is only 9 hours and 56 minutes. However, it is not just the sidereal rotation period which determines the length of a planet's day-night cycle but the length of its orbital period as well - Venus has a rotation period of 224.7 days, but a day-night cycle just 116.75 days long due. Mercury has the longest day-night cycle as a result of its 3:2 resonance between its orbital period and rotation period - this resonance gives it a day-night cycle 176 days long. A planet may experience large temperature variations between day and night, such as Mercury, the planet closest to the sun. This is one consideration in terms of planetary habitability or the possibility of extraterrestrial life.


== Impact on life ==
The disappearance of sunlight, the primary energy source for life on Earth, has dramatic impacts on the morphology, physiology and behavior of almost every organism. Some animals sleep during the night, whilst other nocturnal animals including moths and crickets are active during this time. The effects of day and night are not seen in the animal kingdom alone; plants have also evolved adaptations to cope best with the lack of sunlight during this time. For example, crassulacean acid metabolism is a unique type of carbon fixation which allows photosynthetic plants to store carbon dioxide in their tissues as organic acids during the night, which can then be used during the day to synthesize carbohydrates. This allows them to keep their stomata closed during the daytime, preventing transpiration of precious water.
As artificial lighting has improved, especially after the Industrial Revolution, night time activity has increased and become a significant part of the economy in most places. Many establishments, such as nightclubs, bars, convenience stores, fast-food restaurants, gas stations, distribution facilities, and police stations now operate 24 hours a day or stay open as late as 1 or 2 a. m. Even without artificial light, moonlight sometimes makes it possible to travel or work outdoors at night.


=== Cultural aspects ===
Night is often associated with danger and evil, because of the psychological connection of night's all-encompassing darkness to the fear of the unknown and darkness's obstruction of a major sensory system (the sense of sight). Nighttime is naturally associated with vulnerability and danger for human physical survival. Criminals, animals, and other potential dangers can be concealed by darkness. Midnight has a particular importance in human imagination and culture.
The belief in magic often includes the idea that magic and magicians are more powerful at night. Seances of spiritualism are usually conducted closer to midnight. Similarly, mythical and folkloric creatures as vampires and werewolves are described as being more active at night. Ghosts are believed to wander around almost exclusively during night-time. In almost all cultures, there exist stories and legends warning of the dangers of night-time. In fact, the Saxons called the darkness of night the 'death mist'.
In literature, night and the lack of light are often color-associated with blackness which is historically symbolic in many cultures for villainy, non-existence, or a lack of knowledge (with the knowledge usually symbolized by light or illumination).
The cultural significance of the night in Islam is completely opposite of night in Western culture. The Koran was revealed during the Night of Power, the most significant night according to Islam. Muhammad made his famous journey from Mecca to Jerusalem and then to heaven in the night. Another prophet, Abraham came to a realization of the supreme being in charge of the universe at night. There are many references in the Koran about the benefits of the night for contemplation and spiritual development.


== See also ==

Black Sun (mythology)
Earth's shadow
Night aviation regulations in the US
Night sky
Nightlife
Nocturne
Olbers' paradox


== References ==


== External links ==
 Quotations related to Night at Wikiquote
 The dictionary definition of night at Wiktionary