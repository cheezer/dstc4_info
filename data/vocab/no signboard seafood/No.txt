No and variants may refer to:
One of a pair of English words, yes and no, which signal confirmation or a negative response respectively
One of the English determiners


== Alphanumeric ==
No. (with dot) or №, also called the Numero sign, an abbreviation of the word "Number" as an identifier (or ordinal value), not as a quantity (cardinal value)
No (kana), a letter/syllable in Japanese script
No symbol, a circle with a diagonal line through it


== Geography ==
Lake No, South Sudan
New Orleans, Louisiana
Nō, Niigata, a town in Japan
No Creek (disambiguation)
NO, the ISO 3166-1 country code for Norway
no, the ISO 639-1 code for Norwegian language
.no, the internet ccTLD for Norway

North


== Literature, film and television ==
Dr. No (novel), a 1958 book by Ian Fleming; the sixth of his James Bond series
Dr. No (film), a 1962 British spy film; the first of the James Bond series
Julius No, the titular and antagonist character of the film Dr. No

No (2012 film), a 2012 Chilean film
Nô (film), 1998 film by director Robert Lepage
No, A concept in the anime FLCL
Nō, a (female) playable video game character in Koei's Samurai Warriors and Warriors Orochi


== Music ==
No (album), an album by Old Man Gloom
No!, an album by They Might Be Giants


=== Songs ===
"No" (song), by Shakira
"No", a song by Soulfly from their eponymous debut album
"No", song by Monrose on their album Temptation
"No", a song by Jason Aldean from his second album Relentless


== Science and technology ==
Normally open, a type of electrical switch
Nitric oxide (NO), a chemical substance
Nobelium (No), a chemical element


== Other uses ==
Neos (airline), an Italian leisure airline currently using IATA airline designator code NO
Aus-Air, a defunct Australian regional airline formerly using the IATA airline designator code NO
National Offensive, German neo-Nazi party
Noh, a style of Japanese theatre
Novus Ordo, a composite term for the post-1969 Roman Rite Mass


== See also ==
Dr. No (disambiguation)
Negation (disambiguation)
Negative (disambiguation)
Negativity (disambiguation)
No, No, No (disambiguation)
No-no (disambiguation)