The Nation's Noodle is an instant noodle product, similar to Pot Noodle. The product was launched in the United Kingdom on 3 August 2009, by Symington's, using the Golden Wonder name. Golden Wonder themselves used to own Pot Noodle.


== Current UK flavours ==
All Day Breakfast (The Nation's Noodle)
Chicken and Mushroom (The Nation's Noodle)
Beef & Tomato (The Nation's Noodle)
Sweet & Sour (The Nation's Noodle)Discontinued.
Chip Shop Curry (The Nation's Noodle)
Inferno Chilli (The Nation's Noodle)
Spicy Tomato (The Nation's Pasta)
Spaghetti Bolognese (The Nation's Pasta)
Macaroni Cheese (The Nation's Pasta)


== References ==


== External links ==
Official website