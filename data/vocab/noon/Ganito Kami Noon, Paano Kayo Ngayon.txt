Ganito Kami Noon, Paano Kayo Ngayon? (Tagalog, "This is how we were, How are you doing now?") is a 1976 Filipino romantic musical drama film set in the era of Spanish colonization in the Philippines. It was directed by Eddie Romero and starred Christopher De Leon and Gloria Diaz. The film was selected as the Philippine entry for the Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee.
A digitally restored version of the film by the ABS-CBN Archives and Central Digital Lab will be shown as the opening entry for the Cinema One Originals Festival.


== Plot ==
Set at the turn of the 20th century during the Filipino revolution against the Spaniards and, later, the American colonizers, it follows a naive peasant through his leap of faith to become a member of an imagined community.


== Cast ==
Christopher De Leon as Nicolas "Kulas" Ocampo
Gloria Diaz as Diding
Eddie Garcia
Leopoldo Salcedo


== Awards ==


== See also ==
List of submissions to the 49th Academy Awards for Best Foreign Language Film
List of Philippine submissions for the Academy Award for Best Foreign Language Film


== References ==


== External links ==
Ganito kami noon, paano kayo ngayon at the Internet Movie Database