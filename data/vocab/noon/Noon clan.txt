Noon (Punjabi: نون) is a Punjabi tribe in Pakistan variously ascribed as Jatt, Rajput and Gujjar. Members of the Noon clan use Rana, Malik and Chaudhry as their titles.


== History and origin ==
The Noon tribe has varied origins based on their area of settlement. The tribe is settled in Punjab in the form of a large clan based around the area of Multan, and a family, traced back to one ancestry, in [Sargodha]and [islamabad].
The Noon family of Sargodha is closely related to the Tiwana Muslim Rajput. In addition to the Tiwanas, they are closely allied to the Awan, Bandial,Gujjar and Piracha tribes in Pakistan. The family is also amongst the "First families of Pakistan", its members having held various political and government positions. Traditionally the Noon family has remained in agriculture and politics, however it has also given rise to many professionals and technocrats, dating back to the Mughal era. The Noon family has also been closely linked to the equestrian sport of polo, and has produced several outstanding polo players at a regional level.
The Noon family of Sargodha use the title of Malik, whereas the Noon clan situated around Multan uses the title of [[Rana]and in Islamabad Malik]


== Distribution ==
The detailed distribution of the tribe is as follows:
The Noon tribe/clan is found along the banks of the Sutlej and Chenab from Sargodha in the north to Multan and Shujabad in the south. There are several families of Noons here. In addition, the clan can also be found in Kallur Kot tehsil of Bhakkar District, in Shujabad Tehsil of Multan District, in Dera Ismail Khan and Lodhran. The Sargodha based Noon family owns and resides in its 27 villages in Bhalwal, including:
Ali Pur Noon
Aamin Abad Noon
Nurpur noon
Sultan Pur Noon
Kot Hakim Khan Noon
Zaffer Abad Noon


== Prominent Noons ==
Malik Feroz Khan Noon
Malik Anwar Ali Noon
Viqar un Nisa Noon
Amjad Ali Noon
Rana Muhammad Qasim Noon


== See also ==
Muslim Rajput clans
Gurjar clans


== References ==