Noon (Non, None, Serer-Noon) is a Cangin language of Senegal. Ethnologue reports that it is 84% cognate (and 52% intelligible) with Lehar, essentially a divergent dialect, and 68% cognate with the other Cangin languages.
The Noon people identify themselves ethnically as Serer. However, their language, often called Serer-Noon on the assumption that it is a Serer dialect, is not closely related.


== Status ==
Like many of the local languages in Senegal, the Noon language is officially recognized as one of the national languages of country.


== Writing ==
The Noon language is written using the Latin alphabet. In 2005, a decree was passed by the Senegalese Government in order to regulate the spelling to Noon.


== Notes ==