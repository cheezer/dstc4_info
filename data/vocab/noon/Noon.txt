Noon (also midday or noon time) is usually defined as 12 o'clock in the daytime. The term midday is also used colloquially to refer to an arbitrary period of time in the middle of the day. Solar noon is when the sun crosses the meridian and is at its highest elevation in the sky, at 12 o'clock apparent solar time. The local or clock time of solar noon depends on the longitude and date. The opposite of noon is midnight.
In many cultures in the Northern Hemisphere, noon had ancient geographic associations with the direction "south" (as did midnight with "north" in some cultures). Remnants of the noon = south association are preserved in the words for noon in French (Midi) and Italian (Mezzogiorno), both of which also refer to the southern parts of the respective countries. Modern Polish, Ukrainian, and Serbian go a step farther, with the words for noon (południe, південь, пoднe – literally "half-day") also meaning "south" and the words for "midnight" (północ, північ, пoнoħ – literally "half-night", as with English mid(dle) meaning "half") also meaning "north".


== Etymology ==

The word noon is derived from Latin nona hora, the ninth hour of the day, and is related to the liturgical term none. The Roman and Western European medieval monastic day began at 6:00 a.m. (06:00) at the equinox by modern timekeeping, so the ninth hour started at what is now 3:00 p.m. (15:00) at the equinox. In English, the meaning of the word shifted to midday and the time gradually moved back to 12:00 local time (that is, not taking into account the modern invention of time zones). The change began in the 12th century and was fixed by the 14th century.


== Solar noon ==
Solar noon is the moment when the sun transits the celestial meridian – roughly the time when it is highest above the horizon on that day ("Sun transit time"). This is also the origin of the terms ante meridiem (a.m.) and post meridiem (p.m.) as noted below. The sun is directly overhead at solar noon at the Equator on the equinoxes, at the Tropic of Cancer (latitude 23°26′14.1″ N) on the June solstice, and at the Tropic of Capricorn (23°26′14.1″ S) on the December solstice. In the Northern hemisphere, above the Tropic of Cancer, the sun is directly to the south of the observer at solar noon, and in the Southern hemisphere, below the Tropic of Capricorn, it is directly to the north.


== Nomenclature ==

Noon is commonly indicated by 12 p.m., and midnight by 12 a.m. While some argue that such usage is "improper" based on the original Latin meanings (a.m. stood for ante meridiem and p.m. for post meridiem, meaning "before midday" and "after midday" respectively), the usage of 12 p.m. to refer to noon is conventional. An earlier standard of indicating noon as "12M" or "12m" (for "meridies"), which was at one time specified in the U.S. GPO Government Style Manual, has fallen into relative obscurity; the current edition of the GPO makes no mention of it.
However, some still consider the use of 12 a.m. and 12 p.m. confusing and seek a less ambiguous phrasing. Common alternative methods of representing these times are:
to use a 24-hour clock (00:00 and 12:00, 24:00; but never 24:01)
to use "12 noon" or "12 midnight" (though "12 midnight" may still present ambiguity regarding the specific date)
to specify midnight as between two successive days or dates (as in "midnight Saturday/Sunday" or "midnight December 14/15")
to avoid those specific times and to use "11:59 p.m." or "12:01 a.m." instead. (This is common in the travel industry to avoid confusion as to passengers' schedules, especially train and plane schedules.)


== See also ==
12-hour clock
Dipleidoscope
Midnight
Hour angle
Solar azimuth angle


== Notes ==
^ The 29th edition of the U.S. GPO Government Printing Office Style Manual (2000) section 12.9


== References ==


== External links ==
 Media related to Noon at Wikimedia Commons
Generate a solar noon calendar for your location
U.S. Government Printing Office Style Manual (2008), 30th edition
Shows the hour and angle of sunrise, noon, and sunset drawn over a map.