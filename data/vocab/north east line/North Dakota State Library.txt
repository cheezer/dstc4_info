The North Dakota State Library is a government operated library in the U.S. state of North Dakota. Located in the state's capital city of Bismarck on the capital grounds, the library has been in operation since 1907.


== History ==
The State Library was established as the Public Library Commission in 1907, and it occupied a single room in the North Dakota State Capitol building. In 1909, the library's name was changed to the State Library Commission. In 1936, the library moved to the Liberty Memorial Building on the Capitol Grounds, which is where it remained until 1970. At that point, the library moved once again, this time to the Randal Building north of the city. The agency's name was changed to the North Dakota State Library in 1979, which is still its name today. In 1982, the State Library returned to the Liberty Memorial Building, its present location.


== State Librarians ==
Mary J. Soucie, 2014-current
Hulen E. Bivins, 2010-2013
Doris A. Ott, 2001-2010
Joseph C. Linnertz (acting director), 2000-2001
Mike Jaugstetter, 1996-2000
Joseph C. Linnertz (acting director), 1995-1996
William R. Strader, 1991-1993
Patricia L. Harris, 1985-1991
Margaret M. Stefanak, 1983-1985
Ruth E. Mahan, 1981-1983
Richard J. Wolfert, 1969-1981
Leone Morrison (acting director), 1968-1969
Freda W. Hatten, 1964-1968
Hazel Webster Byrnes, 1948-1964
Lillian E. Cook, 1922-1948
Mary E. Downey, 1921-1923
S. Blanche Hedrick, 1919-1921
Minnie Clarke Budlong, 1909-1919
Zana K. Miller, 1907-1908


== Function ==
The State Library specializes in information services to state agencies and to the general public.


== Departments ==
North Dakota State Library's departments include: Administration, Digital Initiatives, Field Services, Interlibrary Loan, Information Technology, Public Awareness, Public Services, State Document Depository, Statewide Catalog Development, Talking Books, Technical Services, and Training. 


== Publications ==
North Dakota State Library staff produce publications on Library Vision, State Library services, handbooks for North Dakota public library board members, copyright, interlibrary loan, North Dakota library law, search warrants, and North Dakota public library statistics. These publications include:
Brochures
Flyers
Handbooks & Manuals
Library Directories
Reports


== References ==


== External links ==
Official website