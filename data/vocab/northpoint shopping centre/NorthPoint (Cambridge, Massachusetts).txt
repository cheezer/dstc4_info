NorthPoint is a mixed-use redevelopment of an old railroad yard in the East Cambridge section of Cambridge, Massachusetts.
Situated on a rail yard originally built by the Boston and Maine Railroad, Pan Am Systems (formerly Guilford Transportation Industries) decided to turn the now-unused rail yard into a large development, containing commercial, retail and residential development, along with a relocated Lechmere Green Line transit station.


== §Site specifics ==
The site is bounded by the Gilmore Bridge, Monsignor O'Brien Highway, Water Street and the MBTA Commuter Rail Fitchburg Line. The area also includes some existing buildings which will remain, along with some new construction on the rail yard land, but built outside of the NorthPoint umbrella. The site contains roughly 19 land "parcels", in addition to new road infrastructure and parkland.


== §Construction phases ==
Phase 1 of the project is complete, with two NorthPoint buildings containing condominiums standing in front of a central portion of the park. Phase 2 is about to begin, which will include additional new streets being constructed. The final plans include commercial, residential and combined buildings and almost 20 acres of greenspace between the structures. The new relocated Lechmere station is in development with the Commonwealth of Massachusetts but ground has not yet been broken.
The area is also located a short distance away from the Community College MBTA station located in Charlestown.


== §External links ==
Developer web site
Condominiums Blog
Condos at NorthPoint web site
Bushari Listing
North Point Car Free
NorthPoint overview on ArchBOSTON