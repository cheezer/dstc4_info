Nostalgia for the Soviet Union or Soviet nostalgia is a moral-psychological phenomenon in Russia and post-Soviet states, as well as among some Russophone persons abroad who were born in the Soviet Union (Soviet people, Soviet generation). Nostalgia for the Soviet Union could be expressed in nostalgia for the Soviet regime, Soviet society, Soviet lifestyle, Soviet culture, or simply the aesthetics of the Soviet epoch.
On April 25, 2005 the President of Russia, Vladimir Putin stated that the biggest geopolitical disaster of the 20th century was the dissolution of the Soviet Union.


== Revival of Stalin's cult ==

Since 2009 in Ukraine, the Communist Party of Ukraine has actively tried to revive the cult of Joseph Stalin. On 22 June 2013, Serhiy Topalov, a People's Deputy from the Communist Party attacked a law enforcement agent over a portrait of Stalin.


== See also ==

Dissolution of the Soviet Union
National Bolshevism
Neo-Stalinism
Putinism
Russian nationalism
Soviet socialist patriotism


=== Communist nostalgia in Europe ===
Ostalgie, in the new states of Germany
Yugo-nostalgia, in the Balkans


== References ==


== Further reading ==
Satter, D. It Was a Long Time Ago and It Never Happened Anyway: Russia and the Communist Past. Yale University Press. New Haven, 2012. ISBN 0300111452.
Boffa, G. "From the USSR to Russia. History of unfinished crisis. 1964—1994"
Mydans, S. 20 Years After Soviet Fall, Some Look Back Longingly. New York Times. August 18, 2011
Weir, F. Why nearly 60 percent of Russians 'deeply regret' the USSR's demise. The Christian Science Monitor. December 23, 2009.
Houslohner, A. Young Russians never knew the Soviet Union, but they hope to recapture days of its empire. Washington Post. June 10, 2014


== External links ==


=== News ===
Blundy, A. Nostalgia for the Soviet Era Sweeps the Internet. Newsweek. July 30, 2014.
Pippenger, N. Why Are So Many Russians Nostalgic For The USSR? New Republic. August 19, 2011.


=== Internet societies ===
Project "Encyclopedia of our childhood", Soviet Union through the eyes of contemporaries
Museum "20th century". Recollections about the Soviet epoch
3a_cccp — society "For our Soviet Motherland!"
ussr_ru — society "USSR (all about the 1917—1991 epoch)"
vospominanija — society "What always is nice to remember..."
cccp_foto — society "1922 — 1991: USSR in photos"
How it was. "Remember all". ч. 1 ч. 2 ч. 3
Blog commemorated to the USSR, things, style, photo, nostalgia
Soviet cards and posters
USSR in scale, a website commemorated to a private collection of Soviet technology and vehicles in the scale 1:43
In Barnaul was opened a store "Sovietsky" (photo)
Soviet heritage: between zoo, reservation and sanctuary (about "Soviet epoch parks") // Новая Эўропа — DELFI, 11 сентября 2013