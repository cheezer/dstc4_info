Yugo-nostalgia is a little-studied psychological and cultural phenomenon occurring among citizens of the former Socialist Federal Republic of Yugoslavia (SFRY). While its anthropological and sociological aspects have not been clearly recognized, the term, and the corresponding epithet "Yugo-nostalgic", is commonly used by the people in the region in two distinct ways: as a positive personal descriptive, and as a derogatory label.
Present cultural and economic manifestations of Yugo-nostalgia include music groups with Yugoslav or Titoist retro iconography, art works, films, theater performances, and many organized, themed tours of the main cities of the former Yugoslav republics. The notion of Yugo-nostalgia should not be confused with Yugoslavism which is the ideology behind the unity of South Slavic nations. The concepts may go hand in hand but Yugo-nostalgia celebrates the pre-1991 period whereas Yugoslavism and Yugoslav reunification (as a branch of Pan-Slavism) are an ongoing mindset just as likely to appeal to persons born after the breakup of Yugoslavia that feel their national interests may be best served by unification.


== Positive sense ==

In its positive sense, Yugo-nostalgia refers to a nostalgic emotional attachment to both subjective and objectively desirable aspects of the SFRY. These are described as one or more of: economic security, sense of solidarity, socialist ideology, multiculturalism, internationalism and non-alignment, history, customs and traditions, and more rewarding way of life. As Halligan argues, such nostalgia effectively "reclaims" pre-1989 cultural artefacts, even propaganda films. These positive facets, however, are opposed to the perceived faults of the successor countries, many of which are still burdened by the consequences of the Yugoslav wars and are in various stages of economic and political transition. The faults are variously identified as parochialism, jingoism, corruption in politics and business, the disappearance of the social safety net, economic hardship, income inequities, higher crime rates, as well as a general disarray in administrative and other state institutions.


== Negative sense ==
In the negative sense, the epithet has been used by the supporters of the new post-dissolution regimes to portray their critics as anachronistic, unrealistic, unpatriotic, and potential traitors. In particular, during and after the Yugoslav wars, the adjective has been used by state officials and media of some successor countries to deflect criticism and discredit certain avenues of political debate. In fact, it is likely that the term Yugo-nostalgic was originally coined precisely for this purpose, appearing as a politically motivated pejorative label in government-controlled media, for example in Croatia, very soon after the breakup of the SFRY.
According to Dubravka Ugrešić the term Yugo-nostalagic is used to discredit a person as a public enemy and a "traitor".


== Decline and rise of Yugoslavism ==
At the breakup of SFRY, the idea of Yugoslavism had lost popularity. Serbia and Montenegro continued a South Slavic union as Federal Republic of Yugoslavia from April 1992 to February 2003, then renamed the country with the federal republics' individual names. The number of declared Yugoslavs in the region reached an all-time low. The last census in Serbia showed approximately 80,000 Yugoslavs, but at this time the country was still known as such. The former country's main language, Serbo-Croatian, is no longer the official language of any of the former state's constituent republics. Few resources are published about the language, and it has no standardizing body. The .yu Internet domain name, which was popular among Yugo-nostalgic websites, was phased out in 2010.
Yugo-nostalgia is seeing a comeback in the former Yugoslav states. In Vojvodina (north province of Serbia), one man has set up Yugoland, a place dedicated to Tito and Yugoslavia. Citizens from former Yugoslavia have traveled great distances to celebrate the life of Tito and the country of Yugoslavia.


== Yugoslav reunification ==
Yugoslav reunification refers to an idea of reunification of the former six Yugoslav republics – Croatia, Serbia, Slovenia, Bosnia and Herzegovina, Montenegro and Macedonia. This process has always been met with many difficulties due to continuous tension between the countries, which have become vastly different through over two decades of separation.


== Yugo-nostalgia in contemporary culture ==
These programmes are pan-regional and comprise the participation of a minimum of three former Yugoslav republics and no states outside of the territory.


=== Reality TV ===
Celebrity shows
Veliki brat (Big Brother)
merging with existing Croatian participation and arrangements as of Veliki brat 2011

Farma (The Farm)
Singing contests
Zvezde Granda
X Factor Adria, October 2013–present, (SR, BH, HR, MK, CG)
Operacija trijumf, 2008–09, (SR, BH, HR, MK, CG)
Idol, 2003–05, (SR, CG, MK)


=== TV Series ===
Kursadžije, comedy
The Scent of Rain in the Balkans, drama


== See also ==

Yugoslavs
Yugoslavism
Yugoslavia
Titoism
Ostalgie - a similar phenomenon of nostalgia for the former socialist East Germany
Nostalgia for the Soviet Union, a similar phenomenon of nostalgia in the post-Soviet states


== Bibliography ==
Halligan, Benjamin: "Idylls of Socialism: The Sarajevo Documentary School and the Problem of the Bosnian Sub-proletariat". In Studies in Eastern European Cinema (Autumn 2010). (http://usir.salford.ac.uk/11571/3/visualrecollectivisationpostcopyedit.pdf)
Trovesi, Andrea: L'enciclopedia della Jugonostalgija. In Banchelli, Eva: Taste the East: Linguaggi e forme dell'Ostalgie, Sestante Edizioni, Bergamo 2006, ISBN 88-87445-92-3, p. 257-274.
Dejan Djokic, ed. "Yugoslavism: Histories of a Failed Idea, 1918–1992". London: Hurst & Co., 2003. 356 pp.
Yugo-Nostalgia: Cultural Memory and Media in the Former Yugoslavia, Author: Volcic, Zala, Critical Studies in Media Communication, Volume 24, Number 1, March 2007, pp. 21–38(18), Publisher: Routledge.
Kristen R. Ghodsee, "Red Nostalgia? Communism, Women's Emancipation, and Economic Transformation in Bulgaria."


== References ==


== External links ==
The Independent (1 March 2008). "Bringing back Tito". London. Retrieved 21 May 2010. 
NPR (9 October 2006). "'Yugonostalgia' Takes Hold in Slovenia". Retrieved 21 May 2010.