A novena (from Latin: Novem, meaning Nine) is an act of religious pious devotion originating in ancient Christianity, often consisting of private or public prayers repeated for nine successive days in belief of obtaining special intercessory graces. Novenas are most often prayed by members of the Roman Catholic Church, as well as by communicants of the Anglican Church, Eastern Orthodox Church and Lutheran Church. In addition, novenas have also been used in an ecumenical Christian context, such as those promulgated by Premier, in order to pray for Church renewal.
The prayers are often derived from devotional prayer books, or consist of the recitation of the rosary (a "rosary novena"), or of short prayers through the day. Novena prayers are customarily printed in small booklets, and the novena is often dedicated to a specific angel, saint, a specific Marian title of the Blessed Virgin Mary, or it invokes one of the personages of the Holy Trinity.
Within the Roman Catholic discipline, novena prayers for public use must have an Imprimatur, Nihil Obstat, and Imprimi potest. These ecclesiastical sanctions are usually granted by a bishop or any ranking prelate for publication and approval.


== History ==
The practice of saying novenas may be derived from Holy Scripture:

The practice of the novena may have been influenced from the Medieval practice of holding daily Mass for nine consecutive days for recently departed members of the upper classes and clergy, which itself may trace its origins to an early Greek and Roman custom performed by families, consisting of nine days of mourning after the death of a loved one, followed by a feast, which originally prompted Catholic writers such as St. Augustine, Pseudo-Alcuin and John Beleth to warn Christians not to emulate the custom.
Over time, members of Roman Catholic faith began to place less emphasis on the number nine's connection to the pagan custom, and more on its association with the nine months Jesus spent in the womb, the giving up of His spirit at the 9th hour, and the event which occurred in the Upper Room with Twelve Apostles and the Blessed Virgin Mary when they prayed for nine days until the Holy Spirit descended on the Feast of the Pentecost. In the New Testament, this biblical event is often quoted from Acts of the Apostles, 1:12 – 2:5. The Church Fathers also assigned special meaning to the number nine, seeing it as symbolic of imperfect man turning to God in prayer (due to its proximity with the number ten, symbolic of perfection and God). These developments first affected Christian mortuary celebrations, and then carried over to prayer.
In the Roman Catholic Church, there are four recognized categories of novenas which belong to more than one of these categories:
Mourning, or in anticipation of a Burial
In anticipation of a Church Feast or ending in Vespers (often requires Church attendance)
Individual or Group Petition (Expiatory)
Indulgence for the remission of Sins (often requires Sacrament of Confession or Church attendance)
By standard liturgical norms, novenas are performed in church, at home, or anywhere where solemn prayers are appropriate, though some indulgenced novenas require church attendance. Sometimes, a special candle or incense is lit at the beginning of the novena which burns during the nine days of prayer. The first chapter of the General Principles of Sacrosanctum Concilium, #13 is often cited as a guideline regarding the implementations of public novenas.
Novena prayers are also practised by Lutheran, Orthodox and Anglican Christians, who hold close or similar beliefs regarding its pious practice. In addition, novenas have also been used in an ecumenical Christian context, such as those promulgated by Premier, in order to pray for Church renewal.


== See also ==
Marian devotions – Miraculous Medal novena
Novenas in anticipation of Christmas: Las Posadas, Simbang Gabi, Novena of aguinaldos
Novena of Grace
Novena to Our Mother of Perpetual Help


== References ==


== Bibliography ==
Right Reverend Monsignor Joseph F. Stedman, The New Revised 'Triple' Novena Manual, Confraternity of the Precious Blood, 1975.
Barbara Calamari & Sandra DiPasqua, Novena, Penguin Studio, 1999. ISBN 0-670-88444-8.


== External links ==
List of Novenas at EWTN
"Novena for the repose of the soul of John Paul II", United States Conference of Catholic Bishops (USCCB)