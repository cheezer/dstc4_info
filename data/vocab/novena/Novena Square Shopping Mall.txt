Novena Square Shopping Mall (Chinese: 诺维娜广场) is a shopping mall in Novena, Singapore. It lies directly above Novena MRT Station. From November 2006, Novena Square Shopping Mall rebranded itself as a brand new sports and lifestyle mall with a new building extension, called the Velocity@Novena Square.


== External links ==
Official website