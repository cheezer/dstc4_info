Nufenen Pass (Italian: Passo della Novena, German: Nufenenpass) (el. 2478 m.) is the second highest mountain pass with a paved road within Switzerland - after Umbrailpass. It lies between the summits of Pizzo Gallina (north) and the Nufenestock (south).
The pass road from Ulrichen in canton of Valais leads to the Bedretto valley in the canton of Ticino, linking Brig to Airolo. It is not the lowest pass between the two valleys, as it is located one kilometre north of a slightly lower unnamed pass at 2,440 metres, which is traversed by a trail.
The pass is of relatively recent construction, having been opened to traffic only since September 1969.
To the east of the top of the pass is the source of the Ticino River. Towards the north is a spectacular view of the Bernese Alps, notably the Finsteraarhorn while there is a view over the Gries Glacier to the south.


== See also ==
List of highest paved roads in Europe
List of mountain passes


== Bibliography ==
Nicola Pfund, Sui passi in bicicletta - Swiss Alpine passes by bicycle, Fontana Edizioni, 2012, p. 54-61. ISBN 978-88-8191-281-0


== Sources and further reading ==
This article incorporates information from the equivalent article in the German Wikipedia, consulted during April 2009.


== External links ==
Cycling Map, Elevation Profile, and Photos
Profile on climbbybike.com