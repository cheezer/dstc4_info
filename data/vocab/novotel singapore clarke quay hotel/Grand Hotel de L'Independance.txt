The Grand Hotel de L'Independance (or Hotel GHI), formerly the Hotel de France, is a hotel in downtown Conakry, Guinea. The hotel was first opened in 1954 as the Hotel de France, with a modernistic design for the time. It was renamed the Grand Hotel de L'Independance when Guinea gained independence in 1958. The hotel was extensively renovated in 1996, operating under management by Novotel until 2013.


== History ==
The hotel was built during the French colonial period in 1953 - 1954 as the first project of the Atelier LWD, with architects Guy Lagneau, Michel Weill and Jean Dimitrijevic. It was sited opposite the mayoral residence. The hotel was originally called the Hotel de France, renamed the Grand Hotel de L'Independance in 1958 when Guinea became independent of France. The hotel was given the Novotel name in 1996, after extensive renovations.


== Structure ==

The original building was a long, seven-storied building, supported by pillars, with a restaurant located in a circular pavilion. The interior and furniture were designed by Charlotte Perriand and Jean Prouvé in 1953. The design of the rooms allowed for natural ventilation. Prouvé created the facade of the restaurant, which consists of screens that swing open to the sea, and Perriand used her experience in humid climates with the interior rooms. Mathieu Matégot undertook the decoration.
After renovation, the Hotel GHI is one of the largest hotels in the country, with 12 floors in the new west wing. It is situated near the Presidential Palace, Guinea National Museum and Palais du Peuple. The hotel has 196 rooms, two restaurants, two bars and five conference rooms used for meetings and seminars. The Cote Jardin restaurant serves international cuisine and Cote Mer caters in French cuisine. The hotel bars are Baffila and Sorro.


== See also ==
List of buildings and structures in Guinea


== External links ==
Official website


== References ==