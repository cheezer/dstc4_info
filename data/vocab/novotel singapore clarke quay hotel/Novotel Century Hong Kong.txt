Novotel Century Hong Kong (Chinese: 世紀香港酒店) is a 4-star hotel in Hong Kong. It is located at 238 Jaffe Road, at the junction of Jaffe Road and Steward Road in Wan Chai.
Novotel Century Hong Kong is a convention and business hotel located close to the Hong Kong Convention and Exhibition Centre. It is equipped with business and meeting facilities, three restaurants, a bar, health club, swimming pool, sauna and a range of leisure facilities.


== History ==
The hotel opened in 1991 and was formerly known as the Century Hong Kong Hotel.
On 1 November 2001, in conjunction with the 10th Anniversary of Century Hong Kong Hotel, the Hotel was co-branded to Novotel Century Hong Kong. The new co-branded name signifies the introduction of Accor’s hotel brand Novotel in Hong Kong, following the partnership announcement early in 2001 between Century International Hotels and Accor.


== Facilities ==
The hotel has 511 rooms, 3 restaurants, a bar, meeting and conference facilities, a gym and an outdoor swimming pool as well as a sauna.


== Restaurants and bar ==
Le Cafe 
Pepino Cucina Italiana 
Delicious 
AK's bar + lounge 


== Transportation ==
It takes about 60 minutes from the Hong Kong International Airport to the hotel.
The nearest MTR station is Wan Chai Station.


== Photo Gallery ==


== References ==


== External links ==
Official site
Accorhotels.com site