An army of observation is a military body whose purpose is to monitor a given area or enemy body in preparation for possible hostilities.
Some of the more notable armies of observation include:
Third Reserve Army of Observation, a Russian army tasked to monitor the Austrian border in 1811 prior to the French invasion of Russia
The army of observation at Fort Jesup, Louisiana, United States, which monitored Texas' transition from Spanish to Mexican control, and to eventual independence
The Hanoverian Army of Observation which monitored the border prior to the 1757 Invasion of Hanover


== References ==