An observation post, temporary or fixed, is a position from which soldiers can watch enemy movements, to warn of approaching soldiers (such as in trench warfare), or to direct artillery fire. In strict military terminology, an observation post is any preselected position from which observations are to be made - this may include very temporary installations such as a vehicle parked as a roadside checkpoint, or even an airborne aircraft.


== Operation ==

When selecting a (temporary) observation post, trained troops are to avoid obvious and conspicuous locations such as hilltops, water towers or other isolated terrain features, and to assure that the observation post can be reached via a concealed route. This is especially important as the observer in the post should be rotated every 20–30 minutes, as vigilance decreases markedly after such a time.
Observation posts should be manned with at least two personnel (more, for defense and observer rotation, if the post is to be retained for longer durations), and should be provided a means of communication with their chain of command, preferably by phone instead of by radio.


== Structure ==
Often being positioned in secret very close to the enemy, an observation post is usually a small construct, often consisting largely of camouflage materials and maybe some weather cover. However, where frontlines are expected to be stable for a longer time, an observation post (or ground observation post) may develop into a bunker-like installation.
It is not unusual for soldiers to occupy a 'hide' for long periods of time. To avoid detection they have to remove all their own waste, this is achieved with the aid of clingfilm, plastic bags and empty water bottles.
One example of makeshift observation posts is Gaza Baptist Church, which was commandeered by both Fatah and Hamas troops during the Fatah–Hamas conflict.


== See also ==
Border outpost
Outpost (military)


== References ==