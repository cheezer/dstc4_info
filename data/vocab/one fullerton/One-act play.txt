A one-act play is a play that has only one act, as distinct from plays that occur over several acts. One-act plays may consist of one or more scenes. In recent years, the 10-minute play has emerged as a popular subgenre of the one-act play, especially in writing competitions. The origin of the one-act play may be traced to the very beginning of drama: in ancient Greece, Cyclops, a satyr play by Euripides, is an early example.


== One-act plays by major dramatists ==
Edward Albee -- The Goat, or Who Is Sylvia? (2002)
Samuel Beckett – Krapp's Last Tape (1958)
Anton Chekhov – A Marriage Proposal (1890)
Israel Horovitz – Line (1974)
Eugène Ionesco – The Bald Soprano (1950)
Arthur Miller – A Memory of Two Mondays (1955)
August Strindberg – Pariah (1889), Motherly Love (1892), and The First Warning (1892)
Thornton Wilder – The Long Christmas Dinner (1931)


== See also ==
List of one-act plays by Tennessee Williams
Monodrama
One Act Play Depot


== References ==


== Sources ==
Murray, Stephen. Taking Our Amusements Seriously. LAP, 2010. ISBN 978-3-8383-7608-0.