1 A.D. is a year.
1, one, or ONE may also refer to:
1 (number)
0.999..., a mathematical equivalent of 1

One (pronoun), a pronoun in the English language
1 BC, the year before 1 AD
Hydrogen, with an atomic number of 1
The month of January


== Geography ==
Number One, Kentucky, a community in the United States
Ona, Hordaland (sometimes spelled One) is an island in Øygarden, Norway
L'One, a river that joins the Pique in Bagnères-de-Luchon, France
The number of the French department Ain'
01, the former dialling code for Greater London (later split into 071 and 081 in 1990, then became 0171 and 0181 in 1995, now 020 since 2000)


== Companies ==
One (Telekom Slovenija Group), a Macedonian GSM/UMTS mobile operator
Ubuntu One, an online storage service by Canonical Ltd.
National Express East Anglia, previously branded as one, a train operating company
Orange Austria, previously ONE, an Austrian mobile network operator
One Enterprises, a production company founded by Akir
One NorthEast, the Regional Development Agency for the North East England region
One (Australian TV channel), an Australian television channel
One (Canadian TV channel), a Canadian English-language cable channel
ONE Television, a former Swedish television channel
BBC One, a British Broadcasting Corporation television channel
TV One (New Zealand), a Television New Zealand television channel
Number One Bloor, a high-rise condo in Toronto, Canada


=== Brands ===
Elonex ONE, or simply ONE, a subnotebook computer
HTC One series, a series of Android smartphones produced by Taiwanese smartphone manufacturer HTC
HTC One (2013), the HTC flagship phone released in 2013
HTC One (2014), the HTC flagship phone released in 2014

OnePlus One, the first Android smartphone released by Chinese smartphone manufacturer OnePlus
PSone, a smaller re-branded, original Sony PlayStation console
Purina ONE, a line of pet food products
The Red One (camera), the first camera released by the Red Digital Cinema Camera Company
Xbox One, an eighth generation games console, and the third in the Xbox line, produced by the Microsoft Company


== Firearms ==
Rifle, Number 1, a British rifle
Ruger No. 1, an American rifle


== Organizations ==
Organisation of National Ex-Servicemen (O.N.E.), an organization of veterans of military service in Ireland
ONE Campaign, a campaign to fight global AIDS and poverty
ONE, Inc., a gay rights organization founded in the United States in 1952
ONE National Gay & Lesbian Archives, the largest LGBT archive in the world and a part of the University of Southern California Libraries
Office of National Estimates (ONE), a forerunner of the U. S. National Intelligence Council


== Music ==


=== Songs ===
"1" by Joy Zipper
"#1", by Animal Collective
"No.1", Kiseki (BoA song)
No.1 (Uverworld song)
"Number One" (Helloween song)
"Number One" (Alexia Song)
"Number One" (John Legend song)
"Number One" (Pharrell Williams song)
"Number One" (R. Kelly song)
"Number One" (Skye Sweetnam song)
"Number 1" (Goldfrapp song)
"Number 1" (Nelly song)
"Number 1" (Tinchy Stryder song)
"Numba 1 (Tide Is High)" by Kardinal Offishall
"Number One", by Al Lindsay from the Thrillville: Off the Rails in-game soundtrack
"Number One", by Chas Jankel
"Number One", by Gentle Giant from Civilian
"Number One", by Paramore
"Number One", by Jamie Foxx from Intuition
"Number One", by Max Farenthide
"Number One", by Playgroup
"Number One", by The Rutles
"Number One", by Tadpole from The Buddhafinger
"One" (Ami Suzuki song)
"One" (Bee Gees song)
"One" (Creed song)
"One" (Crystal Kay song)
"One" (Fat Joe song)
"One" (Swedish House Mafia song)
"One" (Harry Nilsson song), also covered by ANBB, Three Dog Night, John Farnham, Aimee Mann and Filter
"One" (JayKo song)
"One" (Metallica song)
"One" (Sky Ferreira song)
"One" (U2 song)
"One (Blake's Got a New Face)", by Vampire Weekend
"One (Always Hardcore)", by Scooter
"One", by Alanis Morissette from Supposed Former Infatuation Junkie
"One", from the musical Bare: A Pop Opera
"One", by Epik High from Pieces, Part One
"One", by Faith Hill from Cry
"One", by Ghostface Killah from Supreme Clientele
"One", by Immortal Technique from Revolutionary Vol. 2
"One", by Kumi Koda from Grow into One
"One", by Lady Gaga
"One!", by Misia from Love Is the Message
"One", by Rip Slyme from Tokyo Classic
"One", by Simple Plan from Still Not Getting Any...
"One", by Zion I from Mind Over Matter
"One", from the musical A Chorus Line
"One", by Parachute Band from Roadmaps and Revelations
"One", by Rapture Ruckus from Live at Worlds End
"One", by Soulfly from 3
"One", by Sunny Day Real Estate from The Rising Tide
"One" by Shapeshifter from Soulstice
"O.N.E.", by Yeasayer from Odd Blood
"Number One (My Chemical Romance single)"


=== EPs ===
One (Alli Rogers EP)
One (Angela Aki EP)
One, by The Brian Jonestown Massacre
One (Charlotte Church EP)
One (Hillsong United EP)
One (Ra EP)
One (Tying Tiffany EP)
The Beatles (No. 1), by The Beatles, also known as simply No. 1
One, by Debby and the Never Ending
#1 (EP), by Suburban Kids with Biblical Names


=== Albums ===
1 (The Beatles album)
1 (The Black Heart Procession album)
1 (Pole album)
#1 (Demy album)
#1 (Fischerspooner album)
One (Angela Aki album)
One (Arashi album)
One (Bee Gees album)
One (Bob James album)
One (Bonnie Pink album)
One, by C418
One (Crown City Rockers album)
One (Cursed album)
One (Dirty Vegas album)
One (Edita Abdieski album)
One (George Jones and Tammy Wynette album)
One (Ida Corr album)
One (Matthew Shipp album)
One (Me Phi Me album)
One (Neal Morse album)
One (NoMeansNo album)
One (Planetshakers album)
One (Sister2Sister album)
One (TesseracT album)
One, an alternate title for the self-titled Three Dog Night
One (Yuval Ron album)
(ONe), by The Panic Channel
One (Hellbound), by Demiricous
One, by Sarah Burgess
ONE, a Ministry of Sound various artists compilation
n.1 (David Carreira album)
No. 1 (BoA album)
No. 1 (Teen Top album)
Number 1 (Big Bang album)
Number 1 (Linda Király album)
Number 1 (O-Zone album)
Number One (M Trill album)
Number One (Pist.On album)
Number One (Billie Godfrey album)
Number One (Racine album)
#1, by Felix
01 (Son of Dave album)
'01 (Richard Müller album)
Ones (Selena album)


=== Artists ===
One (band), from Cyprus
#1, the pseudonym of American drummer Joey Jordison, when performing with the band Slipknot


=== Other ===
One (opera), a chamber opera by Michel van der Aa
Number One (video), the first DVD video release by Greek singer Elena Paparizou
Number One (magazine), a UK music magazine
Number One, a guitar played by Stevie Ray Vaughan
No. 1 (or any of the variants), the top spot, or a song or album reaching the top spot, on any record chart
Numero Uno (company), Italian record-producing company, formerly a subsidiary of RCA Records


== Entertainment ==


=== Literature ===
One (Conrad Williams novel), the 2010 British Fantasy Award winner for Best Novel
One (David Karp novel)
One (Richard Bach novel), a 1988 novel by Richard Bach
Number One (Artemis Fowl), a demon character in the Artemis Fowl novel series
Number 1, Ernst Stavro Blofeld, chief of SPECTRE in James Bond novels and films
Number One, a character in the novel series H.I.V.E. by Mark Walden
Number One (Golgafrinchan), a character from The Hitchhiker's Guide to the Galaxy by Douglas Adams
Number 1, a book by Billy Martin and Peter Golenbock


=== Film ===
1: Nenokkadine, a 2014 Telugu film starring Mahesh Babu
1 (2013 film), a 2013 film about Formula One
1 (2009 film), a 2009 Hungarian film by Pater Sparrow
Number One (1969 film), a film starring Charlton Heston
Number One (1973 film), an Italian language film
Number One (1976 film), a short film by Dyan Cannon
Number One (1985 film), a film starring Bob Geldof as a snooker player
Number One (1994 film), a 1994 Telugu film
Number Ones (TV series), a Canadian music-video program block
ONE: The Movie, a documentary about the meaning of life
1, a character in 9 (2009 film)


=== Television ===
Number One (Babylon 5), a recurring character in the TV series Babylon 5
Number One (Star Trek), the unnamed second-in-command character in the of the pilot episode of Star Trek
"Number One", Captain Picard's frequent way of addressing William Riker in Star Trek: The Next Generation
"Number One", an episode of My Name Is Earl
Number One, John Cavil, a Cylon model in the reimagined version of Battlestar Galactica
Number One (The Prisoner), a character in the television series The Prisoner
"One" (Law & Order: Criminal Intent), an episode of Law & Order: Criminal Intent
"One" (Star Trek: Voyager), an episode of Star Trek: Voyager
One, a Borg drone from the Star Trek: Voyager episode "Drone"
Number One, the head of the Stonecutters in the Simpsons episode "Homer the Great"
Numbuh 1, leader of Sector V, from Codename: Kids Next Door


=== Video games ===
One (video game), a video game released in 1997 for the Sony PlayStation
ONE (N-Gage game), a video game for N-Gage and N-Gage 2.0
One: Kagayaku Kisetsu e, a Japanese video game by Tactics


== Sport ==
Formula One
Armand Oné (born 1983), a French footballer
Pro Wrestling ZERO1-MAX, a wrestling promotion formerly known as Pro Wrestling ZERO-ONE
BAR 01, a Formula One chassi
Division 1 (disambiguation)


== Transportation ==
List of highways numbered 1
List of public transport routes numbered 1
BMW 1 Series


== Other uses ==
Number 1 (painting), by Jackson Pollock
One (Enneagram), a personality type
One (magazine), a homophile magazine published by One, Inc.
Kolmogorov's zero-one law, a law of probability theory
A call sign suffix of any official vehicle used for the transportation of the President of the United States, for example Air Force One


== See also ==
-one (suffix)
The One (disambiguation)
First (disambiguation)
+1 (disambiguation)
Channel One (disambiguation)
World number 1 (disambiguation)
Ones (disambiguation)
Onesie (disambiguation)
OneNote
Category:Number-one singles
All pages beginning with "One"