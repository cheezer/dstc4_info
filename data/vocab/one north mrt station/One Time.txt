One Time may refer to:
"One Time" (song), a song by Canadian singer Justin Bieber
"One Time", a song by King Crimson from their album Thrak, also appearing in their album/DVD Eyes Wide Open
"One Time", a song by rap group Migos
"Rocket Sneaker/One x Time", a song by Japanese artist Ai Otsuka
1Time, a South African airline
One timer, an ice hockey shot
One time, one times, one-time, etc., an American (commonly found in hip hop music) slang term for police officers