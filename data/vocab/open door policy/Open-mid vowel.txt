An open-mid vowel (also mid-open vowel, low-mid vowel, mid-low vowel or half-open vowel) is any in a class of vowel sound used in some spoken languages. The defining characteristic of an open-mid vowel is that the tongue is positioned one third of the way from an open vowel to a close vowel.


== Partial list ==
The open-mid vowels that have dedicated symbols in the International Phonetic Alphabet are:
open-mid front unrounded vowel [ɛ]
open-mid front rounded vowel [œ]
open-mid central unrounded vowel [ɜ] (older publications may use ⟨ɛ̈⟩)
open-mid central rounded vowel [ɞ] (older publications may use ⟨ɔ̈⟩)
open-mid back unrounded vowel [ʌ]
open-mid back rounded vowel [ɔ]
Other near-open vowels can be indicated with diacritics of relative articulation applied to letters for neighboring vowels.