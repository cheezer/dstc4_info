Open-pit or open-cast mining is a surface mining technique of extracting rock or minerals from the earth by their removal from an open pit or borrow.
This form of mining differs from extractive methods that require tunneling into the earth, such as long wall mining. Open-pit mines are used when deposits of commercially useful minerals or rocks are found near the surface; that is, where the overburden (surface material covering the valuable deposit) is relatively thin or the material of interest is structurally unsuitable for tunneling (as would be the case for sand, cinder, and gravel). For minerals that occur deep below the surface—where the overburden is thick or the mineral occurs as veins in hard rock—underground mining methods extract the valued material.
Open-pit mines that produce building materials and dimension stone are commonly referred to as "quarries."
Open-pit mines are typically enlarged until either the mineral resource is exhausted, or an increasing ratio of overburden to ore makes further mining uneconomic. When this occurs, the exhausted mines are sometimes converted to landfills for disposal of solid wastes. However, some form of water control is usually required to keep the mine pit from becoming a lake, if the mine is situated in a climate of considerable precipitation or if any layers of the pit forming the mine border productive aquifers.


== Extraction ==

Open-cast mines are dug on benches, which describe vertical levels of the hole. These benches are usually on four to sixty meter intervals, depending on the size of the machinery that is being used. Many quarries do not use benches, as they are usually shallow.
Most walls of the pit are generally dug on an angle less than vertical, to prevent and minimize damage and danger from rock falls. This depends on how weathered the rocks are, and the type of rock, and also how many structural weaknesses occur within the rocks, such as a faults, shears, joints or foliations.
The walls are stepped. The inclined section of the wall is known as the batter, and the flat part of the step is known as the bench or berm. The steps in the walls help prevent rock falls continuing down the entire face of the wall. In some instances additional ground support is required and rock bolts, cable bolts and shotcrete are used. De-watering bores may be used to relieve water pressure by drilling horizontally into the wall, which is often enough to cause failures in the wall by itself.
A haul road is usually situated at the side of the pit, forming a ramp up which trucks can drive, carrying ore and waste rock.
Waste rock is piled up at the surface, near the edge of the open pit. This is known as the waste dump. The waste dump is also tiered and stepped, to minimize degradation.
Ore which has been processed is known as tailings, and is generally a slurry. This is pumped to a tailings dam or settling pond, where the water evaporates. Tailings dams can often be toxic due to the presence of unextracted sulfide minerals, some forms of toxic minerals in the gangue, and often cyanide which is used to treat gold ore via the cyanide leach process. This toxicity can harm the surrounding environment.


== Rehabilitation ==

After mining finishes, the mine area must undergo rehabilitation. Waste dumps are contoured to flatten them out, to further stabilise them. If the ore contains sulfides it is usually covered with a layer of clay to prevent access of rain and oxygen from the air, which can oxidise the sulfides to produce sulfuric acid, a phenomenon known as acid mine drainage. This is then generally covered with soil, and vegetation is planted to help consolidate the material. Eventually this layer will erode, but it is generally hoped that the rate of leaching or acid will be slowed by the cover such that the environment can handle the load of acid and associated heavy metals. There are no long term studies on the success of these covers due to the relatively short time in which large scale open pit mining has existed. It may take hundreds to thousands of years for some waste dumps to become "acid neutral" and stop leaching to the environment. The dumps are usually fenced off to prevent livestock denuding them of vegetation. The open pit is then surrounded with a fence, to prevent access, and it generally eventually fills up with ground water. In arid areas it may not fill due to deep groundwater levels.


== Typical open cut grades ==
Gold is generally extracted in open-pit mines at 1 to 2 ppm (parts per million) but in certain cases, 0.75 ppm gold is economical. This was achieved by bulk heap leaching at the Peak Hill mine in western New South Wales, near Dubbo, Australia.
Nickel, generally as laterite, is extracted via open-pit down to 0.2%. Copper is extracted at grades as low as 0.15% to 0.2%, generally in massive open-pit mines in Chile, where the size of the resources and favorable metallurgy allows economies of scale.
Materials typically extracted from open-pit mines include:
Bitumen
Clay
Coal
Copper
Coquina
Diamonds
Gravel and stone (stone refers to bedrock, while gravel is unconsolidated material)
Granite
Gritstone
Gypsum
Limestone
Marble
Metal ores, such as copper, iron, gold, silver and molybdenum
Uranium
Phosphate


== See also ==
List of open-pit mines
Closure problem applied to open-pit mines for optimal extraction (not related to closing the mine)
Cut-and-cover tunneling
Cut (earthmoving)
Trench


== References ==


== External links ==
The biggest, deepest and deadliest mines in the world
Deepest open pit mines