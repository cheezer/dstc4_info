The Ophir Awards (Hebrew: פרס אופיר‎), also the Israeli Oscars, are film awards awarded by the Israeli Academy of Film and Television to recognize excellence of professionals in the film industry. They are named after actor Shaike Ophir and first given out in 1982.
The first Israeli Academy Awards ceremony was held in 1982 with the first award being presented to director Shimon Dotan for the film Repeat Dive, and since 1990 has been held annually at the Tel Aviv Performing Arts Center. The annual Best Film Award winner represents Israel at the American Academy Awards (Exceptions include Aviva My Love and The Band's Visit).
The highest number of Ophir Awards won by a single film is 11, achieved only by Nina's Tragedies. Assi Dayan won the award 8 times and is the only person to have won as a director, as a screenwriter and also as an actor.
The winner of the Best Film award becomes Israel's submission for the Academy Award for Best Foreign Language Film.


== List of Best Film Award winners ==
1990: The Lookout (Shuroo)
1991: Beyond the Sea
1992: Life According to Agfa
1993: Revenge of Itzik Finkelstein
1994: Sh'Chur
1995: Lovesick on Nana Street
1996: Saint Clara
1997: Pick a Card
1998: Circus Palestine
1999: Yana's Friends
2000: Time of Favor
2001: Late Marriage
2002: Broken Wings
2003: Nina's Tragedies
2004: Campfire
2005: What a Wonderful Place
2006: Aviva My Love and Sweet Mud
2007: The Band's Visit
2008: Waltz with Bashir
2009: Ajami
2010: The Human Resources Manager
2011: Footnote
2012: Fill the Void
2013: Bethlehem
2014: Gett: The Trial of Viviane Amsalem


== See also ==
Cinema of Israel
Israeli Academy of Film and Television
List of Israeli submissions for the Academy Award for Best Foreign Language Film


== References ==


== External links ==
Official site of the Israeli Film and Television Academy
Info on IMDB.com