RMS Ophir was a twin-screw ocean liner of the Orient Steam Navigation Company of London, which worked company's London — Aden — Colombo — Australia route from 1891. In 1901 she served as the Royal yacht HMS Ophir. In 1915 she was requisitioned by the Admiralty and was an armed merchant cruiser until 1918, when she was returned to her owners. She was not restored to passenger service, but was scrapped in 1922.


== History ==
One appreciative passenger was "the Welsh Swagman" Joseph Jenkins who embarked at Melbourne on 24 November 1894, bound for Tilbury Docks in a second-class cabin at the fare of £26 15s 6d. When he first saw the vessel, it appeared so huge that he wrote "it is a wonder to me that it would move". Jenkins, a noted diarist, proceeded to record in detail the 103-day voyage passing through the new Suez Canal.

In 1901, as HMS Ophir, she took the Duke and Duchess of Cornwall and York (the future King George V and Queen Mary) on their tour of the British Empire. The visit was scheduled to open the new Federal Parliament in Melbourne, Australia, but the royal party also visited Gibraltar, Malta, Ceylon, the Straits Settlements, New Zealand, South Africa and Canada. The Admiralty provided crew for the tour, while the engine-room staff came from the Orient Company´s own engineers.

A petty officer named Harry Price was with the tour from February to November 1901, and made a careful record, later published as The Royal Tour 1901, or the Cruise of H.M.S. Ophir; Being a Lower Deck Account of their Royal Highnesses, The Duke and Duchess of Cornwall and York's Voyage Around the British Empire. The 1901 cruise was also filmed by CPO McGregor working for AJ West's 'Our Navy' company and cinematograph film and lantern slides of the cruise were shown to the British Royal Family and staff at Sandringham on November 9, 1901.
On the completion of the royal tour, Ophir was paid off at Tilbury Docks 6 November 1901.


== References ==


== External links ==
Shipping News monograph with photo
Interior gallery and saloon at English Heritage Images
Cinematographic presentation of the Ophir voyage at Sandringham
'Our Navy' film of the Ophir Cruise shown by Alfred J West by Royal Command at Sandringham