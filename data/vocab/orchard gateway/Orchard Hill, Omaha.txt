The Orchard Hill is a neighborhood in North Omaha, Nebraska. One of the oldest neighborhoods in the city, Orchard Hill is home to some of the city's oldest homes. The neighborhood's boundaries are Hamilton Street on the south, Blondo Street on the north, 36th Street on the east and the Omaha Belt Line on the west. The John A. Creighton Boulevard runs through the eastern part of the neighborhood.


== History ==
The site of an apple orchard from earliest settlement of the city of Omaha, today the Orchard Hill Neighborhood contains two historic homes that are almost 120 years old. The oldest was built in 1889, and the second oldest was built two years later, in 1891. Jesse Lowe, a local businessman credited with naming the city of Omaha and the first mayor of Omaha, established the neighborhood. The neighborhood was the location of a Methodist mission called Wesley Church that was founded in 1890. Omaha's Habitat for Humanity has built several new homes in the neighborhood since the 1980s.
The neighborhood is home to City Sprouts, the oldest community garden in Omaha. Orchard Hill is located next to the Walnut Hill neighborhood.


== See also ==
History of Omaha
Neighborhoods in Omaha, Nebraska


== References ==