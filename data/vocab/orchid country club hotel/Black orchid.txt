Black orchid or Black Orchid may refer to:


== Plants ==
Any member of several species of genus Bulbophyllum commonly called "Black orchid"
Calanthe triplicata, the Australian Black orchid
Coelogyne pandurata (Black orchid), a species of orchid native to Borneo
Cymbidiella falcigera (Black orchid), a species of orchid native to Madagascar and Île aux Nattes
Cymbidiella humblotii (Black orchid), another Madagascar orchid species
Cymbidium canaliculatum (Queensland black orchid), a species of orchid native to northern and eastern Australia
Fredclarkeara (Truly black orchid), an intergeneric hybrid created by Fred Clarke of sunset valley orchids
Liparis nervosa (koku-ran コクラン, 黒蘭, "Black orchid"), a species of orchid native to Japan and China
Masdevallia rolfeana (Black orchid), a species of orchid native to Central America
Oncidium henckenii (Black orchid), a species of orchid
Pleurothallis immersa (Black orchid), also known as Specklinia immersa, a species of orchid ranging from Mexico to Venezuela
Prosthechea cochleata (Black orchid), the national flower of Belize that is also known as the Clamshell (or Cockleshell) orchid
Trichoglottis phillipinensis var. brachiata (Black orchid), a species of orchid natide to the Philippines
Black Orchid may also refer to:


== Books and comics ==
Black Orchid (comics), a character from DC Comics
Black Orchid (Doctor Who), a Doctor Who serial
Black Orchids, a Nero Wolfe double mystery by Rex Stout
Black Orchid (Killer Instinct), a character in Killer Instinct video game series


== Film ==
Black Orchid (1953 film), the 1953 film with Ronald Howard
The Black Orchid (1958 film), the 1958 film with Sophia Loren and Anthony Quinn


== Music ==
The Black Orchid, Chicago night club that existed from 1949 to 1959
Black Orchid (band), an Australian gothic metal band


=== Albums ===
Black Orchid (album), 1962 jazz album by The Three Sounds
Black Orchid, 1978 Hawaiian album by Peter Moon Band


=== Songs ===
"Black Orchid", a song from the album Stevie Wonder's Journey Through "The Secret Life of Plants"
"Black Orchid", a jazz tune written by Neal Hefti


== See also ==
Bouquet of Black Orchids, a compilation album by The Tear Garden