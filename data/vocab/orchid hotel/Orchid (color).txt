Orchid is a bright rich purple color that is a representation of the color of the orchidaceae flower.
Various tones of orchid may range from grayish purple to purplish-pink to strong reddish purple.
The first recorded use of orchid as a color name in English was in 1915.
In 1987, orchid was included as one of the X11 colors. After the invention of the world wide web in 1991, these became known as the X11 web colors.


== Derivation ==
The name originates from the flowers of some species of the vast orchid family (Orchidaceae), such as Laelia furfuracea and Ascocentrum pusillum, which have petals of this color.
The word Orchid derives from the Greek word orchis which means testicle, after the appearance of the roots of plants of the genus Orchis.


== Variations of orchid ==


=== Orchid pink ===
Displayed at right is the color orchid pink.
The source of this color is the "Pantone Textile Paper eXtended (TPX)" color list, color #13-2010 TPX—Orchid Pink.


=== Wild orchid ===
Displayed at right is the color wild orchid.
The source of this color is the "Pantone Textile Paper eXtended (TPX)" color list, color #16-2120 TPX—Wild Orchid.


=== Dark orchid ===

Displayed at right is the web color dark orchid.


== Orchid in human culture ==
Art
In the Crayola brand of markers, desert flower is a color equivalent to orchid.
Medicine
Orchid is a British charity which funds research into the diagnosis, prevention and treatment of prostate, penile and testicular cancer. It was set up in 1996 by former testicular cancer sufferer, Colin Osborne. The prostate, penile, and testicular cancer awareness ribbon is represented by the color orchid.


== See also ==
List of colors
Orchid


== References ==