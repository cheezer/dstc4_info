Wild Orchid may refer to:
Wild orchid, a noncultivated orchid
Wild Orchids (film), a 1929 MGM film starring Greta Garbo
Wild Orchid (film), a 1989 film starring Mickey Rourke and Carré Otis
Wild Orchid (band), a vocal trio that featured Stacy Ferguson, later of the Black Eyed Peas
Wild Orchid (album), the debut album by the band

Wild Orchids (album), a 2006 album by Steve Hackett