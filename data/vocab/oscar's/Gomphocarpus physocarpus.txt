Gomphocarpus physocarpus, commonly known as balloonplant, balloon cotton-bush, bishop's balls or swan plant, is a species of milkweed. The plant is native to southeast Africa, but it has been widely naturalized. It is often used as an ornamental plant. The name "balloonplant" is an allusion to the swelling bladder-like follicles which are full of seeds.


== Description ==

Gomphocarpus physocarpus is an undershrub perennial herb, that can grow to over six feet. The plant blooms in warm months. It grows on roadside banks, at elevations of 2800 to 5000 feet above sea level. The plant prefers moderate moisture, as well as sandy and well-drained soil and full sun.
The flowers are small, with white hoods and about 1 cm across. The follicle is a pale green, and in shape an inflated spheroid. It is covered with rough hairs. It reaches three inches in diameter. The leaves are light green, linear to lanceolate and 3 to 4 inches long, 1.2 cm broad. The brown seeds have silky tufts.
This plant will readily hybridize with Gomphocarpus fruticosus creating intermediate forms.


== Butterflies ==
Gomphocarpus physocarpus is a food of the caterpillars of Danaus butterflies, including the Monarch butterfly. This plant is also popular in traditional medicine to cure various ailments.


== References ==


== External links ==
Asclepias physocarpa photos
USDA Plants Profile