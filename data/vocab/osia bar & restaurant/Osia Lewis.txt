Osia Lewis (born December 3, 1962) is a former American football player who played four seasons in the Arena Football League with the Chicago Bruisers and Albany Firebirds. He played college football at Oregon State University and attended Tucson High School in Tucson, Arizona.


== College career ==
Lewis was a four-year letterman for the Oregon State Beavers from 1982 to 1985. He primarily played linebacker but also saw time at quarterback, safety and wide receiver for the Beavers. As a senior in 1985, he was named the team's Most Inspirational Player, served as team captain and earned All-Pac-10 and honorable mention All-America honors by the Associated Press. Lewis set school records for single-season defensive points, season fumble recoveries with four and career fumble recoveries with eight.


== Professional career ==


=== Chicago Bruisers ===
Lewis played for the Chicago Bruisers from 1987 to 1989, earning Second Team All-Arena honors in 1988.


=== Albany Firebirds ===
Lewis played for he Albany Firebirds in 1990.


== Coaching career ==


=== Western Oregon State College ===
Lewis was as assistant coach for the Western Oregon Wolves from 1989 to 1990.


=== Oregon State University ===
Lewis served as an assistant coach for the Oregon State Beavers working with linebackers and special teams from 1991 to 1996.


=== University of Illinois at Urbana–Champaign ===
Lewis was defensive line coach for the Illinois Fighting Illini from 1997 to 2000 before serving as linebackers coach from 2001 to 2002.


=== University of New Mexico ===
Lewis was defensive coordinator of the New Mexico Lobos from 2003 to 2007 while also serving stints as defensive line and linebacker coach.


=== University of Texas at El Paso ===
Lewis served as defensive coordinator of the UTEP Miners from 2008 to 2009.


=== Hartford Colonials ===
Lewis was defensive coordinator of the Hartford Colonials of the United Football League in 2010.


=== San Diego State University ===
Lewis has served as defensive line coach of the San Diego State Aztecs since 2011.


== References ==


== External links ==
Just Sports Stats