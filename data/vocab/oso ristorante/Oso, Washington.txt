Oso is a census-designated place (CDP) in Snohomish County, Washington, United States located west of Darrington, Washington, south of the North Fork of the Stillaguamish River and approximately 50 air miles from Seattle. The population of Oso was 180 at the 2010 census. The community made international news as the site of a large landslide in March 2014.


== Geography ==
Oso is located at 48°16′20″N 121°55′9″W (48.272281, -121.919235).
According to the United States Census Bureau, the CDP has a total area of 3.7 square miles (9.6 km²), of which, 3.6 square miles (9.4 km²) of it is land and 0.1 square miles (0.2 km²) of it (2.43%) is water.


== History ==
The town was originally named Allen, but was renamed to Oso to avoid confusion with Allyn in Mason County. Oso is Spanish for bear and was named by J. P. Britzius after the town of Oso, Texas in Fayette County. The Texas town no longer exists, having been abandoned in favor of the nearby town of Flatonia which had a railway station.


== Demographics ==
As of the census of 2000, there were 246 people, 96 households, and 69 families residing in the CDP. The population density was 68.0 people per square mile (26.2/km²). There were 102 housing units at an average density of 28.2/sq mi (10.9/km²). The racial makeup of the CDP was 99.19% White, 0.41% African American, and 0.41% from other races. Hispanic or Latino of any race were 0.41% of the population.
There were 96 households out of which 37.5% had children under the age of 18 living with them, 60.4% were married couples living together, 8.3% had a female householder with no husband present, and 28.1% were non-families. 22.9% of all households were made up of individuals and 11.5% had someone living alone who was 65 years of age or older. The average household size was 2.56 and the average family size was 3.01.
In the CDP the age distribution of the population shows 28.5% under the age of 18, 5.7% from 18 to 24, 27.2% from 25 to 44, 24.4% from 45 to 64, and 14.2% who were 65 years of age or older. The median age was 37 years. For every 100 females there were 95.2 males. For every 100 females age 18 and over, there were 95.6 males.
The median income for a household in the CDP was $75,315, and the median income for a family was $78,786. Males had a median income of $39,489 versus $21,250 for females. The per capita income for the CDP was $23,700. None of the population or families were below the poverty line.


== 2014 mudslide ==

On Saturday, March 22, 2014, at 10:37 a.m. local time, a major mudflow occurred 4 miles (6.4 km) east of Oso, when a portion of an unstable hill collapsed, sending mud and debris across the North Fork of the Stillaguamish River, engulfing a rural neighborhood, and covering an area of approximately 1 square mile (2.6 km2). Forty-three people were killed.


== Points of interest ==
Jim Creek Naval Radio Station


== References ==
^ a b "American FactFinder". United States Census Bureau. Retrieved 2008-01-31. 
^ "US Board on Geographic Names". United States Geological Survey. 2007-10-25. Retrieved 2008-01-31. 
^ "Oso Census Designated Place: United States , geographic coordinates, administrative division, map". Geographical Names. Retrieved 2014-03-28. 
^ "Oso: United States , geographic coordinates, administrative division, map". Geographical Names. Retrieved 2014-03-28. 
^ "US Gazetteer files: 2010, 2000, and 1990". United States Census Bureau. 2011-02-12. Retrieved 2011-04-23. 
^ "Oso". Washington Place Names database. Tacoma Public Library. Retrieved 2009-03-06. 
^ "Some Mysteries About Grandma Quick's Grave", Tuesday, January 9, 1973, Dallas Morning News (Dallas, Texas), p. 15.
^ Snohomish County Medical Examiner's Office (July 23, 2014). "Snohomish County Medical Examiner's Office Media Update". Retrieved July 27, 2014.