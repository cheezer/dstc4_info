The 2007 College Football All-America Team is composed of the following All-American Teams: Associated Press, Football Writers Association of America, American Football Coaches Association, Walter Camp Foundation, The Sporting News, Sports Illustrated, Pro Football Weekly, ESPN, CBS Sports, College Football News, Rivals.com, and Scout.com. 
The College Football All-America Team is an honor given annually to the best American college football players at their respective positions. The original usage of the term All-America seems to have been to such a list selected by football pioneer Walter Camp in the 1890s. The NCAA officially recognizes All-Americans selected by the AP, AFCA, FWAA, TSN, and the WCFF to determine Consensus and Unanimous All-Americans.


== Offense ==


=== Quarterback ===
Tim Tebow, Florida (Associated Press, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Matt Ryan, Boston College (AFCA-Coaches, Pro Football Weekly)


=== Running back ===
Darren McFadden, Arkansas (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, Pro Football Weekly, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Kevin Smith, Central Florida (Associated Press, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, ESPN, CBS Sports, College Football News, Scout.com)
Jonathan Stewart, Oregon (AFCA-Coaches)
Rashard Mendenhall, Illinois (Pro Football Weekly)
Ray Rice, Rutgers (Rivals.com)


=== Fullback ===
Brannan Southerland, Georgia (Pro Football Weekly)


=== Wide receiver ===
Michael Crabtree, Texas Tech (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Jordy Nelson, Kansas State (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sports Illustrated, Pro Football Weekly, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Percy Harvin, Florida (Sporting News)
Jeremy Maclin, Missouri (Pro Football Weekly)


=== Tight end ===
Martin Rucker, Missouri (Associated Press, AFCA-Coaches, FWAA-Writers, ESPN, CBS Sports, Scout.com)
Travis Beckum, Wisconsin (Walter Camp, Sports Illustrated, College Football News, Rivals.com)
Fred Davis, Southern California (Sporting News)
Brandon Pettigrew, Oklahoma State (Pro Football Weekly)


=== Tackles ===
Jake Long, Michigan (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, Pro Football Weekly, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Anthony Collins, Kansas (Associated Press, FWAA-Writers, Walter Camp, Sports Illustrated, CBS Sports, College Football News, Rivals.com)
Kirk Barton, Ohio State (AFCA-Coaches, ESPN)
Sam Baker, Southern California (Walter Camp)
Alex Mack, California (Sporting News)
Max Unger, Oregon (Sports Illustrated)
Ryan Clady, Boise State (AFCA-Coaches, Sporting News, Scout.com)
Jeff Otah, Pittsburgh (Pro Football Weekly)
Barry Richardson, Clemson (College Football News)


=== Guards ===
Duke Robinson, Oklahoma (Associated Press, AFCA-Coaches, Sporting News, Sports Illustrated, Pro Football Weekly, ESPN, College Football News, Rivals.com, Scout.com)
Martin O'Donnell, Illinois (Associated Press, Rivals.com, Scout.com)
Hercules Satele, Hawaii (FWAA-Writers)
Ryan Stanchek, West Virginia (FWAA-Writers)
Tony Hills, Texas (Walter Camp)
Roy Schuening, Oregon State (Pro Football Weekly, ESPN)
Gosder Cherilus, Boston College (CBS Sports)


=== Center ===
Jonathan Luigs, Arkansas (FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, CBS Sports, College Football News, Rivals.com, Scout.com)
Steve Justice, Wake Forest (Associated Press, AFCA-Coaches,Pro Football Weekly, ESPN, CBS Sports)


== Defense ==


=== Ends ===
Chris Long, Virginia (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, Pro Football Weekly, ESPN, CBS Sports, Rivals.com, Scout.com)
George Selvie, South Florida (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sports Illustrated, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Greg Middleton, Indiana (Sporting News, CBS Sports, College Football News)
Vernon Gholston, Ohio State (Pro Football Weekly)


=== Tackles ===
Glenn Dorsey, LSU (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, Pro Football Weekly, College Football News, Rivals.com, Scout.com)
Sedrick Ellis, Southern California (Associated Press, AFCA-Coaches FWAA-Writers, Walter Camp Sporting News, Sports Illustrated, Pro Football Weekly, ESPN, College Football News, Rivals.com, Scout.com)


=== Linebackers ===
James Laurinaitis, Ohio State (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, Pro Football Weekly, ESPN, CBS Sports, Rivals.com, Scout.com)
Dan Connor, Penn State (Associated Press, Walter Camp, ESPN, College Football News, Rivals.com, Scout.com)
Jordon Dizon, Colorado (Associated Press, Walter Camp, Sporting News, ESPN, College Football News, Rivals.com)
J Leman, Illinois (AFCA-Coaches, FWAA-Writers, ESPN, CBS Sports)
Curtis Lofton, Oklahoma (FWAA-Writers, Sporting News, Sports Illustrated, College Football News)
Xavier Adibi, Virginia Tech (AFCA-Coaches)
Keith Rivers, Southern California (Sports Illustrated, Pro Football Weekly)
Scott McKillop, Pittsburgh (Scout.com)
Sean Lee, Penn State (Pro Football Weekly)
Ali Highsmith, LSU (CBS Sports)


=== Cornerback ===
Aqib Talib, Kansas (Associated Press, AFCA-Coaches, FWAA-Writers, Walter Camp, Sporting News, Sports Illustrated, ESPN, CBS Sports, College Football News, Rivals.com)
Antoine Cason, Arizona (Associated Press, Walter Camp, Sporting News, Sports Illustrated, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Dwight Lowery, San Jose State (AFCA-Coaches)
Brandon Flowers, Virginia Tech (AFCA-Coaches)
Mike Mickens, Cincinnati (FWAA-Writers)
Alphonso Smith, Wake Forest (Scout.com)
Leodis McKelvin, Troy (Pro Football Weekly)
Malcolm Jenkins, Ohio State (Pro Football Weekly)


=== Safety ===
Craig Steltz, LSU (Associated Press, FWAA-Writers, Walter Camp, Sports Illustrated, ESPN, CBS Sports, College Football News, Rivals.com, Scout.com)
Jamie Silva, Boston College (Associated Press, FWAA-Writers, Walter Camp, Sports Illustrated, CBS Sports, College Football News, Scout.com)
Mike Jenkins, South Florida (AFCA-Coaches)
Taylor Mays, Southern California (Sporting News)
Chris Horton, UCLA (Sporting News)
William Moore, Missouri (Pro Football Weekly, Rivals.com)
Nic Harris, Oklahoma (Pro Football Weekly)
Marcus Griffin, Texas (ESPN)


== Special teams ==


=== Kicker ===
Thomas Weber, Arizona State (Associated Press, CBS Sports, College Football News, Scout.com)
Taylor Mehlhaff, Wisconsin (AFCA-Coaches)
Daniel Lincoln, Tennessee (FWAA-Writers)
John Sullivan, New Mexico (Walter Camp, Sporting News, ESPN)
Austin Starr, Indiana (Sports Illustrated)
Steven Hauschka, North Carolina State (Pro Football Weekly)
Louie Sakoda, Utah (Rivals.com)


=== Punter ===
Kevin Huber, Cincinnati (Associated Press, Walter Camp, Sporting News, Scout.com)
Chris Miller, Ball State (AFCA-Coaches)
Louie Sakoda, Utah (FWAA-Writers, CBS Sports)
Durant Brooks, Georgia Tech (Sports Illustrated, Pro Football Weekly, ESPN, College Football News, Rivals.com)


=== All-purpose player / return specialist ===
Jeremy Maclin, Missouri (Associated Press, FWAA-Writers, Sporting News-KR, Sports Illustrated-PR, ESPN-KR, CBS Sports-KR, Rivals.com, Scout.com)
DeSean Jackson, California (AFCA-Coaches)
Felix Jones, Arkansas (Walter Camp-Returner, Rivals.com-KR, Scout.com-PR)
Leodis McKelvin, Troy (Sporting News-PR, Rivals.com-PR, Pro Football Weekly-PR, Scout.com-KR)
Matthew Slater, UCLA (Sports Illustrated-KR)
Chris Johnson, East Carolina (Pro Football Weekly)
Kevin Robinson, Utah State (CBS Sports, College Football News-PR)
A. J. Jefferson, Fresno State (College Football News)


== References ==
AFCA Coaches
Associated Press
CBS Sports
College Football News
ESPN
Football Writers Association of America (FWAA)
Pro Football Weekly
Rivals
Scout.com
Sports Illustrated
Sporting News
Walter Camp


== Notes ==