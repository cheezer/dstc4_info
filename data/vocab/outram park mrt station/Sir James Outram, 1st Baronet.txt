Lieutenant General Sir James Outram, 1st Baronet GCB, KCSI (29 January 1803 – 11 March 1863) was an English general who fought in the Indian Rebellion of 1857.


== Early life ==
James Outram was the son of Benjamin Outram of Butterley Hall, Butterley, Derbyshire, a civil engineer. His father died in 1805, and his mother, a daughter of James Anderson of Hermiston, the Scottish writer on agriculture, moved to Aberdeenshire in 1810. From Udny school the boy went in 1818 to the Marischal College, Aberdeen and in 1819 an Indian cadetship was given to him. Soon after his arrival at Bombay his remarkable energy attracted notice, and in July 1820 he became acting adjutant to the first battalion of the 12th regiment on its embodiment at Poona, an experience which he found to be of immense advantage to him later in his career.


== Khandesh - 1825 ==

In 1825, he was sent to Khandesh, where he trained a light infantry corps, formed of the Bhils, a tribe native to the densely forested hills of that region. He gained over them a marvellous personal influence, and employed them with great success in checking outrages and plunder. Their loyalty to him had its principal source in their admiration of his hunting achievements, which in cool daring and hairbreadth escapes have perhaps never been equalled. Originally a puny lad, and for many years after his arrival in India subject to constant attacks of sickness, Outram seemed to gain strength by every new illness, eventually acquiring a strong constitution and "nerves of steel, shoulders and muscles worthy of a six-foot Highlander."


== Gujarat and the North-West ==
In 1835 he was sent to Gujarat to make a report on the Mahi Kantha district, and for some time he remained there as political agent. On the outbreak of the First Afghan War in 1838 he was appointed extra aide-de-camp on the staff of Sir John Keane, and went to Afghanistan, where he conducting various raids against Afghan tribes and performed an extraordinary exploit in capturing a banner of the enemy before Ghazni. In 1839, he was promoted to Major and appointed political agent in Lower Sindh, later being moved to Upper Sindh (at this time, Gujarat and Sindh were both under the Bombay Presidency). While in Sindh, he strongly opposed the policy of his superior, Sir Charles Napier, which led to the annexation of Sind. However, when war broke out, he heroically defended the residency at Hyderabad against 8000 Baluchis, causing Sir Charles Napier to describe him as the "Bayard of India." On his return from a short visit to England in 1843, he was, with the rank of brevet lieutenant-colonel, appointed to a command in the Mahratta country, and in 1847 he was transferred from Satara to Baroda, where he incurred the resentment of the Bombay government by his fearless exposure of corruption.


== Lucknow - 1854 ==

In 1854 he was appointed resident at Lucknow, in which capacity two years later he carried out the annexation of Oudh and became the first chief commissioner of that province. Appointed in 1857, with the rank of lieutenant-general, to command an expedition against Persia during the Anglo-Persian War, he defeated the enemy with great slaughter at Khushab, and conducted the campaign with such rapid decision that peace was shortly afterwards concluded, his services being rewarded by the grand cross of the Bath.
From Persia he was summoned in June to India, with the brief explanation "We want all our best men here". It was said of him at this time that a fox is a fool and a lion a coward by the side of Sir J. Outram. Immediately on his arrival in Calcutta he was appointed to command the two divisions of the Bengal army occupying the country from Calcutta to Cawnpore; and to the military control was also joined the commissionership of Oudh. Already hostilities had assumed such proportions as to compel Henry Havelock to fall back on Cawnpore, which he held only with difficulty, although a speedy advance was necessary to save the garrison at Lucknow. On arriving at Cawnpore with reinforcements, Outram, in admiration of the brilliant deeds of General Havelock, conceded to him the glory of relieving Lucknow, and, waiving his rank, tendered his services to him as a volunteer. During the advance he commanded a troop of volunteer cavalry, and performed exploits of great brilliancy at Mangalwar, and in the attack at the Alambagh; and in the final conflict he led the way, charging through a very tempest of fire. The volunteer cavalry unanimously voted him the Victoria Cross, but he refused the choice on the grounds that he was ineligible as the general under whom they served. Resuming supreme command, he then held the town till the arrival of Sir Colin Campbell, after which he conducted the evacuation of the residency so as completely to deceive the enemy. In the second capture of Lucknow, on the commander-in-chief's return, Outram was entrusted with the attack on the side of the Gomti, and afterwards, having recrossed the river, he advanced through the Chattar Manzil to take the residency, thus, in the words of Colin Campbell, putting the finishing stroke on the enemy. After the capture of Lucknow he was gazetted lieutenant-general.


== Thanks - Bayard of India ==
In February 1858, he received the special thanks of both houses of Parliament, and in the same year the dignity of baronet with an annuity of 1000l.. When, on account of shattered health, he returned finally to England in 1860, a movement resulted in the presentation of a public testimonial, and the erection of statues in London (by sculptor Matthew Noble) and Calcutta. He died on 11 March 1863, and was buried in the nave of Westminster Abbey, where the marble slab on his grave bears the poignant epitaph The Bayard of India.


== Legacy ==
Outram Street, West Perth is an street near the King's Park in Perth, Australia, named after Sir James Outram. Two other nearby streets (Colin Street and Havelock Street) are named in honour of Generals concerned in the Indian Mutiny.
Outram, Singapore is an area of the city of Singapore named after Outram Road which was named in Sir James' honour in 1858.
Outram, New Zealand is a small town near Dunedin. It was named after Sir James by Sir John Richardson.
Outram Road in Croydon, south London, is named after Outram. The road is near Addiscombe Military Seminary which trained officers for the East India Company.
General James Outram is played by Richard Attenborough in the 1977 Satyajit Ray film The Chess Players.
The Outram Ghat in Kolkata, West Bengal, India, has been named after General Outram.
Outram Lines, Kingsway Camp, Delhi, India
Outram Road in Southsea, Hampshire, United Kingdom is named for Sir James Outram.


== References ==

Attribution
 This article incorporates text from a publication now in the public domain: Chisholm, Hugh, ed. (1911). "Outram, Sir James". Encyclopædia Britannica (11th ed.). Cambridge University Press. 


=== Additional sources ===
"Dispatch from Major-General R. England" (pdf). London Gazette (London: Stationery Office) (20185): 82. 10 January 1843. Retrieved 14 September 2008. On this occasion also Major Outram gave me his able assistance, as well as in flanking the extremity of the Bolan pass near Kundye, where I had good reason to expect again to meet some hostile tribes, but the total disappointment of the Kakurs on the 3d, and the effectual flanking arrangements made on all occasions by our troops, seem to have prevented any renewal of interruption. 
Also see: Outram, Lieutenant Colonel James. The Conquest of Scinde: A commentary. London: William Blackwood, 1846.
Westminster Abbey: History: Sir James Outram