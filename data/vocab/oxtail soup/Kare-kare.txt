Kare-kare is a Philippine stew. It is made from a base of stewed oxtail (sometimes this is the only meat used), pork hocks, calves feet, pig feet, beef stew meat; and occasionally offal, or tripe. Vegetables, which include (but are not limited to): eggplant, Chinese cabbage, or other greens, daikon, green beans, and asparagus beans are added - usually equaling or exceeding the amount of meats. The stew is flavored with ground roasted peanuts or peanut butter, onions, and garlic. The stew is colored with annatto (extracted from annatto seeds in oil or water), and can be thickened with toasted or plain ground rice. Other flavorings may be added, but the dish is usually quite plain, compared to other Filipino dishes as other seasonings are added at the table. Other variants may include goat meat or (rarely) chicken. It is often eaten with bagoong (shrimp paste), sometimes spiced with chili, bagoong guisado (spiced and sautéed shrimp paste), and sprinkled with calamansi juice. Traditionally, any Filipino fiesta (particularly in Pampanga region) is not complete without kare-kare. In some Filipino-American versions of the dish, oxtail is exclusively used as the meat.


== History ==
As with many things in the Philippines, there are several stories as to the origins of this rather unusual yet distinctly Filipino dish. The first one is that it came from Pampanga. Another, from the regal dishes of the Moro elite who once settled in Manila before the Spanish arrival (interestingly enough, in Sulu and Tawi-Tawi, Kare-kare also remains a popular dish).Another is from Sepoy conscripts that settled in Philippines during the British Occupation of the Philippines.Homesick,they improvise their own cuisine with available materials.They call it kari-kaari,curry,and now,kare-kare.It's a comfort food for Filipinos, and is a perennial family favorite in both local and overseas Filipino households.


== Preparation ==

Oxtail, with the skin on and cut into 2-inch lengths, and ox tripe are boiled until tender. Sometimes pieces of ox feet or shins are added. When the meat is tender, the soup becomes gelatinous and to this is added ground roasted peanuts (or peanut butter), ground roasted glutinous rice to make the soup thicker. Atsuete (annatto) is added to give color. The basic vegetables for kare-kare include young banana flower bud or "heart" (puso ng saging), eggplant, string beans, and Chinese cabbage (pechay). Kare-kare is often served hot with special bagoong alamang (sauteed salted shrimp paste).


== See also ==
Mechado
Adobo
List of stews


== References ==
^ "Kare-Kare: Filipino ox tail stew". 


== External links ==