This is a list of German soups. German cuisine has evolved as a national cuisine through centuries of social and political change with variations from region to region. In Germany, soups are a popular and significant food, and most Germans eat soup at least once daily. In German cuisine, it may be served as a first course or as a main course. The use of a roux to thicken soups is common in German cuisine. The use of legumes and lentils is significant and used in several German soups, such as split pea soup. Common soups in German restaurants include oxtail, beef or chicken broth with noodles, dumplings, or rice, goulash, split pea, cream of asparagus, turtle soup (Echte Schildkrötensuppe) and cream of lobster.
In the 1880s, Germans had an appreciation for soups prepared with beer as a primary ingredient, which was prepared with beer with a lesser alcohol content compared to standard beers. One recipe utilized beer, water, sugar, raisins, spices and grated, stale bread.
This list includes soups that originated in Germany as well as those that are common in the country.


== German soups ==


== In culture ==
The German tale of Suppenkasper involves "a little boy who faded away because he refused to eat his soup".


== See also ==

List of German cheeses
List of German dishes
List of soups
List of stews


== References ==