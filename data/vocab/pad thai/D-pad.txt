A D-pad (short for directional pad; also known as a Control Pad) is a flat, usually thumb-operated four-way directional control with one button on each point, found on nearly all modern video game console gamepads, game controllers, on the remote control units of some television and DVD players, and smart phones. Like early video game joysticks, the vast majority of D-pads are digital; in other words, only the directions provided on the D-pad buttons can be used, with no intermediate values. However, combinations of two directions (up and left, for example) do provide diagonals and many modern D-pads can be used to provide eight-directional input if appropriate.
Although digital D-pads offer less flexibility than analog sticks, they can easily be manipulated (requiring little movement of the thumb) with very high accuracy. They are also far less demanding in maintenance and do not protrude very far from the controller, making them ideal for portable consoles such as the Game Boy and the PSP.
D-pads have appeared on other kinds of electronic equipment, including A/V remote controls (especially since the appearance of DVD players, which are heavily menu driven), calculators, PDAs, smartphones, and car stereos such as the AutoPC.


== History ==
A precursor to the D-pad was the four directional buttons used in arcade games such as UPL's Blockade (1976) and SNK's Vanguard (1981). A precursor to the standard D-pad on a video game console was used by the Intellivision, which was released by Mattel Electronics in 1980. The Intellivision's unique controller featured the first alternative to a joystick on a home console, a circular pad that allowed for 16 directions of movement by pressing it with the thumb. A precursor to the D-pad also appeared on Entex's short lived "Select A Game" cartridge based handheld system; it featured non-connected raised left, right, up and down buttons aligned to the left of a row of action buttons. Similar directional buttons were also used on the Atari Game Brain, the unreleased precursor to the Atari 2600, and on some early dedicated game consoles such as the VideoMaster Star Chess game. A controller similar to the D-pad appeared in 1981 on a handheld game system: Cosmic Hunter on Milton Bradley's Microvision; it was operated using the thumb to manipulate the onscreen character in one of four directions.
The modern "cross" design was developed in 1982 by Nintendo's Gunpei Yokoi for their Donkey Kong handheld game. The design proved to be popular for subsequent Game & Watch titles, although the previously introduced non-connected D-pad style was still utilized on various later Game & Watch titles, including the Super Mario Bros. handheld game. This particular design was patented and later earned a Technology & Engineering Emmy Award. In 1984, the Japanese company "Epoch" created a handheld game system called the "Epoch Game Pocket Computer". It featured a D-pad, but it was not popular for its time and soon faded.
Initially intended to be a compact controller for the Game & Watch handheld games alongside the prior non-connected style pad, Nintendo realized that Yokoi's updated design would also be appropriate for regular consoles, and Nintendo made the D-pad the standard directional control for the hugely successful Famicom/Nintendo Entertainment System under the name "+Control Pad". All major video game consoles since have had a D-pad of some shape on their controllers. Sega coined the term "D button" to describe the pad, using the term when describing the controllers for the Sega Genesis in instruction manuals and other literature. Arcade games, however, have largely continued using joysticks.
Modern consoles, beginning with the Nintendo 64, provide both a D-pad and a compact thumb-operated analog stick; depending on the game, one type of control may be more appropriate than the other. In many cases with games that use a thumbstick, the D-pad is used as a set of extra buttons, all four usually centered around a kind of task, such as giving commands to friendly non-player characters. Even without an analog stick, some software uses the D-pad's eight-directional capabilities to act as eight discrete buttons, not related to direction or on-screen movement at all. Jam Sessions for the Nintendo DS, for example, uses the D-pad to select music chords during play.


== On non-gaming equipment ==
D-pads appear on a number of menu-driven devices as a simple navigational tool; though superficially similar to those used for gaming devices, they are not optimized for real-time control and therefore can usually accept input from only one direction at a time. Many, though not all, such designs include a trigger button in the center of the button arrangement, usually labeled "Enter", "OK", or the like. Some older devices do not have d-pads as such, but simple single-axis, up/down or left/right pads. On some remotes, the d-pad can also be used to control a robot using a signal-compatible receiver.
On remote control devices, the buttons on the d-pad function in the same manner as other buttons, and are generally used to navigate on-screen menus. Though initially not common, the quick success of the DVD format led to wide availability of remote designs with D-pads circa 2000, and most current menu-driven consumer electronics devices include some sort of d-pad on the remote (and, occasionally, on the unit itself).
In addition, many small computing and communications devices, particularly PDAs, mobile phones, and GPS receivers, include d-pads not only for menu navigation but as general input devices similar to a joystick or mouse. Less-sophisticated designs similar to those on remote controls appear on some calculators, particularly scientific and graphing calculators, which use the d-pad for cursor control on multi-line screens, as well as input/output recall, menu navigation, and occasionally direct screen access (graphing calculators in particular allow the use of the d-pad to determine values at specific points on a displayed graph). On programmable units, the d-pad can also be mapped directly, allowing it to be used as a gaming or pointer control.


== Consoles with D-pads ==


== Other ==


=== Car Stereos with D-pads ===
1990 - AutoPC.


=== Mobile Phones ===
Nokia 9000 Communicator.
Nokia 9210 Communicator.


== Patents ==
U.S. Patent 4,687,200 (expired in 2005) - Nintendo's multi-directional switch


== See also ==
Arrow keys
Joystick


== Footnotes ==