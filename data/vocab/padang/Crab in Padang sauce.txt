Crab in Padang sauce or Padang crab (Indonesian: Kepiting saus Padang) is an Indonesian seafood dish of crab served in hot and spicy Padang sauce. It is one of the two most popular ways that crab is served in Indonesia, commonly found in coastal cities with abundant seafood, such as Padang, Jakarta, Medan, Surabaya, Makassar and Cirebon. Its closest analogue probably is chili crab, however Padang crab uses richer spices.


== Origin ==
The recipe derived from West Sumatran Padang style of seasoning seafood with rich, hot and spicy bumbu (spice mixture). The ground spice mixture includes shallot, garlic, red chili pepper, bird's eye chili, ginger, turmeric and candlenut. The most popular crab species used in this recipe is mud crab, however sometimes blue crab can also be used.


== Ingredients ==
The crabs are boiled in hot water until medium done and cut into pieces, the hot water being used in the boiling process will be used as crab broth. The bumbu ground spices mixture includes garlic, shallot, bird's eye pepper, ginger, turmeric, candlenut and red chili pepper. The ground spice paste is stir fried briefly in palm oil to release its aroma, together with chopped onion, salam leaf (Indonesian bay leaf) and lemon leaf. Then the crab pieces are mixed with stir fried spice paste together with crab broth, scallion, tomato ketchup, chili sauce, oyster sauce, salt and pepper. Then the sauce is being thickened with beaten eggs. The Padang sauce crab tastes savoury, hot and spicy. Other than crab, the identical Padang sauce is often used in other seafood, such as kerang saus Padang (clams in Padang sauce).


== See also ==


== References ==


== External links ==
Kepiting Saus Padang recipe (Indonesian)
Kepiting Saus Padang recipe (Indonesian)