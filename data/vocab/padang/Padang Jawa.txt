Padang Jawa is a small town in Selangor, Malaysia. It is located a few kilometres west of Shah Alam and a few kilometres east of Klang. It is located near the major Bandar Baru Klang township.


== Train stations ==
Padang Jawa has only one train station; which is the Padang Jawa Komuter station under the KTM Komuter line.