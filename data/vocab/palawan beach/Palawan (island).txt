Palawan Island is the largest island of the Palawan Province, Philippines. The northern coast of the island is along the South China Sea, while the southern coast forms part of the northern limit of the Sulu Sea. This island is very undeveloped and traditional. Abundant wildlife, jungle mountains, and white sandy beaches attract many tourists.
Palawan, the only Philippine island cited, is rated by National Geographic Traveler magazine as the best island destination in East and Southeast Asia region in 2007, and the 13th best island in the world having "incredibly beautiful natural seascapes and landscapes. One of the most biodiverse (terrestrial and marine) islands in the Philippines. The island has had a Biosphere Reserve status since early 1990s, showing local interest for conservation and sustainable development".
Iwahig Prison and Penal Farm, one of seven operating units of the Bureau of Corrections, is located on the island.
In April 2013, a fishing vessel loaded with illegally poached animals ran aground on a coral atoll off the coast of Palawan Island.
In 2014, armed forces chief of staff General Emmanuel Bautista said that Oyster Bay may be developed into a naval base with United States Navy support.


== Bibliography ==
C.Michael Hogan. 2011. Sulu Sea. Encyclopedia of Earth. Eds. P.Saundry & C.J.Cleveland. Washington DC


== References ==


== External links ==