Palestinian Americans (Arabic: الأميركيون الفلسطينيون‎) are Americans of Palestinian ancestry. It is difficult to say when the first Palestinian immigrants arrived in the United States; however, many of the first immigrants to arrive were Christians escaping persecution from the Ottoman Empire in the late 19th century. Others came as a result of the tension during the 1948 Arab–Israeli War and the 1967 Six-Day War.


== History ==

The first Palestinians who emigrated to the United States arrived after 1908. Palestinian emigration began to decline after 1924, with the law limiting the number of immigrants, as well as the Great Depression. The population began to increase after World War II: the Arab-Israeli War, the Nakba, and the foundation of the state of Israel in 1948 caused many Palestinians to immigrate, most as refugees. However, the greatest wave of Palestinian immigration began in 1967 after the Six-Day War, or as Arabs call it the June War. This wave of immigrants reached its peak in the 1980s. Most Palestinians that immigrated to the United States in this period were more educated than the Palestinians that arrived until 1967, thanks to the schools sponsored by the ONU and the increased number of universities in the Middle East.


== Demographics ==

Many Palestinians settled in the metropolitan areas of New York City and Paterson in northern New Jersey, as well as in California, Phoenix, Miami, Chicago, Detroit, and Cleveland, alongside other Mediterranean communities, including the Lebanese, Syrians, Greeks, Italians, Egyptians and Turks.
Paterson, New Jersey has been nicknamed Little Ramallah and contains a neighborhood with the same name, with an Arab American population estimated as high as 20,000 in 2015.
According to the 2000 United States Census, there were 72,112 people of Palestinian ancestry living in the United States, increasing to 85,186 by the 2009-2013 American Community Survey.


== Education ==
In the United States approximately 46% of Palestinians have obtained at least a college degree, compared to 18% of the American population. The study of culture and the Arabic language is increasingly important among Palestinians, especially in college and graduate school. Thus, some Palestinian or Arab organizations are working to monitor and improve the teaching of Arab history and culture in the American schools.


== Language and culture ==
Palestinian culture is a blend of Eastern Mediterranean influences. Palestinians share commonalities with nearby Levantine peoples, including Israelis and Jews, Egyptians, Lebanese, Syrians, and Jordanians. Palestinians speak Palestinian Arabic.


== See also ==
Arab American
List of Palestinian Americans
Palestinian Christians
Palestinian cuisine
Palestinian diaspora


== References ==


== External links ==
Palestinian American Council
Arab Americans