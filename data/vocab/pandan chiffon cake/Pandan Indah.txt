Pandan Indah is a major township in Ampang, Selangor, Malaysia. It is located between Ampang proper and Cheras. The Pandan Indah LRT station directly services this township.
This mini city has transformed into a big and famous city since the flyover was built. "Pandan Indah" has a police station, a fire station, a primary and a secondary school also not forgetting big shopping malls.


== Facilities ==
Majlis Perbandaran Ampang Jaya (MPAJ) main headquarters
Balai Polis Pandan Indah
Balai Bomba Pandan Indah
Taman Rekreasi Pandan Indah


== Educational ==
Sekolah Kebangsaan Pandan Indah
Sekolah Menengah Kebangsaan Pandan Indah


== References ==