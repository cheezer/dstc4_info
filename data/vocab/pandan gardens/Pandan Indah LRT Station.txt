Pandan Indah LRT station is a Malaysian at-grade rapid transit station situated near and named after Pandan Indah, in Ampang Jaya, Selangor. The station is part of the Sri Petaling-Ampang branch of the Ampang Line (formerly known as STAR, and the Ampang and Sri Petaling Lines), and was opened on December 16, 1996, as part of the first phase of the STAR system's opening, alongside 13 adjoining stations along the Sultan Ismail-Ampang route.
The station shares a similar name with the neighbouring Pandan Jaya station 1 kilometre southwest, leading to some confusion among passengers.


== §Location ==
The Pandan Indah station is situated next to and named after the Ampang Jaya locality of Pandan Indah to the southeast, and is within walking distance of Taman Bakti (Malay; English: Bakti Estate) directly beside the station, Taman Cempaka (Cempaka Estate) and the eastern edges of Pandan Jaya to the north. The station is directly accessible from the north through a flats complex off Jalan Cempaka (Cempaka Road), while the station is open to Pandan Indah via Jalan Pandan Indah 22 (Pandan Indah Road 22), a branch road from the main thoroughfare of Jalan Pandan Indah (Pandan Indah Road).
The Pandan Jaya station was constructed along two leveled tracks, reusing the now defunct Federated Malay States Railway and Malayan Railway route between Kuala Lumpur, Ampang town and Salak South.


== §Design ==

Overall, the Pandan Indah station was built as a low-rise station along two tracks for trains traveling in opposite direction. Because the station is nearly at-grade and features two side platforms, the station designates individual ticketing areas for each of the station's two platforms at their level, ensuring access to trains traveling the opposite direction is not freely possible. The station also serves as a public crossing across the Ampang Line tracks between Taman Bakti and Pandan Indah via a walkway running underneath the tracks and platforms.
The principal styling of the station is similar to most other stations in the line, featuring curved roofs supported by latticed frames, and white plastered walls and pillars. Because stairways are only used to link street level with the station's ticket areas and platforms, the station is not accommodative to disabled users.


== §External Links ==
KL MRT Line Integrations