Park Avenue (officially avenue du Parc in French) is one of central Montreal's major north-south streets. It derives its name from Mount Royal park, by which it runs. Between Mount Royal Avenue and Pine Avenue, the street serves as the boundary between the mountain, to the west, and the smaller Jeanne Mance Park to the east. South of Sherbrooke Street, the street's name changes to Bleury Street, and south of Saint Antoine Street in Old Montreal, the name changes again to Saint Pierre Street. The northern end of Park Avenue is Jean Talon Street and the former Canadian Pacific Railway Park Avenue station, now serving, in part, as the Parc metro station and commuter train station.
However, there is also a short stretch of Park Avenue between Crémazie Boulevard (A-40) and Chabanel Street.
Once one of Montreal's most elegant residential avenues, Park Avenue is now a busy commercial street, home to the former Rialto Theatre. Since 1924, it has also been an important part of Montreal's Greek community.
Park also lends its name to the residential neighbourhood Park Extension, at its northern end.


== History ==

In 1883, a request by English-speaking citizens was filed with the City of Montreal to name this street Park Avenue (in reference to Mount Royal Park which opened in 1876). It was always officially referred to by its English name, Park Avenue, until September 29, 1961, when its French name, Avenue du Parc, was officially recognized.
In 1937, the government planned to change the name of the street to Marconi Street, but decided to keep its current name following protests by citizens.
A similar event occurred on October 18, 2006 when mayor Gérald Tremblay proposed to rename Park Avenue (along with Bleury Street) in honour of former Quebec premier Robert Bourassa. On November 28, Montreal City Council voted in favour (40-22) of the motion. If the Commission de toponymie du Québec had approved the change, Park Avenue and Bleury Street in their entirety would have been renamed Robert Bourassa Avenue. The proposal was controversial, especially in light of the historical nature of the name. (The STM's Parc metro station (and AMT commuter rail station) were to remain "Parc" due to a moratorium on renaming metro stations.) After Bourassa's family publicly expressed reservations about the controversy, Tremblay announced on February 6, 2007 that he would not pursue the issue further and that the council would be presented with a motion to withdraw the resolution made November 28.
In 2005, a C$25 million project began to transform the intersection of Pine Avenue and Park, known as the Pine-Park Interchange (French: Échangeur des Pins). Both Park and Pine remained open during the work. The Pine-Park Interchange was demolished and was replaced in the summer of 2006, with a more traditional intersection, easier to navigate for pedestrians and cyclists.


== Greektown ==
Greektown is a proposed name for a neighbourhood located on Park Avenue between Mount Royal Avenue and Van Horne Avenue. Historically Greek influence has been very strong in this area along with the adjoining Park Extension neighbourhood. There are over 61,000 Montrealers of Greek descent.
Montreal's Greektown was where fans celebrated the victory of Greece in the 2004 UEFA European Football Championship.


== Public transit ==
Bus routes along Park Avenue include the Société de transport de Montréal's 80 Avenue du Parc and 435 Reserved Lane Parc/Côte-des-Neiges.
Due to the high volume of bus passengers in this corridor, the city of Montreal has proposed to build a tramway along the length of Park Avenue. This line is projected to connect the city centre to the Agence métropolitaine de transport's Parc commuter rail station and the Montreal Metro's Parc station. The AMT believes the tram has a potential ridership of 11,600 daily passengers.


== See also ==
Park Avenue bus
435 Reserved Lane Parc/Côte-des-Neiges
Cocoa Locale, a bakery on Park Avenue
Théâtre Fairmount, a music venue on Park Avenue
Black Watch Armoury, located on Bleury Street


== References ==
^ Google Map
^ "'Turn the page' on Park Avenue debate: mayor". CBC News. November 29, 2006. 
^ "Bourassa statue unveiled as street naming stirs controversy". CBC News. October 19, 2006. 
^ "Holà aux changements de nom des stations de métro" (in French). Retrieved 2006-11-28. 
^ Montreal Gazette:City to name Park a Greek village
^ Ethnocultural Portrait of Canada Highlight Tables, 2006 Census
^ URBANPHOTO: Cities / People / Place » Rebranding Park Avenue