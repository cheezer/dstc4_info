Masters Inn is a chain of motels in the United States. The chain operates 15 locations in 6 states.
The first location existed in Kansas City in 1985 through the name change of a previously existing motel.
In 2007, the chain was acquired by Supertel. The group sold its Cave City, Kentucky location in 2010.


== See also ==
List of motels


== References ==