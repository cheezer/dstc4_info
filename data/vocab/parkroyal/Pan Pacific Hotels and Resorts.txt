The Pan Pacific (泛太平洋酒店及度假村）portfolio is a subsidiary of and managed by Pan Pacific Hotels Group Limited, which has 20 luxury hotels, resorts and serviced suites across Asia, Greater China, North America, Oceania, and others that are still under development. It is headquartered in Singapore.


== Overview ==
In line with the brand's plans to expand the Pacific Hotels and Resorts portfolio in Asia, Greater China and Oceania, Pan Pacific debuted in China with Pan Pacific Xiamen in 2009 and Pan Pacific Suzhou in the following year. The brand also extended its footprint into Australia with the launch of Pan Pacific Perth in 2011.
The opening of Pan Pacific Ningbo and Pan Pacific Serviced Suites Ningbo in 2012 and Pan Pacific Tianjin in 2014 has further strengthened the brand's presence in China.


== History ==
The first Pan Pacific hotel established was Sari Pan Pacific Jakarta which opened in 1976, and Pan Pacific Hotels and Resorts has since extended its presence to other parts of Asia and North America, including Singapore, Malaysia, Manila, Vancouver, Whistler and Seattle.
In 2007, Pan Pacific Hotels and Resorts became a founding member of the Global Hotel Alliance, the world's largest alliance of independent hotel brands comprising 14 member brands with nearly three hundred upscale and luxury hotels across fifty-two countries. A year later, the brand launched its first extended-stay property with the debut of Pan Pacific Serviced Suites Orchard, Singapore.


=== Rebranding ===
In 2009, Pan Pacific Xiamen was rebranded from Sofitel Plaza Xiamen. In 2010, Pan Pacific Suzhou was rebranded from Sheraton Suzhou Hotel and Towers, Pan Pacific Nirwana Bali Resort was rebranded from Le Meridien Nirwana Golf and Spa Resort Bali, and Pan Pacific Serviced Suites Bangkok, the brand's first extended-stay product outside of Singapore, opened.
In 2011, Pan Pacific Perth was rebranded from Sheraton Perth. Pan Pacific Hotels and Resorts unveiled its refreshed brand identity with a global brand campaign that saw the launch of new brand advertisements across various media platforms, including top-tier print titles, inflight channels and online sites.


== References ==