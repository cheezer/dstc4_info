Parkview Health System was incorporated in May, 1995. Parkview Health traces its roots back to Fort Wayne City Hospital, founded in 1878. Subsequent hospitals in our history include Hope Hospital (1891-1922), Methodist Hospital (1922-1953), Parkview Memorial Hospital (1953-1955), and Parkview Hospital Randallia (1955 - ). Parkview Health's flagship hospital campus, Parkview Regional Medical Center, was founded in March 2012. Located in northeastern Indiana, Parkview Health is made up of seven hospitals and is the region's largest employer with more than 8,500 co-workers. The Parkview Physicians Group  is also part of the Parkview Health System. Parkview Physicians Group includes more than 300 providers in more than 25 specialties in more than 80 locations throughout northeast Indiana and northwest Ohio, and numerous walk-in clinics.


== Recently ==
Recently, Parkview officials announced a facelift of $3.2 million to Parkview Randilla Hospital. It includes a new entryway, new signage, a large courtyard, and a park. An interior facelift was conducted right after the Parkview Regional Medical Center was complete in May 2012, which included turning 150 patient rooms into private, more comfortable rooms. Other highlights include acute and continuing care centers, a surgery area, the Center for Wound Healing, Center on Aging and Health and a full-service emergency department. The facility also offers a family birthing center, imaging and lab services, endoscopy, and a sleep lab.


== CyberKnife ==
Parkview has the region’s only CyberKnife®, a precise and virtually painless outpatient treatment that effectively targets tumors and dramatically reduces procedure time for some patients. CyberKnife delivers focused beams of radiation to the tumor from numerous angles with a high degree of accuracy, thus preserving the healthy tissue surround the tumor. Typically, only five or fewer CyberKnife treatments are needed compared to as many as 40 conventional radiation treatments. With virtually no recovery period and virtually no side effects, CyberKnife is a powerful tool against cancer. Since 2006, more than 900 patients have been treated with Parkview’s CyberKnife System.


== Statistics ==
Parkview Health System has over 8,500 employees serving Northeast Indiana and Northwest Ohio


== Locations ==
Parkview Regional Medical Center, Fort Wayne, Indiana
Parkview Ortho Hospital
Parkview Heart Institute
Parkview Women & Children's Hospital
Mirro Family Research Center (Opening 2015)

Parkview Hospital Randallia, Fort Wayne, Indiana
Parkview Behavioral Health, Fort Wayne, Indiana

Parkview Noble Hospital, Kendallville, Indiana
Parkview Huntington Hospital, Huntington, Indiana
Parkview Whitley Hospital, Columbia City, Indiana
Parkview LaGrange Hospital, Lagrange, Indiana


== References ==