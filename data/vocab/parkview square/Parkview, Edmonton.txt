Parkview is a residential neighbourhood in west Edmonton, Alberta, Canada overlooking the North Saskatchewan River valley. The neighbourhood is informally split into two smaller neighbourhoods, with the portion east of 142 Street called Valleyview and the portion west of 142 Street called Parkview. There is a small strip shopping centre, Valleyview Shopping Centre, located near the centre of the neighbourhood on the Parkview side of 142 Street.


== Demographics ==
In the City of Edmonton's 2012 municipal census, Parkview had a population of 7003333400000000000♠3,334 living in 7003134300000000000♠1,343 dwellings, a -1.4% change from its 2009 population of 7003338200000000000♠3,382. With a land area of 1.54 km2 (0.59 sq mi), it had a population density of 7003216490000000000♠2,164.9 people/km2 in 2012.


== Residential development ==

Most of the residential construction in the neighbourhood occurred after the end of World War II, with seven out of ten residences being constructed between 1946 and 1960. Substantially all residential construction was completed by 1970. Residences east of 142 Street tend to be more affluent, while residences west of 142 Street tend to be more affordable. According to the 2005 municipal census, 100% of the redsidences in the neighbourhood are single-family dwellings. Substantially all residences are owner-occupied.
There are two schools in the neighbourhood. Parkview Elementary Junior High School is operated by the Edmonton Public School System. St. Rose Junior High School is operated by the Edmonton Catholic School System.
Edmonton's Valley Zoo, a family oriented petting zoo, is located in the river valley to the south east of the neighbourhood with access provided by Buena Vista Road. Buena Vista Road also provides access to Laurier Park, a good place for families to go for picnics, and Buena Vista Park. Both parks are part of the city's river vallye park system. A foot bridge, located in the river valley to the east of the neighbourhood provides access to Hawrelak Park on the south side.
Whitemud Drive, with access from 149 Street, provides residents with good access to destinations on the south side, including: the University of Alberta, Old Strathcona, Whyte Avenue, Southgate Centre, and Fort Edmonton Park. Travel west along 87 Avenue takes residents to West Edmonton Mall. Residents also enjoy good access to the downtown core.
Parkview is bounded on the north by the McKenzie Ravine, on the east by the North Saskatchewan River valley, on the west by 149 Street, and on the south by 87 Avenue and Buena Vista Road.


== Surrounding neighbourhoods ==


== References ==


== External links ==
Parkview Neighbourhood Profile