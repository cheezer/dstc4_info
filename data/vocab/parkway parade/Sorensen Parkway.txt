Sorensen Parkway is a modern link in the boulevard system in Omaha, Nebraska. The Parkway flows west from North 30th Street to North 90th Street, and has been viewed as the northern boundary that defines the area called North Omaha.


== About ==
Built along the abandoned Fremont, Elkhorn and Missouri Valley Railroad bed in North Omaha, the Sorensen Parkway was named for Alexander V. Sorensen, the Mayor of Omaha from 1965 to 1969. Early plans called for the extension of the Parkway, and recent conversations have focused on connecting the Sorensen Parkway to the Fremont Connector 
The intersection of North 72nd and Sorensen Parkway has been developed in recent years, including the addition of the first movie theater in northwest Omaha in 20 years.


== See also ==
Boulevards in Omaha


== References ==