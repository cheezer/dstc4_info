Pasir Ris Group Representation Constituency (Traditional Chinese: 白沙集選區;Simplified Chinese: 白沙集选区) is a former four member Group Representation Constituency that locates in the eastern part of Singapore at Pasir Ris. It had only existed once in 1997 general election.
In 2001 General Election, this GRC had absorbed 3 wards in Punggol areas, while 3 of the existing 4 member wards in Pasir Ris was consolidated into 2 wards. With such enlargement, this GRC was renamed into Pasir Ris-Punggol GRC and expands to a 5 members GRC. Since then, it has again further expanded to 6 members GRC on 2006 General Election which the arrangmement is still exists to date.


== Members of Parliament ==


== Candidates and results ==


=== Elections on 1990s ===


== See also ==
Pasir Ris-Punggol GRC


== References ==

1997 General Election's result