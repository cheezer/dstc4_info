Pasir Ris is a primarily residential area in Singapore comprising many high-rise residential dwelling blocks, mostly public housing built by the Housing and Development Board in Pasir Ris New Town. The town is located in the eastern part of Singapore.
Like other new towns, public transport facilities were factored into the development of Pasir Ris. Pasir Ris is easily accessible via bus services at Pasir Ris Bus Interchange and the Mass Rapid Transit at Pasir Ris MRT Station. Today, the new landmarks in the area are the popular NTUC Pasir Ris Resort, Pasir Ris Beach Park and White Sands, a shopping mall.


== Etymology and history ==
The first reference to a village of Pasir Ris, Passier Reis, appeared in 1853. There has been conjecture that perhaps this is a contraction of Pasir Hiris (pasir is "sand" and hiris means "to shred" in Malay). Pasir Ris may also mean 'white sand' in Malay. Pasir Ris Town is named after the long stretch of sandy white beach along the north-east coastline of Singapore, facing Pulau Ubin. The town exudes a resort ambience and its architecture and identity take inspiration from the surrounding beach and sea elements.
Pasir Ris was originally a low-lying, undeveloped area with kampongs and villages like Kampong Pasir Ris, Kampong Bahru and the various Chinese kampongs along Elias Road. The area was well known for its many plantation estates including the Singapore United Plantations, Loh Lam Estate, Hun Yeang and Thai Min Estates. The beach was a popular resort for water skiing in the 1950s. There was also the Pasir Ris Hotel, venue of many memorable parties and picnic gatherings in the 1950s, 1960s and early 1970s.
Pasir Ris means ‘white sand’ in Malay. Pasir Ris Town is named after the long stretch of sandy white beach along the north-east coastline of Singapore, facing Pulau Ubin. The town exudes a resort ambience and its architecture and identity take inspiration from the surrounding beach and sea elements.
The development of Pasir Ris New Town began in 1983 with Phase 1 being an Elias Road/Pasir Ris Estate, followed by Loyang N1 and N2 by 1989–90, N4 from 1992 to 1993, N5 from 1995 and N6 in 1997 together with N7. The proposed Neighbourhood 3 (N3) is just an empty land and (N8) is part of military training area, will be reviewed in 2018. The area was known as White Sand because it had a good beach. Today, White Sands Shopping Centre, located next to Pasir Ris MRT Station embodies the local place name.


== Geography ==

Pasir Ris is geographically divided into 3 regions: Loyang (East), Central and Elias (West) with some hills less than 40 metres. Since 2014, the hills have been flattened to make way for upcoming residential development.


=== Political boundaries ===
Pasir Ris comes politically under the Pasir Ris-Punggol Group Representation Constituency since 2001 and Pasir Ris Group Representation Constituency from 1997 to 2001. After the general election in 2001, a large part of the former Pasir Ris Central Division and the Pasir Ris-Loyang Division came together as Pasir Ris East with Zainal Sapari as the Member of Parliament since 2011.
Pasir Ris West consists of Elias housing estate and the western area of Pasir Ris and its Member of Parliament being Deputy Prime Minister and Minister for Home Affairs Teo Chee Hean. In conjunction with the expansion of Pasir Ris-Punggol Group Representation Constituency (GRC), a Community Club scaling six storeys high, was built in Pasir Ris East. In October 2004, Pasir Ris West also saw the completion of Pasir Ris Elias Community Club.


== Amenities ==


=== Recreation ===

In proximity to the sea, Pasir Ris has several recreation areas like the NTUC Downtown East, which have facilities like chalets, theme parks, a bowling alley, a park for pets and pet owners and Pasir Ris Park. There is also another park by the name of Pasir Ris Town Park, close to where the Shopping Centre White Sands is located. The park sports a park connector, a playground, an eatery and fishing pond and a section of the park has been removed to build the Sports and Recreation Centre with facilities such as swimming pool, a sports hall to be used mainly for badminton, a street soccer court and also a gym. The bowling alley building in Downtown East has been demolished to make way for a new four-storey shopping and entertainment complex with a cinema which has been completed in 2008 called e!hub. e!hub also includes a new bowling alley, which replaces the old one. The area also includes a water-based theme park and a no longer operational theme park . They are Escape Theme Park which opened in 2000 but was closed down in 2011 and Wild Wild Wet which is a water-based theme park that opened in 2004. The current site of the former Escape Theme Park will be utilized for expansion of Wild Wild Wet.


=== Industry ===
Pasir Ris is also part of the Jurong Town Corporation's plan to develop wafer fabrication facilities in Singapore. The other two locations are Tampines and Woodlands. Presently, there are two major wafer fabrication parks in Pasir Ris. Companies situated there include United Microelectronics Corporation and Systems on Silicon Manufacturing.Pasir Ris have two recycling factory in the N2 area.


=== Education ===
Primary schools
Casuarina Primary School
Loyang Primary School
Coral Primary School
Elias Park Primary School
Park View Primary School
Meridian Primary School
Pasir Ris Primary School
White Sands Primary School
Secondary schools
Coral Secondary School
Pasir Ris Crest Secondary School
Hai Sing Catholic School
Loyang Secondary School
Greenview Secondary School
Siglap Secondary School
Post-secondary schools
Meridian Junior College


=== Places of worship ===
Buddhist temples
Sakya Tenphel Ling (Tibetan Monastery)
Catholic churches
Church of the Divine Mercy
Mosques
Al-Istighfar Mosque
Protestant churches
Bethesda Pasir-Ris Mission Church
Pentecost Methodist Church
RiverLife Church
Shalom Bible Presbyterian Church


=== Commercial services ===

Shopping centres, malls and markets
Loyang Point (Pasir Ris East)
Pasir Ris Drive 6 (Pasir Ris East Central)
White Sands Shopping Centre (Pasir Ris Central)
Elias Mall (Pasir Ris West)
Pasir Ris West Plaza (Pasir Ris West)
NTUC Downtown East
E-Hub Shopping Mall (Pasir Ris Central)
Petrol/Diesel Stations
Singapore Petroleum Centre (Drive 4)
Esso Petrol (Drive 1)
Shell (Loyang Ave)


=== Community services ===
Community Centres
Pasir Ris East CC (Drive 4)
Pasir Ris Elias Community Club(next to Elias Mall)


== In popular culture ==
Singaporean singer and songwriter Kevin Mathews, who grew up in the area once wrote a song Pasir Ris Sunrise, a parody of the song Waterloo Sunset


== References ==