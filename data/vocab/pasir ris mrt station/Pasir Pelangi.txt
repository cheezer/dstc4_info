Pasir Pelangi is the royal village of Johor, Malaysia. It is situated near Stulang and Johor Bahru. This royal village was established in the early 1900s during the reign of Sultan Ibrahim of Johor. The main attractions of Pasir Pelangi are the several palaces situated here.


== Notable places in Pasir Pelangi ==
Istana Pasir Pelangi
Istana Hinggap Diraja
Istana Tengku Mahkota Johor
Masjid Diraja Pasir Pelangi
Kelab Polo Diraja
Kelab Lumba Kuda Diraja
Best 104 royal radio station
Mados Travel Services offices
Taman Iskandar


== See also ==
Istana Besar
Istana Bukit Serene