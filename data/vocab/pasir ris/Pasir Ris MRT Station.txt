Pasir Ris MRT Station is the eastern terminal station of the East West Line in Singapore. The station is located at the central part of Pasir Ris New Town. Its station colour is red.
Demand is moderate and stagnant, and Pasir Ris is the closest to Downtown East and Pulau Tekong, where many National Servicemen would take the bus to the SAF Ferry Terminal. Train drivers will usually walk from one carriage to another for logging out of the journey eastbound, and logging back again to continue the journey westbound.


== Modifications ==
Pasir Ris MRT Station was one of the many above ground stations built in the past with no platform screen doors to prevent commuters from falling off the platform and onto the train tracks. However on 13 August 2009, Pasir Ris MRT Station became the first above ground MRT Station in Singapore to begin installing half height platform screen doors. On 1 November 2009, the platform screen doors were fully completed and started operations.
Pasir Ris was the first eastern terminus station of the East West Line, installed with Rite Hite Revolution High Volume, Low Speed (HVLS) fans and have been operating since 9 July 2012 together with Simei.


== Exits ==
A: Pasir Ris Bus Interchange, White Sands, Pasir Ris (East), Downtown East, Wild Wild Wet, Pasir Ris Park
B: Pasir Ris (West), Pasir Ris Bus Interchange


== Station layout ==


== Transport connections ==


=== Rail ===


== References ==


== External links ==
Official website
Pasir Ris to Changi Airport MRT Station Route