Electronic Payment Services (Chinese: 易辦事), commonly known as EPS, is the largest electronic payment system in Hong Kong, Macau and Shenzhen starting from 1985. The service is provided by EPS Company (Hong Kong) Limited. Currently there are over 25,000 acceptance locations.


== Provider ==
Established in 1984, EPS Company (Hong Kong) Limited (formerly Electronic Payment Services Company) is currently a consortium of 21 major banks in Hong Kong.


== System ==
In each retail location, a terminal is installed and is usually connected to a POS system of retailer. The terminal can also connect to banks through the public phone system.
The operation of the system is easy. The retailer swipes a customer's ATM card in the terminal and keys in the payment amount. The cardholder then selects the appropriate bank account and keys in the PIN. After successful connection to the bank, a receipt will be printed for the customer and the retailer. Some automatic terminals require the cardholder to insert his or her ATM card into a terminal machine rather than have the merchant swipe the card.
Payment for each transaction is directly transferred from the payer's bank account to receiver's.


== Service at consumer level ==


=== EPS service ===
EPS (易辦事) entails the simple use of an ATM card or a credit cards (able to be used at ATMs) at point of service (POS) locations. No application for the service is required. All ATM cards or credit cards (able to be used at ATMs) issued by EPS member banks can use the EPS system.
The EPS device is a dual unit device consisting of a removable card processor and a stationary base that serves as a charger and data link. A typical transaction by EPS would entail the consumer handing over the EPS card to the retailer, who then swipes the card in the processor, keys in the dollar amount of the transaction and then hands the processor to the consumer to type in her/his PIN. The device is returned to the base unit at which point communication between the EPS device and the EPS servers begins. Albeit a removable device, the EPS processor does not communicate with the EPS base unit wirelessly (no attempt has been made to permit this either, for security reasons). EPS receipts are printed with "ACCEPTED" for successful transactions, while various other error messages result depending on the erroneous circumstances.


=== EPS EasyCash service ===
EPS EasyCash (提款易) allows card holders to withdraw cash at over 1,200 locations in Hong Kong upon a regular purchase by EPS. The cash withdrawal amount must be in units of $100, up to $500. EPS EasyCash service is also commonly known as Cashback.
EPS EasyCash service is available at Hong Kong based chain stores such as Gourmet, Great, IKEA, Mannings, MarketPlace, Massimo Dutti, Circle K, Oliver's, Parknshop, Taste, Threesixty, Vango, China Resources Vanguard (CRV), V'ole and Wellcome.


== EPS Company Member Bank List ==
Bank of China (Hong Kong) Limited
Bank of Communications, HK Branch
China Construction Bank (Asia)
China Merchants Bank Company Limited
Chiyu Banking Corporation Limited
Chong Hing Bank Limited
Citibank (Hong Kong) Limited
Citic Ka Wah Bank Limited
Dah Sing Bank Limited
DBS Bank (Hong Kong) Limited
Fubon Bank (Hong Kong) Limited
Hang Seng Bank Limited
The Hongkong and Shanghai Banking Corporation (HSBC)
ICBC (Asia)
Mevas Bank Limited
Nanyang Commercial Bank Limited
Shanghai Commercial Bank Limited
Standard Chartered Bank (Hong Kong) Limited
The Bank of East Asia Limited
Wing Hang Bank Limited
Wing Lung Bank Limited
Remark: All ATM cards or credit cards (able to be used at an ATM) issued by above member banks can perform EPS service.


== Official website ==
Official website


== See also ==
Payment by Phone Service (PPS)
Octopus card
Credit card


== External links ==
Official website (English) (traditional Chinese (HK)) (Chinese)