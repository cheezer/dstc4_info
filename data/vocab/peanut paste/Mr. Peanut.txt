Mr. Peanut is the advertising logo and mascot of Planters, an American snack-food company and division of Kraft Foods. He is depicted as an anthropomorphic peanut in its shell dressed in the formal clothing of an old-fashioned gentleman: a top hat, monocle, white gloves, spats, and a cane.


== History ==
Planters Peanut Company was founded in 1906, in Wilkes-Barre, Pennsylvania by Amedeo Obici and was incorporated two years later as the Planters Nut and Chocolate Company. While many sources - often merely repeating previous undocumented sources - refer to a 1916 Planters-sponsored contest to create a logo for the company, there is no evidence of such a contest, including any contemporary references, that can be found anywhere. That year, a 14-year-old Suffolk boy, Antonio Gentile, whose family was known to Obici and the recipient of much financial support over the years, submitted multiple drawings of an anthropomorphic peanut with a cane, in a variety of poses, for which he received five dollars. Frank P. Krize, Sr., a Wikes-Barre commercial artist who Obici would later hire to head up the printing department at the Suffolk plant, later added spats, a top hat, and a monocle to the drawing, and Mr. Peanut, or as Antonio called him, Bartholomew Richard Fitzgerald-Smythe, was born.
By the mid-1930s, the raffish figure had come to symbolize the entire peanut industry. Mr. Peanut has appeared on almost every Planters package and advertisement. He is now one of the best-known icons in advertising history.
Mr. Peanut has appeared in many TV commercials as an animated cartoon character. More recent commercials have shown him stop motion animated in a real-world setting.
In 2006, Planters conducted an online contest to determine whether to add a bow tie, cufflinks, or a pocketwatch to Mr. Peanut. The public voted for no change.
While the character's television commercials were often accompanied by an elegant accented narrator, Mr. Peanut never had dialogue. On November 8, 2010, Planters announced that Mr. Peanut would officially be given a voice, supplied by American actor Robert Downey Jr.
In 2011 Mr. Peanut's "stunt double" named Peanut Butter Doug was introduced to tie-in with the Planter's Peanut Butter launch. The character is voiced by Kevin Dillon.
Planters announced on July 1, 2013 that its mascot, Mr. Peanut, would be getting a new voice—that of comedian and Saturday Night Live alumnus Bill Hader, who is actually very allergic to peanuts.


== In literature ==
In the 2010 novel Mr. Peanut, a man fantasizes about killing his peanut-allergic wife by force-feeding her peanuts. "He poured out a handful and ate them and then wiped the salt from his empty hand on his pants. He looked at the chipper Planters Peanuts man tipping his top hat hello and thought about how one bite could kill Alice dead."


== In popular culture ==
In an episode of the 1960s sitcom He and She, Dick Hollister greets an overdressed party guest with "Well, it's Mister Peanut!".
The artist Vincent Trasov, dressed as Mr. Peanut, ran as a joke candidate in the 1974 Vancouver, British Columbia civic elections.
In the twentieth episode of the first season of Friends, when comparing Mr. Peanut with Mister Salty, Joey Tribbiani says Mr. Peanut is a great dresser. Phoebe Buffay, in response, says that Mr. Peanut is gay.
In November 2010, The New Yorker magazine published a spoof confession by Mr. Peanut that he is gay and in a relationship with his new sidekick, Benson.
In the fifth season episode "New Boss" of The Office, Michael states that he thinks Mr. Peanut is classy. Dwight retorts that Mr. Peanut is not classy, and that Michael only thinks so because he has a top hat and a monocle.
Mr. Peanut was featured in an episode of MADtv - in an episode of "CLOPS" (a short claymation parody of the show, COPS), Mr. Peanut is depicted as holding several nuts hostage after going insane. He kills one via nutcracker before committing suicide with the same device.
In the second season of American Dad!, Black Mystery Month, Mr. Peanut is used as a clue in a Da Vinci Code-style mystery.
In the sixth season episode of The Simpsons, Treehouse of Horror VI, in the segment "Attack of the 50-Foot Eyesores", one of the giant monsters resembles Mr. Peanut.
In the second episode of Freakazoid!, the character Toby Danger (a Jonny Quest parody) dresses up as Mr. Peanut in an attempt to sneak into a casino.
In season 8, episode 17 of Seinfeld called "The English Patient", a woman mistakes George for her boyfriend Neil, a guy who looks just like him. This intrigues George; he wants to meet Neil. While talking with Jerry about him, George speculates about what Neil might look like and according to this description, Jerry compares Neil to Mr. Peanut. In the end, George finally meets Neil and says "So, we meet at last. I admire your skills, Mr. Peanut." after discovering that he (Neil) has a cane.
Mr. Peanut is a permanent character in the long-running San Francisco musical revue Beach Blanket Babylon.
In season 11, episode 16 of Frasier, Martin gets a new cane and says "give me a top hat and I'm Fred Astaire". Frasier responds "add a monocle and you're Mr. Peanut."
Mr. Peanut appears as a monster in the online game Kingdom of Loathing.


== References ==


== External links ==
Planters.com: History of the Planters Nut