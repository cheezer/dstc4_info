Peanut butter, popular in many countries, is a food paste made primarily from ground dry roasted peanuts. Some varieties contain added salt, seed oils, emulsifiers, and sugar, whereas "natural" types of peanut butter consist solely of ground peanuts. It is mainly used as a sandwich spread, sometimes in combination with other spreads such as jam, honey, chocolate (in various forms), vegetables or cheese. The United States is a leading exporter of peanut butter. Nuts are also prepared comparably as nut butters.


== History ==
Canadian Marcellus Gilmore Edson (February 7, 1849 – March 6, 1940) of Montreal, Quebec was the first to patent peanut butter, in 1884. Peanut flour already existed. His cooled product had "a consistency like that of butter, lard, or ointment" according to his patent application. He included the mixing of sugar into the paste so as to harden its consistency.
Edson, a chemist (pharmacist), developed the idea of peanut paste as a delicious and nutritious staple for people who could hardly chew on solid food, a not uncommon state back in those days. Peanut paste was initially sold for six cents per pound.
Edson was issued United States patent #306727 in 1884. The patent describes a process of milling roasted peanuts until the peanuts reached "a fluid or semi-fluid state".
John Harvey Kellogg was issued a patent for a "Process of Producing Alimentary Products" in 1898 and used peanuts, although he boiled the peanuts rather than roasting them. Kellogg served peanut butter to the patients at his Battle Creek Sanitarium. Other makers of modern peanut butter include George Bayle, a snack-food maker in St. Louis, Missouri, who was making peanut butter with roasted peanuts as early as 1894, and George Washington Carver, who is often mistakenly credited as the inventor due to his extensive work in cultivating peanut crops and disseminating recipes.
Early peanut-butter-making machines were developed by Joseph Lambert, who had worked at John Harvey Kellogg's Battle Creek Sanitarium, and Ambrose Straub.
January 24 is National Peanut Butter Day in the United States.


== Health ==


=== Nutritional profile ===
Peanut butter is an excellent source (> 19% of the Daily Value, DV) of protein, dietary fiber, vitamin E, pantothenic acid, niacin and vitamin B6 (table, USDA National Nutrient Database). Also high in content are the dietary minerals manganese, magnesium, phosphorus, zinc and copper (table). Peanut butter is a good source (10–19% DV) of thiamin, iron and potassium (table).
Both crunchy/chunky and smooth peanut butter are sources of saturated (primarily palmitic acid) and unsaturated fats (primarily oleic and linoleic acids).


=== Peanut allergy ===
For people with a peanut allergy, an estimated 4–6% of the population, peanut butter can cause a variety of possible allergic reactions. This potential effect has led to banning peanut butter, among other common foods, in some schools.


== Other uses ==

Peanut butter is included as an ingredient in many recipes, especially cookies and candies. Its flavor combines well with other flavors, such as chocolate, oatmeal, cheese, cured meats, savory sauces, and various types of breads and crackers.
Peanut butter is known to work well combined with jelly (as the American peanut butter and jelly sandwich, which also extends to jam), banana, sambal, pickles, mayonnaise, olives, onion, horseradish, chocolate chips, bacon, honey, Marmite, or Vegemite in a sandwich.
A flavorful, appealing snack for children is called "Ants on a Log"; a celery stick is the "log", and raisins arranged in a row along a base of peanut butter are the "ants".
Plumpy'nut is a peanut butter-based food used to fight malnutrition in famine stricken countries. A single pack contains 500 calories, can be stored unrefrigerated for 2 years, and requires no cooking or preparation.
By placing a medium amount of peanut butter inside the opening of a hollow sturdy chew toy, it is easy to create a toy that will keep a dog occupied trying to extract the peanut butter.
A common, simple outdoor bird feeder can be made by coating a pine cone once with peanut butter, then again with birdseed.


== Other names ==
A slang term for peanut butter in World War II was "monkey butter".
In the Netherlands peanut butter is called pindakaas (peanut cheese) rather than pindaboter (peanut butter) because the word butter is only supposed to be used with products that contain actual butter.


== See also ==

Peanut sauce
Almond butter
Cashew butter
Hazelnut butter
List of butters
Sesame butter
Sunflower butter
Peanut butter and jelly sandwich


== References ==


== Further reading ==
Krampner, Jon (2013). Creamy and Crunchy: An Informal History of Peanut Butter, the All-American Food. Columbia University Press. ISBN 9780231530934. 


== External links ==
The National Peanut Board
US Code of Federal Regulations, Peanut butter; part 164.150; last amended 24 March 1998
The USDA's Commercial Item Description for peanut butter and peanut spread (PDF) (Last accessed September 3, 2008)
How Products are Made: Volume 1: Peanut Butter (Last accessed October 16, 2009)