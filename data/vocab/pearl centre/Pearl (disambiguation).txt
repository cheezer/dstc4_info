A pearl is a hard object produced by mollusks.
Pearl may also refer to:


== People ==
Pearl (given name)
Pearl (surname)
Pearl Aday (born 1975), American singer-songwriter
Pearl (drag queen) (born 1991/1992), stage name of drag performer and record producer Matthew James Lent
Nicknamed Pearl
Earl Monroe (born 1944), a.k.a. "Earl The Pearl", American basketball player
Janis Joplin (1943–1970), American singer-songwriter


== Places ==
Pearl, Ontario, Canada
Pearl, Illinois, United States
Pearl, Kansas, United States
Pearl, Michigan, United States
Pearl, Mississippi, United States
Pearl, United States Virgin Islands
Pearl Rocks, Antarctica
Pearl Islands, Panama
The Pearl-Qatar, an artificial island in Qatar


== Literature ==
The Pearl (novel), by John Steinbeck
Pearl (poem), a 14th-century Middle English work
Pearl (literary magazine), a journal founded in 1974
The Pearl (magazine), a pornographic publication published 1879-1880
P.E.A.R.L. Awards, an award for paranormal romantic literature


== Music ==
Performers
Pearl Bailey, American jazz musician
The Pearls, a musical group
Albums
Pearl (album), by Janis Joplin
Pearls (Elkie Brooks album)
Pearls (Ronnie Drew album)
The Pearl (album), by Harold Budd and Brian Eno
Pearls, a 1995 album by David Sanborn
Songs
"Pearl", a song by Chapterhouse from their album Whirlpool
"Pearl", a song by Katy Perry from her album Teenage Dream
"Pearl", a song by Tommy Roe
Others
Pearl Award, an award of the Faith-centered Music Association
Pearl Drums, a musical instrument manufacturer


== Film and television ==
Pearl (TV series), a 1996-1997 sitcom
Pearl (miniseries), a 1978 TV series about the attack on Pearl Harbor
Pearl (One Piece), a character in the One Piece manga
Pearl (SpongeBob SquarePants), a SpongeBob SquarePants character
The Pearl (DHARMA Initiative), a research station on the TV series Lost
Pearl Forrester, a Mystery Science Theater 3000 character
TVB Pearl, an Hong Kong TV channel
La perla (film) (The Pearl), a 1947 film adaptation of the Steinbeck novel
Pearl, a character from a cartoon series, Steven Universe
Pearl, a Little Mermaid character
Pearl, a character from Disney's Home on the Range
Pearl, a competitor on season 7 of RuPaul's Drag Race


== Ships ==
HMS Pearl, the name of several Royal Navy ships
MS Pearl, a cruise ship built in 1967, originally called the MS Finlandia, now the MS Golden Princess
Norwegian Pearl, a cruise ship built in 2006
USS Pearl, the name of more than one United States Navy ship
The Pearl, a schooner in the Pearl incident, a slave escape attempt


== Other uses ==
Pearl (color)
Pearl (culfest)
Pearl (DART station)
pearl (typography), the type size between agate and diamond
PEARL (programming language)
Pearl (cultural festival), the annual cultural festival of BITS Pilani Hyderabad Campus
Pearl (radio play), by John Arden
The Pearl (Edmonton), a residential tower
BlackBerry Pearl, a smartphone
E Ink Pearl, an electronic paper technology
Pokémon Pearl, a video game
Tapioca pearls
Pearl Art and Craft Supply
Pearl barley, barley processed to remove its hull and bran
Pearl Brewing Company
Pearl Group, an insurance service provider
Pearl Index, a birth control research technique
Pearl the Observation Car, a Starlight Express character
Perlan (Icelandic for "Pearl"), a Reykjavík building
Partnership for Enhancing Agriculture in Rwanda through Linkages
The Pearl, an 1867 U.S. Supreme Court case
Polar Environment Atmospheric Research Laboratory, a laboratory at Eureka, Nunavut, Canada
Pearl, a printing press made by Golding & Company
a barley (Hordeum vulgare) cultivar


== See also ==
Paarl
Perl (disambiguation)
Purl (disambiguation)
Pearl District, Portland, Oregon
Pearl Harbor, Hawaii
Pearl River (disambiguation)
Pearl necklace (disambiguation)
All pages beginning with "Pearl"
All pages with titles containing "Pearl"