Pearl hunting is the act of recovering pearls from oysters in the sea. Pearl hunting used to be the prime industry in the Persian Gulf region and Japan.


== History ==

Before the beginning of the 20th century, the only means of obtaining pearls was by manually gathering very large numbers of pearl oysters or mussels from the ocean floor or lake or river bottom. The bivalves were then brought to the surface, opened, and the tissues searched. More than a ton were searched in order to find at least 3-4 quality pearls.
In order to find enough pearl oysters, free-divers were often forced to descend to depths of over 100 feet on a single breath, exposing them to the dangers of hostile creatures, waves, eye damage, and drowning, often as a result of shallow water blackout on resurfacing. Because of the difficulty of diving and the unpredictable nature of natural pearl growth in pearl oysters, pearls of the time were extremely rare and of varying quality. The Great Depression in the United States made it hard to get good prices for pearl shell. The natural pearls found from harvested oysters were a rare bonus for the divers. Many fabulous specimens were found over the years. By the 1930s, overharvesting had severely depleted the oyster beds. The government was forced to strictly regulate the harvest to prevent the oysters from becoming extinct, and the Mexican government banned all pearl harvesting from 1940 to 1960.
In Asia, some pearl oysters could be found on shoals at a depth of 5–7 feet (1.5–2 meters) from the surface, but more often divers had to go 40 feet (12 meters) or even up to 125 feet (40 meters) deep to find enough pearl oysters, and these deep dives were extremely hazardous to the divers. In the 19th century, divers in Asia had only very basic forms of technology to aid their survival at such depths. For example, in some areas they greased their bodies to conserve heat, put greased cotton in their ears, wore a tortoise-shell clip to close their nostrils, gripped a large object like a rock to descend without the wasteful effort of swimming down, and had a wide mouthed basket or net to hold the oysters.
For thousands of years, most seawater pearls were retrieved by divers working in the Indian Ocean, in areas such as the Persian Gulf, the Red Sea, and in the Gulf of Mannar (between Sri Lanka and India). A fragment of Isidore of Charax's Parthian itinerary was preserved in Athenaeus's 3rd-century Sophists at Dinner, recording freediving for pearls around an island in the Persian Gulf.
Pearl divers near the Philippines were also successful at harvesting large pearls, especially in the Sulu Archipelago. In fact, pearls from the Sulu Archipelago were considered the "finest of the world" which were found in "high bred" shells in deep, clear, and rapid tidal waters. At times, the largest pearls belonged by law to the sultan, and selling them could result in the death penalty for the seller. Nonetheless many pearls made it out of the archipelago by stealth, ending up in the possession of the wealthiest families in Europe. Pearling was popular in Qatar, Bahrain, and some areas in Persian Gulf countries.
In a similar manner as in Asia, Native Americans harvested freshwater pearls from lakes and rivers like the Ohio, Tennessee, and Mississippi, while others successfully retrieved marine pearls from the Caribbean and waters along the coasts of Central and South America.
In the time of colonial slavery in northern South America (off the northern coasts of modern Colombia and Venezuela), a unique occupation amongst slaves was that of a pearl diver. A diver's career was often short-lived because the waters being searched were known to be shark-infested, resulting in frequent attacks on divers. However, a slave who discovered an extra-large pearl could sometimes purchase his freedom.


== Present ==
Today, pearl diving has largely been supplanted by cultured pearl farms, which use a process widely popularized and promoted by Japanese entrepreneur Kokichi Mikimoto. Particles implanted in the oyster encourage the formation of pearls, and allow for more predictable production. Today's pearl industry produces billions of pearls every year. Ama divers still work, primarily now for the tourist industry.
Pearl diving in the Ohio and Tennessee rivers of the United States still exists today.


== See also ==

Ama divers – female Japanese divers
Fijiri – vocal music of the Gulf pearl diver
Culture of Eastern Arabia
Paravar
Pearl farming industry in China
Pearling in Western Australia
Zubarah – a Qatari pearl fishing and trading port in the 18th and 19th centuries


== Bibliography ==
Al-Hijji, Yacoub Yusuf (2006). Kuwait and the Sea. A Brief Social and Economic History. Arabian Publishing. ISBN 978-0-9558894-4-8. 
al-Shamlan, Saif Marzooq (2001). Pearling in the Arabian Gulf. A Kuwaiti Memoir. The London Centre of Arab Studies. ISBN 1-900404-19-2. 
Bari, Hubert; Lam, David (2010). Pearls. SKIRA / Qatar Museums Authority. ISBN 978-99921-61-15-9.  Especially chapter 4 p. 189-238 The Time of the Great Fisheries (1850-1940)
Ganter, Regina (1994). The Pearl-Shellers of Torres Strait: Resource Use, Development and Decline, 1860s-1960s. Melbourne University Press. ISBN 0-522-84547-9. 
George Frederick Kunz: Book of the Pearl (G.F. Kunz was America's leading gemologist and worked for Tiffany's in the beginning of the 20th century)


== References ==


== External links ==
American Museum of Natural History