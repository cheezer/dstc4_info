Nasi pecel is a Javanese rice dish served with pecel (cooked vegetables and peanut sauce). The vegetables are usually kangkung or water spinach, long beans, cassava leaves, papaya leaves, and in East Java often used kembang turi. It tastes best when eaten with fried tempeh and traditional cracker called peyek. It is popular in East and Central Java.


== References ==


== See also ==
Nasi bogana
Nasi campur
Nasi goreng
Nasi kucing
Nasi kuning
Nasi lemak
Nasi uduk
Nasi ulam
List of Indonesian dishes


== External links ==