Peanut sauce, satay sauce, bumbu kacang, sambal kacang, or pecel is a sauce made from ground roasted or fried peanuts, widely used in the cuisines of Indonesia, Malaysia, Thailand, Vietnam, China and Africa. It is also used, to a lesser extent, in European (particularly Dutch) and Middle Eastern cuisine. It is also used in Filipino cuisine but rarely.
Peanut sauce goes well with chicken, meat and vegetables. It is often used to add flavour to grilled skewered meat such as satays, poured over vegetables as salad dressing such as in gado-gado, or as dipping sauces for spring rolls.


== Ingredients ==
The main ingredient is ground roasted peanuts, for which peanut butter can act as a substitute. Several different recipes for making peanut sauces exist, resulting in a variety of flavours, textures and consistency. A typical recipe usually contains ground roasted peanuts or peanut butter (smooth or crunchy), coconut milk, soy sauce, tamarind, galangal, garlic, and spices (such as coriander seed, cumin, etc.). Other possible ingredients are chili peppers, sugar, milk, fried onion, and lemon grass. The texture and consistency (thin or thick) of a peanut sauce corresponds to the amount of water being mixed in it. Some recipes create thin or watery peanut sauce, while others make thick or gloppy peanut sauce.
In Western countries, the readily and widely available peanut butter is often used as a substitute ingredient to make peanut sauce, however the texture of peanut butter is too smooth and soft. To achieve authenticity, some recipes might insist on making roasted ground peanuts from scratch, using traditional stone mortar and pestle for grinding to achieve desired texture, graininess and earthy flavour of peanut sauce.


== Regional ==

One of the main characteristics of Indonesian cuisine is the wide applications of bumbu kacang (peanut sauce) in many Indonesian signature dishes such as satay, gado-gado, karedok, ketoprak, asinan and pecel. It is usually added to main ingredients (meat or vegetable) to add taste, used as dipping sauce such as sambal kacang (a mixture of ground chilli and fried peanuts) for otak-otak or ketan or as a dressing on vegetables. Introduced from Mexico by Portuguese and Spanish merchants in the 16th century, peanuts found a place within Indonesian cuisine as a popular sauce. Peanuts thrived in the tropical environment of Southeast Asia, and today, they can be found roasted and chopped finely, topping a variety of dishes and in marinades and dipping sauces. Peanut sauce reached its sophistication in Indonesia, with the delicate balance of taste acquired from various ingredients according to each recipe of peanut sauce; fried peanuts, gula jawa (palm sugar), garlic, shallot, ginger, tamarind, lemon juice, lemongrass, salt, chilli, pepper, sweet soy sauce, ground together and mixed with water to acquire right texture. The secret to good peanut sauce is "not too thick and not too watery." Indonesian peanut sauce tends to be less sweet than the Thai one (which is a hybrid adaptation). Gado-gado is eaten with peanut sauce throughout Indonesia showcasing the delicate balance of sweet, spicy and sour.
Through its former possessions in South East Asia, peanut sauce has become a common side dish in the Netherlands. Besides being used in certain traditional Indonesian and Dutch-Indonesian dishes, it has found its way in to a purely Dutch context when it is eaten during, for instance, a (non Asian style) barbecue or with French fries. A popular combination at Dutch fast food outlets is French fries with mayonnaise and peanut sauce (often with raw chopped onions), called a Patat Oorlog (lit. "French fries War"). Peanut sauce is also eaten with baguette, bread, cucumber or potatoes. It is also used as an ingredient in the deep-fried snack food called Satékroket, a croquette made with a flour-thickened ragout based on Indonesian satay.
In Chinese cooking, the sauce is often used on grilled meat. Other uses include hot pot and Dan dan noodles. In Singapore, peanut sauce is not only used as dipping sauce for satay. It is also eaten with rice vermicelli known as Satay bee hoon. In Vietnam, it is called tương đậu phộng and is used in cuốn diếp dish.
Commercial versions of the usually home made sauce are available in a number of countries. In Australia, common brands include Fountain and Ayam brand.
In the United States, common brands include House of Tsang, Annie Chun's, and Sajen.


== See also ==
Peanut butter
Sambal
Shacha sauce
Siu haau sauce


== References ==


== External links ==
Martha Stewart's Peanut Sauce Recipe
Indonesian-Spiced Peanut Sauce Recipe
Peanut Sauce – Recipe for Satay Sauce from About Food
Peanut Sauce Recipe at Cooking with Amy weblog