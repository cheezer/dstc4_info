John Peck may refer to:
John Peck (Australian rules footballer) (1937–1993) Australian rules footballer, played for Hawthorn, 1954–1968
John Peck (athlete), Canadian athlete at the 1904 Summer Olympics
John Peck (diplomat), British ambassador to Senegal, 1962–1966, and Ireland, 1970–1973
John Peck (poet) (b. 1941), American poet
John C. Peck (1828–?), American businessman and building contractor
John E. L. Peck (1918–2013), first permanent head of the computer science department at the University of British Columbia
J. Eddie Peck, full name John Edward Peck (born 1958), American actor
John H. Peck (1838–1919), tenth president of Rensselaer Polytechnic Institute
John J. Peck (1821–1878), U.S. soldier who fought in the Mexican-American War and American Civil War
John Mason Peck (1789–1858), American Baptist missionary
John Weld Peck (1874–1937), American judge
John Weld Peck II (1913–1993), American judge