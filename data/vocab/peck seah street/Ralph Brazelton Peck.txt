Dr. Ralph Brazelton Peck (June 23, 1912 – February 18, 2008) was an eminent civil engineer specializing in soil mechanics. He died on February 18, 2008 from congestive heart failure. He was awarded the National Medal of Science in 1975 "for his development of the science and art of subsurface engineering, combining the contributions of the sciences of geology and soil mechanics with the practical art of foundation design."
Peck was born in Winnipeg to O.K. and Ethel Peck, and moved to the United States at age six. In 1934 he received his Civil Engineer degree from Rensselaer Polytechnic Institute and was given a three year fellowship for graduate work in structures. On June 14, 1937 he married Marjorie Truby and obtained a Doctor of Civil Engineering degree.
After receiving his degree, he worked briefly for the American Bridge Company, then on the Chicago Subway, but Peck spent the majority of his teaching career (32 years) at the University of Illinois, initially in structures but later focused on civil engineering under the influence of Karl Terzaghi, ultimately retiring in 1974. He continued to work until 2005 and was highly influential as a consulting engineer, with some 1,045 consulting projects in foundations, ore storage facilities, tunnel projects, dams, and dikes, including the Cannelton and Uniontown lock and dam construction failures on the Ohio River, the dams in the James Bay project, the Trans-Alaska Pipeline System, the Dead Sea dikes and the Rion-Antirion Bridge in Greece.
On May 8, 2008, the Norwegian Geotechnical Institute in Oslo, Norway opened the Ralph B. Peck Library. This Library is next to the Karl Terzaghi Library, also at NGI. Correspondence between these two men are part of the two working libraries. The Karl Terzaghi Library tells about the birth and growth of soil mechanics. The Ralph B. Peck Library tells about the practice of foundation engineering, and how one engineer exercised his art and science for more than sixty years. Diaries from between 1939-1941 containing Peck's work with the Chicago Subway are included along with papers and reports on many of his subsequent jobs.
During his career Peck authored over 200 publications, and served as president of the International Society of Soil Mechanics and Foundation Engineering from 1969 to 1973. He received many awards, including:
1944 The Norman Medal of the American Society of Civil Engineers
1965 The Wellington prize of the ASCE
1969 The Karl Terzaghi Award
1975 The National Medal of Science, presented by President Gerald Ford
1988 The John Fritz Medal
In 1999, the American Society of Civil Engineers (ASCE) created the Ralph B. Peck Award to honor outstanding contributions to geotechnical engineering profession through the publication of a thoughtful, carefully researched case history or histories, or the publication of recommended practices or design methodologies based on the evaluation of case histories.


== References ==
^ "Geotechnical Engineers Hall of Fame". Electronic Journal of Geotechnical Engineering. Retrieved 2009-05-07. 
^ "Professor Ralph Peck's Legacy Website". Geoengineer.org's educational websites. 


== Further reading ==
Norwegian Geotechnical Institute retrospective
"Ralph B. Peck, Educator and Engineer – The Essence of the Man", edited by John Dunnicliff and Nancy Peck Young, BiTech Publishers Ltd, Vancouver, CA, 2006


== External links ==
Obituary in The Times, 12 March 2008
Observational Method Technology Review, InfoMine.com