Trinity Peninsula is the northernmost part of the Antarctic Peninsula, It extends northeastward for about 130 km (81 mi) to Cape Dubouzet from an imaginary line connecting Cape Kjellman on the north-west coast and Cape Longing on the south-east coast. Prime Head is the northernmost point of this peninsula.
It was first sighted on 30 January 1820 by Edward Bransfield, Master, Royal Navy, immediately after his charting of the newly discovered South Shetland Islands nearby. In the century following the peninsula's discovery, chartmakers used various names (Trinity Land, Palmer Land, and Land of Louis Philippe) for this portion of it, each name having some historical merit. The recommended name derives from "Trinity Land", given by Bransfield during 1820 in likely recognition of the Corporation of Trinity House, Britain's historical maritime pilotage authority, although the precise application by him has not been identified with certainty and is a matter of different interpretation by Antarctic historians.


== Map ==
Trinity Peninsula. Scale 1:250000 topographic map No. 5697. Institut für Angewandte Geodäsie and British Antarctic Survey, 1996.


== External links ==
Composite Gazetteer of Antarctica – ID 14958: Trinity Peninsula