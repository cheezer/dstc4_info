"Teddy Bears' Picnic" is a song consisting of a melody by American composer John Walter Bratton, written in 1907, and lyrics added by Irish songwriter Jimmy Kennedy in 1932. It remains popular as a children's song, having been recorded by numerous artists over the decades. Kennedy lived at Staplegrove Elm and is buried in Staplegrove Church, Taunton, Somerset, England. Local folklore has it that the small wooded area between the church and Staplegrove Scout Hut was the inspiration for his lyrics.


== Background ==
Bratton composed and personally copyrighted it in 1907, and then assigned the copyright to M. Witmark & Sons, New York, who published it later that year as "The Teddy Bears Picnic. Characteristic Two Step", according to the first page of the published piano score, as well as the orchestral parts Witmark published in an arrangement by Frank Saddler. However, the illustrated sheet music cover gives the title as THE TEDDY BEARS' PICNIC, with apostrophe on "BEARS" and no genre descriptor. Irish songwriter Jimmy Kennedy wrote the now familiar lyrics for it in 1932.
After Bratton wrote "The Teddy Bears' Picnic", however, many people felt that the composer plagiarized portions of the melody. Music aficionados pointed out in particular that the refrain echoed the theme from Robert Browne Hall's 1895 "Death or Glory March". Nevertheless, charges were not filed and Bratton's song still has the same tune it had in 1907.
The first recording of the piece was by the Edison Symphony Orchestra, made at Edison Records' "New York Recording Department" studio, 79 5th Avenue, New York, in November 1907 and was released as Edison two-minute cylinder 9777 in March 1908, as announced on page 3 of the January, 1908 issue of The Edison Phonograph Monthly (vol. VI, no. 1). Arthur Pryor's Band made the work's first disc recording for the Victor Talking Machine Company in Camden, New Jersey, on September 14, 1908. Take 2 from that session was released in November 1908 as Victor single-faced disc 5594 and as side A of the company's first double-faced disc 16001, with the title on the label reading "The Teddy Bears' Picnic/Descriptive Novelty". An early UK recording was made by the Black Diamonds Band for Zonophone records in 1908.
The first vocal version was recorded in 1932 by Henry Hall and His Orchestra (EMI SH 172), with Val Rosing singing Kennedy's lyrics. The song was subsequently recorded by Bing Crosby, The Nitty Gritty Dirt Band, Rosenshontz, Frank DeVol, Jackie Lynton, Rosemary Clooney, Dave Van Ronk, Jerry Garcia, John Inman, Trout Fishing in America, and Anne Murray.


== In popular culture ==
As a two-step, it appears briefly in the musical score by Robert Israel to accompany Buster Keaton's 1926 silent film The General.
The song was occasionally used as background music in the cartoon series Looney Tunes.
The song appears in the movie adaptation of Fear and Loathing in Las Vegas as the backing music during the Bazooko Circus scene.
In the late 1940s and early 1950s a version performed by organist Ethel Smith was used as the theme song for the Big Jon and Sparkie radio program, a children's show presented on weekdays and Saturday mornings. The Saturday show was later named "No School Today".
The wartime BBC sitcom Dad's Army uses the song in an episode from 1970 called The Big Parade.
In 1983 Green Tiger Press turned Kennedy's lyrics into a children's book, with illustrations by company co-founder Sandra Darling (under the name Alexandra Day). Original printings included a 7" record, with the Bing Crosby recording on the A side, and a recording by a local klezmer band, dubbed "The Bearcats", on the B side.
The Val Rosing/Henry Hall version is perhaps the best-known in the UK and was featured in the Dennis Potter drama The Singing Detective from 1986.
The song features prominently in Peter Greenaway's 1986 film A Zed & Two Noughts, and is sung by characters in the film.
Composer Grant Kirkhope concluded that some of his music from the video game Banjo-Kazooie had been based on "Teddy Bear's Picnic".
The Val Rosing/Henry Hall version was used as background music in a 2006 Microsoft Xbox commercial.
A reversed sample from the song was included by Boards of Canada in the track "A is to B as B is to C" from their album Geogaddi.
Teddy Bear's Picnic is sung to Boog (Martin Lawrence) in Open Season as a lullaby. Later on in the movie, Elliot (Ashton Kutcher), sings his revision of the song. Shaw (Gary Sinise) also sings the song while searching for Boog after Boog accidentally breaks into Shaw's cabin.
The Val Rosing/Henry Hall version was used in a 2011 Audi commercial to advertise a night vision feature.
Swedish witch house group V△LH△LL prominently sample the original recording in their song "Ɖ0Ɯƞ Iƞ ƚhę ƜǪoƉƽ."
In the 2012 Pretty Little Liars Halloween episode, Mona sings a line from it in the opening scene.
It is sung by the vampire Angelus in the hit TV series Angel
"Teddy Bears' Picnic" is mentioned in the British historical crime drama television series Peaky Blinders during the third episode of the second season.


== Use by BBC radio engineers ==
The 1932 Henry Hall recording was of especially good quality with a large tonal range. It was used for more than 30 years by BBC audio engineers (up until the early 1960s) to test and calibrate the frequency response of audio equipment.


== References ==


== External links ==
"Visit Somerset (Nursery Rymes & Children's TV) - visitsomerset.co.uk". Retrieved 4 Jun 2012. 
"Remembering Val Rosing - The Man Who Sang Teddy Bears Picnic - valrosing.com". Retrieved 4 Jun 2012. 
Teddy Bears' Picnic on YouTube
"Teddy Bears' Picnic" Mp3 and lyrics
Animation: Teddy Bears' Picnic (The Teddy Bear Male Voice Choir)