The Pig's Eye Brewing Company, established in 2002 in Saint Paul, Minnesota, brew the "Pig's Eye" brand of beers and the "Pit Bull" beer. The company was established when the former Minnesota Brewing Company, which had introduced the beer in 1992, folded due to financial troubles. The beer is named after Pierre "Pig's Eye" Parrant, a probably French-Canadian man who became the first resident of Saint Paul and operated a tavern out of Fountain Cave near what is now downtown.
The Pig's Eye Brewing Company brews four beers:
Pig's Eye Pilsner
Pig's Eye Lean Light
Pig's Eye Ice
Pit Bull
The Pig's Eye Beers have received awards at the Great American Beer Festival, Between The Bluffs Beer Festival, and the World Beer Championships. BeerAdvocate has rated their beers at between C and D on an A-F scale.


== References ==