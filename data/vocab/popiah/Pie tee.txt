Pie Tee is a thin and crispy pastry tart shell kuih filled with a spicy, sweet mixture of thinly sliced vegetables and prawns. It is a popular Peranakan dish. The shells are made of flour and though some stores will make them from scratch, they can usually be found ready made in most supermarkets. Similar to popiah, the main filling is shredded Chinese turnips and carrots, and usually these two dishes are sold by the same stall in hawker centers.


== See also ==
Peranakan cuisine


== References ==