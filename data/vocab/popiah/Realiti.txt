Realiti is a Malaysian television series starring Chelsia Ng, Ashraf Sinclair, Melissa Maureen, Alvin Wong, Azizan Nin, Radhi Khalid and Maria Farida. It is produced by Ng Ping Ho under Popiah Pictures. Its first season which was aired on 8TV, having run from 3 September to 5 November 2006. 


== Plot ==
This drama series revolves 5 contestants of the fictional Idol-styled television singing contest called Malaysian Star. Former rock star Mac assumes responsibility as the vocal coach for the contestants, Amir, Melody, Burn, Nickson, and Baby. The story also depicts the hard path which the contestants take to become the champion of a competition. Each of them has their deep secret, dark past and complicated background. Only one of them would emerge winner.


== Characters ==


=== Malaysian Star contestants ===
Amir (Zizan Nin) – he started out as a bellboy and a lounge performer in the hotel in which the Malaysian Star contestants stayed. After one of the original five contestants fainted while performing live on stage and declined to continue her journey in the competition, an emergency audition was held to find a replacement and he was eventually selected to replace her, though in a less conventional way. Amir comes from a village in Kedah and left for Kuala Lumpur after being estranged with his family. Being not fluent in English, Amir does have problems carrying English songs. Amir is notably a fan of OAG, oft-seen singing the band's songs in the series.
Melody Yeoh (Chelsia Ng) – Hailing from Penang, touted as the contestant with the best vocals, she is not happy with the path she was forced to take by her father, who is a vocal coach himself. She was induced to musical and singing skills the different way from other youths, which made her fellow schoolmates astonished by her ignorance of Britney Spears (as expressed in one episode).
Burn Ramli (Ashraf Sinclair) – the hip-hop boy from Petaling Jaya who stood out amongst the contestants with his charm toward young female fans, yet he has an aggressive, brawl-inclined character which threatens to put him and his image into trouble. He mixed with other hip-hop-imaged youths who turned out to be drug dealers.
Nickson Wong (Alvin Wong) – a male hairstylist from Kuala Lumpur with an explicitly girlish personality. Although looking smiley most of the time, he suffers from domestic problems, in which his father has been abusive towards his mother.
Baby (Melissa Maureen) – also from Kuala Lumpur, an ex-fashion model who attracts fans with her popular establishment and pretty looks. Again, behind all this glam is a dark secret and complicated past that she has been striving to hide from the public eyes and ears — it was later known that she gave birth to a child outside the wedlock.
Jessie (Celina Khor) — the original fifth contestant, who fainted on-stage during the starlet's first performance, and was eventually replaced by Amir.


=== Malaysian Star Crew and Proxies ===
Mac (Radhi Khalid) – he was a rock star in the 1980s, now he lands a tough job as the vocal coach of the Malaysian Star. His strict disciplinarian approach made him compared to a drill sergeant in the military infantry, as said by Burn in the series.
Jay (Nazruddin Habibur Rahman) – the scheming producer of the contest and manager of Rock Records. In the series he had expressed a wish to set up his own record company and whisk one of the contestants into that new label.
Malaysian Star Host (Aishah Sinclair) – self-explanatory.
Pop TV Host (Juliana Ibrahim) – in the show, her job is to interview the participants and members of the public on the streets for comments about the show, as well as assistant host in studio.
Malaysian Star judges
FlyGuy
Bernie Chan (only appears during the emergency auditions)
Carmen Soo
Phat Fabes (only appears during the Showdowns)

Wahida (Maria Farida) – a tabloid reporter who seems aggressive and only wants to scoop on controversial issues pertaining to the contest and its participants, yet she has a kind heart


=== The contestants' family and friends ===
David Yeoh (Kee Thuan Chye) – Melody's father, a vocal trainer who pushes his daughter to pursue a musical career against her will.
Dayang (Lydia Ibtisam) – Amir's flame while working in the hotel, whose relationship with him falls apart gradually since he joined Malaysian Star.
Calvin (Gambit Saifullah) – a close aide of Nickson, with whom he paired up to form a musical group and compose together. The duo have themselves accused of being gay while caught by the reporters and photographers holding hands in a rather "uncouth" manner.
Fazrina (Cheyenne Stutzriem) – Baby's "niece" who really is Baby's child born out of the wedlock. The girl is under the custody of one of Baby's friends.
Joe (Reefa) – Burn's hip-hopping fellow with a shady background. Toward the end of season one he was found out to be a drug trafficker who once sold to Melody, finding out which Burn brwaled and broke up with him.


== Episodes ==


=== Season 1 ===
Episode 1 (aired 3 September 2006)
Five contestants of the singing competition Malaysian Star face the reality that if they want to be stars, they have to behave like one whatever their personal problems are. On their first 'live' performance, one of them fainted on stage and eventually declined to continue her journey.
Episode 2 (aired 10 September 2006)
An emergency audition was held to look for a replacement in the competition, and eventually Amir was selected, yet in an unexpected manner. Burn's rebellious streak begins to show, Baby and Melody fight, and all this under a reporter's prying eye.
Episode 3 (aired 17 September 2006)
It's time to meet the fans, but the five contestants learn that fame has its price. Melody and Burn find each other pleasant distractions, Amir's willingness to help gets him in trouble with Dayang, and Baby meets a face from her past.
Episode 4 (aired 24 September 2006)
While the contestants strut their stuff for a photoshoot, Amir runs back to his kampung. Jay, a producer with Malaysian Star, tries to spin the story to the show's advantage. Meanwhile, Baby does her best to convince a photographer to put her on the cover and Burn tries to get Melody to come out of her shell.
Episode 5 (aired 1 October 2006)
The contestants are to perform an unplugged concert, but it is overshadowed when Baby gets a poison pen letter. Amir's confidence takes a blow when he has to perform an English song, Nickson argues with Calvin about how he should present himself. Meanwhile, Melody and Burn get even more drawn to one another.
Episode 6 (aired 8 October 2006)
It's only a week until the first Malaysian Star showdown, but the contestants have more than just elimination on their minds. Baby has to contend with a blackmail letter from someone who knows a secret from her past, Burn discovers Melody's dependence on drugs and Nickson's mum sets him up on a blind date.
Episode 7 (aired 15 October 2006)
The first showdown begins and one contestant will be eliminated. But before that, Mac discovers Melody's drug problem and Burn fights to keep her on the show, Amir gets a chance to sign a recording contract but only if he does not win Malaysian Star, and Nickson is unhappy that his mum has moved back in with his dad.
Eventually, it is Burn who received the elimination and he fumes in front of the camera in the aftermath.
Episode 8 (aired 22 October 2006)
Melody gets hospitalised as a result of a drug overdose and Burn is brought back into the competition. Nickson's mother moves in with him to escape her abusive husband and Baby and Amir's relationship grows closer.
Episode 9 (aired 29 October 2006)
Amir finds himself as the man in the middle when Baby suspects Dayang of stealing her photograph of Fazrina, while Burn reluctantly agrees to help Joe sell drugs in order to raise money to help Melody.
Episode 10 (aired 5 November 2006)
Melody wakes up from her coma and rejoins the competition, Baby confronts her relationship with Fazrina and Amir clashes with Jay again. And all this with the grand finale as well!
During the finalé, Mac confronts Jay regarding the latter's plot to rig the votes. Eventually, Melody wins the competition and the recording contract with Rock Records. At the final moments of the episode, Burn was deliberately run over by a car driven by Joe.


== Soundtrack ==
Theme song
Scarecrow Adams (Disagree)
Showdown 1 (Episode 7)
Amir: Venusia (OAG)
Baby: Baby
Burn: Tiada Lagi Cinta (Ruffedge)
Melody: Ratu Hati
Nickson: Detik Bersamamu
Showdown 2 (Episode 8)
Amir: Venusia (OAG)
Baby: Meant To Be
Burn: Khayalan
Nickson: Katakan Saja
Showdown 3 (Episode 9)
Baby: Pernah (Ferhad)
Amir: NowWhy2 (OAG)
Burn: Ayu (V.E)
Grand finalé (Episode 10)
Top 3 group performance: Best of Them All (Douglas Lim)
Baby: Selamanya (Douglas Lim)
Melody: Looking Down (Douglas Lim)
Amir: Akustatik (OAG)


== External links ==
dzof.org: Realiti, a review site done by Dzofrain Azmi, scriptwriter of this show