Pork rind is the skin (rind) of a pig. Pork rind is a common ingredient of sausages, which helps to improve their consistency. Fried or roasted pork rind and fat is also a snack. The frying renders much of the fat that is attached to the uncooked rind, causing the cooked product to reduce considerably in size. The product may be known by alternative names, such as pork scratchings or pork crackling in the UK, although the term crackling may also refer to the rind atop a roasted pork joint.


== Snack ==
Often a by-product of the rendering of lard, it is also a way of making even the tough skin of a pig edible. In many ancient cultures, animal fats were the only way of obtaining oil for cooking and it was common in many people's diet until the industrial revolution made vegetable oils more common and more affordable.
Microwaveable pork rinds are sold in bags that resemble microwaveable popcorn (although not exhibiting the popping sound) and can be eaten still warm. Pickled pork rinds, on the other hand, are often enjoyed refrigerated and cold. Unlike the crisp and fluffy texture of fried pork rinds, pickled pork rinds are very rich and buttery, much like foie gras.


== Preparation ==
For the large scale production of commercial pork rinds, frozen, dried pork skin pellets are used. They are first rehydrated in water with added flavouring, and then fried in pork fat at 200-210 degrees Celsius. Cooking makes the rinds expand and float on the oil surface. The rinds are then removed from the fat, flavoured and air dried. Antioxidants may be added to improve stability.


== Nutritional value ==
Like many snack foods, pork rinds are high in sodium and fat, however, they are low in carbohydrates and are sometimes considered an alternative snack food for those following the Atkins diet. According to Men's Health, a one-ounce (28 g) serving contains nine times the protein and less fat than is found in a serving of potato chips, which are much higher in carbohydrates. They add that 43 percent of pork rind's fat is unsaturated, and most of that is oleic acid, the same healthy fat found in olive oil. Another 13 percent of its fat content is stearic acid, a type of saturated fat that is considered harmless because it does not raise cholesterol levels. A 60g serving of pork rind contains 29g of fat, 375 kcal and 0.65g of sodium. However, pork rinds are considered an incomplete source of protein because they contain very low amounts of some essential amino acids, including methionine, tryptophan and histidine. 


== Regional variations ==


=== Americas ===


==== Canada ====
Scrunchions is a Newfoundland term for small pieces of pork rind or pork fatback fried until rendered and crispy. They are often used as a flavouring over other foods, such as salt fish and potatoes, and mainly used as a condiment for fish and brewis.
In Quebec, they are often called oreilles de Christ (Christ ears) and are eaten almost exclusively as part of traditional cabane à sucre meals.


==== Mexico ====
Mexico is one of the world's largest producers and consumers of pork rinds, known as chicharrón it may still have fat attached, called chicharrón con gordo "pork rind with fat" (as in overweighed).
It is commonly served in homes across Mexico. It can be served in a soup sometimes called "chicharrón en salsa" (pork rind with sauce) or "salsa de chicharrón" (pork rind sauce). It is often served as an appetizer, or even offered as snack in family reunions. However, chicharrones can be purchased on the street and are, usually, eaten with hot sauce and lime juice.
One popular breakfast is salsa de chicharron, (also chicharrón en salsa in some regions) cooked in green tomato or tomato salsa spiced with epazote. If liquid is drained then is used in tacos, beside other tacos in the same menu: potato tacos, moronga tacos, guacamole tacos, meat tacos, stuffed chile tacos, either as fast food products or kitchen made.
The dryness in pork rind pairs perfectly with humidity and softness in pico de gallo and both fills perfectly a corn tortilla as taco.
While most pork rinds are fried and eaten in crispy way, not all the pork skin reaches the frier, as some of the skin free of fat is pickled in a pork feet recipe.
A by-product in frying rinds is the decanted residues in fryer called "asiento" or "boronas" (grounds). The process requires moving rinds to uniform cooking and while the product dehydrates it cracks, losing small pieces, after collected becomes a thick, fatty salsa, that can be mixed as an ingredient in other salsa de chicharrón recipe or used for savory and fat in pan frying. Obviously a second by-product in frying rinds is lard.
Cueritos are the same as pork rinds but are soft and chewy, as they are not heavily cooked unlike the chicharrón, which is very crispy and often overcooked. They are easily available in Mexico as antojo and sold on the streets usually by butchers, oftentimes served fresh, but you can also find them marinated with vinegar and onion on "tienditas", popular convenience stores where the clerk is usually the owner. If marinated, they are served with lemon and salt, powdered chilly and probably with "salsa Valentina".


==== United States ====
Pork rinds, sometimes cracklings, is the American name for fried or roasted skins of pigs, geese or other animals, regardless of the status or amount of pork in the snack. Pieces of fried meat, skin, or membrane produced as a byproduct of rendering lard are also called cracklings. Cracklings consist of either roasted or fried pork rind that has had salt rubbed into it and scored with a sharp knife: "a crackling offers a square of skin that cracks when you bite into it, giving way to a little pocket of hot fat and a salty layer of pork meat."
Cajun cracklings (or "cracklins") from Cajun cuisine (called "gratons" in Louisiana French), are fried pieces of pork fat with a small amount of attached skin, flavored after frying with a mixture of peppery Cajun spices.
Pork rinds normally refers to a snack food commercially sold in plastic bags. They are made in a two-step process: pork skin is first rendered and dried, and then fried and puffed. These are also called by the Mexican name, chicharrón, in reference to the popular Mexican food.
Pork rinds sold in the United States are occasionally stained with a pink or purple spot. These edible marks are actually USDA stamps used on the skins to mark that they have been inspected, and graded. They are not harmful.
In 2003, sales of pork rinds experienced rapid growth, but they have dropped "by $31 million since 2004, when they reached $134 million, and now make up barely more than 1 percent of the salty snack market."


=== Asia ===


==== Philippines ====
Chicharon (derived from the Spanish chicharrón; also spelled tsitsaron) is usually bought from balut vendors as pulutan (i.e. appetizer dishes usually eaten with alcoholic beverages). It is prepared by deep-frying the dried pork rind with a little salt. It is sometimes eaten with vinegar (chopped chilies and/or soy sauce are added) or with bagoong, lechon liver sauce, or atchara. Chicharong manok, which is made from chicken skin, and chicharong bulaklak (literally 'flower chicharrón', from its distinctive shape) made of pig intestine, are also popular. It is also used as a topping for pancit palabok and pancit malabon and in preparing pork sisig.


==== Thailand ====

Khaep mu (Thai: แคบหมู, pronounced [kʰɛ́p mǔː]), as crispy pork rinds are known in Thai cuisine, are a speciality of the northern Thai city of Chiang Mai. One way of making khaep mu is to first cure the pork skin, with an attached layer of fat, in salt for several days, after which it is soaked in water for a couple of hours. This ensures that the fat cells will expand, resulting in a "puffed skin" after cooking. The slabs of belly fat are then slowly cooked at a low heat in, preferably, lard but other animal fat and vegetable oil can also be used. Similar to a confit, the pork thus treated can be stored. The pork is then cut into smaller pieces and baked in an oven until perfectly crispy. Another method of making the pork rinds again involves salting the pork skin, but instead of soaking it, the skin is hung out to dry in the sun after which it is sliced and deep-fried twice. Yet another way to make this dish in Thailand is to first cut the pork skin into strips, then boil them in water after which they are thoroughly dried before being deep-fried.
Northern Thai people most often eat pork rinds together with different Thai chili pastes, such as nam phrik num (made with grilled green chili peppers) and nam phrik ong (made with dried chili peppers, tomato and minced pork). It can also be eaten as a snack, either on its own, or with nam chim khaep mu, a dipping sauce made with lime juice, fresh chili peppers and a sweet chili paste. It can also figure as an accompaniment to Thai dishes such as Nam ngiao and the famous Thai salad som tam or used crushed as an ingredient, for instance in sa makhuea, a northern Thai salad made with minced pork and Thai eggplant.


==== Vietnam ====
Pork rinds used to be a very common food in Vietnam before the Doi moi program in 1986. Due to various economic difficulties in the pre-Doi moi era, cooking oil and meat were still considered "luxury goods", and consequently fat liquid and pork rind became excellent replacements in Vietnamese daily meals. Nowadays with a better economic situation for the country, pork rind is no longer a substitute food, but rather a special component in many Vietnamese dishes, such as cơm tấm, noodle and snails (bún ốc), noodle soup, etc. In Vietnamese, pork rind is called tóp mỡ, which translates to "dried piece of fat".


=== Europe ===
In France, pork rinds are known as grattons, and are an essential ingredient to some slow-cooked stews, such as cassoulet.
In Spain, they are called cortezas de cerdo when they do not have any solid fat attached, and chicharrones or torreznos when they do. In Catalonia and other Catalan-speaking areas, they are usually called cotnes (sing. cotna), which is the pork rinds per se, when prepared as snacks, whereas llardons (sing. llardó) are a specially prepared, pressed variety. The latter are also known regionally as greixons (sing. greixó) or llardufes (sing. llardufa), among other names.
Portugal has torresmos and couratos, the latter normally on sale at large popular gatherings, such as football matches, usually on a sandwich, and accompanied by beer. In the Netherlands and Belgium, they are known as knabbelspek, which translates to "nibbling bacon". They are usually sold with no flavorings other than salt at most butchers and supermarkets. They are usually eaten as a snack food.
In Denmark, they are known as flæskesvær and can be found in most grocery stores and kiosks. In Austria and Southern Germany, they are known as Grammeln or Grieben, in other German-speaking regions as Schweinekrusten (pig crusts).
United Kingdom Pork scratchings is the British name for deep-fried, salted, crunchy pork rind with fat produced separately from the meat. This is then eaten cold. Pork scratchings typically are heavy and hard, have a crispy layer of fat under the skin, and are flavored only with salt. The pig hair is usually removed by quickly burning the skin of the pig before it is cut into pieces and cooked in hot fat.
Pork scratchings are sold as a snack food in the same way pork rinds are in the USA. Unlike the physically large, but relatively light bags of 'deep-fried skin without the fat' sold around the world, in the UK they are sold in relatively small bags, which usually weigh between 42g and 90g. Traditionally, they are eaten as an accompaniment to a pint of beer in a pub, just like crisps or peanuts. Scratchings can also be bought from butchers, supermarkets or newsagents. They have been taken to both the North and South Poles on various expeditions, because of their high energy content.
This food is believed to have originated in the West Midlands or Black Country, where they were widely consumed by the working classes. The pork scratching dates back to the 1800s, when families kept their own pigs as a source of food; in order to not waste any element of the pig, due to the scarcity of food, even the offcuts of fat and skin were fried for food.
There are three distinct types. Traditional scratchings are made from shank rind and cooked just once. Pork crackling is also made from shoulder rind, but is fried twice. It is first rendered at a low heat, and then cooked at a higher temperature for a less fatty, crispier result. A more recent development is the pork crunch, which is made from back rind and again double-fried to become a large, puffy snack. Some supermarkets now sell just the layer of skin and fat (no meat), in a raw form for home grilling or roasting, or cooked and ready to eat from hot food counters. The term 'crackling' is also often applied to a twice-cooked variety of pork scratchings.


== See also ==

Chicharron
Ciccioli, an Italian food made from pressed pork scraps
Čvarci
Gribenes, a Jewish snack made from chicken skin
Krupuk kulit, a similar Indonesian snack but more commonly made from cattle skin.


== References ==