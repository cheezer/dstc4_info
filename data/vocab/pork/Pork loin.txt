Pork loin is a cut of meat from a pig, created from the tissue along the top of the rib cage.


== Chops and steaks ==
Pork loin may be cut into individual servings, as chops (bone in) or steaks (boneless) which are grilled, baked or fried.


== Joints or roasts ==
A pork loin joint or pork loin roast is a larger section of the loin which is roasted. It can take two forms: 'bone in', which still has the loin ribs attached, or 'boneless', which is often tied with butchers string to prevent the roast from falling apart. Pork rind may be added to the fat side of the joint to give a desirable crackling which the loin otherwise lacks.


== Back bacon ==
Loin can also be cured to make back bacon, which is particularly popular in the United Kingdom and Canada; in the U.S., this is called "Canadian bacon" to distinguish it from the usual U.S.-style streaky bacon.


== See also ==

Cuts of pork