Pulled pork is a method of cooking pork where what would otherwise be a tough cut of meat is cooked slowly at low temperatures, allowing the meat to become tender enough so that it can be "pulled", or easily broken into individual pieces.
Pulled pork is found around the world in a variety of forms.


== Preparation ==
In the United States, pulled pork, usually shoulder cut (sometimes referred to as Boston butt) or mixed cuts, is commonly slow-cooked by a smoking method, though a non-barbecue method might also be employed using a slow cooker or a domestic oven. In rural areas across the United States, either a pig roast/whole hog, mixed cuts of the pig/hog, or the shoulder cut alone are commonly used, and the pork is served with or without a vinegar-based sauce.


== See also ==
Carnitas
Cochinita pibil
Pig pickin'
Porchetta
Rillettes
Ropa vieja
Rousong
Shredded beef


== References ==


== External links ==
 Media related to Pulled pork at Wikimedia Commons