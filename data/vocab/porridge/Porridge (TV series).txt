Porridge is a British situation comedy broadcast on BBC1 from 1974 to 1977, running for three series, two Christmas specials and a feature film also titled Porridge (the movie was released under the title Doing Time in the United States). Written by Dick Clement and Ian La Frenais, it stars Ronnie Barker and Richard Beckinsale as two inmates at the fictional HMP Slade in Cumberland. "Doing porridge" is British slang for serving a prison sentence, porridge once being the traditional breakfast in UK prisons.
Porridge was critically acclaimed and is widely considered to be one of the greatest British sitcoms of all time. The series was followed by a 1978 sequel, Going Straight, which established that Fletcher would not be going back to prison again.


== History ==

Porridge originated with a 1973 project commissioned by the BBC Seven of One, which would see Ronnie Barker star in seven different situation comedy pilot episodes. The most successful would then be made into a full series. One of the episodes, "Prisoner and Escort", written by Dick Clement and Ian La Frenais (who appear in one episode) about a newly convicted criminal, Norman Stanley Fletcher (Barker), being escorted to prison by two warders: the timid Mr. Barrowclough (Brian Wilde) and the stern Mr. Mackay (Fulton Mackay). It was broadcast on 1 April 1973 on BBC2. Despite Barker's initial preference for another of the pilots, a sitcom about a Welsh gambling addict, "Prisoner and Escort" was selected. It was renamed Porridge, a slang term for prison; Barker and Clement and La Frenais actually came up with the same title independently of each other.
In their research, Clement and La Frenais spoke to Jonathan Marshall, a former prisoner who had written a book, How to Survive in the Nick, and he advised them about prison slang, dress and routines. Struggling to think up plots and humour for such a downbeat, confined environment, a particular phrase used by Marshall – "little victories" – struck a chord and convinced them to base the series on an inmate who made his daily life in prison more bearable by beating the system, even in trivial ways.
The BBC was forced to look around for locations because the Home Office refused permission for any production filming in or outside a real prison. Instead the main gatehouse of the disused St Albans Prison (in the town's Victoria Street) was used in the opening credits. Exteriors were first filmed at a psychiatric hospital near Watford. However after the completion of the second series, the hospital withdrew permission for more filming following complaints from patients' families. Another institution near Ealing was then used for the third series. Scenes within cells and offices were filmed at the BBC's London studios. But for shots of the wider prison interior, series production designer Tim Gleeson, converted an old water tank, used at Ealing Studios for underwater filming, into a multi-storey set.
The first episode, "New Faces, Old Hands", was aired on BBC1 on 5 September 1974, attracting a television audience of over 16 million, and received positive reviews from critics. Two further series were commissioned, as well as two Christmas special episodes. The final episode of Porridge, "Final Stretch", was broadcast on 25 March 1977. The producers and the writers were keen to make more episodes, but Barker was wary of being "stuck with a character" and also wanted to move on to other projects, so the series came to a close. Barker did, however, reprise his role as Fletcher in a sequel, Going Straight, which ran for one series in 1978. A feature-length version of the show was made in 1979 and in 2003 a follow-up mockumentary was aired.


== Plot ==
The central character of Porridge is Norman Stanley Fletcher, described by his sentencing judge as "a habitual criminal" from Muswell Hill, London. Fletcher is sent to HMP Slade, a fictional Category C prison in Cumberland, alongside his cellmate, Lennie Godber, a naïve inmate from Birmingham serving his first sentence, whom Fletcher takes under his wing. Mr Mackay is a tough warder with whom Fletcher often comes into conflict. Mackay's subordinate, Mr Barrowclough, is more sympathetic and timid – and prone to manipulation by his charges.


== Locations ==
The prison exterior in the title sequence is the old St Albans prison gatehouse and Maidstone Prison, which was also featured in the BBC comedy series Birds of a Feather. The interior shots of doors being locked were filmed in Shepherds Bush Police Station - the BBC had a good relationship with officers there. In the episode "Pardon Me" Fletcher speaks to Blanco (David Jason) in the prison gardens: this was filmed in the grounds of an old brewery outside Baldock on the A505 to Royston. The barred windows approximated a prison. The building has since been demolished. The 1974 episode "A Day Out", which features a prison work party, was filmed in and around the Welsh village of Penderyn, the prisoners' ditch being excavated by a JCB. Elland Road, the home of Leeds United Football Club, was briefly featured in "Happy Release". In the episode "No Way Out", Fletcher tries to get MacKay to fall into a tunnel in a tarmac area, these outside shots were filmed at Hanwell Asylum in West London, the barred windows in this case, being those of the hospital pharmacy. The interior shots for the 1979 film were shot entirely at Chelmsford Prison, Essex.


== Cast ==
Ronnie Barker had suggested the part of Lennie Godber for Paul Henry, but the decision to cast Richard Beckinsale was taken by the production team.
Ronnie Barker as Norman Stanley Fletcher
Fulton MacKay as Principal Officer Mr MacKay
Richard Beckinsale as Lennie Godber
Brian Wilde as Prison Officer Mr Henry Barrowclough
Peter Vaughan as "Genial" Harry Grout
Sam Kelly as "Bunny" Warren
Tony Osoba as Jim McLaren
Christopher Biggins as Lukewarm
David Jason as Blanco Webb
Ken Jones as "Horrible" Ives
Ronald Lacey as Harris
Michael Barrington as Geoffrey Venables (the Governor)
Madge Hindle as Mrs Hesketh (the Governor's secretary)
Patricia Brake as Ingrid Fletcher


=== Recurring characters ===
Maurice Denham as The Honourable Mr Justice Stephen Rawley
Brian Glover as Cyril Heslop
Ray Dunbobbin as Evans
Dudley Sutton as Reg Urwin "with a U"
Philip Madoc as Williams
Alun Armstrong as Spraggon
David Daker as Jarvis
Eric Dodson as Banyard
Peter Jeffrey as Napper Wainwright
Paul McDowell as Mr Collinson
Jane Wenham as Mrs Jamieson
John Dair as Crusher
Paul Angelis as Navy Rum
Philip Jackson as Dylan
The programme's scriptwriters appear, uncredited, outside Fletch and Godber's cell in the episode "No Peace for the Wicked".


== Episode list ==
Each episode 30 minutes except where stated.


=== Pilot (1973) ===


=== Series 1 (1974) ===


=== Series 2 (1975) ===


=== Christmas specials (1975–1976) ===


=== Series 3 (1977) ===


== Titles and music ==
The opening credits consist of outside shots of Slade prison and of several doors and gates being closed and locked, which was intended to set the scene for the programme. In the first series, there were also shots of St Pancras railway station, which was changed in subsequent series to shots of Fletcher walking around Slade prison. Title music was thought unsuitable for a show set in prison, so instead there is a booming narration (performed by Barker himself) given by the presiding judge passing sentence on Fletcher:
Subsequently, Barker is reported to have said that he regretted recording himself as the judge, a character role subsequently played by Maurice Denham in two episodes of the third series.
The theme music for the closing credits was written by Max Harris, who had also written the theme music for numerous other TV shows, including The Strange World of Gurney Slade and Doomwatch, and would go on to write the theme for Open All Hours, another of the Seven of One pilots. The cheery theme was "deliberately at variance with the dour comedy" and given a music hall feel by Harris because of the lead character's Cockney origins.


== Spin-offs ==


=== Going Straight ===

A sequel to Porridge, Going Straight, was aired between 24 February and 7 April 1978. Beginning with Fletcher's release from prison on parole, it follows his attempts to "go straight" and readjust to a law-abiding life. Richard Beckinsale reprised his role as Godber, now the fiancée of Fletcher's daughter Ingrid (Patricia Brake), and the couple married in the final episode. Nicholas Lyndhurst also featured as Fletcher's gormless son, Raymond. The series lasted six episodes, and generally was not as well received as its predecessor, although it did win two BAFTAs, for Best Situation Comedy and Best Light Entertainment Performance (jointly with The Two Ronnies) for Ronnie Barker.


=== Porridge - the film ===

Following the example of other sitcom crossovers, such as Dad's Army, Steptoe and Son and The Likely Lads, a feature-length version of Porridge was made in 1979. Barker again starred as Fletcher, and most of the supporting cast also returned. Unlike the television series, it was actually filmed at a real prison as HMP Chelmsford was temporarily vacant following a fire.


=== Life Beyond the Box: Norman Stanley Fletcher ===

On 29 December 2003, a mockumentary follow-up to Porridge was broadcast on BBC Two. It looked back on Fletcher's life and how the various inmates of Slade had fared 25 years after Fletcher's release from prison. Warren is now a sign painter, Lukewarm is married to Trevor, McLaren is an MSP, Grouty has become a celebrity gangster, Horrible Ives collects money for non-existent charities, Godber is now a lorry driver and still married to Ingrid, and Fletcher runs a pub with his childhood sweetheart, Gloria.


=== Novelisations and audio ===
Novelisations of the three series of Porridge and the film were issued by BBC Books, as well as an adaptation of Going Straight. BBC Enterprises released an LP record featuring two Porridge episodes, "A Night In" and "Heartbreak Hotel" in 1977.(REB 270) Two volumes of audio cassette releases (comprising four episodes each) were issued in the mid-1990s. They were later re-released on CD.


=== Stage show ===
In 2009 Porridge was adapted into a stage show, also written by Clement and La Frenais, starring former EastEnders actor Shaun Williamson as Fletcher and Daniel West as Godber. Peter Kay, a fan of the show, was previously offered the role but turned it down. It opened in September 2009 to positive reviews.


=== Adaptations ===
An American version entitled On the Rocks (1975–76) ran for a season, while a Dutch version Laat maar zitten (free translation: Keep 'em inside) ran from 1988 to 1991; later episodes of the Netherlands version were original scripts


== DVD releases ==


== Popularity with prisoners ==
Porridge was immensely popular with British prisoners. Erwin James, an ex-prisoner who writes a bi-weekly column for The Guardian newspaper, stated that:
He also noted:


== Contributions to the English language ==
The script allowed the prisoners to swear without offending viewers by using the word "naff" in place of ruder words ("Naff off!", "Darn your own naffing socks", "Doing next to naff all"), thereby popularising a word that had been recorded at least as early as 1966. Ronnie Barker did not claim to have invented it, and in a television interview in 2003 it was explained to him on camera what the word meant, as he hadn't a clue.
A genuine neologism was "nerk", which was used in place of the more offensive "berk". It should be noted that "berk" has changed meaning since its inception, and is generally used now to mean "fool" while the original rhyming slang meaning refers to female genitalia. Another term was "scrote" (presumably derived from scrotum), meaning a nasty, unpleasant person.


== See also ==
List of films based on British sitcoms


== References ==


== Bibliography ==
Webber, Richard (2005). Porridge: The Complete Scripts and Series Guide. London: Headline Book Publishing. ISBN 0-7553-1535-9


== External links ==
BBC Comedy Guide
Porridge at BBC Programmes
RonnieBarker.com on Porridge
100 Greatest Sitcoms: Porridge
Porridge at the Internet Movie Database
Porridge at the BFI's Screenonline
Porridge at British TV Comedy
Porridge: The Unofficial Homepage
Porridge at the British Comedy Guide
Erwin James (prisoner) article on Porridge in The Guardian