Penaeus esculentus (the brown tiger prawn, tiger prawn or common tiger prawn) is a species of prawn which is widely fished for consumption around Australia.


== Ecology ==
Juvenile P. esculentus live in seagrass beds, and reach sexual maturity at a carapace length of around 32 millimetres (1.3 in). Adults grow up to 155 millimetres (6.1 in) long, and resemble Penaeus monodon, albeit smaller and browner. They live offshore at depths of up to 200 metres (660 ft).


== Distribution ==
P. esculentus appears to be endemic to Australian waters, being found in warm waters from central New South Wales (near Sydney) to Shark Bay, Western Australia, chiefly at depths of 16–22 metres (52–72 ft). There is little population structure in the species, with only slight differentiation between regions east and west of the Pleistocene land bridge between Australia and New Guinea.


== Fisheries and aquaculture ==
Around 500 tonnes (490 long tons; 550 short tons) of brown tiger prawns are caught each year. Fisheries in Torres Strait are worth around A$24 million per year. It is closely related to Penaeus monodon, with which it can hybridise. It has the potential to be used in aquaculture (shrimp farming) since, although it grows less rapidly than P. monodon, it commands higher prices.


== Taxonomic history ==
William Aitcheson Haswell arrived in Australia in 1878, and began working in a marine zoology laboratory at Watsons Bay. In 1879, he described Penaeus esculentus in a paper in the Proceedings of the Linnean Society of New South Wales, basing his description on material in the Macleay Museum which had come from Port Jackson and Port Darwin, and noting that P. esculentus is "the common edible prawn of Sydney, and Newcastle, etc.".


== References ==