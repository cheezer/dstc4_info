Preferential voting may refer to:
Ranked voting systems, all election methods that involve ranking candidates in order of preference
Instant-runoff voting, referred to as "preferential voting" in Australia, is one type of ranked voting system.
Range voting, in which voters assign points to each candidate
Open list, sometimes known as "preferential voting" in Europe and nations such as Sri Lanka
Bucklin voting, which was sometimes known as "preferential voting" when used in the United States