The Ministry of Minor Export Crop Promotion is the Sri Lankan government ministry responsible for “leading the spice industry to achieve the excellence in cultivation, production, marketing and promotion.”


== List of ministers ==
The Minister of Minor Export Crop Promotion is an appointment in the Cabinet of Sri Lanka.
Parties
      Sri Lanka Freedom Party       United National Party


== See also ==
List of ministries of Sri Lanka


== References ==


== External links ==
Ministry of Minor Export Crop Promotion
Government of Sri Lanka
Template:Sri Lankan Minor Export Crop Promotion ministers