The Irish Pub of Kabul is a pub in Kabul, Afghanistan; it opened on St. Patrick's Day, 2003. The pub is licensed by the Afghan government, with the caveat that it not sell alcohol to Afghans.


== References ==
"Threats close Kabul's Irish bar". BBC News. 26 April 2003. Retrieved 10 August 2013. 
"Kabul Pub Offers a Wee Tipple of Home". Los Angeles Times. 20 April 2003. Retrieved 10 August 2013. 
The Survival Guide to Kabul©, The Irish Club