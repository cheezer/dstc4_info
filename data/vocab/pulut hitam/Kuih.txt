Kuih (also spelled Hokkien/Teochew: kueh or kway; from the Hokkien: 粿 koé, Indonesian: kue) are bite-sized snack or dessert foods commonly found in Malaysia, Singapore, Indonesia (where it is called kue), as well as the Southern China provinces of Fujian and Chaoshan, also in the Netherlands through its colonial link to Indonesia. Kuih is a fairly broad term which may include items that would be called cakes, cookies, dumplings, pudding, biscuit, or pastries in English and are usually made from rice or glutinous rice.
Kuih are more often steamed than baked, and are thus very different in texture, flavour and appearance from Western cakes or puff pastries. Many kuih are sweet, but some are savoury. The term Kueh/Kuih is widely used in Malaysia and Singapore to refer to sweet or savoury desserts. It is hard to distinguish between kuih of Malay or Peranakan (also known as "Straits Chinese" people) origin because the histories of these recipes have not been well-documented. Cross-cultural influencing is also very common.
Though called by other names, one is likely to find various similar versions of kuih in neighbouring countries, such as Vietnam, Thailand, and Burma. For example, the colourful steamed kue lapis and the rich kuih bingka ubi are also available in Burma, Thailand, and Vietnam.
Kuihs are not confined to a certain meal but can be eaten throughout the day. They are an integral part of Malaysian and Singaporean festivities such as Hari Raya and Chinese New Year, which is known as Tahun Baru Cina in Malay among the Peranakan.
In the Northern states of Perlis, Kedah, Perak and Kelantan, kuih (kuih-muih in Malay) are usually sweet. In the Southeast Peninsular states of Negeri Sembilan, Melaka and Selangor, savory kuih can be found. This is largely due to the large population of ethnic Chinese and Indians which held much cultural influence in these states.
In almost all Malay and Peranakan kuih, the most common flavouring ingredients are grated coconut (plain or flavoured), coconut cream (thick or thin), pandan (screwpine) leaves and gula melaka (palm sugar, fresh or aged). While those make the flavour of kuih, their base and texture are built on a group of starches – rice flour, glutinous rice flour, glutinous rice and tapioca. Two other common ingredients are tapioca flour and green bean (mung bean) flour (sometimes called "green pea flour" in certain recipes). They play a most important part in giving kuihs their distinctive soft, almost pudding-like, yet firm texture. Wheat flour is rarely used in Southeast Asian cakes and pastries.
For most kuih there is no single "original" or "authentic" recipe. Traditionally, making kuih was the domain of elderly grandmothers, aunts and other women-folk, for whom the only (and best) method for cooking was by "agak-agak" (approximation). They would instinctively take handfuls of ingredients and mix them without any measurements or any need of weighing scales. All is judged by its look and feel, the consistency of the batter and how it feels to the touch. Each family holds its own traditional recipe as well as each region and state.
Nyonya (Peranakan) and Malay kuih should not be distinguished since Peranakans have settled in the Malay Peninsula. They have adapted to Malay culinary and cultural heritage. Therefore there are many kuih native to Malay culture which have been improvised and retained by the Peranakans.
Nonya kuih come in different shapes, colours, texture and designs. Some examples are filled, coated, wrapped, sliced and layered kuih. Also, as mentioned earlier, most kuih are steamed, with some being boiled or baked. They can also be deep-fried and sometimes even grilled.


== Variants ==
Examples of notable kuih-muih include:
Ang koo kueh (Chinese: 紅龜粿) - a small round or oval shaped Chinese pastry with red-coloured soft sticky glutinous rice flour skin wrapped around a sweet filling in the center.
Apam balik - a turnover pancake with a texture similar to a crumpet with crisp edges, made from a flour based batter with raising agent. It is typically cooked on a griddle and topped with castor sugar, ground peanut, creamed corn, and grated coconut in the middle, and then turned over. Many different takes on this dish exist as part of the culinary repertoire of the Malay, Chinese, Peranakan, Indonesian, and ethnic Bornean communities; all under different names.
Bahulu - tiny crusty sponge cakes which come in distinctive shapes like button and goldfish, acquired from being baked in molded pans. Bahulu is usually baked and served for festive occasions.
Chwee kueh (Chinese: 水粿) - Teochew-style steamed bowl-shaped rice cakes topped with diced preserved radish and chilli relish.
Cucur - deep-fried fritters, sometimes known as jemput-jemput. Typical varieties include cucur udang (fritters studded with a whole unshelled prawn), cucur badak (sweet potato fritters), and cucur kodok (banana fritters).
Curry puff - a small pie filled with a curried filling, usually chicken and/or potatoes, in a deep-fried or baked pastry shell.
Kuih akok - a rich confection made with liberal quantities of eggs, coconut milk, flour and brown sugar, akok have a distinctive sweet caramel taste. It is popular in the states of Kelantan and Terengganu.
Kuih cincin - a deep fried dough pastry-based snack popular with East Malaysia's Muslim communities.
Kuih dadar or kuih ketayap - mini crepes rolled up with a palm sugar-sweetened coconut filling. The crepes are coloured and flavoured with pandan essence.
Kuih jala - a type of traditional fried confection in the eastern states of Sabah and Sarawak. A rice flour batter is ladled into an emptied coconut shell bearing many small holes underneath, which is then held over hot oil and moved in a circular motion. The mixture will drip into the oil like thread, and forms a lattice-like layer on the oil as it fries to a solid crisp.
Kuih jelurut - also known as kuih selorot in Sarawak, this kuih is made from a mixture of gula apong and rice flour, then rolled with palm leaves into cones and steam cooked.
Kuih kapit, sapit or sepi - these crispy folded wafer biscuits are colloquially known as "love letters".
Kuih keria - fried doughnuts made with a sweet potato batter and rolled in caster sugar.
Kuih kochi - glutinous rice dumplings filled with a sweet paste, shaped into a pyramid-like and wrapped with banana leaves.
Kuih lapis - a sweet steamed cake made from rice flour, coconut milk, sugar and various shades of edible food colouring done with many individual layers.
Kuih lidah - Kuih lidah (lidah literally means "tongue") hails from the Bruneian Malay community of Papar, specifically Kampung Berundong, in Sabah and possesses designated GI status.
Kuih pie tee - this Nyonya specialty is a thin and crispy pastry tart shell filled with a spicy, sweet mixture of thinly sliced vegetables and prawns.
Kuih pinjaram - a saucer-shaped deep fried fritter with crisp edges and a dense, chewy texture towards the centre. It is widely sold by street food vendors in the open air markets of East Malaysia.
Kuih serimuka - a two-layered kuih with steamed glutinous rice forming the bottom half and a green custard layer made with pandan juice.
Kuih talam — this kuih has two layers. The top consists of a white layer made from coconut milk and rice flour, whereas the bottom layer is green and is made from green pea flour flavoured with pandan.
Kuih wajid or wajik - a compressed Malay confection made of glutinous rice cooked with coconut milk and gula melaka.
Niangao (Chinese : 年糕) or kuih bakul - a brown sticky and sweet rice cake customarily associated with Chinese New Year festivities. It is also available year round as a popular street food treat, made with pieces of niangao sandwiched between slices of taro and sweet potato, dipped in batter and deep-fried.
Onde onde - small round balls made from glutinous rice flour colored and flavored with pandan, filled with palm sugar syrup and rolled in freshly grated coconut.
Or Kuih (Chinese : 芋粿) - a steamed savoury cake made from pieces of taro (commonly known as "yam" in Malaysia), dried prawns and rice flour. It is then topped with deep fried shallots, spring onions, sliced chilli and dried prawns, and usually served with a chilli dipping sauce.
Pineapple tart - flaky pastries filled with or topped with pineapple jam.
Pulut inti - wrapped in banana leaf in the shape of a pyramid, this kuih consists of glutinous rice with a covering of grated coconut candied with palm sugar.
Pulut panggang - glutinous rice parcels stuffed with a spiced filling, then wrapped in banana leaves and char grilled. Depending on regional tradition, the spiced filling may include pulverized dried prawns, caramelized coconut paste or beef floss. In the state of Sarawak, the local pulut panggang contains no fillings and are wrapped in pandan leaves instead.
Putu piring - a round steamed cake made of rice flour dough, with a palm sugar sweetened filling.
Yi Buah/Buak (Chinese : 意粑) - a Hainanese steamed dumpling made of glutinous rice flour dough. Also known as Kuih E-Pua, it is filled with a palm sugar sweetened mixture of grated coconut, toasted sesame seeds and crushed roasted peanuts, wrapped with sheets of banana leaves pressed into a fluted cup shape, and customarily marked with a dab of red food colouring. This kuih is traditionally served during a wedding and a baby's full-moon celebration.


== See also ==
Ketupat
Lemang
Kue nagasari
Dodol
Cucur


== References ==