Kue putu or putu bambu, putu buluh or puto bumbong is a traditional cylindrical-shaped and green-colored steamed cake. The cake is made of rice flour with green color acquired from pandan leaf called suji, filled with palm sugar, and steamed in bamboo tube, hence its name, and served with grated coconut. This traditional bite-size snack is commonly found in Maritime Southeast Asia, which includes Indonesian, Malaysian and Philippines cuisines, and believed to be derived from Indian puttu of Tamil origin. Kue putu is also can be found in the Netherlands owed to their colonial ties with Indonesia.
Kue putu is one of Indonesian kue or traditional snack, and a popular street food commonly sold by travelling vendor, together with klepon, which is actually ball-shaped kue putu, but made with sticky glutinous rice flour instead.


== Ingredients and cooking method ==
It is consists of rice flour with green pandan leaf coloring, filled with ground palm sugar. This green coconut-rice flour ingredients with palm sugar filling is filled into bamboo tube container. Subsequently, the filled bamboo tubes are steamed upon a steam cooker with small holes opening to blow the hot steam. The cooked tubular cakes then pushed out from the bamboo tube container, and served with grated coconut.


== Variations ==

The variations of kue putu is often in its shapes or in its fillings. Kue putu of different shapes with almost identical ingredient, filling and recipes exist in Southeast Asia. The white-colored flatter dics-like shaped putu is called putu piring (Malay for: disc/plate putu) and more common in Malaysia, while the more thicker and rounder white or green-colored putu mangkok (Indonesian for: bowl putu) is more common in Indonesia. In Singapore however, putu mangkok is called kueh tutu.
Traditionally kue putu is filled with palm sugar, today however there are several new variation using different fillings, such as chocolate or abon (beef floss).
In the Philippines, kue putu, locally known as puto bumbong is commonly served as a snack during the Christmas season, and is usually associated with the nine-day traditional Simbang Gabi novena. However, puto bumbong is usually dyed purple instead of grees as in other Asian countries, with purple being the liturgical color of the Advent season, the time when Simbang Gabi usually takes place.


== See also ==

Puttu
Puto
Kueh tutu
Kue
Klepon
Kue lapis
Getuk


== References ==


== External links ==
Kue Putu (Indonesian Bamboo Cake) Recipe
Kue Putu Recipe