Queenstown Road is a railway station in South London, between Vauxhall and Clapham Junction. It is a short walk from Battersea Park station and Battersea Park. It has seven tracks through it but only three platforms, two of which are in use, serving trains on the Waterloo to Richmond line and the Hounslow Loop.


== History ==
The station was opened on 1 November 1877, by the London and South Western Railway, as Queen's Road (Battersea). The entrance still bears the name Queen's Road, not to be confused with Queens Road Peckham, Walthamstow Queen's Road or Queensway tube station, which was also originally called Queens Road.
The station was renamed Queenstown Road (Battersea) on 12 May 1980. The station's modern entrance and platform signage lacks the "(Battersea)" suffix that appears in timetables and on some maps. The latest "Oyster Rail Services" map produced by Transport for London shows the station as plain "Queenstown Road". On the map produced by the station managers, South West Trains, the station is called "Queenstown Road".


== Services ==
Queenstown Road is on the early stage of the South Western Main Line but with only 2 platforms in use. The off-peak frequency in trains per hour is:
8 to London Waterloo
2 on the Kingston Loop via Richmond and Kingston
4 on the Hounslow Loop:
2 clockwise
2 anti-clockwise

2 to Weybridge via Hounslow and Staines


== Connections ==
London Buses routes 137; 156; and night route N137 serve the station.


== References ==


== External links ==
Train times and station information for Queenstown Road (Battersea) railway station from National Rail