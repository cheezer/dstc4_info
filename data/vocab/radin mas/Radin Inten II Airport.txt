Radin Inten II Airport (IATA: TKG, ICAO: WILL) is a domestic airport (later an international airport) that serves the city of Bandar Lampung in Lampung, Indonesia. The name is taken from Radin Inten II, the last Sultan of Lampung.
It is on the Jalan Branti Raya in Branti, Natar, northwest of Bandar Lampung in the South Lampung regency.
This airport is a managed by PT. Angkasa Pura II (Persero) since its renovation.[1]


== Airlines and destinations ==


== See also ==

List of airports in Indonesia


== References ==


== External links ==
Airport information for WICT at World Aero Data. Data current as of October 2006.Source: DAFIF.
Airport information for TKG / WICT at Great Circle Mapper. Source: DAFIF (effective Oct. 2006).
Accident history for TKG / WICT at Aviation Safety Network