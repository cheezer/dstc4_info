Raffles Place is a geographical location in Singapore, south of the mouth of the Singapore River. Located in the Downtown Core and the Central Area, it features some of the tallest buildings and landmarks of the country.


== History ==
The founder of modern Singapore, Sir Stamford Raffles, intended Singapore to become a "great commercial emporium". At the heart of this dream was Raffles Place. Charted by Garrison Engineer Lieutenant R.N. Philip Jackson's map of Raffles' 1822 Town Plan, Raffles Place was located on the south bank of the Singapore River.
Where a hill originally stood at Raffles Place, the soil of which was then used to reclaim the southern bank of the Singapore River to form Boat Quay. Known as Commercial Square then, Raffles Place was no more than a quiet green when it was first developed from 1823 to 1824. As the economy of Singapore grew, two- and four-storey buildings sprang up around the square, housing mercantile offices, banks and trading companies.
In 1858, Commercial Square was renamed Raffles Place. The sea came right up to the buildings on the south side of the square then, many of which were godowns with jetties that allowed cargo to be loaded and unloaded directly from boats. From 1857 to 1865, the land by the south side was reclaimed for commercial use. This new land became Collyer Quay. With a larger area designated for commerce, more businesses flocked to Raffles Quay, most notably retail stores and banks.
The second half of the 19th century saw the setting up of the two oldest department stores which survive today: Robinson's and John Little. Some of the first banks to operate in Raffles Place were HSBC and Standard Chartered Bank.

At the turn of the 20th century, the banking industry in Singapore took off. Home-grown banks came into play, competing against bigger banks with lower interest rates and a cultural affinity with their customers. From the 1950s, banking in Singapore entered a new league, with Bank of America establishing itself here in 1955 at 31 Raffles Place, and Bank of China at the adjacent Battery Road. In the early 1960s, the Whiteaways Building, previously a department store, was demolished to make way for Malayan Bank. This was followed in 1965 by the construction of the United Overseas Banktowers, which were, for many years, the tallest buildings in Singapore.
Raffles Place was the subject of carpet bombing during World War II when seventeen Japanese bombers conducted the first air raid on Singapore on 8 December, 1941, during the Battle of Malaya.
With the exception of the Japanese Occupation years, the commercial development of Raffles Place took place almost continuously. The 1960s and 1970s saw an exodus of retailers to locations such as High Street, North Bridge Road and Orchard Road, leaving Raffles Place the primary domain of finance houses. Skyscrapers with flagship banks, such as Singapore Land Tower, Clifford Centre, Ocean Building, OUB Centre and Republic Plaza, replaced the older buildings.


== Notable buildings ==
Several key buildings are located in Raffles Place, including UOB Plaza, One Raffles Place, Republic Plaza, One Raffles Quay and OCBC Centre. The Fullerton Hotel Singapore, a hotel at the renovated old General Post Office building, the famous tourist icon the Merlion, and an ultra modern durian shaped Art Centre Esplanade Theatre are located nearby. The stock exchange of Singapore - the Singapore Exchange - is also located in the vicinity. Several key administrative buildings in Singapore, such as the Parliament House, the Supreme Court and City Hall are located north across the river, but are technically not part of Raffles Place.


== Transportation ==
The underground Mass Rapid Transit station, the Raffles Place MRT Station, lies directly underneath the centre of Raffles Place, and is one of the primary public transport links for Raffles Place into Singapore's transport system. Bus services also run along Raffles Place.


== Sources ==
National Heritage Board (2002), Singapore's 100 Historic Places, Archipelago Press, ISBN 981-4068-23-3


== External links ==
360° X 360° interactive virtual tour of Raffles Place
A street level map of Raffles Place