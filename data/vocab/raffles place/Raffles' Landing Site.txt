The Raffles' Landing Site is the location where tradition holds that Sir Stamford Raffles landed in on 29 January 1819. The site is located at Boat Quay within the Civic District, in the Downtown Core of the Central Area, Singapore's central business district.


== History ==
On 29 January 1819, Sir Stamford Raffles landed at this site for his first visit, which lasted ten days. During this period, he concluded the first treaty with the local rulers.
The site is denoted by a statue of Sir Stamford Raffles and is located on the north bank of the Singapore River. The present polymarble statue was unveiled in 1972 which was made from plaster casts from the original 1887 figure that currently stands opposite Victoria Concert Halls.


== Plaque inscription ==
The Plaque at the landing site reads "On this historic site, Sir Thomas Stamford Raffles first landed in Singapore on 29th January 1819, and with genius and perception changed the destiny of Singapore from an obscure fishing village to a great seaport and modern metropolis."


== See also ==
History of Singapore
Timeline of Singaporean history


== Notes ==


== References ==
Norman Edwards, Peter Keys (1996), Singapore - A Guide to Buildings, Streets, Places, Times Books International, ISBN 9971-65-231-5
National Heritage Board (2006), Discover Singapore - Heritage Trails, ISBN 981-05-6433-3