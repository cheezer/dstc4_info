The Raffles Bulletin of Zoology is a scientific journal published by the Raffles Museum of Biodiversity Research in Singapore. It publishes papers on the taxonomy, ecology, and conservation of Southeast Asian fauna. Two regular issues are published each year. Supplements are published as and when funding permits and may cover topics that extend beyond the normal scope of the journal depending on the targets of the funding agency. It is currently a hybrid open access journal with most of the papers published available online. In time, all papers published by the Raffles Bulletin of Zoology will be made freely accessible to all. The journal is largely supported by funding from the Faculty of Science, National University of Singapore.


== See also ==
List of zoology journals


== External links ==
Official website