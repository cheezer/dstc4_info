Kluang Inner Ring Road comprising Jalan Hospital, Jalan Rambutan and Jalan Mohd Salim, Federal Route  (Formerly Johor State Route J191) is a federal road in Kluang town, Johor, Malaysia.


== Route background ==


== Features ==


=== Notable features ===
At most section, the Federal Route 172 was built under the JKR R5 road standard, allowing maximum speed limit of up to 90 km/h.


=== Overlaps ===


=== Alternate routes ===


=== Sections with motorcycle lanes ===


== List of junctions ==


== References ==