The rambutan (/ræmˈbuːtən/; taxonomic name: Nephelium lappaceum) is a medium-sized tropical tree in the family Sapindaceae. The name also refers to the fruit produced by this tree. The rambutan is native to Malay-Indonesian region, and other regions of tropical Southeast Asia. It is closely related to several other edible tropical fruits including the lychee, longan, and mamoncillo.


== Etymology ==
The name 'rambutan' is derived from the Malay-Indonesian languages word for rambut or "hair", a reference to the numerous hairy protuberances of the fruit, together with the noun-building suffix -an. In Vietnam, it is called chôm chôm (meaning "messy hair") due to the spines covering the fruit's skin.


== Origin and distribution ==
Rambutan is native to tropical Southeast Asia and commonly grown throughout Indonesia, Malaysia, Thailand and the Philippines. It has spread from there to various parts of Asia, Africa, Oceania and Central America. The widest variety of cultivars, wild and cultivated, are found in Malaysia.
Around the 13th to 15th centuries, Arab traders that played a major role in Indian Ocean trade introduced rambutan into Zanzibar and Pemba of East Africa. There are limited rambutan plantings in some parts of India. In the 19th century, the Dutch introduced rambutan from their colony in Southeast Asia and Suriname in South America. Subsequently the plant spread to tropical Americas, planted in the coastal lowlands of Colombia, Ecuador, Honduras, Costa Rica, Trinidad and Cuba. In 1912, rambutan was introduced to the Philippines from Indonesia. Further introductions were made in 1920 (from Indonesia) and 1930 (from Malaya), but until the 1950s its distribution was limited. There was an attempt to introduce rambutan to the United States, with seeds imported from Java in 1906, but the species proved to be unsuccessful, except in Puerto Rico.


== Description ==

It is an evergreen tree growing to a height of 12–20 m. The leaves are alternate, 10–30 cm long, pinnate, with three to 11 leaflets, each leaflet 5–15 cm wide and 3–10 cm broad, with an entire margin. The flowers are small, 2.5–5 mm, apetalous, discoidal, and borne in erect terminal panicles 15–30 cm wide.
Rambutan trees can be male (producing only staminate flowers and, hence, produce no fruit), female (producing flowers that are only functionally female), or hermaphroditic (producing flowers that are female with a small percentage of male flowers).
The fruit is a round to oval single-seeded berry, 3–6 cm (rarely to 8 cm) long and 3–4 cm broad, borne in a loose pendant cluster of 10–20 together. The leathery skin is reddish (rarely orange or yellow), and covered with fleshy pliable spines, hence the name, which means 'hairs'. The fruit flesh, which is actually the aril, is translucent, whitish or very pale pink, with a sweet, mildly acidic flavor very reminiscent of grapes.
The single seed is glossy brown, 1–1.3 cm, with a white basal scar. Soft and crunchy, the seeds may be cooked and eaten. Some folklore regards the seeds as poisonous, but tests of seed extracts reveal no toxicity to mice, even in doses up to 2500 mg/kg. The peeled fruits can be cooked and eaten, first the grape-like aril, then the nutty seed, with no waste.


== Pollination ==
Aromatic rambutan flowers are highly attractive to many insects, especially bees. Flies (Diptera), bees (Hymenoptera), and ants (Solenopsis) are the main pollinators. Among the Diptera, Lucilia spp. are abundant, and among the Hymenoptera, honey bees (Apis dorsata and A. cerana) and the stingless bee genus Trigona are the major visitors. A. cerana colonies foraging on rambutan flowers produce large quantities of honey. Bees foraging for nectar routinely contact the stigma of male flowers and gather significant quantities of the sticky pollen from male blossoms. Little pollen has been seen on bees foraging female flowers. Although male flowers open at 6 am, foraging by A. cerana is most intense between 7 and 11 am, tapering off rather abruptly thereafter. In Thailand, A. cerana is the preferred species for small-scale pollination of rambutan. Its hair is also helpful in pollination where pollen can be hooked on and transported to female flowers.


== Production ==
Rambutan is an important fruit tree of humid tropical Southeast Asia, traditionally cultivated especially in Indonesia, Malaysia and Thailand. It is a popular garden fruit tree and propagated commercially in small orchards. It is one of the best-known fruits of Southeast Asia and is also widely cultivated elsewhere in the tropics including Africa, the Caribbean islands, Costa Rica, Honduras, Panama, India, the Philippines, and Sri Lanka; it is also produced in Ecuador where it is known as achotillo and on the island of Puerto Rico.
Thailand is the largest producer of rambutan, with 588,000 tonnes (55.5%), followed by Indonesia with 320,000 tonnes (30.2%) and Malaysia with 126,300 tonnes (11.9%) in 2005, the three countries collectively accounting for 97% of the world's supply of rambutan. In Thailand, major cultivation center is in Surat Thani Province. In Indonesia, the production center of rambutan is located in the western parts of Indonesia, which includes Java, Sumatra and Kalimantan. In Java, the orchards and pekarangan (habitation yards) in the villages of Greater Jakarta and West Java, has been known as rambutan production centers since colonial era, with trading center in Pasar Minggu, South Jakarta. Rambutan production is increasing in Australia and, in 1997, was one of the top three tropical fruits produced in Hawaii.
The fruit are usually sold fresh, used in making jams and jellies, or canned. Evergreen rambutan trees with their abundant coloured fruit make beautiful landscape specimens.
In India, rambutan is imported from Thailand as well as grown in Pathanamthitta District of the southern state of Kerala.
Rambutans are not a climacteric fruit — that is, they ripen only on the tree and appear not to produce a ripening agent such as the plant hormone, ethylene, after being harvested.


== Cultivation ==
Rambutan is adapted to warm tropical climates, around 22–30 °C, and is sensitive to temperatures below 10 °C. It is grown commercially within 12–15° of the equator. The tree grows well at elevations up to 500 m (1,600 ft) above sea level, and does best in deep soil, clay loam or sandy loam rich in organic matter, and thrive on hilly terrain as they require good drainage. Rambutan is propagated by grafting, air-layering, and budding; the latter is most common as trees grown from seed often produce sour fruit. Budded trees may fruit after two to three years with optimum production occurring after eight to 10 years. Trees grown from seed bear after five to six years.
The aril is attached to the seed in some commercial cultivars, but "freestone" cultivars are available and in high demand. Usually, a single light brown seed is found, which is high in certain fats and oils (primarily oleic acid and arachidic acid) valuable to industry, and used in cooking and the manufacture of soap. Rambutan roots, bark, and leaves have various uses in traditional medicine and in the production of dyes.

In some areas, rambutan trees can bear fruit twice annually, once in late fall and early winter, with a shorter season in late spring and early summer. Other areas, such as Costa Rica, have a single fruit season, with the start of the rainy season in April stimulating flowering, and the fruit is usually ripe in August and September. The fragile fruit must ripen on the tree, then they are harvested over a four- to seven-week period. The fresh fruit are easily bruised and have a limited shelf life. An average tree may produce 5,000–6,000 or more fruit (60–70 kg or 130–155 lb per tree). Yields begin at 1.2 tonnes per hectare (0.5 tons/acre) in young orchards and may reach 20 tonnes per hectare (8 tons per acre) on mature trees. In Hawaii, 24 of 38 cultivated hectares (60 of 95 acres) were harvested producing 120 tonnes of fruit in 1997. Yields could be increased by improved orchard management, including pollination, and by planting high-yielding compact cultivars.
Most commercial cultivars are hermaphroditic; cultivars that produce only functionally female flowers require the presence of male trees. Male trees are seldom found, as vegetative selection has favored hermaphroditic clones that produce a high proportion of functionally female flowers and a much lower number of flowers that produce pollen. Over 3,000 greenish-white flowers occur in male panicles, each with five to seven anthers and a nonfunctional ovary. Male flowers have yellow nectaries and five to seven stamens. About 500 greenish-yellow flowers occur in each hermaphroditic panicle. Each flower has six anthers, usually a bilobed stigma, and one ovule in each of its two sections (locules). The flowers are receptive for about one day, but may persist if pollinators are excluded.
In Thailand, rambutan trees were first planted in Surat Thani in 1926 by the Chinese Malay K. Vong in Ban Na San. An annual rambutan fair is held during August harvest time.
In Malaysia, rambutan flowers from March to July and again between June and November, usually in response to rain following a dry period. Flowering periods differ for other localities. Most, but not all, flowers open early in the day. Up to 100 flowers in each female panicle may be open each day during peak bloom. Initial fruit set may approach 25%, but a high abortion level contributes to a much lower level of production at harvest (1 to 3%). The fruit matures 15–18 weeks after flowering.
Rambutan cultivation in Sri Lanka mainly consists of small home gardens. Malwana, a village located in the Kelani River Valley, is popular for its rambutan orchards. Their production comes to market in May, June, and July, when it is very common to observe seasonal traders along the streets of Colombo. Sri Lanka also has some off-season rambutan production in January and February in areas such as Bibile, Medagama, and Monaragala.
Both male and female flowers are faintly sweet-scented and have functional nectaries at the ovary base. Female flowers produce two to three times more nectar than male flowers. Nectar sugar concentration ranges between 18% and 47% and is similar between the flower types. Rambutan is an important nectar source for bees in Malaysia.
Cross-pollination is a necessity because the anther is absent in most functionally female flowers. Although apomixis may occur in some cultivars, rambutan, like lychee, is dependent upon insects for pollination. In Malaysia, where only about 1% of the female flowers set fruit, no fruit is set on bagged flowers while hand pollination resulted in 13% fruit set. Pollinators may maintain a fidelity to either male or hermaphroditic flowers (trees), thus limiting pollination and fruit set under natural conditions where crossing between male and female flowers is required.


== Cultivars ==

Well over 200 cultivars were developed from selected clones available throughout tropical Asia. Most of the cultivars are also selected for compact growth, reaching a height of only 3–5 m for easier harvesting.
In Indonesia, 22 rambutan cultivars were identified with good quality, with five as leading commercial cultivars: 'Binjai', 'Lebak Bulus', 'Rapiah', 'Cimacan' and 'Sinyonya', with other popular cultivars including 'Simacan', 'Silengkeng', 'Sikonto' and 'Aceh kuning'. In Malaya, commercial varieties include 'Chooi Ang', 'Peng Thing Bee', 'Ya Tow', 'Azimat', and 'Ayer Mas'.
In Nicaragua, a joint World Relief–European Union team distributed seedlings to organizations such as Ascociación Pueblos en Acción Comunitaria in 2001 to more than 100 farmers. Some of these farmers saw the first production of rambutan from their trees in 2005–2006 with development directed at the local market.
In the Philippines, two cultivars of rambutan are distinguished by their seed. The common rambutan seed and fruit are difficult to separate, while the 'Maharlika Rambutan' fruit separates cleanly from its seed. The fruit taste and size of these two cultivars are identical, but the 'Maharlika Rambutan' is more popular with a higher price.


== Nutrients and phytochemicals ==
Rambutan fruit contains diverse nutrients but in modest amounts, with only manganese having moderate content at 16% of the Daily Value per 100 g consumed (right table; note data are for canned fruit in syrup, not as raw which may have different nutrient contents).
As an unpigmented fruit flesh, rambutan does not contain significant polyphenol content, but its colorful rind displays diverse phenolic acids, such as syringic, coumaric, gallic, caffeic and ellagic acids having antioxidant activity in vitro. Rambutan seeds contain equal proportions of saturated and unsaturated fatty acids, where arachidic (34%) and oleic (42%) acids, respectively, are highest in fat content.
The pleasant fragrance of rambutan fruit derives from numerous volatile organic compounds, including beta-damascenone, vanillin, phenylacetic acid and cinnamic acid.


== Gallery ==


== See also ==
Korlan
Longan
Lychee
Mamoncillo
Pulasan


== References ==