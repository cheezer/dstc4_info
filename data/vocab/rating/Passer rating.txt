Passer rating (also known as quarterback rating, QB rating, or passing efficiency in college football) is a measure of the performance of passers, primarily quarterbacks, in American football and Canadian football. There are two formulae currently in use: one used by both the National Football League (NFL) and Canadian Football League (CFL), and the other used in NCAA football. Passer rating is calculated using a player's passing attempts, completions, yards, touchdowns, and interceptions. Since 1973, passer rating has been the official formula used by the NFL to determine its passing leader.
Passer rating in the NFL is on a scale from 0 to 158.3. Passing efficiency in college football is on a scale from -731.6 to 1261.6.


== History ==
Before the development of the passer rating, the NFL struggled with how to crown a passing leader. In the mid-1930s, it was the quarterback with the most passing yardage. From 1938 to 1940, it was the quarterback with the highest completion percentage. In 1941, a system was created that ranked the league's quarterbacks relative to their peers' performance. Over the next thirty years the criteria used to crown a passing leader changed several times, but the ranking system made it impossible to determine a quarterback's rank until all the other quarterbacks were done playing that week, or to compare quarterback performances across multiple seasons. In 1971, NFL commissioner Pete Rozelle asked the league's statistical committee to develop a better system. The committee was headed by Don Smith of the Pro Football Hall of Fame, Seymour Siwoff of the Elias Sports Bureau, and NFL executive Don Weiss. Smith and Siwoff established passing performance standards based on data from all qualified pro football passers between 1960 and 1970, and used that data to create the passer rating. The formula was adopted by the NFL in 1973.


== NFL formula ==
The NFL passer rating formula includes four variables: completion percentage, yards per attempt, touchdowns per attempt, and interceptions per attempt. Each of those variables is scaled to a value between 0 and 2.375, with 1.0 being statistically average (based on league data between 1960-1970). When the formula was first created, a 66.7 rating indicated an average performance, and a 100+ rating indicated an excellent performance. However, passing performance has improved steadily since then and in 2008 the league average rating was 83.2. A perfect passer rating (158.3) requires at least a 77.5% completion rate, 12.5+ yards per attempt, a touchdown on at least 11.875% of attempts, and no interceptions.
The four separate calculations can be expressed in the following equations:

where
ATT = Number of passing attempts
COMP = Number of completions
YDS = Passing yards
TD = Touchdown passes
INT = Interceptions
Then, the above calculations are used to complete the passer rating:

Note: The result of each of the four components a through d is capped at 2.375, and components that result in a negative number are made to be zero.


== NCAA formula ==
The NCAA passing efficiency formula is similar to that of the NFL passer rating, but does not impose limits on the four components:

where
ATT = Number of passing attempts
COMP = Number of completions
YDS = Passing yards
TD = Touchdown passes
INT = Interceptions
The NCAA passer rating has an upper limit of 1,261.6 (every attempt is a 99-yard completion for touchdown), and a lower limit of -731.6 (every attempt is completed, but results in a 99-yard loss). A passer who throws only interceptions will have a -200 rating, as would a passer who only throws completed passes losing an average of 35.714 yards.


== Advantages ==
In 2011, Sports Illustrated published an article by Kerry Byrne of Cold Hard Football Facts highlighting the importance of passer rating in determining a team's success. "Put most simply," the article states, "you cannot be a smart football analyst and dismiss passer rating. In fact, it's impossible to look at the incredible correlation of victory to passer rating and then dismiss it. You might as well dismiss the score of a game when determining a winner. [...] Few, if any, are more indicative of wins and losses than passer rating. Teams that posted a higher passer rating went 203-53 (.793) in 2010 and an incredible 151-29 (.839) after Week 5." Byrne made an expanded defense of the passer rating and its importance for the Pro Football Researchers Association in 2012. The study noted that of the eight teams since 1940 to lead the league in both offensive passer rating and defensive passer rating, all won championships.


== Criticism ==
The NFL on its website states: "Passer rating is used to evaluate passers, not quarterbacks." The formula does not include rushing statistics, sacks, or fumbles, nor does it put added weight on performance during crucial situations such as third downs or fourth quarter scoring drives. Passer rating also cannot account for the quality of wide receivers or pass protection from the offensive line.
To account for the fact that the NFL's passer rating formula does not factor in sacks, Pro-Football-Reference.com introduced Adjusted Net Yards Per Attempt (ANY/A). Pro-Football-Reference.com says that the formula [(pass yards + 20*(pass TD) - 45*(interceptions thrown) - sack yards)/(passing attempts + sacks)] provides a better estimate of a quarterback's contributions. Still, this number does not include rushing or fumbles.
In 2011, ESPN, Inc. introduced a proprietary statistic called the Total Quarterback Rating (QBR) which it claims more accurately measures a quarterback's contributions to team wins. However, QBR has also been met with criticism amongst analysts and fans.


== Records ==


=== NFL ===

Highest passer rating, career (minimum 1,500 attempts): 106.0, Aaron Rodgers, 2005–2014
Highest passer rating, season (minimum 100 attempts): 122.5, Aaron Rodgers, 2011
Highest passer rating, rookie, season: 102.4, Robert Griffin III, 2012
Most times leading league in passer rating, career: 6, Steve Young, 1991–1994, 1997-1998, Sammy Baugh, 1937, 1940, 1943, 1945, 1947, 1949 
Most consecutive seasons leading league in passer rating, career: 4, Steve Young, 1991–1994 
Wide receiver Antwaan Randle El, with a passer rating of 157.5 from 21 completed passes of a possible 26, has the highest career rating of any non-QB with more than twenty attempts. Peyton Manning holds the record for the most games with a perfect passer rating (4). As of 2012, 61 NFL quarterbacks have completed a game with a perfect passer rating of 158.3, and seven have done so multiple times. Phil Simms holds the record for the highest passer rating in a Super Bowl, at 150.92 in Super Bowl XXI. Ben Roethlisberger holds the record for the lowest passer rating to win a Super Bowl, at 22.6 in Super Bowl XL.


=== NCAA ===

Highest passing efficiency, career (minimum 325 completions): 175.6, Sam Bradford, Oklahoma, 2007–2009 
Highest passing efficiency, season (minimum 15 attempts per game): 191.8, Russell Wilson, Wisconsin, 2011


== See also ==
List of NFL quarterbacks who have posted a perfect passer rating
List of NFL quarterbacks who have posted a passer rating of zero
NFL career passer rating leaders
Total Quarterback Rating


== References ==


== External links ==
NFL.com QB Rating Page
Online passer rating calculator for NFL/CFL, NCAA and AFL formulas
A look at and breakdown of the NFL Passer Rating
NFL Career Passer Rating Leaders
QB Rating Calculator
Adjustable NFL passer rating calculator and database. Stats from 1932 - present.
New York Times - The N.F.L.'s Passer Rating, Arcane and Misunderstood - January 14, 2004