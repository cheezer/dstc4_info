ReD (Retail Decisions) is a specialist global fraud prevention provider, serving merchants, issuers, acquirers, processors and switch networks. 


== Acquisition ==
August 2014 | ReD (Retail Decisions) became a wholly owned subsidiary of ACI Worldwide.


== History ==
ReD (Retail Decisions) was founded in January 2000 after the consolidation of the existing fraud prevention and security company, Card Clear Plc. CardClear Plc was formed in 1998 after the amalgamation of Card Clear UK, a card fraud prevention company in the retail sector, TBR, a card fraud prevention company operating in the Telecoms sector, Master Change, a niche bureau de change, HTEC, a manufacturer of information and retail systems and Inter Clear, a specialist in internet security services. In January 2000 the decision to focus the business solely on payment fraud prevention led Card Clear to divest Master Change, HTEC and Inter Clear. The newly consolidated company, continued operations with Card Clear in the UK and TBR in North America.
In May 2000 ReD launched its South African operation and, in August of that year, acquired the net assets of I.C.E Group Australia. Between 2001 and 2006 ReD continued its acquisitions with the rights to PRISM technology, Motorchange Ltd, Payment Plus, Fuelserv and E com, taking its operations global.


=== Acquisition by Palamon ===
ReD was acquired by Palamon Capital Partners, LP in December 2006 in a public to private deal worth £186 million. At the time of the acquisition ReD operated four business units:
Merchant Services – a global fraud prevention and payment processing business
European Fuel Cards – an issuer of fuel cards across Europe that can be used to pay for fuel and related products
Australian Fuel Cards – as Europe but the largest multi branded fuel card issuer in Australia
Australian Prepaid Cards – the leading processor of prepaid gift cards in Australia
In 2006, ReD continued its expansion acquiring Express Diesel, Liquidcard and Lomo Card Germany. The UK and Irish operations of the European Fuel Card business was sold to FleetCor Technologies Inc in August 2009. Proceeds were used to pay down external bank debt. The two Australian businesses were sold to Wright Express Corporation, with completion occurring in September 2010. The proceeds of this sale allowed for repayment of all external bank debt and a distribution to institutional shareholders that repaid their original investment in the business from 2006.
In August 2014 Payment technology company ACI Worldwide completed its $205 million purchase of Retail Decisions Plc (ReD), a leader in fraud prevention solutions.


== References ==


== External links ==
ReD http://www.redworldwide.com/