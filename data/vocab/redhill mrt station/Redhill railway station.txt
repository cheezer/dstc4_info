Redhill railway station serves the town of Redhill, Surrey, England. The station is a major interchange point on the Brighton Main Line 21 miles (34 km) south of London Victoria. It is managed by Southern, which operates most trains serving Redhill.


== History ==
The local topography determined that it was cheaper to build and operate a railway line between London and Brighton which by-passed the parliamentary borough and long-established market town of Reigate and instead passed through the nearby Redstone or Red Hill gap in the Reigate Foreign (countryside) parish. According to the Acts of Parliament establishing railways between London and Brighton, and London and Dover, the line was to be shared between Croydon and Red Hill after which these two would deviate. The London and Brighton Railway (L&BR) constructed the new line during 1840 and 1841, with the South Eastern Railway (SER) contributing half of the construction cost and taking ownership of the section between Stoats Nest, Coulsdon, and Redhill. (The SER had however been running services over the line since 1842.) The inevitable and continuing conflict between the two railway companies over the use of this joint line gave rise to the construction of four railway stations at the site of what was then a hamlet on the eastern side of Reigate.


=== Red Hill and Reigate Road (London & Brighton Railway) station ===

The original station was opened by the London and Brighton Railway on 12 July 1841 on a site to the south of the proposed junction with the South Eastern Main Line to Dover. The nearby market town was served by a horse-drawn omnibus service operated by the railway. This station was designed by the architect David Mocatta, and was one of a series of standardised modular buildings used by the railway. It closed on 15 April 1844, when the LBR began to share the SER Redhill and Reigate station and was demolished soon afterwards.


=== Redhill/Reigate (SER) stations ===
On 26 May 1842 the SER opened what was originally called 'Redhill', but later misleadingly renamed 'Reigate' station, on their own stretch of line just beyond the junction. Passengers transferring between the two railways did so at the old Merstham station further up the line. The SER wanted to replace their 'Reigate' station with a joint station immediately before the junction, but the L&BR opposed the plan. As a result the SER forced the issue by ending the arrangements at Merstham, thereby forcing passengers to transfer between the two stations at Redhill by foot.


=== Redhill and Reigate station ===

On 15 April 1844 the SER built a new station at the present site, named 'Redhill and Reigate' which was to be used by both railways as the interchange station. On the same day the two existing stations were closed. The branch line to Reigate was opened in 1849 with a new station called Reigate Town. Nevertheless the London Brighton and South Coast Railway (the successor of the L&BR) continued to operate the omnibus service for its own passengers.


=== Redhill Junction station ===

The SER 'Redhill and Reigate' station was rebuilt and enlarged on the same site in August 1858 when it was renamed 'Redhill Junction'. The chronic congestion at the station was however eased after 1 May 1868 when Redhill ceased to be on the South Eastern Main Line to Dover following the opening of the 'Sevenoaks cut off' line between St Johns and Tonbridge railway station. A ten year agreement between the SER and the London Brighton and South Coast Railway (LBSCR the successor to the L&BR after July 1846) over the use of the station and lines to Coulsdon was signed 1 February 1869 and renewed ten years later. However, during the 1880s, as traffic increased the disputes between over the use of line and Redhill station re-occurred. This became known as the 'Southern Lines Controversy' and ultimately led to the construction of the Quarry Line by the LB&SCR in 1899, which avoided Redhill. The LB&SCR diverted many of its Brighton main line to the new line, but retained running powers over the original line and the use of Redhill station. These were continued until both the SER and the LBSCR came under the ownership of the Southern Railway 1 January 1923 and the name of the joint station was changed to Redhill in July 1929.


=== Electrification ===
The Brighton main line and the line from Redhill to Reigate were both electrified 1 January 1933. The Redhill to Tonbridge Line was electrified in 1993.


== Description ==
Redhill station is at the junction of the Brighton Main Line, which runs north to London and south to Gatwick Airport and Brighton, with the ex-SER North Downs Line, which runs west to Guildford and Reading, and the Redhill to Tonbridge Line, to the east.

The station has three passenger platforms and a parcels bay (which is now out of use). Platforms 1 and 2 are an island on the northbound (Up) side and Platform 3 and the old parcels dock are on the southbound (Down) side. There are two through lines between platforms 2 and 3. All passenger platforms are subdivided into 'a' (north end) and 'b' (south end), and all are of 12 car length. All platforms have access to all routes, although there is no access from either through line to or from the North Downs Line - all traffic from this direction must pass through a platform.
Platforms are linked by a subway, and by an out-of use parcels/staff bridge. There are lifts from the platforms to the subway and a level entrance from the Platform 3 exit, but no level entrance at the main entrance, which is at street level. The main entrance faces the town centre, and is opposite Redhill bus station.
The ticket office has four windows and four Shere FASTticket self-service ticket machines, and there are four automatic ticket barriers. There is a Puccino's coffee shop on platforms 1 and 2. There is an additional Shere FASTticket machine at the platform 3 exit.


== Services ==

Southern operate most train services, others being provided by First Great Western.
Thameslink operate peak-time services between Bedford and Brighton, and on rare occasions Southeastern operate services between London Bridge and Hastings via Redhill.
General off-peak train service frequency per hour:
1 to Gatwick Airport (FGW)
2 to Horsham (Southern)
1 to Portsmouth Harbour and Bognor Regis via Horsham (Southern)
1 to Southampton Central and Bognor Regis via Horsham (Southern)
4 to London Bridge (Southern)
2 to London Victoria (Southern)
2 to Reading (FGW)
1 to Reigate (Southern)
1 to Tonbridge (Southern)


== Motive power depot ==

An engine shed, turntable and locomotive coaling and servicing facilities were installed by the South Eastern Railway in 1855 in the area between the Brighton and Tonbridge lines. These facilities were rebuilt by the Southern Railway in 1924 and lasted until the end of steam in the area in 1965. The site of the depot remained in use as a stabling point for many years after this. The sidings remain in place, but appear to have fallen into disuse in recent years.


== References ==


== External links ==
Southern Railway station information page