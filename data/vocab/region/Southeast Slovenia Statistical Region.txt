The Southeast Slovenia Statistical Region (Slovene: Jugovzhodna Slovenija statistična regija) is a statistical region in the south-east of Slovenia. The largest town in the region is Novo Mesto. It is the largest region in Slovenia. It has the highest share of young people (aged 0-14) with 15.8%. The population is on the increase due to a positive net migration rate as well as the natural increase.


== Municipalities ==
The Southeast Slovenia Statistical Region comprises the following 21 municipalities:


== Demographics ==
The population in 2004 was 139,095. It has a total area of 2,675 km².


== Economy ==
Employment structure: 41.5% services, 50.7% industry, 7.5% agriculture.


=== Tourism ===
It attracts only 2.9% of the total number of tourists in Slovenia, most being from Slovenia (48.1%).


== Transportation ==
Length of motorways: 6 km
Length of other roads: 1,460 km


== Sources ==
Slovenian regions in figures 2006