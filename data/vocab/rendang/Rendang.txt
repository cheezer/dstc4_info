Rendang is a spicy meat dish which originated from the Minangkabau ethnic group of Indonesia, and is now commonly served across the country. One of the characteristic foods of Minangkabau culture, it is served at ceremonial occasions and to honour guests. Rendang is also served among the Malay community in Malaysia, Singapore, Brunei and Southern Philippines. Rendang is traditionally prepared by the Minangkabau community during festive occasions such as traditional ceremonies, wedding feasts and Hari Raya (Eid al-Fitr). Culinary experts often describe rendang as: ‘West Sumatran caramelised beef curry’. In 2011 an online poll by 35,000 people held by CNN International chose rendang as the number one dish of their ‘World's 50 Most Delicious Foods (Readers' Pick)’ list.


== Composition and cooking method ==
The cooking technique flourished because of its role in preserving meat in a tropical climate. Prior to refrigeration technology, this style of cooking enabled preservation of the large amount of meat. Rendang is rich in spices. Along with the main meat ingredient, rendang uses coconut milk (Minangkabau: karambia) and a paste of mixed ground spices, which includes ginger, galangal, turmeric leaves, lemongrass, garlic, shallot, chilli and other spices. This spice mixture is called ‘pemasak’ in Minangkabau. The spices, garlic, shallot, ginger and galangal used in rendang have antimicrobial properties and serve as natural organic preservatives. If cooked properly, dry rendang can last for as long as four weeks.
Traditional Padang rendang takes hours to cook. Cooking rendang involves pounding and grinding ingredients as well as slow cooking, and so is time-consuming and requires patience. The meat pieces are slowly cooked in coconut milk and spices until almost all the liquid is gone, allowing the meat to absorb the condiments. The cooking process changes from boiling to frying as the liquid evaporates. The slow cooking process allows the meat to absorb all the spices and become tender. To cook the meat until tender with almost all the liquid evaporated requires great care, if the meat is not to be burnt or be spoilt. Because of its generous use of numerous spices, rendang is known for having a complex and unique taste.
Rendang is often served with steamed rice, ketupat (a compressed rice cake) or lemang (glutinous rice cooked in bamboo tubes), accompanied with vegetable side dishes such as boiled cassava leaf, cubadak (young jackfruit gulai), cabbage gulai and lado (red or green chilli pepper sambal).


== Cultural significance ==

Rendang is revered in Minangkabau culture as an embodiment of the philosophy of musyawarah, discussion and consultation with elders. It has been claimed that the four main ingredients represent Minangkabau society as a whole:
The meat (dagiang) symbolises the Niniak Mamak, the traditional clan leaders such as the datuk, the nobles, royalty and revered elders.
The coconut milk (karambia) symbolises the Cadiak Pandai, intellectuals, teachers, poets and writers.
The chilli (lado) symbolises the Alim Ulama, clerics, ulama and religious leaders. The hotness of the chilli symbolises Sharia.
The spice mixture (pemasak) symbolises the rest of Minangkabau society.
In Minangkabau tradition, rendang is a requisite dish for special occasions in traditional Minang ceremonies, from birth ceremonies to circumcision, marriage, Qur'an recitals, and religious festivals such as Eid al-Fitr and Eid al-Adha.


== History ==

Rendang originates from the Sumatran Minangkabau region. One of the earliest written records of rendang is from the early 16th century Hikayat Amir Hamzah. The making of rendang spreads from Minangkabau region to Mandailing, Riau, Jambi, across the strait to Malacca and Negeri Sembilan, resulting in a variety of rendang traditions.
The popularity of rendang has spread widely from its original domain because of the merantau (migrating) culture of Minangkabau people. Overseas Minangkabau leave their home town to start a career in other Indonesian cities as well as neighbouring countries, and Padang restaurants, Minangkabau eating establishments that are ubiquitous in Indonesian cities, spring up. These Padang restaurants have introduced and popularised rendang and other Padang food dishes across Indonesia, Malaysia, Singapore, and the wider world.
Andalas University historian, Prof. Gusti Asnan suggests that rendang began to spread across the region when Minangkabau merchants and migrant workers began to trade and migrate to Malacca in the 16th century. ‘Because the journey through the river waterways in Sumatra took much time, a durable preserved dry rendang is suitable for long journey.’ The dried Padang rendang is a durable food, good to consume for weeks, even when left at room temperature.


== Types ==

In Minangkabau culinary tradition, there are three recognised stages in cooking meat in spicy coconut milk. The dish which results is categorised according to the liquid content of the cooked coconut milk, which ranges from the most wet and soupy to the most dry: Gulai — Kalio — Rendang. The ingredients of gulai, kalio and rendang are almost identical with the exceptions that gulai usually has less red chilli pepper and more turmeric, while rendang has richer spices.
If pieces of meat are cooked in spicy coconut milk and the process stopped right when the meat is done and the coconut milk has reached its boiling point, the dish is called ‘gulai’. If the process continues until the coconut milk is partly evaporated and the meat has started to brown, the dish is called ‘kalio’. For a traditional dry rendang, the process continues hours beyond this, until the liquid has all but completely evaporated and the colour turns to a dark brown, almost black colour. Thus not only liquid content but also colour indicate which type of rendang is involved: gulai is light yellow, kalio is brown and rendang is very dark brown. Today, one mostly finds only two simpler categories of rendang: either dry or wet.


=== Dried rendang ===
According to Minangkabau tradition, their true rendang is the dry one. Rendang is diligently stirred, attended and cooked for hours until the coconut milk evaporated and the meat absorbed the spices. It is still served for special ceremonial occasions or to honour guests. If cooked properly, dried rendang can last for three to four weeks stored in room temperature and still good to consume. It can even last months stored in a refrigerator, and up to six months if frozen.


=== Wet rendang or kalio ===

Wet rendang, more accurately identified as ‘kalio’, is a type of rendang that is cooked for a shorter period of time and much of the coconut milk liquid has not evaporated. If stored at room temperature, kalio lasts less than a week. Kalio usually has a light golden brown colour, paler than dry rendang.
Outside of its native land in Minangkabau, rendang is also known in neighbouring countries such as Malaysia, Singapore and the Philippines. Most Malaysian rendang is more like kalio, lighter in colour and taste when compared with its Minangkabau counterpart. Malaysian rendang has several variants, such as Kelantan rendang and Negeri Sembilan rendang. Malaysian styles of rendang are typically cooked for shorter periods, and use kerisik (toasted grated coconut) to thicken the spice, instead of stirring over a low heat for many hours to evaporate the coconut milk as Indonesian rendang requires. Nonetheless, in Malaysia the rendang Tok variant, found in the state of Perak, is a dry one.
Other ethnic groups in Indonesia also have adopted a version of rendang into their daily diet. For example, in Java, other than Padang rendang sold in Padang restaurants, the Javanese cooked a wet rendang, slightly sweeter and less spicy to accommodate Javanese tastes. Through colonial ties the Dutch are also familiar with rendang and often serve the wet kalio version in the Netherlands — usually as part of a rijsttafel.


== Variations ==

Rendang is made from beef (or occasionally beef liver, chicken, mutton, water buffalo, duck, or vegetables like jackfruit or cassava). Chicken or duck rendang also contains tamarind and is usually not cooked for as long as beef rendang.
The original Indonesian-Minangkabau rendang has two categories, rendang darek and rendang pesisir. Rendang darek (‘land rendang’) is an umbrella term for dishes from old regions in mountainous areas of Minangkabau such as Batusangkar, Agam, Lima Puluh Kota, Payakumbuh, Padang Panjang and Bukittinggi. It mainly consists of beef, offal, poultry products, jackfruit, and many other vegetables and animal products which are found in these places. Rendang pesisir (‘coastal rendang’) is from the coastal regions of Minangkabau such as Pariaman, Padang, Painan and Pasaman. Rendang pesisir mainly consists of seafood, although it is not unusual for them to incorporate beef or water buffalo meat in their rendang.
Indonesian Rendang variations:
Rendang daging: meat rendang. The most common rendang is made from beef, but may also be from water buffalo, goat, mutton or lamb, speciality of Padang.
Rendang ayam: chicken rendang, speciality of Batusangkar and Bukittinggi.
Rendang baluik (rendang belut): eel rendang, speciality of Solok. In the Solok dialect, it is also called ‘randang baluk’.
Rendang cubadak (rendang nangka): jackfruit rendang, speciality of Payakumbuh.
Rendang hati: cow's liver rendang, speciality of Minangkabau.
Rendang itiak (rendang bebek): duck rendang, speciality of Bukittinggi.
Rendang jamur: mushroom rending
Rendang jantung pisang: banana blossom rendang, speciality of Minangkabau.
Rendang jariang (rendang jengkol): jengkol rendang, speciality of Bukittinggi.
Rendang jo kantang: beef rendang with baby potatoes, speciality of Kapau.
Rendang lokan (rendang tiram): marsh clam rendang, speciality of coastal regions of Minangkabau, such as Pariaman, Painan and Pesisir Selatan.
Rendang paru: cow's lung rendang, speciality of Payakumbuh.
Rendang pucuak ubi (rendang daun singkong): cassava leaves rendang, speciality of Minangkabau.
Rendang runtiah (rendang suir): shredded beef or poultry rendang, speciality of Payakumbuh.
Rendang tahu: tofu rendang, speciality of Minangkabau.
Rendang talua (rendang telur): egg rendang, speciality of Payakumbuh.
Rendang tempe: tempe rendang, speciality of Minangkabau.
Rendang tongkol: mackerel tuna rendang, speciality of coastal regions of Minangkabau.


== References ==


== See also ==

Cuisine of Indonesia
Curry
(Indonesian) Urang Minang.com - Inilah Rendang Minang Juara dunia itu