Primm Valley Resort & Casino (formerly known as Primadonna Resort & Casino) is a hotel and casino located in Primm, an unincorporated area in Clark County, Nevada, United States. It includes the Primm Valley Golf Club across the border in California.
The hotel offers 624 rooms and suites. The casino has over 1,500 slot machines, as well as table games, keno, and a Lucky's Race & Sports Book. Primm Valley Resort and Casino is owned by Primm Valley Resorts.
Free shuttle buses operate between the Primm Valley Resort and the resorts other hotels, Whiskey Pete's and Buffalo Bill's. Primm Valley has an exhibit featuring the bullet-riddled car that Bonnie and Clyde were driving when they were killed. As of October 2011 the car is located inside the casino of Whiskey Pete's.


== History ==

Built by Primadonna Resorts and opened in 1990 as the Primadonna Resort & Casino.

The Fashion Outlets were added as a part of the Primadonna Resort in 1998.
On October 31, 2006, MGM Mirage announced plans to sell the Primm Valley Resorts, which includes the Primm Valley Resort, to Herbst Gaming for $400 million. The sign in front of the resort has since been changed from "Primm Valley Resorts and Casino" to "Terrible's Resort and Casino", although the resort is still apparently operating under the name "Primm Valley".
As of March 23, 2009, the Herbst family relinquished control of the Terrible's Primm Valley Casino Resort, along with both other properties, to their lenders.


== References ==


== External links ==
Official website