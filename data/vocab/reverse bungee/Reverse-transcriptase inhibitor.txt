Reverse-transcriptase inhibitors (RTIs) are a class of antiretroviral drugs used to treat HIV infection or AIDS, and in some cases hepatitis B. RTIs inhibit activity of reverse transcriptase, a viral DNA polymerase that is required for replication of HIV and other retroviruses.


== MechanismEdit ==
When HIV infects a cell, reverse transcriptase copies the viral single stranded RNA genome into a double-stranded viral DNA. The viral DNA is then integrated into the host chromosomal DNA, which then allows host cellular processes, such as transcription and translation to reproduce the virus. RTIs block reverse transcriptase's enzymatic function and prevent completion of synthesis of the double-stranded viral DNA, thus preventing HIV from multiplying.
A similar process occurs with other types of viruses. The hepatitis B virus, for example, carries its genetic material in the form of DNA, and employs a RNA-dependent DNA polymerase to replicate. Some of the same compounds used as RTIs can also block HBV replication; when used in this way they are referred to as polymerase inhibitors.


== TypesEdit ==
RTIs come in three forms:
Nucleoside analog reverse-transcriptase inhibitors (NARTIs or NRTIs)
Nucleotide analog reverse-transcriptase inhibitors (NtARTIs or NtRTIs)
Non-nucleoside reverse-transcriptase inhibitors (NNRTIs)
The antiviral effect of NRTIs and NtRTIs is essentially the same; they are analogues of the naturally occurring deoxynucleotides needed to synthesize the viral DNA and they compete with the natural deoxynucleotides for incorporation into the growing viral DNA chain. However, unlike the natural deoxynucleotides substrates, NRTIs and NtRTIs lack a 3′-hydroxyl group on the deoxyribose moiety. As a result, following incorporation of an NRTI or an NtRTI, the next incoming deoxynucleotide cannot form the next 5′–3′ phosphodiester bond needed to extend the DNA chain. Thus, when an NRTI or NtRTI is incorporated, viral DNA synthesis is halted, a process known as chain termination. All NRTIs and NtRTIs are classified as competitive substrate inhibitors.
Unfortunately, NRTIs/NtRTIs compete as substrates for not only viral but also host DNA synthesis, acting as chain terminators for both. The former explains NRTIs'/NtRTIs' antiviral effect, while the latter explains their drug toxicity/side effects.
In contrast, NNRTIs have a completely different mode of action. NNRTIs block reverse transcriptase by binding directly to the enzyme. NNRTIs are not incorporated into the viral DNA like NRTIs, but instead inhibit the movement of protein domains of reverse transcriptase that are needed to carry out the process of DNA synthesis. NNRTIs are therefore classified as non-competitive inhibitors of reverse transcriptase.


=== Nucleoside analog reverse-transcriptase inhibitors (NARTIs or NRTIs)Edit ===
Nucleoside analog reverse-transcriptase inhibitors (NARTIs or NRTIs) compose the first class of antiretroviral drugs developed. In order to be incorporated into the viral DNA, NRTIs must be activated in the cell by the addition of three phosphate groups to their deoxyribose moiety, to form NRTI triphosphates. This phosphorylation step is carried out by cellular kinase enzymes.
Zidovudine, also called AZT, ZDV, and azidothymidine, has the trade name Retrovir. Zidovudine was the first antiretroviral drug approved by the FDA for the treatment of HIV.
Didanosine, also called ddI, with the trade names Videx and Videx EC, was the second FDA-approved antiretroviral drug. It is an analog of adenosine.
Zalcitabine, also called ddC and dideoxycytidine, has the trade name Hivid. This drug has been discontinued by the manufacturer.
Stavudine, also called d4T, has trade names Zerit and Zerit XR.
Lamivudine, also called 3TC, has the trade name Zeffix and Epivir. It is approved for the treatment of both HIV and hepatitis B.
Abacavir, also called ABC, has the trade name Ziagen, is an analog of guanosine.
Emtricitabine, also called FTC, has the trade name Emtriva (formerly Coviracil). Structurally similar to lamivudine, it is approved for the treatment of HIV and undergoing clinical trials for hepatitis B.
Entecavir, also called ETV, is a guanosine analog used for hepatitis B under the trade name Baraclude. It is not approved for HIV treatment.


==== Nucleotide analog reverse-transcriptase inhibitors (NtARTIs or NtRTIs)Edit ====
As described above, host cells phosphorylate nucleoside analogs to nucleotide analogs. The latter serve as poison building blocks (chain terminators) for both viral and host DNA, causing respectively the desired antiviral effect and drug toxicity/side effects. Taking nucleotide analog reverse-transcriptase inhibitors (NtARTIs or NtRTIs) directly obviates the intitial phosphorylation step.
Tenofovir, also known as TDF is a so-called 'prodrug' with the active compound deactivated by a molecular side chain that dissolves in the human body allowing a low dose of tenofovir to reach the site of desired activity. One example of the prodrug form is tenofovir disoproxil fumarate with the trade name Viread (Gilead Sciences Inc USA). It is approved in the USA for the treatment of both HIV and hepatitis B.
Adefovir, also known as ADV or bis-POM PMPA, has trade names Preveon and Hepsera. It not approved by the FDA for treatment of HIV due to toxicity issues, but a lower dose is approved for the treatment of hepatitis B.
While often listed in chronological order, NRTIs/NtRTIs are nucleoside/nucleotide analogues of cytidine, guanosine, thymidine and adenosine:
Thymidine analogues: zidovudine (AZT) and stavudine (d4T)
Cytidine analogues: zalcitabine (ddC), lamivudine (3TC), and emtricitabine (FTC)
Guanosine analogues: abacavir (ABC) and entecavir (ETV)
Adenosine analogues: didanosine (ddI), tenofovir (TDF), and adefovir (ADV)


=== Non-nucleoside reverse-transcriptase inhibitors (NNRTIs)Edit ===
Non-nucleoside reverse-transcriptase inhibitors (NNRTIs) are the third class of antiretroviral drugs that were developed. In all cases, patents remain in force until beyond 2007. This class of drugs was first described at the Rega Institute for Medical Research (Belgium)
Efavirenz 
Efavirenz has the trade names Sustiva and Stocrin.
Nevirapine 
Nevirapine has the trade name Viramune.
Delavirdine 
Delavirdine, currently rarely used, has the trade name Rescriptor.
Etravirine 
Etravirine has the trade name Intelence, and was approved by the FDA in 2008.
Rilpivirine 
Rilpivirine has the trade name Edurant, and was approved by the FDA in May 2011.


=== Portmanteau inhibitorsEdit ===
Researchers have designed molecules which dually inhibit both reverse transcriptase (RT) and integrase (IN). These drugs are a type of "portmanteau inhibitors".


== Mechanisms of resistance to reverse transcriptase inhibitorsEdit ==
While NRTIs and NNRTIs alike are effective at terminating DNA synthesis and HIV replication, HIV can and eventually does develop mechanisms that confer the virus resistance to the drugs. HIV-1 RT does not have proof-reading activity. This, combined with selective pressure from the drug, leads to mutations in reverse transcriptase that make the virus less susceptible to NRTIs and NNRTIs. Aspartate residues 110, 185, and 186 in the reverse transcriptase polymerase domain are important in the binding and incorporation of nucleotides. The side chains of residues K65, R72, and Q151 interact with the next incoming nucleotide. Also important is L74, which interacts with the template strand to position it for base pairing with the nucleotide. Mutation of these key amino acids results in reduced incorporation of the analogs.


=== NRTI resistanceEdit ===
There are two major mechanisms of NRTI resistance. The first being reduced incorporation of the nucleotide analog into DNA over the normal nucleotide. This results from mutations in the N-terminal polymerase domain of the reverse transcriptase that reduce the enzyme’s affinity or ability to bind to the drug . A prime example for this mechanism is the M184V mutation that confers resistance to lamivudine (3TC) and emtricitabine (FTC). Another well characterized set of mutations is the Q151M complex found in multi-drug resistant HIV which decreases reverse transcriptase’s efficiency at incorporating NRTIs, but does not affect natural nucleotide incorporation. The complex includes Q151M mutation along with A62V, V75I, F77L, and F116Y. A virus with Q151M alone is intermediately resistant to zidovudine (AZT), didanosine (ddI), zalcitabine (ddC), stavudine (d4T), and slightly resistant to abacavir (ABC). A virus with Q151M complexed with the other four mutations becomes highly resistant to the above drugs, and is additionally resistant to lamivudine (3TC) and emtricitabine (FTC).

The second mechanism is the excision or the hydrolytic removal of the incorporated drug or pyrophosphorlysis. This is a reverse of the polymerase reaction in which the pyrophosphate/PPI released during nucleotide incorporation reacts with the incorporated drug (monophosphate) resulting in the release of the triphosphate drug. This ‘unblocks’ the DNA chain, allowing it to be extended, and replication to continue. Excision enhancement mutations, typically M41L, D67N, K70R, L210W, T215Y/F, and K219E/Q, are selected for by thymidine analogs AZT and D4T; and are therefore called thymidine analog mutations (TAMs). Other mutations including insertions and deletions in the background of the above mutations also confer resistance via enhanced excision.


=== NNRTI resistanceEdit ===
NNRTIs do not bind to the active site of the polymerase but in a less conserved pocket near the active site in the p66 subdomain. Their binding results in a conformational change in the reverse transcriptase that distorts the positioning of the residues that bind DNA, inhibiting polymerization. Mutations in response to NNRTIs decrease the binding of the drug to this pocket. Treatment with a regimen including efavirenz (EFV) and nevirapine (NVP) typically results in mutations L100I, Y181C/I, K103N, V106A/M, V108I, Y188C/H/L and G190A/S. There are three main mechanisms of NNRTI resistance. In the first NRTI mutations disrupt specific contacts between the inhibitor and the NNRTI binding pocket. An example of this is K103N and K101E which sit at the entrance of the pocket, blocking the entrance/binding of the drug. A second mechanism is the disruption of important interactions on the inside of the pocket. For example Y181C and Y188L result in the loss of important aromatic rings involved in NNRTI binding. The third type of mutations result in changes in the overall conformation or the size of the NNRTI binding pocket. An example is G190E, which creates a steric bulk in the pocket, leaving little or no room for an NNRTI to tightly bind.


== See alsoEdit ==
Discovery and development of non-nucleoside reverse-transcriptase inhibitors
Protease inhibitors
Discovery and development of nucleoside and nucleotide reverse-transcriptase inhibitors


== ReferencesEdit ==


== External linksEdit ==
Reverse Transcriptase Inhibitors at the US National Library of Medicine Medical Subject Headings (MeSH)