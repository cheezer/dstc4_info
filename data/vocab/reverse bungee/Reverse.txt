Reverse may refer to:
The reverse side of currency or a flag; see Obverse and reverse
A change in the direction of:
the movement of a motor or other prime mover; see Transmission (mechanics)
an engineering design: see Reverse engineering
a jet engine's thrust: see Thrust reversal

Reverse lookup (disambiguation) as in:
Reverse telephone directory
Reverse DNS lookup
Backmasking


== In sports and other games ==
Reverse (American football), a trick play in American football
Reverse swing, a cricket delivery
Reverse (bridge), a type of bid in contract bridge


== Media ==
Reverse (Morandi album), 2005
Reverse (Eldritch album), 2001
Revers (film), 2009
"Reverse", a 2014 song by SomeKindaWonderful


== Others ==
Taking a video, movie or sound and playing it backwards. (See Backwards masking)
REVERSE, an art space located in Williamsburg, Brooklyn


== See also ==
Auto-reverse
Reverse swing (disambiguation)
Revers, display of reverse side of jacket cuffs, etc.
Reversal (disambiguation)
Reversion (disambiguation)
Reversing (disambiguation)