Jollof rice /ˈdʒɒləf/, also called Benachin (Wolof: "one pot"), is a popular dish in many West African countries, especially Nigeria and Ghana. Its name is derived from the name of the Wolof people.
According to the The Diner's Dictionary: Word Origins of Food and Drink "Jollof rice is a subject of great debate in West Africa. Every country has its own version, and abhors "inauthentic* variations." The most common basic ingredients include rice, tomatoes and tomato paste, onions, salt, and hot red pepper. Beyond that, nearly any kind of meat, vegetable, or spice can be added.


== Ingredients ==
The dish consists of rice, tomatoes and tomato paste, onions, salt, spices (such as nutmeg, ginger, Scotch bonnet (pepper), and cumin) and chili peppers; optional ingredients can be added such as vegetables, meats, or fish.
The cooking method for Jollof rice begins with using oil (palm or peanut oil) to fry finely-chopped onions, tomatoes and ground pepper (plus any other optional seasoning); adding stock; and then cooking the rice in this mixture so it takes up all the liquid. The rice takes on a characteristic orange color from the mixture. It can be served with cooked meat, such as chicken or fish, and vegetables separately on the plate or they can be stirred in at the end. Optional ingredients can include garlic, peas, thyme, tea-bush leaves, partminger (a herb from the basil family), and curry powder. It is also often served with fried plantain and salad.


== See also ==


== References ==