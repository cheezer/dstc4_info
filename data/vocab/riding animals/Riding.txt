Riding is a homonym of two distinct English words:
From the word ride:
Equestrianism, riding a horse
Riding animal, an animal bred or trained for riding
Riding hall, a building designed for indoor horse riding
Ridin', a song by Chamillionaire
From Old English *þriðing:
Riding (country subdivision), an administrative division of a county, or similar district
Electoral district (Canada), a Canadian term for an electoral district
Riding association, Canadian political party organization at the riding level
Riding officer, a name once used for customs officials who patrolled for smugglers on beaches and other informal landing spots
Common Riding, an event celebrated in some Scottish towns to commemorate the guarding the boundaries of the town's common land by local men


== Other uses ==
Riding (surname)