List of rivers in Minnesota (U.S. state).


== Alphabetically ==


=== A–C ===


=== D–H ===


=== I–L ===


=== M–O ===


=== P–R ===


=== S ===


=== T–Z ===


== By drainage basin ==
This list is arranged by drainage basin, with respective tributaries indented under each larger stream's name.


=== Great Lakes Drainage Basin ===


==== Lake Superior Drainage Basin ====
Lake Superior
Pigeon River
Swamp River
Stump River
Lower Stump River

Royal River
Crocodile River

Reservation River
Flute Reed River
Brule River
Greenwood River
South Brule River

Kadunce River
Kimball Creek
Devil Track River
Little Devil Track River

Fall River
Cascade River
North Branch Cascade River

Poplar River
Tait River

Onion River
West Branch Onion River

Temperance River
Sawbill Creek
Kelso River

Vern River

Cross River
Two Island River
Caribou River
Little Manitou River
Manitou River
South Branch Manitou River

Little Marais River
Baptism River
East Branch Baptism River
West Branch Baptism River

Beaver River
East Branch Beaver River
West Branch Beaver River

Split Rock River
East Split Rock River
West Split Rock River

Gooseberry River
Skunk Creek
Little Gooseberry River

Encampment River
Stewart River
Little Stewart River

Knife River
Little Knife River, located in St. Louis County
West Branch Knife River
Little Knife River, located in Lake County

Little Sucker River
Big Sucker Creek
French River
Talmadge River
Lester River
St. Louis River
Pokegama River
Red River
Little River
Midway River
Pine River
Cloquet River
Us-kab-wan-ka River
Beaver River
Otter River
Little Cloquet River
West Branch Cloquet River
Langley River
Little Langley River

Artichoke River
Ahmik River
McCarty River
East Savanna River
Floodwood River
West Branch Floodwood River

Whiteface River
Little Whiteface River (South), located south of Meadowlands
Little Whiteface River (North), located east of Meadowlands
Paleface River
North Branch Whiteface River
South Branch Whiteface River

East Swan River
West Swan River

West Two River
East Two River
Embarrass River
Partridge River
South Branch Partridge River

East River
North River

Nemadji River
South Fork Nemadji River
Net River
Little Net River

Blackhoof River


=== Mississippi River Drainage Basin ===


==== Lower Mississippi Drainage Basin ====
Mississippi River
Missouri River (MO, IA)
Little Sioux River
Ocheyedan River
West Fork Little Sioux River

Big Sioux River (IA)
Rock River
Little Rock River
East Branch Rock River

Split Rock Creek
Pipestone Creek

Flandreau Creek

Des Moines River
East Fork Des Moines River
Heron Lake Outlet
Jack Creek
Diversion Creek
Okabena Creek

Lime Creek
Beaver Creek

Iowa River (IA)
Cedar River
Shell Rock River
Winnebago River (IA)
Lime Creek

Little Cedar River (Iowa and Minnesota), a tributary of the Cedar River rising in Mower County, Minnesota
Little Cedar River (Dodge County, Minnesota), a tributary of the Cedar River entirely in Dodge County

Wapsipinicon River
Upper Iowa River
Pine Creek
Little Iowa River
North Branch Upper Iowa River

Winnebago Creek
Crooked Creek
North Fork Crooked Creek
South Fork Crooked Creek

Root River
South Fork Root River
South Branch Root River
North Branch Root River
Middle Branch Root River

Pine Creek
Whitewater River
North Fork Whitewater River
Middle Fork Whitewater River

South Fork Whitewater River

Zumbro River
North Fork Zumbro River
Middle Fork Zumbro River
North Branch Middle Fork Zumbro River
South Branch Middle Fork Zumbro River

South Fork Zumbro River

Wells Creek
Hay Creek
Cannon River
North Cannon River, a distributary
Little Cannon River (Cannon River), near Cannon Falls
Straight River
Little Cannon River (Sabre Lake), near Kilkenny

Vermillion River
South Branch Vermillion River


==== St. Croix River Drainage Basin ====
Mississippi River
St. Croix River
Sunrise River
North Branch Sunrise River
West Branch Sunrise River
South Branch Sunrise River

Snake River
Groundhouse River
South Fork Groundhouse River
West Fork Groundhouse River

Ann River
Little Ann River

Knife River

Kettle River
Grindstone River
North Branch Grindstone River
South Branch Grindstone River

Pine River
Willow River
Little Willow River

Moose Horn River
Portage River
West Fork Moose Horn River

Split Rock River
Dead Moose River
West Branch Kettle River

Sand Creek
Lower Tamarack River
Upper Tamarack River


==== Minnesota River Drainage Basin ====
Mississippi River
Minnesota River
Credit River
Sand Creek
High Island Creek
Rush River
South Branch Rush River
Middle Branch Rush River
North Branch Rush River

Blue Earth River
Le Sueur River
Maple River
Cobb River
Little Cobb River

Little Le Sueur River

Watonwan River
Perch Creek
South Fork Watonwan River
North Fork Watonwan River

Elm Creek
East Branch Blue Earth River
Middle Branch Blue Earth River
West Branch Blue Earth River

Little Cottonwood River
Cottonwood River
Sleepy Eye Creek
Plum Creek

Redwood River
Hawk Creek
Yellow Medicine River
Spring Creek
North Branch Yellow Medicine River
South Branch Yellow Medicine River

Chippewa River
Shakopee Creek
East Branch Chippewa River
Little Chippewa River

Lac qui Parle River
West Branch Lac qui Parle River

Pomme de Terre River
Yellow Bank River
North Fork Yellow Bank River
South Fork Yellow Bank River

Whetstone River
Little Minnesota River


==== Upper Mississippi River Drainage Basin ====
Mississippi River
Rice Creek
Rum River
West Branch Rum River

Crow River
North Fork Crow River
Middle Fork Crow River
Skunk River

South Fork Crow River
Buffalo Creek

Elk River
St. Francis River
West Branch St. Francis River

Snake River

Clearwater River
Sauk River
Ashley Creek

Watab River
North Fork Watab River
South Fork Watab River

Platte River
Skunk River

Two River
North Two River
South Two River

Little Two River
Swan River
Little Swan River
Spring Branch Swan River

Little Elk River
South Branch Little Elk River

Nokasippi River
Little Nokasippi River

Crow Wing River
Gull River
Long Prairie River
Partridge River
Little Partridge River

Leaf River
Redeye River
Wing River

Cat River
Shell River
Fish Hook River
Straight River
Portage River
Potato River

Blueberry River
Kettle River

Rabbit River
Pine River
Little Pine River
South Fork Pine River

Little Willow River
Ripple River
Rice River
Willow River
Hill River
Little Hill River

Moose River
North Fork Willow River
South Fork Willow River

Sandy River
Prairie River
Tamarack River
Little Tamarack River

West Savanna River
Swan River
Prairie River
West Fork Prairie River

Vermilion River
Deer River
Ball Club River
Leech Lake River
Bear River
Leech Lake
Boy River
Swift River

Shingobee River
Kabekona River
Necktie River

Steamboat River

First River
Pigeon River
Third River
Turtle River
North Turtle River
Gull River

Schoolcraft River
Little Mississippi River


=== Hudson Bay Drainage Basin ===
Nelson River (Manitoba)
Lake Winnipeg (Manitoba)
Winnipeg River (Manitoba)
Lake of the Woods

Red River of the North


==== Red River of the North Drainage Basin ====
Red River of the North
Roseau River
Lost River
South Fork Roseau River

Joe River
Two Rivers
North Branch Two Rivers
Little Joe River

South Branch Two Rivers
Middle Branch Two Rivers

Tamarac River
Snake River
Middle River
South Branch Snake River

Grand Marais Creek
Red Lake River
Gentilly River
Black River
Little Black River

Clearwater River
Lost River
Poplar River
Hill River

Thief River
Lost River
Mud River
Moose River

Red Lake
Sandy River
Mud River
Blackduck River
North Cormorant River
South Cormorant River

Battle River
North Branch Battle River
South Branch Battle River

Tamarac River
Little Tamarac River
Lost River

Sand Hill River
Marsh River
Wild Rice River
South Branch Wild Rice River
White Earth River

Buffalo River
South Branch Buffalo River

Otter Tail River
Pelican River
Dead River
Toad River
Egg River

Bois de Sioux River
Rabbit River
South Fork Rabbit River

Mustinka River
Twelvemile Creek
West Branch Twelvemile Creek
West Fork Twelvemile Creek
East Fork Twelvemile Creek


==== Lake of the Woods Drainage Basin ====
Lake of the Woods
Warroad River
East Branch Warroad River
West Branch Warroad River

Rainy River
Winter Road River
Baudette River
West Fork Baudette River

Rapid River
East Fork Rapid River
Barton Brook
Wing River

North Branch Rapid River

Black River
West Fork Black River
South Fork Black River

Big Fork River
Bear River
Sturgeon River
Rice River
Popple River
Dunbar River

Bowstring River
Turtle River

Little Fork River
Beaver Brook
Cross River
Deer River

Ester Brook
Nett Lake River
Lost River

Rapid River
Valley River
Willow River
Sturgeon River
Bear River
Bearskin River

Dark River
East Branch Sturgeon River
Shannon River

Rice River

Rainy Lake/Kabetogama Lake/Namakan Lake/Crane Lake
Rat Root River
East Branch Rat Root River

Ash River
Black Duck River

Moose River
Johnson River
Vermilion River
Pelican River
Elbow River

Vermilion Lake
Armstrong River
East Two River
West Two River
Pike River
Sand River

Echo River
Hunting Shack River

Loon River
Little Indian Sioux River
Little Pony River
Korb River

Namakan River (Ontario)
Lac la Croix/Crooked Lake
Hustler River
Boulder River
Dahlgren River
Stuart River

Nina Moose River
Oyster River
Moose River
Portage River

Bottle River
Beartrap River
Basswood River
Horse River
Basswood Lake/Fall Lake
Range River
Shagawa River
Burntside River
Dead River

Bear Island River
Beaver River

Kawishiwi River
South Kawishiwi River
Birch Lake
Keeley Creek
Birch River

Isabella River
Little Isabella River
Island River
Dumbbell River

Perent River

Snake River
Stony River
Sand River
Greenwood River

Dunka River

Little Saganaga Lake/Gillis Lake
Frost River
Chub River

Louse River
Phoebe River

Knife River
Knife Lake

Maligne River (Ontario)
Saganaga Lake
Sea Gull River
Granite River
Pine River
Cross River
Tucker River
Long Island River


== See also ==
List of lakes in Minnesota
List of rivers in the United States


== References ==
Minnesota Watersheds
USGS Geographic Names Information Service
USGS Hydrologic Unit Map – State of Minnesota (1974)


== External links ==
Minnesota Streamflow Data from the USGS