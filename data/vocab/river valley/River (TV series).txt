River (stylized as RIVER) is a British drama television series that is set to first broadcast on BBC One in 2015. The six-part series was created and written by Abi Morgan.


== Cast ==
Stellan Skarsgård as John River, a police officer.
Nicola Walker as Detective Sergeant Jackie "Steve" Stevenson
Adeel Akhtar as Detective Sergeant Ira King
Lesley Manville as Chief Inspector Chrissie Read


== Production ==
The series was commissioned by Charlotte Moore and Ben Stephenson. The executive producers are Jane Featherstone, Manda Levin, Abi Morgan and Lucy Richer. Filming began in London in October 2014. The series was made by Kudos and will be distributed globally by Shine International.


== References ==