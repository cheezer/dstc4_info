Mainstream Rock is a music chart in Billboard magazine which ranks the most-played songs on mainstream rock radio stations, a category that combines the formats of active rock and heritage rock.


== History ==
The Rock Albums & Top Tracks charts were introduced in the March 21, 1981, issue of Billboard. The 50- and 60-position charts ranked airplay on album rock radio stations. Because album-oriented rock stations focused on playing tracks from albums rather than specifically released singles, these charts were designed to measure the airplay of any and all tracks from an album. Rock Albums combined airplay from all tracks from a particular album to come up with the top albums, while Top Tracks listed the top individual songs being played on rock radio. Mike Harrison of Billboard explained that when major artists release albums, more than one song from the album can become popular at the same time. The first number-one song on the Top Tracks chart was "I Can't Stand It" by Eric Clapton.
In September 1984, the two charts were merged and renamed Top Rock Tracks. It reduced from a 60-song tally to 50 songs on October 20, 1984, following a major revamp to the magazine. Coinciding with an increase in its reporting panel of album rock stations, the name of the chart was changed again with the issue dated April 12, 1986, to Album Rock Tracks. In November 1991, instead of reporting panels, Billboard changed its methodology of measuring airplay by using monitored airplay as provided by Nielsen Broadcast Data Systems to compile many of its charts. As a result, this data showed that many songs could spend months to over a year on the Album Rock Tracks chart. Billboard decided to drop to a 40-position chart (still its current format), and songs that fell out of the top 20 and after spending 20 weeks on the chart were moved to a new 10-position recurrent chart. The recurrent chart was scrapped two years later, but not the methodology.
Acknowledging the trend that rock radio was moving away from being an album-based format to a song-focused one, Billboard changed the name of the chart to Mainstream Rock Tracks beginning with issue dated April 13, 1996. The Mainstream Rock Tracks chart did not appear in the print edition of Billboard from its issued dated August 2, 2003, being accessible only through the magazine's subscription-based website, Billboard.biz. In late 2013, the chart was reintroduced to its primary website and magazine.
When R&R ceased publication in June 2009, Billboard incorporated its rock charts, Active Rock and Heritage Rock into its own publication. The radio station reporters of the two charts combine to make up the Mainstream Rock chart. Active rock stations concentrate on current hits over classic rock standards while heritage rock stations put a greater emphasis on classic rock with a few newer tracks mixed in. The individual Active Rock and Heritage Rock components were discontinued by Billboard at the end of November 2013 due to a lack of difference between the two charts.


== Long-running No. 1s ==
Shown below are the songs which have spent ten or more weeks at number one on this chart.
21 weeks
"Loser" by 3 Doors Down (2000–01)
20 weeks
"It's Been Awhile" by Staind (2001)
17 weeks
"Higher" by Creed (1999–2000)
"When I'm Gone" by 3 Doors Down (2002–03)
16 weeks
"Touch, Peel and Stand" by Days of the New (1997)
15 weeks
"Interstate Love Song" by Stone Temple Pilots (1994)
"Heavy" by Collective Soul (1999)
14 weeks
"So Far Away" by Staind (2003)
"Boulevard of Broken Dreams" by Green Day (2005)
"Fake It" by Seether (2007–08)
"Inside the Fire" by Disturbed (2008)
13 weeks
"Start Me Up" by The Rolling Stones (1981)
"How You Remind Me" by Nickelback (2001)
"Figured You Out" by Nickelback (2004)
"Pain" by Three Days Grace (2006–07)
"Chalk Outline" by Three Days Grace (2012)
"Something from Nothing" by Foo Fighters (2014–15)
12 weeks
"Mysterious Ways" by U2 (1991–92)
"Like a Stone" by Audioslave (2003)
"Save Me" by Shinedown (2005–06)
"Dani California" by Red Hot Chili Peppers (2006)
"Face to the Floor" by Chevelle (2011–12)
"Bully" by Shinedown (2012)
11 weeks
"Remedy" by The Black Crowes (1992)
"Turn the Page" by Metallica (1999)
"Fall to Pieces" by Velvet Revolver (2004)
"Break" by Three Days Grace (2009–10)
"Hail to the King" by Avenged Sevenfold (2013)
10 weeks
"Lightning Crashes" by Live (1995)
"The Down Town" by Days of the New (1998)
"Scar Tissue" by Red Hot Chili Peppers (1999)
"Blurry" by Puddle of Mudd (2002)
"Second Chance" by Shinedown (2008–09)
"Country Song" by Seether (2011)


== Achievements and records ==
Van Halen holds the record for the most tracks to hit number one on the Mainstream Rock chart, with thirteen. John Mellencamp holds the record for the most tracks by a solo artist to hit number one on the chart, with seven.
Three Days Grace holds the record for most cumulative weeks at number one, with 70. They also hold the record for most consecutive years with a number one single with seven (2009-2015).
Only twice have two consecutive number-one songs been by the same artist. In 1992, the Black Crowes hit number one with "Remedy", followed by "Sting Me". In 1994, the Stone Temple Pilots' "Vasoline" hit number one and was replaced by "Interstate Love Song".
Shinedown is the only band in history to have all of its singles place in the top 5 on the Mainstream Rock chart, doing so with its first 19 singles.


== References ==


== External links ==
Current Billboard Mainstream Rock chart