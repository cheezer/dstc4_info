Roti prata is a fried flour-based pancake that is cooked over a flat grill. It is usually served with a vegetable- or meat-based curry and is from Malaysia and Singapore. Prata is also commonly cooked upon request with cheese, onion, banana, red bean, chocolate, mushroom or egg. It is listed at number 45 in the World's 50 most delicious foods readers' poll compiled by CNN Go in 2011.


== Preparation and servingEdit ==
Roti prata is prepared by flipping the dough into a large thin layer before folding the outside edges inwards. The dough is cooked on a flat round iron pan measuring about three feet in diameter. Other ingredients such as cooked mutton, onion and egg, cheese, banana, as well as instant noodles, if desired, are added into the dough during the cooking process, which lasts two to three minutes. Prata is commonly served with a fish- or chicken-based curry. Some people also eat it with mutton curry or dhaal. Some people prefer to eat it with sugar.


== HistoryEdit ==
The prata has always been known as parotta in the Indian subcontinent, while it is usually known as roti canai in other countries such as Malaysia.


== See alsoEdit ==


== ReferencesEdit ==


== External linksEdit ==
Roti Prata Video Recipe
Roti Prata, Singapore Infopedia, National Library Board, Singapore