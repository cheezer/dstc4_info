Roti is an Indian Subcontinent flat bread, made from stoneground wholemeal flour, traditionally known as atta flour, that originated and is consumed in India, Pakistan, Nepal, Sri Lanka and Bangladesh. It is also consumed in parts of South Africa, the southern Caribbean, particularly in Trinidad and Tobago, Guyana, and Suriname, and Fiji. Its defining characteristic is that it is unleavened. Indian naan bread, by contrast, is a yeast-leavened bread. A kulcha in Indian cuisine is a bread-like accompaniment, made of processed flour (maida) leavened with yeast.
Various types of roti are integral to South Asian cuisine.


== Etymology ==

The word roti is derived from the Sanskrit word रोटिका (roṭikā), meaning "bread". Names in other languages are Hindi: रोटी; Assamese: ৰুটী; Nepali : रोटी; Bengali: রুটি; Sinhalese: රොටි; Gujarati: રોટલી; Marathi: पोळी; Odia: ରୁଟି; Malayalam: റൊട്ടി; Kannada: ರೊಟ್ಟಿ; Telugu: రొట్టి; Tamil: ரொட்டி; Urdu: روٹی‎; Dhivehi: ރޮށި; Punjabi: ਰੋਟੀ,ਫੂਲਕਾ; Thai: โรตี, Indonesian: Indonesia. It is also known as maani in Sindhi and phulka in Punjabi and Saraiki.


== South Asia ==

Many different variations of flat breads are found in many cultures across the globe, from South Asia to the Americas. The traditional flat bread originating from South Asia is known as roti, pronounced "RHO-tee". It is normally eaten with cooked vegetables or curries; it can be called a carrier for curries or cooked vegetables. It is made most often from wheat flour, cooked on a flat or slightly concave iron griddle called a tawa. Like breads around the world, roti is a staple accompaniment to other foods. In Iran, the two variants of this bread are called khaboos and lavash. These two breads (the former of which is almost exactly prepared like Indian roti) are quite similar to other South Asian rotis.

In Sri Lanka, probably the most popular type of roti is pol roti (coconut roti), made of wheat flour, kurakkan flour, or a mixture of both, and scraped coconut. Sometimes, chopped green chillies and onion are added to the mixture before cooking. These are usually thicker and harder than other roti types. They are usually eaten with curries, or some types of sambol or lunu miris and considered a main meal rather than a supplement.


== Southeast Asia ==

In Indonesia and Malaysia the term encompasses all forms of bread, including western-style bread, as well as the traditional Indian breads.
In Thailand, "โรตี" refers to the maida paratha—known in Indonesia as roti maryam, roti cane or roti konde, Malaysia as roti canai and in Singapore as roti prata—which is sometimes drizzled with condensed milk, rolled up, and eaten as a hot snack, or fried with egg as a larger dish.


== West Indies ==
Roti is eaten widely across in the West Indies, especially in countries with large Indo-Caribbean populations such as Trinidad and Tobago. Originally brought to the islands by indentured laborers from South Asia, roti has become a popular staple in the culturally rich cuisines of Trinidad and Tobago, Guyana and Jamaica. In the West Indies, roti is commonly eaten as an accompaniment to various curries and stews. The traditional way of eating roti, is to break the roti by hand, using it to sop up sauce and pieces of meat from the curry. However, in the West Indies, the term roti may refer to both the flat-bread(roti) its self as well as the more popular street food item, in which the roti is folded around a savory filling in the form of a wrap.
The "roti wrap" is the commercialization of roti and curry together as a fast-food or street-food item in the Caribbean. This wrap form of roti originated in Southern Trinidad. It was first created in the mid-1940s by Sackina Karamath, who later founded Hummingbird Roti Shop in San Fernando, Trinidad. The wrap was convenient as the meal could be eaten faster and while on the go, as well as keeping one's hands from getting dirty. In Trinidad and Tobago, various wrapped roti are served, including chicken, conch, goat, beef and shrimp. Vegetables can also be added including potato, pumpkin and spinach as well a variety of local condiments; pepper sauce(hot sauce) and mango chutney being the most popular.
The roti wrap quickly gained popularity across the island and spread throughout the rest of the Caribbean. "Roti shops" are now abundant in Trinidad and Tobago and the wrapped roti a staple street food. The wrap is now simply referred to as a roti or just roti. As Caribbeans moved to North American cities such as Toronto, New York, and Montreal, they exported with them the wrapped version of roti. This iconic version is what most North Americans know as roti. The growth in popularity has recently lead to referring to the flat-bread its self (roti) that surrounds the filling as a "roti skin" or "roti shell". A practice that is now common in both restaurants and commercial companies.
Various types of roti are eaten throughout the West Indies. Roti is most prominently featured in the diets of people in Trinidad and Tobago, Guyana, and Suriname. West Indian style roti is primarily made from wheat flour, baking powder, salt, and water, and cooked on a tawa. Certain rotis are also made with butter.

Dotsi roti is a roti common in Guyana.
A small amount of fat is placed in each piece of dough before it is rolled out to make the roti softer. Usually vegetable oil is used, but butter, or margarine can also be used. Ghee is not used in everyday cooking but is used on special occasions, especially amongst Hindus. The roti is usually clapped by hand or beaten a bit, hot off the tava, so it softens but does not break.
A good roti in Guyana is one that is very soft, with layers (almost like pastry layers if possible), which remains whole.
The type of roti one gets is determined by what is placed in the dough before it is rolled out. Various types include dhalpuri, aloo (potato) roti, and even sugar (to keep the kids busy, while the mother finishes cooking).
In Guyana, a rolled-out thin flat dough like a roti that is deep-fried in ghee is called a puri, and is only made for Hindu religious gatherings. Therefore, a dhalpuri is not really a puri, as a puri and a roti are two different things.
Another item prepared like roti is bake or bakes or floats. A Guyanese or Trinidadian fry bake seems to be more similar to an Indian puri. A bake is made with butter or margarine and has a different ratio of flour to fat. It is made much quicker than roti and is usually made in the mornings. Dough is rolled out and cut into shapes or rolled into small rounds. Guyanese bakes are fried, but bakes from other parts of the West Indies can be baked in an oven. Bakes are usually paired with a quick fry-up for breakfast or dinner, stewed saltfish, or eggs ("western" style, with onions, tomatoes, green peppers). Bakes are also made in other parts of the West Indies including Trinidad, Barbados and St. Vincent. In Trinidad and Tobago, a "bake and shark" is a popular street food sandwich in which fried shark is sandwiched between two halves of a sliced bake with a plethora of local condiments. Pepper sauce, shado beni, garlic sauce, tamarind and mango chutney are most common as well as lettuce, tomato and cucumber for fillers.
In Suriname, roti refers mainly to dhalpuri or aloo puri. It is most often eaten with curried chicken. As in Trinidad and the West Indies, roti can also refer to the stuffed roti wrap. It is customary to eat this dish out of hand. Because of a mass emigration of Indian Surinamese in the 1970s, roti became a popular take-out dish in the Netherlands. It usually includes chicken curry, potatoes, a boiled egg and various vegetables, most notably the kousenband or yardlong bean. Another variation includes shrimp and aubergine. The meat with gravy, potatoes, egg and yardlong beans are served side by side on a plate, with the aloo puri folded in fours on top. One then has the option to spice the dish with a very hot chutney made of Madame Jeanette peppers.


=== Other dishes ===

Roti, pronounced "rooti" in Cape Town, was initially introduced to South Africa by Indian migrants during the 19th century and subsequently became incorporated into Cape Malay cuisine. It is widely eaten by the Indian and Cape Malay communities living in South Africa and is either eaten as a flat bread or a wrap with locally made curries.


== See also ==

Bhatura
Chapati
Kottu roti
Kulcha
Luchi
Naan
Paratha
Puri
Roti canai
Tortilla
Wrap roti
Jolada rotti


== References ==


== External links ==
Tandoori roti recipe