Safety is the condition of being protected against harmful conditions or events, or the control of hazards to reduce risk.
Safety may also refer to:
Safety (firearms), a device that inactivates the trigger
Safety (distributed computing), a class of guarantees in distributed computing
Safety (album), an album by Ty Tabor
Safety (EP), an EP by Coldplay
SAFETY, the U.S. Internet Stopping Adults Facilitating the Exploitation of Today's Youth Act of 2009
Safety Island, Antarctica


== Sports ==
Safety (American and Canadian football position), a type of defensive back
Safety (cue sports), an intentional defensive shot
Safety (gridiron football score), a type of scoring play


== See also ==
Category:Occupational safety and health