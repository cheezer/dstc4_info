In gridiron football, the safety (American football) or safety touch (Canadian football) is a scoring play which results in two points being awarded to the scoring team. Safeties can be scored in a number of ways, such as when a ballcarrier is tackled in his own end zone or when a foul is committed by the offense in their own end zone. After a safety is scored in American football, the ball is kicked off to the team that scored the safety from the 20-yard line; in Canadian football, the scoring team also has the options of taking control of the ball at their own 35-yard line or kicking the ball off themselves. The ability of the scoring team to receive the ball through a kickoff differs from the touchdown and field goal, which require the scoring team to kick the ball off to the scored upon team. Despite being of relatively low point value, safeties can have a significant impact on the result of games, and Brian Burke of Advanced NFL Stats estimated that safeties have a greater abstract value than field goals, despite being worth a point less, due to the field position and reclaimed possession gained off the safety kick.
Safeties are the least common method of scoring in American football, but are not rare occurrences – since 1932, a safety has occurred once every 14.31 games in the National Football League (NFL), or about once a week under current scheduling rules. A much rarer occurrence is the one-point safety, which can be scored by the offense on an extra point or two-point conversion attempt; those have occurred at least twice in NCAA Division I football since 1996, most recently at the 2013 Fiesta Bowl. No conversion safeties have occurred since at least 1940 in the NFL. A conversion safety could also be scored by the defense in a score in college football; although this has never occurred, it is the only possible way a team could finish with a single point in an American football game.


== Scoring a safety ==


=== American football ===
In American football, a safety is scored when any of the following conditions occur:
The ball carrier is tackled in his own end zone
The ball becomes dead in the end zone, with the exception of an incomplete forward pass, and the defending team is responsible for it being there
The offense commits a foul in its own end zone


=== Canadian football ===
In Canadian football, a safety touch is scored when any of the following conditions occur:
The ball becomes dead in the goal area of the team in possession of the ball
The ball touches or crosses the dead line or a sideline in goal after having been directed from the field of play into the Goal Area by the team scored against or as the direct result of a blocked scrimmage kick.


== Resuming play after a safety ==


=== American football ===
After a safety is scored, the ball is put into play by a free kick. The team that was scored upon must kick the ball from their own 20-yard line and can punt, drop kick, or place kick the ball. In professional play, a kicking tee cannot be used – however, a tee can be used in high school or college football. Once the ball has been kicked, it can be caught and advanced by any member of the receiving team, and it can be recovered by the kicking team if the ball travels at least 10 yards and bounces at least once or a player of the receiving team touches the ball.


=== Canadian football ===
After scoring a safety touch, the scoring team has the option of taking control of the ball and beginning play from their own 35-yard line, kicking the ball off from their 35-yard line, or accepting a kickoff from the 25-yard line of the team that conceded the score. If a kickoff is chosen it must be a place kick, and the ball can be held, placed on the ground, or placed on a tee prior to the kick. As in American football, the ball must go at least ten yards before it can be recovered by the kicking team.


== Elective safeties ==
In American football, intentionally conceded safeties are an uncommon strategy. Teams have utilized elective safeties to gain field position for a punt when pinned deep in their own territory and, when ahead near the end of a game, to run down the clock so as to deny the other team a chance to force a turnover or return a punt. Teams have also taken intentional safeties by kicking a loose ball out the back of their end zone, with the intent of preventing the defense from scoring a touchdown.
Elective safeties are more common in Canadian football, where they can result in better field position than a punt. The 2010 Edmonton Eskimos surrendered a Canadian Football League (CFL)-record 14 safeties, a factor that led CFL reporter Jim Mullin to suggest increasing the value of the safety touch from two to three points as a deterrent.


== Conversion safety ==
In American football, if what would normally be a safety is scored on an extra point or two-point conversion attempt (officially known in the rulebooks as a try), one point is awarded to the scoring team. This is commonly known as a conversion safety or one-point safety and it can be scored by the offense. There are at least two known occurrences of the conversion safety in Division I college football – a November 26, 2004 game in which Texas scored against Texas A&M, and the 2013 Fiesta Bowl in which Oregon scored against Kansas State. In both games the PAT kick was blocked, recovered by the defense, and then fumbled or thrown back into the end zone. Coincidentally, play-by-play commentator Brad Nessler called both of these games. No conversion safeties have been scored in the NFL since 1940, although it is now slightly more likely after the rule change in 2015 which allowed the defense to take possession and score on a conversion attempt. Before 2015, the only scenario in which a one-point safety could have been scored in the NFL would have involved the defense kicking or batting a loose ball out the back of the end zone without taking possession of it. After the 2015 rule change, a one-point safety can also be also be scored after the defense takes possession and fumbles out of their own end zone or is tackled in it after leaving it, as in the NCAA.
In college football and the NFL, a conversion safety could also be scored by the defense. To accomplish this, the kicking team would have to retreat all the way back to their own end zone and then fumble the ball out of it or be tackled in it. While such a conversion safety has never been scored by the defense, it is the only possible way in which a team could finish with a single point in an American football game.


== See also ==
List of safety records
Touchback
Own goal


== Notes ==
Notes

Footnotes


== References ==