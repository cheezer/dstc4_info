Officer Cadet School (OCS) is one of the several training establishments within the SAFTI Military Institute camp complex. Its role is to train officers of all three services of the Singapore Armed Forces. It is run independently from the other schools in the complex, but is ultimately overseen by the Commandant of SAFTI MI.


== Officer Cadet ==
Officer Cadet School is the only commissioning route in the Singapore Armed Forces. Outstanding graduates from Basic Military Training and the Specialist Cadet School (Specialists are NCO-equivalents in the SAF) are sent to OCS to train to be officers. Regular Specialists may also attend OCS on application, recommendation, and selection. Trainees are known as Officer Cadets for Army and Air Force servicemen, and Midshipmen for Naval servicemen respectively.
There are a total of 12 wings in OCS inclusive of tri-service wings. They are: Alpha, Bravo, Charlie, Delta, Echo, Foxtrot, Golf, Hotel, Sierra, Tango, Mids and Air. Mids and Air Wings are for the Navy and Air Force Officer Cadets respectively.


== Course ==
The Officer Cadet Course (apart from the Medical Officer Cadet Course) used to last 42 weeks but this was reduced to 38 weeks after a revamp of the National Service system. The course is divided into three terms: Service, Professional, and Joint term.
Common Leadership Module
The Service Term starts with the Common Leadership module in the first two weeks of the Officer Cadet Course. This is also the Initiation period where the Officer Cadets are granted some latitude in terms of discipline and regimentation to facilitate adjustment. In this Module, the newly inducted officer cadets are introduced to the SAF Leadership Framework and the SAF Core Values before they are streamed to the army, navy, or air force subsequently. One of the aims of this module is getting the new Officer Cadets used to the new environment. Subsequently, Officer Cadets are streamed into their services and receive training specific to their service. The end of this module is marked by a solemn initiation ceremony in SAFTI MI's ceremonial hall, to officially mark the new officer cadet's formal entry to Officer Cadet School.
Service term
Officer Cadets who remain in the Army Wings (company-equivalents) undergo platoon and section-level infantry training for their Service term, from weeks 3 to 14. At the end of Service term, the Officer Cadets may indicate their preferred vocation: Infantry, Armour, Artillery, Combat Engineers, Logistics, or Signals. The Officer Cadets are sent to the various vocations based on their personal preference, as well as other factors such as performance in the course, security clearance, etc.
Professional term
For the professional term, weeks 15 to 35, Officer Cadets in the Infantry remain in SAFTI MI, while those belonging to the other arms will depart for their respective schools (e.g. Engineer Training Institute, Signals Institute) to receive training specific to their arm of service. Before reporting to their respective schools, Officer Cadets in the support arms are sent for SOCJOT (Support arms Officer Cadet Jungle Orientation Training) in Brunei.
Infantry Officer Cadets will go on two overseas training trips during their stay in OCS. Usually, they will go to either Taiwan or Thailand for general infantry training, and Brunei for training in jungle terrain for the Jungle Confidence Course.
Officer Cadets in the Navy and Air Force Wings continue to receive training within SAFTI MI, before going for further training at sea for the Midshipmen, and Air Force Training Command for Air Force Cadets.
Joint term
On completion of their military training, all the Officer Cadets will return to SAFTI MI for their Commissioning Parade in the Joint term from weeks 36 to 38.
The first week of the Joint term mainly involves visits to all services and getting to know more on how the three services work with each other. Rehearsal for the commissioning parade will commence as early as two weeks before the date itself, it will be conducted by the School Sergeant Major as well as with the assistance of other Wing Sergeant Majors. Either the President, or a representative (a cabinet minister) will officially commission them as Second Lieutenants in the Singapore Armed Forces.


== Appointments and rank insignia ==
Like officers and warrant officers, Officer Cadets wear their rank insignia on chest epaulets and shoulderboards. The insignia consist of one, two, or three white bars, denoting their seniority (Freshmen, Junior Bar, and Senior Bar cadets respectively). Senior cadets also have the privilege of taking on school-level appointments in deference to their seniority and course performance during their service term. The graduating class of Officer Cadets wear peaked caps instead of berets to indicate their impending commissioning.
All Officer Cadets will have the chance to take on exercise and admin appointments to hone their various leadership skills. Exercise roles involve selected cadets serving in command positions during training exercises, whereas administrative roles involve selected cadets being in charge of other cadets as well as seeing to their administrative needs when the cadets are not on exercise. Their performance during these exercise and admin appointments are evaluated as part of their course evaluation criteria. Each Wing's respective Wing Sergeants Major will conduct a change of command parade whenever there is a change in appointment holders.
Admin rank insignia
Additional loops and whorls denote appointment holders such as Cadet Platoon Commander, etc. These ranks, known affectionately as "fishes" are considered "admin ranks", as compared with "exercise ranks".
Exercise rank insignia
During field exercises, appointment holders (e.g. exercise platoon commander, platoon sergeant, etc.) wear orange-colored exercise rank insignia similar to the rank insignia of a substantive appointment holder. For instance, the exercise platoon commander will wear orange lieutenant's bars on his chest epaulet, while the exercise Platoon Sergeant will wear a chest epaulet with First Sergeant's chevrons.


== See also ==
National Service in Singapore
Specialist Cadet School
Basic Military Training Centre


== External links ==
Official site