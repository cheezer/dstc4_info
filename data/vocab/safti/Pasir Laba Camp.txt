Pasir Laba Camp (Abbreviation: PLC) is an installation of the Singapore Armed Forces. It is composed primarily of Army tenant units, apart from tri-service SAF Warrant Officer School. It is considered the home of the Warrant Officer and Specialist Corps, just as SAFTI Military Institute is the home of the Officer Corps.


== Location ==
Pasir Laba Camp is located at the intersection of Pasir Laba and Upper Jurong Roads. By virtue of their locations, Pasir Laba and Sungei Gedong Camps both control the entrances to the SAFTI Live Firing Area, which is restricted from civilian entry.


== History ==
SAF Training Institute (SAFTI) was constructed at the end of Pasir Laba Road since 1961, with preparations to construct the area. It used the temporary site (Pasir Laba Road starting), because it was able to control the SAFTI live firing area. SAFTI at Pasir Laba Road was opened on 18 June 1966. The SAFTI Live Firing Area was drawn out in 1967 but there are 504 signboards in the vicinity (the signboards do indicate 'Danger. Live Firing Area-KEEP CLEAR'). Malay villagers were found dead.
In 1980, several tenant units including School of Infantry Specialists and School of Infantry Weapons were located at the site together with HQ Infantry (now 9DIV/Inf). Officer Cadet School was given the sole right to the name "SAFTI", while the other units were housed in what was known as Pasir Laba Camp.
However, this left the men of Pasir Laba Camp feeling like second class citizens. As a result, Pasir Laba Camp renamed "SAFTI" once again on 31 May 1986. This lasted until Officer Cadet School and various other officer-training institutions were moved to a new complex, SAFTI Military Institute, along Upper Jurong Road on June 1995. It was renamed to Pasir Laba Camp back again.


== Tenant units ==
The anchor tenant of Pasir Laba Camp today is Headquarters, Training and Doctrine Command (HQ TRADOC). Notable is a company of aggressors who serve as enemy simulators for infantry battalions' performance evaluation.
Other tenant units are the Specialist Cadet School and SAF Warrant Officer School, the key institutes training the Warrant Officer and Specialist Corps. Other training schools present include School of Military Intelligence and School of Infantry Weapons.
Training Resource Management Centre, which controls and maintains the SAFTI Live Firing Area in addition to other functions, is conveniently located here as well.
A new Multi Mission Range Complex (MMRC) has been opened on 6 October 2013 to house the marksmanship of soldiers.


== Support units ==
Camp Commandant's Office (CCO) provides additional vehicular and logistic support to the tenant units on top of their organic inventory. A medical center under command of CCO provides medical support to all tenant units during office hours, and to SAFTI Military Institute and Jurong Camp after office hours, when their own medical centers are closed. It also provides 24-hour support to units training in the southern part of the SAFTI live firing area, namely the Peng Kang Hill, FOFO Hill, Bunker Hill, Elephant Hill and Good Morning Hill/Chua Chu Kang Hill. It also included the Matador Range and M210 Range.
The rest that are not listed are all under Sungei Gedong Camp (Area A, Area B, Area C, Area D, Armour, FIBUA Controls).


== External links ==