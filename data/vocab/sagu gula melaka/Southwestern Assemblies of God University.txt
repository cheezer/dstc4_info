Southwestern Assemblies of God University (SAGU) is a private Christian university located in Waxahachie in Ellis County in the Dallas-Fort Worth metroplex, USA. SAGU is regionally accredited with the Commission on Colleges of the Southern Association of Colleges and Schools and officially endorsed by the Assemblies of God USA. It is the only Assemblies of God university located in Texas. The university offers associates, bachelor's and master's degrees in liberal arts and Bible and church ministries.


== History ==
Southwestern Assemblies of God University began life as three separate Bible schools. The first, known as Southwestern Bible School, was established in 1927 nat Enid, Oklahoma, under the leadership of the Reverend P. C. Nelson. The second, Shield of Faith Bible Institute, was founded in Amarillo Texas, in 1931 under the direction of the Reverend Guy Shields. It included not only a Bible school, but also a grade school and a high school. The third, which was operated as Southern Bible College in connection with the Richey Evangelistic Temple, began in 1931 at Goose Creek, Texas (now Baytown), in 1931. It was started by Reverend J. T. Little in Trinity Tabernacle and moved to Houston in 1933. The school's name was then changed to Southern Bible Institute.
The Bible school division of Shield of Faith Bible Institute was moved to Fort Worth in 1935. The high school division was transferred the following year. In 1940, a merger resulted in Southern Bible Institute, moving to Fort Worth. The combined school, operating as South Central Bible Institute, came under the ownership and direction of the Texas District Council of the Assemblies of God.
The school in Enid merged with South Central in 1941, at which time the name was changed to Southwestern Bible Institute. In 1943, the institute was moved to its present facilities in Waxahachie, Texas.
During the 1944–45 term, a junior college curriculum was added to the school's program. The Junior College Division soon accounted for about half of the enrollment in the College.
Southwestern Bible Institute became a regional school in 1954. At that time seven districts of the Assemblies of God—Arkansas, Louisiana, New Mexico, North Texas, Oklahoma, South Texas and West Texas—owned and operated the school. In 1969, the Rocky Mountain District, composed of Colorado and Utah, was admitted to the Region. The Mississippi District was then added to the Region in 1979. In 1980, the Rocky Mountain District voted to withdraw from the Southwestern Region and to remain neutral.
The proposal to change the name of Southwestern was ratified by all seven Districts, and the name became Southwestern Assemblies of God College. In 1963, the upper two years of the College were renamed Southwestern College of the Bible. In 1968, the separation of the divisions of the College was made more complete, and the Junior College was designated Southwestern Junior College of the Assemblies of God. In 1987 the two divisions were reunited.
Beginning in the early nineties, Southwestern experienced phenomenal enrollment increases. From 596 students in the fall of 1991, enrollment grew to 1492 students in 1997. Along with the enrollment increase, opportunities to expand the curriculum and programs developed. In December 1994, the Board of Regents unanimously approved the name change to Southwestern Assemblies of God University to more accurately reflect its purpose and mission as a Bible university of theological and professional studies.
In 1996, SAGU expanded to include a graduate school. Approximately nineteen graduate programs are available through SAGU’s Harrison School of Graduate Studies.
In 2004, the academic divisions of the university realigned into two colleges, the College of Bible & Church Ministries and the College of Arts & Professions. Both colleges maintain Bible-based curriculum and strive to fulfill the mission of SAGU.
Since 2000, SAGU has added 24 new academic programs, bringing the total to more than 60 programs. Additionally, under the direction of President Kermit Bridges, the campus has continued to grow. In 2006–2007, Teeter and Bridges Halls were added. They were followed by the new Alton Garrison Student Wellness Center in 2009.
Amidst the physical expansion, SAGU experienced consecutive record enrollments in Fall 2007, Fall 2008, Fall 2009, and Fall 2010 reaching a milestone of 2,064.


== Mascot – Judah the Lion ==
Influenced by Vice President George Brazell's acquisition of a four-month-old lion cub named Judah, Southwestern adopted the "Lion of Judah" as its mascot in 1963.


== Music ==
In Spring 2011, it was announced the SAGU had attained all-Steinway status for its exclusive use of Steinway pianos on campus, a distinction limited to a select few universities.
In 1950, then Southwestern Bible Institute, expelled Jerry Lee Lewis for his brand of gospel piano.


== Athletics ==
SAGU teams, nicknamed athletically as the Lions, are part of the National Association of Intercollegiate Athletics (NAIA), primarily competing in the Red River Athletic Conference (RRAC), while its football team competes in the Central States Football League (CSFL). The Lions also compete as a member of the National Christian College Athletic Association (NCCAA) and holds their home games at Lumpkins Stadium a local high school football stadium of Waxahachie High School. Men's sports include baseball, basketball, football and soccer; while women's sports include basketball, soccer, softball and volleyball.
In September 2012, SAGU announced they will move from the RRAC to the Sooner Athletic Conference in the 2013-2014 season.
There is a cheerleading squad. The school colors are purple and gold.


=== Basketball ===
Retired numbers: 34 – Monte Warcup, who was the first inductee to the SAGU Athletic Hall of Fame.
The 2012-2013 season made 2013 a record-setting year for SAGU Lions Basketball, with the Lions achieving the NAIA second-place championship ranking.


== Notable alumni ==
Gary Elkins, Republican member of the Texas House of Representatives since 1995 from his native Houston, Texas
Mike Evans, author, journalist, middle east commentator.
Tommy Barnett, pastor, First Assembly of God, Phoenix, AZ., largest Assemblies of God Church in the United States.


== References ==


== External links ==
Official website
Official athletics website
Distance Education & Online Learning Admissions website
Harrison School of Graduate Studies Admissions website
SAGUtv Streaming website
AG Colleges & Universities
SAGU YouTube Channel
Programs, tuition, and admissions at Southwestern Assemblies of God University