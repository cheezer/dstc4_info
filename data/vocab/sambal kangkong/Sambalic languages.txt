The Sambalic languages are a part of the Central Luzon language family spoken by the Sambals, an ethnolinguistic group on the western coastal areas of Central Luzon and the Zambales mountain ranges. The largest Sambalic languages are Sambal, Bolinao, and Botolan with approximately 200,000, 105,000 and 72,000 speakers respectively based on the 2007 population statistics from the National Statistical Coordination Board (NSCB). These figures are the combined population of the municipalities where the language is spoken.
For the Sambali or Sambal ethnolinguistic subgrouping, the estimated number of speakers is based on the total population of Santa Cruz, Candelaria, Masinloc, Palauig, and Iba municipalities of Zambales. For the Sambal Bolinao subgrouping, a projected number of speakers is taken from the combined populations of Anda and Bolinao municipalities of Pangasinan. The Sambal Botolan subgroup, on the other hand, takes the aggregated population of Botolan and Cabangan municipalities. The rest are smaller languages spoken almost exclusively within various Aeta communities. In total, there are approximately 390,000 speakers of Sambalic languages. Speakers can also be found in other towns of Zambales not mentioned above: Olongapo City, Bataan, Tarlac, and Metro Manila.
An estimated 6000 speakers can also be found in Panitian, Quezon, Palawan and Puerto Princesa City. The language is also spoken by many Filipino immigrants in the U.S. and Canada. In Dartmouth, Nova Scotia, Canada, for instance, the language is spoken by a clan of Zambals. In Casino Nova Scotia in the Maritimes city of Halifax, a group of Sambals can be found running the card games. Community organizations of Sambal-speaking Filipino-Americans are found in San Diego and San Francisco, California as well as in Hawaii.
The Sambalic languages are most closely related to Kapampangan and to an archaic form of Tagalog still spoken in Tanay in the province of Rizal. This has been interpreted to mean that Sambal-speakers had once inhabited that area, later being displaced by migrating Tagalog-speakers, pushing the original inhabitants northward to what is now the province of Zambales, in turn, displacing the Aetas. There is also a possible relationship between these Sambalic language speakers and the population of the island provinces of Marinduque and Romblon based on commonalities in some traditions and practices.


== Speakers ==
Sambal (Spanish: Zambal) is the common collective name for all Sambalic languages speakers. It is also the term referring to the Sambalic language subgrouping in northern municipalities of Zambales, which comprises the majority of Sambals or more than 50 percent (200,000) of all Sambalic languages speakers (390,000). Sambal may also refer to the inhabitants of Zambales as a whole and the residents of Bolinao and Anda in Pangasinan.


== Sample text ==
Below are translations in Sambal, Bolinao, and Botolan of the Philippine national proverb “He who does not acknowledge his beginnings will not reach his destination,” followed by the original in Tagalog.


== See also ==
Languages of the Philippines
Sambal people


== References ==


=== Citations ===


=== Bibliography ===


== External links ==