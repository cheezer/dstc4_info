The sambal is a folk membranophone instrument from Western India. It consists of two wooden drums united from a side, with skin heads stretched on their top mouths. One drum is higher in pitch than the other one. This instrument is played with two wooden sticks, one beater having a circular tip. The sambal is also a traditional drum of the Gondhali people and the konkani people.
Sambal is a traditional instrument used by the peoples who are servants of goddess Mahalaxmi Devi and used in the gondhal pooja.


== See also ==
 Percussion portal


== References ==


== External links ==
YouTube video
Photo