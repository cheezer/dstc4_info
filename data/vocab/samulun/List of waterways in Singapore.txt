This is a List of waterways and other maritime features in Singapore:


== Basins ==
Benoi Basin
Gul Basin
Jurong Basin
Kallang Basin
Northern Tuas Basin
Southern Tuas Basin
Store Basin
Telok Ayer Basin


== Bays ==
Cruise Bay
Marina Bay


== Channels ==
Fairburn Channel
Keppel Channel
Ketam Channel
Nenas Channel
Sebarok Channel


== Docks ==
Dock Number 1
Dock Number 2
King George VI Dock
King's Dock
Queen's Dock


== Waterways ==
East Jurong Fairway
Jong Fairway
Sinki Fairway
Sisters Fairway
Temasek Fairway
West Jurong Fairway
West Keppel Fairway


== Harbours ==
Keppel Harbour
Serangoon Harbour


== Lakes ==
See Lakes of Singapore


== Reservoirs ==
See Reservoirs in Singapore


== Rivers ==
See List of rivers in Singapore


== Seas ==
South China Sea


== Straits ==
Selat Ayer Merbau
Selat Banyan
Selat Berkas
Selat Biola
Selat Bukom
Selat Pandan
Selat Pauh
Selat Pawai
Selat Pesek
Selat Pulau Damar
Selat Johor (Straits of Johor)
Selat Jurong
Selat Sakra
Selat Salu
Selat Samulun
Selat Sengkir
Selat Sinki
Selat Sudong
Selat Tanjong Hakim
Straits of Singapore (Selat Singapura)


== References ==