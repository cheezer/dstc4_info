The Reuben sandwich is a hot sandwich composed of corned beef, Swiss cheese, sauerkraut, and Russian dressing, grilled between slices of rye bread. Several variants exist.


== Possible origins ==


=== Reuben Kulakofsky: Omaha, Nebraska ===
One account holds that Reuben Kulakofsky (sometimes spelled Reubin, or the last name shortened to Kay), a Lithuanian-born grocer residing in Omaha, Nebraska, was the inventor perhaps as part of a group effort by members of Kulakofsky's weekly poker game held in the Blackstone Hotel from around 1920 through 1935. The participants, who nicknamed themselves "the committee", included the hotel's owner, Charles Schimmel. The sandwich first gained local fame when Schimmel put it on the Blackstone's lunch menu, and its fame spread when a former employee of the hotel won a national contest with the recipe. In Omaha, March 14 was proclaimed as Reuben Sandwich Day.


=== Reuben's Delicatessen: New York City ===
Another account holds that the Reuben's creator was Arnold Reuben, the German owner of the famed yet defunct Reuben's Delicatessen in New York City who according to an interview with Craig Claiborne invented the "Reuben special" around 1914. The earliest references in print to the sandwich are New York–based but that is not conclusive evidence, though the fact that the earliest, from a 1926 edition of Theatre Magazine, references a "Reuben special", does seem to take its cue from Arnold Reuben's menu.
A variation of the above account is related by Bernard Sobel in his book, Broadway Heartbeat: Memoirs of a Press Agent, which claims that the sandwich was an extemporaneous creation for Marjorie Rambeau inaugurated when the famed Broadway actress visited the Reuben's Delicatessen one night when the cupboards were particularly bare.
Some sources name the actress in the above account as Annette Seelos, not Marjorie Rambeau, while noting that the original "Reuben special" sandwich did not contain corned beef or sauerkraut and was not grilled; still other versions give credit to Alfred Scheuing, Reuben's chef, and say he created the sandwich for Reuben's son, Arnold Jr., in the 1930s.


== Variations ==


=== Rachel sandwich ===
The Rachel sandwich is a variation on the standard Reuben sandwich, substituting pastrami for the corned beef, and coleslaw for the sauerkraut. Other recipes for the Rachel call for turkey instead of corned beef or pastrami. In some parts of the United States, especially Michigan, this turkey variant is known as a "Georgia Reuben" or "California Reuben", which sometimes uses barbecue sauce or French Dressing instead of Russian or Thousand Island.


=== Grouper Reuben ===
The grouper Reuben is a variation on the standard Reuben sandwich, substituting grouper for the corned beef, and sometimes will substitute coleslaw for the sauerkraut as well. This variation is often a menu item in restaurants in Florida. Another variation is the Lobster Reuben served in the Florida Keys.


=== West Coast Reuben ===
The West Coast Reuben is a variation on the standard Reuben sandwich, substituting Dijon mustard as the dressing.


=== Montreal Reuben ===
The Montreal Reuben substitutes Montreal smoked meat for corned beef.


=== Reuben egg rolls ===
Reuben egg rolls, sometimes called "Irish egg rolls" or "Reuben balls", use the standard Reuben sandwich filling of corned beef, sauerkraut, and cheese inside a deep-fried egg roll wrapper. Typically served with thousand island dressing as an appetizer or snack, they originated at Mader's, a German restaurant in Milwaukee, Wisconsin, where chef Dennis Wegner created them for a summer festival in about 1990.


=== Walleye Reuben ===
The Walleye Reuben is a Minnesotan version of the classic that features its state fish, the walleye, Sander vitreus.


== See also ==

Sloppy joe (New Jersey)
List of American sandwiches
List of sandwiches


== References ==


== Further reading ==