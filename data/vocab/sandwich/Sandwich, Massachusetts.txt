Sandwich /ˈsændwɪtʃ/ is a town in Barnstable County, Massachusetts, United States. The population was 20,675 at the 2010 census. The Town Hall is located right next to the Dexter Grist Mill, in the historic district of town.
The town motto is Post tot Naufracia Portus (Latin for "after so many shipwrecks, a haven"). For more geographic and demographic information on specific parts of the town of Sandwich, please see the articles on East Sandwich, Forestdale, and Sandwich (CDP). The postal ZIP codes for the town are 02537, 02644, and 02563, respectively. It is the oldest town on Cape Cod. In 2014, Sandwich turned 375 years old.


== History ==
Cape Cod was occupied for thousands of years by indigenous peoples, including the historic Algonquian-speaking Wampanoag, who encountered the English colonists of the Massachusetts Bay and Plymouth Colonies.
Sandwich was first settled by Europeans in 1637 by a group from Saugus with the permission of the Plymouth Colony. It is named for the seaport of Sandwich, Kent, England. It was incorporated in 1639 and is the oldest town on Cape Cod. The western portion of the town was separated from the original "Town of Sandwich", and it became the town of Bourne in 1884.

There are many historic homes in Sandwich, including the Benjamin Nye Homestead on Old County Road (formerly known as the original "Old King's Highway"), and the Benjamin Holway House, built in 1789, at 379 Route 6A, which property also now currently hosts one of the original Nye Homestead structures, built in 1698, believed to have originally served as either a tavern or a shop, and which is now used as a law office.

Sandwich was the site of an early Quaker settlement. However, the settlement was not well-received, as their beliefs clashed with those of the Puritans who founded the town. Many Quakers left the town, either for further settlements along the Cape, or elsewhere, including places like Dartmouth. Early industry revolved around agriculture, with fishing and trading also providing for the town. Later, the town grew a small industrial component along the Scusset River and Old Harbor Creek and its tributaries. Today, most of its industry revolves around tourism.

Deming Jarves founded the Boston & Sandwich Glass Factory in 1825. Sandwich had proximity to a shallow harbor, was a possible canal site, and had local supplies of timber to fuel the glass furnaces. The glass works primarily made lead-based glass, and was known for its use of color. Jarves received several patents for his improvements in glass mold designs and pressing techniques. The factory declined after the Civil War due to competition from Ohio, Pennsylvania and West Virginia companies that produced less expensive pressed soda-lime glass tableware.

There are several attractions in Sandwich, including Heritage Museums and Gardens, the Wing Fort House, the Sandwich Glass Museum, the Thornton Burgess Museum and Green Briar Nature Center and Jam Kitchen, Hoxie House (the oldest house on Cape Cod), the Dan'l Webster Inn, the Dexter Grist Mill, the oldest on Cape Cod, and Shawme Lake. The Sandwich town boardwalk is also popular for tourists. The walkway was destroyed by Hurricane Bob in 1991, but was rebuilt via private donations. Sandwich is home to numerous art galleries, rare book and antique stores. It is also home to a major portion of Otis Air National Guard Base, including half the land the runways are on.


== Geography ==
According to the United States Census Bureau, the town has a total area of 44.2 square miles (114.5 km2), of which 42.7 square miles (110.7 km2) is land and 1.5 square miles (3.8 km2), or 3.32%, is water. Sandwich is bordered by Cape Cod Bay to the north, Barnstable to the east, Mashpee and Falmouth to the south, and Bourne to the west. It is approximately 57 miles (92 km) south-southeast of Boston.

Sandwich is the site of the Cape Cod Bay entrance to the Cape Cod Canal. The northern point of Sandwich, where Sagamore Hill and Scusset Beach State Reservation lie, is divided from the rest of the town by the canal. The town is also the location of the Shawme-Crowell State Forest and the Massachusetts State Game Farm. The town is home to six beaches along the shores of Cape Cod Bay. The rest of the town's geography is typical of the rest of the Cape, with many small ponds and hills, with most of the trees being pine or oak. Sandwich is also the site of Old Harbor Creek, a large inlet with several other small creeks feeding it, which once served to provide safe harbor for ships.


== Transportation ==
U.S. Route 6, also known as the Mid-Cape Highway on Cape Cod, passes through the town to the north of Otis Air National Guard Base as a four-lane divided freeway. There are three exits off this route within town. Massachusetts Route 6A passes through town to the north of Route 6, and is the town's main local road. The northern terminus of Massachusetts Route 130 is near the intersection of Routes 6 and 6A within town. The road passes along the eastern side of Otis on its way towards Mashpee.

Freight rail service is provided by the Massachusetts Coastal Railroad. The Cape Cod Central Railroad operates seasonal tourist excursions from Hyannis to Sandwich and Sagamore. The nearest inter-city (Amtrak) passenger rail stations are Boston's South Station and Providence. Close by are the Kingston/Route 3 and Middleborough/Lakeville stations of the MBTA's commuter rail system, providing direct service to Boston. There is also seasonal passenger rail available in nearby Hyannis, the CapeFlyer, with service to Boston's South Station along the Middleborough/Lakeville line.
The nearest private and regional air service can be found in Barnstable, and the nearest national and international airport is Logan International Airport in Boston.


== Demographics ==

As of the census of 2000, there were 20,136 people, 7,335 households, and 5,515 families residing in the town. The population density was 467.9 people per square mile (180.6/km²). There were 8,748 housing units at an average density of 78.5 persons/km² (203.3 persons/sq mi). The racial makeup of the town was 97.75% White, 0.38% African American, 0.31% Native American, 0.54% Asian, 0.01% Pacific Islander, 0.32% from other races, and 0.69% from two or more races. Hispanic or Latino of any race were 0.80% of the population.
There were 7,335 households out of which 38.1% had children under the age of 18 living with them, 65.0% were married couples living together, 8.1% had a female householder with no husband present, and 24.8% were non-families. 20.0% of all households were made up of individuals and 8.9% had someone living alone who was 65 years of age or older. The average household size was 2.72 and the average family size was 3.18.
In the town the population was spread out with 28.4% under the age of 18, 4.9% from 18 to 24, 27.7% from 25 to 44, 25.3% from 45 to 64, and 13.7% who were 65 years of age or older. The median age was 40 years. For every 100 females there were 94.5 males. For every 100 females age 18 and over, there were 89.2 males.
The median income for a household in the town was $61,250 and the median income for a family was $66,553. Males had a median income of $49,195 versus $33,516 for females. The per capita income for the town was $26,895. About 2.2% of families and 3.1% of the population were below the poverty line, including 3.3% of those under the age of 18 and 4.1% ages 65 years or older.


== Government ==
Sandwich is represented in the Massachusetts House of Representatives as a part of the Fifth Barnstable District, which also includes portions of Barnstable, Bourne and Mashpee. The town is represented in the Massachusetts Senate as a part of the Plymouth and Barnstable district, which includes Bourne, Falmouth, Kingston, Pembroke, Plymouth, Plympton and portions of Barnstable. The town is patrolled by the Sandwich Police Department. The Police Department has 28 full-time staff members, including Chief, Lieutenants, Sergeants, Detectives, Patrol Officers and support staff. The Police Department Mission Statement lists the following Core Values: Respect, Excellence, Service, and the Highest Levels of Ethical and Moral Conduct. The State Representative is Randy Hunt (politician) who lives in East Sandwich, elected in 2010. The Sandwich Board of Selectmen has five members: Ralph Vitacco(Chair), Frank Pannorfi, Patrick Ellis, Susan James, and James Pierce.
On the national level, Sandwich is a part of Massachusetts's 10th congressional district, and is currently represented by William Keating. The state's junior (Class II) member of the United States Senate is Ed Markey. The senior (Class I) senator is Elizabeth Warren.
Sandwich is governed by the open town meeting form of government. Day-to-day operation is led by a town manager and a board of selectmen. The town operates a police department, and a fire department consisting of three fire stations. There are three post offices in town (East Sandwich, Sandwich, and Forestdale). The Sandwich Public Library is located in the town's center, and belongs to the Old Colony Library Network, the Southeastern Massachusetts Regional Library System, and the Massachusetts Library Information Network. Sandwich has a public access television station (PEG) Sandwich Community TV which archives all government meetings on its website.


== Education ==
Sandwich operates its own public school system. Three schools (Forestdale, Oak Ridge and Henry T. Wing) serve the town's kindergarten through eighth grade classes. (Wing Elementary also serves pre-kindergarten classes.) Sandwich High School, located in East Sandwich, operates grades nine through twelve. The school teams are called the Blue Knights, and their colors are Columbia blue and white.
High school students may also choose to attend the Upper Cape Cod Regional Technical High School in Bourne free of charge (Sandwich taxpayers contribute to that school's budget) as well as Sturgis Public Charter School in Hyannis. Students may also choose to attend private schools in Barnstable, Bourne, Falmouth or other neighboring communities. A few Sandwich students choose to attend private high schools off-Cape, such as Tabor Academy, Sacred Heart High School, and Bishop Stang High School.


== Notable people ==
Charles H. Bridges (1873–1948), Adjutant General of the United States Army in 1928–1933
Sam Brown, comedian and writer of the sketch comedy group The Whitest Kids U' Know, grew up in Sandwich
Thornton Burgess (1874–1965), conservationist and children's author (Peter Rabbit/Cottontail)
Jeffrey Adam "Duff" Goldman, star of the Food Network's Ace of Cakes; moved to Sandwich as a child
Aimee Lynn Chadwick actor, singer, songwriter
Chris McNamara scholar, rugby player for Loyola RFC; suffered fractured skull while leading team to victory, notable social media page creator


== References ==


== External links ==
Town website
Sandwich Chamber of Commerce
 Sandwich travel guide from Wikivoyage