Santa Cruz Bicycles is a manufacturer of high end mountain bikes based in Santa Cruz, California. They sponsor the Santa Cruz Syndicate, a downhill racing team. The company is moving premises in early 2013. On July 3, 2015. Santa Cruz bicycle was sold to Pon Holding Group. 


== History ==
Santa Cruz Bicycles was founded by Rob Roskopp and Rich Novak in 1993. Roskopp had spent many years as a professional skateboarder, and Novak's Santa Cruz Skateboards company had put out a special "Roskopp" model before the two met. Roskopp and Novak went into partnership with bike engineer Mike Marquez, who had particular experience in bicycle suspension, and Tom Morris, a designer, to build some prototypes.
Their first bike, in 1994, was a full suspension bike called the Tazmon. It had a 3 inches (76 mm) travel single pivot design, the first on the market. It was followed a year later by the 4 inches (100 mm) Heckler, a model name that continues to this day.
The company acquired the patents for their Virtual Pivot Point from Outland Bikes around 1999.


== Models ==
The company manufactures 20 models of mountain bikes, including the "Juliana" range of mountain bikes designed specifically for women. Their bikes are fabricated from either carbon fiber or aluminum, and suited to a wide range of disciplines.


== Santa Cruz Syndicate ==

Santa Cruz Syndicate is a sponsored downhill team affiliated with the company.


== References ==