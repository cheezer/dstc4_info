In cooking, en brochette refers to food cooked, and sometimes served, on brochettes, or skewers. The French term generally applies to French cuisine, while other terms like shish kebab, satay, or souvlaki describe the same technique in other cuisines. Food served en brochette is generally grilled.


== Description ==
The skewer itself, the brochette can also be used to dip pieces of food in a fondue. In those cases it normally takes a slightly different form and is sold as a brochette de fondue or as a set along with the fondue pot.
Typically, meats and vegetables are put on a brochette, but small pieces of bread can also be skewered along with the other ingredients as well.
In Louisiana barbecue, brochette is sometimes cooked at the barbecue in addition to ribs, sausage, steak, and chicken. This is due to the influence of Cajun cuisine, which is in turn influenced by French cuisine.
In Portuguese cuisine, they are known as Espetada.
Shashlik (Polish: szaszłyk, Russian: шашлы́к) is a similar dish in Eastern Europe (Belarus, Hungary, Poland, Russia, Ukraine), Caucasus, Iran, Central Asia, India, Iran, Israel, Mongolia, Morocco, and Pakistan. In Russia, like in Iran, the meat is usually marinated, similarly to Iranian shashlyk while the form of meat cubes, rather than large pieces of meat, is maintained. While it is not unusual to see shashlik listed on the menu of restaurants, it is more commonly sold in form of fast-food by street vendors who roast the skewers over wood, charcoal, or coal.


== Latin American and Spanish barbecued pinchos ==
Mixed grill, barbecued meat and vegetables on sticks, are known as espetinhos, pinchos, pinchos americanos, brochetas, or anticuchos in Central & South America. These barbecued pinchos may include pieces of beef, pork, chicken, fish/shark, Mexican chorizo (or sausage), kidney, or liver, among others.
In Puerto Rico, a barbecued type of pincho is served by street vendors. Unlike the Basque pincho, usually only one or two slices of bread are in the pincho, while the rest is barbecued chicken, pork, shark, or other meat. The meats and the bread are skewered on a wooden stick, rather than served on a plate; the stick is grabbed from the bottom and the contents are eaten.
In Spain, barbecued meat pinchos previously marinated in a spicy garlic and red pepper mixture are known as Pinchos morunos (Moorish skewers). They are similar (but not identical) to Middle Eastern kebabs. When they are small they are also known as pinchitos.


== See also ==
Similar dishes and variations:
Skewer - similar dish, usually grilled, with cubes of meat and vegetables, popular in all Europe
Anticuchos - Peru and other Andean states
Chuanr - China
Frigărui - Romania
Pinchitos - Spain (Andalusia and Extremadura)
Satay - Indonesia, Malaysia, Singapore, Brunei, Thailand, and the Netherlands
Yakitori - Japan
Mixed grill


== References ==


== External links ==
Brochettes on the Grill at Cooking.com