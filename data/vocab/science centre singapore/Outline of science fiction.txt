The following outline is provided as an overview of and topical guide to science fiction:
Science fiction – genre of fiction dealing with the impact of imagined innovations in science or technology, often in a futuristic setting. Exploring the consequences of such innovations is the traditional purpose of science fiction, making it a "literature of ideas".


== What is science fiction? ==

Definitions of science fiction: Science fiction includes such a wide range of themes and subgenres that it is notoriously difficult to define. Accordingly, there have been many definitions offered.
Science fiction is a type of:
Fiction – form of narrative which deals, in part or in whole, with events that are not factual, but rather, imaginary and invented by its author(s). Although fiction often describes a major branch of literary work, it is also applied to theatrical, cinematic, and musical work.
Genre fiction – fictional works (novels, short stories) written with the intent of fitting into a specific literary genre in order to appeal to readers and fans already familiar with that genre. Also known as popular fiction.
Speculative fiction

Genre – science fiction is a genre of fiction.


== Genres of science fiction ==
Science fiction genre – while science fiction is a genre of fiction, a science fiction genre is a subgenre within science fiction. Science fiction may be divided along any number of overlapping axes. Gary K. Wolfe's Critical Terms for Science Fiction and Fantasy identifies over 30 subdivisions of science fiction, not including science fantasy (which is a mixed genre).


=== Science ===
Genres concerning the emphasis, accuracy, and type of science described include:
Hard science fiction—a particular emphasis on scientific detail and/or accuracy
Soft science fiction—focus on human characters and their relations and feelings, while de-emphasizing the details of technological hardware and physical laws


=== Characteristics ===

Themes related to science, technology, space and the future, as well as characteristic plots or settings include:
Apocalyptic and post-apocalyptic science fiction
Cyberpunk—uses elements from the hard-boiled detective novel, film noir, Japanese anime, and post-modernist prose to describe the nihilistic, underground side of a cybernetic society
Climate fiction emphasizes effects of anthropogenic climate change and global warming at the end of the Holocene era
Dying Earth science fiction
Military science fiction
Mundane SF
Steampunk—denotes works set in (or strongly inspired by) an era when steam power was still widely used — usually the 19th century, and often set in Victorian England — though with otherwise high technology or other science fiction elements
Time travel
Space colonization
Space opera—emphasizes romantic adventure, exotic settings, and larger-than-life characters
Spy-fi
Social science fiction—concerned less with technology and more with sociological speculation about human society


=== Movements ===
Genres concerning politics, philosophy, and identity movements include:
Christian science fiction
Feminist science fiction
Gay/lesbian science fiction
Libertarian science fiction


=== Eras ===
Genres concerning the historical era of creation and publication include:
Cyberpunk—noted for its focus on "high tech and low life" and taking its name from the combination of cybernetics and punk.
Golden Age of Science Fiction—a period of the 1940s during which the science fiction genre gained wide public attention and many classic science fiction stories were published.
New Wave science fiction—characterised by a high degree of experimentation, both in form and in content.
Pulp science fiction
Scientific romance—an archaic name for what is now known as the science fiction genre, mostly associated with the early science fiction of the United Kingdom.
Steampunk—alternate histories in the spirit of Jules Verne, where a Victorian era steam-powered society develops advanced technologies.


=== Combinations ===

Genres that combine two different fiction genres or use a different fiction genre's mood or style include:
Alternate history science fiction—fiction set in a world in which history has diverged from history as it is generally known
Comic science fiction
Science fiction erotica
Adventure science fiction—science fiction adventure is similar to many genres and is emphasized in popular culture (see Romantic Science Fiction and Space Opera)
Gothic science fiction—a subgenre of science fiction that involves gothic conventions
New Wave science fiction—characterized by a high degree of experimentation, both in form and in content
Science fantasy—a mixed genre of story which contains some science fiction and some fantasy elements
Science fiction opera—a mixture of opera and science fiction involving empathic themes
Science fiction romance—fiction which has elements of both the science fiction and romance genres
Science fiction mystery—fiction which has elements of both the science fiction and mystery genres, encompassing Occult detective fiction and science fiction detectives
Science fiction Western—fiction which has elements of both the science fiction and Western genres
Space Western—a subgenre of science fiction that transposes themes of American Western books and film to a backdrop of futuristic space frontiers.


== Related genres ==
Fantasy
Science fantasy

Mystery fiction
Horror fiction
Slipstream fiction
Speculative fiction
Weird fiction

Superhero fiction


== Science fiction by country ==
Australian science fiction
Japanese science fiction
Canadian science fiction
Science fiction in China
Croatian science fiction
Czech science fiction and fantasy
French science fiction
Japanese science fiction
Norwegian science fiction
Science fiction and fantasy in Poland
Romanian science fiction
Russian science fiction and fantasy
Serbian science fiction
Spanish science fiction
Science fiction writers by nationality (category)


== History of science fiction ==

History of science fiction films


== Elements of science fiction ==


=== Character elements in science fiction ===
List of stock characters in science fiction
Extraterrestrials in fiction


=== Plot elements in science fiction ===


==== Plot devices in science fiction ====
Hyperspace


=== Setting elements in science fiction ===
The setting is the environment in which the story takes place. Elements of setting may include culture (and its technologies), period (including the future), place (geography/astronomy), nature (physical laws, etc.), and hour. Setting elements characteristic of science fiction include:


==== Place ====
Parallel universes
Planets in science fiction
Hyperspace
Slipstream
Earth far in the future


==== Cultural setting elements ====
Political ideas in science fiction
Utopian and dystopian fiction
World government in science fiction
World government in fiction

Religious ideas in science fiction
List of religious ideas in science fiction

Religion in speculative fiction
Xenology


==== Sex and gender in science fiction ====
Gender in science fiction
Sex in science fiction
Pregnancy in science fiction
LGBT themes in speculative fiction


===== Technology in science fiction =====

Computer technology
Artificial intelligence in fiction
List of fictional computers
Mind uploading in fiction

Transportation
Dry docks in science fiction

Weapons in science fiction
Railguns in science fiction

Resizing
Simulated reality in fiction
Space warfare in fiction
Weapons in science fiction


=== Themes in science fiction ===

First contact


=== Style elements in science fiction ===


== Works of science fiction ==


=== Science fiction art ===
List of science fiction and fantasy artists
Science fiction comics


=== Science fiction games ===


==== Science fiction computer games ====


==== Science fiction role-playing games ====


=== Science fiction literature ===
Science fiction comics
Speculative poetry


==== Science fiction novels ====
List of science fiction novels


==== Science fiction short stories ====
List of science fiction short stories


===== Venues for science fiction short stories =====
Science fiction magazine
Science fiction fanzine


=== Science fiction video ===
Science fiction film
Science fiction on television
List of science fiction television programs
List of science fiction sitcoms

U.S. television science fiction
British television science fiction


=== Science fiction radio ===
Science fiction radio programs


== Information sources ==
Baen Free Library
Internet Speculative Fiction DataBase
Science Fiction and Fantasy Writers of America
The Encyclopedia of Science Fiction


== Science fiction in academia ==
Science fiction studies
New Wave science fiction
Science in science fiction
Materials science in science fiction

Science fiction and fantasy journals
Science fiction libraries and museums


== Science fiction subculture ==
Science fiction conventions
List of science fiction conventions
List of fan conventions by date of founding

Science fiction fandom
Science fiction fanzine

Science fiction organizations


== Science fiction awards ==
The science fiction genre has a number of recognition awards for authors, editors and illustrators. Awards are usually granted annually.


=== International awards ===
Hugo Award—since 1955—General Science Fiction
Nebula Award—since 1965—General Science Fiction
Edward E. Smith Memorial Award (the Skylark)—since 1966
BSFA Award—since 1970—British Science Fiction
Seiun Award—since 1970—Japanese Science Fiction
Locus Award—since 1971—SciFi / Fantasy / New Authors (separate awards)
Saturn Award—film and television SF—since 1972
John W. Campbell Memorial Award for Best Science Fiction Novel—since 1973
Rhysling Award—for best science fiction poetry, given by the Science Fiction Poetry Association—since 1978
Parsec Award—since 2006
Philip K. Dick Award—since 1982
Arthur C. Clarke Award—since 1987
Theodore Sturgeon Memorial Award for best short science fiction—since 1987
Robert A. Heinlein Award—since 2003


=== Nationality specific awards ===
Aurealis Award—Australian
Prix Aurora Awards—for Canadian science fiction
Chandler Award—for contributions to Australian Science fiction
The Constellation Awards—for the best SF/fantasy film or television works released in Canada
Ditmar Award—for SF by Australians
Endeavour Award—for SF by Pacific Northwest author(s)
Janusz A. Zajdel Award—award of Polish fandom
Kitschies—for speculative fiction novels published in the UK
Kurd-Laßwitz-Preis—German SF award
Nautilus Award—Polish award
Paul Harland Prize—for Dutch SF
Prix Jules-Verne—France 1927–1933 and 1958–1963
Prix Tour-Apollo Award—France since 1972
SFERA Award—given by SFera, a Croatian SF society
Sir Julius Vogel Award—for SF by New Zealanders
Stalker Award – for the best Estonian SF novel, given out on Estcon by Eesti Ulmeühing, the Estonian SF society.
Tähtivaeltaja Award—for the best SF novel released in Finland
TBD Science Fiction Story Award — Turkey
Premio Urania—for Italian SF
The Galaxy Awards (银河奖)—given by magazine Science Fiction World for Chinese SF&F
SRSFF Award—România


=== Themed awards ===
Prometheus Award—best libertarian SF—since 1979
Lambda Literary Award—since 1988
Tiptree Award—since 1991
Golden Duck Awards—best children's SF—since 1992
Sidewise Award for Alternate History—since 1995
Gaylactic Spectrum Awards—since 1999
Geffen Award—Israeli award—since 1999
Norton Award—San Francisco—since 2003
Science Fiction & Fantasy Translation Awards—since 2009


=== New artists / first works ===
Writers of the Future—contest for new authors
Jack Gaughan Award for Best Emerging Artist
John W. Campbell Award for Best New Writer
Compton Crook Award For Best first Novel in the genre in a given year.


=== Career awards ===
Damon Knight Memorial Grand Master Award—associated with the Nebula


== Persons influential in science fiction ==


=== Creators of science fiction ===


==== Science fiction artists ====
List of science fiction and fantasy artists


==== Science fiction filmmakers ====


==== Creators of science fiction literature ====
List of science fiction authors
Women science fiction authors

List of science fiction editors


=== Science fiction scholars ===
Brian Aldiss
Isaac Asimov—Asimov on Science Fiction
Brian Attebery
Everett F. Bleiler
John W. Campbell
John Clute—co-editor of The Encyclopedia of Science Fiction (with Peter Nicholls)
Samuel R. Delany
Hugo Gernsback—founder of the pioneering science fiction magazine Amazing Stories, and the person who the Hugo Awards are named after.
David Hartwell
Larry McCaffery
Judith Merril
Sam Moskowitz
Peter Nicholls—co-editor of The Encyclopedia of Science Fiction (with John Clute)
Alexei Panshin
David Pringle—editor of Foundation and Interzone; author of Science Fiction: The 100 Best Novels
Andrew Sawyer
Dorothy Scarborough
Brian Stableford
Darko Suvin
Gary K. Wolfe


== See also ==

Outline of fiction
Fantasy
Horror fiction
Space rock


== References ==


== External links ==
Science Fiction (Bookshelf) at Project Gutenberg
SF Hub—resources for science-fiction research, created by the University of Liverpool Library
Science fiction fanzines (current and historical) online
Science Fiction and Fantasy Writers of America—their "Suggested Reading" page
Science Fiction Museum & Hall of Fame
Science Fiction Research Association
Science Fiction at the Internet Archive
(Russian) Ковтун Е. Н. Художественный вымысел в литературе 20 века. — Высшая школа, 2008. — 1500 экз. — ISBN 978-5-06-005661-7.
(Russian) Жанры — рубрика журнала «Мир фантастики»
(Russian) Вячеслав Бабышев Внешние и внутренние жанры фантастики // Уральский следопыт. — 2014. — № 11 (689). — С. 81-84.
(Russian) Жанры фантастики на Фантлабе.
(Russian) Классификация фантастики на сайте «Фэнта Зиландия».