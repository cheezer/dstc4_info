Vintage scuba is a reference to the art of scuba diving using vintage-style gear, which is generally defined at this time as equipment dating from 1975 and earlier, and to that equipment.


== Twin hose regulators ==
The most striking and well recognized example of vintage scuba gear is the twin-hose or double hose regulator, a popular style of regulator since Jacques-Yves Cousteau and Emile Gagnan pioneered the first such design, the C45 Scaphandre Autonome, which was marketed in the USA (along with a tank and harness) as the Aqua-Lung. At present, there is a renewed interest in the care and use of double hose regulators from the "Vintage Era" for scuba. The durability of the regulators from the 1950s through the early 1970s lent them to easily be refurbished and restored. Since 2007, Vintage Double Hose, in Wesley Chapel, Florida, has been manufacturing parts for original regulators as well as a modern version of the double-hose scuba regulator. The regulator is composed of modern polymers and specialty metals. It allows for additional scuba equipment to be attached, such as a submersible pressure gage, which overcomes one of the problems of the original double hose regulators which were not able to incorporate accessories.


== Gallery ==


== See also ==


== References ==