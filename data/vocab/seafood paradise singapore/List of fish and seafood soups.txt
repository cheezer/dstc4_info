This is a list of soups made with fish and seafood.


== Seafood soups ==

Bermuda fish chowder
Bouillabaisse — a Provencal dish, especially in the port of Marseilles
Buridda
Caldillo de congrio
Caldillo de perro
Cantonese seafood soup
Chupe
Cioppino
Clam chowder
Cullen skink
Dashi
Fish chowder
Fish soup
Fish soup bee hoon
Fish tea
Fisherman's soup
Gumbo – Often includes seafood, made with shrimp or crab stock
Herring soup
Lobster bisque
Lohikeitto
Lung fung soup
Mohinga
Phở – Some versions use seafood
Psarosoupa
She-crab soup
Sliced fish soup
Sopa marinera — a Spanish seafood dish made oysters, clams, seashells, crab, lobster, shrimp and spices like achiote and cumin
Tom Yum
Ukha


== See also ==

Fish stew
List of soups
List of seafood dishes


== References ==