Seafood birdsnest is a common Chinese cuisine dish found in Hong Kong, China and most overseas Chinatown restaurants. It is also found within Cantonese cuisine. It is usually classified as a mid to high-end dish depending on the seafood offered.


== Basket ==
The edible nest holding the seafood is made entirely out of fried taro or noodles.  There are different intricate netting used in the nest making. In all cases, the basket is tough and crunchy.


== Fillings ==
Despite the name there is nothing bird-related in this dish, nor are there any dried ingredients. The most common ingredients are scallops, peapods, boneless fish fillet, celery sticks, straw mushrooms, calamari, shrimp.


== See also ==

List of fish dishes


== References ==