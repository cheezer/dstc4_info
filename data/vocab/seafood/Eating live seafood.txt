The practice of eating live seafood, such as fish, crab, oysters, young shrimp, or young octopus, is widespread.


== Live seafood dishes ==


== Ethical viewpoints ==
London resident Louis Cole runs a YouTube channel in which he eats live seafood. The Guardian commented on the ethical issues raised by the behaviour of Cole that: "It seems objectively less cruel to kill a scorpion instantly than to rear chickens in battery cages or pigs in the most miserable pork farms.
The view that oysters are acceptable to eat, even by strict ethical criteria, has notably been propounded in the seminal 1975 text Animal Liberation, by philosopher Peter Singer. However, subsequent editions have reversed this position (advocating against eating oysters). Singer has stated that he has "gone back and forth on this over the years", and as of 2010 states that "while you could give them the benefit of the doubt, you could also say that unless some new evidence of a capacity for pain emerges, the doubt is so slight that there is no good reason for avoiding eating sustainably produced oysters".


== Health issues ==
In India, the government provides support for an annual fish medicine festival in Hyderabad, where asthma patients are given a live sardine to eat which is supposed to cure their asthma.
Infection by the fish tapeworm Diphyllobothrium latum is seen in countries where people eat raw or undercooked fish, such as some countries in Asia, Eastern Europe, Scandinavia, Africa, and North and South America.


== See also ==


== References ==


== External links ==
Macho foodies in New York develop a taste for notoriety The Observer, Sunday 30 May 2010.
Foods to Try Before You Die Fox News, 19 October 2009.
Lobsters and Crabs Feel Pain, Study Shows Discovery News, 27 March 2009.
Do fish feel pain? HowStuffWorks. Retrieved 27 May 2012.