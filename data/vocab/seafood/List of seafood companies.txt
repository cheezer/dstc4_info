This is a list of seafood companies in some countries. Seafood is any form of sea life regarded as food by humans. Seafood prominently includes fish and shellfish. Seafood companies are typically involved with fishing, fish processing, distribution and marketing. Seafood companies also produce feed and nutrition products for farmed fish.


== Seafood companies ==


=== Greenland ===

Royal Greenland


=== Norway ===

Aker BioMarine
Copeinca
Domstein
EWOS
GC Rieber
Grieg Seafood
Havfisk
Lerøy
Marine Farms
Marine Harvest
Nergård AS
Norges Sildesalgslag 
Norway Pelagic
Rauma Group
Rem Offshore
Stolt-Nielsen
Volden Group


=== Sweden ===
Abba Seafood 


=== Thailand ===
Thai Union Group


=== United Kingdom ===
John West Foods
Pescanova
Ross Group
Shippam's
Whitby Seafoods Ltd
Young's Bluecrest


=== United States ===

American Seafoods 
Anna Maria Fish Co.
Bumble Bee Foods 
Old Dixie Seafood
Pacific Seafood
Phillips Foods, Inc. and Seafood Restaurants
Punta Gorda Fish Co.
Trident Seafoods


=== Vietnam ===
Cuulong Fish
Hung Hau Pangasius


=== Pakistan ===
Kanpa International Sales


== See also ==


== References ==