HarbourFront (Chinese: 港湾; pinyin: Gǎng Wān) is a district situated in southern Singapore, within the Bukit Merah Planning Area. Harbourfront is also the gateway to Sentosa.
Main thoroughfares HarbourFront include Keppel Way and Telok Blangah Road. Notable buildings in the vicinity include HarbourFront Centre, Singapore Cruise Centre, St James Power Station and VivoCity. HarbourFront is currently being redeveloped into a new business and lifestyle hub.


== Etymology ==
Before recent developments, the area was known as Seah Im. After the Port of Singapore expanded, it became known as Maritime Square, and was also commonly known as the "World Trade Centre" area. The name HarbourFront was coined in 2000 to give a more upmarket feel to the area.


== History ==
HarbourFront was once close to the southernmost point of the main island of Singapore, until land reclamation in Tanjong Pagar and Tuas. Its location in the sheltered waters of Keppel Harbour helped the area to thrive as a commercial area adjacent to Keppel Shipyards, particularly with the building of the former World Trade Centre in 1978. Singapore's first major Exhibitions and Conventions venue, the World Trade Centre Exhibition Complex, was subsequently built beside the World Trade Centre, and also served as a transport node with ferries to Sentosa and other regional destinations such as Batam. The Singapore Cruise Centre opened in 1991 in HarbourFront as the country's first international cruise terminal.
Massive redevelopment in the area after the closure of the shipyard subsequently led to the name HarbourFront being coined in the early 2000s to refer to the immediate vicinity of the World Trade Centre, which was in the midst of a major refurbishment project. Relaunched as HarbourFront Centre, the former World Trade Centre also has its neighbouring Exhibition Complex demolished to make way for VivoCity, the upgrading of the Cable Car Towers and the construction of two new office blocks adjacent to it. Transportation into the area was vastly improved with the opening of the HarbourFront MRT Station and extension of the HarbourFront Bus Interchange. Also, the new monorail link to Sentosa, the Sentosa Express has been completed and opened on 15 Jan 2007. The HarbourFront station of the Circle Line had opened on 8 October 2011.
A new six-storey office building is also in the works as a new addition to the HarbourFront Office Park. The new site is known as the HarbourFront Merrill Lynch, which is a six-storey building and will be entirely occupied by American bank Merrill Lynch with a total area of 200,000 sq ft (19,000 m2). Another office development is being planned on the site of the current SPI Building. Furthermore, a residential development is possible in the future. [1] The Singapore Cruise Centre would be expanded to accommodate over 6 million passengers by 2010.


== External links ==
The HarbourFront Precinct Masterplan
The HarbourFront Precinct Masterplan