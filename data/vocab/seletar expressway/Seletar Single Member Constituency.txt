Seletar Single Member Constituency used to be a single-seat ward between 1951 and 1959 in Seletar area of Singapore within its North-East Region. It was split into various wards in 1959.


== References ==