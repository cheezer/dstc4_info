Upper Seletar Reservoir (Chinese: 实里达蓄水池上段 ; Malay: Takungan Air Seletar Atas) is Singapore's third impounding reservoir, after MacRitchie Reservoir and Peirce Reservoir (now the Upper Peirce Reservoir and the Lower Peirce Reservoir). It is located within the Central Water Catchment area of Singapore island.


== History ==
Upper Seletar Reservoir was formerly known as the Seletar Reservoir, before the completion of the Lower Seletar Reservoir near Nee Soon, formed by the damming up of the mouth of Seletar River, in 1986. The Seletar Reservoir was built to meet the surge in water demand after World War I. It was completed in 1940 and officially opened on 10 August 1969 by HRH Princess Alexandra.


== Highlights ==
The Upper Seletar Reservoir Park, formerly known as Seletar Reservoir Park, covers a large expanse of open space on the eastern side of the reservoir. The 15-hectare park was completed in 1973. It features a viewing tower, and is a frequent venue for joggers, walkers and fishing enthusiasts. Formerly, Hash Harriers and horse riders visited the Upper Seletar Reservoir Park as well.
The Singapore Zoo, formerly known as Singapore Zoological Gardens, the Night Safari and the Mandai Orchid Garden are located on the margins of the Upper Seletar Reservoir.
It is said there are crocodiles currently living in the reservoir, however no concrete evidence have been found, as all past reports were based on eyewitnesses accounts. Nonetheless, signboards with "Beware of Crocodile" have been placed along various locations at the sides of the reservoir.


== See also ==
Lower Seletar Reservoir
List of Parks in Singapore


== References ==
Norman Edwards and Peter Keys (1996), Singapore - A Guide to Buildings, Streets and Places, Times Books International, ISBN 981-204-781-6


== External links ==
National Parks Board, Singapore