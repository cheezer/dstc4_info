Seletar Robbery (Simplified Chinese: 实里达大劫案, literally "The Great Seletar Robbery") is a 90-minute Singaporean action drama produced by Singapore Broadcasting Corporation (now MediaCorp) in 1982.


== Production ==
SBC began filming the drama on May 31, 1982, and filming lasted 24 days.


== Plot ==
Three thugs, Zhao, Soon and Chai, rob an employee of the workers' payroll belonging to a construction company of S$300000 which was just withdrawn from bank. The construction company was located at Seletar Reservoir, hence the drama's name. The police is alerted by an electrician who had witnessed the robbery. The robbers are about to escape when the police arrive.


== Cast ==
Lim Sin Ming as Inspector Kao
Huang Wenyong as Chen
Chin Chi Kang as Chai
Richard Wong as Zhao
Tan Tian Song as Soon
David Kwok as an insane man


== Significance ==
Although it has only one episode, and there were earlier Chinese Language productions, Seletar Robbery is considered by MediaCorp to be the first locally produced Chinese Language television drama series, and Its first airing date- 25 July 1982 at 8 pm- is considered by SBC and its modern incarnation MediaCorp TV as the birth of local Mandarin Chinese drama production.
In 2007, MediaCorp marked its 25 years of Chinese television drama production by producing The Golden Path, which had a storyline commencing in 1982, the same year the Seletar Robbery was first aired. It showed clips of this series being watched by the characters of The Golden Path on television, along with some scenes borrowed from the original series.


== External links ==
MediaCorp MOBTV
[1] Straits Times Digitised