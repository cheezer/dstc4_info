Sembawang MRT Station (NS11) is an elevated Mass Rapid Transit (MRT) station on the North South Line in Singapore. The station is located in Sembawang.


== History ==
When the North South Line Woodlands Extension was proposed in 1990, Sembawang was not included on this planned route. It was later included as a provisional station to be built at later stage. However, the government later decided to construct this station as part of the extension. The station was opened on 10 February 1996 along with the other five stations on the Woodlands Extension. When the station was opened, there is nothing except the Canberra Road and under construction Sembawang New Town. This was resolved in 1998 because of the accelerating development of Sembawang New Town.
Following numerous incidents of commuters falling on the tracks and unauthorised intrusions, the Land Transport Authority made the decision in 2008 to install half-height platform screen doors for all above-ground stations in phases. Installation started on 29 October 2011 and the screen doors began operation on 31 January 2012.


== High volume, low speed (HVLS) fans ==
Station installed with Rite Hite Revolution High Volume, Low Speed (HVLS) fans and commenced operations on 9 November 2012 together with Admiralty.


== Station layout ==


== Exits ==
A: Sembawang Bus Interchange
B: Jelutong CC, Sembawang NPC, 3M and Seagate Building, Sembawang Drive, Sembawang Sec Sch
C: Sembawang Way
D: Admiralty Country Club, Assyafaah Mosque, Canberra Road, Wellington Pri Sch


== Transport connections ==


=== Rail ===


== References ==


== External links ==
Official website