"HMS Simbang, "RAF Sembawang" ,"RNAS Sembawang" & "Sembawang Airfield" redirects here.
Sembawang Air Base (ICAO: WSAG) is a military airbase of the Republic of Singapore Air Force located at Sembawang, in the northern part of Singapore. The base motto is "Dare and Will".


== §RAF Sembawang ==
 Media related to RAF Sembawang at Wikimedia Commons
Prior to Singapore's independence from the United Kingdom, it was a Royal Air Force station known as RAF Sembawang as well as being the Royal Naval Air Station – HMS Simbang – to the carrier pilots of the Royal Navy Fleet Air Arm (attached to the Eastern Fleet based in Singapore) who used it for rest and refit whenever an aircraft carrier of the Royal Navy berthed at the nearby HMNB Singapore for refuel and repairs, which also housed the largest Royal Navy dockyard east of Suez up to the time of UK forces withdrawal from Singapore.
After the Japanese capture of Singapore during World War II, the Imperial Japanese Navy Air Service took over the two RAF stations of Sembawang and Seletar. Singapore was split into north-south spheres of control, and the Imperial Japanese Army Air Force took over RAF Tengah. It was not until September 1945 that the two airfields reverted to British control following the Japanese surrender.
RAF Sembawang was a key part of Britain's continued military presence in the Far East (along with the three other RAF bases in Singapore: RAF Changi, RAF Seletar, RAF Tengah) during the critical period of the Malayan Emergency (1948–1960), the Brunei Revolt in 1962 and the Indonesia–Malaysia confrontation (1962–1966).


== §Sembawang Air Base ==
The base was renamed Sembawang Air Base (SBAB) in 1971 when it was handed over to the Singapore Air Defence Command (SADC). From 1971 to 1976, under the auspices of the Five Power Defence Arrangements (FPDA), Sembawang housed British, Australian and New Zealand forces.
In 1983, the airbase became a full fledged rotary-wing air base when the first resident helicopter squadron – 120 Squadron – was permanently relocated from Changi Air Base.
Currently, there are approximately 100 helicopters based in Sembawang Air Base, almost all are operating in support of the Singapore Army and the Republic of Singapore Navy. It is the home base to all the RSAF helicopter squadrons, consisting of AS-332 Super Pumas, CH-47SD Chinooks, Sikorsky S-70B (derivative of Sikorsky SH-60 Seahawk) naval helicopters, as well as the Fennecs and UH-1Hs, which are currently stored in reserve. Recently added to the base are the AH-64D Longbow Apache attack helicopters.
The Flying squadrons are:
120 Squadron with 20 AH-64D Longbow Apaches;
123 Squadron with 6 S-70B Seahawks, these are owned and operated by the Republic of Singapore navy but flown by RSAF pilots;
124 Squadron with 5 EC120 Colibri, headquartered at SBAB with a training detachment at Seletar Airport;
125 Squadron with 22 AS332M Super Puma, four of these are configured for Search and rescue duties;
126 Squadron with 12 AS532UL/AL Cougar but is currently based at Oakey Airbase in support of SAF's training need in Australia;
127 Squadron with 6 CH-47D and 12 CH-47SD Chinooks.
The Support Squadrons are:
Air Logistics Squadron (ALS)
Airfield Maintenance Squadron (AMS)
Field Defence Squadron (FDS)
Flying Support Squadron (FSS)
Currently, the RSAF's Chong Pang Camp SADA (Singapore Air Defence Artillery), with its associated Air Defence assets, is also located within the compound of the air base as well as the famous local Sembawang Hot Spring.


== §Photo gallery ==


== §See also ==
Republic of Singapore Air Force
Singapore strategy
British Far East Command
Far East Air Force (Royal Air Force)
Far East Strategic Reserve
Former overseas RAF bases
Battle of Singapore
Malayan Emergency
Indonesia–Malaysia confrontation


== §References ==


== §External links ==
History of RAF
Airport information for WSAG at World Aero Data. Data current as of October 2006.
RSAF web page on Sembawang Air Base (SBAB)
Background history of R.N.A.S. Sembawang