Sembawang Bus Interchange is a bus interchange located in Sembawang, Singapore. Opened on 20 November 2005, it provides hassle-free transfer to the nearby Sembawang MRT Station.
This is one of the interchanges in a residential estate to not have any feeders or intratown services.


== History ==
Before its opening, bus services terminated at scattered parts of Sembawang town, such as Admiralty Road West Bus Terminal for Service 980, Sembawang Road End Bus Terminal (near Sembawang Park) for Service 167 and the pair of bus stops outside Sembawang MRT Station for Service 981. When the bus interchange opened, these bus services, along with Service 859, which operated from Yishun Bus Interchange, were amended to terminate at the newly opened bus interchange. A replacement bus service, Service 882, was created to replace the lost link between Sembawang and Sembawang Park. Services to the former Admiralty Road West Bus Terminal from Sembawang were covered by Service 856, which passes by both places along its route between Yishun Bus Interchange and Woodlands Regional Bus Interchange, and bus Service 981, which passes by both place on its route to Senoko Industrial Estate.


== External links ==
Interchange/Terminal (SMRT Buses)
Sembawang Town Map & Services Guide