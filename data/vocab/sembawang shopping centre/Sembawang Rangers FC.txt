Sembawang Rangers Football Club was a football club in Singapore, based in Yishun. The club is also referred to among supporters as the "Stallions".
The club was formed as merger between Gibraltar Crescent and Sembawang Sports Club to enter the inaugural S.League in 1996.
Following a league revamp, the club was removed from the S.League at the end of the 2003 season.


== S.League Record ==
1996 - Series 1: 7th place; Series 2: 6th place
1997 - 8th place
1998 - 8th place
1999 - 8th place
2000 - 9th place
2001 - 8th place
2002 - 6th place
2003 - 9th place


== Singapore Cup Record ==
1997 - First Round
1998 - 4th place
1999 - First Round
2000 - First Round
2001 - First Round
2002 - Quarter-finals
2003 - First Round


== External links ==
S.League website page on Sembawang Rangers