Sentosa Development Corporation (Abbreviation: SDC; Chinese: 圣淘沙发展局; Malay: Perbadanan Pembangunan Pulau Sentosa) is a statutory board in Singapore, under the purview of the Ministry of Trade and Industry.


== Purpose ==


=== Islands ===
The Sentosa Development Corporation (SDC) was formed on 1 September 1972. Its main task is to oversee the development, management and promotion of the island resort, Sentosa Island.
SDC also oversees the management of nine other offshore islands south of Sentosa, collectively known as the Southern Islands. These include:
Saint John's Island
Sisters' Islands
Pulau Hantu
Pulau Biola
Pulau Jong
Lazarus Island
Kusu Island
Pulau Seringat
Pulau Tekukor


=== Subsidiaries ===
SDC is in charge of the following four subsidiaries:
Sentosa Leisure Management (SLM) oversees the day-to-day operations of Sentosa.
Sentosa Cove Resort Management (SCRM) is the place manager for Sentosa Cove, a residential and commercial district on the island’s waterfront.
Sentosa Golf Club (SGC) manages two award-winning 18-hole championship golf courses on the island, The Serapong and The Tanjong.
Mount Faber Leisure Group (MFLG) operates Singapore’s sole cableway system and offers lifestyle merchandise as well as F&B dining on the hill at Faber Peak (formally the Jewel Box).


=== Monorail ===
SDC owns the Sentosa Express, a S$140 million light rail system that provides easy access into Sentosa from VivoCity.
The old SDC office at Allanbrooke Road was once served by the Sentosa Monorail which ceased operations on 16 March 2005. The new SDC office is currently based at Artillery Avenue.


== See also ==
Sentosa Island
Sentosa Cove
Statutory boards of the Singapore Government


== External links ==
official Sentosa Development Corporation website.
Sentosa Development Corporation Act — Singapore Statutes.