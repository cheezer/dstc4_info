Resorts World Sentosa is an integrated resort on the island of Sentosa, off the southern coast of Singapore. The key attractions include one of Singapore's two casinos, a Universal Studios theme park, Adventure Cove Water Park, and a Marine Life Park, which includes the world's largest oceanarium.
The S$6.59 billion (US$4.93 billion) resort is developed by Genting Singapore, listed on the SGX. It is the third most expensive building ever constructed. The resort occupies over 49 hectares (121 acres) of land and, when fully open, will employ more than 10,000 people directly. Resorts World Sentosa is a sister resort to Resorts World Genting, Pahang, Malaysia and Resorts World Manila, Philippines.
The soft launch of the first four hotels took place on 20 January 2010, with the FestiveWalk shopping mall following on 1 February. The casino opened on 14 February 2010 on the first auspicious day of the Chinese New Year. The Maritime Experiential Museum opened on 15 October 2011 and the last attraction opened on 22 November 2012, The Marine Life Park.
The grand opening of the integrated resort held on 7 December 2012, officiated by Prime Minister Lee Hsien Loong together with Genting Group Chairman Lim Kok Thay. Resorts World Sentosa is also expected to hold large-scale exhibitions such as Valentino, Retrospective: Past/Present/Future.


== History ==


=== Integrated Resorts in Singapore history ===


=== Resort history ===
Construction of Resorts World Sentosa Singapore began on 16 April 2007 on the demolished plot of Imbiah Lookout. It opened in a record time of 34 months of construction on 20 January 2010. Crockfords Tower, Hard Rock Hotel Singapore, Festive Hotel and Hotel Michael opened 20 January 2010, followed by FestiveWalk on 31 January 2010. Resorts World Sentosa Casino opened on 14 February 2010.Universal Studios Singapore was opened for sneak peek week in view of the Chinese New Year Celebrations, from 5 pm to 9 pm every night between 14 to 21 February 2010. The whole park was opened but none of the rides were operational. Visitors had to pay SGD10 to get into the park. Park tickets for the week were sold out in 2 days. The park had its soft opening period from 18 March 2010 to 26 October 2010.


=== Timeline ===
10 October 2006 – Genting International and Star Cruises submitted its proposal, suitably named Resorts World at Sentosa to the Singapore government.
16 October 2006 – Genting International and Star Cruises unveiled its proposal "Resorts World at Sentosa".
8 December 2006 – Genting International and Star Cruises won the bid for the Sentosa IR.
16 April 2007 – Resorts World at Sentosa broke ground on 49-hectare site.
7 November 2007 – Resorts World at Sentosa announced that it will be adding six new attractions to its resort.
October 2008 – Resorts World at Sentosa announced new Transformers attraction for Universal Studios Singapore.
September 2009 – Universal Studios Singapore unveiled Far Far Away and Madagascar.
October 2009 – Universal Studios Singapore unveiled attractions, dining options and merchandise outlets at Universal Studios Singapore
20 January 2010 – Soft Opening of Resorts World Sentosa – Crocksford Tower, Hotel Michael, Hard Rock Hotel and Festive Hotel
31 January 2010 – Opening of FestiveWalk
1 February 2010 – Opening of Waterfront Station of Sentosa Express.
14 to 21 February 2010 – Sneak Peak week for Universal Studios Singapore.
14 February 2010 – Opening of Resorts World Sentosa Casino.
Late February 2010 – Opening of Link Bridge from Resorts World Sentosa to Imbiah Lookout.
18 March 2010 – Soft opening of Universal Studios Singapore with Pantages Hollywood Theater, Lights Camera Action, Battlestar Galactica, Accelerator, Revenge of the Mummy, Treasure Hunters, Canopy Flyer, Jurassic Park Rapids Adventure, Waterworld, Amber Rock Climb, Dino-Soarin', Shrek 4D Adventure, Donkey Live, Enchanted Airways, Magic Potion Spin and King Julien’s Beach Party-Go-Round.
24 April 2010 – MediaCorp and RWS presents the Star Awards 2010 Show 2, a Mandarin TV awards ceremony which is a prize presentation for performance based and popularity awards.
2 July 2010 – New Lake Hollywood Spectacular show on Friday and Saturday nights at Universal Studios Singapore.
October 2010 – Opening of Candylicious, one of Asia's largest candy store.
Late October 2010 – Opening of Waterfront Area.
25 December 2010 – Opening of Crane Dance.
Spring 2011 – First year anniversary.
21 February 2011 – The re-opening of Battlestar Galactica after a technical glitch caused it to be closed just one week after opening.
23 April 2011 – MediaCorp presents the Star Awards 2011 Show 2 for the second year running.
16 May 2011 – The scheduled opening of Madagascar: A Crate Adventure in Universal Studios Singapore.
15 October 2011 – Opening of Maritime Experiential Museum.
3 December 2011 – Opening of Transformers: The Ride at Universal Studios Singapore. The ride had its world premiere at an exclusive evening event on 2 December 2011 with director Michael Bay graced the event.
16 February 2012 – Opening of Equarius Hotel and Beach Villas.
6 July 2012 – Opening of ESPA by RWS.
22 November 2012 – Opening of Marine Life Park (S.E.A Aquarium [except Dolphin Island] and Adventure Cove Waterpark)
7 December 2012 – Official Opening Ceremony of Resorts World Sentosa.
1 March 2013 – Opening of Sesame Street Spaghetti Space Chase at Universal Studio Singapore.
9 April 2013 – Unveiling of dolphins for public viewing at S.E.A. Aquarium.
22 May 2013 – Launch of Ultimate Maritime Encounters with Open Ocean Dive at Adventure Cove Waterpark.
15 June 2013 – Launch of Shark Encounter at Adventure Cove Waterpark.
30 June 2013 – Launch of Sea Trek Adventure at Adventure Cove Waterpark.
21 July 2013 – Battlestar Galactica ride at Universal Studios Singapore closed for an "attraction review".
30 September 2013 – Opening of Dolphin Island at Marine Life Park.
8 April 2015 - Puss In Boots ride opens in Universal Studios Singapore. 
27 May 2015 - Battlestar Galactica ride at Universal Studios Singapore re-opens. 


== Resort layout ==
The resort was designed primarily by American architect Michael Graves. Four hotels offer a total of 1,840 rooms for accommodation. Each hotel is designed with a different theme, catering to both the leisure and business visitors. The resort is split into west, central and east zones.


=== Central Zone ===


==== Hotels ====
The resort has six hotels with 1,800 rooms.
Crockfords Tower, formerly planned to be named Maxims Tower, is an 11-storey all-suite hotel overlooking the Singapore harbour and the Southern Islands. The resort's casino is located beneath the tower. The hotel was topped-out on 27 February 2009 and opened on 20 January 2010. Both the latter and Hotel Michael sit on the area of the former Sentosa Musical Fountain The hotel also features Crockfords Premier, a casino club with private rooms for High Roller located on 10th floor.
Hotel Michael is an 11-storey hotel named after Michael Graves. Hotel Michael topped-out on 15 July 2009 and was opened on 20 January 2010. Together with Crockfords Tower, it replaces the site of former Sentosa Musical Fountain
Festive Hotel is a family-oriented hotel next to Crockfords Tower and Festive Walk. Beneath the hotel is Festive Grand, a 1,600 seat plenary hall which will host Resorts World Sentosa's resident musical Voyage de la Vie.
The Hard Rock Hotel Singapore is the site of meeting and conference facilities, and indoor exhibition space. This includes 26 differently-designed function rooms and one of Asia's largest ballrooms with seating for 7,300 guests. Construction of Singapore's first Hard Rock Hotel started in May 2008, and the hotel opened on 20 January 2010.
Equarius Hotel is situated at the west of the resort.


==== Casino ====

The casino, capped at 15,000 sq.m. by regulation, is located beneath Crockfords Tower. Government regulations also require Singapore citizens and permanent residents to purchase a S$100 day pass or S$2000 yearly membership for access into the casino. Singaporean patrons can enter/leave the casino freely as long as their levy has not expired. Both types of entry levy (daily or yearly) allow the patron to visit to only one casino.
Overseas citizens can also enter and leave free of charge with their passports or employment passes.
The Casino Levy money is collected by the Singapore Totalisator Board controlled by the Government, and is used for public and charity causes. The Casino does not keep any part of the levy money.
On May 2011, the Casino Regulatory Authority has fined Resorts World Sentosa (Casino) for 2 violations related to reimbursements and 2 other violations related to surveillance practices. The total fine was S$530,000 (US$425,000).


==== Fine Dining ====
Tunglok heen by Susur Lee – Chinese cuisine
Restaurant de Joël Robuchon by Joël Robuchon – French cuisine
L'Atelier de Joël Robuchon by Joël Robuchon – Contemporary French cuisine
Osia by Scott Webster – Modern Australian cuisine
Forest by Sam Leong – Contemporary Chinese cuisine


==== Casual Dining ====
Palio at Hotel Michael – Italian cuisine
Starz Restaurant at Hard Rock Hotel – Western & Asian Buffet


==== Resorts World Galleria Luxury Fashion ====
The Galleria links the Festive Hotel, casino entrance and Hotel Michael. It includes the first Victoria's Secret boutique in Asia.


==== Shows ====
Voyage de la Vie is the first permanent production show to open at Resorts World Sentosa. This resident rock musical is set in the Festive Grand Theatre with a capacity of 1,600 people. The production was created by Mark Fisher.
Martial Combat, Asia's largest mixed martial arts fighting championship, is staged over six months each year at the Compass Ballroom, and broadcast by ESPN STAR Sports.
Crane Dance is a multimedia moving art installation with choreographed animatronic cranes built over the sea, and designed by Jeremy Railton. Edward S. Marks and Bob Chambers (of The Producers Group) were brought on to oversee the construction, installation and programming of the Cranes. Marks served as Project Director and Producer while Chambers served as its Senior Technical Director. It opened on 25 December 2010.
Lake of Dreams is a multimedia spectacular that combines the elements of water, fire, air and light, designed by Jeremy Railton. Edward S. Marks and Bob Chambers oversaw the construction, installation and programming of this attraction as well.


=== West Zone ===


==== Hotels ====
Equarius Hotel, close to the Marine Life Park.
Beach Villas, a collection of 22 villas floating on a lagoon. It opened on 16 February 2012.


==== Salon & Spa ====
Salon Royale
Spa Republic
ESPA, The first ESPA in Asia commences on 6 July 2012.


==== Marine Life Park ====

Marine Life Park, the world's largest oceanarium, opens its doors on 22 November 2012. The park houses two attractions, the S.E.A Aquarium and the Adventure Cove Waterpark, previously known as the Equarius Water Park.


==== The Maritime Experiential Museum ====

The Maritime Experiential Museum was opened on 15 October 2012 that features more than 400 artefacts and replicas with a 360-degree Multi-sensory Typhoon Theatre. It is the only museum in Singapore to display the history of ancient maritime trade where visitors have the opportunity to immerse themselves in the history of maritime Silk Route from the 15th to 19th century. The museum consists of more than 10 interactive points as well as an experience to board on the authentic harbour ships from Asia docked outside the museum. Also, it will become the permanent home of the Jewel of Muscat, a gift from the Oman Government.


=== East Zone ===


==== Universal Studios Singapore ====

Universal Studios Singapore is Southeast Asia's first Universal Studios theme park and opened its doors on 18 March 2010. It features 24 attractions and is divided into seven zones – including Sci-Fi City, Ancient Egypt, New York, The Lost World, Far Far Away, Madagascar and Hollywood.


== See also ==
Integrated resort
Marina Bay Sands


== References ==


== External links ==
Official website