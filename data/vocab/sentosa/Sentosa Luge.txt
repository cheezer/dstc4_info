Skyline Luge Sentosa is a luge located in Sentosa, Singapore. The attraction opened in the second half of 2005. The luge, situated on the Imbiah Lookout cluster opposite the Tiger Sky Tower, has two tracks. The older one is called the Jungle Trail (628 metres (2,060 ft)) and the newer one the Dragon Trail (688 metres (2,257 ft)). The luge is a self-driving car system in which riders control the speed by pushing a pair of handlebars back and forth. The luge ride goes downhill, relying on gravitational pull to move. After the ride, the Skyride using chairlifts brings riders, luge carts, and the helmets back to the starting point. The Skyride is similar to a ski lift.


== Incidents ==
On 19 May 2008, two children aged nine and six fell as they were getting off the moving chairlift and were trapped in the small space between the chairlift and the concrete ground. The chairlift dragged them for half a metre before they were pulled out by a friend. The children suffered cuts and bruises.


== References ==


== External links ==
Sentosa Luge and Skyride
Skyline Luge Sentosa