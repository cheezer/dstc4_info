Serangoon Bus Interchange (Chinese: 实龙岗巴士转换站) is a bus interchange serving Serangoon New Town in Singapore, and is within walking distance from Serangoon. Opened on 3 September 2011, it is the smallest air-conditioned bus interchange in Singapore with 8 services and 17 bus bays. It is also the second air conditioned bus interchange to open along the North East Line and the first on the Circle Line.


== History ==
On 10 March 1988, Serangoon Bus Interchange opened along Serangoon Central, built underneath a multi-storey carpark (Block 264). This interchange took over the overcrowded Serangoon Gardens Bus Interchange and a temporary bus terminal along Serangoon Avenue 3. The spacious compound had both end-on berths at the East side and sawtooth berths (with a bus park) at the West side. Entrance and exit was located along Serangoon Central.
When the shopping mall nex opened on 25 November 2010, the old bus interchange continued to operate due to congestion at Serangoon Avenue 2, carpark problems and large crowds in the shopping mall.


=== Relocation ===
On 3 September 2011, it was relocated to the new bus interchange within the shopping mall directly above Serangoon, making it the fifth bus interchange to be air conditioned and the second along the North East Line and first along the Circle Line. The new facility was the first to have glass walls instead of cement walls facing the bus bays, allowing commuters to be aware of the departure of buses, which is what lacks in the earlier air conditioned bus interchanges.


== External links ==
Interchanges and Terminals (SBS Transit)