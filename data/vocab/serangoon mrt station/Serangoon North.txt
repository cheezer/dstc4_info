Serangoon North Village and Estate is a cluster of HDB apartment flats located north of the Serangoon New Town situated in Serangoon Central. It sandwiches between Ang Mo Kio to the west, Serangoon Gardens to the southeast and Hougang to the east.
Serangoon North has neighbourhood precincts of N1 and N5. Neighbourhood Zone 5 was the latest addition to Serangoon North, which is the current Serangoon North Estate that runs through Serangoon North Avenues 3, 4, 5 and 6. It was built in the period of years 1992 to 1994. The rest (Serangoon North Village), were built before the 1990s.
Currently, there are 2 areas of Serangoon North, one constituted by the Workers' Party (WP) under the Aljunied GRC, which is the Serangoon North Village (Serangoon North Avenues 1 and 2). Serangoon North Estate, on the other hand, is under the constitution management of the People's Action Party (PAP) in the Ang Mo Kio GRC.


== Amenities ==
At blocks 151 to 154 in the Serangoon North Village, a shopping area called Central Plaza, was created, which amazingly attracted FairPrice Super supermarket to stay open for 24 hours a day; while Serangoon North Estate is the area where factories sprouted. Ang Mo Kio Industrial Park II forms part of the industrial area too.


== Amenities in Serangoon North Village ==
Sakon Thai Restaurant (Serangoon North Avenue 1)
The Serangoon Community Centre (Serangoon North Avenue 2)
Zong Yi Tian Ming Temple (Serangoon North Avenue 1)
Teng San Tian Hock Temple (Serangoon North Avenue 1)
Hiang Foo San Temple (Serangoon North Avenue 1)
Sri Darma Muneeswaran Temple (Serangoon North Avenue 1)
Al-Istiqamah Mosque (Serangoon North Avenue 2)
Serangoon Garden Secondary School (Serangoon North Avenue 1)


== Amenities in Serangoon North Estate ==
Rosyth School
Hwi Yoh Community Centre (Serangoon North Avenue 4)
Sheng Siong Hypermarket (Serangoon North Avenue 5)
First Centre (Serangoon North Avenue 4)