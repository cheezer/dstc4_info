Serangoon is a town situated in the central-eastern part of the city-state of Singapore, within the North-East Region. The Housing and Development Board housing estate of Serangoon New Town in Serangoon is one of the smaller new towns. Its town centre is known as Serangoon Central, and is the target of extensive future development. The Serangoon Planning Area, an urban planning zone under the Urban Redevelopment Authority, encompasses the entire Serangoon New Town and the private estates at Serangoon Gardens. It lies between Ang Mo Kio and Hougang.
For public transport, Serangoon New Town is served by the Mass Rapid Transit line, at the North-East Line and Circle Line station of Serangoon MRT Station, and public bus services.
Serangoon also refers to the area where Serangoon Road passes through and where is now known as Little India.


== Major roads ==

Serangoon Road, together with Upper Serangoon Road, forms a radial arterial road from the city area of Singapore to the Serangoon, Hougang, Sengkang and Punggol areas. For this arterial road, the section from Selegie Road to MacPherson Road is called Serangoon Road, while the section north of MacPherson Road is called Upper Serangoon Road.
A large part of the North East MRT Line runs in parallel with this arterial road. Stations that are located along this road includes Little India, Farrer Park, Boon Keng, Potong Pasir, Woodleigh, Serangoon and Kovan.


== Etymology and history ==
Tamil Muslim traders from India pioneered the settlement of Serangoon in early 19th century. Serangoon Road was first identified as a bridle path in 1821, and by 1822 it had reached the present Woodsville Corner. A road was proposed in 1822 and by 1827 the road reached the Serangoon River.
It is difficult to say what the word Serangoon actually means. The etymology of the word is uncertain. The most plausible view holds that it was derived from a small marsh bird, the burong ranggong, which was common in the swamps of the Serangoon River (formerly the Rangon River). It had a black back, white breast, long, sharp bill, grey crest, long neck and unwebbed feet.
On early maps of Singapore, the name of the area is called Seranggong. Se is short for satu, or "one", in Malay. An alternative derivation is offered by Haji Sidek, an amateur etymologist interested in Malay place names, who speculates that the name Serangoon is derived from the Malay words diserang dengan gong, which means "to be attacked by gongs and drums". According to Haji Sidek, people used to go to Benut in Johore through the Serangoon area and had to use gongs to frighten off wild animals and snakes which used to roam the jungle covered area. Serang dengan gong gradually became Serangoon over the years.
Siddique and Puru Shotam, however, argue that such as derivation meant that the name developed after the road, which is inconsistent with the fact that the term ranggong predates the development of the road.
In the 1828 Franklin and Jackson's Plan of Singapore, there are three references to Rangung: Tanjong "Rangung", the River "Rangung" and the Island "Rangung". In Coleman's 1836 Map of Singapore, the names Tanjong Rangon and Rangon River can already be found. Rangong in Malay means "warped or shrunken", as of the plane of a wall, roof or decking. It is evident that the road cut from Selegi was named by the early references to "Rangung" in the north-east of the island — one road linking the swamp bird area of ranggong — hence "Serangoon".
For the Chinese, the road is called hou gang lu(後港路) or ow kang, meaning "back of the port road", signifying that one could go to the back of the port in Kallang.

The northern tip of Serangoon Road is known as nan sheng hua yuen pien (南生花園邊), or "fringe of garden in the south", which referred to the Chinese vegetable gardens in the Bendemeer area. This general area was also termed mang chai chiao (feet of the jackfruit) because of the many jackfruit trees which grew there. The Hokkien Chinese name for Serangoon Road was au kang in, meaning "back creek". The Chinese refer to Serangoon Gardens as ang sali.
The main occupants of the area were, however, the Indian community. Indians congregated here from 1826 to work in the brick kilns செங்கல் சூளை and cattle industries situated here. The kilns were discontinued in 1860 and the cattle sheds were removed by the municipality in 1936. The area, however, remains predominantly Indian and today is known as "Little India", லிட்டில் இந்தியா the locus of Singapore Indian retailing, everyday culture and festivities.
The Sri Veeramakaliamman Temple ஸ்ரீ வீரமாகாளியம்மன் கோயில் on Serangoon Road built more than a century ago went under rebuilding for three years (1984–1987) costing S$2.2 million. Consecration ceremony of the new temple took place on 8 February 1987.


== Schools ==
Primary Schools
CHIJ Our Lady of Good Counsel
Rosyth School
St. Gabriel's Primary School
Yangzheng Primary School
Zhonghua Primary School

Secondary Schools
Peicai Secondary School
St. Gabriel's Secondary School
Serangoon Garden Secondary School
Zhonghua Secondary School

Junior Colleges
Nanyang Junior College

Other Schools
Australian International School Singapore
Lycée Français De Singapour


== See also ==
Serangoon North
Serangoon Gardens
Little India
Nex


== Gallery ==


== References ==


== Sources ==
Victor R Savage, Brenda S A Yeoh (2003), Toponymics - A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1


== External links ==
Masterplan 2003 – Urban Redevelopment Authority
Serangoon Planning Report 1995