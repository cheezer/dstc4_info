The Distinguished Service Order (DSO) is a military decoration of the United Kingdom, and formerly of other parts of the Commonwealth of Nations and British Empire, awarded for meritorious or distinguished service by officers of the armed forces during wartime, typically in actual combat.


== Creation ==
Instituted on 6 September 1886 by Queen Victoria in a Royal Warrant published in the London Gazette on 9 November, the first DSOs awarded were dated 25 November 1886. It is typically awarded to officers ranked major (or its equivalent) or higher, but the honour has sometimes been awarded to especially valorous junior officers. During the First World War, 8,981 DSOs were awarded, each award being announced in the London Gazette.
The order was established for rewarding individual instances of meritorious or distinguished service in war. It was a military order, until recently for officers only, and normally given for service under fire or under conditions equivalent to service in actual combat with the enemy, although it was awarded between 1914 and 1916 under circumstances which could not be regarded as under fire (often to staff officers, which caused resentment among front-line officers). After 1 January 1917, commanders in the field were instructed to recommend this award only for those serving under fire. Prior to 1943, the order could be given only to someone mentioned in despatches. The order is generally given to officers in command, above the rank of captain. A number of more junior officers were awarded the DSO, and this was often regarded as an acknowledgement that the officer had only just missed out on the award of the Victoria Cross. In 1942, the award of the DSO was extended to officers of the Merchant Navy who had performed acts of gallantry while under enemy attack.


== Modern era ==
Since 1993, its award has been restricted solely to distinguished service (i.e. leadership and command by any rank), with the Conspicuous Gallantry Cross being introduced as the second highest award for gallantry. It has, however, despite some very fierce campaigns in Iraq and Afghanistan, remained an officers-only award and it has yet to be awarded to a non-commissioned rank.


== Nomenclature ==
Recipients of the order are officially known as Companions of the Distinguished Service Order. They are entitled to use the post-nominal letters "DSO". One or more gold medal bars ornamented by the Crown may be issued to DSO holders performing further acts of such leadership which would have merited award of the DSO. The bars are worn as clasps on the medal ribbon of the original award.


== Description ==
The medal signifying its award is a gold (silver-gilt) cross, enamelled white and edged in gold. In the centre, within a wreath of laurel, enamelled green, is the imperial crown in gold upon a red enamelled background.
On the reverse is the royal cypher in gold upon a red enamelled ground, within a wreath of laurel, enamelled green. A ring at the top of the medal attaches to a ring at the bottom of a gold "suspension" bar, ornamented with laurel. At the top of the ribbon is a second gold bar ornamented with laurel.
The red ribbon is 1.125 in (2.86 cm) wide with narrow blue edges. The medals are issued unnamed but some recipients have had their names engraved on the reverse of the suspension bar.
The bar for an additional award is plain gold with an Imperial Crown in the centre. The back of the bar is engraved with the year of the award. A rosette is worn on the ribbon in undress uniform to signify the award of a bar.


== Notable recipients ==
The following received the DSO and three bars:
Archibald Walter Buckle, rose from being a naval rating in the Royal Naval Volunteer Reserve to command the Anson Battalion of the Royal Naval Division during the First World War.
William Denman Croft, First World War army officer
William Robert Aufrere Dawson, Queen's Own Royal West Kent Regiment during the First World War, wounded nine times and mentioned in despatches four times
Basil Embry, Second World War Royal Air Force officer
Bernard Freyberg, also awarded Victoria Cross
Edward Albert Gibbs, Second World War destroyer captain
Arnold Jackson, 1500 metres Olympic gold medal winner in 1912
Douglas Kendrew, served as a brigade commander in Italy, Greece and the Middle East between 1944 and 1946. Subsequently appointed Governor of Western Australia.
Robert Sinclair Knox, First World War British Army officer.
Frederick William Lumsden, also awarded Victoria Cross
Paddy Mayne, Special Air Service commander and Irish rugby player
Sir Richard George Onslow, Second World War destroyer captain and later admiral
Alastair Pearson, a British Army officer who received his four awards within the space of two years during the Second World War
James Brian Tait, RAF pilot also awarded the DFC and bar, completed 101 bombing missions in the Second World War.
Frederic John Walker, Second World War British Navy captain and U-boat hunter
Edward Allan Wood, First World War army officer


== See also ==
British and Commonwealth orders and decorations
Category:Companions of the Distinguished Service Order


== References ==


== External links ==
UK Cabinet Office, Honours System: Orders of Chivalry
Search recommendations for the DSO on The UK National Archives' website.