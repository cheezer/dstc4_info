Douglas Mitchell "Doug" Pitt (born November 2, 1966) is an American businessman, investor and philanthropist. He is a Goodwill Ambassador for the United Republic of Tanzania, an appointment he has held since April 2010. Pitt is the founder of Care To Learn, a USA based domestic charity that funds emergent health, hunger and hygiene needs for children. His business career started with him founding ServiceWorld Computer Center in April 1991 which merged with TSI Global in July 2013. A large capacity of his work is in East Africa doing water projects as a Board Member of WorldServe International.


== Early life ==
Doug Pitt was born in St. Louis, Missouri; the son of Bill & Jane Pitt. He has a brother, American actor Brad Pitt (3 years older) and a sister, Julie (Pitt) Neal (2 years younger). All three Pitts have projects in Africa, although unrelated. The family grew up in Springfield, Missouri where Doug resides today.


== Business ==
Pitt attended schools in Springfield, MO, graduating from Kickapoo High School (Springfield, Missouri). Pitt graduated from Missouri State University in 1990. In 1991, he founded ServiceWorld Computer Center. In November 2007, Pitt sold a 75% interest in ServiceWorld Computer Center to Miami Nations Enterprises, the Indian Tribe of Miami, Oklahoma. Pitt remained an owner and principal operational partner of ServiceWorld. ServiceWorld is a past recipient of the W. Curtis Strube Small Business of the Year and in 2010 was awarded the SBJ Philanthropic Business of the Year. In 2012, Pitt formed TSI Integrated Services in partnership with TSI Global, a St. Louis corporation. In May 2013, the Tribe of Miami purchased a controlling interest in TSI Global. Pitt and the Tribe merged ServiceWorld into TSI Global in July 2013 where Pitt remains an operational member. As part of the technology family, Pitt had formed Nexus Underground in 2012, which provides cloud based services. Nexus Underground was sold by Pitt to the Miami Tribe and merged into TSI Global in August 2013. As a tribal owned entity, MBS and the Miami tribe were recognized in August 2013 as the 8a Business of the Year.
In 2012, Pitt founded Inventrue, LLC with Jim Zender. Inventrue provides an operational cloud-based web solution for RV dealerships in the USA. Outside of the technology industry, Pitt is a real estate investor and has interests in several real estate projects across the United States. In July 2013, Pitt launched an Innovation and Business Incubation Center in owned retail space in conjunction with the Ozark Chamber of Commerce in Ozark, Missouri. Pitt joined the Board of Directors of Great Southern Bank in January, 2015.


== Philanthropy ==
Pitt has served on many Boards in Missouri starting with Big Brothers Big Sisters in 1995. Since then, some of the others include Make & Wish; Easter Seals; The Red Cross, and is a past Chairman of the Springfield Area Chamber of Commerce. He has also served on many community task forces including Co-Chairing the Homeless Task Force in 2010 and serving on the Ready To Learn child project in 2013.
In 2013, Pitt was named to the Advisory Council of The ONE Organization of Missouri.


== Care to Learn ==
In late 2007, Pitt heard sobering statistics on poverty in Southwest, Missouri, including humiliating stories of what kids were suffering through. Pitt founded Care to Learn to provide emergent funding for child health, hunger & hygiene needs. The fund started in April 2008, and by Fall of 2014, has grown to 19 Chapters and fulfilled over 500,000 individual child needs. Care to Learn is active in advocacy of children’s rights and works to redevelop child supportive programs.The story that particularly bothered most was one of a fifth-grade boy being teased mercilessly because he had to wear his mom’s jeans to school. “I think why it bothers me so much is because it’s so easy to fix,” he says. “And here I couldn’t find that kid. I didn’t know who he was.”


== Projects in Africa ==
Pitt first began his International service with Convoy of Hope, doing hunger missions in Budapest and Portugal. He teamed up with WorldServe International in 2005. Pitt is a current Board Member of WorldServe and helped the organization grow into one of the largest private drilling company in East Africa, via MajiTech, a Tanzanian wholly owned company of WorldServe. Since Pitt’s involvement in 2005, WorldServe has provided clean water to over 1,000,000 Tanzanians. Pitt has worked tirelessly to bring the global challenge challenge of clean water into focus, commenting, "Today 18,000 children will die from preventable diseases like pneumonia or diarrhea simply because they don’t have access to clean water." In 2012, WorldServe brought two drilling rigs online in Kenya serving the refugee crises areas in Dadaab and eastern Kenya. In addition to water wells, WorldServe has also has launched a medical dispensary, sanitation building and three schools in Loibersoit, Tanzania.
In July 2010, Pitt was recognized by President of Tanzania, HE Jakaya Kikwete, in New York City at an event where he was named Tanzanzia’s first ever Goodwill Ambassador for the United Republic of Tanzania. His duties include working with the Ministry of Tourism, Ministry of Water, agriculture, education and wildlife. In 2014, Pitt has partnered with both private and government groups to address anti-poaching measures in Tanzania.
In July 2013, Pitt hosted NFL future Hall of Famer Ray Lewis on his first trip to Tanzania, raising funds for clean water.


== Photography ==
Pitt has been a photographic enthusiast for over 20 years. He has done photo exhibits in galleries in NY, PA, CA, MN and MO, with proceeds going to clean water. His photos have been featured in numerous domestic and international publications.


== Virgin Mobile ==
Pitt became an ad spokesman for Virgin Mobile Australian, portraying The Second Most Famous Pitt, a parody of his comedic likeness as the brother of actor Brad Pitt. The initial web video went viral and garnered over 1,200,000 YouTube hits in its first week. This campaign became the most successful in history for Virgin Mobile becoming the top 5 viewed online ad in the world in its first month. The campaign won the Grand Prix for Effectiveness at Spikes, a Black Lion at Cannes for Effectiveness, Best of Show at AdFest, the Gold Prize at the 12th Annual Creative Business Idea Awards, two Gold AdFest Awards as well as a Gold & Silver Clio and countless Cannes Lions, AWARD, New York Festivals and many other awards. It was the thirteenth most awarded campaign in 2014. The popularity of the campaign got Pitt a spot on the Today Show with host Matt Lauer. A topic of conversation was his response to conservative comments his mother made about President Obama and gay marriage. He responded with “Moms and dads and kids agree to disagree all over the world, so why would our family be any different? There can be healthy discussion when people disagree with you, and I think there should be. The bad thing is when it turns into venom and negativity and we don’t have that in our family. It’s open discussion. We can learn from each other and, if anything, it solidifies your point or maybe you learn something.”


== Biking ==
Pitt is an avid road, mountain bike and urban bicyclist that supports numerous biking organizations. He also works to promote safe biking lanes; city & mountain bike trail systems; and bike health & safety. Pitt hosts biking fundraisers domestically and internationally.
In January 2011, Pitt became the first American on record to climb Mount Kilimanjaro and descend on a mountain bike. Biking is illegal on Kilimanjaro and permission was granted via the Tanzanian government. The ride was sponsored by Trek Bikes, Eddie Bauer/First Ascent and the Hershey Company. As a fundraiser, the event raised $750,000 for clean water projects in Tanzania. Pitt duplicated this effort in March 2013 hosting a mountain biking trip down Kilimanjaro that netted $250,000 for clean water. An additional $100,000 was raised with dual sponsors Trek Bikes, Otter Box, Pentair and Jimmy Johns as Pitt completed the 2012 Leadville 100 Mountain Bike race in Leadville, Colorado; and event he had to complete under the 12-hour deadline to collect the $100,000 for clean water; his time was 10 hours, 22 minutes.


== Recognitions ==
2015 Horace Mann Friend of Education 2015 Award presented by the Missouri NEA
2015 Ingram Magazine "50 Missourians You Should Know"
2014 Edwin P. Hubble Medal of Initiative
2013 Missouri ONE Honorary Advisory Committee Member
2012 Missourian Award Recipient
2012, 2013, 2014 Honorary Chairman – St. Louis World Food Day
2011, Doug was honored with the Humanitarian Leadership Award at the Starkey Foundation Gala with fellow honorees President Bill Clinton, Randy Hogan of Pentair and Actress Marlee Matlin.
2011 Springfield Public Schools Hall of Fame Inductee
2011 DAR Excellence in Community Service Award
2010 Goodwill Ambassador for the United Republic of Tanzania
2010 Community Foundation of the Ozarks Humanitarian of the Year
2010 CASA (SW Missouri) Champion for Children
2010 SBJ Philanthropic Business of the Year (ServiceWorld)
2009 Sertoma Service to Mankind Award Recipient
2006 Rotary (Springfield SE) Volunteer of the Year
2003 Gift of Time Recipient – Civic & Community Betterment
2002 Small Business Person of the Year Nominee – U.S. Small Business Administration
2000 Outstanding Young Alumni Award – S. Missouri State University Alumni Association
1999 W. Curtis Strube Small Business of the Year – Springfield, Missouri Chamber of Commerce
1999 Family of the Year – Prevent Child Abuse of Missouri


== References ==