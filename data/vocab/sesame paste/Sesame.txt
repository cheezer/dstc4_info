Sesame (/ˈsɛsəmiː/; Sesamum indicum) is a flowering plant in the genus Sesamum. Numerous wild relatives occur in Africa and a smaller number in India. It is widely naturalized in tropical regions around the world and is cultivated for its edible seeds, which grow in pods.
Sesame seed is one of the oldest oilseed crops known, domesticated well over 3000 years ago. Sesame has many species, most being wild and native to Sub-Saharan Africa. Sesame Indicum, the cultivated type, originated in India. Sesame is highly tolerant to drought-like conditions and grows where other crops may fail.
Sesame has one of the highest oil contents of any seed. With a rich nutty flavor, it is a common ingredient in cuisines across the world. Like other nuts and foods, it can trigger allergic reactions in some people.
The world harvested about 4.8 million metric tonnes of sesame seeds in 2013. The largest producer of sesame seeds in 2013 was Myanmar. The world's largest exporter of sesame seeds was India, and Japan the largest importer.

It is an annual plant growing 50 to 100 cm (1.6 to 3.3 ft) tall, with opposite leaves 4 to 14 cm (1.6 to 5.5 in) long with an entire margin; they are broad lanceolate, to 5 cm (2 in) broad, at the base of the plant, narrowing to just 1 cm (0.4 in) broad on the flowering stem.
The flowers are yellow, tubular, 3 to 5 cm (1.2 to 2.0 in) long, with a four-lobed mouth. The flowers may vary in colour with some being white, blue or purple.
Sesame fruit is a capsule, normally pubescent, rectangular in section and typically grooved with a short triangular beak. The length of the fruit capsule varies from 2 to 8 cm, its width varies between 0.5 to 2 cm, and the number of loculi from 4 to 12. The fruit naturally splits open (dehisces) to release the seeds by splitting along the septa from top to bottom or by means of two apical pores, depending on the varietal cultivar. The degree of dehiscence is of importance in breeding for mechanised harvesting as is the insertion height of the first capsule.
Sesame seeds are small. The size, form and colours vary with the thousands of varieties now known. Typically, the seeds are about 3 to 4 millimeters long by 2 millimeters wide and 1 millimeter thick. The seeds are ovate, slightly flattened and somewhat thinner at the eye of the seed (hilum) than at the opposite end. The weight of the seeds is between 20 and 40 milligrams. The seed coat (testa) may be smooth or ribbed.
Sesame seeds come in many colours depending on the cultivar harvested. The most traded variety of sesame is off-white coloured. Other common colours are buff, tan, gold, brown, reddish, gray and black.
Sesame seed is sometimes sold with its seed coat removed (decorticated). This is the variety often present on top of buns in developed economies.


== Etymology and languages ==
The word sesame is from Latin sesamum or Greek sésamon "seed or fruit of the sesame plant".
From the roots above, words with the generalized meaning “oil, liquid fat” are derived. Sesame names vary among numerous languages.


== Origins ==
Sesame seed is considered to be the oldest oilseed crop known to humanity. Sesame has many species, and most are wild. Most wild species of the genus Sesamum are native to sub-Saharan Africa. Sesame Indicum, the cultivated type, originated in India.
Charred remains of sesame recovered from archeological excavations have been dated to 3500-3050 BC. Fuller claims trading of sesame between Mesopotamia and the Indian sub-continent occurred by 2000 BC. Some reports claim sesame was cultivated in Egypt during the Ptolemiac period, while others suggest the New Kingdom
Records from Babylon and Assyria, dating about 4000 years ago mention sesame. Egyptians called it sesemt, and it is included in the list of medicinal drugs in the scrolls of the Ebers Papyrus dated to be over 3600 years old. Archeological reports from Turkey indicate that sesame was grown and pressed to extract oil at least 2750 years ago in the empire of Urartu.
The historic origin of sesame was favored by its ability to grow in areas that do not support the growth of other crops. It is also a robust crop that needs little farming support—it grows in drought conditions, in high heat, with residual moisture in soil after monsoons are gone or even when rains fail or when rains are excessive. It was a crop that could be grown by subsistence farmers at the edge of deserts, where no other crops grow. Sesame has been called a survivor crop.


== Cultivation ==

Sesame is very drought-tolerant, in part due to its extensive root system. However, it requires adequate moisture for germination and early growth. While the crop survives drought as well as presence of excess water, the yields are significantly lower in either conditions. Moisture levels before planting and flowering impact yield most.
Most commercial cultivars of sesame are intolerant of water-logging. Rainfall late in the season prolongs growth and increases high harvest-shattering losses. Wind can also cause shattering at harvest.
Initiation of flowering is sensitive to photoperiod and to sesame variety. The photoperiod also impacts the oil content in sesame seed; increased photoperiod increases oil content. The oil content of the seed is inversely proportional to its protein content.
Sesame varieties have adapted to many soil types. The high yielding crops thrive best on well-drained, fertile soils of medium texture and neutral pH. However, these have low tolerance for soils with high salt and water-logged conditions. Commercial sesame crops require 90 to 120 frost free days. Warm conditions above 23 °C (73 °F) favor growth and yields. While sesame crops can grow in poor soils, the best yields come from properly fertilized farms.
Since sesame is a small flat seed, it is difficult to dry it after harvest because the small seed makes movement of air around the seed difficult. Therefore, the seeds need to be harvested as dry as possible and stored at 6 percent moisture or less. If the seed is too moist, it can quickly heat up and become rancid.


=== Processing ===
After harvesting, the seeds are usually cleaned and hulled. In some countries, once the seeds have been hulled, they are passed through an electronic colour-sorting machine that rejects any discolored seeds to ensure perfectly coloured sesame seeds. This is done because sesame seeds with consistent appearance are perceived to be of better quality by consumers, and sell for a higher price. Immature or off-sized seeds are removed and used for oil production.


== Production and trade ==
In 2013, the global harvest from an aggregate of 9.4 million hectares was about 4.8 million metric tonnes of sesame seeds, with Myanmar, India and China as the top three producers accounting for 44 percent of global production (table). In total for 2013, Asia produced 2.46 million metric tons, the most of any continent.
The most productive sesame seed farms in the world in 2013 were in the European Union with an average composite yield of 6.3 metric tonnes per hectare (compared to the global average of 0.5); Italy reported the highest nationwide yield of 8.5 metric tonnes per hectare. There is a large yield gap and farm loss differences between major sesame seed producers, in part because of knowledge gap, poor crop management practices and use of technologies.
The white and other lighter coloured sesame seeds are common in Europe, the Americas, West Asia, and the Indian subcontinent. The black and darker coloured sesame seeds are mostly produced in China and southeast Asia. Africa produces a variety of sesame seeds.

Beginning in the 1950s, U.S. production of the crop has been largely centered in Texas, with acreage fluctuating between 10,000 to 20,000 acres (40 to 80 km2) in recent years. The country's crop does not make up a significant global source; indeed imports have now outstripped domestic production.


=== Trade ===
The world traded over a billion dollars worth of sesame seeds in 2010. The trade volume has been increasing rapidly in the last two decades.
Japan is the world's largest sesame importer. Sesame oil, particularly from roasted seed, is an important component of Japanese cooking and traditionally the principal use of the seed. China is the second largest importer of sesame, mostly oil-grade sesame. China exports lower priced food grade sesame seeds, particularly to southeast Asia. Other major importers are the United States, Canada, Netherlands, Turkey and France.
Sesame seed is a high value cash crop. Sesame prices have ranged between US$800 to 1700 per metric ton between 2008 and 2010.
Sesame exports sell across a wide price range. Quality perception, particularly how the seed looks, is a major pricing factor. Most importers who supply ingredient distributors and oil processors only want to purchase scientifically treated, properly cleaned, washed, dried, colour-sorted, size-graded and impurity-free seeds with a guaranteed minimum oil content (not less than 40 percent) packed according to international standards. Seeds that do not meet these quality standards are considered unfit for export and are consumed locally. In 2008, by volume, by premium prices and by quality, the largest exporter was India, followed by Ethiopia and Myanmar.


== Nutritional information ==

For a 100 gram serving, sesame seeds (whether roasted or just dried) are rich in calories (565 kcal), providing an excellent source of essential nutrients as part of the Daily Value (DV, tables). While containing high amounts of protein, dietary fiber and total fat (mainly as linoleic acid and oleic acid), sesame seeds are also particularly rich (> 20% DV) in B vitamins and the dietary minerals manganese, magnesium, calcium, iron and zinc (table).
The flour that remains after oil extraction from sesame seeds is 35-50 percent protein and contains carbohydrates. This flour, also called sesame meal, is an excellent high-protein feed for poultry and other livestock.


== Chemical composition ==
Sesame seeds contain the lignans, sesamolin, sesamin, pinoresinol and lariciresinol.


== Cuisine ==

Sesame seed is a common ingredient in various cuisines. It is used whole in cooking for its rich nutty flavour. Sesame seeds are sometimes added to breads, including bagels and the tops of hamburger buns. Sesame seeds may be baked into crackers, often in the form of sticks. In Sicily and France, the seeds are eaten on bread (called "ficelle sésame", sesame thread). In Greece the seeds are also used in cakes.
Fast-food restaurants use buns with tops sprinkled with sesame seeds. Approximately 75% of Mexico's sesame crop is purchased by McDonald's for use in their sesame seed buns worldwide.
In Asia, sesame seeds are sprinkled onto some sushi style foods. In Japan whole seeds are found in many salads and baked snacks and tan and black sesame seed varieties are roasted and used to make the flavouring gomashio. East Asian cuisines, like Chinese cuisine use sesame seeds and oil in some dishes, such as dim sum, sesame seed balls (Chinese: 麻 糰; pinyin: mátuǎn or 煎堆; Cantonese: jin deui), and the Vietnamese bánh rán. Sesame flavour (through oil and roasted or raw seeds) is also very popular in Korean cuisine, used to marinate meat and vegetables. Chefs in tempura restaurants blend sesame and cottonseed oil for deep-frying.
Sesame, or "simsim" as it is known in East Africa, is used in African cuisine. In Togo the seeds are a main soup ingredient and in the Democratic Republic of the Congo and in the north of Angola, wangila is a delicious dish of ground sesame, often served with smoked fish or lobster.
Sesame seeds and oil are used extensively in India. In most parts of the country, sesame seeds mixed with heated jaggery, sugar or palm sugar is made into balls and bars similar to peanut brittle or nut clusters and eaten as snacks. In Manipur (India) black sesame is used in the preparation of Thoiding and in Singju (a kind of salad). Thoiding is prepared with ginger and chili and vegetables are used in the spicy Singu dish. In Assam, black sesame seeds are used to make Til Pitha and Tilor laru (sesame seed balls) during bihu. In Punjab and Tamil Nadu (both in India), a sweet ball called "Pinni" (پنی) in Urdu and 'Ell urundai' in Tamil, "Ellunda"(എള്ളുണ്ട) in Malayalam, "Yellunde" (sesame ball, usually in jaggery) in Kannada and tilgul in Marathi is made of its seeds mixed with sugar. It is eaten in various forms during the festival of Makar Sankranti.
Also in Tamil Nadu, sesame oil used extensively in their cuisine, Milagai Podi, a ground powder made of sesame and dry chili is used to enhance flavor, and is consumed along with other traditional foods such as idli. In Tamil Nadu and Andhra Pradesh, sesame oil is used as a preservative, as well as to temper the heat of their spicy foods, pickles and condiments.
Sesame seed cookies and wafers, both sweet and savory, are popular in places like Charleston, South Carolina. Sesame seeds, also called benne, are believed to have been brought into 17th century colonial America by West African slaves. Since then, they have become part of various American cuisines.
In Caribbean cuisine, sugar and white sesame seeds are combined into a bar resembling peanut brittle and sold in stores and street corners.
Sesame is a popular and essential ingredient in many Middle Eastern cuisines. Sesame seeds are made into a paste called tahini (used in various ways, including hummus bi tahini) and the Middle Eastern confection halvah. Ground and processed, the seeds is also used in sweet confections.
In South Asia, Middle East, East Asian cuisines, popular confectionery are made from sesame mixed with honey or syrup and roasted into a sesame candy. In Japanese cuisine goma-dofu (胡麻豆腐) is made from sesame paste (Tahini) and starch.
Mexican cuisine refers to sesame seeds as Ajonjolí. It is mainly used as a sauce additive, such as mole or adobo. It is often also used to sprinkle over artisan breads and baked in traditional form to coat the smooth dough, especially on whole wheat flat breads or artisan nutrition bars, such as alegrías.
In Sicilian cuisine, what are commonly called "Italian sesame seed cookies" are known as giuggiuleni (plural, pronounced ju-ju-LAY-nee). A giuggiulena (singular) usually refers to a cookie, while a giurgiulena usually refers to a nougat-like candy, often made as a Christmas food. Both are alternative spellings for "sesame seed" in the Sicilian language.
Sesame oil is sometimes used as a cooking oil in different parts of the world, though different forms have different characteristics for high-temperature frying. The "toasted" form of the oil (as distinguished from the "cold-pressed" form) has a distinctive pleasant aroma and taste, and is used as table condiment in some regions, especially in East Asia. Toasted sesame oil is also added to flavor soups and other hot dishes, usually just before serving, to avoid dissipating the volatile scents too rapidly.
Although sesame leaves are edible as a leaf vegetable, recipes for Korean cuisine calling for "sesame leaves" are often a mistranslation, and really mean perilla.


== Allergy ==
Sesame seeds and sesame oil are a serious allergen to some people including infants. In Australia the occurrence of allergy to sesame seed was estimated to be 0.42 percent among all children, while in the United Kingdom the allergic reaction was found to affect 0.04 percent of adults. The occurrence of allergy to sesame in patients with some form of food allergy was found to be much higher than in the general population, ranging from 0.5 percent in Switzerland to 8.5 percent in Australia. In other words, allergy to sesame affects a small percentage of overall human population, but sesame allergy is high in people who already show symptoms of allergy to other foods.
The symptoms of sesame seed allergy can be classified into:
Systemic reactions: Primarily presenting anaphylaxis characterized by symptoms including hives (urticaria), lip and eyelid swelling (angioedema ) sneezing, nasal itching, congestion, rhinorrhea, wheezing, cough, tightness of throat, hoarse voice, difficulty in breathing, abdominal pain, unconsciousness, shock with drop of blood pressure. In the systemic reactions can also be included severe reactions like dizziness, drowsiness, chills and collapse as has been reported in patients after ingestion of a falafel burger.
Other symptoms: Facial or generalized redness ("flushing"), hives (urticaria) on smaller or larger parts of the body, swelling of the eyelids, lips or other parts of the face, itching of the eyes or of the skin in general, hay fever symptoms in the eyes and eczema. Respiratory symptoms observed include hay fever, asthma, cough, wheeze, or difficulty in breathing. Gastrointestinal symptoms: Itching in the mouth and/or tongue soon after chewing and ingesting (Oral allergy syndrome) and abdominal pain.
Amounts as low as 100 mg of sesame seeds or flour and 3 ml of oil can trigger allergic reactions in severe cases of sesame allergic individuals. Most patients, however, show allergic reactions after consuming 2–10 grams of sesame seeds or flour. The onset of the symptoms may occur within a few minutes up to 90 minutes after ingestion of a sesame seed product. Most patients had other allergic diseases such as asthma, hay fever, and eczema, and most patients also had a relative with an allergic disease. More than two thirds of the patients with sesame allergy also had food allergic reactions to other foods.

Prevalence of sesame allergy varies per country. While it is one of the three most common allergens in Israel, sesame allergy prevalence is considered small relative to other allergens in the United States. Some experts consider sesame allergies to have "increased more than any other type of food allergy over the past 10 to 20 years" in the United States. Such increasing prevalence led Canada to issue regulations that require food labels to note the presence of sesame.
In addition to products derived from sesame such as tahini and sesame oil, persons with sesame allergies are warned to stay away from a broad assortment of processed foods including baked goods, tempeh, and generic "vegetable oil." In addition to possible food sources, individuals allergic to sesame have been warned that a variety of non-food sources may also trigger a reaction to sesame, including adhesive bandages, cosmetics, hair care products, perfumes, soaps and sunscreens, drugs, some fungicides and insecticides, lubricants, ointments and topical oils, and pet food.
At least one study found that "standard skin and blood testing for food allergies doesn’t predict whether a child has true sesame allergy." In which case, a food challenge under the direction of a physician may be required to properly diagnose a sesame allergy.
There appears to be cross-reactivity between sesame allergens and peanut, rye, kiwifruit, poppy seed, and various tree nuts (such as hazelnut, black walnut, cashew, macadamia and pistachio).


== Literature ==

In myths, the opening of the capsule releases the treasure of sesame seeds, as applied in the story of "Ali Baba and the Forty Thieves" when the phrase "Open Sesame" magically opens a sealed cave. Upon ripening, sesame seeds split, releasing a pop and possibly indicating the origin of this phrase.
Sesame seeds are used conceptually in Urdu literature, in the proverbs "til dharnay ki jagah na hona", meaning a place so crowded that there is no room for a single seed of sesame, and "in tilon mein teil nahee", referring to a person who appears to be useful but is not (selfish) when the time for need comes, literally meaning "there is no oil (left) in this sesame."


== Gallery ==


== See also ==
Za'atar


== References ==


== External links ==
 Data related to Sesamum indicum at Wikispecies