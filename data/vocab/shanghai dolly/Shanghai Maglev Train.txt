The Shanghai Maglev Train or Shanghai Transrapid (simplified Chinese: 上海磁浮示范运营线; traditional Chinese: 上海磁浮示範運營線; pinyin: Shànghǎi Cífú Shìfàn Yùnyíng Xiàn; literally: "Shanghai Maglev Demonstration Operation Line") is a magnetic levitation train, or maglev line that operates in Shanghai, China. The line is the first commercially operated high-speed magnetic levitation line in the world.
Construction of the line began in March 1, 2001, and public commercial service commenced on 1 January 2004. The top operational commercial speed of this train is 431 km/h (268 mph), making it the world's fastest train in regular commercial service since its opening in April 2004. During a non-commercial test run on 12 November 2003, a maglev train achieved a Chinese record speed of 501 km/h (311 mph). The Shanghai Maglev has a length of 153m, a width of 3.7m, a height of 4.2m and a three- class, 574- passenger configuration.
The train line was designed to connect Shanghai Pudong International Airport and the outskirts of central Pudong where passengers could interchange to the Shanghai Metro to continue their trip to the city center. It cost $1.2 billion to build. The train set was built by a joint venture of Siemens and ThyssenKrupp in Kassel. The track (guideway) was built by local Chinese companies who, as a result of the alluvial soil conditions of the Pudong area, had to deviate from the original track design of one supporting column every 50 meters to one column every 25 meters, to ensure that the guideway meets the stability and precision criteria. Several thousand concrete piles were driven to depths up to 70 meters to attain stability for the support column foundations. A mile-long, climate controlled facility was built alongside the line's right of way to manufacture the guideways.
The electrification of the train was developed by Vahle, Inc. Two commercial maglev systems predated the Shanghai system: the Birmingham Maglev in the United Kingdom and the Berlin M-Bahn. Both were low-speed operations and closed before the opening of the Shanghai Maglev Train.
The train was inaugurated in 2002 by the German chancellor, Gerhard Schröder, and the Chinese premier, Zhu Rongji.
The line is not a part of the Shanghai Metro network, which operates its own service to Pudong Airport from central Shanghai and from Longyang Road Station.


== Background ==

The line runs from Longyang Road station in Pudong to Pudong International Airport; The Pudong International Airport station provides a transfer to Line 2, but the Longyang Road station provides access to Line 2 and Line 7. At full speed, the journey takes 7 minutes and 20 seconds to complete the distance of 30 km (18.6 mi), although some trains in the early morning and late afternoon take about 50 seconds longer. A train can reach 350 km/h (217 mph) in 2 minutes, with the maximum normal operation speed of 431 km/h (268 mph) reached thereafter.
Hans-Dieter Bott, vice president of Siemens that won the contract to build the rail link, stated that "Transrapid views the Shanghai line, where the ride will last just eight minutes, largely as a sales tool. This serves as a demonstration for China to show that this works and can be used for longer distances, such as Shanghai to Beijing". However, the decision was eventually made to implement the Beijing–Shanghai High-Speed Railway with conventional high-speed technology. Plans for a shorter maglev extension from Longyang Road to Hangzhou, the Shanghai–Hangzhou Maglev Line, have been suspended.
Speculation that a line would be built from Shanghai to Beijing mounted in 2002. It would cover a distance of 800 miles, at an estimated cost of £15.5bn. The chief executive of ThyssenKrupp, Dr Ekkehard Schulz said he was certain that not only Germany, but many countries would follow the Chinese example. The German government along with a selection of German companies sought to win more projects for their maglev technology, and highlighted that a train between Shanghai and the Chinese capital, Beijing remained a possibility. However, no projects have been revealed as of 2014.


== Operation ==

The line is operated by Shanghai Maglev Transportation Development Co., Ltd and runs from 06:45 to 21:30, with services every 15 to 20 minutes. A one-way ticket costs ¥50 (US$8), or ¥40 ($6.40) for those passengers holding a receipt or proof of an airline ticket purchase. A round-trip return ticket costs ¥80 ($12.80) and VIP tickets cost double the standard fare.
Following the opening, overall maglev train ridership levels were at 20% of capacity. The levels were attributed to limited operating hours, the short length of the line, high ticket prices and that it terminates at Longyang Road in Pudong – another 20 min by subway from the city centre.
Travel time was sped up considerably, as the 30km (19m) journey takes 45 minutes by road.


=== Stations ===


=== Operating Costs ===
A 2007 statement by Transrapid USA said with 4 million passengers in 2006 the system was able to cover its operating costs. The ratio of costs were given as: 64%-energy, 19%-maintenance & 17%-operations/support services; no amount was given. The high proportion of energy costs was attributed to the short trip time and high operating speed.


== Construction ==

The Shanghai Transrapid project took ¥10 billion (US$1.33bn) and two and a half years to complete. The line is 30.5 km (18.95 mi) track and has a further separate track leading to a maintenance facility.


=== Extensions ===

In January 2006, the Shanghai–Hangzhou Maglev Line extension project was proposed by the Shanghai Urban Planning Administrative Bureau. The extension would continue the existing line towards Shanghai Hongqiao International Airport, running via Shanghai South Railway Station and the Expo 2010 site, with a possible continuation towards Hangzhou. The extension would allow transferring between the two airports—located 55 km (34 mi) apart—in approximately 15 minutes.
The plan for the extension to Hangzhou was first approved by the central government in February 2006, with a planned date of completion in 2010. Work was suspended in 2008, owing to public protests over radiation fears despite an environmental assessment by the Shanghai Academy of Environmental Sciences saying the line was safe and would not affect air and water quality, and noise pollution could be controlled. According to China Daily, as reported on People's Daily Online 27 February 2009, the Shanghai municipal government was considering building the maglev line underground to allay the public's fear of electromagnetic pollution and the final decision on the maglev line had to be approved by the National Development and Reform commission.
Another approval was granted in March 2010, with construction to begin in late 2010. The new link was to be 199.5 km (124 mi) long, 24 km (15 mi) longer than the original plan. The top speed is expected to be 450 km/h (280 mph) but limited to 200 km/h (124 mph) in built-up areas.
However, in October 2010 the non-maglev Shanghai–Hangzhou High-Speed Railway was opened, bringing travelling time between the two cities down to 45 minutes. Plans for a Maglev link have been suspended again.


=== Incidents ===
On 11 August 2006, a Maglev train compartment caught fire at 2:40 p.m., after having left Pudong International Airport in the direction of Pudong Longyang Road Station. There were no injuries aboard. Preliminary reports indicated that an electrical problem may have been the cause.


=== Technology transfer ===
As part of the contract, Transrapid agreed to a limited technology transfer with China. The first Chinese-built four-car vehicle containing German core components and technology went into service in January 2011.


== See also ==

Magnetic levitation
Maglev
Transrapid
Beijing S1 Maglev
Incheon Airport Maglev
Linimo
Chuo Shinkansen


== References ==


== External links ==
Official website
International Maglevboard Shanghai