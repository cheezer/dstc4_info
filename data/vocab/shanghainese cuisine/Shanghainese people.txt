The Shanghainese (Chinese: 上海人, p Shànghǎirén; Shanghainese: Zaanhening, [zɑ̃.hé.ɲɪɲ]) are the people of Shanghai, China.
The Old City of Shanghai was a minor settlement until the later Qing Dynasty and many districts of the present municipality of Shanghai originally had separate identities, including separate dialects of Taihu Wu. In recent decades, millions of Chinese have moved to the city, both as internal immigrants and as migrant workers. The 2010 Chinese census found 9 million of Shanghai's 23 million residents (almost 40%) were migrants without a Shanghai hukou, triple the number from the year 2000 census. These "New Shanghainese" (新上海人) are generally distinguished from the Shanghainese proper.
The term "Shanghainese" may thus apply to several different groups of varying exclusivity. Legally, it refers to those holding a hukou for one of the local governments in the municipality of Shanghai. Culturally, it most often means those who consider Shanghai to be their hometown, although this is sometimes restricted to those in the central districts or who speak the Shanghainese dialects of those districts (as opposed, for example, to the unintelligible subdialects in Jinshan).


== Shanghainese diaspora ==

Although Shanghai was long a cosmopolitan city as one of China's treaty ports, it was not connected with the large-scale emigration seen in Fujian and Guangdong. Maritime commerce did, however, create a Shanghainese community in Hong Kong. These Shanghainese or their forebears fled Mainland China prior to its occupation by the Communists in 1949. Some actors on the TVB network, a television network based in Hong Kong, are originally from Shanghai, such as Liza Wang, Tracy Ip, and Natalie Tong.
More recently, appreciable numbers of Shanghainese have migrated to other countries. There is a significant Shanghainese community in Sydney, Australia, particularly the suburbs of Ashfield and Burwood. Less prominent communities exist in the Chinatowns of other large metropolitan areas such as New York and Los Angeles in the United States.


== Notable People ==
Shanghai is home to many outstanding and well known athletes such as Yao Ming , Liu Xiang , Wang Liqin , Wang Yihan


== See also ==

Han Chinese
Demographics of Shanghai


== References ==