Shangri-La is a large, dark region of Saturn's moon Titan at 10°S 165°W. It is named after Shangri-La, the mythical paradise in Tibet. It is thought to be an immense plain of dark material. It is thought that these regions of Titan were seas, but that they are now dry.
Shangri-La is studded with bright 'islands' of higher ground. It is bounded by the larger regions of high ground: Xanadu to the east, Adiri to the west, and Dilmun to the north.
The Huygens probe landed on a westerly part of Shangri-La, close to the boundary with Adiri.


== References ==