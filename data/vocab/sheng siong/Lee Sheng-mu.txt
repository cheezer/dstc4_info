Lee Sheng-mu (traditional Chinese: 李勝木; simplified Chinese: 李胜木; pinyin: Lǐ Shèngmù; born October 3, 1986 in Taipei) is a badminton player from Taiwan. He competed at the 2010 and 2014 Asian Games.


== Career ==
Lee Sheng-mu's elite career began in the 2009 badminton season when he reached the semifinals of the 2009 Korea Open Super Series in Men's Doubles with Fang Chieh-min. The pair continued their success in 2010 with victories at the 2010 Singapore Super Series and the 2010 Indonesia Super Series.
Lee is also partnered with Chien Yu-chin in Mixed Doubles. Their top result came in 2010 when they reached the semifinals of the 2010 Paris World Championships.


== Record against selected opponents ==
Men's Doubles results with Fang Chieh-min against Super Series finalists, Worlds Semi-finalists, and Olympic quarterfinalists.


== References ==