YI Sheng (born June 13, 1972 in Beijing, China) is an official of the Chinese Baseball Association who is most notable for coaching the China national baseball team in the World Baseball Classics. He has also coached for the Beijing Tigers. Furthermore, he has coached Team China in the 2002 Asia Games, 2005 Asian Championship.


== References ==