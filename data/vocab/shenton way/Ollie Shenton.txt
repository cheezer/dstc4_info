Oliver "Ollie" Shenton (born 6 November 1997) is an English footballer who plays as a midfielder for Stoke City.


== Career ==
Shenton was born in Blythe Bridge and joined the Stoke City Academy in 2004. At 15 Shenton began playing regularly with the under-18s side and he signed a long-term contract with the club in September 2013. In April 2014 he won the Generation Adidas Cup with Stoke City U17s. He signed on scholarship forms in July 2014. In 2014–15 pre-season Shenton began training with the first-team and played 30 minutes in a 0–0 draw with Burton Albion. He began the 2014–15 season playing with the under-21s and scored on his debut in a 1–1 draw with Aston Villa.
He made his professional debut on 27 August 2014, coming on as an 81st-minute substitute for Peter Odemwingie in a 3–0 win over Portsmouth in the Football League Cup second round. After making his senior debut Shenton earned praise from assistant manager Mark Bowen for his performance. On 25 November 2014 Shenton signed a professional contract with Stoke. Shenton made his Premier League debut on 11 February 2015, coming on as an added-time substitute for Steven N'Zonzi in a 4–1 home defeat against Manchester City. The match came two days after the funeral of his mother; following the game Shenton swapped shirts with Frank Lampard.


== Personal life ==
Shenton attended Blythe Bridge High School and grew up supporting Stoke City. His mother, Mandy, died of cancer in January 2015.


== Career statistics ==
As of match played 11 February 2015


== References ==


== External links ==
Ollie Shenton career statistics at Soccerbase