Le Centre Sheraton Hotel is a skyscraper hotel in Montreal, Quebec, Canada. It is located at 1201 René Lévesque Boulevard West in Downtown Montreal, between Stanley Street and Drummond Street.
Le Centre Sheraton has 825 rooms and stands 118 metres (387 ft) tall with 38 floors. It was built by Le Group Arcorp and was completed in 1982.
Baseball hall of famer Don Drysdale died in room 2518 on July 3, 1993. It hosted a meeting of G-20 finance ministers and central bank governors on October 24-25, 2000.


== §Notes ==


== §External links ==
Le Centre Sheraton (official website)