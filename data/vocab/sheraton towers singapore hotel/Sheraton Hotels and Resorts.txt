Sheraton Hotels and Resorts is Starwood Hotels and Resorts Worldwide's largest and second oldest brand (Westin being the oldest).


== History ==
The origins of the brand date back to 1937 when Ernest Henderson and Robert Moore acquired the Stonehaven Hotel in Springfield, Massachusetts. The chain got its name from another early hotel that the pair had acquired, which had a lighted sign on the roof saying "Sheraton Hotel" which was large and heavy and therefore too expensive to change. Instead, they decided to call all their hotels by that name.
Henderson and Moore had opened three hotels in Boston by 1939, continuing with their rapid expansion opening properties along the entire East Coast. In 1945, it was the first hotel chain to be listed on the New York Stock Exchange.
In 1949 Sheraton expanded internationally with the purchase of two Canadian hotel chains. In 1956, Sheraton purchased the Eppley Hotel Company, which was then the largest privately held hotel business in the United States, for $30 million. Three years later, in 1959 it purchased the four hotels owned by the Matson Lines in Honolulu, Hawaii, its first hotels outside North America. In 1955, Sheraton began to build large highway hotels (100-300 rooms); in 1962 a franchise division was created to promote Sheraton Motor Inns. These provided free parking and competed with roadside motels.
The 1960s saw the first Sheraton hotels outside the US and Canada with the opening of the Tel Aviv-Sheraton in Israel in February 1961 and the Macuto-Sheraton outside Caracas, Venezuela, in 1963. By 1965, the 100th Sheraton Motor Inn had opened its doors. The multinational conglomerate ITT purchased the chain in 1968, after which it was known as ITT Sheraton. The chain deployed its automated Reservatron system and, in late 1969, a US national toll-free number displaced two hundred local Sheraton reservation numbers.
In 1985, Sheraton became the first western company to operate a hotel in the People's Republic of China, assuming management of the state-built Great Wall Hotel in Beijing, which became the Great Wall Sheraton.
In 1994, ITT Sheraton purchased a controlling interest in the Italian CIGA chain, the Compagnia Italiana Grandi Alberghi, or Italian Grand Hotels Company, which had been seized from its previous owner, the Aga Khan, by its creditors. The chain had begun by operating hotels in Italy, but over-expanded across Europe just as a recession hit. These hotels formed the core of what came to be the ITT Sheraton Luxury group, later Starwood's Luxury Collection.
In April 1995, Sheraton introduced a new, mid-scale hotel brand Four Points by Sheraton Hotels, to replace the designation of certain hotels as Sheraton Inns. In 1998, Starwood Hotels & Resorts Worldwide, Inc. acquired ITT Sheraton, outbidding Hilton. Under Starwood's leadership, Sheraton has begun renovating many existing hotels and expanding the brand's footprint.
From 2003 until 2013, Sheraton sponsored the Hawaii Bowl.


== Specific hotels ==
In the 1940s, Sheraton purchased the famous Hotel Kimball of Springfield, Massachusetts, and transformed the 4-star hotel into The Sheraton-Kimball Hotel, attracting guests like President John F. Kennedy.


== Gallery ==
Sheraton Hotels and Resorts


== See also ==
Eppley Hotel Company
Sheraton on the Falls
Sheraton Hawaii Bowl


== References ==


== External links ==
Sheraton Hotels - Official Site
Starwood Hotels - Official Site
Sheraton - Hotels Directory