Hietalahti shipyard (also known as Helsinki New Shipyard, Finnish: Helsingin uusi telakka) is a shipyard in Hietalahti, in downtown Helsinki, Finland. It is operated by Arctech Helsinki Shipyard, a joint venture between STX Finland Cruise Oy and United Shipbuilding Corporation.


== History ==
The shipyard, previously known as Hietalahden Sulkutelakka ja Konepaja Oy (Swedish: Sandvikens Skeppsdocka och Mekaniska Verkstads Ab), was founded in 1865 and delivered its first ship in 1868. It also constructed horse-drawn trams and railroad cars. Wärtsilä bought the parent company Kone ja Silta in 1930's; it included also the Crichton-Vulcan shipyard in Turku. In 1965 the yard was renamed Wärtsilä Helsingin Telakka (Wärtsilä Helsinki Shipyard).
After the bankruptcy of Wärtsilä Marine in 1989 the yards were operated by the newly formed Masa Yards, bought by the Norwegian Kværner group in the mid 1990s and known as Kvaerner Masa Yards. In 2005 the company merged with the Aker Finnyards shipyard in Rauma and was renamed Aker Yards in 2006. In 2008 Aker Yards was acquired by STX Europe.
The shipyard and the cluster of cooperating companies have a strong know-how in shipbuilding for Arctic conditions; 60% of the icebreakers of the world are built in Helsinki.


== See also ==
List of ships built at Hietalahti shipyard


== References ==