The Norfolk Naval Shipyard, often called the Norfolk Navy Yard and abbreviated as NNSY, is a U.S. Navy facility in Portsmouth, Virginia, for building, remodeling, and repairing the Navy's ships. It's the oldest and largest industrial facility that belongs to the U.S. Navy as well as the most multifaceted. Located on the Elizabeth River, the yard is just a short distance upriver from its mouth at Hampton Roads.
It was established as Gosport Shipyard in 1767. Destroyed during the American Revolutionary War, it was rebuilt and became home to the first operational drydock in the United States in the 1820s. Changing hands during the American Civil War, it served the Confederate States Navy until it was again destroyed in 1862, when it was given its current name. The shipyard was again rebuilt, and has continued operation through the present day.


== History ==


=== British control ===
The Gosport Shipyard was founded on November 1, 1767 by Andrew Sprowle on the western shore of the Elizabeth River in Norfolk County in the Virginia Colony. This shipyard became a prosperous naval and merchant facility for the British Crown. In 1775, at the beginning of the American Revolution, Sprowle stayed loyal to the Crown and fled Virginia, which confiscated all of his properties, including the shipyard. In 1779, while the newly formed Commonwealth of Virginia was operating the shipyard, it was burned by British troops.


=== American control ===
In 1794, United States Congress passed "An Act to Provide a Naval Armament," allowing the Federal Government to lease the Gosport Shipyard from Virginia. In 1799 the keel of USS Chesapeake, one of the first six frigates authorized by Congress, was laid, making her the first ship built in Gosport for the U.S. Navy.
The federal government purchased the shipyard from Virginia in 1801 for $12,000. This tract of land measured 16 acres (65,000 m²) and now makes up the northeastern corner of the current shipyard. In 1827, construction began on the first of what would be the first two dry docks in the United States. The first one was completed three weeks ahead of similar projects in both Boston, Massachusetts and South America, making it the first functional dry dock in the Americas. Dry Dock One, as it is referred to today, is still operational and is listed as historical landmark in Portsmouth, VA. Officer's Quarters A, B, and C were built about 1837. Additional land on the eastern side of the Elizabeth River was purchased in 1845.
The shipyard and neighboring towns suffered from a severe yellow fever epidemic in 1855, which killed about a quarter of the population, including James Chisholm, whose account was published shortly after his death in the epidemic.


=== American Civil War ===

In 1861, Virginia joined the Confederate States of America. Fearing that the Confederacy would take control of the facility, the shipyard commander Charles Stewart McCauley ordered the burning of the shipyard. The Confederate forces did in fact take over the shipyard, and did so without armed conflict through an elaborate ruse orchestrated by civilian railroad builder William Mahone (then President of the Norfolk and Petersburg Railroad and soon to become a famous Confederate officer). The capture of the shipyard allowed a tremendous amount of war material to fall into Confederate hands. 1,195 heavy guns were taken for the defense of the Confederacy, and employed in many areas from Hampton Roads all the way to Fort Donelson Tennessee, Port Hudson, and Fort de Russy, Louisiana. The Union forces withdrew to Fort Monroe across Hampton Roads, which was the only land in the area which remained under Union control.
In early 1862, the Confederate ironclad warship CSS Virginia was rebuilt using the burned-out hulk of USS Merrimack. In the haste to abandon the shipyard, the Merrimack had only been destroyed above the waterline, and an innovative armored superstructure was built upon the remaining portion. The Virginia, which was still called the Merrimack by Union forces and in many historical accounts, sank the USS Cumberland, the USS Congress, and engaged the Union ironclad USS Monitor in the famous Battle of Hampton Roads during the Union blockade of Hampton Roads. The Confederates burned the shipyard again when they left in May 1862.
Following its recapture of Norfolk and Portsmouth (and the shipyard) by the Union forces, the name of the shipyard was changed to Norfolk after the county in which it was located, outside the city limits of Portsmouth at the time. This choice of name was probably to minimize any confusion with the pre-existing Portsmouth Naval Shipyard in Kittery, Maine near Portsmouth, New Hampshire.


=== Modern shipyard ===

From the Reconstruction Era until 1917, the shipyard was used both for ship repair and construction and for ship stationing; the current major naval base for the region, Naval Station Norfolk, did not yet exist. As such, the then Norfolk Navy Yard served as the official Homeport for ships stationed in the Hampton Roads region.
No major expansion occurred at the facility until World War I when it was expanded to accommodate 11,000 employees and their families. The shipyard was again expanded in World War II, doubling its physical size, and greatly expanding its productive capacity. During its peak, from 1940 to 1945, 43,000 personnel were employed and 6,850 vessels were built.
After World War II, the shipyard shifted from being a ship construction facility to an overhaul and repair facility. Work on the Iowa-class battleship, USS Kentucky (BB-66) was suspended in 1950. Its last two ships, USS Bold and her sister ship, Bulwark, wooden minesweepers, were christened on March 28, 1953 during the Korean War.
Currently, the shipyard is composed of several noncontiguous areas totaling 1,275 acres (5.2 km²). Norfolk Naval Shipyard provides repair and modernization services for every type of ship that the U.S. Navy has in service, which includes amphibious vessels, submarines, guided missile cruisers, and supercarriers, although in recent years the shipyard has primarily focused on nuclear ships and nuclear support ships. The Norfolk yard is one of the few facilities on the east coast capable of dry docking nuclear aircraft carriers. Another facility capable of drydocking such carriers is Huntington Ingalls Industries (HII), located on the other side of Hampton Roads in Newport News, which is the only U.S. shipyard that currently builds and refuels nuclear aircraft carriers.
Captain William Kiestler, commanding officer of Norfolk Naval Shipyard was relieved of duty on July 1, 2010 by order of Vice Admiral Kevin M. McCoy, commander of Naval Sea Systems Command, after a year on the job because of a loss of confidence in his ability to command.
Captain Greg Thomas was permanently relieved of command on October 26, 2011. Rear Admiral Joseph Campbell held the post as acting shipyard commander until February 16, 2012 when the command was assumed by Captain Mark Bridenstine.


== Notable ships ==

USS Chesapeake - 38 gun frigate, contemporary to USS Constitution, fought in the War of 1812.
USS Delaware - first ship to be dry docked in the Western Hemisphere.
CSS Virginia - First Confederate ironclad warship, rebuilt from burned out hulk of USS Merrimack; participant in Battle of Hampton Roads against USS Monitor
USS Texas - First U.S. naval battleship to be commissioned.
USS Raleigh - First modern cruiser completely built by the U.S. government.
USS Langley - First U.S. aircraft carrier; converted from USS Jupiter.
USS Shangri-La - Only U.S. aircraft carrier paid for solely by U.S. Warbonds and subscriptions.
USS Alabama
USS Arizona


== Museum ==

Outside the facility on the nearby Old Town Portsmouth waterfront is the Portsmouth Naval Shipyard Museum, which features displays and artifacts from its history.


== See also ==

Rear Admiral Walter McLean, commander of the Shipyard during World War I
John H. Burroughs, superintendent of the Shipyard during the Union occupation of the American Civil War


== References ==


== External links ==
Official website
Historic American Engineering Record (HAER) No. VA-65-A, "Norfolk Naval Shipyard, Mast House, West side of Warrington Avenue between Shubrick & Breeze Streets, Portsmouth, Portsmouth, VA"
HAER No. VA-65-B, "Norfolk Naval Shipyard, Building No. 28A, Adjoining Buildings No. 28 & 29 on Shubrick & Breeze Streets, Portsmouth, Portsmouth, VA"