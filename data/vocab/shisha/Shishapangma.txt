Shishapangma, also called Gosainthān, is the 14th highest mountain in the world at 8,027 metres (26,335 ft) above sea level. It was the last 8,000 metre peak to be climbed, due to its location entirely within Tibet and the restrictions on visits by foreign travelers to the region imposed by authorities of the Government of China and of the Tibet Autonomous Region.


== Name ==
Geologist Toni Hagen explained the name as meaning a "grassy plain" or "meadow" (pangma) above a "comb" or a "range" (shisha or chisa) in the local Tibetan dialect, thereby signifying the "crest above the grassy plains".
On the other hand, Tibetologist Guntram Hazod records a local story that explains the mountain's name in terms of its literal meaning in the Standard Tibetan language: shisha, which means "meat of an animal that died of natural causes" and sbangma which means "malt dregs left over from brewing beer". According to the story, one year a heavy snowfall killed most of the animals at pasture. All that the people living near the mountain had to eat was the meat of the dead animals and the malt dregs left over from brewing beer, and so the mountain was named Shisha Pangma (shisha sbangma), signifiying "meat of dead animals and malty dregs".
The Sanskrit name of the mountain, Gosainthan, means "place of the saint" or "Abode of God". Still, its most common name is Shishapangma.


== Geography ==
Shishapangma is located in south-central Tibet, five kilometres from the border with Nepal. It is the only eight-thousander entirely within Chinese territory. It is also the highest peak in the Jugal Himal which is contiguous with and often considered part of Langtang Himal. The Jugal/Langtang Himal straddles the Tibet/Nepal border. Since Shishapangma is on the dry north side of the Himalayan crest and further from the lower terrain of Nepal, it has less dramatic vertical relief than most major Himalayan peaks.
Shishapangma has a subsidiary peak higher than 8000 m: Central-Peak at 8008 m.


== Ascents and attempts ==
Up to 2014, 27 people have died climbing Shishapangma, including Alex Lowe and Dave Bridges (both US) in 1999, and veteran Portuguese climber Bruno Carvalho. Nevertheless, Shishapangma is one of the "easier" eight-thousanders to climb. The standard route ascends via the northwest face and northeast ridge and face ("Northern Route"), and boasts relatively easy access, with vehicle travel possible to base camp at 5,000 m (16,400 ft). Routes on the steeper southwest face are more technically demanding and involve 2,200 metres (7,220 ft) of ascent on a 50-degree slope.


=== First ascent ===
Shishapangma was first climbed via the Northern Route on 2 May 1964 by a Chinese expedition led by Xǔ Jìng 许竞. In addition to Xǔ Jìng, the summit team consisted of Zhāng Jùnyán 张俊岩, Wang Fuzhou (Wáng Fùzhōu 王富洲), Wū Zōngyuè 邬宗岳, Chén Sān 陈三, Soinam Dorjê (Suǒnán Duōjí 索南多吉), Chéng Tiānliàng 程天亮, Migmar Zhaxi (Mǐmǎ Zháxī 米马扎西), Dorjê (Duōjí 多吉) and Yún Dēng 云登.


=== Later ascents and attempts ===
1980 7 May, "Northern Route", (2nd ascent) by Michl Dacher, Wolfgang Schaffert, Gunter Sturm and Fritz Zintl; Sigi Hupfauer and Manfred Sturm (12 May); as part of a German expedition.
1980: 13 October, "Northern Route", (3rd ascent) by Ewald Putz and Egon Obojes, as part of an Austrian expedition.
1981: 30 April, "Northern Route", (4th ascent) by Junko Tabei, Rinzing Phinzo and Gyalbu Jiabu, as part of a Japanese women's expedition.
1981: 28 May, "Northern Route", (5th ascent) by Reinhold Messner and Friedl Mutschlechner, as part of an Austrian expedition.
1982: 28 May, "British Route", southwest face, also known as "Right-hand couloir" (alpine style), FA by Doug Scott, Alex Macintyre and Roger Baxter-Jones (all UK). Route follows the right-hand couloir on the southwest face.
1987: 18 September, Elsa Ávila and Carlos Carsolio become the first Mexicans to summit Shishapangma. For Ávila, her first eight-thousander and for Carsolio, his second, via the northern face/ridge to the central summit, then along the arete to the main summit, with Wanda Rutkiewicz, Ramiro Navarrete, and Ryszard Warecki.
1987: 18 September, west ridge, FA by Jerzy Kukuczka and Artur Hajzer (both Poland). A new road along the ridge west, by the western summit (first entry) and continue through by the middle summit on the main summit. Kukuczka skied down from near the summit. This was his last of fourteen eight-thousanders.
1987: 19 September, central couloir, north face, FA by Alan Hinkes (UK) and Steve Untch (US).
1989: 19 October, Central buttress, southwest face, FA by Andrej Stremfelj and Pavle Kozjek.
1990: Left-hand couloir, southwest face (not reaching the main summit), Wojciech Kurtyka (Poland), FA by Erhard Loretan (Switzerland) and Jean Troillet (Switzerland)
1993: Far-right couloir, southwest face, FA solo by Krzysztof Wielicki (Poland).
1994: Left-hand couloir, southwest face (not reaching the main summit), Erik Decamp (France), Catherine Destivelle (France).
1999: 28 September, Edmond Joyeusaz (Italy) first ski descent from central summit.
2002: 5 May, "Korean Route" on southwest face, FA by Park Jun Hun and Kang Yeon Ryoung (both South Korea).
2002 26 October: Tomaž Humar(Slovenia), Maxut Zhumayev, Denis Urubko, Alexey Raspopov and Vassily Pivtsov got to the summit. Tomaž Humar climbed last 200 m (80°/50–60°, 200 m) of ascent and descent (65–75°, 700 m)
2004: 11 December, Jean-Christophe Lafaille (France) provokes controversy when he climbs the "British Route" on the southwest face, solo, and claims a winter ascent. Since this was not calendar winter, he changes his claim to an ascent "in winter conditions."
2005: 14 January, first (calendar) winter ascent by Piotr Morawski (Poland) and Simone Moro (Italy).
2011: 16–17 April, Ueli Steck (Switzerland) solos the southwest face in 10.5 hours, leaving base camp (5,306m) at 10:30 pm on 16 April and returning to base camp 20 hours later.
2014: September 24, Sebastian Haag died together with the Italian mountaineer Andrea Zambaldi in an avalanche accident. He was 35.


== Bibliography ==
A Photographic record of the Mount Shisha Pangma Scientific Expedition. Science Press Peking 1966.
Scott, Doug; MacIntyre, Alex (2000) [1984]. Shisha Pangma: The Alpine Style First Ascent of the South-West Face. Seattle: The Mountaineers Books. ISBN 0-89886-723-1. 
Venables, Stephen; Fanshawe, Andy (1996). Himalaya Alpine-Style: The Most Challenging Routes on the Highest Peaks. Seattle: Mountaineers Books. ISBN 0-89886-456-9. 
Sale, Richard, Cleare, John: On Top of the World (Climbing the World's 14 Highest Mountains), lists of ascents, HarperCollins Publ., 2000, ISBN 978-0-00-220176-6.


== Notes and references ==


== External links ==
Shisha Pangma main page on Himalaya-Info.org
Shisha Pangma description on Summitpost
Shisha Pangma on Peakware
Shisha Pangma on www.8000ers.com, the complete list of ascent up to 2009 by Eberhard Jurgalski (PDF)
Shisha Pangma on everestnews.com, photodiagram of the routes on the SW face
himalaya-info.org panoramas from the slopes and peak of Shisha Pangma with exact explanations, images 6th to 13th are from very top. The topography of variations to normal route from northern side is explained by the two last photographs (no. 13 i 14), including the view from main summit to the other two. (German)