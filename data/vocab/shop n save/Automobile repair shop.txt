An automobile repair shop (also known as a garage) is a repair shop where automobiles are repaired by auto mechanics and electricians.


== Types ==
Automotive garages and repair shops can be divided into following categories:
The auto parts stores or motor factors who also maintain service operations. This is not common in the United Kingdom but more common in the US.
Automobile repair workshops that are independently owned and operated businesses. These may also include regional or national chains and franchises including OEM car dealership sites. In the United States, these sites are commonly certified by their respective manufacturer to perform warranty and recall repairs by that manufacturer or distributor. Independent automobile repair shops in the US may also achieve certification through manufacturer sponsored programs. In the European Union a recent law (The EC Block Exemption Regulation 1400/2002 (October 2003)) allows motorists more flexibility in selecting where they can get their car serviced. Due to this legislation, maintenance and service work does not have to be done by the main dealer as long as the garage uses Original Equipment 'Matching Quality' parts, and are recorded as such, and the garage follow the manufacturer's service schedules. The Block Exemption Regulation (BER) covers service and maintenance during the warranty period and prohibits vehicle manufacturers’ warranties from including conditions that require normal maintenance to be provided within the vehicle manufacturer’s network or that all parts used must be the manufacturer’s original spare parts. This means that motorists benefit from open market competition in aftermarket parts, repairs and services thus reducing the cost of servicing through better labor rates and competitively priced parts.
Specialty automobile repair shops are shops specializing in certain parts such as brakes, mufflers and exhaust systems, transmissions, body parts, tires, automobile electrification, automotive air conditioner repairs, automotive glass repairs and installation, and wheel alignment or those who only work on certain brands of vehicle or vehicles from certain continents of the world. There are also automotive repair shops that specialize in vehicle modifications and customization. Oftentimes, various specialized auto repair shops will have varied infrastructure and facilities (for specific jobs or vehicles), as well as technicians and mechanics with different qualifications.
Online automobile repair shops providing doorstep repair services and home delivery of new and used auto parts of different late model and classic cars whose parts are not widely available in the market. Such kind of organizations are predominant in US with wide acceptance and high growth in UK also.The developing countries are still adapting to the e-commerce marketplace and it is expected that with its success in the US this will also prove to be revolutionary there also.


== Auto body repair ==
Automotive repair shops also offer paintwork repairs to scratches, scuffs and dents to vehicle damage as well as damage caused by collisions and major accidents. Many body shops now offer paintless dent repair, which is done by pushing the dents out from inside. OEM Certified Collision Centers have the highest standards.


== See also ==
Automotive Service Excellence (ASE)
Breakdown (vehicle)
Preventive maintenance
Reliability centered maintenance


== References ==