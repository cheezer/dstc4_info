The Potteries Shopping Centre (branded as Intu Potteries) is an indoor shopping centre in Hanley, Stoke on Trent, in the Staffordshire Potteries.


== Stores and facilities ==
The centre houses anchor outlets Primark and Debenhams, as well as a Starbucks coffee shop, a River Island clothing store and HMV entertainment store. On site facilities include a Customer Service Desk, information and traffic kiosks, and public toilets.


== Hanley Market Hall ==
The centre also contains access to the Hanley Market Hall, which dates back to the 1600s. The market opened at its current location in 1987, although it still stands on the grounds of the 1849 market hall.


== References ==


== External links ==
Official website