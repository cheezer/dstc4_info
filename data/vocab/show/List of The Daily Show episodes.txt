The Daily Show is an American late-night satirical television program airing each Monday through Thursday on Comedy Central and, in Canada, The Comedy Network. The half-hour-long show premiered on July 21, 1996, and was hosted by Craig Kilborn until December 1998. Jon Stewart took over as host in January 1999, making the show more strongly focused on politics and the national media, in contrast with the pop culture focus during Kilborn's tenure. It is currently the second longest-running program on Comedy Central, and has won 20 Primetime Emmy Awards. A total of 2,677 episodes have aired.
Describing itself as a fake news program, The Daily Show draws its comedy and satire from recent news stories, political figures, media organizations, and often, aspects of the show itself. The show typically opens with a long monologue from Jon Stewart relating to recent headlines and frequently features exchanges with one or more of several correspondents, who adopt absurd or humorously exaggerated takes on current events against Stewart's straight man persona. The final segment is devoted to a celebrity interview, with guests ranging from actors and musicians to nonfiction authors and political figures.


== Episodes ==


=== Craig Kilborn tenure ===
List of The Daily Show episodes (1996)
List of The Daily Show episodes (1997)
List of The Daily Show episodes (1998)


=== Jon Stewart tenure ===


== References ==


== External links ==