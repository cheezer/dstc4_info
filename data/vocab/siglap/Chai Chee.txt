Chai Chee is a neighbourhood within Bedok New Town in Singapore. The area was formerly a small village known as Kampong Chai Chee. It is served by two major namesake roads, namely Chai Chee Street and Chai Chee Drive, with minor lanes of Chai Chee Road, Chai Chee Avenue and the original Jalan Chai Chee. Geographically, Chai Chee lies to the north of Siglap, east of Kembangan and to the west of the town centre of Bedok New Town.


== Etymology and early history ==
In the Hokkien dialect of the Chinese language, Chai Chee may be translated to mean Vegetable Market (菜市). Before the estate was built, the area was primarily farmland over a series of rolling hills. The kampong, or village, was located on a high hill around where the Singapore Anti-tuberculosis Association [1] Clinic now stands. Before, one needs to go downhill to reach the clinic. Today the clinic sits much higher than the surrounding areas after the hills were levelled.
The area got its name as farmers would gather there daily to sell their produce. A thriving community soon evolved and Chai Chee became a focal point for the kampong residents there. When development began in the late 1960s, the estate was built originally with 40 blocks of flats, consisting mainly of rental units. It was the first Housing and Development Board estate to be built on the eastern part of Singapore. Community amenities were built, such as a market, food centre, shops, banks, library, community center, kindergarten and a bus terminus. To provide for employment, there were 3 factories built - Rollei Cameras, Varta Batteries and Nippon Miniature Bearing (NMB).
Chai Chee Estate quickly gained importance as it was then the only urban estate in the mainly rural part of eastern Singapore. In the early 1970s, the electoral division of Chai Chee stretches all the way southwards to Upper East Coast Road. This led to the naming of the school there as Chai Chee Secondary School [2], even though today that area is no longer part of Chai Chee Estate.


== Chai Chee Neighbourhood ==

Today, Chai Chee has been redeveloped and expanded into a modern housing estate. Most of the old rental blocks have been torn down and replaced with larger, purchased dwellings. There are now primary and secondary schools, a vocational institute, mosque and church. Two large industrial areas house many multinational firms. One of the industrial area was redeveloped on the site of the former Rollei and Varta factories. Chai Chee Bus Terminus was closed and services moved to Bedok Bus Interchange. On its site now stands some new high-rise residential blocks. With the rise of Bedok New Town, most of the hustle and bustle of town life naturally moved to Bedok Central, and the neighbourhood seemed to have reverted to the laid-back, rural charm of the old kampong days.


== Transportation ==
The neighbourhood is served by two feeder bus services from Bedok Bus Interchange, SBS Transit 222 and 229, while major trunk services ply along New Upper Changi Road at the southern perimeter and Bedok North Road at the northern side.


== Education institutions ==
East Coast Primary School [3]
Ping Yi Secondary School [4]
ITE College Central (Bedok Campus) (Closed, moved into ITE College West)
NPS International School [5]


== Places of worship ==
Chai Chee United Temple
Chia Hung Boo Temple
Masjid Al-Ansar[6]
Bethesda Cathedral [7]