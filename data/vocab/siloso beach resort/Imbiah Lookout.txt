Imbiah lookout is Singapore's biggest cluster of attraction located in Sentosa Island. The area contains 11 attractions.


== History ==
Built in the 1980s, Imbiah lookout was unnamed until 2003. The area originally covered 30% of Sentosa's area. Later, much of Imbiah Lookout was demolished to make way for Resorts World Sentosa, so that it now only covers 20% of Sentosa, which is half the area of Mount Imbiah and down its slope facing Siloso Beach.


== Attractions ==


=== Tiger Sky Tower ===

The Tiger Sky Tower (previously known as the Carlsberg Sky Tower) is free-standing observation tower on Sentosa. At a height of 110 metres above ground and 131 metres above sea level, it offers visitors a panoramic view of Sentosa, Singapore, and the Southern Islands. On a clear day, the view extends to parts of Malaysia and Indonesia. At ground level, visitors enter a large disc-shaped airconditioned cabin fitted with glass windows all round. The cabin then revolves slowly as it ascends the column of the tower. The cabin has a capacity of 72 visitors.
The Sky Tower used to sit at the very spot of what was formerly known as the Dragon Court. It has a dragon statue in the centerpiece with water spouting out from its mouth. In one of its claws, it holds a previous logo of Sentosa which was used in the 1980s. Its tail ends at the dragon trail at the northern part of Imbiah Lookout. It was demolished a few months before the groundbreaking ceremony of the sky tower. It was opened on 7 February 2004, is situated in the Imbiah Lookout zone in the centre of Sentosa and can be reached by Cable Car, Sentosa Luge Chair Lift, by Sentosa Express or by bus.


=== Images of Singapore ===

Images of Singapore, at 1.2540°N 103.8176°E﻿ / 1.2540; 103.8176, is an award-winning historical museum in that exhibits the culture and history of Singapore using multi-media displays, multi-screen theatre presentations and lifesize tableaus depicting major events in Singapore's history.The attraction is housed in a former military hospital known as the "Sick Quarters", used throughout 1893–1950. The museum is near the Singapore Cable Car stop at Sentosa and the Merlion. It offers a chronological history of Singapore from its earliest days to the present. There are numerous exhibitions, covering the pre-British period of Malaysian rule, British colonialism, the founding of colonial Singapore by Thomas Stamford Raffles, the Japanese occupation, and the post-colonialist era under Singapore's first Prime Minister Lee Kuan Yew .
Opened in 1983 as the Pioneers of Singapore & Surrender Chambers museum, the museum was extended in 1994 to include a section called Festivals of Singapore and the attraction was rechristened with its present name. Stories of the Sea, a new multi-sensory maritime experience was opened in 2002, but was removed from the museum within the following year when it proved unpopular with guests. The museum was closed in 2003 for a major upgrade. Award winning experience designer Bob Rogers (designer) and the design team BRC Imagination Arts, were tasked to undertake the retelling of the story of the multicultural melting pot of the island's citizens. When the renewed Images of Singapore experience reopened in 2005 it contained a more integrated look and additional attractions, including the multi-screen special effects show Four Winds of Singapore, a tribute to the national contributions of Singapore's Chinese, Eurasian, Malay and Indian populations.
The 2005 renovation won a prestigious Thea Award for "Best Reinvention of a Cultural Heritage Center" in April 2006.
A restaurant and a souvenir shop is located inside and at the exit area of the museum.


=== Butterfly Park and Insect Kingdom ===
The Butterfly Park is a landscape garden with over 15,000 live butterflies, representing more than fifty species. Housed in a cool outdoor conservatory, these butterflies range from the 25 millimetre (1 in) Eurema sari to the 150 mm (6 in) Papilio iswara.
The Insect Kingdom houses some 3,000 species of rare insects from around the world, including a 160 mm Dynastes Hercules beetle.


=== Sentosa 4D Adventureland ===
The Sentosa 4D Adventureland is Singapore's first and Southeast Asia's original 4-dimensional theatre. Opened in January 2006 at the cost of S$3.5 million, the theatre is equipped with digital projection and a DTS 6.1 sound system. Guests are seated on a motion based chair in a typical movie theatre watching a 3D show with visual effects popping out of the screen coupled with environmental effects providing a lifelike feel. The current show is the 'Journey2!' in 4D comedy, as offered in other theme parks around the world.
It also has a simulation ride which is called Extreme Log Ride and also a shooting game name Desperados. All of which are in 4D.


=== Sentosa CineBlast ===
Opened in June 2007, Cineblast, which replaced Cinemania, is Singapore's only cinema ride. It features high definition wide-screen projection and a 6 axis motion system, and takes visitors on a log ride.


=== MegaZip Adventure Park ===
Located at the top of Mount Imbiah, MegaZip Adventure Park is Singapore's first adventure park with one of the longest and steepest zip wires in Asia, a 12m high ropes course, a free-fall parachute simulator and a challenging climbing wall.


=== Sentosa Luge & Skyride ===

The Sentosa Luge & Skyride has a self-steering, gravity-driven three-wheel cart. Originally from New Zealand, the non-motorised cart allows rider to speed down a hill over a course of 650 m ending at the Siloso Beach. At the end of the luge, there is the Skyride that can allow rider to see from a high view. It also can be boarded at the start of the Luge.


=== The Flying Trapeze ===
The Flying trapeze is a first of its kind in Singapore.It allows guest to do a simulation of a flying fox and at the same time bounce on to a trapeze. The minimum age should be around 4 years old to simulate The Flying Trapeze.


== Defunct Attractions ==


=== Sijori Wondergolf ===
Sijori Wondergolf was a miniature golf park. There were 54 landscaped greens set in three different 18-hole courses. It eventually went under redevelopment and will re-open in the coming years.


=== Fantasy Island ===
Fantasy Island was a water based theme park in Sentosa. Opened in 1994 at a cost of S$54 million, it had numerous water slides and other features. Once a very popular park, it was plagued by several accidents, including two fatalities, and this eventually led to the closure of the park on 2 November 2002.


=== Volcanoland ===
Volcanoland featured an artificial volcano along with Mayan motifs and scenery. It was closed down and demolished to make way for the new integrated resort that will feature a similar attraction.


=== Musical Fountain ===

The Musical Fountain opened in 1982 and was the star attraction of its era. It underwent three extensive renovations in 1972 (construction), 1992(upgrading project) and 1999 (major restoration and upgrading project). In 2002, world-renowned fountain designer, Yves Pepin (who also designed Songs of the Sea) replaced all the musical fountain shows with the Magical Sentosa Show for a permanent basis but the show did not last long. After operating for 25 years, it was shut down and demolished to make way for the integrated resort. Its last show was staged on 25 March 2007. It was replaced by the Songs of the Sea show.


== See also ==
Sentosa Island
Resorts World Sentosa
Tiger Sky Tower
Images of Singapore
Sentosa Luge
Sentosa Musical Fountain
Magical Sentosa


== References ==


== External links ==
 Media related to Imbiah Lookout at Wikimedia Commons
Sentosa.com: official Imbiah Lookout website
Official Images of Singapore Live website