This is a list of museums in Singapore:


== National museums ==
Asian Civilisations Museum (ACM)
Peranakan Museum
National Museum of Singapore
Singapore Art Museum (SAM)
8Q SAM


== Other museums ==
ArtScience Museum (Marina Bay Sands)
Baba House (National University of Singapore)
The Battle Box
Fort Siloso (Sentosa)
Fuk Tak Chi Museum
Images of Singapore (Sentosa)
JCU Museum of Video and Computer Games (https://www.facebook.com/JcuMuseum)
Lee Kong Chian Natural History Museum (National University of Singapore)
Maritime Experiential Museum & Aquarium (Sentosa)
Mint Museum of Toys
NUS Museum (National University of Singapore)
Nei Xue Tang Museum
Red Dot Design Museum Singapore
The Republic of Singapore Navy Museum
Singapore Pinacothèque de Paris
Singapore Philatelic Museum (SPM)
Singapore Coins and Notes Museum (SCNM)
Sports Museum
The Intan


== Heritage institutions ==
Chinatown Heritage Centre
Chinese Heritage Centre - Nanyang Technological University
Civil Defence Heritage Gallery
Heritage Conservation Centre {HCC)
Malay Heritage Centre
National Archives of Singapore (NAS)
Police Heritage Centre
Reflections at Bukit Chandu (RBC)
Singapore City Gallery (URA)
Singapore Discovery Centre
Science Centre, Singapore
Sun Yat Sen Nanyang Memorial Hall
Teochew Cultural Centre


== See also ==
List of memorials in Singapore


== External links ==
National Heritage Board
Museums and Heritage Centres