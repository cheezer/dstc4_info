Pulau Palawan, also known as Palawan Island, is a slipper-shaped islet located just off the southwestern coast of Sentosa, south of Singapore. It lies approximately opposite Beach Station of the Sentosa Express monorail system, which is between Siloso Beach and Palawan Beach. Palawan is most likely a variant of the Malay word pahlawan, meaning "hero" or "warrior".
Originally a reef known as Serembu Palawan, and marked on at least one map as "Palawan Reef", it was renamed Pulau Palawan after land reclamation. The island now has an area similar to that of Pulau Biola, about 0.4 hectares (0.99 acres).


== Confusion with island off Palawan Beach ==

Pulau Palawan is not to be confused with an unnamed U-shaped artificial sandy islet that is linked to Palawan Beach on Sentosa by a simple suspension bridge. The islet has two lookout towers, and there is a sign on the islet erected by the Sentosa Development Corporation declaring that it is the "southernmost point of Continental Asia". This may be disputed on the ground that the islet is not part of Continental Asia since it is only linked to Sentosa by a bridge. Sentosa itself is connected to the main island of Singapore by a causeway, and Singapore is in turn linked to Peninsular Malaysia by two causeways. In addition, another beach on Sentosa called Tanjong Beach is further south than the islet.


== Notes ==