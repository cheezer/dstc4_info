A dual SIM mobile phone is one which holds two SIM cards.
Devices that use more than two SIM cards have also been developed and released, notably the LG A290 triple SIM phone, and even handsets that can run on four SIMs, such as the Cherry Mobile Quad Q70.


== History ==
The first phone to include dual SIM functionality was the Benefon Twin, released by Benefon in 2000. It wasn't until the late 2000s, however, when more dual SIM phones entered the marketplace and started to attract mainstream attention, most of them coming from small Chinese firms producing phones using Mediatek systems-on-a-chip.
Such phones were initially eschewed by major manufacturers due to potential pressure from telecommunications companies, so it wasn't until recently that Nokia, Samsung, Sony and several others followed suit, with the Nokia C2-00, Nokia C1-00 and most notably the Nokia X, phones from Samsung's Duos series, and the Sony Xperia Z3 Dual, Sony Xperia C and tipo dual.


== Types ==


=== Adapters ===
Prior to the introduction of dual SIM phones, adapters were made for phones to accommodate two SIMs, and to switch between them when required.


=== Passive ===
Dual SIM switch phones, such as the Nokia C1-00, are effectively a single SIM device as both SIMs share the same radio, and thus are only able to place or receive calls and messages on one SIM at the time. They do, however, have the added benefit of alternating between cards when necessary.


=== Standby ===
Dual standby phones, such as those running on Mediatek chipsets, allows both SIMs to be accessed through time multiplexing. When making or receiving calls, the modem locks to the active channel; the other channel would be ignored and thus unavailable during the duration of the call. Two examples of Dual-SIM Standby smartphones are the Samsung Galaxy S Duos and the Sony Xperia M2 Dual.


=== Active ===
Dual SIM active phones or dual active phones, however, come with two transceivers, and are capable of receiving calls on both SIM cards, at the cost of increased battery consumption. One example is the HTC Desire 600.


== Reception ==
Dual SIM phones have become popular especially with business users due to reduced costs by being able to use two different networks based on signal strength or cost, as well as negating the need for having two or more separate devices.
Some sub-contract Chinese companies supply inexpensive dual SIM handsets, mainly in Asian countries. The phones, which also usually include touch screen interfaces and other modern features, typically retail for a much lower price than branded models. While some such phones are sold under generic names or are rebadged by smaller companies under their own brand, numerous manufacturers, especially in China, produce dual SIM phones under counterfeit trademarks such as those of Nokia or Samsung, either as cosmetically-identical clones of the originals, or in completely different designs, with the logo of a notable manufacturer present in order to take advantage of brand recognition or brand image.
Dual SIM phones are common in developing countries, especially in Southeast Asia and the Indian subcontinent, with local firms like Karbonn Mobiles, Micromax and Cherry Mobile releasing feature phones and smartphones incorporating multiple SIM slots.


=== Usage ===
Dual SIMs are popular in locations where lower prices apply to calls between clients of the same provider; they also allow separate numbers for personal and business calls on the same handset.
Dual SIM phones allow users to keep separate contact lists on each SIM, and allow easier roaming by being able to access a foreign network while keeping the existing local card.
Vendors of foreign SIMs for travel often promote dual-SIM operation as a means to substitute their card for a home country provider's card seamlessly on the same handset.


== See also ==
Dual mode mobile
Subscriber Identity Module
SIM cloning
Shanzhai


== References ==