SimSim Humara (Urdu: سِم سِم ہمارا, also known as Sim Sim Hamara; meaning Our Sim Sim) is the Pakistani version of the children's television series Sesame Street. In April 2011, USAID announced that it would fund $20 million to start a local version of Sesame Street in Pakistan. The show, which began airing in December 2011 on PTV, will broadcast 78 episodes. Although the series consists of Urdu, most of the episodes will be translated into the regional tongues of Punjabi, Sindhi, Pashto and Balochi as well. The show includes Elmo and a host of new Pakistani characters. The aim of the program is to increase education among children. Sesame Street has had many co-productions around the world and the one in Pakistan is the first localised version in the country itself. The show has been viewed in Pakistan before, in the early 1990s, although it was in English and the characters and context could only be understood by a westernised minority of Pakistani children. The theme of the show is based on 'tolerance'.
In June 2012, it was reported that the US had terminated its funding of the program. This was later verified by the U.S. Embassy, Islamabad. The decision came after a Pakistani newspaper reported allegations of corruption by the local puppet theater that was working on the initiative, although these allegations were denied by the operator of the theater. The theater will seek alternative sources of funding to continue the production of the show.


== See also ==
Official website of Sim Sim Hamara
SimSim Humara on MuppetWiki


== References ==


== External links ==
CI: Global: Early Childhood Development - Sim Sim Hamara (27 April 2012)
The Guardian: Sesame Street comes to Pakistan (7 April 2011)