The Sims is a strategic life-simulation video game developed by Maxis, published by Electronic Arts, and released on February 4, 2000. The game's development was led by game designer Will Wright, also known for developing SimCity. It is a simulation of the daily activities of one or more virtual persons ("Sims") in a suburban household near SimCity.


== Gameplay ==
The Sims uses a combination of 3D and 2D graphics techniques. The Sims themselves are rendered in 3D, the house, and all its objects, are pre-rendered and displayed dimetrically.
While gameplay occurs in the game's Live mode, the player may enter Build mode or Buy mode to pause time and renovate the house or lot. When the game begins, each family will start off with §20,000 Simoleons (regardless of its number of members). These funds can be used to purchase a small house or vacant lot on the Neighborhood screen. Once a lot is purchased, a house can be constructed or remodeled in Build mode, and/or purchase or move furniture in the Buy mode. All architectural features and furnishings customizable in the Build and Buy modes follow a square tile system in which items must be placed on a tile. Walls and fences go on the edge of a tile and can follow the edge of the tile or cross it, but furniture items cannot be placed on either side of a crossed tile. The base game contains over 150 items including furniture and architectural elements.
Sims are instructed by the player to interact with objects, such as a television set, or other Sims. Sims may receive guests, invited or not, from other playable lots or from a pool of unhoused NPC Sims. If enabled in the game's options, Sims can have a certain amount of free will, allowing them to autonomously interact with their world. However, the player can override most autonomous actions, by cancelling them out in the action queue at the top of the screen. Unlike the simulated environments in games such as SimCity, SimEarth, or SimLife, the Sims are not fully autonomous. They are unable to take certain actions without specific commands from the player, such as paying their bills, finding a job, working out, and conceiving children.

The player can make decisions about time spent in skill development, such as exercise, reading, creativity, and logic, by adding activities to the daily agenda of the Sims. Daily needs such as hygiene maintenance and eating can and must also be scheduled. Although Sims can autonomously perform these actions, they may not prioritize them effectively. Much like real humans, Sims can suffer consequences for neglecting their own needs. In addition to fulfilling their needs, Sims need to maintain balanced budgets. The most conventional method of generating an income is to obtain a job. The game presents various career tracks with ten jobs. Sims may earn promotions by fulfilling skill and friendship requirements of each level, which lead to new job titles, increased wages, and different work hours. Other means of generating an income include creating and selling various items such as artworks and gnomes at home. Sims communicate in a fictional language called Simlish.
The inner structure of the game is actually an agent based artificial life program. The presentation of the game's artificial intelligence is advanced, and the Sims will respond to outside conditions by themselves, although often the player/controller's intervention is necessary to keep them on the right track. The Sims technically has unlimited replay value, in that there is no way to win the game, and the player can play on indefinitely. It has been described as more like a toy than a game.

In addition, the game includes a very advanced architecture system. The game was originally designed as an architecture simulation alone, with the Sims there only to evaluate the houses, but during development it was decided that the Sims were more interesting than originally anticipated and their once limited role in the game was developed further.
While there is no eventual objective to the game, states of failure do exist in The Sims. One is that Sims may die, either by starvation, drowning, fire, or electrocution. When a Sim dies, a tombstone or an urn will appear in (In later expansion packs the Grim Reaper will appear first), the ghost of the deceased Sim may haunt the building where it died. In addition, Sims can leave a household (and game) for good and never return, if fed up with another Sim; two adult Sims with a bad relationship may brawl, eventually resulting in one of them moving out. Children will be sent away for good if they fail their classes.


== Objects ==
Players have a broad array of objects which their respective Sims may purchase. Objects fall into one of eight broad categories: Seating, surfaces, decorative, electronics, appliances, plumbing, lighting and miscellaneous.


== Music ==
The game music was composed by Jerry Martin, Marc Russo, Kirk R. Casey, and Dix Bruce. The game disc contains 37 tracks, of which 15 were published in 2007 as an official soundtrack album. Most of the tracks contain no vocals, but some of them feature Simlish lyrics.


== Expansion packs ==

The Sims is one of the most heavily expanded computer game franchises ever, with a total of seven expansion packs produced. Each expansion generally adds new items, characters, skins, and features.
The Sims

Livin' Large

The Sims: Livin' Large (The Sims: Livin' It Up in the United Kingdom) is the first expansion pack for The Sims, released on August 31, 2000 in North America. This expansion pack focuses on adding new unconventional characters, careers, items, and features.
House Party 

The Sims: House Party is the second expansion pack for The Sims, released on April 2, 2001 in North America. House Party gives players the ability and facilities to hold parties and gatherings in their Sims' homes. House Party was reissued in October 2002, to mark the release of The Sims Deluxe Edition and to match the box covers of the Hot Date and Vacation expansion packs.
Hot Date

The Sims: Hot Date is the third expansion pack for The Sims, released on November 12, 2001 in North America. Hot Date adds the ability for Sims to leave their homes and travel to new destinations. In this expansion pack, the new destination is composed of ten new lots and is called "Downtown". All of the following expansion packs for The Sims add new destinations as well. Hot Date also introduces a revamped relationship system involving short and long term relationships. Sims can also carry inventory and give gifts to other Sims. Hot Date also adds a variety of items and new characters.
Vacation 

The Sims: Vacation (The Sims: On Holiday in the Republic of Ireland, the UK, China, and Scandinavia) is the fourth expansion pack for The Sims, released on March 28, 2002 in North America. Vacation introduces a new destination called "Vacation Island" where Sims can take vacations with family members or with other Sims. This marks the first time Sims can stay on lots away from home. In other words, the game can be saved while a Sim is on Vacation Island. Vacation Island is split into three distinct environments: beach, forest, and snow-capped mountain. Sims can stay at a hotel or rent a tent/igloo to rough it in the wild. They can also purchase or find souvenirs. As with other expansion packs, Vacation introduces new items, characters, and features pertaining to the theme of vacations.
Unleashed 

The Sims: Unleashed is the fifth expansion pack for The Sims, released on November 7, 2002 in North America. Unleashed introduces pets into the game. While dogs and cats are treated as Sims, other pets are treated as objects. However, dogs and cats cannot be controlled directly like human Sims are; only their movements can be directed by the player. Unleashed also introduces gardening. In Unleashed, the original ten-lot neighborhood is expanded to over forty, with the added ability to rezone these lots for residential or community use. Community lots may be modified to contain shops, cafes, and other commercial establishments.
Superstar 

The Sims: Superstar is the sixth expansion pack for The Sims, released on May 13, 2003 in North America. This expansion allows the player's Sims to become entertainment figures and includes representations of several famous personalities. A number of celebrities make cameo appearances but cannot be controlled by the player, and include Avril Lavigne, Andy Warhol, Marilyn Monroe, Jon Bon Jovi, Christina Aguilera, Freddie Prinze, Jr., Sarah McLachlan, and Richie Sambora. Superstar adds new work and leisure items, and a new destination called "Studio Town". This new destination functions as a workplace for celebrity sims where regular visits may be required to maintain their fame and career, marking the first time where players could follow their sims to work. Non-celebrity sims may choose to visit Studio Town for leisure.
Makin' Magic 

The Sims: Makin' Magic is the seventh and final expansion pack for The Sims, released on October 29, 2003 in North America. It introduces magic to the game and allows Sims to cast spells, forge charms, and buy alchemical ingredients. Makin' Magic introduces the Magic Town lots, which house vendors of magical ingredients and items and a number of magic-related mini-games. In addition, it introduces baking and nectar-making. Additional residential lots are also included in Magic Town, marking the first time sims may live outside of the main neighborhood. These lots contain new aesthetic accents such as new grass textures and background sound effects; they also have a higher chance of growing magical items. This expansion pack includes a disc containing a preview of The Sims 2.


== Compilations ==


=== Core game with expansions ===
The Sims has been repackaged in numerous editions. These are not expansions in themselves, but compilations of the base game plus pre-existing expansion packs and additional game content.


=== Expansions only ===
There have also been compilations of expansion packs without the core game released in only North America, and some parts of the UK.


== Reception ==
In 2002, The Sims became the top-selling PC game in history, displacing the game Myst, by selling more than 11.3 million copies worldwide. As of February 7, 2005, the game has shipped 16 million copies worldwide. Critics praised it with positive reviews. It has been a success in many ways—attracting casual gamers and female gamers (the latter making up almost 60% of players). It became a best-seller shortly after launch. In March 2009, Electronic Arts announced that The Sims, as a franchise, has sold more than 110 million copies. Game Informer ranked it the 80th best game ever made in its 100th issue in 2001.


== Legacy ==
The Sims was first released on February 4, 2000. By March 22, 2002, The Sims had sold more than 6.3 million copies worldwide, making it the best-selling PC game in history; and by February 2005, the game has shipped 16 million copies worldwide. Since its initial release, seven expansion packs have been released, as have sequels The Sims 2, The Sims 3 and The Sims 4. The Sims has won numerous awards, including GameSpot's "Game of the Year Award" for 2000.


=== Sequels and spinoffs ===

The Sims Online — Online version of The Sims, where players can interact with other players in real-time
The Sims 2 — Sequel to The Sims; second generation of the main series
The Sims Stories — Spinoff series featuring goal-directed Story Mode
MySims — A Wii spinoff focused more on building objects
The Sims 3 — Prequel to The Sims; third generation of the main series; the default world, Sunset Valley, is set 25 years before The Sims
The Sims Medieval — First title in a line of spinoff products set in medieval times
The Sims Social — Facebook spinoff.
The Sims FreePlay — A freemium version of The Sims for Android, iOS and [Windows Phone] based devices
The Sims 4 — Sequel to The Sims 3; fourth generation of the main series
The Urbz: Sims in the City — A console-only game with Sims gameplay, but with new faction relationships taking place in a hip city setting.


=== Ports and remakes ===
The Sims and all its expansion packs were ported to the Mac by Aspyr Media, Inc.. The Sims was ported to Linux using Transgaming's WineX technology and was bundled with Mandrake Linux Gaming Edition. The WineX engine is unable to run the Windows version of the game. It was released on March 12, 2003.
A separate version of the game was released for the PlayStation 2, Xbox, and Nintendo GameCube in 2003. Gameplay is similar to that of the PC versions and retains many of the core elements. Notable changes include a full 3D camera perspective (instead of the original 2D isometric viewpoint), more detailed appearances of Sims, and the introduction of a "Get A Life" goals-based story mode. The ports enjoyed a generally favorable reception, with Metacritic scores ranging from 83-85 as of August 2009.
The console versions were each followed by a sequel, The Sims Bustin' Out, and a spin-off game, The Urbz: Sims in the City. These versions incorporate some features of later PC expansion packs, and add a multiplayer mode supporting two simultaneous players.


== Film ==
The Sims (working title) is a live action, drama film in preproduction since 2007.
On May 25, 2007, it was announced that The Sims film rights had been purchased by 20th Century Fox. It will be written by Brian Lynch, the writer of Angel: After The Fall. The film will be produced by John Davis, who has worked on films such as Norbit and Eragon.


== See also ==
Simulated reality
Simulated reality in fiction


== References ==

Nakamura, Rika; Wirman, Hanna (October 2005). "Girlish Counter-Playing Tactics". Game Studies 5 (1). 
Pearce, Celia (July 2002). "Sims, BattleBots, Cellular Automata God and Go". Game Studies 2 (1). 
Paulk, Charles (December 2006). "Signifying Play: The Sims and the Sociology of Interior Design". Game Studies 6 (1). 
Atkins, Barry. More than a game: the computer Game as fictional form Manchester: Manchester Univ. Press, 2003.


== External links ==
The Sims at the Wayback Machine (archived June 3, 2002)
The Sims at the Wayback Machine (archived February 6, 2001)
The Sims at the Wayback Machine (archived November 9, 2000)
The Sims at the Wayback Machine (archived October 8, 1999)