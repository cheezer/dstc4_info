The Sims Stories is a series of computer games in The Sims series. They are optimized for play on laptops and some slower computers, due to their lower system requirements than original The Sims 2, but can still be played on desktop computers. The games include:
The Sims Life Stories (2007)
The Sims Pet Stories (2007)
The Sims Castaway Stories (2008)
The Sims Stories Collection includes the three games and was released in Europe on December 4, 2008.