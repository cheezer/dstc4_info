This is a list of airlines which have a current Air Operator Certificate issued by the Civil Aviation Authority of Singapore.


== See also ==
List of airlines
List of defunct airlines of Singapore
List of airports in Singapore
List of defunct airlines of Asia