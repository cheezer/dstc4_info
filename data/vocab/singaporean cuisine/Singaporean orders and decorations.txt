Orders and decorations conferred to civilians and military personnel in Singapore, listed by order of precedence. Awards specific to the military or police forces are separately listed.
Note that the ribbons shown are those used after 1996. For pre-1996 ribbons, see the appropriate award page.
 Bintang Temasek (Star of Temasek) - BT
 Darjah Utama Temasek (Order of Temasek) - DUT
 Darjah Utama Nila Utama (Order of Nila Utama) - DUNU
 Sijil Kemuliaan (Certificate of Honour)
 Darjah Utama Bakti Cemerlang (Distinguished Service Order) - DUBC
 Pingat Kehormatan (Medal of Honour)
 Pingat Gagah Perkasa (Conspicuous Gallantry Medal)
 Pingat Jasa Gemilang (Meritorious Service Medal) - PJG
 Bintang Bakti Masyarakat (Public Service Star) - BBM
 Pingat Pentadbiran Awam, Emas (Public Administration Medal, Gold) - PPA(E)
 Pingat Keberanian (Medal of Valour)
 Pingat Pentadbiran Awam, Perak (Public Administration Medal, Silver) - PPA(P)
 Pingat Pentadbiran Awam, Gangsa (Public Administration Medal, Bronze) - PPA(G)
 Pingat Kepujian (Commendation Medal) - PK
 Pingat Bakti Masyarakat (Public Service Medal) - PBM
 Pingat Berkebolehan (Efficiency Medal) - PB
 Pingat Bakti Setia (Long Service Award) - PBS


== See also ==
Awards and decorations of the Singapore Armed Forces
Awards and decorations of the Singapore Police Force
Awards and decorations of the Singapore Civil Defence Force


== External links ==
Prime Minister's Office, Order of Precedence of National Awards for Civilians, 26 March 2010.
Jean-Paul leBlanc, Medals of Singapore, 11 Feb 2007.
Megan C. Robertson, Decorations and Medals of Singapore, [1], 2 July 2007