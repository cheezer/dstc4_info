A skateboarding trick, or simply a trick, is a maneuver performed on a skateboard while skateboarding. Skateboarding tricks may vary greatly in difficulty.


== Categories ==


=== Ollie ===

An Ollie is a jump where the front wheels leave the ground first. This motion is attained with a snap of the tail (from the backfoot) and sliding your front-foot forward to reach any altitude. A lot of technical tricks transpire from this element (e.g. the kickflip, heelflip, 360-flip). A nollie is when the back wheels leave the ground first, or relatively, it's a switch-stance ollie riding fakie.


=== Aerial ===

Aerials involve floating in the air while using a hand to hold the board on his or her feet or by not keeping constant and careful pressure on the board with the feet to keep it from floating away. Aerials usually combine rotation with different grabs. This class of tricks was first popularized when Tony Alva became famous for his frontside airs in empty swimming pools in the late 1970s and has expanded to include the bulk of skateboarding tricks to this day, including the ollie and all of its variations. The 900 and 1080  fall under the class of aerials.


=== Flip tricks ===

Flip tricks are a subset of aerials which are all based on the ollie. An example is the kickflip, the most widely known and performed flip trick. You can spin the board around many different axes, and even combine several rotations into one trick. These tricks are undoubtedly most popular among street skateboarding purists, although skaters with other styles perform them as well. Combining spins and flips is extremely popular in today's culture. A common trick at today's competitions is called a treflip. A treflip is the combination of a skateboard spinning 360 degrees and a kickflip. There are also double kickflips and triple kickflips which are very difficult but more unique in the skateboarding culture. Bojczan flip -it is a late pressure flip landing with an extra late heelflip


=== Freestyle ===

Freestyle skateboarding tricks are tricks specifically associated with freestyle skateboarding


=== Slides and grinds ===

Slides and grinds involve getting the board up on some type of ledge, rail, or coping and sliding or grinding along the board or trucks, respectively. When it is primarily the board which is contacting the edge, it's called a slide; when it's the truck, it is a grind. Grinding and sliding skateboards started with sliding the board on parking blocks and curbs, then extended to using the coping on swimming pools, then stairway handrails, and has now been expanded to include almost every possible type of edge.


=== Lip tricks ===

Lip tricks are done on the coping of a pool or skateboard ramp. Most grinds can be done on the coping of a ramp or pool as well, but there are some coping tricks which require the momentum and vertical attitude that can only be attained on a transitioned riding surface. These include inverts and their variations as well as some dedicated air-to-lip combinations.


== References ==