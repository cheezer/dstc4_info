Sleepy Creek is a 44.0-mile-long (70.8 km) tributary of the Potomac River in the United States, belonging to the Chesapeake Bay's watershed. Sleepy Creek's source lies near the Hampshire County, West Virginia border at Good, north of State Route 127 in Frederick County, Virginia. From Frederick County, Sleepy Creek flows north through Morgan County, West Virginia, where it drains into the Potomac at the community of Sleepy Creek on the old Baltimore & Ohio Railroad mainline. While Sleepy Creek Lake is a part of the Sleepy Creek watershed, it is an impoundment of its tributary, Meadow Branch, in Berkeley County and not of Sleepy Creek itself.


== Bridges ==


== Tributaries ==
Tributary streams listed from south (source) to north (mouth).
Bear Garden Run
Hands Run
Breakneck Run
Indian Run
South Fork Indian Run
Middle Fork Indian Run
North Fork Indian Run

Rock Gap Run
Middle Fork Sleepy Creek
Iden Run

South Fork Sleepy Creek
Merchant Run

Mountain Run
Yellow Spring Run
Meadow Branch


== List of cities and towns along Sleepy Creek ==
Good
Johnsons Mill
New Hope
Omps
Ridge
Sleepy Creek
Smith Crossroads
Stohrs Crossroads
Stotlers Crossroads


== See also ==
List of West Virginia rivers
List of Virginia rivers


== References ==


== External links ==
Sleepy Creek Watershed Association