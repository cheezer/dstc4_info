John Smith may refer to:
John Smith, a common placeholder name


== Academics ==
John Smith (professor), mathematician at the University of Oxford, 1766–97
John Blair Smith (1764–1799), president of Union College, New York
John Smith (lexicographer) (died 1809), professor of languages at Dartmouth College
John Smith (astronomer) (1711–1795), Lowndean Professor of Astronomy and Master of Caius
John Augustine Smith (1782–1865), president of the College of William and Mary, 1814–1826
John Smith (botanist) (1798–1888), curator of Kew Gardens
J. Lawrence Smith (1818–1883), American doctor and chemist
John Smith (dentist) (1825–1910), founder of Edinburgh's School of Dentistry
John Campbell Smith (1828–1914), Scottish writer, advocate and Sheriff-Substitute of Forfarshire
John Donnell Smith (1829–1928), biologist and taxonomist
John McGarvie Smith (1844–1918), Australian metallurgist and bacteriologist
John Alexander Smith (1863–1939), British Idealist philosopher
John Maynard Smith (1920–2004), geneticist
John Cyril Smith (1922–2003), leading authority on English criminal law
John Derek Smith (1924–2003), Cambridge molecular biologist
John D. Smith (born 1946), indologist at the University of Cambridge
John H. Smith (mathematician), Boston College educator (retired 2005)


== Arts ==
John Smith (engraver born 1652) (1652–1742), English mezzotint engraver
John Smith (English poet) (1662–1717), English poet and playwright
John Christopher Smith (1712–1795), English composer
John Warwick Smith (1749–1831), British watercolour landscape painter and illustrator
John Stafford Smith (1750–1836), composer of the tune for "The Star-Spangled Banner"
John Raphael Smith (1752–1812), English mezzotint engraver and painter
John Thomas Smith (engraver) (1766–1833), draughtsman, engraver and antiquarian
John Smith (clockmaker) (1770–1816), Scottish clockmaker
John Rubens Smith (1775–1849), London-born painter, printmaker and art instructor who worked in the United States
John Smith (architect) (1781–1852), Scottish architect
John Smith (art historian) (1781-1855), British art dealer
John Orrin Smith (1799–1843), English wood engraver
John Frederick Smith (1806–1890), English novelist
John Moyr Smith (1839–1912), British artist and designer
John Berryman (1914–1972), originally John Allyn Smith, American poet
John Smith (Canadian poet) (born 1927), Canadian poet
John Smith (actor) (1931–1995), American actor
John N. Smith (born 1943), Canadian film director and screenwriter
John Smith (English filmmaker) (born 1952), avant-garde filmmaker
John F. Smith American soap opera writer
John Smith (comics) (born 1967), British comics writer
John G. Smith (poet), Scottish poet
John Smith (musician), English contemporary folk musician and recording artist


== Military ==
John Smith (banneret) (1616–1644), Englishman who supported the Royalist cause in the English Civil War
John Smith (Royal Artillery officer) (1754–1837), soldier in the American Revolutionary War
John Mark Frederick Smith (1790–1874), British general and colonel-commandant of the Royal Engineers
John Smith (sergeant) (1814–1864), soldier in the Bengal Sappers and Miners, and Indian Mutiny Victoria Cross recipient
John E. Smith (1816–1897), Swiss emigrant, Union general during the Civil War
John Smith (private) (1822–1866), soldier in the 1st Madras (European) Fusillers and Indian Mutiny Victoria Cross recipient
John Smith (Medal of Honor, b. 1826), American Civil War sailor and Medal of Honor recipient
John Smith (Medal of Honor, b. 1831), American Civil War sailor and Medal of Honor recipient
John Smith (Medal of Honor, b. 1854), United States Navy sailor and Medal of Honor recipient
John Manners Smith (1864–1920), recipient of the Victoria Cross
John Smith (flying ace) (1914–1972), United States Marine Corps flying ace and Medal of Honor recipient


== Politicians ==


=== Canada ===
John David Smith (1786–1849), businessman and political figure in Upper Canada
John Smith, 1800s Cree Chief and Treaty Six signatory; founder of the Muskoday First Nation in Saskatchewan
John Shuter Smith (c. 1813–1871), lawyer and political figure in Canada West
John Smith (Kent MPP), member of the 1st Ontario Legislative Assembly, 1867–1871
John Smith (Manitoba politician) (1817–1889), English-born farmer and politician in Manitoba
John Smith (Peel MPP) (1831–1909), Scottish-born Ontario businessman and political figure
John Smith (Ontario MP) (1894–1977), member of Canadian House of Commons, Lincoln electoral district
John James Smith (1912–1987), member of Canadian House of Commons, Moose Mountain, Saskatchewan electoral district
John Roxborough Smith, Canadian politician in the Legislative Assembly of Ontario


=== Great Britain ===
John Smith (MP for Coventry), Member of Parliament (MP) for Coventry in the 1491 Parliament.
John Smith (Newcastle-under-Lyme MP) (by 1489–1561), MP for Newcastle-under-Lyme
John Smith (High Sheriff of Kent) (1557–1608), MP for Aylesbury and Hythe
John Smith (antiquarian born 1567) (1567–1640), English genealogical antiquary and politician who sat in the House of Commons, 1621–1622
John Smith (Cavalier born 1608) (1608–57), English politician who sat in the House of Commons, 1640–1644
John Smith (Chancellor of the Exchequer) (1655/56–1723), English Chancellor of the Exchequer and Speaker of the House of Commons, 1705–1708
John Smith (judge) (died 1726) Justice of Common Pleas in Ireland until 1702, then Baron of the Exchequer
John Smith (Deputy Governor of Anguilla) (died 1776), Deputy Governor of Anguilla
John Smith (Wendover MP) (1767–1842), member of Parliament for Wendover
John Spencer Smith (1769–1845), British diplomat, politician and writer
John Benjamin Smith (1796–1879), British Liberal MP for Stirling Burghs 1847–1852 and Stockport 1852–1874
John Abel Smith (1802–71), British Member of Parliament for Chichester and Midhurst
John Smith (Conservative politician) (1923–2007), former Member of Parliament (MP) for the Cities of London and Westminster
John Smith, Baron Kirkhill (born 1930), life peer in the House of Lords
John Smith (Labour Party leader) (1938–1994), leader of the British Labour Party
John Smith (Welsh politician) (born 1951), Welsh politician and Labour Party member of Parliament


=== United States ===
J. Gregory Smith (1818–1891), 28th Governor of Vermont
J. H. Smith (Mayor of Everett) (1858–1956), mayor of Everett, Washington and co-founder of Anchorage, Alaska
J. Hyatt Smith (1824–1886), United States representative from New York's 3rd Congressional District
J. Joseph Smith (1904–1980), United States representative from Connecticut and Federal judge
John Smith (explorer) (1580–1631), helped found the Virginia Colony and became Colonial Governor of Virginia
John Smith (President of Rhode Island) (died 1663), colonial president (governor) of Rhode Island
John Smith (New York politician born 1752) (1752–1816), United States senator from New York
John Smith (Ohio Senator) (c. 1735–1824), United States senator from Ohio
John Smith (Vermont) (1789–1858), United States representative from Vermont's 4th Congressional District
John Smith (Virginia burgess) (1620–63), Virginia colonial politician
John Smith (Virginia representative) (1750–1836), United States representative from Virginia's 3rd Congressional District
John Smith (Washington politician), American politician of the Republican Party
John Ambler Smith (1847–1892), United States representative from Virginia
John Armstrong Smith (1814–1892), United States representative from Ohio
John Arthur Smith (born 1942), Democratic member of the New Mexico Senate
John B. Smith (Wisconsin) (died 1879), Wisconsin politician
John Butler Smith (1838–1914), 52nd Governor of New Hampshire
John C. Smith (politician) (1832–1910), Lieutenant Governor of Illinois
John Cotton Smith (1765–1845), eighth Governor of Connecticut
John E. Smith (New York) (1843–1907), New York politician
John Hugh Smith (1819–1870), three-time Mayor of Nashville, Tennessee between 1845 and 1865
John Lee Smith (1894–1963), Lieutenant Governor of Texas
John Lyman Smith (fl. 1852–1853), member of the 2nd Utah Territorial Legislature
John R. Smith (agriculture commissioner) (fl. 1898–1899), North Carolina politician
John M. Smith (politician, born 1872) (1872–1947), American businessman and politician
John M. C. Smith (1853–1923), United States representative from Michigan's 3rd Congressional District
John Montgomery Smith (1834–1903), Wisconsin politician
John Quincy Smith (1824–1901), United States representative from Ohio's 3rd Congressional District
John R. Smith (politician, born 1945), Louisiana state senator
John Robert Smith, mayor of Meridian, Mississippi
John Speed Smith (1792–1854), United States representative from Kentucky
John T. Smith (congressman), United States representative from Pennsylvania's 3rd Congressional District, 1843–1845
John Walter Smith (1845–1925), 44th Governor of Maryland
John William Smith (1792–1845), Texas political figure and mayor of San Antonio, Texas
John Y. T. Smith (1831–1903), three time member of Arizona Territorial Legislature


=== Other countries ===
John Smith (Victoria politician) (John Thomas Smith, 1816–1879), Australian politician
John Hope Smith (c. 1787–1831), Governor of colonial Ghana, 1817–22
John Samuel Smith (1841–1882), New South Wales politician
John Smith (New South Wales politician) (1821–1885), Scottish/Australian professor and politician
J. Valentine Smith (1824–1895), New Zealand politician with the full name John Valentine Smith


== Religion ==
John Smith (bishop, died 1479) (died 1479), bishop of Llandaff, 1476–1479
John Smith (Platonist) (1618–1652), one of the founders of the Cambridge Platonists
John Smith (Unitarian) (fl. 1648–1727), Unitarian writer
John Smith (priest, born 1659) (1659–1715), English editor of Bede
John Smith (uncle of Joseph Smith) (1781–1854), Presiding Patriarch and member of the First Presidency of The Church of Jesus Christ of Latter-day Saints
John Smith (Restoration Movement) (1784–1868), early Restoration Movement leader
John Smith (missionary) (1790–1824), English missionary in Demerara
John Smith (Revivalist) (1794–1831), English Methodist minister known as "The Revivalist"
John Smith (nephew of Joseph Smith) (1832–1911), Presiding Patriarch of The Church of Jesus Christ of Latter-day Saints
John Henry Smith (1848–1911), apostle in The Church of Jesus Christ of Latter-day Saints
John Taylor Smith (1860–1938), Anglican Bishop of Sierra Leone
John Smith (Archdeacon of Wiltshire) (1933–2000), Anglican priest
John M. Smith (bishop) (born 1935), American bishop of the Roman Catholic Church
John Smith (God's Squad), Australian founder of the God's Squad motorcycle club


== Sports ==


=== Cricket ===
John Smith (1845 Yorkshire cricketer), English cricketer
John Smith (cricketer, born 1833) (1833–1909), Lancashire and Yorkshire cricketer
John Smith (cricketer, born 1834) (1834–?), Nottinghamshire and All-England Eleven cricketer
John Smith (cricketer, born 1835) (1835–89), English cricketer
John Smith (Derbyshire cricketer) (1841–98), English cricketer
John Smith (cricketer, born 1843) (1843–73), English cricketer
John Smith (cricketer, born 1882) (1882–59), English cricketer
John Smith (cricketer, born 1924) (1924–91), English cricketer
John Smith (New Zealand cricketer) (born 1960), New Zealand cricketer also known as Campbell Smith


=== Football (soccer) ===
John Smith (footballer, born 1855) (1855–1937), Scottish footballer of the 1870s and 1880s
John Smith (footballer, born 1865) (1865–1911), Scottish footballer who played as a striker
John Smith (footballer, born 1898), Scottish footballer (Ayr United, Middlesbrough, Scotland)
John Smith (footballer, born 1921), English footballer
John Smith (footballer, born 1927), English footballer (Liverpool)
John Smith (footballer, born 1939) (1939–1988), West Ham
John Smith (footballer, born 1970), football full back (Tranmere Rovers)
John Smith (1930s footballer), footballer of the 1930s for (Gillingham)
John Smith (businessman) (1920–1995), chairman of Liverpool (1973–90)
John Smith (inside-left), English footballer 1932–33
John Smith (soccer), retired English football striker
Ted Smith (footballer, born 1914), English football player and manager (birth name John Edward Smith)


=== Baseball ===
John Smith (AL first baseman) (1906–1982), first baseman in 1931
John Smith (NL first baseman) (1858–1899), first baseman in 1882
John Smith (shortstop), shortstop, 1873–75


=== Rugby ===
John Smith (rugby league), New Zealand international
John Sidney Smith (rugby union) (1860–?), Wales rugby union international
Johnny Smith (rugby union) (1922–1974), New Zealand rugby player, baker, soldier, and sportsman


=== American football ===
John "Clipper" Smith (1904–1973), American football player and coach
John L. Smith (born 1948), American college football coach
John Smith (American football) (born 1949), former New England Patriots kicker
J. T. Smith (American football) (John Thomas Smith, born 1955), former professional American football player
John Smith (running back), former American football running back


=== Other sports ===
John Smith (athlete) (born 1950), former American sprint athlete and now coach
John Smith (Australian footballer) (born 1933), footballer for St Kilda
John Smith (basketball) (born 1944), former American professional basketball player
John Smith (Canadian rower) (1899–?), Canadian rower at the 1924 Olympics
John Smith (South African rower) (born 1990), South African rower at the 2012 Olympics
John Smith (wrestler) (born 1965), American wrestler, two-time Olympic gold medalist
J.T. Smith (wrestler), American professional wrestler, better known by his ring name J.T. Smith


== Criminals ==
John Smith (housebreaker) (1661–after 1727), burglar who evaded hanging thrice and was eventually transported to Virginia
John Smith (murderer) (born 1951), convicted murderer who killed his first and second wives
John Eldon Smith (1930–1983), convicted of the murders of Ronald and Juanita Akins


== Businessmen ==
John Smith (brewer) (1824–1879), Tadcaster brewery founder in North Yorkshire, UK
John F. Smith, Jr. (born 1938), former chairman and chief executive officer, General Motors
John J. Smith (1820–1906), African American abolitionist, Underground Railroad contributor and politician
John Smith (BBC executive) (born 1957), chief executive officer, BBC Worldwide Ltd


== Others ==
John Smith (explorer) (1580–1631), one of the founders of the Virginia Colony
John Smith (Chippewa Indian) (died 1922), reputed to have died at the age of 137
John Smith (died 1835), one of the last two Englishmen that were hanged for sodomy in 1835
Sir John Smith (police officer) (born 1938), British police officer, Deputy Commissioner of the Metropolitan Police, 1991–1995
Sir John Smith, 1st Baronet (1744–1807), baronet
John Brown Smith (1837–?), American author, shorthand developer, utopianist, tax resister
John Chaloner Smith (1827–1895), Irish civil engineer and writer on mezzotints
John Douglas Smith (born 1966), sound editor
John Baptist Smith (1843–1923), invented and helped build a lantern system of naval signaling
John K. Smith (died 1845), founder of SmithKline as in GlaxoSmithKline, the leading pharmaceutical business
John Kilby Smith (1752–1842), public servant from New England
John Sidney Smith (1804–1871), legal writer
John Gordon Smith (1792–1833), Scottish surgeon and professor of medical jurisprudence
John Kelday Smith (1834–1889), Scottish bellhanger and songwriter
John Pye-Smith (1774–1851), Congregational theologian and tutor


== Characters ==
Ranger Smith (John Francis Smith), a park ranger in Yogi Bear cartoons
J. Wesley Smith (1899–1965), a character in cartoons of Burr Shafer
John Smith (Kyon), Kyon's alias in the novels and animations of the Haruhi Suzumiya series
John "Hannibal" Smith, a character in The A-Team
John Smith, the human identity of Red Tornado, a fictional superhero appearing in DC Comics
John Smith, also known as "Number Four", the main protagonist of the young adult novel, I Am Number Four
John Smith, the main protagonist played by Bruce Willis in the film Last Man Standing
John Smith, a protagonist played by Brad Pitt in Mr. & Mrs. Smith
John Smith (Doctor Who), an alias of the Doctor in Doctor Who media
John Smith (Jericho), a character in the TV series Jericho
Agent Smith, also known as John Smith, the main villain in The Matrix franchise


== Film ==
John Smith (film), a lost 1922 silent film comedy-drama


== See also ==
Joe Bloggs
John Doe
John Smyth (disambiguation)
John Smythe (disambiguation)
Johnny Smith (disambiguation)
Jack Smith (disambiguation)
Jonathan Smith (disambiguation)
John Smit (born 1978), South African rugby player
Juan Smith (born 1981), South African rugby player
John Smith's Brewery, a brewery founded in 1758 by John Smith at Tadcaster in North Yorkshire, England
John Smith & Son, Glasgow-based bookseller
Ode to J. Smith, the Travis album written about John Smith, the unknown everyday man
Johann Schmidt (disambiguation)