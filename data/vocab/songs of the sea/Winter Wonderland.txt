"Winter Wonderland" is a winter song, popularly treated as a Christmastime pop standard, written in 1934 by Felix Bernard (music) and Richard B. Smith (lyricist). Through the decades it has been recorded by over 200 different artists.


== History ==
Dick Smith, a native of Honesdale, Pennsylvania, was reportedly inspired to write the song after seeing Honesdale's Central Park covered in snow. Smith had written the lyrics while in the West Mountain Sanitarium, being treated for tuberculosis, better known then as consumption. The West Mountain Sanitarium is located off N. Sekol Ave. in Scranton, Pennsylvania.
The original recording was by Richard Himber and his Hotel Ritz-Carlton Orchestra on RCA Bluebird in 1934. At the end of a recording session with time to spare, it was suggested that this new tune be tried with an arrangement provided by the publisher. This excellent "studio" orchestra included many great New York studio musicians including the legendary Artie Shaw. The biggest chart hit at the time of introduction was Guy Lombardo's orchestra, a top ten hit. Singer-songwriter Johnny Mercer took the song to #4 in Billboard's airplay chart in 1946. The same season, Perry Como hit the retail top ten. Como would record a new version for his 1959 Christmas album.
Due to its seasonal theme, "Winter Wonderland" is often regarded as a Christmas song in the Northern Hemisphere, although the holiday itself is never mentioned in the lyrics. There is a mention of "sleigh-bells" several times, implying that this song refers to the Christmas period. In the Swedish language lyrics, Vår vackra vita vintervärld, the word tomtar is mentioned.


== Awards and achievements ==
In November 2007, ASCAP, a performance rights organization in the United States, listed "Winter Wonderland" as the most-played ASCAP-member-written holiday song of the previous five years, and cited the Eurythmics' 1987 version of the song is the one most commonly played.


== References ==


== External links ==
Full lyrics of this song at MetroLyrics