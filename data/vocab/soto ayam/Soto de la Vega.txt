Soto de la Vega, Soutu de la Veiga, in Leonese language, is a municipality located in the province of León, Castile and León, Spain. According to the 2007 census (INE), the municipality has a population of 1,888 inhabitants.


== §See also ==
Tierra de La Bañeza
Leonese language
Kingdom of León