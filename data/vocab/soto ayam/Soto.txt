Soto may refer to:
Sōtō, one of the two major Japanese Zen sects
Soto (food) or Coto, Indonesian dish
So'to, indigenous people of the Amazon


== Geography ==
Soto, California, former settlement in Butte County
Soto, Curaçao, Netherlands Antilles
Soto (Aller), parish in Asturias, Spain
Soto (Las Regueras), parish in Asturias, Spain
Soto de Cerrato, municipality in Palencia Province, Spain
Soto de la Vega, municipality in León Province, Spain
Soto de los Infantes, parish in Asturias, Spain
Soto de Luiña, parish in Asturias, Spain
Soto del Barco (parish), parish in Asturias, Spain
Soto del Real, municipality in Madrid Province, Spain
Soto la Marina, Tamaulipas, municipality in Mexico
Soto Street, in Los Angeles, California
Soto y Amío, municipality in León Province, Spain
Soto, Russia, a rural locality (a selo) in Megino-Kangalassky District of the Sakha Republic, Russia


== Given name ==
Sotirios Kyrgiakos (born 1979), Greek and Liverpool FC footballer
Soto Grimshaw (1833–1900), Argentine naturalist


== Surname ==
Alberto Soto (born 1990), Mexican footballer
Alejandro Morera Soto (1909–1995), Costa Rican footballer
Apolinar de Jesús Soto Quesada (1827–1911), Costa Rican politician
Bernardo Soto Alfaro (1854–1931), president of Costa Rica
Blanca Soto (born 1979), Mexican model
Caitro Soto (1934–2004), Peruvian musician
Carlos Soto Arriví (1959–1978), Puerto Rican independence activist
Cecilia Soto, Mexican politician
Cesar Soto (boxer) (born 1971), Mexican boxer, former WBC featherweight champion
Clemente Soto Vélez (1905–1993), Puerto Rican writer and journalist
Cynthia Soto, American politician
Daniel Garcia Soto, Puerto Rican wrestler
Darren Soto (born 1978), American politician
Eddie Soto (born 1972), American soccer player
Elías M. Soto (1858–1944), Colombian musician and composer
Elkin Soto (born 1980), Colombian footballer
Felix Soto Toro (born 1967), Puerto Rican astronaut
Francisco Puertas Soto (born 1963), Spanish rugby player
Freddy Soto (1970–2005), American comedian
Gabriel Soto (born 1975), Mexican model and actor
Gary Soto (born 1952), American author and poet
Geovany Soto (born 1983), Puerto Rican baseball player
Héctor Soto (born 1978), Puerto Rican volleyball player
Hernando de Soto (born 1496), Spanish explorer who was recorded to be the first European to cross the Mississippi River
Humberto Soto (born 1980), Mexican boxer
Iván Hernández Soto (born 1980), Spanish footballer
Iván Sánchez-Rico Soto (aka Riki; b. 1980), Spanish footballer
Jafet Soto (born 1976), Costa Rican footballer
Jaime Soto (born 1955), American Roman Catholic bishop
Jay Soto, American jazz guitarist
Jeff Soto (born 1975), American artist
Jeff Scott Soto (born 1965), American singer
Jesús Rafael Soto (1923–2005), Venezuelan artist
Jock Soto, former New York City Ballet principal dancer
Joel Soto (born 1982), Chilean footballer
Jorge Soto (footballer) (born 1971), Peruvian footballer
Jorge Soto (golfer) (born 1945), Argentine golfer
Jorge Azanza Soto (born 1982), Spanish bicycle racer
José Soto (born 1970), Peruvian footballer
Jose Chemo Soto, Puerto Rican politician
Jose Luis de Quintanar Soto y Ruiz (1772–1837), Mexican military officer
Josu De Solaun Soto (born 1982), Spanish pianist
Lindsay Soto (born 1976), American sports journalist
Lornna Soto (born 1970), Puerto Rican politician
Luis Gutiérrez Soto (1890–1977), Spanish architect
Manuel Ángel Núñez Soto (born 1951), Mexican politician
Marco Aurelio Soto (1846–1908), President of Honduras
Mario Soto (baseball) (born 1956), Dominican baseball player
Mario Soto (footballer) (born 1950), Chilean footballer
Máximo Soto Hall, Guatemalan novelist
Miriam Blasco Soto (born 1963), Spanish judoka
Nell Soto (1926–2009), American politician
Onell Soto (born 1932), American Episcopal bishop
Pablo Soto (born 1979), Spanish computer scientist
Pedro Blanco Soto (1789–1825), President of Bolivia
Pedro Juan Soto (1928–2002), Puerto Rican writer
Roberto Soto, Puerto Rican wrestler
Rodolfo Campo Soto (born 1942), Colombian politician
Santiago Cervera Soto (born 1965), Spanish politician
Steve Soto (born 1963), American musician
Talisa Soto (born 1967), American model and actress
Victoria Leigh Soto (1985–2012), American teacher and murder victim


== See also ==
De Soto (disambiguation)
Sotho (disambiguation)