Spanish guitar may refer to:
Classical guitar, a six-stringed guitar with nylon strings
Flamenco guitar, similar to a classical guitar
"Spanish Guitar" (song), a 2000 song by Toni Braxton
"Spanish Guitar", a 1971 song by Gene Clark from his album White Light
"Spanish Guitar", a 1978 song by Gary Moore from his album Back on the Streets
Duets with Spanish Guitar, a 1958 album by Laurindo Almeida with Salli Terri and Martin Ruderman