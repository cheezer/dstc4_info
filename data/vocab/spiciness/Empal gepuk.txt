Empal gepuk (Sundanese) or sometimes simply known just as empal or gepuk is an Indonesian sweet and spicy fried beef dish. This dish is commonly popular in Java island, but can trace its origin to Sundanese cuisine of West Java, Indonesia.
The beef preferably used in this dish is shank part. First the beef is boiled until medium well, then being cut quite thick along the muscle fiber into half-palm size. Then using stone pestle and mortar, the beef pieces are beaten mildly to loosen the meat fibers and spread its size a little bit. The spices used in this dish are ground shallot, garlic, chili pepper, coriander, palm sugar and salt, mixed with bruised lemongrass, galangal, daun salam (Indonesian bay leaf), a little coconut milk and turmeric water. The meat pieces are cooked with the spices well until the spice are absorbed into the meat and the stock evaporate. Then the meat pieces are fried in coconut oil until the color darken and the meat is done. Empal gepuk is sprinkled with bawang goreng (fried shallot) and served with steamed rice. This fried beef dish tastes succulent with mild sweetess acquired from palm sugar and a hint of spiciness.


== See also ==
Empal gentong
Rendang


== References ==


== External link ==
Empal gepuk recipe