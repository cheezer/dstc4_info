The I Beach South American Games was a multi-sport event held from 2 to 13 December 2009 in Montevideo and Punta del Este, Uruguay. The Games was organized by the South American Sports Organization (ODESUR).


== Medal count ==
The medal count for these Beach Games is tabulated below. This table is sorted by the number of gold medals earned by each country. The number of silver medals is taken into consideration next, and then the number of bronze medals.


== Sports ==
Beach handball
Beach rugby
Beach soccer
Beach volleyball
Fitness (demonstration)
Marathon swimming
Sailing
Surfing
Triathlon
Water ski


== External links ==
Official website