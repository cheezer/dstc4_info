A town square is an open public space commonly found in the heart of a traditional town used for community gatherings. Other names for town square are civic center, city square, urban square, market square, public square, piazza, plaza, and town green.
Most town squares are hardscapes suitable for open markets, music concerts, political rallies, and other events that require firm ground. Being centrally located, town squares are usually surrounded by small shops such as bakeries, meat markets, cheese stores, and clothing stores. At their center is often a fountain, well, monument, or statue. Many of those with fountains are actually called fountain square.


== Urban planning ==
In urban planning, a city square or urban square is a planned open area in a city, usually or originally rectangular in shape.

Red Square in Moscow was originally used as an outdoor marketplace and later became the stage for Soviet military parades and May Day demonstrations.
Palace Square in St Petersburg was designed to be the central square of Imperial Russia and ironically became the setting of revolutionary protests that led to the overthrow of monarchy during the February Revolution of 1917.
Similarly, Beijing's Tiananmen Square was the scene of both communist parades and anti-government protests.
John-F.-Kennedy-Platz (formerly Rudolph-Wilde-Platz) was the site of the West Berlin town hall and John F. Kennedy's famous Ich bin ein Berliner speech.
New York City's Times Square as well as Bryant Park; Washington, D.C.'s National Mall; or Kennedy Plaza in Providence often fill this role for the United States.
Trafalgar Square in London does the same for the United Kingdom.
Saint Peter's Square in Vatican City, the papal enclave within Rome, Italy.
Yonge-Dundas Square in Toronto is a renowned and famous square in Canada.
Nathan Phillips Square is a popular square in front of Toronto's landmark City Hall.
Hviezdoslavovo námestie is one of the best-known squares in Bratislava (Slovakia) and a centre of a social life.
Dam Square in Amsterdam for the Netherlands.
Three squares: Main Market Square, Kraków, Town Square in Piotrków Trybunalski and Castle Square, Warsaw for Poland.
The City Hall Square, Copenhagen for Denmark
Trg Republike, Belgrade for Serbia
Praça do Comércio (or Commerce Square), in Lisbon, Portugal, was formerly known as the Terreiro do Paço (Palace Court). It was destroyed after the 1755 Lisbon earthquake but was rebuilt and renamed to indicate its new function in the economy of Lisbon. The symmetrical buildings around the square hold government bureaus and ministries.
Wenceslas Square is one of the main city squares in the New Town of Prague, Czech Republic.


== China ==
In Mainland China, People's Square is a common designation for the central town square of modern Chinese cities, established as part of urban modernization within the last few decades. These squares are the site of government buildings, museums and other public buildings. The probably best-known and largest such square in China is Tienanmen Square.


== Germany ==
The German word for square is Platz, which also means "Place", and is a common term for central squares in German-speaking countries. These have been focal points of public life in towns and cities from the Middle Ages to today. Squares located opposite a Palace or Castle (German: Schloss) are commonly named Schlossplatz. Prominent Plätze include the Alexanderplatz, Pariser Platz and Potsdamer Platz in Berlin, Heldenplatz in Vienna, and the Königsplatz in Munich.


== Italy ==

A piazza (Italian pronunciation: [ˈpjattsa]) is a city square in Italy, Malta, along the Dalmatian coast and in surrounding regions. The term is roughly equivalent to the Spanish plaza. In Ethiopia, it is used to refer to a part of a city.
When the Earl of Bedford developed Covent Garden – the first private-venture public square built in London – his architect Inigo Jones surrounded it with arcades, in the Italian fashion. Talk about the piazza was connected in Londoners' minds not with the square as a whole, but with the arcades.
A piazza is commonly found at the meeting of two or more streets. Most Italian cities have several piazzas with streets radiating from the center. Shops and other small businesses are found on piazzas as it is an ideal place to set up a business. Many metro stations and bus stops are found on piazzas as they are key point in a city.
In Britain, piazza now generally refers to a paved open pedestrian space, without grass or planting, often in front of a significant building or shops. King's Cross Station in London is to have a piazza as part of its redevelopment. The piazza will replace the existing 1970s concourse and allow the original 1850s façade to be seen again. There is a good example of a piazza in Scotswood at Newcastle College.
In the United States, in the early 19th century, a piazza by further extension became a fanciful name for a colonnaded porch. Piazza was used by some, especially in the Boston area, to refer to a verandah or front porch of a house or apartment.
Piazza is a common last name for Italians and Italian-Americans. The name grew out of the region surrounding Venice. Populations of Piazza live in Calabria, Sicily, and Venice.
A central square just off Gibraltar's Main Street, between the Parliament Building and the City Hall officially named John Mackintosh Square is colloquially referred to as The Piazza.


== Indonesia ==
A large open square common in villages, towns and cities of Indonesia is known as alun-alun. It is a Javanese term which in modern-day Indonesia refers to the two large open squares of kraton compounds. It is typically located adjacent a mosque or a palace. It is a place for public spectacles, court celebrations and general non-court entertainments.


== Iran ==

In traditional Persian architecture, town squares are known as maydan or meydan. A maydan is considered as one of the essential features in urban planning and they are often adjacent to bazaars, large mosques and other public buildings. Naqsh-e Jahan Square in Isfahan and Azadi Square in Tehran are examples of classic and modern squares.


== The Low Countries ==
Squares are often called "markt" because of the usage of the square as a market place. Almost every town in Belgium and the southern part of the Netherlands has a "Grote Markt" (for example "Grote Markt" in Brussels) or "Grand Place" in French. The "Grote Markt" is often the place where the town hall is situated and therefore the centre of the town.


== Russia ==

In Russia, central square (Russian: центра́льная пло́щадь, romanised: tsentráĺnaya plóshchad́) is a common term for an open area in the heart of the town used for community gatherings. Often, it has no official name or is informally referred to as Central Square. The name of the town can be added for precision. Central Squares are usually located opposite the administration building or some major landmark like a Great Patriotic War memorial or a cathedral.


== United Kingdom ==
In the United Kingdom, and especially in London and Edinburgh, a "square" has a wider meaning. There are public squares of the type described above but the term is also used for formal open spaces surrounded by houses with private gardens at the centre, sometimes known as garden squares. Most of these were built in the 18th and 19th centuries. In some cases the gardens are now open to the public. See the Squares in London category. Additionally, many public squares were created in towns and cities across the UK as part of urban redevelopment following the Blitz. Squares can also be quite small and resemble courtyards, especially in the City of London.


== United States ==

In the United States, a town square typically consists of a park or plaza in front of the original county courthouse or town hall.
In some cities, especially in New England, the term "square" (as its Spanish equivalent, Plaza) is applied to a commercial area (like Central Square in Cambridge, Massachusetts), usually formed around the intersection of three or more streets, and which originally consisted of some open area (many of which have been filled in with traffic islands and other traffic calming features). Many of these intersections are irregular rather than square.
In newer cities that have been planned mostly around the use of automobiles, public squares are rare and tend to be limited to the older central districts. The social utility of such public spaces can be severely curtailed by the high cost of downtown parking; at the same time, the high demand for parking has historically led to the proliferation of street-level parking lots which has further diminished the potential availability of open pedestrian space. Another deterrent is the perception of the urban core as a particularly dangerous crime center, whether the notion is justified or not.
Throughout North America, words like place, square, or plaza frequently appear in the names of commercial developments such shopping centers and hotels.


== See also ==
Cathedral Square
List of city squares
List of city squares by size
Piazza
Plaza
Village green


== References ==