Sri or Shri may refer to:
Dewi Sri, ancient Javanese and Balinese goddess of rice and domestic prosperity
Lakshmi, the Hindu goddess of wealth
Shree (raga), a North Indian musical scale
Shree ragam, a Carnatic musical scale
Shri (musician), a.k.a. Shrikanth Sriram, in the British Asian music scene
Sri, a Hindu honorific
SRi, a car specification badge
Sri (1999 film), a 1999 Indonesian film directed by Marselli Sumarno
Sri (2005 film), a 2005 Telugu-language Indian film directed by Dasaradh
Sri Kommineni, a South Indian music director, Singer who works mainly in Tollywood
Sri Lanka, an island state at the south tip of India, formerly called Ceylon
Sri Sri, a more emphatic version of the honorific Sri
Sri Sri (writer), Telugu poet and writer (Srirangam Srinivasarao)
SRI may refer to:
Sacrum Romanum Imperium, the Holy Roman Empire
Samarinda Airport or Temindung Airport, IATA code
Seafarers' Rights International, a human rights organisation
Selection Research, Inc. (SRI) that acquired and merged with the Gallup Organization in 1988
Send Routing Info, a Global Title request in the Signalling Connection Control Part of Signaling System 7
Serikat Rakyat Independen, Union of (Indonesian) Independent People Party
Serotonin reuptake inhibitor
Servicio de Rentas Internas, Ecuador's Internal Revenue Service
Serviciul Român de Informaţii, the Romanian domestic intelligence service
Siena Research Institute
Silsoe Research Institute
Socially Responsible Investment, also called Sustainable Responsible Investment
Socorro Rojo Internacional
Solar Reflectance Index, a method of evaluating a roof's ability to reject solar heat
Sound Reduction Index, a measurement used in construction and acoustics
Southern Research Institute
Space Research Institute of the Russian Academy of Sciences
Space Research Institute – Institut für Weltraumforschung of the Austrian Academy of Sciences
SRI or Sorcin, a human gene
SRI International, also known as the Stanford Research Institute, one of the world's largest contract research institutes.
Standard Register Industrial, a communications management company of Dayton, Ohio
Sunnybrook Research Institute, the research component of Sunnybrook Health Sciences Centre in Toronto, Canada
Surveillance, reconnaissance, and intelligence
Swiss Radio International
System of Rice Intensification, a method of increasing the yield of rice produced in farming


== See also ==
All pages beginning with "Sri"
Selective serotonin reuptake inhibitor (SSRI)
Serotonin-norepinephrine reuptake inhibitor (SNRI)
Shree (disambiguation)