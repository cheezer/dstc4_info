The Prime Minister of Sri Lanka is a member of the parliment and the head of government in Sri Lanka.


== History ==

The post of Prime Minister of Ceylon was created in 1947 prior to independence from Britain and the formation of the Dominion of Ceylon in 1948. United National Party leader D. S. Senanayake became the first Prime Minister of Then Ceylon in 1947 after independence. In 1972 when Sri Lanka became a republic the name of the post changed to Prime Minister of Sri Lanka. With a Westminster-based political system established the Prime Minister was the head of government therefore held the most powerful political office of the country at the time. This changed with a constitutional change in 1978, when the Executive Presidency was created, making the President both head of state and head of government. Until 1978 the Prime minister was also the Minister of Defence and External Affairs. The Prime Minister is appointed by the President as a member of the cabinet of ministers. In the event the president dies in office the Prime Minister becomes the acting president until Parliament convenes to elect a successor or new elections could be held to elect a new president. This was the case with H.E. President Dingiri Banda Wijetunge. United National Party leaders Dudley Senanayake and Ranil Wickramasinghe together with Sri Lanka Freedom Party leader Sirimavo Bandaranaike was appointed three times to the position. With passing of the 19th amendment to the constitution in 2015, the prime minister was granted more powers when appointing ministers and leading the cabinet.
The current Prime Minister of Sri Lanka is Ranil Wickremesinghe, he was appointed by President Maithripala Sirisena on 9 January 2015. This was the third time that Wickramasinghe was appointed Prime Minister of Sri Lanka. On 28 April 2015 the parliment approuved the Nineteenth Amendment to the Constitution of Sri Lanka by givin the power of the Government of Sri Lanka to the prime minister and the president of sri lanka remains the head of state and commander in chief.


== List of Sri Lankan Prime Ministers ==


== Official residence and office ==
The official residence of the prime minister is the Prime Minister's House most commonly referred to as Temple Trees. The Prime Minister's Office is located on Sir Ernest de Silva Mawatha (formerly known as Flower Road) in Colombo.


== See also ==
President of Sri Lanka
Prime Minister's Office


== References ==


== External links ==
Parliament of Sri Lanka - Handbook of Parliament, Prime Ministers