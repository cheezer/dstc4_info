ST, St, St. or st may refer to:
St, or St., a contraction of Saint, especially in Christianity
St, or St., a contraction of Street, a public thoroughfare
St, or St., a contraction of Strait, a body of water
Short ton, a unit of mass equal to 2000 lb, or one ton in the United States
Stere, a unit of volume equivalent to one cubic meter, used to measure cordwood
St, Stoke as a unit for viscosity
Stone (weight), a unit of mass used in the British Isles and other countries
in ordinal number abbreviations, such as 1st and 21st
s.t., abbreviation of "subject to" or "such that" to indicate constraints


== Academia ==
s. t. (sine tempore, Latin for "without time") indicates that a lecture will begin at the exact time; this is in contrast to "c.t." (cum tempore, Latin for "with time"); see Academic quarter


== Business and Organization ==
St or StA, symbol meaning Common stock (US) or ordinary shares (British) on the German Stock Exchange
Statstjänstemannaförbundet, or Swedish Union of Civil Servants, a trade union
Secret Team, an alleged covert alliance between the CIA and American industry
STMicroelectronics, a worldwide manufacturer of semiconductors


== Computer ==
.st, Internet country code top-level domain for São Tomé and Príncipe
ST, the ISO 3166-1 country code for São Tomé and Príncipe
ST connector, a type of optical fiber connector
Atari ST, a personal computer
Internet Stream Protocol, an experimental Internet protocol
Spread Tow, a technology for spreading a tow to make ultra light fabric for the composites industry
State Threads, a user-space threading C-library, especially for network-applications
Structured text, a high-level programming language that syntactically resembles Pascal
SpeedTouch, a networking equipment company
Sublime Text, a cross-platform text and source code editor


== Language ==
ﬅ, or st, a typographic ligature
ST, the ISO language code for the Sesotho language
stet, a printer's proofing mark on manuscripts
Standard Theory, in generative grammar


== Mathematics, medicine and science ==
Speech therapy, a treatment in Speech-Language Pathology
ST segment, the part of an electrocardiogram connecting the QRS complex and the T wave
String theory, a branch of theoretical physics
Stanton number, a term used in physics
Strouhal number, a term used in fluid mechanics
Standard part function, a term used in non-standard analysis
Stratosphere-Troposphere boundary in the atmosphere


== Media, music, and entertainment ==
st., abbreviation for stanza
School Tycoon, a video game
Self-titled, a term used in the record industry for eponymous albums
The Sims 3: Showtime, the sixth expansion pack for The Sims 3
The Legend of Zelda: Spirit Tracks, a video game
Storm Track, an early magazine about storm chasing
Star Trek, a television and film franchise
Suicidal Tendencies, an American heavy metal/hardcore punk band


== Transport ==
ST, IATA transport code for Germania (airline)
ST, vehicle license plate code and local abbreviation for the city of Split, Croatia
ST, railroad reporting mark for the Springfield Terminal Railway (Vermont)
ST, abbreviation for the Maharashtra State Road Transport Corporation
Sound Transit, the popular name of Washington state's Central Puget Sound Regional Transit Authority
Steam tug, ship class prefix
Student Transportation, Inc., a school bus contractor
Suffolk County Transit, or Suffolk Transit, the bus system serving Suffolk County, New York


== Other uses ==
Septic tank, on real estate documents