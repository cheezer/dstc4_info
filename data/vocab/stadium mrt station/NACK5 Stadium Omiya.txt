NACK5 Stadium Omiya (ナックファイブスタジアム大宮, Nakku-faibu Sutajiamu Ōmiya) is a football (soccer) stadium located in Ōmiya-ku, Saitama city, Saitama Prefecture, Japan. It is the home stadium of a J.League club Omiya Ardija.
Since May 14, 2007 it has been called NACK5 Stadium Ōmiya (ナックファイブスタジアム大宮, Nakku-faibu Sutajiamu Ōmiya) for the naming rights.


== History ==
Built in 1960, it was one of the first stadia in Japan dedicated to the code. The grandstands were added to host several matches of 1964 Summer Olympics and 1967 National Sports Festival of Japan. The stadium used to accommodate 12,500 spectators.
In 2006-2007 it was closed for expansion works to meet the J. League Division 1 requirements for Ardija to host its home matches. Ardija used Saitama Stadium 2002 and Urawa Komaba Stadium until works were complete.
From May 14, 2007 it would be called NACK5 Stadium Ōmiya (ナックファイブスタジアム大宮, Nakku-faibu Sutajiamu Ōmiya) to reflect a six-year sponsorship from FM NACK5 (エフエムナックファイブ, Efu Emu Nakku-Faibu) (JODV-FM, 79.5 MHz), an independent commercial radio station based in Ōmiya-ku and covering Saitama Prefecture.
The expansion works were complete in October 2007 and since it accommodates 15,500 spectators.
On November 11, the re-opening match was held as a J. League season match between the Ardija and Ōita Trinita (1-2).
A fun fact about the stadium is that this is the venue for the High School National Championships on the soccer manga-anime "Captain Tsubasa"


== References ==
1964 Summer Olympics official report. Volume 1. Part 1. p. 133.
Omiya Ardija stadium


== External links ==
Official website
FM NACK5