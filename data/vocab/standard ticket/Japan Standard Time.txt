Japan Standard Time or JST (日本標準時 Nihon Hyōjunji or 中央標準時 Chūō Hyōjunji) is the standard timezone in Japan, and is 9 hours ahead of UTC, i.e. it is UTC+09:00. There is no daylight saving time, though its introduction has been debated several times. During World War II, it was often called Tokyo Standard Time.
Japan Standard Time is the same as Korean Standard Time, Indonesian Eastern Standard Time and Yakutsk Time.


== History ==
Before the Meiji era (1868–1912), each local region had its own timezone in which noon was when the sun was exactly at its zenith. As modern transportation like trains was adopted, this practice became a source of confusion. For example, there is a difference of about 5 degrees longitude between Tokyo and Osaka and because of this, a train that departed from Tokyo would arrive at Osaka 20 minutes ahead of the time in Tokyo. In 1886, Ordinance 51 was issued in response to this problem, which stated:

Ordinance 51 (on the precise calculation of time using the Prime Meridian) - July 13, 1886
The prime meridian passes through England's Greenwich Observatory.
Longitudes are calculated using the prime meridian, counting 180 degrees either east or west. Positive degrees are east, negative degrees are west.
On January 1, 1888, 135 degrees east longitude will be set as the standard meridian for all of Japan, allowing precise times to be fixed.

According to this, the standard time (標準時, Hyōjunji) was set 9 hours ahead of GMT (UTC had not been established yet). In the ordinance, the first clause mentions GMT, the second defines east longitude and west longitude and the third says the standard timezone would be in effect from 1888. Coincidentally, the city of Akashi in Hyōgo Prefecture is located exactly on 135 degrees east longitude and subsequently became known as Toki no machi (Town of Time).
With the annexation of Taiwan in 1895, Ordinance 167 (pictured on the right) was issued to rename the previous Standard Time to Central Standard Time (中央標準時, Chūō Hyōjunji) and establish new Western Standard Time (西部標準時, Seibu Hyōjunji) at 120° longitude. While Korea came under Japanese rule in 1910, Korea Standard Time of UTC+8:30 continued to be used until 1912, when it was changed to Central Standard Time.
Western Standard Time, which was used in Taiwan and some parts of Okinawa, was abolished by ordinance 529 in 1937 and replaced by Central Standard Time in those areas. Territories occupied by Japan during World War II, including Singapore and Malaya, adopted Japan Standard Time for the duration of their occupation, but reverted after Japan's surrender.
In 1948–1951 occupied Japan observed daylight saving time (DST) from the first Sunday in May at 02:00 to the second Saturday in September at 02:00, except that the 1949 spring-forward transition was the first Sunday in April. More recently there have been efforts to bring back DST in Japan, but so far this has not happened.


== Time zones of the Japanese Empire ==
The two-time-zone system was implemented in Japan between January 1896 and September 1937:
From October 1937, Central Standard Time was also used in western Okinawa and Taiwan.


== IANA time zone database ==
The IANA time zone database contains one zone for Japan in the file zone.tab, named Asia/Tokyo.


== Daylight saving time in Japan ==
From 1948-51, Japan observed DST between May and September every year under an initiative of the U.S.-led occupation army. The unpopularity of DST, for which people complained about sleep disruption and longer daytime labor (some workers had to work from early morning until dusk) caused Japan to abandon DST in 1952, shortly after its sovereignty was restored upon the coming into effect of the San Francisco Peace Treaty. Since then, DST has never been officially implemented nationwide in Japan.
Starting in the late 1990s, a movement to reinstate DST in Japan gained some popularity, aiming at saving energy and increasing recreational time. The Hokkaido region is particularly in favor of this movement because daylight starts as early as 03:30 (in standard time) there in summer due to its high latitude and its location near the eastern edge of the time zone, with much of the region's solar time actually closer to UTC+10:00. In the early 2000s, a few local governments and commerce departments promoted unmandated hour-earlier work schedule experiments during the summer without officially resetting clocks.
The Council on Economic and Fiscal Policy is expected to propose that the Japanese government begin studying DST in an attempt to help combat global warming. Japanese PM Shinzo Abe made a significant effort to introduce daylight saving time, but was ultimately unsuccessful. However, it is not clear that DST would conserve energy in Japan. A 2007 simulation estimated that introducing DST to Japan would increase energy use in Osaka residences by 0.13%, with a 0.02% saving due to lighting more than outweighed by a 0.15% increase due to cooling costs; the simulation did not examine non-residential buildings.


== See also ==
JJY
Japanese calendar
UTC+09:00
Japanese clock


== References ==