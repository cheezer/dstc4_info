The Port Stanley Terminal Rail (PSTR) is a heritage railway that passes over the historic tracks of The London and Port Stanley Railway (L&PS) between Port Stanley and St. Thomas, Ontario. The tourist trains began operating in 1983, after volunteers started maintaining the abandoned L&PS train corridor.


== History ==

The first passenger train reached Port Stanley on July 5, 1856. Use of the line increased until 1943, when the end of gas rationing and the increased use of automobiles caused a slow decline in passenger traffic. On February 1, 1957, passenger service ended on the L&PS line.
Afterwards, the railway continued to carry freight traffic, especially between St. Thomas and London, Ontario. The rail section between St. Thomas and Port Stanley fell into disrepair and was finally abandoned in 1982 after a washout. When the line was officially abandoned, a group of railway preservationists created the Port Stanley Terminal Rail Inc. and purchased the rail to be used as a heritage railway. After rebuilding the tracks, the group finally received a Provincial railway charter to operate trains between the cities of St. Thomas and Port Stanley in 1987. This was the first charter issued in Ontario in several decades.


== Recognition ==
In 2012, the Port Stanley Terminal Rail was inducted to the North America Railway Hall of Fame. The PSTR was recognized for its contribution to railroading as a "Community, Business, Government or Organization" in the "Local" category (pertaining specifically to the area in and around St. Thomas, Ontario.)


== Present day ==

Currently, the railway has four historic diesel electric locomotives from the '40s and '50s and nine passenger cars. Trains leave the Port Stanley railway station on most weekends (and daily during July and August except Monday and Tuesday) for an hour-long ride that ends up just south of St. Thomas. Additionally, a number of special train rides are scheduled throughout the year, like the Santa Express which runs in December and the Murder Mystery series.
The PSTR railway is classed as a tourist railway and all passenger boarding is done in Port Stanley.
The line is run and maintained through a volunteer effort.


== See also ==
List of heritage railways in Canada
London and Port Stanley Railway
North America Railway Hall of Fame


== References ==


== External links ==
Port Stanley Terminal Rail's official website
North America Railway Hall of Fame: Port Stanley Terminal Rail