Stanley No. 215 (2006 Population 509 ) is a rural municipality in south-eastern Saskatchewan, Canada encompassing 820.81 square kilometers in area. The rural municipality maintains its office in Melville, Saskatchewan. The rural municipality in conjunction with the provincial government is in charge of maintenance of highways in its area. As well, the municipality provides policing, fire protection and municipal governance for the rural district, with a reeve as its administrator.


== Statistics ==


== References ==