Chicken fried steak (also known as country fried steak) is an American breaded cutlet dish consisting of a piece of steak (tenderized cube steak) coated with seasoned flour and pan-fried. It is associated with the Southern cuisine of the United States.
Chicken fried steak resembles the Austrian dish Wiener Schnitzel and the Italian-South American dish Milanesa, which is a tenderized veal or pork cutlet, coated with flour, eggs, and bread crumbs, and then fried. It is also similar to the recipe for Scottish collops.


== HistoryEdit ==
The precise origins of the dish are unclear, but many sources attribute its development to German and Austrian immigrants to Texas in the 19th century, who brought recipes for Wiener Schnitzel from Europe to the USA. Lamesa, the seat of Dawson County on the Texas South Plains, claims to be the birthplace of chicken fried steak, and hosts an annual celebration accordingly.
The Virginia Housewife, published in 1838 by Mary Randolph, has a recipe for veal cutlets that is one of the earliest recipes for a food like chicken fried steak. The recipe for what we now know as chicken fried steak was included in many regional cookbooks by the late 19th century. The Oxford English Dictionary's earliest attestation of the term "chicken-fried steak" is from a restaurant advertisement in the 19 June 1914 edition of the Colorado Springs Gazette newspaper.
A 1943 American cookbook recipe for Wiener Schnitzel includes a white salt and pepper cream gravy.
Chicken fried steak is among numerous popular dishes which make up the official state meal of Oklahoma, added to the list in 1988.


== PreparationEdit ==
Chicken fried steak is prepared by taking a thin cut of beefsteak and tenderizing it by pounding, cubing, or forking. It is then either immersed in egg batter and/or dredged in flour to which salt, pepper, and often other seasonings have been added (called breading). After this, the steak is fried in a skillet or, less commonly, deep-fried. Restaurants often call the deep fried version chicken fried and the pan fried type country fried. The frying medium has traditionally been butter, lard, or other shortening, but in recent years, health concerns have led most cooks to substitute the shortening with vegetable oil.
The cuts of steak used for chicken fried steak are usually the less expensive, less desirable ones, such as chuck, round steak, and occasionally flank steak. The method is also sometimes used for chopped, ground, or especially cube steak. When ground beef is used, it is sometimes called a "chuckwagon". Chicken fried steak is usually served for lunch or dinner topped with cream gravy and with mashed potatoes, vegetables, and biscuits served on the side. In the Midwest, it is also common to serve chicken fried steak for breakfast, along with toast and hash browns.
The steak can be served on a hamburger bun as a sandwich, cubed and stuffed in a baked potato with the gravy and cheese, or cut into strips and served in a basket with fries and gravy, which is then known as "steak fingers".


== VariantsEdit ==

Typically, in Texas and surrounding states, chicken fried steak is fried in a thick layer of oil in a pan and served with traditional peppered milk gravy. The same dish is sometimes known as "country fried steak" in other parts of the United States, where it is subject to some regional variations. Often there is a brown gravy, and occasionally the meat is either pan-fried with little oil, or simmered in the gravy. In some areas, "country steak" may refer to Salisbury steak, a chopped or minced beef patty in brown gravy.
Other meats may be used, with "chicken-fried chicken" having appeared on many menus, substituting a boneless chicken breast for the steak. Chicken fried chicken differs from the dish known as "fried chicken" because the meat is removed from the bones, and cooked in the fashion of chicken fried steak. Another term is "steak-fried chicken". Boneless pork chops, usually center cut, can be served in this manner, as well as chicken fried buffalo steak.


== See alsoEdit ==
Chicken fried bacon
City chicken
Cotoletta
List of regional dishes of the United States
Milanesa
Parmo
Parmigiana
Schnitzel
Tonkatsu
Steak
 Food portal


== ReferencesEdit ==


== External linksEdit ==
Country Fried Steak at The Food Timeline
Chicken Fried Steak in 'Texas Cooking Magazine'