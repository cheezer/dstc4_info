Strand Theatre or Strand Theater may refer to:


== England ==
Royal Strand Theatre, London
Novello Theatre, London (called Strand Theatre from 1909 to 1911, and 1913 to 2005)
in the United States (by state then city)
Strand Theatre, former movie house in Ocean Beach, San Diego, California, built in 1925
Strand Theatre (San Francisco), reopening in 2015 now ownded by American Conservatory Theatre, originally opened in 1917 and shuttered in 2003, in San Francisco, California
Strand Theatre (Jennings, Louisiana), listed on the National Register of Historic Places in Jefferson Davis Parish, Louisiana
Strand Theatre (Shreveport, Louisiana), listed on the National Register of Historic Places in Caddo Parish, Louisiana
Strand Theatre (Rockland, Maine), listed on the National Register of Historic Places in Knox County, Maine
Strand Theatre, Brockton, Massachusetts, site of a fire that killed 13 firefighters in 1941
Strand Theatre (Dorchester, Massachusetts), a restored vaudeville house located in Uphams Corner in Dorchester, Massachusetts
Strand Theater (Lexington, Mississippi), a Mississippi Landmark
Strand Theater (Louisville, Mississippi), a Mississippi Landmark
Strand Theater (Manchester, New Hampshire), theater portion destroyed by fire in 1985
Strand Theatre (Lakewood, New Jersey), listed on the National Register of Historic Places in Ocean County, New Jersey
Strand Theatre (Brooklyn), a former vaudeville house now home to BRIC Arts and UrbanGlass
Strand Theatre (Ithaca, New York), listed on the National Register of Historic Places in Tompkins County, New York
Strand Theatre (Manhattan), demolished Broadway movie theatre opened in 1914
Strand Theater (Plattsburgh, New York), listed on the National Register of Historic Places in Clinton County, New York
Strand Theatre (Grafton, North Dakota), listed on the National Register of Historic Places in Walsh County, North Dakota
Strand Theatre (Delaware, Ohio), movie theatre opened in 1916
Strand Theater (Zelienople, Pennsylvania), a community theatre
Strand Theater (Washington, D.C.), listed on the National Register of Historic Places in Washington, D.C.
Strand Theatre (Moundsville, West Virginia), home of the Wheeling Jamboree radio program
Strand Theater (Allentown, Pennsylvania), a former cinema in Allentown, Pennsylvania. It closed in 1953. Today, part of the building is used for retail and office space
Strand Theatre (Marietta, GA), a performing arts and film center in Marietta, Georgia, United States of America