A studio school is a type of secondary school in England that is designed to give students practical skills in workplace environments as well as traditional academic and vocational courses of study. Like traditional schools, studio schools teach the National Curriculum and offer academic and vocational qualifications. However studio schools also have links to local employers and offer education related to the world of work.


== Description ==
Studio Schools are a type of Free School. They are part of the Academies Programme, and are funded by the taxpayer, non-selective, free to attend and not controlled by a local education authority. While this is also true of most other academies and free schools, Studio Schools are collectively distinctive in a number of ways. Studio Schools are sponsored by existing schools, colleges, and community groups. However, existing schools cannot convert to become a Studio School - all Studio Schools have to be stand alone schools with no direct transfer intake of pupils. Studio Schools are designed to be small, with a maximum of 300 students, which enables them to foster a supportive, personalised learning environment with a strong focus on pastoral care. The schools forge close links with businesses and enterprises in their specialist industries who support the schools through activities such as mentoring, work placements, and curriculum design and delivery. To further prepare students for the world of work, employability skills are embedded throughout all school activities using the CREATE employability skills framework.
Like University Technical Colleges, Studio Schools are designed for students aged 14–19, whereas free schools and other academies can choose the age range of their pupils. Some Studio Schools which operate in areas with a three-tier school system have intakes for students aged 13.
The name 'Studio School' is derived from the concept of the Renaissance studio which existed in Europe from 1400 to 1700. Students at these studios were taught by an experienced master in the same place in which the master created and produced his work. Modern-day Studio Schools aim to give students skills required by employees and businesses in the local area, in an environment which simulates genuine workplaces. As part of this, Studio Schools are open all year round and have a longer school day, typically 9am to 5pm.
The Studio Schools programme as a whole is overseen by the Studio Schools Trust, who are responsible for helping in the establishment of new Studio Schools, and supporting existing schools to implement the model. Part of this work involves facilitating the sharing of best practice through networking sessions and training and CPD events. Businesses involved with the Studio Schools programme include National Space Centre, Talk Talk, Barclays, National Nuclear Laboratory, and National Trust.


== Criticism ==
The establishment of studio schools has been criticised by some teaching unions, who claim they will cause further fragmentation state school provision. The age intake range of studio schools have also been criticised, with some unions arguing that 14 is too early an age for most children to receive such a specialised education.


== List of studio schools ==


=== Established in 2010 ===
Barnfield Skills Academy
Creative & Media Studio School


=== Established in 2011 ===
Durham Studio Sixth Form (closed in 2014)
Green Hub Studio School
Stephenson Studio School


=== Established in 2012 ===


=== Established in 2013 ===


=== Opened in 2014 ===


=== Opening in 2015 ===
Atrium Studio School
Bicester Technology Studio
Space Studio West London
The Aldridge Centre for Entrepreneurship Studio School
The STEM Studio School


== See also ==
University Technical College
Academy (English school)
Free school (England)
Polytechnic


== References ==


== External links ==
Studio School Trust homepage