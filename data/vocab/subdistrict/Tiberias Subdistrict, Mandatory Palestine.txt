The Tiberias Subdistrict (Arabic: قضاء طبريا‎) was one of the subdistricts of Mandatory Palestine. It was situated around the city of Tiberias.
In 1945 it was part of Galilee District.
After the 1948 Arab-Israeli War, the subdistrict disintegrated.


== Borders ==
Safad Subdistrict (North)
Acre Subdistrict (West)
Beisan Subdistrict (South)
Nazareth Subdistrict (West)
Syria (East)


== Depopulated towns and villages ==