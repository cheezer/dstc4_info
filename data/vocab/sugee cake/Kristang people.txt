The Kristang (otherwise known as "Portuguese-Eurasians" or "Malacca Portuguese") are a creole ethnic group of people of mixed Portuguese and Malaccan descent based in Malaysia and Singapore. People of this ethnicity have, besides Portuguese, a strong Dutch heritage, as well as some British, Chinese and Indian heritage due to intermarriages, which is common among the Kristang. In addition to this due to the Portuguese Inquisition in the region a lot of the Jews of Malacca assimilated into the Kristang community. The creole group arose in Malacca (Malaysia) between the 16th and 17th centuries, when the city was a port and base of the Portuguese Empire. Some descendants speak a distinctive Kristang language or Malacca Portuguese, a creole based on Portuguese. Today the government classifies them as Portuguese Eurasians.
The Kristang language is formally called Malacca-Melayu Portuguese Creole, made up of elements of each. The Malay language, or Bahasa Malaysia, as it is now called in Malaysia, has changed to incorporate many Kristang words. For example, garfu (Portuguese: garfo) is Kristang for "fork" and almari (Portuguese: armário) is Kristang for "cupboard"; the Malay language incorporated these Kristang words whole.
Scholars believe the Kristang community originated in part from liaisons and marriages between Portuguese men (sailors, soldiers, traders, etc.) and local native women. The men came to Malacca during the age of Portuguese explorations, and in the early colonial years, Portuguese women did not settle in the colony. Nowadays intermarriage occurs more frequently between Kristang and people of Chinese and Indian ethnicity rather than Malay because of endogamous religious laws. These require non-Muslims intending to marry Malay-Muslims first to convert to Islam. Eurasians are not always willing to alter their religious and cultural identity in this way. In earlier centuries, Portuguese and local Malays were able to marry without such conversions, because such religious laws did not exist.
The name "Kristang" is sometimes incorrectly used for other people of mixed European and Asian descent presently living in Malaysia and Singapore. This includes people of Portuguese descent who were not part of the historical Kristang community, and people with other European ancestry, such as Dutch or British.
The name comes from the Portuguese creole Kristang (Christian), derived from the Portuguese Cristão. A derogatory term for the Malacca Portuguese community was Gragok (slang term for Portuguese geragau or shrimp, referring to the fact that the Portuguese Malaccans were traditionally shrimp fishermen). In the native tongue, they also call themselves gente Kristang (Christian people).


== History ==


=== Portuguese expeditions ===
Malacca was a major destination in the great wave of sea expeditions launched by Portugal around the turn of the 16th century. It eventually was controlled as part of the Portuguese Empire. The first Portuguese expedition to reach Malacca landed in 1507. The Sejarah Melayu (Malay Annals) noted that the Malays first called them Bengali Puteh (White Bengalis), as the Portuguese brought to mind traders from Bengal but were more pale skinned. In the early years, the Malays called the Portuguese Serani (a Malay contraction of the Arabic Nasrani, meaning followers of Jesus the Nazarene). A story was recorded that the Portuguese landing party inadvertently insulted the Malaccan sultan by placing a garland of flowers on his head, and he had them detained. In 1511, a Portuguese fleet came from India to free the landing party and conquer Malacca.
At that time, Portuguese women were barred from travelling overseas due to superstition about women on ships, as well as the substantial danger of the sea route around Cape Horn. Following the Portuguese colonisation of Malacca (Malaysia) in 1511, the Portuguese government encouraged their explorers to marry local indigenous women, under a policy set by Afonso de Albuquerque, then Viceroy of India. To promote settlement, the King of Portugal granted freeman status and exemption from Crown taxes to Portuguese men (known as casados, or "married men") who ventured overseas and married local women. With Albuquerque's encouragement, mixed marriages flourished and some 200 were recorded by 1604. By creating families, the Portuguese men would make more settled communities, with families whose children would be Catholic and loyal to the Crown.


=== The Dutch takeover ===
A powerful sea power, the rising Dutch nation took Malacca from the Portuguese in 1641. This coincided with the Portuguese Restoration War in Portugal that ended the 60-year period known as the "Union of the Crowns" (1580–1640), when the crown of Portugal was joined with the crown of Spain by personal union. Almost all political contact between Portugal and Malacca ended, large number of Portuguese descent in the city were evacuated to Batavia, the VOC headquarter as war captives, where they settled in an area called "Kampung Tugu". Portuguese trade relations with the former colonial outpost of Macau have continued to this day.
Even after Portugal lost Malacca in 1641, the Kristang community largely preserved its traditions, practising Catholicism and using the Portuguese language within the community. Some Dutch crypto-Catholics were also absorbed into the community during this time.


== Present status ==
The Kristang community still has cultural and linguistic continuities with today's Portugal, especially with the Minho region, from where many early settlers emigrated. The Kristang continue to hold some church services in Portuguese, and Malaysians, including themselves, often refer to the community as "Portuguese". As the Kristang language is not taught in schools, it is nearing extinction, with the exception of within the Portuguese Settlement in Ujong Pasir Malacca.
The Kristang people in Malaysia do not have full bumiputra status, a status which applies to indigenous ethnic groups. However they have been given the privilege to apply to be members of a trust scheme known as Amanah Saham Bumiputra. This is a privilege shared by Malaysians of Thai descent. The government sponsored this program to help the Malays increase their participation in the national economy. The Kristang community in Singapore is part of a larger umbrella group, known generically as the Eurasian community. Some members have emigrated to Perth, Western Australia over the past three decades.
The Portuguese Settlement is a thriving Kristang community in Malacca, established in 1933 with the goal of gathering the dispersed Kristang community and preserving their culture. A simple village of poor fishermen for many decades, it has recently become a major tourist attraction. This has helped to improve the income of the Kristang population.


== Culture ==
Modern Kristang culture can be best described as an Anglicized Malay-Latin culture.


=== Fishing ===
Since Portuguese times, the Kristang have been living by the sea. It is still an important part of their culture. Even today, with only 10 percent of the community earning their living by fishing, many men go fishing to supplement their income or just to relax with their neighbours. Traditionally men fish from small wooden perahus, or by pushing the langgiang, a traditional bamboo-poled shrimp net through the shallows.


=== Music and dance ===
Kristang traditional music and dance, such as the Branyo and the Farrapeira are descendents of Portuguese folk dances. The Branyo, descends from the southern Portuguese folk dance Corridinho, and can be easily mistaken for the Malay Joget dance, which is believed to have developed from the Branyo. The adoption of western music instruments and musical scales by traditional Malay and Indian orchestras suggests a strong Portuguese influence. The most popular branyo tune, Jingkli Nona, is regarded as the unofficial "anthem" for Portuguese Eurasians.


=== Cuisine ===
Kristang or Malacca Portuguese cuisine consists of heavy local influence, with the additions of stews and the inclusion of pork and seafood in the diet, and rice is the staple food. Among the many dishes in Kristang cuisine, the most popular is Cari Debal. Other popular delicacies include Portuguese grilled fish, pineapple prawn Curry, Cari Seccu (dry curry), Caldu Pescador (Fisherman's soup), Sambal Chili Bedri (green chilli sambal), Soy Limang, Porku Tambrinyu (pork tamarind stew), Achar Pesi (fish pickle), Pang Su Si (Su Si Bun), and Sugee Cake. As is the custom, Kristang people commonly eat using their hands instead of utensils. It is similar to the Eurasian cuisine of Singapore and Malaysia.


=== Names ===
The Kristang people traditionally used Portuguese and Christian first names, while their surnames Portuguese. Among the many common surnames are Fernandes, Lopes, Santa Maria, Siqueira, De Costa, Pereira, Teixeira, Gomes, Pinto and other Portuguese surnames.


== Religion ==
In general the Kristang practice Roman Catholicism. Christmas (Natal) is the most festive occasion of the year, when many Kristang families get together to celebrate by eating seasonal dishes, singing carols and branyok, and revelling in saudadi. Like many other Portuguese-speaking Catholic communities around the world, the Kristang also celebrate a string of major Saints' days at the end of June, beginning with St. John (San Juang) on 24 June and closing with St. Peter (San Pedro), the fishermen's patron saint, on 29 June. The June festival of St. John's village is a major tourist attraction of Malacca. Tourists come to observe the festivities, which are religiously based.


== Portuguese influence on Malay language ==
The Portuguese ruled Malacca from 1511 to 1641. About 300 Portuguese words were adopted in the Malay language. These include:
kereta (from carreta, car);
sekolah (from escola, school);
bendera (from bandeira, flag);
mentega (from manteiga, butter);
keju (from queijo, cheese);
gereja (from igreja, church);
meja (from mesa, table); and
nenas (from ananás, pineapple).


== Notable Kristang people ==
Malaysian
Marion Caunter - TV personality
Bernard Sta Maria - Author of 'The Golden Son of Kadazan' and 'My People My Country' - Malacca Portuguese leader/politician
Joseph Sta Maria - Author of 'Undi Nos By Di Aki' (Where Do We Go From Here?) - Portuguese community advisor/social activist.
Peter van Huizen - Malaysian hockey goalkeeper (1956 Olympics).
Tony Fernandes - CEO of Air Asia (Maternal).
Dr. Siri Roland Xavier - Well known Malaysian International Professor of Entrepreneurship.
Datuk Eugene Campos - The Hon. Consul of Portugal & Patron of S.A.F.T.E.A. (Selangor and Federal Territory Eurasian Association). Well known businessman.
William Noel Clark - Malaysian musician - Represented Malaysia in 1987 Asian Broadcasting Union (ABU) competition.
Brian J. Chong - Public Relations Expert
Dawn Jeremiah - Media & Marketing Guru
Datuk Dr. Rebecca Fatima Sta. Maria - The Secretary General of the Ministry of International Trade & Investment (MITI) 6 December 2010 to present.
Tun Jeanne Abdullah - Wife of former Prime Minister of Malaysia, Tun Abdullah Ahmad Badawi.
Kimberley Leggett - Miss Universe Malaysia 2012.
Sharon Alaina Stephen - Actress in Disney's Waktu Rehat.
Andrea Fonseka - Model and TV presenter. Also Miss Universe Malaysia 2004 and daughter of former Miss Universe Malaysia, Datin Josephine Fonseka.
Malaysian/Filipino
Monika Sta. Maria - 1st runner-up in Asia's Next Top Model (Cycle 3)
Singaporean
Jeremy Monteiro - Singaporean jazz pianist, singer, composer and music educator.
Andrew Lim - Television star, actor, popular radio broadcaster and Sephardic Jew.
Australian
Guy Sebastian - Malaysian born Australian singer.
Sarah Marbeck - Malaysian born Australian model.
Royston Sta Maria - Well known singer and songwriter from Malaysia.


== See also (Related ethnic groups) ==
Eurasian – various ethnic groups of mixed European-Asian ancestry
Anglo-Burmese people - ethnic group from Myanmar
Burgher people - ethnic group from Sri Lanka
Eurasian Singaporean - ethnic group from Singapore
Filipino mestizo - ethnic group from the Philippines
Indo people - ethnic group from Indonesia
Macanese people - ethnic group from Macau usually with some Portuguese ancestry
Mardijker people - ethnic group from Indonesia with Portuguese ancestry
East Indians - Ethnic group from Mumbai, India


== References ==


== External links ==
Gerard Fernandis, "Paipia, Relijang e Tradisang" (People, Religion and Tradition), The Portuguese Eurasians in Malayasia: 'Bumiquest', "A Search for Self Identity", Lusotopie, 2000, Sciences Politiques - Bordeaux
"Penang Eurasians", Penang Story