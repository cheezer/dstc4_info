The 2020 Summer Olympics, officially known as the Games of the XXXII Olympiad (第三十二回オリンピック競技大会, Dai Sanjūni-kai Orinpikku Kyōgi Taikai) and commonly known as Tokyo 2020, is a major international multi-sport event due to be celebrated in the tradition of the Olympic Games as governed by the International Olympic Committee (IOC). The games are planned to be held from 24 July to 9 August 2020 in Tokyo, Japan. Tokyo was announced as the host city at the 125th IOC Session in Buenos Aires, Argentina, on 7 September 2013. Tokyo previously hosted the 1964 Summer Olympic Games, and in 2020 will become the fifth city (and the first city in Asia) to host the Summer Olympic Games more than once. Tokyo will also be hosting the 2020 Summer Paralympics.


== Bidding ==

Tokyo, Istanbul and Madrid were the three candidate cities. The applicant cities of Baku and Doha were not promoted to candidate status. A bid from Rome was withdrawn.


=== Vote ===
48 votes needed for selection in opening round & runoff; 49 in final round.

The IOC voted to select the host city of the 2020 Summer Olympics on 7 September 2013 at the 125th IOC Session at the Buenos Aires Hilton in Buenos Aires, Argentina. An exhaustive ballot system was used. No city won over 50% of the votes in the first round, and Madrid and Istanbul were tied for second place. A run-off vote between these two cities was held to determine which would be eliminated. In the final vote, a head-to-head contest between Tokyo and Istanbul, Tokyo was selected by 60 votes to 36.


== Development and preparation ==
The Tokyo metropolitan government set aside a fund of ¥400 billion Japanese yen (over $3 billion USD) to cover the cost of hosting the Games. The Japanese government is considering increasing slot capacity at both Haneda Airport and Narita Airport by easing airspace restrictions. A new railway line is planned to link both airports through an expansion of Tokyo Station, cutting travel time from Tokyo Station to Haneda from 30 minutes to 18 minutes, and from Tokyo Station to Narita from 55 minutes to 36 minutes; the line would cost ¥400 billion yen and would be funded primarily by private investors. But East JR is planning a new route near Tamachi to Haneda Airport. Funding is also planned to accelerate completion of the Central Circular Route, Tokyo Gaikan Expressway and Ken-Ō Expressway, and to refurbish other major expressways in the area. There are also plans to extend the Yurikamome automated transit line from its existing terminal at Toyosu Station to a new terminal at Kachidoki Station, passing the site of the Olympic Village, although the Yurikamome would still not have adequate capacity to serve major events in the Odaiba area on its own.
The Organizing Committee is headed by former Prime Minister Yoshiro Mori. Olympic and Paralympic Minister Toshiaki Endo is overseeing the preparations on behalf of the Japanese government.


== Sports ==
Following the 2012 Games, the IOC assessed the 26 sports held in London, with the remit of selecting 25 'core' sports to join new entrants golf and rugby sevens at the 2020 Games. In effect, this would involve the dropping of one sport from the 2016 Games program. This would leave a single vacancy in the 2020 Games program, which the IOC would seek to fill from a shortlist containing seven unrepresented sports and the removed sport. Events such as modern pentathlon, taekwondo and badminton were among those considered vulnerable.
On 12 February 2013, IOC leaders voted to drop wrestling from the Olympic program, a surprise decision that removed one of the oldest Olympic sports from the 2020 Games. Wrestling, which combines freestyle and Greco-Roman events, goes back to the inaugural modern Olympics in Athens in 1896, and even further to the Ancient Olympic Games. The decision to drop wrestling was opposed in many countries and by their NOCs. Wrestling therefore joined other sports in a short list applying for inclusion in the 2020 Games.
On 29 May 2013, it was announced that three sports made the final shortlist; squash, baseball/softball, and wrestling. Five other sports (karate, roller sports, sport climbing, wakeboarding, and wushu) were excluded from consideration at this point. On 8 September 2013, at the 125th IOC Session, the IOC selected wrestling to be included in the Olympic program for 2020 and 2024. Wrestling secured 49 votes, while baseball/softball secured 24 votes and squash got 22 votes.
Under new IOC policies that shift the Games to an "event-based" programme rather than sport-based, the host organizing committee can now also propose the addition of sports to the programme—with a particular focus on adding sports that are popular in the host country. As a result of these changes, a new shortlist of eight sports were unveiled on 22 June 2015. These sports include baseball/softball, bowling, karate, roller sports, sport climbing, squash, surfing, and wushu. In September 2015, organisers will recommend one or more of the sports to the IOC for inclusion in 2020, with the final decision in August 2016.


== Calendar ==
All dates are JST (UTC+9)
This calendar is adapted from the candidature file.


== Venues ==

It was confirmed in February 2012 that the National Olympic Stadium in Tokyo would receive a $1 billion upgrade and full–scale reconstruction for the 2019 Rugby World Cup as well as the 2020 Olympics. As a result, a design competition for the new stadium was launched. In November 2012 the Japan Sport Council announced that out of 46 finalists, Zaha Hadid Architects was awarded the design for the new stadium. Plans included dismantling the original stadium, and expanding the capacity from 50,000 to a modern Olympic capacity of about 80,000. However, Japanese Prime Minister Shinzo Abe announced in July 2015 that plans to build the new National Stadium would be scrapped and rebid on amid public discontent over the stadium's building costs.
28 of the 33 competition venues in Tokyo are within 8 kilometres (5 miles) of the Olympic Village. 11 new venues are to be constructed.


=== Heritage Zone ===
Seven venues will be located within the central business area of Tokyo, northwest of the Olympic Village. Several of these venues were also used for the 1964 Summer Olympics.
National Olympic Stadium – Opening and Closing Ceremonies, Athletics, Football (Final)
Yoyogi National Gymnasium – Handball
Tokyo Metropolitan Gymnasium – Table tennis
Nippon Budokan – Judo
Tokyo International Forum – Weight Lifting
Imperial Palace Garden – Cycling (Road)
Kokugikan Arena – Boxing


=== Tokyo Bay Zone ===
20 venues will be located in the vicinity of Tokyo Bay, southeast of the Olympic Village, predominantly on Ariake, Odaiba and the surrounding artificial islands.
Kasai Rinkai Park – Canoe Kayak (slalom)
Oi Seaside Park – Hockey
Olympic Aquatics Centre – Aquatics (swimming, diving and synchronised swimming)
Tatsumi International Swimming Center - Water polo
Dream Island Stadium – Equestrian (jumping, dressage and eventing)
Dream Island Archery Field – Archery
Ariake Arena – Volleyball
Olympic Velodrome – Cycling (track)
Olympic BMX Course – Cycling (BMX)
Olympic Gymnastic Centre – Gymnastics (artistic, rhythmic and trampoline)
Ariake Coliseum – Tennis
Odaiba Marine Park – Triathlon and Aquatics (marathon swimming)
Shiokaze Park – Beach Volleyball
Sea Forest Cross–Country Course – Equestrian (eventing)
Sea Forest Waterway – Rowing and Canoe Kayak (sprint)
Sea Forest Mountain Bike Course – Cycling (mountain bike)


=== Sites farther than 8 km (5 miles) from the Olympic Village ===
Asaka Shooting Range – Shooting
Musashino Forest Sport Centre – Modern pentathlon (fencing), badminton
Tokyo Stadium – Football, modern pentathlon (swimming, riding, running, shooting) and rugby sevens
Kasumigaseki Country Club – Golf
Saitama Super Arena - Basketball
Enoshima - Sailing
Makuhari Messe - Fencing, taekwondo and wrestling


=== Football venues ===

National Olympic Stadium
Tokyo Stadium
International Stadium Yokohama
Saitama Stadium 2002
Sapporo Dome
Miyagi Stadium


=== Non-competition venues ===
Imperial Hotel, Tokyo – IOC
Harumi Futo – Olympic Village
Tokyo Big Sight – Media Press Center, International Broadcast Center


== Marketing ==


=== Emblem ===
The official emblem of the 2020 Summer Olympics and Paralympics were unveiled on 24 July 2015. The logo resembles a stylized "T"; a red circle in the top-right corner represents a beating heart, the flag of Japan, and an "inclusive world", and a black column in the centre represents diversity.
Shortly after the unveiling, allegations of plagiarism surfaced; Belgian graphics designer Olivier Debie pointed out that the emblem, without the circle in the top-right, used a similar layout and shapes as a logo he had designed for the Théâtre de Liège. The emblem's designer, Kenjiro Sano, did not comment. Tokyo's organizing committee denied that the emblem design was plagiarized, arguing that the design had gone through "long, extensive and international" intellectual property examinations before it was cleared for use.


== Media ==


=== Sponsors ===
As of 2015 total sponsorship for the 2020 Games reached approx. $1.5 billion, setting an Olympics record (in contrast, the 2008 Summer Olympics in Beijing attracted only $1.3 billion).


==== Worldwide Olympic Partners ====
The Coca-Cola Company
Atos
Bridgestone
Dow Chemical Company
Electronic Arts
General Electric
McDonald's
Omega SA
Panasonic
Procter & Gamble
Samsung Electronics
Toyota
Visa Inc.


==== The Gold Partners ====
Asahi Breweries
ASICS
Canon Inc.
Fujitsu
JX Holdings
Mitsui Fudosan
Mizuho Financial Group
Nippon Life
NEC Corporation
Nippon Telegraph and Telephone
Nomura Holdings
Sumitomo Mitsui Financial Group
Tokio Marine Nichido


==== The Official Partners ====
All Nippon Airways
Japan Airlines
Tokyo Gas
Yamato Holdings


=== Broadcasting ===
In the United States, the 2020 Summer Olympics will be broadcast by NBC, as part of a US$4.38 billion agreement that began at the 2014 Winter Olympics.
In Europe, these will be the first Summer Olympics under the IOC's exclusive pan-European rights deal with Discovery Communications, which began at the 2018 Winter Olympics. The rights for the 2020 Games cover almost all of Europe, excluding Russia, as well as France and the United Kingdom due to pre-existing rights deals that will expire following these Games, thus marking the BBC's and France Télévisions' final Olympics. Discovery will sub-license coverage to free-to-air networks in each territory.
Below are the confirmed television right holders:
 Asia – Dentsu (Rights to be sold to local broadcaster)
 Australia – Seven Network
 Canada – CBC/Radio-Canada, Bell Media, Rogers
 China – CCTV
 Europe – Discovery Communications, Eurosport
 France – France Télévisions, Canal+
 Japan – Japan Consortium
 MENA – beIN Sports
 North Korea – SBS
 South Korea – SBS
 United Kingdom – BBC
 United States – NBC


== See also ==
1964 Summer Olympics
2020 Summer Paralympics


== Notes ==


== External links ==
Tokyo 2020
International Olympic Committee
IOC Page For Tokyo 2020
Japanese Olympic Committee