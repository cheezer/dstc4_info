The Sun Journal is a daily newspaper published in Lewiston, Maine, USA, covering western Maine.
In addition to its main office in Lewiston, the paper maintains satellite news and sales bureaus in the Maine towns of Farmington, Norway and Rumford.


== Prices ==
The Sun Journal prices are: $1.00 daily, $2.25 Sunday.


== Sister weeklies ==
On October 1, 2007, the Sun Journal purchased Kirkland Newspapers of Farmington, the publisher of four weekly newspapers:
The Franklin Journal of Farmington (4,500 twice-weekly circulation)
The Rangeley Highlander of Rangeley (2,300 biweekly)
The Penobscot Times of Old Town (3,300 weekly)
Livermore Falls Advertiser of Livermore Falls (2,300 weekly)
The Sun Journal also owns The Forecaster, a free regional paper.


== References ==


== External links ==
Sun Journal Website
Today's Sun Journal front page at the Newseum website