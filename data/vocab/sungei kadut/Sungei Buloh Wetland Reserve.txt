The Sungei Buloh Wetland Reserve (Chinese: 双溪布洛湿地保护区) is a nature reserve in the northwest area of Singapore. It is the first wetlands reserve to be gazetted in Singapore (2002), and its global importance as a stop-over point for migratory birds was recognised by the Wetlands International's inclusion of the reserve into the East Asian Australasian Shorebird Site Network. The reserve, with an area of 130 hectares, was listed as an ASEAN Heritage Park in 2003.


== History ==

Previously unheard of as a nature area, the site gained prominence only in 1986 when a call was made to conserve the area by members of the Singapore Branch of the Malayan Nature Society. Particularly significant was its unusually high variety of bird species, which included migratory birds from as far as Siberia on their way to Australia to escape the winter months. Their suggestion was taken up by the government, and a site with an area of 0.87 km² was given nature park status in 1989. The Parks & Recreation Department, a precursor to today's National Parks Board, developed and managed the nature park with a team of experts. The most notable names from the team included the Wildfowl & Wetlands Trust from the United Kingdom and World Wide Fund for Nature. Sungei Buloh Wetland Reserve, then known as Sungei Buloh Nature Park, was officially opened on 6 December 1993 by then Prime Minister Goh Chok Tong. Over the years, Sungei Buloh charmed people to support its cause. It welcomed its 100,000th visitor in 1994. In 1997, the Park found its corporate sponsor in HSBC, which set up the Sungei Buloh Education Fund in support of its nature outreach programmes. In 1999, Woodlands Secondary School became the first school to adopt the park. It was followed by Commonwealth Secondary School in 2001 and Hillgrove Secondary School in 2002. The latter two are still actively involved in the programme. The government formally announced on 10 November 2001 that the park would be accorded nature reserve status, a step that protects the area from any unauthorised destruction or alteration. The second phase of the park was opened, and the entire site of 130 hectares officially gazetted on 1 January 2002 as the Sungei Buloh Wetland Reserve. It is one of the four nature reserves to be gazetted. The others are Labrador Nature Reserve, Bukit Timah Nature Reserve and Central Catchment Nature Reserve.


== Wildlife ==

Crabs and mudskippers dominate the littoral zone, the area between the low and high tide zones. Mud lobsters and their volcano-like mounds can be observed above the high-tide level. One may find the Malayan water monitor in the area. Fish are in abundance due to the cessation of fishing. The mullet, archer fish and halfbeak are some species of fish in the area.
Amongst the many birds that can be spotted feeding on the diverse fauna variety of worms and molluscs, are whimbrel, common greenshank, common redshank, Mongolian plover, curlew sandpiper, marsh sandpiper and Pacific golden plover, yellow bittern and cinnamon bittern. Lucky visitors to the reserve may be able to spot the resident family of smooth otters, as well as the rare lesser whistling-duck,and the rare milky stork. The reserve forms part of the Kranji-Mandai Important Bird Area (IBA), identified as such by BirdLife International because it supports Chinese egrets, greater spotted eagles and greater crested terns. Atlas moth, the largest species of moth in Southeast Asia can be found in the back mangrove.
Observation hides are available where visitors can observe the flora and fauna in the surroundings in tranquility and at a distance from the animals and birds. Saltwater crocodiles (Crocodylus porosus) are occasionally seen in the reserve, although it is not known whether or not these are individuals that had wandered over from Malaysia/Indonesia or a remnant localised population. (This species was once common in Singapore but was said to be extinct.)


== Nature education ==
Since its inception, the reserve provided nature education programs as well as a volunteer guide programme for schools and the general public. These include the SUN Club programme which are meant for students with special needs, mentorship programmes for secondary school students and a Young Naturalists Programme. Many such programmes were collaboration efforts with partners such as the British Council and the Ministry of Education. The reserve distributes education materials such as workshops, guidebook and a triannual magazine, Wetlands, to further enrich the students and public. Each year, the nature reserve receives more than 400 organised school visits.
On 25 August 2007, a wireless learning trail was launched at Sungei Buloh Nature Reserve. The new initiative which integrates technology with nature education was a partnership amongst Ministry of Education (Singapore) (MOE), Infocomm Development Authority of Singapore (IDA) and a private sector company iCELL Network. Sungei Buloh Nature Reserve was the first park in Singapore to engage such a learning method.


== Future development ==
Unveiled in 2008, the SBWR masterplan was conceptualized together with key stakeholders and partners to strengthen the conservation of the area’s biodiversity, while allowing more visitors to experience the wonders of a wetland habitat. To balance visitor and human impact on the rich biodiversity of the reserve, the Sungei Buloh Wetland Reserve was zoned into activity areas (high, medium, and low activity zones), with programmes to cater to different groups of visitors, from beginners all the way to the expert level. This facilitates experiential and lifelong learning and to encourage repeat visits to Sungei Buloh Wetland Reserve. The SBWR extension is zoned as a high activity zone, while the western end of SBWR and Kranji Marshes were zoned as low and medium activity zones respectively.
Work for phase 3 of the SBWR masterplan, previously announced in 2008, has begun. Phase 3 of the masterplan includes the enhancing of two areas: the western end of SBWR (including Cashin House), and Kranji Marshes.
Western end of SBWR In the next few years, the western end of SBWR, including Cashin House, will be sensitively enhanced to serve as a low activity learning area. By end-2017, nature trails will link the current Reserve to Cashin House, which could serve as a facility for organised groups, researchers and volunteers. Nature appreciation of coastal habitats, education and outreach will take centre stage at the 6.16ha western end of SBWR.
Relevant conservation authorities and experts will be consulted so as to maintain the integrity of the historical heritage of Cashin House, and the building will retain its existing architectural structure.


== Getting there ==
Sungei Buloh Wetland Reserve is at Neo Tiew Crescent, 15 minutes from Kranji MRT Station. Buses include service 925C and the Kranji Express which leaves every hour from 1200 to 1700 daily. 925C leaves every half-hour on Sundays and public holidays.


== See also ==
Labrador Nature Reserve
Central Catchment Nature Reserve
Bukit Timah Nature Reserve
List of parks in Singapore


== References ==


== External links ==
Official site
National Parks Board, Singapore
360 Degree Virtual Tour Walkthrough Of Sungei Buloh Wetland Reserve
Peter Chou's PBase Gallery Sungei Buloh.
Gallery of photographs by John Larkin