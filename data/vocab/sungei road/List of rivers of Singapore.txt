The geographically small island nation of Singapore has few rivers of significant length or width, the longest of which, the Kallang River is only 10 km in length. The Singapore River, arguably the most famous in the country, is of insignificant length as well. However, the country's tropical climate and heavy rainfall require a very comprehensive network of natural draining systems, much of which has become concrete system as urbanisation spread across the island.


== Natural rivers ==


=== Canalised ===
Alexandra Canal, Singapore
Geylang River
Kallang River
Pelton Canal
Rochor Canal
Rochor River
Siglap Canal
Singapore River
Stamford Canal, Singapore
Sungei Api Api
Sungei Bedok
Sungei China
Sungei Ketapang
Sungei Lanchar
Sungei Pandan (lower reaches dammed as Pandan Reservoir)
Sungei Pinang (Hougang)
Sungei Punggol
Sungei Seletar Simpang Kiri
Sungei Sembawang
Sungei Serangoon
Sungei Simpang Kanan
Sungei Tampines
Sungei Ulu Pandan
Sungei Whompoe


=== On offshore islands ===


==== Pulau Tekong ====
Sungei Belang
Sungei Chek Mat Nah
Sungei Pasir
Sungei Permatang
Sungei Sanyongkong
Sungei Seminei
Sungei Unum


==== Pulau Ubin ====
Sungei Asam
Sungei Batu Kekek
Sungei Besar
Sungei Jelutong
Sungei Mamam
Sungei Puaka
Sungei Pulau Ubin
Sungei Teris
Sungei Tiga
Sungei Wat Siam


== Former rivers ==


=== Dammed and flooded ===
Sungei Chik Abu - flooded and part of Lower Seletar Reservoir
Sungei Puaka - flooded and part of Lower Seletar Reservoir
Sungei Kranji - now the Kranji Reservoir
Sungei Murai - now the Murai Reservoir
Sungei Poyan - now the Poyan Reservoir
Sungei Sarimbun - now the Sarimbun Reservoir
Sungei Sopok - flooded and part of Lower Seletar Reservoir
Sungei Tengeh - now the Tengeh Reservoir


== References ==