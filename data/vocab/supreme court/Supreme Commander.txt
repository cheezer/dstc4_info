Supreme Commander may refer to:


== Military ==
Commander-in-chief, a military rank
Generalissimo or generalissimus, a (usually informal) term for a senior military commander, or a foreign term for a senior general
Allied Supreme Commander, position held by Marshal Ferdinand Foch during World War I
Supreme Allied Commander, title held by the most senior commander within certain multinational military alliances
Supreme Allied Commander Europe, military commander of the North Atlantic Treaty Organization (NATO)
Supreme Allied Commander Atlantic, head of NATO's now-defunct Allied Command Atlantic
Supreme Commander for the Allied Powers, title held by General Douglas MacArthur during the Occupation of Japan following World War II
Supreme Commander of the Korean People's Army
Supreme Commander of the Malaysian Armed Forces
Supreme Commander-in-Chief of the Armed Forces of the Russian Federation
Supreme Commander of the Swedish Armed Forces
Supreme Commander of the Unified Armed Forces of the Warsaw Treaty Organization


== Media ==
Supreme Commander series, a video game series developed by Gas Powered Games
Supreme Commander (video game), real-time strategy video game released in 2007
Supreme Commander: Forged Alliance, a stand-alone expansion for Supreme Commander
Supreme Commander 2, the sequel to Supreme Commander


== Others ==
Supreme Commander (militant), a title used for the head of a militant group or an organization