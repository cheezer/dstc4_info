The Supreme Court of Nigeria is the highest court in Nigeria, and is located in the Central District, Abuja, in what is known as the Three Arms Zone, so called due to the proximity of the offices of the Presidential Complex, the National Assembly, and the Supreme Court.


== Overview ==
In 1963, the Federal Republic of Nigeria was proclaimed and Nnamdi Azikiwe became its first President. Appeals from the Federal Supreme Court to the Judicial Committee of the Privy Council were abolished at that point, and the Supreme Court became the highest court in Nigeria. In 1976, the Court of Appeal (originally known as the Federal Court of Appeal) was established as a national court to entertain appeals from the High Courts of each of Nigeria's 36 states, which are the trial courts of general jurisdiction. The Supreme Court in its current form was shaped by the Supreme Court Act of 1990 and by Chapter VII of the 1999 Constitution of Nigeria.
Under the 1999 constitution, the Supreme Court has both original and appellate jurisdictions, has the sole authority and jurisdiction to entertain appeals from Court of Appeal, having appellate jurisdiction over all lower federal courts and highest state courts. Decisions rendered by the court are binding on all courts in Nigeria except the Supreme Court itself.


== Structure and organization ==
The Supreme Court is composed of the Chief Justice of Nigeria and such number of justice not more than 21, appointed by the President on the recommendation of the National Judicial Council and subject to confirmation by the Senate. Justices of the Supreme Court must be qualified to practice law in Nigeria, and must have been so qualified for a period not less than fifteen years. Justices of the Supreme Court of Nigeria have a mandatory retirement age of 70 years.


== References ==


== External links ==
Official Website of the Nigerian Supreme Court
Supreme Court of Nigeria News
Supreme Court of Nigeria
Section of the 1999 Constitution of Nigeria on the Judicature