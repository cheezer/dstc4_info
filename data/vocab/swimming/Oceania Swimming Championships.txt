The Oceania Swimming Championships are currently held every 2 years, in even years. They are organized by the Oceania Swimming Association, and feature teams representing countries and islands from that region.
The most recent championships were the 10th edition, held in May 2014.


== Locations ==


== See also ==
List of Oceania Championships records in swimming


== References ==