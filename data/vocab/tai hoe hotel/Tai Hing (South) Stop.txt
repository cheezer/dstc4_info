Tai Hing (South) (Chinese: 大興（南）站) is a MTR Light Rail stop located on the ground at Tai Fong Street (Chinese: 大方街) near Tai Hing Shopping Centre (Chinese: 大興商場), Tai Hing Estate (Chinese: 大興邨) in Tuen Mun District. It began service on 18 September 1988 and belongs to Zone 2. It serves the south of Tai Hing Estate.


== Neighbouring stops ==


== References ==


== See also ==
Tai Hing Estate