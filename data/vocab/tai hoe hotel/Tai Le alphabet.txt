Tai Le is the name of Tai Nüa script, the script used for the Tai Nüa language.


== Unicode ==

Tai Le script was added to the Unicode Standard in April, 2003 with the release of version 4.0.
The Unicode block for Tai Le is U+1950–U+197F:


== See also ==
New Tai Lue script for Tai Lü language, derived from Old Tai Lue script "Dai Tam"


== References ==


== External links ==
GNU FreeFont Tai Le range in serif face.