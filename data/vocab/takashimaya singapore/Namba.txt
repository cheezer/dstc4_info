Namba (難波, Nanba, IPA: [namba]) is a district of Osaka, Japan. Namba is regarded as the center of so-called Minami ("South") area of Osaka. Its name is one of variations on the former name of Osaka, Naniwa. Namba is best known as the city's main south-central railway terminus: JR, Kintetsu, Nankai, Hanshin, and three lines of the Osaka Municipal Subway have stations there.
Some of the most famous images of Osaka, including the Glico Man and the Kani Doraku Crab, are located around the Dōtonbori canal in Namba. Namba is also known as an entertainment district, and hosts many of the city's most popular bars, restaurants, nightclubs, arcades, and pachinko parlors. The area is also known for shopping, with the Takashimaya department store (for older styles) and the sprawling underground Namba City shopping mall (for newer styles).
Namba Parks is a new development consisting of a high office building, called "Parks Tower," and a 120-tenant shopping mall with rooftop garden. Various kinds of restaurants (Japanese, Korean, Italian, etc.) are located on the 6th floor, and shops on the 2nd to 5th floors. Parks Garden features enough greenery to help visitors forget that they’re in the middle of the city. There is also an amphitheater for live shows, as well as space for small personal vegetable gardens and wagon shops.
Namba is located in Chūō and Naniwa wards.


== Railway stations ==
Namba Station
●Nankai Electric Railway
●Nankai Line
●Koya Line

Osaka Municipal Subway
●Midosuji Line (M20)
●Yotsubashi Line (Y15)
●Sennichimae Line (S16)

JR Namba Station
●JR West
●Kansai Main Line (Yamatoji Line)

Ōsaka Namba Station
●Kintetsu
Namba Line (through service to the Nara Line)

●Hanshin Railway
●Hanshin Namba Line


== References ==