Ngee Ann City (Chinese: 义安城; pinyin: Yì'ān Chéng) is a shopping and commercial centre located on Orchard Road, Singapore. The S$520 million building was officially opened on 21 September 1993 by then Prime Minister, Goh Chok Tong. Many Singaporeans refer to Ngee Ann City as "Taka", a slang-abbreviation of the anchor tenant's name, Takashimaya.
Ngee Ann City currently houses the High Commission of New Zealand, which is situated on the 15th floor of Tower A.


== History ==
In the 1950s, the land that Ngee Ann City sits on was a burial ground, owned and managed by Ngee Ann Kongsi. It was part of a parcel of land known as Tai Shan Ting, which was bounded by Orchard Road, Paterson Road and Grange Road. A ten-storey Ngee Ann Building was then built on the site, and was demolished to make way for Ngee Ann City.
Redevelopment of the site was first considered as early as 1967. Ngee Ann City was planned by Ngee Ann Development and the Orchard Square Development Corporation in the late 1980s. Raymond Woo, the architect who designed the complex, drew inspiration from the Great Wall of China. The intent was to reflect the dignity, solidity and strength of the Ngee Ann Kongsi. Wong spent five years designing and overseeing the project.
The land belonging to Ngee Ann Kongsi was a much sought-after piece of real estate in Singapore. Ng Teng Fong of Far East Organisation was unsuccessful in his bid to buy the land, even after upping his offer to S$175 million from S$140 million. The land was also sought by the owners of Hilton International Hotel.
There were also a series of disputes between Ngee Ann Kongsi and the Metro Group which had subsequently acquired the redevelopment rights. These were only resolved in 1981, resulting in the setting up of a joint venture in which Ngee Ann had a 73% stake, and Metro the balance 27%. The partners paid heavily for the dispute, as the Singapore Government acquired half the site in 1983. This left them only 28,322 square metres for development.
Work on Ngee Ann City began 22 years after the project was first proposed. The construction of the S$520 million complex took four years. Ngee Ann City was officially opened by Prime Minister Goh Chok Tong on 31 August 1993.


== Facilities ==
Ngee Ann City has two office towers, Tower A and B which are both 26 storeys high. Among its many shops are the Takashimaya department store and Kinokuniya, the second-largest bookstore in Southeast Asia. Till 2007, it housed the library@orchard, part of the National Library Board on the 5th floor. Ngee Ann City is also home to the largest Best Denki in Singapore, known as Big Best. In 2005, the shopping mall opened an art and creativity section on the 4th floor called iFORUM, the first of its kind in Singapore.
When Ngee Ann City opened in 1993, Tangs Studio (a division of Tangs) occupied three floors of the building at the Tower B section of the building. A few years later, the Level 2 of the department store was closed, and it eventually closed down in 1999 due to poor business. This part of the mall became part of the speciality shop section (mainly branded boutiques) on the mall on Level 2, Books Kinokuniya on Level 3, and a shop section mainly children's boutiques and shops on Level 4 that was converted to iForum in 2005. The top floor of the mall, Level 5, was part of the upper level carpark. In 1997, the 5th floor was converted to retail space.
The Civic Plaza is where roadshows, concerts, functions, performances are held. There is a fountain at the front of the Civic Plaza facing Orchard Road. The building is connected by underpasses to Wisma Atria, ION, Wheelock Place, Isetan and Lucky Plaza.
The two towers of Ngee Ann City were intended by the designer to symbolise Chinese door gods, representing strength, generosity and unity. Tenants in the office tower include Takashimaya Singapore, Books Kinokuniya, Metro department store, Ngee Ann Development, and a medical floor on Level 8 of Tower B.


== References ==

"Ngee Ann City comes alive", The Straits Times, August 7, 1993
Tan Sung, "Takashimaya ready to face sluggish sector", The Straits Times, August 6, 1993


== External links ==
Official website