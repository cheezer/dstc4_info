Punggol-Tampines Single Member Constituency (Traditional Chinese: 榜鵝-淡濱尼單選區; Simplified Chinese: 榜鵝-淡滨尼单选区) is a former short-lived single member constituency in Tampines and Punggol, Singapore that existed only from 1955 to 1959.


== Formation and dissolution ==
The ward was formed in 1955, consisting largely of present day Hougang, Pasir Ris, Punggol, Sengkang, Simei and Tampines. This north-eastern area of Singapore was considered rural and some of the least populated. At that time, there were only 6,628 voters in the constituency, of which only 3,886 turned out to vote.
Subsequently in 1959, this ward had split into Punggol SMC and Tampines SMC. The present day Hougang, Punggol and Sengkang were hived off as Punggol SMC. As for the Tampines ward, it had instead taken on significant portions of Ulu Bedok SMC to become one of the larger wards in eastern Singapore. Tampines SMC then comprised the entire present day Bedok (except Kampong Chai Chee which is part of the Kampong Kembangan SMC), Pasir Ris, Simei and Tampines.
With the split due to growing population within its borders, the Punggol-Tampines SMC ceased to exist.


== Members of Parliament ==
Goh Chew Chua (1955–1959)
Constituency Abolished (1959 – present)


== Candidates and Results ==


=== Elections in 1950s ===


== References ==

In 1955, it started with Punggol - Tampines
Map of Punggol-Tampines ward in 1955
Map of Tampines ward in 1959