Tampines Rovers Football Club (nicknamed the Stags) are a professional association football club based in Tampines, Singapore, that play in the S.League. Founded in 1945, Tampines has won major trophies in Singapore football, including the Singapore Cup and the S.League. They have won the national league championship seven times, the Singapore Cup thrice, the S.League five times and the ASEAN Club Championship once. Their temporary home ground is the Jurong West Stadium as their traditional home ground, the Tampines Stadium (opened 1 April 1989), is being replaced by the Tampines Town Hub. The Stags are also known for boosting a considerably high attendance at away games. Their main rivals are Geylang International, with whom they contest the Eastern Derby. Tampines Rovers is one of the richest clubs in Singapore football and around the world.


== History ==
Several football enthusiasts from Tampines decided to form a football club in 1945. After many name changes, they finally decided on "Tampines Rovers" as the official club name. The Stags spent the 1950s and 1960s competing in the Singapore Amateur Football Association League, where they were among the top teams, until they were placed in Division II of the newly formed National Football League in 1974. 1975 was a watershed year for Tampines, as they were promoted to Division I after winning all their league matches and reached the President's Cup final, where they lost 0–1 to the Singapore Armed Forces Sports Association in front of a national record crowd of 30,000. They continued to challenge for honours for the next decade, reaching another President's Cup final in 1978, then emerging as national champions in 1979, 1980 and 1984. The Stags were relegated to the second tier in 1988, but under a new management team, won their league in 1994, and were one of eight clubs selected to compete in the newly formed S.League.
However, Tampines did not finish higher than sixth place in the first six seasons of the S.League. In 2002, the Stags secured the services of Malaysian coach Chow Kwai Lam, who guided them to the Singapore Cup and two fourth-place finishes. Under the next coach, Vorawan Chitavanich, Tampines achieved the S.League and Singapore Cup double in 2004. The following season, they successfully defended their S.League title, were named the 'S.League Team of the Decade' and became the first Singapore team to win the ASEAN Club Championship. The Stags were Singapore Cup champions in 2006, but finished runners-up to SAFFC in the S.League. The Stags was also the champion of S-League in 2011 and 2012. They were also runner-up in the 2012 Singapore Cup after losing 1–2 to SAFFC.


=== 2013 Season ===


==== S.League ====
The Stags started the 2013 season with an opening day 5–0 win over Geylang International. They then beat the Courts Young Lions 2–0 but were awarded a 3–0 win as the Young Lions had fielded ineligible players for the game. The Stags then went on a 9-match unbeaten run spanning about 3 months before being beaten 1–0 by Tanjong Pagar United at the Queenstown Stadium on 5 May 2013. They next stumbled away to Balestier Khalsa, losing 4–0 at the Toa Payoh Stadium. Tampines were themselves punished for fielding an ineligible player against Harimau Muda B.
On 20 October 2013, the Stags were announced as the 2013 S.League champions after closest challengers Albirex were beaten 1–2 by Balestier Khalsa. Albirex, left on 40 points, were unable to catch Tampines on 52. The Stags were then given the trophy on 26 Oct 2013 after the 1–1 draw against Tanjong Pagar.


==== Singapore Cup ====
Tampines' head coach Nenad Baćina was sacked just days before the first round of the RHB Singapore Cup after disappointing results in the AFC Cup and the club General Manager Tay Peng Kee took charge. The Stags were eliminated in the first round after losing 2–1 against Hougang United despite having beaten them 3–0 in the league just days ago. Many supporters were furious at the result and the club's decision to sack Baćina. Some even stopped attending games for some time.


== Crest and colours ==
The club selected the stag as its animal mascot as the animal is a symbol of wisdom, its antlers are associated with the tree of life and in Chinese culture, it is a symbol of virility.


== Stadium ==
Since 2012, Tampines Rovers have played their home games at the Clementi Stadium, as their traditional home ground, the Tampines Stadium (opened 1 April 1989), is being replaced by the Tampines Town Hub. The stadium had a grass football pitch, an 8-lane running track and partial athletic facilities. The stadium can hold up to 4,000 spectators. The stadium is currently managed by the Singapore Sports Council(SSC).


== Supporters ==
The Yellow Brigade is the official supporting group for the club. It is one of the largest supporters group in the S.League dedicated to the club. They are also known for its fanaticism and their support towards the club. The Yellow Brigade main colour is black and yellow with mainly yellow football Scarf and Yellow Banners to represent the beloved colour of the club. They will also appear at the Clementi Stadium for the home match. The Yellow Brigade also bring drums, trumpet and flags to every match.


== Players ==


=== Current Squad ===
As of 2 July 2015 
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== Prime League Players ===
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== Out on Loan ===
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


== Club Officials ==


=== Management ===
Chairman: Teo Hock Seng
Vice Chairman: Murali Krishna Ramachandran
Honorary Secretary: Pek Hock Beng
Honorary Treasurer: Philip Beng
First Advisory: Mah Bow Tan
Second Advisory: Yatiman Yusof
Committee Member: Justin Morais
Committee Member: Syed Faruk Bin Syed Salim Alkaff
Committee Member: Jeffrey Low
Committee Member: Ong Kang Sheng
Committee Member: Gerald Seng
Committee Member: Ethan Loke
General Manager: Tay Peng Kee
Club Manager: Andrew Lim
Operations Manager: Michael Chua
Admin Executive: Faridah Matsah
Reference:


=== Technical Staff ===
Team Manager: Clement Teo
Head Coach: V. Sundramoorthy
Assistant Coach: Rafi Ali
Head Coach (Prime League): Satria Mad
Fitness Coach: Aleksandar Đurić
Goalkeeper Coach: Lim Chiew Peng
Goalkeeper Coach: Matthew Tay
Sports Trainer: Thomas Pang
Sports Trainer: Premjit Singh
Logistics Officer: Goh Koon Hiang
Reference:


== Managers ==
 Victor Stănculescu (1997–1998)
 Vorawan Chitavanich (1 Jan 2004 – Dec 2010)
 Steven Tan (1 Jan 2011 – 10 Aug 2012)
 Zulkarnaen Zainal (Jan 2012 – ??)
 Tay Peng Kee (11 Aug 2012 – 31 Dec 2012)
 Nenad Baćina (1 Dec 2012 – 28 May 2013)
 Tay Peng Kee (28 May 2013 – 27 Nov 2013)
 Salim Moin (28 Nov 2013 – 27 April 2014)
 Rafi Ali (27 April 2014 – 5 Nov 2014)
 V. Sundramoorthy (5 Nov 2014 – )


== Honours ==


=== Domestic ===
League
S.League: 5

2004, 2005, 2011, 2012, 2013

National Football League Division One: 3

1979, 1980, 1984

Cup
Singapore Cup: 3

2002, 2004, 2006

Singapore Charity Shield: 4

2011, 2012, 2013, 2014


=== ASEAN Competition ===
ASEAN Club Championship: 1

2005


== Performance in domestic competitions ==
The 1996 season of the S.League was split into two series. Tiger Beer Series winners Geylang United defeated Pioneer Series winners Singapore Armed Forces in the Championship playoff to clinch the S.League title.
2003 saw the introduction of penalty shoot-outs if a match ended in a draw in regular time. Winners of penalty shoot-outs gained two points instead of one.
Last updated on 18 May 2014


== Performance in AFC competitions ==
AFC Cup: 7 appearances

2014: Group stage ( Group H )
2013: Group stage ( Group H )
2012: Group stage ( Group F )
2011: Round of 16 ( lost to Arbil 0–1 )
2007: Quarter-finals ( lost to Al Faisaly 3–7 on Argegate )
2006: Quarter-finals ( lost to Al-Wahdat 0–5 on Argegate )
2005: Quarter-finals ( lost to Al Faisaly 0–2 on Argegate )


== References ==


== External links ==
Official club website
S.League website page on Tampines Rovers FC