Tampines (tæmpəˈniːs) (orTampines New Town) is the largest residential area in the city-state of Singapore. It is the second largest commercial hub outside the Central Region of Singapore after the Jurong Lake District. It is situated in the East Region of the main island. The town is so named because in the 1900s (decade) a large forest of ironwood trees, or tempinis, were there.
Like other districts in Singapore, it is densely populated with it being one of the most populous housing estates in Singapore. Tampines New Town is a regional centre that lies to the east of Singapore's city centre, much like the centrally located Orchard Road.


== History ==

In the past, Tampines was covered by forests, swamp and sand quarries. Ironwood trees, or tempinis, grew abundantly here and thus gave the area its name. It was part of military training area until about 1987.
The name Tampines goes back to the Franklin and Jackson map of 1828. It is named after Sungei Tampenus, which in turn got its name from the tampines trees (Streblus elongatus) which were said to be growing there. The oldest street in the area, Tampines Road, dates to 1864, when it was a cart tract. At the turn of the 20th century, Tampines was a rubber plantation. Tampines is also home to the sand quarry for a long time. Among the plantations were Teo Tek Ho and Hun Yeang estates.
The new town started in 1978. Construction began for Neighbourhoods 1 and 2 and was completed between 1983 and 1987 although they were given priority. Neighbourhoods 8 and 9 started in 1985–1989, followed by Neighbourhood 5 which was completed in 1989 with the Tampines Town Centre. Neighbourhood 4 was completed with the new Tampines North Division between 1986 and 1988. Tampines New Town was at the fast paced expansion, that breaks it into Tampines East, Tampines West, Tampines North and Tampines Changkat divisions.
For the Singapore MRT plans, they showed "Tampines North" and "Tampines South" since the planning stages which is due to the similar townships from 1979 to 1982, before they were renamed respectively in 1985 to Tampines and Simei.
New construction methods expedited the development of the town's infrastructure. More attractive designs, colours and finishings were incorporated into Tampines, compared to earlier public housing which consisted of uniform slabs of concrete laid out row after row with more thought given to function than form. The Housing and Development Board (HDB) managed the construction of the town until 1991, when it handed the reins over to the Tampines Town Council. The Town Council is run by grassroot leaders and the residents themselves.
The Building and Social Housing Foundation (BSHF) of the United Nations awarded the World Habitat Award to Tampines, which was selected as a representative of Singapore's new towns, on 5 October 1992. The award was given in recognition of an outstanding contribution towards human settlement and development.
Neighbourhoods 3 and 7 were only fully completed in 1997, and the constituencies had been reformed to include the new Tampines Central division. Construction was paused until the developments of Tampines Central were started in 2010, which includes The Premiere @ Tampines, Tampines GreenLeaf, Centrale 8, Tampines Trilliant and Citylife @ Tampines, including some of the other leftover pockets of residential developments such as Tampines GreenTerrace, Arc @ Tampines, Q Bay Residences and The Santorini.
Neighbourhood 6, which is also known as Tampines North New Town, has started construction with the first Build-To-Order (BTO) flats Tampines GreenRidges being announced at the end of November 2014. Tampines GreenRidges is also part of the first phase of development of the Tampines North New Town's Park West District, which is the first district to be constructed in the Tampines North New Town development.


== Geography ==
Tampines is bounded by the Tampines Expressway, Tampines Avenue 10, canal north of Bedok Reservoir, Upper Changi Road, Simei Avenue, north of private estate off Upper Changi Road, Simei Road, west of private estate off Upper Changi Road North, the Pan Island Expressway, and back along Tampines Expressway. For administrative purposes, it includes the boundaries of Simei New Town located south of Tampines New Town proper.


== Amenities ==
Tampines New Town is home to over 237,800 residents living in 152,000 HDB flats spread out over 24.24 square kilometres:
Tampines New Town
Tampines North (401-490B, 491A-495F, 496A-496G, 497A-497L, 498A-498M, 499A-499C)
Tampines East (201–271, 352-374A, 381–396, 501–510, 512–513)
Tampines West (140–151, 156–166, 801–829, 887/A-899/A, 902–903, 906–916, 921–922, 924–946)
Tampines Changkat (101–114, 124–127, 136–139, 272–298, 299A/B, 301–343, 345-351A)
Tampines Central (515/A-D-520/A-C, 522/A-C-524/A-B, 701-742A, 830-886A)

Tampines North New Town (U/C)Park West District (601-607C(U/C))
Park East District
Boulevard District
Green Walk District

Others (Mainly commercial and industrial parks, no residential areas)
Tampines Retail Park
Tampines Industrial Park A
Tampines LogisPark
Tampines Wafer Fab Park
Tampines Hi-Tech Park
Tampines Advance Display Park


=== Tampines Regional Centre ===
The urban planning policy of Singapore is to create partially self-sufficient towns, in terms of commercial needs, to relieve strain on traffic drawn to the city centre. Thus, an array of facilities are provided primarily for residents in the new towns. Tampines is one of Singapore's four regional centres (along with Woodlands, Jurong East and future Seletar), under the plan of the Urban Redevelopment Authority. As a result, the Tampines Regional Centre serves the Tampines residents and the entire East Region.


=== Commercial services ===

Retail shopping in the Tampines Regional Centre is done at three main shopping malls: Tampines Mall, Century Square and Tampines 1. Commercial tenants of the shopping centres include restaurants, supermarkets, department stores, cinemas, bookstores, jewellery and gift shops.
On 30 November 2006, IKEA opened its second outlet in Singapore at Tampines Retail Park, with adjacent Courts and Giant, and together, this three is the first to have a warehouse retail store in Singapore.
On 6 April 2009, UNIQLO opened its first outlet in Singapore at Tampines 1.


=== Community services ===

The Tampines Regional Library is near the Tampines Town Centre and organises events for children and adults to promote reading and learning.


=== Parks ===
The three main parks in the Tampines are Sunplaza Park, at Tampines Avenue 7 and 9; Tampines Bike Park (which officially closes on the 17 September 2014, as to make way for the future developments of the future Tampines North New Town.), at the junction of Tampines 9 and 7; and NParks latest nature park as of 24 April 2011, Tampines Eco Green, at the junction of Tampines 12 and 9. All of the parks are close to each other to provide easy access to each.
The other parks in Tampines are mainly community parks– Tampines North Park, Tampines Leisure Park, Tampines Central Park, Tampines Park, Festival Park, Tampines Green, Tampines Tree Garden, and some neighbourhood parks. Occasionally, community-related events are held at Festival Park.
There's also another unofficial park in Tampines, it is Tampines Quarry Park, which originally was a sand quarry. As time passed, rain water filled the quarry. It is the only park in Tampines that is not equipped with any facilities, but this park is still popular among residents living nearby. There are no signs to the park and there is no entrance as it is hidden among the greenery. There are hidden pathways to enter.
In future, there will be mainly 2 new main parks in Tampines, namely Tampines Boulevard Park and Tampines North Quarry Park which will be located at the future Tampines North New Town. There will also be more new neighbourhood parks added in the future in both Tampines New Town and Tampines North New Town together with the developments in the area.


== Transportation ==

A network of expressways, the Pan-Island Expressway and Tampines Expressway, and arterial roads allows easy movement within the town and link it to other parts of the island. Public transportation is served by the Mass Rapid Transit at Tampines MRT Station, Tampines West MRT Station and Tampines East MRT Station, and two bus interchanges, the Tampines Bus Interchange and the future Tampines North Bus Interchange.
SBS Townlink bus services bring residents from the Town Centre (where the Tampines MRT station and Tampines bus interchange is) to their doorsteps.


== Education ==
The 12 primary schools, nine secondary schools, two tertiary institutions (one junior college and one polytechnic), and one international school to provide education for Tampines residents and those living in the region. There are plans to add new schools in Tampines due to a high demand in the East Region of the city-state of Singapore. Due to the drastic decline in student enrolment numbers over the years, the current Qiaonan Primary School and Griffiths Primary School in Tampines will merge to form Angsana Primary School in 2015. Angsana Primary School will help to build the rich histories of both schools and prepare pupils to be of good character.


=== Primary Schools ===
Angsana Primary School
British Council Pre-school
Chongzheng Primary School
East Spring Primary School
East View Primary School
Gongshang Primary School
Junyuan Primary School
Poi Ching School
Saint Hilda's Primary School
Tampines North Primary School
Tampines Primary School
Yumin Primary School


=== Secondary Schools ===
Dunman Secondary School
East Spring Secondary School
East View Secondary School
Junyuan Secondary School
Ngee Ann Secondary School
Pasir Ris Secondary School
Springfield Secondary School
Saint Hilda's Secondary School
Tampines Secondary School


=== Tertiary Institutions ===
Tampines Junior College
Temasek Polytechnic


=== International School ===
United World College of South East Asia (Tampines Campus)


== Sports ==
Tampines Stadium, which is the home to Tampines Rovers FC, is in the area.


== Future ==


=== Tampines Town Hub ===
The Tampines Town Hub will be a new development in Tampines. It will be completed by 2016. Construction started in June 2013. It will located at Tampines Stadium part of Avenue 4 and 5, together with the swimming pool. It is not known whether any facilities belonging to existing sites (such as bus interchange, sports hall, swimming pool, stadium) might move out, but the area might be changed due to DTL Station construction at Tampines Central 1.
It is built for the residents of Tampines and the Town Hub will provide a community space where residents can gather, interact and bond with others from the community. Located in the heart of Tampines town, the first integrated lifestyle destination in the Singapore heartlands will bring retail, sports, and other community and civil services together under one roof. Catering to every interest, it will be key in providing residents with the best facilities and vibrant environment for active community living. Facilities available include a community centre, sports and recreation centres, swimming pools, bowling alleys, information centres and several offices.


=== Downtown Line, Singapore ===
Three new MRT stations will be expected to be completed in the year 2017 to serve residents of Tampines and commuters who frequent the Tampines Regional Centre.
The future Tampines West MRT Station will be under Tampines Ave 4. It is in the neighbourhood of HDB Tampines Polyview and Tampines Palmspring, within easy reach of Junyuan Primary School and East View Primary School. The station is within walking distance to Temasek Polytechnic.
The Tampines Station will be an interchange station with the East West MRT Line Tampines station. It is also next to the Tampines Bus Interchange creating a new transport hub in Tampines. It will provide commuters an alternative choice to meet their travel needs. It will serve commuters going to the office and commercial buildings at the town centre, such as CPF Tampines Building, Tampines One, Tampines Mall and Century Square.
The future Tampines East MRT Station which is under the junction of Tampines Ave 2, 9 and 7, is convenient for the residents of the HDB estates in the Tampines East vicinity and for the students of Tampines Junior College and Ngee Ann Secondary School.


=== Tampines North New Town ===
Tampines North is envisioned to be a new “green shoot” and extension of Tampines Town. The vision for Tampines North is “Tampines in Bloom: Budding Communities within a Green Tapestry”. Guided by this vision, Tampines North will capitalise on its existing greenery and proximity to Tampines Town to create an attractive living environment, through five key strategies.
These strategies are designed around a striking ‘leaf’ concept and include:
A 7.5 ha meandering Boulevard Park that will form the green spine for Tampines North, providing a scenic and seamless connection between Sun Plaza Park in the south to Sungei Api Api in the north;
A “Blossoms Walk” within the Boulevard Park to create a local yet distinct identity;
A 10 ha Quarry Park which could connect to Pasir Ris Town in the future;
A seamless pedestrian and cycling network that will weave through the various housing districts, enabling residents to cycle and walk around Tampines North with convenient links to the main activity spine; and
A new distinctive landmark mixed development comprising both commercial and residential uses, and integrated with a bus interchange.


== Neighbouring areas ==
Northwest - Pasir Ris Industrial Park
North - Pasir Ris New Town
Northeast - Loyang
East - Changi North
Southeast - Changi Business Park
Centre - Tampines
West - Paya Lebar Air Base
Southwest - Bedok Reservoir
South - Bedok North, Simei


== References ==
National Heritage Board (2002), Singapore's 100 Historic Places, Archipelago Press, ISBN 981-4068-23-3


== External links ==
Tampines Mall
Century Square
Tampines 1
Tampines Web Town