Tandoori masala is a mixture of spices specifically for use with a tandoor, or clay oven, in traditional north Indian and Pakistani cooking. The specific spices vary somewhat from one region to another, but typically include garam masala, garlic, ginger, onion, cayenne pepper, and may include other spices and additives. The spices are often ground together with a pestle and mortar.
Tandoori masala is used extensively with dishes as tandoori chicken. In this dish, the chicken is covered with a mixture of plain yogurt and tandoori masala. The chicken is then roasted in the tandoor at very high heat. The chicken prepared in this fashion has a pink-colored exterior and a savory flavor.
Other chicken dishes, in addition to tandoori chicken, use this masala, such as tikka or butter chicken, most of them Punjabi dishes. Meat other than chicken can be used, as can paneer (paneer tikka).
If freshly prepared, the masala can be stored in airtight jars for up to two months. The spice blend is also readily available at larger supermarkets and specialty Asian stores, with varying tastes depending on the brand.


== External links ==
About.com - Indian Food: Tandoori Masala
Tandoori Masala : Buy Indian Spices Online