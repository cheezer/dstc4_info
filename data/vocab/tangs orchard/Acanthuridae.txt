The Acanthuridae are the family of surgeonfishes, tangs, and unicornfishes. The family includes about 82 extant species. The family is composed of marine fish living in tropical seas, usually around coral reefs. Many of the species are brightly colored and popular for aquaria.
The distinctive characteristic of the family is the scalpel-like spines, one or more on either side of the tail ("thorn tails"), which are dangerously sharp. The dorsal, anal, and caudal fins are large, extending for most of the length of the body. The small mouths have a single row of teeth used for grazing on algae.
Surgeonfishes sometimes feed as solitary individuals, but they also often travel and feed in schools. Feeding in schools may be a mechanism for overwhelming the highly aggressive defense responses of small territorial damselfishes that vigorously guard small patches of algae on coral reefs.
Most species are relatively small and have a maximum length of 15–40 cm (6–16 in), but some members of the genus Acanthurus, some members of the genus Prionurus, and most members of the genus Naso can grow larger, with the whitemargin unicornfish (N. annulatus), the largest species in the family, reaching a length of up to 1 m (3 ft 3 in). These fishes can grow quickly in aquaria, so average growth size and suitability should be checked before adding them to a marine aquarium.


== Evolution and fossil record ==
There are several extinct genera known from fossils dating from the Eocene to Miocene:


=== Eocene genera ===
Proacanthurus
Tylerichthys
Gazolaichthys
Naseus
Tauichthys
Eorandallius
Metacanthurus


=== Oligocene genera ===
Glarithurus
Caprovesposus
Arambourgthurus
?Eonaso


=== Miocene genera ===
Marosichthys


== Etymology and taxonomic history ==
The name of the family is derived from the Greek words akantha and oura, which loosely translate to "thorn" and "tail", respectively. This refers to the distinguishing characteristic of the family, the "scalpel" found on each member's caudal peduncle. In the early 1900s, the family was called Hepatidae.


== In the aquarium ==
Tangs are very sensitive to disease in the home aquarium. However if the tang is fed enough algae and the aquarium is properly maintained disease should not be a problem. It is usually necessary to quarantine the animals for a period before introducing them to the aquarium.
Adults range from 15 to 40 centimetres (5.9 to 15.7 in) in length and most grow quickly even in aquaria. When considering a tang for an aquarium it is important to consider the size to which these fish can grow. Larger species such as the popular Pacific blue tang surgeonfish (of Finding Nemo fame), Naso or lipstick tang, lined surgeonfish, Sohal surgeonfish and Atlantic blue tang surgeonfish can grow to 40 cm (16 in) and require swimming room and hiding places.
Many also suggest adding aggressive tangs to the aquarium last as they are territorial and may fight and possibly kill other fish.
Tangs primarily graze on macroalgae from genera such as Caulerpa and Gracilaria, although they have been observed in an aquarium setting to eat meat-based fish foods. A popular technique for aquarists, is to grow macroalgae in a sump or refugium. This technique not only is economically beneficial, but serves to promote enhanced water quality through nitrate absorption. The growth of the algae can then be controlled by feeding it to the tang.


== Gallery ==


== References ==


== External links ==
R. Jamil Jonna. "Acanthuridae: surgeonfishes, tangs, unicornfishes". Animal Diversity Web. University of Michigan Museum of Zoology. 
Sepkoski, Jack (2002). "A compendium of fossil marine animal genera". Bulletins of American Paleontology 364: 560.