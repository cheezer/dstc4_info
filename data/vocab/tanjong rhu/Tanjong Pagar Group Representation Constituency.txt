Not to be confused with the defunct ward called Tanjong Pagar Single Member Constituency
Tanjong Pagar Group Representation Constituency (simplified Chinese: 丹戎巴葛集选区; traditional Chinese: 丹戎巴葛集選區) is currently a five-member Group Representation Constituency in central Singapore.
Tanjong Pagar GRC was originally led by former Minister Mentor Lee Kuan Yew who was Prime Minister from 1959 to 1990. Subsequently, he held the positions of Senior Minister from 1990 to 2004 and Minister Mentor from 2004 to 2011. Mr Lee retired from the Cabinet in 2011, but remained the leading Member of Parliament for Tanjong Pagar GRC until 23 March 2015 when he died in Singapore General Hospital.
Tanjong Pagar GRC has been held by the People's Action Party since 1991, with a walkover victory at every Nomination Day since its creation. Tanjong Pagar consists of five divisions, namely Buona Vista, Queenstown, Tanglin-Cairnhill, Tanjong Pagar-Tiong Bahru and Kreta Ayer-Kim Seng. The constituency consists of the areas of Tanjong Pagar, part of the Marina Bay area, Marina South, Queenstown, Tiong Bahru, Tanglin, Orchard Road, Bukit Merah, Buona Vista, most areas of Chinatown, Clarke Quay, Kim Seng, Outram Park.


== History ==
The GRCs had previously changed divisions as follows:
1991 - Absorbed all of the Tiong Bahru GRC, Tanjong Pagar SMC and Telok Blangah SMC
1997 - Absorbed Brickworks GRC (Queenstown & Brickworks), Buona Vista SMC and Leng Kee SMC & split off Telok Blangah into West Coast GRC
2001 - Absorbed Kreta Ayer-Tanglin GRC (Tanglin-Cairnhill & Moulmein) & split off Buona Vista into the new Holland-Bukit Panjang GRC
2006 - No change except shrinking parts of Holland-Bukit Panjang GRC transferred to Tanjong Pagar GRC.
2011 - Absorbed Holland-Bukit Timah GRC (Buona Vista) & Jalan Besar GRC (Kreta Ayer-Kim Seng), split off Moulmein part into Moulmein-Kallang GRC and Radin Mas part into Radin Mas SMC. Tiong Bahru was merged into Tanjong Pagar.
2015 - A swap between Moulmein-Kallang GRC/Jalan Besar GRC and Tanjong Pagar GRC. In return, Moulmein goes back to Tanjong Pagar GRC while Kreta Ayer-Kim Seng relocates back to Jalan Besar GRC.
Previous Members of Parliament for the GRCs usually cut their teeth of the constituencies and became ministers before moving on to the new GRC:
1991 - 1997: Lim Hng Kiang (became West Coast GRC)
1997 - 2001: Lim Swee Say (became East Coast GRC)
2001 - 2006: Khaw Boon Wan (became Sembawang GRC)
2006 - 2011: Lui Tuck Yew (became Moulmein-Kallang GRC)


== Members of Parliament ==
Note: Lee Kuan Yew died on 23 March 2015. He had served as a MP for the constituency for 60 years. His MPS were conducted by Koo Tsai Kee from 1991 to 2011; and by Indranee Rajah since 2011. His seat in parliament is currently vacant.


== Operational responsibility ==
Tanjong Pagar-Tiong Bahru - Tanjong Pagar (part), The Pinnacle @ Duxton, Everton Park, Spottiswoode Park, Raeburn old flats, Kim Tian, Jalan Membina and Tiong Bahru Pre-War Flats
Kreta Ayer-Kim Seng - Havelock Road, Jalan Bukit Ho Swee, Great World City, Outram Secondary School, Manhattan House, Kreta Ayer, Chinatown, Maxwell Road, MND Building, Marina Bay Sands, Marina Bay Residences, Raffles Place (part), Marina Barrage, Marina Bay, Gardens by the Bay
Tanglin-Cairnhill - Margaret Drive, Strathmore Estate, Jervois Road, Chatsworth Road, Botanic Gardens, Farrer Road (Blocks 5 & 6), Orchard, Somerset, Dhoby Ghaut, Istana, Scotts Square, Cairnhill, Tanglin, Balmoral Road, Bukit Merah View, River Valley Road, Henderson Crescent
Queenstown - All Queenstown HDB flats, Jalan Bukit Merah (Brickworks Estate), Hoy Fatt Road/Jalan Rumah Tinggi/Lengkok Bahru
Buona Vista - Commonwealth Heights, Holland Village, Holland Vista, Buona Vista Court, Holland Oasis, Commonwealth 16, Crescent View & Crescent Green


== Candidates and results ==


=== Elections in 2010s ===


=== Elections in 2000s ===


=== Elections in 1990s ===


== Failed attempt at contest in GE 2011 ==
In GE 2011, a group of independents led by Ng Teck Siong were disqualified from contesting Tanjong Pagar GRC against the PAP team as they submitted nomination papers 35 seconds late.


== See also ==
Lee Kuan Yew
Tanjong Pagar SMC


== References ==