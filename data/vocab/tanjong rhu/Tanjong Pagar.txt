Tanjong Pagar (alternatively spelled Tanjung Pagar) is a historic district located within the Central Business District in Singapore, straddling the Outram Planning Area and the Downtown Core under the Urban Redevelopment Authority's urban planning zones.


== Etymology ==

Tanjong Pagar in Malay means "cape of stakes", a name which reflects its origins as a fishing village situated on a former promontory. It has been surmised that the name was inspired by the presence of kelongs (offshore palisade fishing traps constructed using wooden stakes and cross pieces) set up along the stretch of coast from the village of Tanjong Malang to what is now Tanjong Pagar. It is possibly a corruption of the earlier name Tanjong Passar, a road which led from South Bridge Road to the fishing village and which appeared in George Drumgoole Coleman's 1836 Map of the Town.
A far more picturesque account of the naming of this part of the coast emerges from the realm of local legend. According to the Malay Annals, there was a time when the villages along the coast of Singapore suffered from vicious attacks from shoals of swordfish. On the advice of a particularly astute boy named Hang Nadim, the Sri Maharajah built a barricade of banana stems along the coast, which successfully trapped the attacking fish by their snouts as they leapt from the waters.
The original name for Tanjong Pagar is also said to be Salinter, a fishing village. When the Tanjong Pagar Dock Company (1864) was formed due to the growth of shipping activities in the 1850s, wharves were built. Tanjong is "cape" and pagar means "fence" or enclosed space, i.e. wharf where ships are moored. Tanjong Pagar probably refers to the location of PSA Gate 3 near Victoria Dock. Around Tanjong Pagar were mangrove swamps which were filled in with earth from Mount Palmer and other nearby small hills for extension of the wharves up to Telok Blangah.
Tanjong Pagar Road is known as tan jiong pa kat in Hokkien (Min Nan), which is phonetic.


== History ==

For many years, Tanjong Pagar, located between the docks and the town, was an enclave for the thousands of Chinese and Indian dock workers who had migrated to Singapore from the mid-19th century. With all the traffic between the docks and the town, Tanjong Pagar was also lucrative ground for rickshaw pullers awaiting clients. So prevalent was their presence that in 1904, the government established a Jinricksha Station at the junction of Tanjong Pagar Road and Neil Road.
From the time the docks began operations in 1864, land values in Tanjong Pagar rose, attracting wealthy Chinese and Arab traders to buy real estate there.
The proliferation of impoverished workers led to overcrowding, pollution and social problems such as opium smoking and prostitution. Tanjong Pagar generally deteriorated into an inner city ghetto. By World War II, Tanjong Pagar was a predominantly working class Hokkien area with an Indian minority.
In the mid-1980s, Tanjong Pagar became the first area in Singapore to be gazetted under the government's conservation plan. When the conservation project was completed, many of the area's shophouses were restored to their original appearance. But although a few traces of the old Tanjong Pagar remain – an old swimming pool, the odd street cobbler – the face of Tanjong Pagar has changed. Today, Tanjong Pagar has become a fashionable district, filled with thriving businesses, cafés, bars and restaurants.


== Points of interest ==


=== Tras Street ===

The street name Tras Street dates from an 1898 municipal resolution to "use names of rivers and districts in the Malay Peninsula as being better adapted to the purpose [of naming streets] than the names of persons or families".
Tras Street today is a thriving night spot featuring many pubs, clubs and KTV bars.


=== Cantonment Road ===

Cantonment Road got its name from the contingent of Indian sepoys stationed here in 1819. They had accompanied Sir Stamford Raffles to Singapore and were asked to stay. In India, the English term for permanent military accommodation, as established by the sepoys, is "cantonment".
The local Cantonese had another name for Cantonment Road. They called it Ba Suo Wei, meaning "at the foot of Bukit Pasoh".
Outram Road, which used to be part of Cantonment Road, only became a separate thoroughfare in 1853. The old Chinese name for Outram was Si Pai Po, meaning "sepoy's field", referring to the former sepoy presence in the area during colonial days.


=== Duxton Hill ===

Dr J.W. Montgomerie, the first owner of Duxton Hill, cultivated nutmeg plantations on its slopes. Montgomerie died in 1856 and his land on Duxton was auctioned off. Fourteen acres went to Arab Syed Abdullah bin Omar Aljunied, who divided them into four lots which were leased to wealthy Chinese developers.
By the 1890s, the developers had built two- and three-storey shophouses in Duxton Hill and the more affluent Chinese moved to the area.


=== Tanjong Pagar Plaza ===
Tanjong Pagar Plaza, the site of a complex of which replaced pre-war shophouses along Tanjong Pagar Road, was formerly Cheng Cheok Street after Khoo Cheng Cheok. Khoo Cheng Cheok is believed to be the brother of rice merchant Khoo Cheng Tiong, who was president of the Thong Chai Medical Institution. It was once an important crossroads for traffic between the warehouses along the Singapore River and the wharves. Bullock carts and hand carts streamed through the area carrying goods from one point to the other.
Tanjong Pagar Plaza refers to the shophouses which is built to accommodate businesses by HDB. The food centre is notable for its local dishes such as nasi lemak and fish soup, and there are as many as four stalls selling nasi lemak, and five stalls selling fish soup.
Part of HDB's plan in early urban planning was to integrate housing near businesses within the CBD area. However, the offices and shophouses there are separate from HDB housing.


=== Railway transport ===

The Malaysian railway company (Keretapi Tanah Melayu (KTM)) ran trains to a terminal railway station here. Three daily train ran to Kuala Lumpur and other trains served other parts of Malaysia.
Following an agreement between Malaysia and Singapore on 24 May 2010, the station ceased operation on 1 July 2011. KTM's southern terminus is now at the Woodlands Train Checkpoint near the causeway.
The Singaporean government has promised to conserve the Tanjong Pagar railway station building and may integrate it into future developments on the site.


=== Maxwell Food Centre ===

The Maxwell Food Centre dates back to pre-war days as a fresh food market and food centre. In 1986, it was converted into a food centre, housing hawkers from the vicinity. The present existing hawker centre was renovated in 2001. Stallholders are mainly those from the essentially Cantonese neighbourhood, with many from the famed food street, China Street. A wide variety of authentic local favourites are available at Maxwell Food Centre, with a Cantonese bent. Popular dishes include hum chim peng (a crusty fried pancake), ngor hiang or Hokkien meat roll, and herbal broths made from home-brewed recipes.


=== Telok Ayer Performing Arts Centre ===
The Telok Ayer Performing Arts Centre (TAPAC) houses arts full-time and part-time groups of different cultural traditions and art disciplines, and has the distinction of being the first property to be acquired under the National Arts Council's Arts Housing Scheme in 1985. It is located at Cecil Street next to Tanjong Pagar MRT Station.


=== Little Korea ===
Tanjong Pagar has been greatly influenced by Korean culture in recent years and has earned the nickname of Little Korea and Koreatown. As many as 15 Korean food outlets have sprung up in a 300m stretch of Tanjong Pagar Road near Duxton Hill over the last few years. Apart from Korean eateries, there are also many wedding boutiques along Tanjong Pagar Road and at least three wedding boutiques there offer Korean-themed wedding photography.


== Politics ==
Tanjong Pagar is mainly located in the Tanjong Pagar division of Tanjong Pagar Group Representation Constituency. Since the electoral ward was created in 1955, Lee Kuan Yew of the People's Action Party served as its Member of Parliament until his death on 23 March 2015. Lee's replacement is yet to be announced by the party.


== References ==
Victor R Savage, Brenda S A Yeoh (2003), Toponymics – A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1
National Heritage Board (2002), Singapore's 100 Historic Places, Archipelago Press, ISBN 981-4068-23-3


== External links ==