Tanjong Public Limited Company (MYX: 2267) was initially founded as Tanjong Tin Dredging LTD on 2 January 1926 in England. The company subsequently changed its name to Tanjong PLC in 1991, following a corporate restructure. Tanjong shares are listed on the Bursa Malaysia and were formerly listed on the London Stock Exchange.
The company's principal activities include the operation of Pan Malaysian Pools Sdn Bhd, a Malaysian lottery business. They have diversified into power generation plants, property investment, and liquefied petroleum gas distribution.


== Power generation ==
Through its subsidiary, Tanjong PLC owns and operates 13 power generation plants located throughout Middle East, North Africa, South Asia and Asia.


== References ==