Tatsuya Isaka (伊阪 達也, Isaka Tatsuya, born July 12, 1985, in Ishikawa Prefecture) is a Japanese actor. He played Ichigo Kurosaki in the rock musical Bleach.


== Television dramas ==
Gokusen (2002) (Guest star)
Water Boys (2003)
Genseishin Justiriser (2004–2005)
Tumbling (Film) (2010)


== Stage ==
Rock Musical Bleach - Ichigo Kurosaki


== References ==