Tatsuya Yoshida (吉田達也, Yoshida Tatsuya) (born in Kitakami, Iwate) is a Japanese musician; drummer and composer who is the only consistent member of the renowned progressive rock duo Ruins, as well as Koenji Hyakkei. He is also a member of the progressive rock trios Korekyojinn and Daimonji. Outside of his own groups, Yoshida is renowned for his tenure as drummer in the indie progressive group YBO2, a band also featuring guitarist KK Null, whom he also joins in the current line up of Zeni Geva[1] and he has played drums in a late edition of Samla Mammas Manna. He has been cited as "[the] indisputable master drummer of the Japanese underground" [2].
Along with his participation in bands, he has also released several solo recordings.


== Discography ==
Solo Works '88 (1988)
Solo Works '89 (1989)
Magaibutsu '91 (1991)
Drums, Voices, Keyboards & Guitar (1994)
Pianoworks '94 (1994)
First Meeting (1995)
A Million Years (1997)
A Is for Accident (1997)
With Ruins
Ruins III (1988) (reissued as Infect in 1993)
Stonehenge (1990)
Burning Stone (1992)
Graviyaunosch (1993)
Hyderomastgroningem (1995)
Refusal Fossil (1997/2007)
Vrresto (1998)
Symphonica (1998)
Pallaschtom (2000)
Tzomborgha (2002)
Alone (2011)
With Painkiller
The Prophecy: Live in Europe (Tzadik, 2013)
A complete list of all recordings Yoshida Tatsuya appears on can be found here.


== References ==


== External links ==
Official Website
New Official Website