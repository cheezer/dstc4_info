Kedah Malay or Kedahan (Also known as Pelat Utara or Loghat Utara 'Northern Dialect') is a variety of the Malayan languages mainly spoken in the northwestern northern Malaysian states of Perlis, Kedah, Penang, and northern Perak and in the southern Thai provinces of Trang and Satun, where it is called "Satun Malay" (มลายูถิ่นสตูล). Speakers in Trang are most heavily influenced by Thai language.
Kedahan Malay can be divided into several dialects, namely Kedah Persisiran (standard), Baling or Kedah Hulu, Kedah Utara, Perlis-Langkawi, Penang and some others outside Malaysia. See Malayan languages for a comparison of Kedah Persisiran, Penang and Baling dialects.
The main characteristic of Kedahan Malay is the -a final vocal is pronounced as /ɑ/ such as /a/ in "dark", which is varied from standard Malay -a that pronounced as /a/. Other characteristics of the dialect are final consonant -r is pronounced as -q and final consonant -s is pronounced as -ih (e.g.:Lapar = Lapaq (Hungry), Lepas = Lepaih (release, after) ) while initial and middle r are guttural.
Some word examples of Kedahan Malay:


== See also ==
Kedahan Malay


== References ==


== External links ==
Kedahan Malay Facebook Community
Kedahan-Standard Malay Online dictionary