A hackney or hackney carriage (also called a cab, black cab, hack or London taxi) is a carriage or automobile for hire. A hackney of a more expensive or high class was called a remise.
In the United Kingdom, the name hackney carriage today refers to a taxicab licensed by the Public Carriage Office, local authority (non-metropolitan district councils, unitary authorities) or the Department of the Environment depending on region of the country.
In the United States, the police department of the city of Boston has a Hackney Carriage Unit, analogous to taxicab regulators in other cities, that issues Hackney Carriage medallions to its taxi operators.


== Etymology ==
The name 'hackney' was once thought to be an anglicized derivative of French haquenée—a horse of medium size recommended for lady riders; however, current opinion is that it is derived from the village name Hackney (now part of London). Despite the currency of this opinion, however, earlier sources dispute it. In 1908, a popular London newspaper stated, "The hackney coach - which is commonly supposed, though wrongly, to have taken its name from the district in the north of London - was started in the metropolis so long ago as 1025 by a certain Captain Bailey." The place-name, through its fame for its horses and horse-drawn carriages, is also the root of the Spanish word jaca, a term used for a small breed of horse and the Sardinian achetta horse. The first documented 'hackney coach'—the forerunner of the more generic 'hackney carriage'—operated in London in 1621.
The New York colloquial terms "hack" (taxi or taxi-driver), "hackstand" (taxi stand), and "hack license" (taxi license) are probably derived from "hackney carriage". Such cabs are now regulated by the New York City Taxi and Limousine Commission.


== History ==
"An Ordinance for the Regulation of Hackney-Coachmen in London and the places adjacent" was approved by Parliament in 1654, to remedy what it described as the "many Inconveniences [that] do daily arise by reason of the late increase and great irregularity of Hackney Coaches and Hackney Coachmen in London, Westminster and the places thereabouts". The first hackney-carriage licences date from 1662, and applied literally to horse-drawn carriages, later modernised as hansom cabs (1834), that operated as vehicles for hire. There was a distinction between a general hackney carriage and a hackney coach, a hireable vehicle with specifically four wheels, two horses and six seats, and driven by a Jarvey (also spelled jarvie).
In 19th century London, private carriages were commonly sold off for use as hackney carriages, often displaying painted-over traces of the previous owner's coat of arms on the doors.
The growler was a type of four-wheel, enclosed carriage drawn by two horses used as a hackney carriage, that is, as a vehicle for hire with a coachman. It is distinguished from a cab, hansom cab or cabriolet, in that those had only two wheels. It is distinguished from most coaches by being of slightly smaller size, holding nominally four passengers, and being much less ostentatious.
A small, usually two-wheeled, one-horse hackney vehicle called a noddy once plied the roads in Ireland and Scotland. The French had a small hackney coach called a fiacre.


=== Motorisation ===
Electric hackney carriages appeared before the introduction of the internal combustion engine to vehicles for hire in 1901. In fact there was even London Electrical Cab Company: the cabs were informally called Berseys after the manager who designed them, Walter C. Bersey. Another nickname was ‘Hummingbirds’ from the sound that they made. In August 1897 25 were introduced, and by 1898 there were 50 more. During the 20th century, cars generally replaced horse-drawn models, and the last horse-drawn hackney carriage ceased service in London in 1947.
UK regulations define a hackney carriage as a taxicab allowed to ply the streets looking for passengers to pick up, as opposed to private hire vehicles (sometimes called minicabs), which may pick up only passengers who have previously booked or who visit the taxi operator's office.
In 1999, the first of a series of fuel cell powered taxis were trialled in London. The "Millennium Cab" built by ZeTek gained television coverage and great interest when driven in the Sheraton Hotel ballroom in New York by Judd Hirsch, the star of the television series Taxi. ZeTek built three cabs but ceased activities in 2001.


=== Continuing horse-drawn cab services ===
Horse-drawn hackney services continue to operate in parts of the UK, for example in Cockington, Torquay. The Australian city of Melbourne has, in recent years, introduced horse-drawn hire carriages as an adjunct to its promotion of tourism.. The Town of Windsor, Berkshire, is believed to be the last remaining Town with a continuous lineage of Horse Drawn Hackey carriages, currently run by Orchard Poyle Carriages, the licence having been passed down from driver to driver since 1830. The Royal Borough now licences the carriage for rides around Windsor Castle and The Great Park, however the original hackney license is in place allowing for passenger travel under the same 1662 law originally passed.


== Black cabs ==

Motorised hackney cabs in the UK, traditionally all black in London, are known as black cabs, although they are now produced in a variety of colours, sometimes in advertising brand liveries (see below). The 50 golden cabs produced for the Queen's Golden Jubilee celebrations in 2002 were notable.


=== Vehicle design ===
Historically four-door saloon cars have been highly popular as hackney carriages, but with disability regulations growing in strength and some councils offering free licensing for disabled-friendly vehicles, many operators are now opting for wheelchair-adapted taxis such as the LTI. Other models of specialist taxis include the Peugeot E7 and rivals from Fiat, Volkswagen, Metrocab and Mercedes-Benz. These vehicles normally allow six or seven passengers, although some models can accommodate eight. Some of these 'minibus' taxis include a front passenger seat next to the driver, while others reserve this space solely for luggage.
Many black cabs have a turning circle of only 25 ft (8 m). One reason for this is the configuration of the famed Savoy Hotel: The hotel entrance's small roundabout meant that vehicles needed the small turning circle in order to navigate it. That requirement became the legally required turning circles for all London cabs, while the custom of a passenger's sitting on the right, behind the driver, provided a reason for the right-hand traffic in Savoy Court, allowing hotel patrons to board and alight from the driver's side.


=== Driver qualification ===
In London, hackney-carriage drivers have to pass a test called The Knowledge to demonstrate that they have an intimate knowledge of the geography of London streets, important buildings etc. There are two types of badge, a yellow one for the suburban areas and a green one for all of London. The latter is considered far more difficult. Drivers who own their cabs as opposed to renting from a garage are known as "mushers" and those who have just passed the "knowledge" are known as "butter boys". There are currently around 21,000 black cabs in London, licensed by the Public Carriage Office.
Elsewhere, councils have their own regulations. Some merely require a driver to pass a Criminal Records Bureau disclosure and have a reasonably clean driving licence, while others use their own local versions of London's The Knowledge test.


=== Private users ===
Oil millionaire Nubar Gulbenkian drove about in a custom-built gold and black car, designed to look like a vintage London taxi and powered by a Rolls-Royce engine, because he had been told "it can turn on a sixpence" Other celebrities are known to have used hackney carriages both for their anonymity and their ruggedness/manoeuvrability in London traffic. Such users included Prince Philip, whose cab was converted to run on liquefied petroleum gas, author and actor Stephen Fry, and the Sheriffs of the City of London. A Black cab was used in the band Oasis's video for the song 'Don't look back in anger'. Black cabs also have recently served as recording studios for indie band performances and other performances in the Black Cab Sessions internet project.
British TV Presenter, Yvette Fielding owns one and it is seen on her show Ghosthunting With.... Bez of the Happy Mondays owns one, it was featured on the UK edition of Pimp My Ride.
The Governor's official car In the Falkland Islands was a London taxi between 1976 and 2010.


=== In other countries ===
Between 2003 and 1 August 2009, it was possible to purchase the London taxi model TXII in the United States. Today there are approximately 250 TXIIs in the US, operating as taxis in San Francisco, Dallas, Long Beach, Houston, New Orleans, Las Vegas, Newport, Rhode Island, Wilmington, North Carolina and Portland, Oregon. There are also a few operating in Ottawa, Canada. The largest London taxi rental fleet in North America is located in Wilmington, North Carolina and owned by The British Taxi Company. Cabs can also be seen in Saudi Arabia, South Africa, Lebanon, Egypt, Bahrain and Cyprus, as well as in Israel, where a Chinese-made version of LTI's model TX4 built by Geely Automobile is available for sale. On February 2010, a number of TX4s started operating in the capital of Kosovo - Pristina. They are known as London Taxi. After July 2009, new London taxis were no longer available to buy in the United States. Singapore has used London-style cabs since 1992; starting with the "Fairway". The flagdown fares for the London Taxis are the same as for other taxis. SMRT Corporation, the sole operator, has as of March 2013 replaced its fleet of 15 ageing multi coloured (gold, pink, etc.) taxis with new white ones. These remain the only wheelchair accessible taxis in Singapore and have been brought back following an outcry as to the removal of such a service.
As of 2011, 1000 of a Chinese-made version of LTI's latest model TX4 were ordered by Baku Taxi Company. The plan is part of a program originally announced by the Ministry of Transportation of Azerbaijan to introduce London cabs to the capital, Baku. The move was part of £16 million agreement between Manganese Bronze and Baku Taxi Company.


=== Variety of models ===
There have been different makes and types of hackney cab through the years, including:
Mann & Overton — now LTI
Unic sold in London from 1906 to 1930s
Austin London Taxicab
Austin FX3
Austin/Carbodies/LTI FX4 and Fairway
LTI TX1, TXII and TX4

Mercedes-Benz
Vito W639

Morris
London General Cab Co
Citroen

Beardmore
Nuffield Oxford Taxi
Beardmore Marks I to VII

Metrocab
MCW/Reliant/Hooper Metrocab


=== Use in advertising ===

The unique body of the London taxi is occasionally wrapped with all-over advertising, known as a "livery". In October 2011 the company Eyetease Ltd. first introduced the use of digital screens on the roofs of London taxis for the purposes of broadcast advertising. 


== Future ==
On 14 December 2010, London Mayor Boris Johnson released an air quality strategy paper encouraging a phase-out of the oldest of the LT Cabs and proposing a £1m fund to encourage taxi owners to upgrade to low emission vehicles such as electric taxi cabs. On the same day, transport minister Philip Hammond unveiled the £5,000 electric car subsidy.


== Digital hailing ==
2011 saw the launch of many digital hailing applications for hackney carriages that operate through smartphones including, GetTaxi and Hailo. Many of these applications also facilitate payment and tracking of the taxicabs.


== See also ==
Taxicabs of the United Kingdom
Illegal taxicab operation#Terminology
Manganese Bronze—the manufacturer of London black cabs.
Worshipful Company of Hackney Carriage Drivers
Carriage
Wagon


== References ==


== External links ==
Owners Club and Forum.
Taxi fare calculator based on fares set by local authorities.
Taxis and private hire | Transport for London. Public Carriage Office.
London hackney coach regulations, 1819. Genealogy UK Genealogy and Family History.
Old London Taxi Web Site and Forum | Transport for London. Fairway Driver.