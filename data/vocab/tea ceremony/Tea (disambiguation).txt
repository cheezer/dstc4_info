Tea or TEA can mean:
Tea, a traditional beverage made from steeping the processed leaves, buds, or twigs of the tea bush (Camellia sinensis) in water.


== Beverage and food ==
Herbal tea, or tisane, a catch-all term for any non-caffeinated beverage made from the infusion or decoction of herbs, spices, or other plant material
Tea (meal), any of several mealtimes, depending on different regional customs, (usu. western cultures, such as British)


== Chemistry ==
Tetraethylammonium, a potassium channel blocker used in neurophysiology
Triethanolamine, an organic chemical used in cosmetics and to bind aluminium ions
Triethylaluminium, a volatile organic chemical used in jet engines and as co-catalyst in olefin polymerization
Triethylamine, a colorless, "fishy"-smelling organic chemical used in chemical synthesis
5-epiaristolochene synthase, an enzyme
Thermal Energy Analyzer analytical detector for TSNA


== Geography ==
Tea, South Dakota, a suburb of Sioux Falls
Tea River, a river in Brazil


== Media and music ==
Tea, a novel by Stacey D'Erasmo, published in 2000
Tea: A Mirror of Soul, a 2002 opera by composer Tan Dun


== Medicine ==
TEAS test (Test of Essential Academic Skills), a standardized test for nursing school admission
Transient epileptic amnesia, a temporal lobe epilepsy
Thrombendarterectomy, see Thrombectomy


== Organizations and companies ==
Tamil Eelam Army, a defunct Tamil separatist group in Sri Lanka
Texas Economics Association, a student organization at the University of Texas at Austin
Texas Education Agency, a branch of the Texas, USA state government which oversees public primary and secondary education
Themed Entertainment Association, a group representing people involved in theme parks and similar attractions
Thorium Energy Alliance
Tirupur Exporters' Association, an association of cotton knitwear exporters from Tirupur, India
Trans European Airways, Belgium-based airline, now defunct
Trenes Especiales Argentinos (Spanish: "Special Argentine Trains"), a private railway company in Argentina
Trustees Executors and Agency Company, collapsed Australian trustee company (1879–1983)


== Sports ==
Tea Vikstedt-Nyman (born 1959), Finnish racing cyclist
Tea break, the second of two daily intervals during certain types of cricket


== Technology and science ==
Tea (programming language), a high level scripting language for the Java environment combining features from Scheme, Tcl and Java
TEA laser, Transverse Excitation at Atmospheric pressure
Tiny Encryption Algorithm, a block cipher notable for its simplicity of description and implementation
TETRA Encryption Algorithm, an encryption algorithm used in Terrestrial Trunked Radio


== People ==
Tea (given name), a feminine given name
Tea (family name), a Cambodian given name
Téa, a feminine given name


== Other ==
Targeted Employment Area, a designation for a region of the United States where the threshold for investment to apply for an EB-5 visa is $500,000 instead of the usual $1,000,000.
Transportation Equity Act for the 21st Century, an act that authorized the U.S. Federal surface transportation programs for highways in 1998–2003
Tea, a slang term for marijuana
T.E.A. (Test of English for Aviation)
453 Tea, an asteroid


== See also ==
Tee
All pages beginning with "Tea"