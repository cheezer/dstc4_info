Boon Teck Single Member Constituency was an constituency in Singapore. It existed from 1972 to 1988, and was merged with Toa Payoh GRC.


== Members of Parliament ==
Phey Yew Kok (1972 - 1980)
Liew Kok Phun (1980 - 1984)
Ho Tat Kin (1984 - 1988)