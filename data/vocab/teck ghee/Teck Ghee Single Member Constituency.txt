Teck Ghee Single Member Constituency (Traditional Chinese: 德义單選區; Simplified Chinese: 德义单选区) is a former single member constituency in Ang Mo Kio, Singapore from 1984 to 1991 and absorbed into Ang Mo Kio GRC.
This is the ward where the third and current Prime Minister of Singapore made his debut in 1984 elections. It is one of the People's Action Party's stronghold as this Prime Minister have been held on this constituency since its inception.


== Members of Parliament ==
Lee Hsien Loong (1984 - 1991)
(Still continues as a division of Teck Ghee in Ang Mo Kio GRC, but constituency abolished)


== Candidates and Results ==


=== Elections in 1980s ===


== References ==
1988 General Election's result
1984 General Election's result