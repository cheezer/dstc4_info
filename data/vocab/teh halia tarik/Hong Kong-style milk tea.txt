Hong Kong-style milk tea is a kind of drink originated in Hong Kong, which is made from black tea and milk (usually evaporated milk or condensed milk). It is usually part of lunch in Hong Kong tea culture. Although originating from Hong Kong, it is also frequently found overseas in restaurants serving Hong Kong cuisine and Hong Kong-style western cuisine. In the show Top Eat 100 aired on 4 February 2012, Hong Kong-style milk tea is ranked number 4 in Hong Kong cuisines and Hong Kongers consume a total of 900 million glasses/cups a year.


== Origin ==
Hong Kong-style milk tea originates from British colonial rule over Hong Kong. The British practice of afternoon tea, where black tea is served with milk and sugar, grew popular in Hong Kong. Milk tea is similar, except with evaporated or condensed milk instead of ordinary milk. It is called "milk tea" (Chinese: 奶茶, Cantonese naai5 cha4) to distinguish it from "Chinese tea" (Chinese: 茶, Cantonese cha4), which is served plain. Outside of Hong Kong it is referred to as Hong Kong-style milk tea.


== Production ==
Hong Kong-style milk tea is made of a mix of several types of black tea (the proportion of which is usually a "commercial secret" for some milk tea vendors, often Pu Lei and a type of Ceylon tea), evaporated milk, and sugar, the last of which is added by the customers themselves unless in the case of take-away. A variety uses condensed milk instead of milk and sugar, giving the tea a richer feel.
To make the tea, water and tea (about 1 to 3 teaspoons of tea a cup, depending how strong the drinker likes) are brought to a boil then simmered for about 3–6 minutes. The tea is usually put in a sackcloth bag before the water is added to the pot to filter it out or if no bag available poured through a strainer. Many people also remove the pot from the heat once it boils for about 3 minutes, then bring the pot to a boil again. This process can be repeated several times, intensifying the caffeine/flavor.
The key feature of Hong Kong-style milk tea is that a sackcloth bag is used to filter the tea leaves. However any other filter/strainer may be used to filter the tea. Sackcloth bags are not completely necessary but generally preferred. The bag, reputed to make the tea smoother, gradually develops an intense brown colour as a result of prolonged tea drenching. Together with the shape of the filter, it resembles a silk stocking, giving Hong Kong-style milk tea the nickname of "pantyhose" or "silk stocking" milk tea (Chinese: 絲襪奶茶). This nickname is used in Hong Kong but less so in mainland China and overseas communities.
There is some debate over the most authentic way of making milk tea, i.e. the sequence of adding each ingredient. Some have argued that milk should be added before pouring the tea, while others hold the opposite view. Though, to most people, both methods are acceptable.
Some restaurants may choose to use condense milk, where sweetness is already mixed in and cannot be changed. This creates a creamier than normal milk tea and also a bit thicker in viscosity. On the other hand, other restaurants may use evaporated milk and allow the consumers to mix in the sugar themselves.

Milk tea is a popular part of many Hong Kongers' daily lives, typically served as part of afternoon tea, but also at breakfast or dinner. It enjoys nearly the same ubiquitous status that coffee holds in the West. Whilst not offered by more traditional Cantonese restaurants or dim sum teahouses, milk tea is standard fare in Hong Kong-style western restaurants and cha chaan teng, as well as Hong Kong's historic dai pai dong, with a price between HKD$12-16 (hot, one or two dollars more for cold). A cup of hot milk tea is usually either served in a ceramic cup (often referred to as a "coffee cup" 咖啡杯) or a tall cylindrical plastic glass.


== Criteria for quality milk tea ==
The first criterion of a good cup of milk tea is its "smoothness" (香滑); in other words, how creamy and full-bodied it is.
Another criterion for tasty milk tea (and also bubble tea) is some white frothy residue inside the lip of the cup after some of it has been drunk. This white froth means that the concentration of butterfat in the evaporated milk used is high enough.
There is also another way for locals to distinguish high quality by identifying hints of oil on top of the drink after it has been properly brewed. This is the oil remains from the extensive process through the roasting process.
The taste and texture of 'Hong Kong' style milk tea might be influenced by the milk used. For example, some Hong Kong cafés prefer using a filled milk variant, meaning it is not purely evaporated milk (as with most retail brands) but a combination of skimmed milk and soybean oil.


== Varieties ==

Today, iced milk tea is usually prepared with ice cubes. However, in the old days, when machines for producing ice cubes were not popular, the iced milk tea was made by filling the hot milk tea into a glass bottle and then cooling it in a fridge. Sometimes the milk tea were filled in Vitasoy or Coca-Cola bottles, and were sold by bottle. Today this type of "bottle milk tea" is rare in Hong Kong. Iced milk tea in cans or plastic bottles can be found in many of the convenience stores around Hong Kong such as 7-Eleven and Circle K.
In the case of milk tea with ice cubes, the melting ice will dilute the content, thus affecting the taste of the drink; therefore, many people prefer the old way of preparing iced milk tea. Today, some cha chaan tengs serve ice-less iced milk tea, made by pouring hot milk tea into a plastic cup and then cooling it in a fridge. Another way is to place the cup/bottle into a cold water bath, which is called "ice bath milk tea" (Chinese: 冰鎮奶茶, Hanyu Pinyin: bīng zhèn nǎi chá). This is often used as a selling point.
Cha chow (Chinese: 茶走) is milk tea prepared with condensed milk, instead of evaporated milk and sugar. Its taste is, as can be expected, sweeter than ordinary milk tea. In the old days, Cha chow was mostly drunk by older people who had "congestion" in their throats.
Milk tea and coffee together is called yuan yang (yin yang) (鴛鴦).
A variation on "silk stocking tea" is "silk stocking coffee".


== See also ==
Cuisine of Hong Kong
Hong Kong-style Western cuisine
Milk tea
Teh tarik


== References ==


== External links ==
Hong Kong Tea Company
Hong Kong Tea Company How to Brew Guide
Interview about HK Style Milk Tea and Yuanyang
Recipe of Hong Kong Style Milk Tea, Teas.com.au
Step by Step Guide on How to make Hong Kong Style Milk Tea
(Chinese) How to make "silk stocking" milk tea, Apple Daily
(Chinese) The origin of "silk stocking" milk tea, Apple Daily
Association of Coffee & Tea Hong Kong
Black and White Cow – Filled Milk