Teh halia (or ginger tea in English) is a tea beverage that is commonly consumed in Malaysia and Singapore. It is prepared like English tea with milk although it contains the added ingredient of ginger. Another notable feature of the beverage is that it is 'pulled' in the same style as teh tarik preparation.
Teh halia may also refer to an Indonesian drink consisting of brown sugar and ginger by the same name.
How to make Teh halia:
Ingredients: 2 cups of water 2-inch knob of fresh ginger, sliced 1/4 inch thin (for stronger taste, grate ginger and squeeze juice for use) 2 teaspoons of tea leaves (darjeeling or other) 1/4 cup of brown sugar warm milk or cream
Preparation: Bring water and ginger to a boil. Reduce heat and simmer on low heat for 10 minutes. Remove from heat and add tea leaves. Steep for 3 minutes. Strain and pour into teapot with brown sugar. Stir gently. To serve, add warm milk or cream to each teacup, about a third of the way up. Gently pour tea into the teacups to fill. Serve with an additional brown sugar and warm milk or cream.


== References ==