Teh talua or teh telor is a tea beverage from West Sumatra. The tea is unique because of the use of egg yolk in its preparation. Chicken or duck egg can be chosen for the tea. Other ingredients, in addition to tea and egg yolk, are sugar and calamondin.
A traditional method of preparing this drink is: stir the egg yolk and 2 spoons of sugar in a glass, until a batter has developed. Pour 1 spoon of boiling water onto the tea powder. Pour the tea into the batter and stir. Add calamondin according to taste.


== See also ==

List of hot beverages
List of Indonesian beverages


== References ==