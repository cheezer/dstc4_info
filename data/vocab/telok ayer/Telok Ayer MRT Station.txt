Telok Ayer MRT Station (DT18) is the name for an underground Mass Rapid Transit train station on the Downtown Line Stage 1 in Singapore. It is a 10-minute walk from Raffles Place and is located under Cross Street at the junction with Telok Ayer Street.


== Art in Transit ==
The station features artwork located at walls, floors and columns of the station created by Lim Shing Ge titled, "Bulbous Abode". The artwork demonstrates a whimsical dreamscapes of large rocks depicting ancient monuments, reminiscing the background of Chinese immigrants of old.


== Station layout ==


== Exits ==
A : Cross Street (Bus Stop)
B : China Square Food Centre, Capital Square
C : PWC Building (main entrance)


== Transport connections ==


== Gallery ==


== References ==


== External links ==
Official website