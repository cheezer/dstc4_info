Telok Ayer is a historic district located in Singapore's Chinatown within the Central Business District, straddling the Outram Planning Area and the Downtown Core under the Urban Redevelopment Authority's urban planning zones.


== Etymology ==
Telok Ayer and its namesake road, Telok Ayer Street, was named after Telok Ayer Bay, located at the foot of Mount Wallich. The Malay name refers to "bay water" because Telok Ayer Street was the coastal road along the bay.
Telok Ayer Street appears in George Drumgoole Coleman's 1836 Map of Singapore as Teluk Ayer Street. The Chinese name for Telok Ayer Street refers to the Chinese temple on this street — da bo gong miao jie.
Telok Ayer Street was also known colloquially under two names. The area nearer Merchant Street was called Guan Soon Street because there was a firm located here called Chop Guan Soon that brought in Indian labourers. Guan Sun is also the name of one of the five divisions of Hoklos (Hokkien) which took part in the Chingay procession once every three years.
The other colloquial name for Telok Ayer Street is called the "front street" of Mah Cho Temple in Hokkien because the street is in front of the Thian Hock Keng Temple, dedicated to Goddess of the Sea, Mah Cho or Matsu.


== History ==

In 1822, Telok Ayer was the primary area set aside by Sir Stamford Raffles for the Chinese community. As the main landing site for Chinese immigrants, Telok Ayer Street become one of the first streets in Chinatown and formed the backbone of development of the Chinese immigrant community in early Singapore. Thus, the Telok Ayer district was the original focal point of settlement in Chinatown.
Until the late nineteenth century, Telok Ayer Street was the main commercial and residential thoroughfare in Singapore. As immigration from China increased, so did the adverse qualities usually associated with a highly concentrated population. Between the 1850s and the 1870s, the road was the centre of the notorious Chinese slave trade. By the turn of the century, the area had become polluted and congested, and these factors, as well as the increasing afflucence of some of the merchants, were possibly the main reasons which drove them out of town to look for a more salubrious environment.
Temples and mosques are plentiful in this area as they were built by Chinese and Muslim immigrants to show their gratitude for safe passage. The numerous religious and clan buildings on Telok Ayer Street testify to their importance in the past. These buildings include:
Thian Hock Keng Temple (1820s), the oldest Hokkien temple in Singapore, dedicated to Matsu, Goddess of the Sea, Queen of Heaven and Patron of Sailors,
Fuk Tak Chi Temple (1824), built jointly by the Hakkas and Cantonese, colloquially known as the mah cau toh peh kong, now restored as a museum,
Hock Teck Chi Temple (1824–1869),
Ying Fo Fui Kun Hakka Association Hall (1882),
Ying Fo Fui Kun Temple (1823), the earliest building on the street built by the Hakkas,
Nagore Durgha Shrine (1828–1830),
Al-Abrar Mosque (circa 1827), and
Telok Ayer Chinese Methodist Church (1924).
In the past before land reclamation, boats used to moor in Telok Ayer Bay waiting to get fresh water, carried by bullock carts, from a well at Ann Siang Hill.
The street also was the founding site of one of Singapore's oldest schools, Gan Eng Seng School, which was started in 1885 at 106 Telok Ayer Street as the Anglo-Chinese Free School. The historical site marker of the school is nearby at the junction of Telok Ayer and Cecil Streets.
Today, the whole bay area in front of Telok Ayer Street lies on reclaimed land. In 1863, a group of local businessmen including Whampoa, applied to the Governor Orfeur Cavenagh for permission to build, at their own expense, a pier and a seawall to reclaim land to build warehouses. However, the request was not acceded, and the Hokkien temple of Thian Hock Keng was to remain fronting the sea until 1879.
Plans for reclamation were again put forward in 1865 and reclamation work carried out between 1878 and 1885. The government levelled the hills along the coast, including Mount Wallich, in order to reclaim land from Telok Ayer Bay. Robinson Road and Anson Road were subsequently built. The existing marshland was drained, the nutmeg plantations made way for maritime buildings and Thian Hock Keng Temple found itself five blocks away from the sea.


== Conservation and architecture ==
Telok Ayer has been gazetted under the government's conservation plan. When the conservation project was completed, some of the area's shophouses were restored to their original appearance. Many of these shophouses are two- and three-storey, mostly the result of the land division of the time which consisted of deep sites with narrow frontages. The frontages are based on the then available length of timber beams, usually about 4.8 metres.


== References ==
Victor R Savage, Brenda S A Yeoh (2003), Toponymics - A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1
Norman Edwards, Peter Keys (1996), Singapore - A Guide to Buildings, Streets, Places, Times Books International, ISBN 9971-65-231-5


== External links ==
Uniquely Singapore website