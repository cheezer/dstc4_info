Telok Blangah MRT Station (CC28) is an underground Mass Rapid Transit station on the Circle Line in Singapore.


== Art in Transit ==
The artwork featured under the Art in Transit programme is Notes Towards A Museum Of Cooking Pot Bay by Michael Lee. Located on the lift shaft in the station, the artwork intertwines monuments, pop culture elements, real-life people (including the artist himself) and fantastical elements in a massive concept map which, as the title suggests, aims to contribute to a museum of "Cooking Pot Bay", which is a translation of "Telok Blangah".


== Station layout ==


== Exits ==
There is only one exit (A) at Telok Blangah MRT Station, to connect to Telok Blangah Drive.


== Transport connections ==


=== Rail ===


== References ==


== External links ==
Official website