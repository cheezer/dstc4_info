Teluk Kemang is a main town near Port Dickson, Negeri Sembilan, Malaysia.


== Notable events ==
2000 - Teluk Kemang Parliament by-elections


== References ==

Edmund Terence Gomez, Persatuan Sains Sosial Malaysia (2004). The state of Malaysia: ethnicity, equity and reform. Routledge. p. 258. ISBN 0-415-33357-1.