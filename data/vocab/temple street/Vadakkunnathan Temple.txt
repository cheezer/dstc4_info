Vadakkunnathan Temple (Malayalam: വടക്കുന്നാഥൻ ക്ഷേത്രം) is an ancient Hindu temple dedicated to Shiva at city of Thrissur, of Kerala state in India. This temple is a classic example of the architectural style of Kerala and has monumental towers on all four sides and also a kuttambalam. Mural paintings depicting various episodes from Mahabharata can be seen inside the temple. The shrines and the Kuttambalam display vignettes carved in wood. The temple, along with the mural paintings, has been declared as a National Monument by India under the AMASR Act. According to popular local lore, this is the first temple built by Parasurama, the sixth incarnation of Vishnu. Tekkinkadu Ground, encircling the Vadakkunnathan Temple, is the main venue of the Thrissur Pooram. Non-Hindus are not allowed to enter into the temple.
In the year 2012 Archaeological Survey of India (ASI) has recommended 14 sites, including Vadakkumnathan Temple and palaces, from Kerala to include in the list of UNESCO World Heritage Sites.
The temple is one of the major Shiva temples in Kerala counted along with the Ettumanoor Mahadevar Temple, Kaduthruthy Mahadeva Temple, Vaikom Temple, Ernakulam Shiva Temple and Chengannur Mahadeva Temple.


== Legend ==
The story of the origin of the Vadakkunnathan Temple is briefly narrated in Brahmanda Purana and there are references to it in some other works also. Though there are differences between these accounts on the details, all agree on the central fact, namely, that the temple was founded by Parashurama. Parashurama exterminated Kshatriyas twenty one times. In order to expiate the sin, he performed a yajna at the end of which he gave away all the land to Brahmins as dakshina. He wanted to retire to some new land to do tapasya and so he requested the sea God Varuna to throw up a new piece of land from the sea.

According to another version, some sages approached him at the end of the yajna and requested him to give them some secluded land. Parashurama then made the request to Varuna for their sake. Varuna gave him a winnow (surpa) and asked him to hurl it into the sea, as he did a large territory of land was at once thrown up by the sea; this territory that rose out of the sea was Kerala. It was then known by the name "Surparaka", from the word, "Surpa" meaning winnow.
According to some other accounts, Varuna asked Parashurama to hurl his axe into the sea. Parashurama now wanted to consecrate this new land. So he went to Mount Kailash to his guru, the God Shiva and requested him to take abode in Kerala and thereby bless the region. Shiva accompanied by his wife Parvati, his sons Ganesha and Subrahmanya and his parashadas went along with Parashurama, to satisfy the desire of his disciple. Shiva stopped at a spot, now Thrissur, for his seat and later he and his party disappeared and Parashurama saw a bright and radiant Shiva linga (aniconic symbol of Shiva) at the foot of a huge banyan tree. This place where Shiva first manifested his presence through the linga came to be called the Sri Mula Sthana.
For sometime, the linga remained at Sri Mula Sthana at the foot of a huge banyan tree. The ruler of Cochin Kingdom then decided to shift the linga to a more convenient place and enclose it in a temple. Arrangements were soon made to reinstall the idol in the new place. But there was an initial difficulty. The linga could not be removed without cutting off a large part of the banyan tree. While cutting the branches of the tree, there was the danger of a piece of it falling on the idol and damaging it. When the ruler and the others did not know what to do, the Yogatirippadu came forward with a solution. He lay over the idol so as to cover it completely and asked the men to cut the tree. The cutting began and to the wonder of all not a piece of the tree fell anywhere near the idol. The idol was removed with all due rituals and installed in the new place where it has remained till now. Then a temple was built according to the rules laid down in the Shastras. 


== History ==

The temple was built at the time of Perumthachan from Parayi petta panthirukulam. It is said that Perumthachan lived during the seventh century; so the Koothambalam may be 1,300 years old. According to Malayalam historian VVK Valath, the temple was a pre-Dravidian Kavu (shrine). Later, the temple was influenced by Buddhism, Jainism and Vaishnavism. In the early days, Paramekkavu Bhagavathi was also inside the Vadakkunnathan temple. But Koodalmanikyam Temple, Kodungallur Bhagavathy Temple and Ammathiruvadi Temple, Urakam is older than Vadakkunnathan temple, according to temple documents.


=== Yogiatiripppads ===
The Nambudiris who were looking after the temple affairs were called as Yogiatiripppads. When Kerala Nambudiris gained control, the temple also fell into their hands. The Yogiatiripppads were elected from Thrissur desam. Prior to Sakthan Thampuran's reign, the Yogiatiripppad system declined. Later, the Maharaja of Cochin gained presiding authority over the temple.


=== Adi Shankara ===
Adi Shankara is believed to have been born to Shivaguru and Aryamba of Kalady in answer to their prayers before Vadakkunnathan, as amsavatara of Shiva. The couple had observed bhajan for 41 days in the temple. Legend has it that Shiva appeared to both husband and wife in their dreams and offered them a choice. They could have either a mediocre son who would live a long life or an extraordinary son who would die early. Both Shivaguru and Aryamba chose the second option. In honour of Shiva, they named the son Shankara. According to legend, Adi Shankara attained videha mukti ("freedom from embodiment") in the Vadakkunnathan temple. One tradition, expounded by Keraliya Shankaravijaya, places his place of death as the temple. He also established four Mutts at Thrissur, famously known as Edayil Madhom, Naduvil Madhom, Thekke Madhom and Vadakke Madhom


=== Invasion of Tipu Sultan ===
During the invasion of Tipu Sultan, the temple was not attacked by Tipu’s Army. Even though Tipu Sultan destroyed many temples in Thrissur district at that time, he never touched Vadakkunnathan Temple. But recent studies in history suggest that this might not be true. The British rulers might have created this story to create animosity between Hindus and Muslims. Considering the fact that he had built temples in Srirangapuram, the story could well be false. According to historical accounts when Tipu Sultan was marching towards the Travancore lines locally known as Nedumkotta, he had a short stay at Thrissur from 14 to 29 December 1789. In order to feed his Army, he had borrowed cooking vessels from Vadukanthan Temple. On leaving Thrissur, he not only returned the vessels to the temple but presented it a large bronze lamp.


=== Zamorin of Calicut ===
During 1750 to 1762, the temple affairs were conducted by Zamorin of Calicut who attacked Thrissur and took control of the temple and the city. In 1762 with the help of Kingdom of Travancore, Maharaja of Cochin regained control over Thrissur and the temple.


=== Sakthan Thampuran ===
When Sakthan Thampuran (1751–1805), ascended the throne of Kingdom of Cochin, he changed the capital of Kingdom of Cochin from Thripunithura to Thrissur as the King had a personal relationship with Vadakkunnathan Temple. He later cleared the teak forest around the temple and introduced the famous Thrissur Pooram festival. The King’s personal interest in the temple also changed the fortune of the city.


== Structure ==
The temple is situated in an elevated hillock in the centre of Thrissur and is surrounded by a massive stone wall enclosing an area of nearly 9 acres (36,000 m2). Inside this fortification, there are four gopuras facing four cardinal directions. Between the inner temple and the outer walls, there is a spacious compound, the entrance to which is through gopuras. Of these, the gopuras on the south and north are not open to the public. The public enter either through the east or west gopura. The inner temple is separated from the outer temple by a broad circular granite wall enclosing a broad corridor called Chuttambalam. Entrance into the inner temple is through a passage through the corridor.


== Deities ==

.
Shiva is worshipped in the form of a huge lingam, which is covered under a mound of ghee, formed by the daily abhishekam (ablution) with ghee over the years. A devotee looking into the sanctum can now see only a 16-foot-high (4.9 m) mound of ghee embellished with thirteen cascading crescents of gold and three serpent hoods on top. According to traditional belief, this represents the snow-clad Mount Kailash, the abode of Shiva. This is the only temple where the lingam is not visible. It is said that the ghee offered here for centuries does not have any foul odour and it does not melt even during summer.
In the outer temple, there are shrines for Krishna (Gopala Krishna; Krishna as a cowherd), Shiva's bull vahana (vehicle) Nandikeswara, Parashurama, Simhodara, Ayyappa (Shiva's son, especially venerated in Kerala), Vettekkaran (Shiva as a hunter) and Adi Shankara. Located on the verandah of the Nalambalam is a large white bull Nandikeswara. In the northern side, there is a circular structure with Shiva facing west and behind him, Parvati facing east, denoting their combined form Ardhanarishvara. The two-storied rectangular shrine of the god Rama facing west is located in the south. Between these two sanctums (srikovils) stands a third one, circular and double-storied in shape, which is dedicated to Shankaranarayana, the combined form of Shiva and Vishnu, facing west. There are mukhamandapams (halls) in front of all the three central shrines. The two important murals in the temple, Vasukisayana and Nrithanatha (Nataraja), are worshipped regularly. Ganesha shrine is positioned facing the temple kitchen. The offering of Appam (sweetened rice cake fried in ghee) to him is one of the most important offerings at the temple. Propitiating him here is believed to be a path to prosperity and wealth.


== Architecture ==


=== Murals ===
The temple is famous for the rarity of the temple murals, of which the Vasukishayana and Nrithanatha murals are of great importance and are worshipped daily. The temple also houses a museum of ancient wall paintings, wood carvings and art pieces of ancient times. A study done by Archaeological Survey of India on two paintings in the temple has revealed that it is 350 years old. These two rare paintings were a reclining Shiva and a Nataraja with 20 arms.


=== Koothambalam ===
The temple theatre, known as Koothambalam, has four magnificent gateways called Gopurams and the lofty masonry wall around the temple quadrangle are imposing pieces of craftsmanship and skill. The Koothambalam is used for staging Koothu, Nangyar Koothu and Koodiyattam, an ancient ritualistic art forms of Central Kerala. According to folk lore, before the new Koothambalam was built, there used to be an old and dilapidated structure. The then Diwan T. Sankunni Menon ordered to demolish the structure and construct a new Koothambalam. He gave this task to Velanezhy Nambudiri, a famous Thachushasthranjan or master craftsman. He prepared a mental sketch and built a beautiful Koothambalam there. Velanezhy Illom is in Venganellur Gramam, Chelakkara town.


== Festivals ==


=== Maha Shivaratri ===
Maha Shivaratri is the main festival which is celebrated in the temple. Cultural and musical programmes are held in the temple premises. Around one lakh temple lamps (hundred thousand)are lighted in the festival. The idol of Vadakkumnatha is not taken out for procession.

.


=== Aanayoottu ===
The Aanayoottu of feeding of elephants, is the second biggest festival held in the temple. The devotees refer to elephants as Lord Ganesh's incarnation. The festival falls on the first day of the month of Karkkidakam (timed against the Malayalam calendar), which coincides with the month of July. It has been the regular annual practice at the temple for the last 20 years to conduct a large-scale Ashta Dravya Maha Ganapathy Havana and Aanayoottu on the first day of the Karkidakom month of the Malayalam calendar. It involves a number of unadorned elephants being positioned amid a multitude of people for being worshipped and fed. A large number of people throng the temple to feed the elephants. Gajapooja also is conducted once every four years.


=== Thrissur Pooram ===
One of the most colourful temple festivals of Kerala, Thrissur Pooram is conducted in the temple premises but the temple is not a participant in this festival. There is no special pooja or special offering during the pooram day. The main attraction of the Pooram is the Elanjitharamelam, a two-hour Chendavadyam (with five instruments) is held near Koothambalam in the temple, by the top most artists from the state.


== Names ==
Shiva here is more popularly known as Vadakkunnathan (Sanskrit Vrishabhachala -Tamil Vidaikunrunathan Vidai - Vrishabha, kunru - chala ).
The Sanskrit Vrishabhachala refers to Nandi represented as a mound or hill. The town of Thrissur thus also derives a name Vrishabhadripuram.
The Malayalam Vadakkunnathan literally translated, means The Lord of the North, who resides in Kailasam (The Himalayas) which is in the northern side of India.


== Temple timings ==
The temple opens daily at 03:00 AM, closes at 10:30 AM, reopens at 05:00 PM and closes at 8.30 PM after 'Trippuka', the last rite of the day.


== Order for Praying in the Temple ==
Traditionally, the devotees follow a special order for praying in the inner and outer sanctums of the temple.


== Order of visiting in the outer sanctum ==
1. Sreemoolasthanam
2. Goshalakrishnan
3. Nandikeshwara
4. Parashurama
5. Simhodara
6. Kashivishwanatha- to be viewed from the altar placed a little to the north of Simhodara
7. Sambhukumbham
8. Pray to Chidambaranadha at the small platform at the south-east corner.
9. Pray to Sethunadha of Rameshwaram looking Eastwards.
10. From the south gopura pray to Kodungallur Bhagavathi.
11. From the platform ath the south- west corner look south and pray to Sangameshwara of Koodalmanikyam
12. From the same platform pray to Oorakathamma Thiruvadi
13. Look at the domes/ thazhikakkudam of Vadakkunnadhan, Shankaranarayanan and Sri Rama and pray
14. The one comes upon a flat granite slab called Vyasashila on a platform on which one must write 'Hari Sree Ganapathaye Namah'
15. Ayyappa or Shastha
16. Samadhi of Adi Shankaracharya marked by a Shankha and Chakra
17. Adi Shankara.
18. Vrishabha
19. Vasukishayanam painting. Also known as Phanivarashayana. One of the rarest murals in which Lord Shiva is depicted resembling Lord Vishnu's Ananthashayana form.
20. Nruthanadha painting


== The order of praying in the inner sanctum of the temple ==
1. Lord Vadakkunnadhan
2 Devi Sri Parvati
3. Lord Ganapathy
4. Lord Shankaranarayana
5. Lord Sri Rama
6. Lord Shankaranarayana
7. Lord Ganapathy
8. Devi Sri Parvati
9. Lord Vadakkunnadhan
10. Lord Ganapathy
11. Lord Shankaranarayana
12. Lord Sri Rama
13. Lord Shankaranarayana
14. Lord Sri Rama
15. Lord Shankaranarayana
16. Lord Ganapathy
17. Devi Sri Parvati
18. Lord Vadakkunnadhan


== See also ==
Temples of Kerala
Hindu temples in Thrissur Rural


== References ==