Fun guo, or Chaozhou fun guo sometimes spelled fun quor, fun gor, fen guo, Chiu Chow dumpling, Teochew dumpling, or fun kor is a variety of steamed dumpling from the Chaoshan area of coastal eastern Guangdong, a province in Southern China.


== Ingredients ==
They are typically filled with chopped peanuts, garlic chives, ground pork, dried shrimp, dried radish and shiitake mushrooms. Other filling ingredients may include cilantro, jicama, or dried daikon. The filling is wrapped in a thick dumpling wrapper made from a mixture of flours or plant starches mixed together with boiling water. Although the recipe for the wrapper dough may vary somewhat, it typically consists of de-glutenized wheat flour (澄面), tapioca flour (菱粉), and corn or potato starch (生粉). The dumplings are usually served with a small dish of chili oil.


== Teochew cuisine ==
In the Chaozhou dialect of Min Nan, the dumplings are called hung gue (粉餜), but they are more widely known by their Cantonese name. They are also eaten in non-Chaozhou regions of Guangdong.


== Hawaiian cuisine ==
In Hawaii, fun guo is known as pepeiao, the Hawaiian word for ear, named for its shape resembling an ear.


== See also ==
Chaozhou cuisine


== References ==