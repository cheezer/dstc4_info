Articles in English are the definite article the and the indefinite articles a and an. Use of the definite article implies that the speaker assumes the listener knows the identity of the noun's referent (because it is obvious, because it is common knowledge, or because it was mentioned in the same sentence or an earlier sentence). Use of an indefinite article implies that the speaker assumes the listener does not have to be told the identity of the referent. In some noun phrases no article is used.
Articles are a special case of determiners in English.


== Use of articles ==
The rules of English grammar require that in most cases a noun, or more generally a noun phrase, must be "completed" with a determiner to clarify what the referent of the noun phrase is. The most common determiners are the articles the and a(n), which specify the presence or absence of definiteness of the noun. Other possible determiners include words like this, my, each and many – see English determiners. There are also cases where no determiner is required, as in the sentence John likes fast cars. Or the sentence Bob likes cool trains.
The definite article the is used when the referent of the noun phrase is assumed to be unique or known from the context. For example, in the sentence The boy with glasses was looking at the moon, it is assumed that in the context the reference can only be to one boy and one moon. However, the definite article is not used:
with generic nouns (plural or uncountable): cars have accelerators, happiness is contagious, referring to cars in general and happiness in general (compare the happiness I felt yesterday, specifying particular happiness);
with many proper names: John, France, London, etc.
The indefinite article a (before a consonant sound) or an (before a vowel sound) is used only with singular, countable nouns. It indicates that the referent of the noun phrase is one unspecified member of a class. For example, the sentence An ugly man was smoking a pipe does not refer to any specifically known ugly man or pipe.
No article is used with plural or uncountable nouns when the referent is indefinite (just as in the generic definite case described above). However, in such situations, the determiner some is often added (or any in negative contexts and in many questions). For example:
There are apples in the kitchen or There are some apples in the kitchen;
We do not have information or We do not have any information;
Would you like tea? or Would you like some tea? and Would you like any tea? or Would you like some good tea?
Additionally, articles are not normally used:
in noun phrases that contain other determiners (my house, this cat, America's history), although one can combine articles with certain other determiners, as in the many issues, such a child (see English determiners: Combinations of determiners).
with pronouns (he, nobody), although again certain combinations are possible (as the one, the many, the few).
preceding noun phrases consisting of a clause or infinitive phrase (what you've done is very good, to surrender is to die).
If it is required to be concise, e.g. in headlines, signs, labels, and notes, articles are often omitted along with certain other function words. For example, rather than The mayor was attacked, a newspaper headline might say just Mayor attacked.
For more information on article usage, see the sections Definite article and Indefinite article below. For more cases where no article is used, see Zero article in English.


=== Word order ===
In most cases, the article is the first word of its noun phrase, preceding all other adjectives and modifiers.
The little old red bag held a very big surprise.
There are a few exceptions, however:
Certain determiners, such as all, both, half, double, precede the definite article when used in combination (all the team, both the girls, half the time, double the amount).
The determiner such and exclamative what precede the indefinite article (such an idiot, what a day!).
Adjectives qualified by too, so, as and how generally precede the indefinite article: too great a loss, so hard a problem, as delicious an apple as I have ever tasted, I know how pretty a girl she is.
When adjectives are qualified by quite (particularly when it means "fairly"), the word quite (but not the adjective itself) often precedes the indefinite article: quite a long letter.
See also English determiners: Combinations of determiners and Determiners and adjectives.


== Definite article ==

The only definite article in English is the word the, denoting person(s) or thing(s) already mentioned, under discussion, implied, or otherwise presumed familiar to the listener or reader. The is the most commonly used word in the English language, accounting for 7% of all words.
"The" can be used with both singular and plural nouns, with nouns of any gender, and with nouns that start with any letter. This is different from many other languages which have different articles for different genders and/or numbers.


=== Pronunciation ===
In most dialects "the" is pronounced as /ðə/ (with the voiced dental fricative /ð/ followed by schwa) when followed by a consonant sound. In many dialects, including Received Pronunciation, the pronunciation [ði] is used before words that begin with vowel sounds. The emphatic form of the word is /ðiː/ (like thee) – see Weak and strong forms in English.
In some Northern England dialects of English, the is pronounced [t̪ə] (with a dental t) or as a glottal stop, usually written in eye dialect as ⟨t⟩; in some dialects it reduces to nothing. This is known as definite article reduction. In dialects that do not have the voiced dental fricative /ð/, the is pronounced with the voiced dental plosive, as in /d̪ə/ or /d̪iː/).


=== Etymology ===
The and that are common developments from the same Old English system. Old English had a definite article se, in the masculine gender, seo (feminine), and þæt (neuter). In Middle English these had all merged into þe, the ancestor of the Modern English word the.


=== Usage ===
The principles of the use of the definite article in English are described above under Use of articles. (The word the is also used with comparatives, in phrases like, the sooner the better, and, we were all the happier for it; this form of the definite article has a somewhat different etymology from other uses of the definite article. (See the Wiktionary entry the.)


==== Geographical names ====
An area in which the use or non-use of the is sometimes problematic is with geographic names. Names of rivers, seas, mountain ranges, deserts, island groups and the like are generally used with the definite article (the Rhine, the North Sea, the Alps, the Sahara, the Hebrides). Names of continents, islands, countries, regions, administrative units, cities and towns mostly do not take the article (Europe, Skye, Germany, Scandinavia, Yorkshire, Madrid). However, there are certain exceptions:
Countries and territories whose names derive from common nouns such as "kingdom", "republic" or even "coast" take the article: the United States, the United Kingdom, the Soviet Union, the Czech Republic, the Ivory Coast.
Countries and territories whose names derive from "island" or "land" however only take the definite article if they represent a plural noun: The Netherlands do, the Falkland Islands, the Faroe Islands and the Cayman Islands do, even the Philippines or the Comoros do, though the plural noun "Islands" is omitted there. The (singular) Greenland on the other hand doesn't take the definite article, neither does Christmas Island or Norfolk Island.
Certain countries and regions whose names derive from mountain ranges, rivers, deserts, etc. are sometimes used with an article even though in the singular (the Lebanon, the Sudan, the Yukon), but this usage is declining, although The Gambia remains the recommended name of that country. Since the independence of Ukraine (formerly sometimes called the Ukraine), most style guides have advised dropping the article (in some other languages there is a similar issue involving prepositions). Use of the Argentine for Argentina is now old-fashioned.
Some names include an article for historical reasons, such as The Bronx, or to reproduce the native name (The Hague).
Names beginning with a common noun followed by of take the article, as in the Isle of Wight or the Isle of Portland (compare Christmas Island). The same applies to names of institutions: Cambridge University, but the University of Cambridge.


== Abbreviations for "the" and "that" ==

Since "the" is one of the most frequently used words in English, at various times short abbreviations for it have been found:
Barred thorn:  the earliest abbreviation, it is used in manuscripts in the Old English language. It is the letter þ with a bold horizontal stroke through the ascender, and it represents the word þæt, meaning "the" or "that" (neuter nom. / acc.)
þͤ and þͭ  (þ with a superscript e or t) appear in Middle English manuscripts for "þe" and "þat" respectively.
yͤ and yͭ  are developed from þͤ and þͭ and appear in Early Modern manuscripts and in print (see Ye form below).
Occasional proposals have been made by individuals for an abbreviation. In 1916, Legros & Grant included in their classic printers' handbook Typographical Printing-Surfaces, a proposal for a letter similar to Ħ to represent "Th", thus abbreviating "the" to ħe. Why they did not propose reintroducing to the English language "þ", for which blocks were already available for use in Icelandic texts, or the yͤ form is unknown.
In 2013 an Australian restaurateur named Paul Mathis proposed Ћ, which he nicknamed "The Tap", as a symbol for "the." This symbol is identical to the Serbian Cyrillic letter Ћ (Tshe).


=== Ye form ===

In Middle English, the (þe) was frequently abbreviated as a þ with a small e above it, similar to the abbreviation for that, which was a þ with a small t above it. During the latter Middle English and Early Modern English periods, the letter thorn (þ) in its common script, or cursive, form came to resemble a y shape. As a result, the use of a y with an e above it () as an abbreviation became common. This can still be seen in reprints of the 1611 edition of the King James Version of the Bible in places such as Romans 15:29, or in the Mayflower Compact. Historically the article was never pronounced with a y sound, even when so written.


== Indefinite article ==
The indefinite article of English takes the two forms a and an. Semantically they can be regarded as meaning "one", usually without emphasis. They can be used only with singular countable nouns; for the possible use of some (or any) as an equivalent with plural and uncountable nouns, see Use of some below.


=== Distinction between a and an ===
The form an is used before words starting with a vowel sound, regardless of whether the word begins with a vowel letter. This avoids the glottal stop (momentary silent pause) that would otherwise be required between a and a following vowel sound. Where the next word begins with a consonant sound, a is used. Examples: a box; an apple; an SSO (pronounced "es-es-oh"); a HEPA filter (HEPA is pronounced as a word rather than as letters); an hour (the h is silent); a one-armed bandit (pronounced "won..."); an heir (pronounced "air"); a unicorn (pronounced "yoo-"); an herb in American English (where the h is silent), but a herb in British English.
Some speakers and writers use an before a word beginning with the sound /h/ in an unstressed syllable: an historical novel, an hotel. However, where the "h" is clearly pronounced, this usage is now less common, and "a" is preferred.
Some dialects, particularly in England (such as Cockney), silence many or all initial h sounds (h-dropping), and so employ an in situations where it would not be used in the standard language, like an 'elmet (standard English: a helmet).
There used to be a distinction analogous to that between a and an for the possessive determiners my and thy, which became mine and thine before a vowel, as in mine eyes.


==== In other languages ====
Other more or less analogous cases in different languages include the Yiddish articles "a" (אַ) and "an" (אַן) (used in essentially the same manner as the English ones), the Hungarian articles a and az (used the same way, except that they are definite articles; juncture loss, as described below, has occurred in that language too), and the privative a- and an- prefixes, meaning "not" or "without", in Greek and Sanskrit.


=== Pronunciation ===
Both a and an are usually pronounced with a schwa: /ə/, /ən/. However, when stressed (which is rare in ordinary speech), they are normally pronounced respectively as /eɪ/ (to rhyme with day) and /æn/ (to rhyme with pan). See Weak and strong forms in English.


=== Etymology ===
An is the older form (related to one, cognate to German ein; etc.). An was originally an unstressed form of the number ān 'one'.


=== Usage ===
The principles for use of the indefinite article are given above under Use of articles.
In addition to serving as an article, a and an are also used to express a proportional relationship, such as "a dollar a day" or "$150 an ounce" or "A sweet a day helps you work, rest and play", although historically this use of "a" and "an" does not come from the same word as the articles.


=== Juncture loss ===
In a process called juncture loss, the n has wandered back and forth between the indefinite article and words beginning with vowels over the history of the language, where for example what was once a nuncle is now an uncle. The Oxford English Dictionary gives such examples as smot hym on the hede with a nege tool from 1448 for smote him on the head with an edge tool, as well as a nox for an ox and a napple for an apple. Sometimes the change has been permanent. For example, a newt was once an ewt (earlier euft and eft), a nickname was once an eke-name, where eke means "extra" (as in eke out meaning "add to"), and in the other direction, a napron (meaning a little tablecloth, related to the word napkin) became an apron, and a naddre became an adder. The initial n in orange was also dropped through juncture loss, but this happened before the word was borrowed into English.


== Use of some ==
The existential determinative (or determiner) some is sometimes used as a functional equivalent of a(n) with plural and uncountable nouns (also called a partitive). For example, Give me some apples, Give me some water (equivalent to the singular countable forms an apple and a glass of water). Grammatically this some is not required; it is also possible to use zero article: Give me apples, Give me water. The use of some in such cases implies a more limited quantity. (Compare the forms unos/unas in Spanish, which are the plural of the indefinite article un/una.) Like the articles, some belongs to the class of "central determiners", which are mutually exclusive (so "the some boys" is ungrammatical).
In most negative clauses, and often in questions, the word any is used instead of some: Don't give me any apples; Is there any water?
Some can also have a more emphatic meaning: "some but not others" or "some but not many". For example, some people like football, while others prefer rugby, or I've got some money, but not enough to lend you any. It can also be used as an indefinite pronoun, not qualifying a noun at all (Give me some!) or followed by a prepositional phrase (I want some of your vodka); the same applies to any.
Some can also be used with singular countable nouns, as in There is some person on the porch, which implies that the identity of the person is unknown to the speaker (which is not necessarily the case when a(n) is used). This usage is fairly informal, although singular countable some can also be found in formal contexts: We seek some value of x such that...
When some is used with merely the function of an indefinite article, it is normally pronounced weakly, as [s(ə)m]. In other meanings it is pronounced [sʌm]. See Weak and strong forms in English.


== Effect on alphabetical order ==
In sorting titles and phrases alphabetically, articles are usually excluded from consideration, since being so common makes them more of a hindrance than a help in finding a desired item. For example, The Comedy of Errors is alphabetized before A Midsummer Night's Dream, because the and a are ignored and comedy alphabetizes before midsummer. In an index, the former work might be written "Comedy of Errors, The", with the article moved to the end.


== See also ==
False title


== References ==


== External links ==
Vietnamese learners mastering english articles
"The Definite Article: Acknowledging 'The' in Index Entries", Glenda Browne, The Indexer, vol. 22, no. 3 April 2001, pp. 119–22.
Low MH 2005: "The Phenomenon of the Word THE in English — discourse functions and distribution patterns" — a dissertation that surveys the use of the word 'the' in English text.
When Do You Use Articles: A, An, The
Etymology of the word THE on the Online Etymology Dictionary
Mastering A, An, The: English Articles Solved