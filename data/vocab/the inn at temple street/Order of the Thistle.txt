The Most Ancient and Most Noble Order of the Thistle is an order of chivalry associated with Scotland. The current version of the Order was founded in 1687 by King James VII of Scotland (James II of England and Ireland) who asserted that he was reviving an earlier Order. The Order consists of the Sovereign and sixteen Knights and Ladies, as well as certain "extra" knights (members of the British Royal Family and foreign monarchs). The Sovereign alone grants membership of the Order; he or she is not advised by the Government, as occurs with most other Orders.
The Order's primary emblem is the thistle, the national flower of Scotland. The motto is Nemo me impune lacessit (Latin for "No one provokes me with impunity"). The same motto appears on the Royal Coat of Arms of the United Kingdom for use in Scotland and some pound coins, and is also the motto of the Royal Regiment of Scotland, Scots Guards, The Black Watch (Royal Highland Regiment) of Canada and Royal Scots Dragoon Guards. The patron saint of the Order is St Andrew.
Most British orders of chivalry cover the whole United Kingdom, but the three most exalted ones each pertain to one constituent country only. The Order of the Thistle, which pertains to Scotland, is the second-most senior in precedence. Its equivalent in England, The Most Noble Order of the Garter, is the oldest documented order of chivalry in the United Kingdom, dating to the middle fourteenth century. In 1783 an Irish equivalent, The Most Illustrious Order of St Patrick, was founded, but has now fallen dormant.


== History ==

James VII claimed that he was reviving an earlier Order, but this issue is marked by widely varying claims.
According to legend, Achaius, King of Scots (possibly coming to the aid of Óengus mac Fergusa, King of the Picts), while engaged in battle at Athelstaneford with the Saxon King Æthelstan of East Anglia, saw in the heavens the cross of St Andrew. After he won the battle, Achaius is said to have established the Order of the Thistle, dedicating it to the saint, in 786. The tale is not credible, because the two individuals purported to have fought each other did not even live in the same century. Another story states that Achaius founded the Order in 809 to commemorate an alliance with the Emperor Charlemagne. There is some credibility to this story given the fact that Charlemagne did employ Scottish bodyguards. There is, in addition, a tradition that the order was instituted, or re-instituted, on the battlefield by Robert the Bruce at Bannockburn.
The earliest claim now taken seriously by historians is that James III, who adopted the thistle as the royal plant badge and issued coins depicting thistles, founded the Order during the fifteenth century. He allegedly conferred membership of the "Order of the Burr or Thissil" on King Francis I of France.
However there is no conclusive evidence for a fifteenth-century order. A French commentator writing in 1558 described the use of the crowned thistle and the cross of St Andrew on Scottish coins and war banners, and added that there was no Scottish order of knighthood. Similarly, John Lesley writing around 1578, refers to the three foreign orders of chivalry carved on the gate of James V's Linlithgow Palace with his ornaments of St Andrew, proper to this nation. Some Scottish order of chivalry may have existed during the sixteenth century, possibly founded by James V and called the Order of St. Andrew, but lapsed by the end of that century.
James VII issued letters patent "reviving and restoring the Order of the Thistle to its full glory, lustre and magnificency" on 29 May 1687. Although the "restoration" in 1687 of the Most Ancient and Most Noble Order of the Thistle was accomplished by King James VII & II, the initiative for - essentially - founding this Scottish Royal Order can be attributed to John, 1st Earl and 1st Jacobite Duke of Melfort, then Secretary of State for Scotland, who together with his elder brother James, 4th Earl and 1st Jacobite Duke of Perth, then Lord Chancellor of Scotland, were among the eight Founding Knights. Eight knights, out of a maximum of twelve, were appointed, but the King was deposed in 1688. His successors, the joint monarchs William and Mary, did not make any further appointments to the Order, which consequently fell into desuetude. In 1703, however, Anne once again revived the Order of the Thistle, which survives to this day.


=== Knights founder (restored order) ===
James, Earl of Perth
George, Duke of Gordon
John, Marquis of Atholl
James, Earl of Arran
Kenneth, Earl of Seaforth
John, Earl of Melfort
George, Earl of Dumbarton
Alexander, Earl of Moray


== Composition ==

The Kings of Scots—later the Kings of Great Britain and of the United Kingdom—have served as Sovereigns of the Order. When James VII revived the Order, the statutes stated that the Order would continue the ancient number of Knights, which was described in the preceding warrant as "the Sovereign and twelve Knights-Brethren in allusion to the Blessed Saviour and his Twelve Apostles". In 1827, George IV augmented the Order to sixteen members. Women (other than Queens regnant) were originally excluded from the Order; George VI created his wife Elizabeth Bowes-Lyon a Lady of the Thistle in 1937 via a special statute, and in 1987 Elizabeth II allowed the regular admission of women to both the Order of the Thistle and the Order of the Garter.
From time to time, individuals may be admitted to the Order by special statutes. Such members are known as "Extra Knights" and do not count towards the sixteen-member limit. Members of the British Royal Family are normally admitted through this procedure; the first to be so admitted was Prince Albert. King Olav V of Norway, the first foreigner to be admitted to the Order, was also admitted by special statute in 1962.
The Sovereign has historically had the power to choose Knights of the Order. From the eighteenth century onwards, the Sovereign made his or her choices upon the advice of the Government. George VI felt that the Orders of the Garter and the Thistle had been used only for political patronage, rather than to reward actual merit. Therefore, with the agreement of the Prime Minister (Clement Attlee) and the Leader of the Opposition (Winston Churchill) in 1946, both Orders returned to the personal gift of the Sovereign.

Knights and Ladies of the Thistle may also be admitted to the Order of the Garter. Formerly, many, but not all, Knights elevated to the senior Order would resign from the Order of the Thistle. The first to resign from the Order of the Thistle was John, Duke of Argyll in 1710; the last to take such an action was Thomas, Earl of Zetland in 1872. Knights and Ladies of the Thistle may also be deprived of their knighthoods. The only individual to have suffered such a fate was John Erskine, 6th Earl of Mar who lost both the knighthood and the earldom after participating in the Jacobite rising of 1715.
The Order has five officers: the Dean, the Chancellor, the Usher, the Lord Lyon King of Arms and the Secretary. The Dean is normally a cleric of the Church of Scotland. This office was not part of the original establishment, but was created in 1763 and joined to the office of Dean of the Chapel Royal. The two offices were separated in 1969. The office of Chancellor is mentioned and given custody of the seal of the Order in the 1687 statutes, but no-one was appointed to the position until 1913. The office has subsequently been held by one of the knights, though not necessarily the most senior. The Usher of the Order is the Gentleman Usher of the Green Rod (unlike his Garter equivalent, the Gentleman Usher of the Black Rod, he does not have another function assisting the House of Lords). The Lord Lyon King of Arms, head of the Scottish heraldic establishment and whose office predates his association with the Order serves as King of Arms of the Order. The Lord Lyon often—but not invariably—also serves as the Secretary.


== Habit and insignia ==

For the Order's great occasions, such as its annual service each June or July, as well for coronations, the Knights and Ladies wear an elaborate costume:
The mantle is a green robe worn over their suits or military uniforms. The mantle is lined with white taffeta; it is tied with green and gold tassels. On the left shoulder of the mantle, the star of the Order (see below) is depicted.
The hat is made of black velvet and is plumed with white feathers with a black egret or heron's top in the middle.
The collar is made of gold and depicts thistles and sprigs of rue. It is worn over the mantle.
The St Andrew, also called the badge-appendant, is worn suspended from the collar. It comprises a gold enamelled depiction of St Andrew, wearing a green gown and purple coat, holding a white saltire. Gold rays of a glory are shown emanating from St Andrew's head.
Aside from these special occasions, however, much simpler insignia are used whenever a member of the Order attends an event at which decorations are worn.
The star of the Order consists of a silver St Andrew's saltire, with clusters of rays between the arms thereof. In the centre is depicted a green circle bearing the motto of the Order in gold majuscules; within the circle, there is depicted a thistle on a gold field. It is worn pinned to the left breast. (Since the Order of the Thistle is the second-most senior chivalric order in the UK, a member will wear its star above that of other orders to which he or she belongs, except that of the Order of the Garter; up to four orders' stars may be worn.)
The broad riband is a dark green sash worn across the body, from the left shoulder to the right hip.
At the right hip of the Riband, the badge of the Order is attached. The badge depicts St Andrew in the same form as the badge-appendant surrounded by the Order's motto.
However, on certain collar days designated by the Sovereign, members attending formal events may wear the Order's collar over their military uniform, formal wear, or other costume. They will then substitute the broad riband of another order to which they belong (if any), since the Order of the Thistle is represented by the collar.
Upon the death of a Knight or Lady, the insignia must be returned to the Central Chancery of the Orders of Knighthood. The badge and star are returned personally to the Sovereign by the nearest relative of the deceased.
Officers of the Order also wear green robes. The Gentleman Usher of the Green Rod also bears, as the title of his office suggests, a green rod.
One unusual recipient of the Order of the Thistle was James, Earl of Southesk (1827-1905). He was recognized by the Order for his adventurous spirit and his passion for the wilds of Canada. His portrait in marble by William Grant Stevenson depicts a stern man who had placed himself at some risk as he travelled through the Canadian wilderness and wrote about his admiration for the native peoples of North America.


== Chapel ==

When James VII created the modern Order in 1687, he directed that the Abbey Church at the Palace of Holyroodhouse be converted to a Chapel for the Order of the Thistle, perhaps copying the idea from the Order of the Garter (whose chapel is located in Windsor Castle). James VII, however, was deposed by 1688; the Chapel, meanwhile, had been destroyed during riots. The Order did not have a Chapel until 1911, when one was added onto St Giles High Kirk in Edinburgh. Each year, the Sovereign resides at the Palace of Holyroodhouse for a week in June or July; during the visit, a service for the Order is held. Any new Knights or Ladies are installed at annual services.
Each member of the Order, including the Sovereign, is allotted a stall in the Chapel, above which his or her heraldic devices are displayed. Perched on the pinnacle of a knight's stall is his helm, decorated with mantling and topped by his crest. If he is a peer, the coronet appropriate to his rank is placed beneath the helm. Under the laws of heraldry, women, other than monarchs, do not normally bear helms nor crests; instead, the coronet alone is used (if she is a peeress or princess). Lady Marion Fraser had a helm and crest included when she was granted arms; these are displayed above her stall in the same manner as for knights. Unlike other British Orders, the armorial banners of Knights and Ladies of the Thistle are not hung in the chapel, but instead in an adjacent part of St Giles High Kirk. The Thistle Chapel does, however, bear the arms of members living and deceased on stall plates. These enamelled plates are affixed to the back of the stall and display its occupant's name, arms, and date of admission into the Order.
Upon the death of a Knight, helm, mantling, crest (or coronet or crown) and sword are taken down. The stall plates, however, are not removed; rather, they remain permanently affixed to the back of the stall, so that the stalls of the chapel are festooned with a colourful record of the Order's Knights (and now Ladies) since 1911. The entryway just outside the doors of the chapel has the names of the Order's Knights from before 1911 inscribed into the walls giving a complete record of the members of the order.


== Precedence and privileges ==

Knights and Ladies of the Thistle are assigned positions in the order of precedence, ranking above all others of knightly rank except the Order of the Garter, and above baronets. Wives, sons, daughters and daughters-in-law of Knights of the Thistle also feature on the order of precedence; relatives of Ladies of the Thistle, however, are not assigned any special precedence. (Generally, individuals can derive precedence from their fathers or husbands, but not from their mothers or wives.)
Knights of the Thistle prefix "Sir", and Ladies prefix "Lady", to their forenames. Wives of Knights may prefix "Lady" to their surnames, but no equivalent privilege exists for husbands of Ladies. Such forms are not used by peers and princes, except when the names of the former are written out in their fullest forms.
Knights and Ladies use the post-nominal letters "KT" and "LT" respectively. When an individual is entitled to use multiple post-nominal letters, "KT" or "LT" appears before all others, except "Bt" or "Btss" (Baronet or Baronetess), "VC" (Victoria Cross), "GC" (George Cross) and "KG" or "LG" (Knight or Lady of the Garter).
Knights and Ladies may encircle their arms with the circlet (a green circle bearing the Order's motto) and the collar of the Order; the former is shown either outside or on top of the latter. The badge is depicted suspended from the collar. The Royal Arms depict the collar and motto of the Order of the Thistle only in Scotland; they show the circlet and motto of the Garter in England, Wales and Northern Ireland.
Knights and Ladies are also entitled to receive heraldic supporters. This high privilege is shared only by members of the Royal Family, peers, Knights and Ladies of the Garter, and Knights and Dames Grand Cross of the junior orders of chivalry and clan chiefs.


== Current members and officers ==

Sovereign: Elizabeth II
Knights and Ladies Companion:
 Andrew, Earl of Elgin and Kincardine KT JP DL (1981)
 David, Earl of Airlie KT GCVO PC JP (1985)
 Robert, Earl of Crawford and Balcarres KT GCVO PC DL (1996)
 Lady Marion Fraser LT (1996)
 Norman, Lord Macfarlane of Bearsden KT DL (1996)
 James, Lord Mackay of Clashfern KT PC QC (1997)
 David, Lord Wilson of Tillyorn KT GCMG (2000)
 Stewart, Lord Sutherland of Houndwood KT (2002)
 Sir Eric Anderson KT (2002)
 David, Lord Steel of Aikwood KT KBE PC (2004)
 George, Lord Robertson of Port Ellen KT GCMG PC (2004)
 William, Lord Cullen of Whitekirk KT PC (2007)
 David, Lord Hope of Craighead KT PC QC (2009)
 Narendra, Lord Patel KT (2009)
 David, Earl of Home KT CVO CBE (2014)
 Robert, Lord Smith of Kelvin KT (2014)

Extra Knights and Ladies Companion:
 Prince Philip, Duke of Edinburgh KG KT OM GBE AK QSO PC ADC(P) (1952)
 Prince Charles, Duke of Rothesay KG KT GCB OM AK QSO PC ADC(P) (1977)
 Princess Anne, Princess Royal KG KT GCVO QSO (2000)
 Prince William, Earl of Strathearn KG KT ADC(P) (2012)

Officers:
Dean: Very Reverend Professor Iain Torrance, TD
Chancellor: David, Earl of Airlie KT GCVO PC JP
Gentleman Usher of the Green Rod: Rear Admiral Christopher Hope Layman CB DSO LVO
King of Arms: Dr Joseph Morrow (Lord Lyon King of Arms)
Secretary: Elizabeth Roads LVO (Snawdoun Herald, Lyon Clerk and Keeper of the Records)


== See also ==
List of Knights and Ladies of the Thistle (1687–present)
List of people who have declined a British honour


== Notes ==


== References ==


=== Printed ===


=== Web ===