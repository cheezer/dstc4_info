List of Ancient Greek theatres
Attica & Athens
Theatre of Dionysus, Athens
Odeon of Herodes Atticus, Athens
Theatre of Oropos, Oropos, East Attica
Theatre of Zea, Piraeus
Theatre of Thoricus, East Attica
Theatre of Aegina, Attica (region)
Theatre of Rhamnous, East Attica

Continental Greece & Euboea
Theatre of Chaeronea, Boeotia
Theatre of Orchomenos, Boeotia
Theatre of Delphi, Phocis
Theatre of Stratos, Aetolia-Acarnania
Theatre of Oiniades, Aetolia-Acarnania
Theatre of Eretria, Euboea
Theatre of Thebes, Thebes

Thessaly & Epirus
First Ancient Theatre, Larissa
Theatre B' of Larissa
Theatre of Dodona, Ioannina
Theatre of Ambracia, Arta
Theatre of Omolion, Larissa
Theatre of Demetrias, Volos
Theatre of Cassope, Preveza
Theatre of Gitanae, Thesprotia

Macedonia & Thrace
Theatre of Dion, Pieria
Theatre of Mieza, Imathia
Theatre of Amphipolis, Serres
Theatre of Abdera, Xanthi
Theatre of Vergina (Aigai), Imathia
Theatre of Olynthus, Chalcidice
Theatre of Philippi, Kavala
Theatre of Maroneia, Rhodope

Peloponnese
Theatre of Corinth, Corinthia
Theatre of Argos, Argolis
Theatre A' of Epidaurus, Argolis
Theatre B' of Epidaurus, Argolis
Theatre of Megalopolis, Arcadia
Theatre of Aigeira, Achaea
Theatre of Elis, Elia
Theatre of Gytheion, Laconia
Theatre of Isthmia, Corinthia
Theatre of Mantineia, Arcadia
Theatre of Messene (Ithome), Messenia
Theatre of Orchomenos, Arcadia
Theatre of Sicyon, Corinthia
Theatre of Sparta, Laconia

Aegean Islands
Theatre of Delos, Cyclades
Theatre of Milos, Cyclades
Theatre of Rhodes, Dodecanese
Theatre of Mytilene, Lesbos
Theatre of Hephaistia, Lemnos
Theatre of Samothrace
Theatre of Thasos
Theatre of Thera, Cyclades
Odeon of Kos, Dodecanese

Magna Graecia
Theatre of Metapontum, Basilicata

Cyprus
Soli Theatre of Soli
Salamis Theatre of Salamis
Kourion Theatre of Kourion
Paphos Odeon Amphitheatre

Sicily
Theatre of Catania
Theatre of Segesta
Theatre of Syracuse
Theatre of Taormina

Asia Minor & Ionia (Turkey)
Theatre of Aigai (Aeolis), Manisa Province
Theatre of Alexandria Troas, Çanakkale Province
Theatre of Antiphellus, Kaş, Antalya Province
Theatre of Aphrodisias, Geyre, Aydın Province
Theatre of Arycanda, Antalya Province
Theatre of Aspendos, Antalya Province
Theatre of Assos, Çanakkale Province
Theatre of Ephesus, İzmir Province
Theatre of Halicarnassus, Bodrum, Muğla Province
Theatre of Hierapolis, Denizli Province
Theatre of Knidos, Datça Peninsula, Muğla Province
Theatre of Cyme (Aeolis), İzmir Province
Theatre of Laodicea, Denizli Province
Theatre of Letoon, Antalya Province
Theatre of Miletus, Aydın Province
Theatre of Myrina, İzmir Province
Theatre of Pergamon, İzmir Province
Theatre of Phocaea, İzmir Province
Theatre of Pinara, Muğla Province
Theatre of Pitane (Aeolis), İzmir Province
Theatre of Priene, Aydın Province
Theatre of Sardis, Manisa Province
Theatre of Side, Antalya Province
Theatre of Termessos, Antalya Province
Theatre of Telmessus, Fethiye, Muğla Province
Theatre of Troy, Çanakkale Province