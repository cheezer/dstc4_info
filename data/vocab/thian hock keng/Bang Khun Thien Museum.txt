Bang Khun Thien Museum (Also Bang Khun Thian) is a museum in Bangkok, Thailand. It is a community museum in Bang Khun Thian District on the Thonburi bank of the Chao Phraya River in southern Bangkok. The museum offers an insight into the history and culture of the locals over many centuries and is under the control of the Bangkok Metropolitan Authority.
The area has a long history of farming and trading, particularly rice. The Mons, originally from the Irrawaddy basin in Burma settled in the area in the 16th century and Chinese immigrants, settled after 1810, and became farmers. Fruit and poultry and also shrimp farms became abundant in the area and the Bang Khun Thien Museum captures this economical and ecological heritage.


== References ==