Thomson Reuters Corporation (/ˈrɔɪtərz/) is a major multinational mass media and information firm founded in Toronto and based in New York City and Toronto. It was created by the Thomson Corporation's purchase of British-based Reuters Group on 17 April 2008, and today is majority owned by The Woodbridge Company, a holding company for the Thomson family. The company operates in more than 100 countries, and has more than 60,000 employees around the world. Thomson Reuters was ranked as Canada's "leading corporate brand" in the 2010 Interbrand Best Canadian Brands ranking. Thomson Reuters' operational headquarters are located at 3 Times Square, Manhattan, New York City; its legal domicile is located at 333 Bay Street, Suite 400, Toronto, Ontario M5H 2R2, Canada.


== History ==


=== The Thomson Corporation ===

The company was founded by Roy Thomson in 1934 in Ontario as the publisher of The Timmins Daily Press. In 1953 Thomson acquired the Scotsman newspaper and moved to Scotland the following year. He consolidated his media position in Scotland in 1957 when he won the franchise for Scottish Television. In 1959 he bought the Kemsley Group, a purchase that eventually gave him control of the Sunday Times. He separately acquired the Times in 1967. He moved into the airline business in 1965, when he acquired Britannia Airways and into oil and gas exploration in 1971 when he participated in a consortium to exploit reserves in the North Sea. In the 1970s, following the death of Lord Thomson, the company withdrew from national newspapers and broadcast media, selling the Times, the Sunday Times and Scottish Television and instead moved into publishing, buying Sweet & Maxwell in 1988. The company at this time was known as the International Thomson Organisation Ltd (ITOL).
In 1989, ITOL merged with Thomson Newspapers, forming The Thomson Corporation. In 1996 The Thomson Corporation acquired West Publishing, a purveyor of legal research and solutions including Westlaw.


=== Reuters Group ===

The Company was founded by Paul Julius Reuter in 1851 in London as a business transmitting stock market quotations. Reuter set up his "Submarine Telegraph" office in October 1851 and negotiated a contract with the London Stock Exchange to provide stock prices from the continental exchanges in return for access to London prices, which he then supplied to stockbrokers in Paris, France. In 1865, Reuters in London was the first organization to report the assassination of Abraham Lincoln. The company was involved in developing the use of radio in 1923. It was acquired by the British National & Provincial Press in 1941 and first listed on the London Stock Exchange in 1984. Reuters began to grow rapidly in the 1980s, widening the range of its business products and expanding its global reporting network for media, financial and economic services: key product launches included Equities 2000 (1987), Dealing 2000-2 (1992), Business Briefing (1994), Reuters Television for the financial markets (1994), 3000 Series (1996) and the Reuters 3000 Xtra service (1999).


=== Post-acquisition ===
The Thomson Corporation acquired Reuters Group PLC to form Thomson Reuters on April 17, 2008. Thomson Reuters operated under a dual-listed company (“DLC”) structure and had two parent companies, both of which were publicly listed — Thomson Reuters Corporation and Thomson Reuters PLC. In 2009, it unified its dual listed company structure and stopped its listing on the London Stock Exchange and NASDAQ. It is now listed only as Thomson Reuters Corporation on the New York Stock Exchange and Toronto Stock Exchange (symbol: TRI).
In June 2008, it was reported that it would be launching a news channel to rival Bloomberg TV and CNBC. This turned out to be false. The company released Thomson Reuters Eikon in 2011.
On February 13, 2013, Thomson Reuters announced it will cut 2,500 jobs to cut cost in its Legal, Financial and Risk division. On October 29, 2013, Thomson Reuters announced it will cut another 3,000 jobs, mostly in its Legal, Financial and Risk division.


== Operations ==
The Chief executive of the combined company is James C. Smith, who was the chief executive for the Professional Division, and the chairman is David Thomson, who was the chairman of Thomson.
In late 2011, The company announced a new organizational structure with four divisions:
Financial and Risk Operation:
Financial Professionals & Marketplaces
Enterprise Solutions
Media

Legal:
Formerly North American Legal and Legal & Regulatory; including West, makers of Westlaw, and Carswell

Thomson Scientific (Intellectual Property & Science):
Formerly Thomson Healthcare
Thomson Healthcare then sold in 2012, and is now Truven Health Analytics.

Tax & Accounting:
Formerly Thomson Tax & Accounting

Thomson Reuters shares are listed on the Toronto Stock Exchange (TSX: TRI) and the New York Stock Exchange (NYSE: TRI). It competes with Bloomberg L.P., in aggregating financial and legal news.


== Market position and antitrust review ==

The Thomson-Reuters merger transaction was reviewed by the U.S. Department of Justice and by the European Commission. On February 19, 2008, both the Department of Justice and the Commission cleared the transaction subject to minor divestments. The Department of Justice required the parties to sell copies of the data contained in the following products: Thomson's WorldScope, a global fundamentals product; Reuters Estimates, an earnings estimates product; and Reuters Aftermarket (Embargoed) Research Database, an analyst research distribution product. The proposed settlement further requires the licensing of related intellectual property, access to personnel, and transitional support to ensure that the buyer of each set of data can continue to update its database so as to continue to offer users a viable and competitive product. The European Commission imposed similar divestments: according to the Commission's press release, "the parties committed to divest the databases containing the content sets of such financial information products, together with relevant assets, personnel and customer base as appropriate to allow purchasers of the databases and assets to quickly establish themselves as a credible competitive force in the marketplace in competition with the merged entity, re-establishing the pre-merger rivalry in the respective fields."
These remedies are viewed as very minor given the scope of the transaction. According to the Financial Times, "the remedy proposed by the competition authorities will affect no more than $25m of the new Thomson Reuters group’s $13bn-plus combined revenues."
The transaction was cleared by the Canadian Competition Bureau.
In November 2009, The European Commission opened formal anti-trust proceedings against Thomson Reuters concerning a potential infringement of the EC Treaty's rules on abuse of a dominant market position (Article 82). The Commission investigated Thomson Reuters' practices in the area of real-time market datafeeds, and in particular whether customers or competitors were prevented from translating Reuters Instrument Codes (RICs) to alternative identification codes of other datafeed suppliers (so-called 'mapping') to the detriment of competition. In December 2012, the European Commission adopted a decision that renders legally binding the commitments offered by Thomson Reuters to create a new licence ("ERL") allowing customers, for a monthly fee, to use Reuters Instrument Codes (RICs) in applications for data sourced from Thomson Reuters' real time consolidated datafeed competitors to which they have moved.


== Purchase process ==
Historically, no single individual has been permitted to own more than 15% of Reuters, under the first of the Reuters Principles, which states, "Reuters shall at no time pass into the hands of any one interest, group or faction." However, that restriction was waived for the purchase by Thomson, whose family holding company, the Woodbridge Company currently owns 53% of the enlarged business. Robert Peston, business editor at BBC News, stated that this has worried Reuters journalists, both because they are concerned that Reuters' journalism business will be marginalized by the financial data provision business of the combined company, and because of the threat to Reuters's reputation for unbiased journalism by the appearance of one majority shareholder.
Pehr Gyllenhammar, chairman of the Reuters Founders Share Company, explained that the Reuters Trust's First Principle had been waived for the Thomson family because of the poor financial circumstances that Reuters had been in, stating, "The future of Reuters takes precedence over the principles. If Reuters were not strong enough to continue on its own, the principles would have no meaning." He stated, not having met David Thomson but having discussed the matter with Geoff Beattie, the president of Woodbridge, that the Thomson family had agreed to vote as directed by the Reuters Founders Share Company on any matter that the trustees might deem to threaten the five principles of the Reuters Trust. Woodbridge will be allowed an exemption from the First Principle as long as it remains controlled by the Thomson family.


== Acquisitions ==
2009
In July, Thomson Reuters acquired Streamlogics. Founded in 1999, Streamlogics is a provider of customizable, high volume, real time data mining solutions  for hundreds of enterprises across several verticals including financial services, technology and health care/life sciences. Streamlogics' webcasting solutions are used for training and certification, marketing and lead generation, and corporate communications.
In August, it bought Vhayu Technologies. Vhayu is a provider of tick data services, and Thomson Reuters had been distributing its Velocity product under the Reuters Tick Capture Engine label for the four years prior to the acquisition.
On September 21, 2009, Thomson Reuters bought Hugin Group, the European IR and PR distribution group, from NYSE Euronext. Terms have not been disclosed, but it has been reported in Danish newspapers that the price was between €40 million and €42m.
In September, it also bought the Abacus software business from Deloitte, a provider of corporate taxation software for the U.K., Ireland, the Netherlands, New Zealand, and Hong Kong, as well as Indirect Tax Reporting software for 20 E.U. countries. Terms of the transaction were not disclosed.
In October, Thomson Reuters acquired Breaking Views
In November, The Tax & Accounting business acquired Sabrix, Inc, a global provider of transaction tax management software applications and related services.

2010
In January,Thomson Reuters acquired Discovery Logic
In February, Thomson Reuters acquired Aegisoft LLC to improve their electronic trading capabilities by offering direct market access.
In May, it acquired Point Carbon A/S, a Norwegian company that provides news and trading analytics for the energy and environmental markets.
In June, it acquired Complinet, a compliance software company.
In October, it acquired Serengeti Law, a matter management and ebilling system.
On November 22, it acquired the legal process outsourcing (LPO) provider Pangea3. Pangea3 serves corporate legal departments and law firms worldwide. Financial Terms of the deal were not disclosed.
In November, it also acquired the banking data and analytic provider Highline Financial, and GeneGo, a supplier of systems biology databases, software and services.

2011
In 20 June 2011, Thomson Reuters acquired CorpSmart from Deloitte.
On July 18, 2011 Thomson Reuters acquired Manatron from Thoma Bravo.
In August 2011, Thomson Reuters acquired GFMS.
In December 2011, Thomson Reuters acquired Emochila, a website development firm in the tax and accounting space, in order to further integrate its CS suite of products onto a cloud-based platform.

2012
In January 2012, Thomson Reuters acquired Dr Tax, Canada’s largest independently owned developer of income tax software for accounting firms and consumers.
In February 2012, Thomson Reuters acquired RedEgg, a provider of media intelligence solutions for public relations and marketing professionals.
On March 22, it acquired BizActions, a digital newsletter and Web marketing providers for accounting firms in North America
In 8 June 2012, Thomson Reuters acquired Apsmart, a London-based company specializing in design and development of mobile solutions.
In 25 June 2012, Thomson Reuters acquired Zawya Limited, a regional provider of business intelligence and unique tools for financial professionals in the Middle East and North Africa .
On 10 July 2012, Thomson Reuters acquired FX Alliance Inc, an independent provider of electronic foreign exchange trading solutions to corporations and asset managers.
In July, it also acquired Dofiscal
In 26 July 2012, Thomson Reuters announced acquisition of MarkMonitor, a San Francisco-based company specializing in internet brand protection software and services.

2013
On January 3, 2013, Thomson announced that it was to acquire Practical Law Company, the London-based provider of practical legal know-how and workflow tools to law firms and corporate law departments. Practical Law Company has more than 750 employees, with principal operations in London and New York, and will be part of the Legal business of Thomson Reuters.
On 16 April 2013, Thomson Reuters acquired Select TaxWorks Assets of RedGear Technologies.
On 6 June 2013, Thomson Reuters acquired Pricing Partners, a provider of OTC Derivatives Pricing Analytics and independent valuation.
On 2 July 2013, Thomson Reuters acquired the foreign exchange options business of Tradeweb.
On August 16, 2013, Thomson Reuters acquired the foreign exchange options risk management technology provider SigmaGenix.
On August 18, 2013, Thomson Reuters acquired a majority stage in Omnesys Technologies and acquired completely the company in September 16, 2013 
In August, it also acquired WeComply.
On September 10, 2013, Thomson Reuters acquired the CPE and CPA Division of Bisk Education Inc  and Kortes.
On October 23, 2013, Thomson Reuters acquired Entagen, acquiring the Cortellis family of products for drug pipeline, deals, patents, and company content.
On December 10, 2013, Thomson Reuters acquired Avedas and expands its scholarly-research analytics solution.

2014
In February, Thomson Reuters acquired Brazil's Domínio Sistemas, a company focused on developing accounting solutions.
In July 1, 2014, Thomson Reuters acquired UBS Convertible Indices.

2015
In January, Thomson Reuters acquired K'Origin.


== Sponsorships ==
Thomson Reuters has sponsored Canadian golf champion Mike Weir and the AT&T Williams Formula One team. It also sponsors Marketplace, a radio show from American Public Media.


== See also ==

Web of Knowledge
Thomson Reuters Citation Laureates


== References ==


== Further reading ==
Peter Kaplan (2007-05-04). "Thomson Reuters deal would raise U.S. antitrust issue". Reuters. 
Penny Crosman (2007-05-16). "How Will the Thomson Reuters Marriage Affect Customers?". Wall Street & Technology (CMP Media LLC). 
Ian Austen (2007-05-09). "Combined Thomson Reuters Would Challenge Bloomberg". New York Times. 
Jerry Bowles (2007-06-18). "Reuters using social media software to launch community around environmental markets". WebPro News. 
Ovide, Shira (2011-07-26). "Crunch Time at Thomson Reuters". Wall Street Journal.  "CEO Glocer Is Under Pressure After Restructuring Backed by Controlling Family".


== External links ==
Official website
Reuters Insider
Thomson Reuters On Demand
Thomson Reuters Foundation