A tiger tail donut (also called tiger tail doughnut or simply tiger tail) is a donut that is twisted with another ingredient so that it looks like the tail of a tiger. This other ingredient may vary; examples include chocolate; chocolate-flavoured dough; a combination of chocolate and cinnamon; and a combination of cinnamon, apples, and coconut. The tiger tail is the second most popular donut at The Donut Man in Glendora, California, United States, surpassed only by the shop's signature donut: the strawberry donut. In 2010, Dunkin' Donuts locations in Kuala Lumpur, Malaysia sold tiger tails to celebrate the Year of the Tiger. Erin Allday of The Press Democrat called the tiger tail the "most unusual donut" at the Donut Hut in Santa Rosa, California.


== See also ==

List of doughnuts


== References ==