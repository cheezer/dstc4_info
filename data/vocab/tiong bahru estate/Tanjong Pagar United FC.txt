Tanjong Pagar United Football Club is a professional football club in Singapore. The club took part in Singapore's S.League from 1996 to 2004, and from 2011 to 2014. The club withdrew from the S.League after the 2004 season because of financial problems but returned to the S.League in 2011. They withdrew from the S.League for the same reason again for the 2015 season.
Prior to the formation of the S.League, the team was known as Tiong Bahru Constituency Sports Club, and won Singapore's National Football League in 1983 and 1987. The club changed its name to Tiong Bahru United Football Club in 1996, and then to Tanjong Pagar United in 1998.
The team's mascot is a Jaguar. During its first run in the S.League, the club's home ground was the Queenstown Stadium. However, as the Queenstown Stadium was occupied since 2010 by French club Etoile FC, Tanjong Pagar United was based in Clementi Stadium for the duration of the 2011 season. However, due to Etoile FC's withdrawal from the S-League by the end of the 2011 season, Tanjong Pagar shifted back to Queenstown Stadium for the 2012 season.
The club were formed as Tiong Bahru Constituency Sports Club in 1975 and debuted in Division III of the National Football League, from which they were promoted as champions in 1978. This was followed by a second successive promotion in 1979, bringing the Jaguars to Division I. In 1982, they won the President's Cup and the following year, they were national league champions. They represented Singapore in the 1984 ASEAN Club Games, finishing third, then captured The Double in 1987. The early 1990s saw further successes, as they were Pools Cup winners in 1991 and 1993, finished runners-up in the FAS Premier League from 1991 to 1993 and bagged the FA Cup in 1994. Their strong performances led to their selection as one of eight clubs to compete in the newly formed S.League, so in 1995, they were renamed to Tiong Bahru Football Club and obtained a permanent home at the Queenstown Stadium.


== Seasons ==
The 1996 season of the S.League was split into two series. Tiger Beer Series winners Geylang United defeated Pioneer Series winners Singapore Armed Forces in the Championship playoff to clinch the S.League title.
2003 saw the introduction of penalty shoot-outs if a match ended in a draw in regular time. Winners of penalty shoot-outs gained two points instead of one.
Tanjong Pagar United sat out the S.League from 2005 to 2010, and in 2015.
Last updated on 5 November 2014


== Players ==


=== Current Squad ===
As of 1 July 2014 
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== Prime League Players ===
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


== Club Officials ==


=== Management ===
Chairman: Edward Liu
Vice-Chairman: Andrew Chua
Honorary Secretary: Zen Tay
Honorary Treasurer: Chan Kok Hock
Club Supervisor: Richard Woon


=== Technical Staff ===
Head Coach: Patrick Vallée
Prime League Coach: Nicolas Possetti
Goalkeeping Coach: Chua Lye Heng
Team Manager: Satiman Saim
Logistics Officer: Leonard Koh
Sports Trainer: Jeffrey Tham


== Managers ==
 PN Sivaji
 Robert Alberts (1996–98)
 Tohari Paijan (1998–2002)
 Moey Yok Ham (2003–04)
 Karim Bencherifa (2004)
 Terry Pathmanathan (2011–2012)
 Patrick Vallée (14 Nov 2012–)


== Honours ==


=== Domestic ===
League
National Football League Division One: 2

1983, 1987

Cups
Singapore Cup: 1

1998

Singapore FA Cup: 1

1998

President's Cup: 4

1982, 1985, 1987, 1994


== Performance in AFC competitions ==
Asian Club Championship: 2 appearances

1986: Qualifying Stage
1988: Qualifying Stage


== Sponsors ==
Main Sponsor: SINGA Energy drink
Apparel Supplier: Mitre Sports International
Co-Sponsor: EBCHIP Asia Pacific, Woodlands Transport


== References ==
^ Ong, Terence (4 November 2014). "Jaguars to sit out next S-League season, Rams to merge with Hougang". The Straits Times. Retrieved 6 November 2014. 
^ a b Low, Lin Fhoong (5 November 2014). "Uncertainty over S-League’s changes for 2015". Today. Retrieved 5 November 2014. 
^ Malathi Das and Palakrishnan (1996), "S.League: the kick-off", Singapore Professional Football League Pte Ltd, p. 56
^ http://www.sleague.com/clubs-profile/tanjong-pagar/team-profile


== External links ==
Official club website
S.League website page on Tanjong Pagar United FC