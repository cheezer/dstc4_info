Tiong Bahru MRT Station is an underground station located on the East West Line on the Singapore MRT. The station is directly connected to a shopping mall, Tiong Bahru Plaza and is at Tiong Bahru Road and Jalan Membina. As the name suggests, the station is located in the northern part of the Tiong Bahru which is under the Bukit Merah Urban Planning Area. The MRT Station serves the Bukit Ho Swee, Tiong Bahru and Bukit Merah View estates. Before the HarbourFront opened, it was the nearest MRT station from Sentosa. The station's upgrading, which included a new lift for the disabled, was completed in 2006.
To commemorate Total Defence Day in 2000, the Singapore Civil Defence Force conducted the first ever Shelter Open House at this station on February 15 and February 16, together with Somerset and Lavender.


== Transport connections ==


== Exits ==
A: Tiong Bahru Road. Jalan Membina, Bukit Merah West NPC, Henderson CC, Zhangde Pri Sch
B: Tiong Bahru Plaza, Central Plaza


== Station layout ==


== References ==


== External links ==
Official website