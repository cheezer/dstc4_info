Built in the 1930s, Tiong Bahru Estate is one of the oldest housing estates in Singapore. It was the first project undertaken by the Singapore Improvement Trust (SIT), a government body administered by the British colonial authority, to provide for mass public housing in Singapore. The estate consists of about 30 apartment blocks with a total of over 900 units. The apartment blocks are made up of two to five-storey flats and the units are assorted three to five-room apartments.


== Etymology and Early History ==
Tiong Bahru means "New Cemetery" (thióng 塚 – Hokkien for cemetery, bahru – Malay for new). Till the 1920s, it was an area dotted with many cemeteries. They were new as opposed to the established cemeteries in Chinatown. The present day Tiong Bahru Rd. was at onetime called " Burial Ground Rd". In 1925 this area was declared unsanitary and designated for improvement. The SIT (S'pore Improvement Trust) cleared out the squatters and moved the graves and then filled in and levelled the area.
The construction style of the estate is a mix of Streamline Moderne and local Straits Settlements shop-house architecture. The flats feature rounded balconies, flat rooftops, spiral staircases, light wells and underground storage and shelters. One notable feature of Tiong Bahru estate is that all its streets are named after Chinese pioneers of the 19th and early 20th centuries (Lim Liak, Kim Pong, Guan Chuan, Chay Yan, etc.).
It is apparent that a lot of effort was put into designing the estate with a series of flats that are visually pleasing. Thus the flats in the Tiong Bahru estate contrasted markedly with those of the much later post-war mass housing programs undertaken by SIT's successor, the Housing and Development Board. In contrast with the aesthetic art deco theme of the Tiong Bahru flats, the flats built by the Housing Board in the 1950s and 1960s are starkly utilitarian in appearance and design; where flats are almost identical in their two-dimensional "matchbox" style.
Not many people could afford to live in the Tiong Bahru Estate during the pre-World War II years. It was the choice place of living for the upper class and also the place where the rich and powerful kept their mistresses. For this reason, the estate used to be known as Mei Ren Wo ("den of beauties" in Chinese).
The population in Tiong Bahru estate tripled after the Second World War, and it gradually lost its exalted status as an exclusive upper class housing estate. However, it retained its close-knit Kampung (small village in Malay) spirit and became a bustling and lively little town where everyone knows and looks out for each other.

Tiong Bahru is renowned for its bird-singing aviaries, which has now been torn down and replaced by the Link Hotel. Bird lovers gather with their songbirds every morning to catch up with fellow bird lovers over coffee and tea amid the crisp, melodious chirps of Prinias, Robins, and Shrikes. The bird corner is now part of The Link Hotel that took over the former block of flats in the mid-2000s. Its attempts to revive the bird corner have yet to become fully successful.
Tiong Bahru Market, under the charge of the National Environment Agency, was, in fact, the first modern market to be built in a housing area, in 1955, by the SIT. Prior to the building of the Market, also known as Seng Poh Market, the hawkers were all street-hawkers, who were under constant threat of being chased off as street-trading was illegal. Tired of their uncertain lifestyle, the hawkers organised themselves and petitioned successfully to the Governor of Singapore, for a market to be built. The SIT thus, built an experimental modern market on the same spot as today's market.
In recent years, the population of Tiong Bahru estate has declined steadily. Following the economic boom of the 1970s and 1980s, the next generation of Singaporeans became more affluent and sophisticated. They hold allure to the sleek and modern designs of new towns and private condominiums and began moving out of Tiong Bahru en-masse. Consequently, Tiong Bahru estate for a while became an estate for the elderly, but lately younger Westerners and sophisticated Singaporeans have been rediscovering the charm of the area.
In 2003, as a result of many years of discussion over its heritage status both as a pioneering experiment in modern urban housing in Singapore and S.E.Asia, as well as its entrenched familiarity in Singaporeans' sense of place, 20 blocks of the pre-WW2 flats were gazetted by the Urban Redevelopment Authority for Conservation. Included in the Tiong Bahru Conservation Area are 36 units of shopflats/shophouses along Outram Road.
The area, is also home to several events that demonstrate a lively folk culture. The Market has a regular 7th Lunar Month dinner and auction. The long established Qi Tian Gong Temple on Eng Hoon Street, dedicated to the Monkey God, has birthday celebrations annually on the 16th day of the 1st and 8th Lunar Months, which includes lion, dragon dances, and performances of Chinese street opera.
The estate and its residents were the subject of the 10th in the CIVIC LIFE films by Irish filmmakers Joe Lawlor and Christine Molloy. TIONG BAHRU, starring 150 volunteers from the estate and from across Singapore, was shot in June 2010 and premiered at the National Museum of Singapore in October 2010.
With the relocation of popular independent bookstore BooksActually and coffee joint Forty Hands Coffee in 2011, the area has recently been revitalized by hipster culture, and was listed by Thrillist and Vogue magazines among the most hipster neighborhoods in the world. 


== Amenities ==
There is one shopping centre in Tiong Bahru Estate, Tiong Bahru Plaza and there is also the popular Tiong Bahru Market, known to being one of the best hawker centres in Singapore. Other amenities in Tiong Bahru include Tiong Bahru Community Centre and Tiong Bahru Park. Singapore General Hospital is also located near to Tiong Bahru. There is one school in Tiong Bahru, namely Zhangde Primary School.


== Transportation ==
The nearest Mass Rapid Transit station is Tiong Bahru MRT Station. Bus services 5, 16, 32, 33, 63, 64, 75, 120, 121, 122, 123, 123M, 175, 195, 851, 970 and NR5 passes through Tiong Bahru Road. Havelock MRT Station will be located within close proximity to Tiong Bahru on the upcoming Thomson-East Coast MRT Line. The new MRT Station will be operational from 2021 onwards.


== See also ==
Leng Kee
Alexandra
BooksActually
118, a 255-episode Singaporean television drama airing from 2014 to 2015, set against the backdrop of a coffeeshop in Tiong Bahru


== References ==


== External links ==