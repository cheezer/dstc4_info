Tiong Bahru Plaza (Simplified Chinese: 中峇鲁广场) is a shopping mall located in the northern part of the Tiong Bahru Estate in Singapore, near Tiong Bahru Road, Jalan Membina and Bukit Ho Swee Crescent. It consists of a 20-storey office tower block (Central Plaza), and a 6-storey shopping and entertainment complex, with 3 basement carparks.
It is connected to Tiong Bahru MRT station, leading to Basement 1 and Level 1.
The suburban mall has a total of 167 shops with its anchor tenants namely, NTUC FairPrice, Kopitiam, Challenger, POSBank, UOB, Popular Bookstore and Golden Village.


== References ==
Cheong Suk-Wai, "It's a MALL world after all" by The Sunday Times, 2 April 2006


== External links ==
Official website