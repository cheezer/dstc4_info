Zhang (/dʒɑːŋ/) is the pinyin romanization of the very common Chinese surname written 张 in simplified characters and 張 traditionally. It is spoken in the first tone: Zhāng. It is a surname that exists in many languages and cultures, corresponding to the surname 'Archer' in English for example.
It is also the pinyin romanization of the less-common surname 章 (also Zhāng).
張 was listed 24th in the famous Song-era Hundred Family Surnames. Today, it is one of the most common surnames in the world and 张 was listed by the People's Republic of China's National Citizen ID Information System as the third-most-common surname in mainland China in April 2007, with 87.50 million bearers. A commonly cited but erroneous factoid in the 1990 Guinness Book of Records listed it as the world's most common surname, but no comprehensive information from China was available at the time and more recent editions have not repeated the claim.


== Romanization ==
張 (张) and 章 are also commonly romanized as Chang on Taiwan and among the Chinese diaspora using the older Wade-Giles system. Both are also romanized as Cheung in Hong Kong; Cheong in Macao and Malaysia; Teo and Teoh in Teochew; Chong in Hakka; Tsan and Tsaon among Wu Chinese varieties like Shanghainese; Tong in Gan; and Tiong in East Malaysia and the Philippines; and Tjong, Sutiono or Thiono in Indonesia.
張 was the Chữ Nôm form of the Vietnamese surname Trương. The Hanja of the Korean surname romanized Jang and Chang (장). It remains the Kanji for the Japanese surname romanized Chō.
In Vietnamese, the surname written 章 in Chữ Nôm is clearly distinguished and written as Trang or Chương.


== Distribution ==
As mentioned above, 张 is the third-most-common surname in mainland China, making up 6.83% of the population of the People's Republic of China. On Taiwan, 張 is the fourth-most-common surname, making up 5.26% of the population of the Republic of China. 章 was unlisted among the top 100 in either location.
Among the Chinese diaspora, the name remains common but takes on various romanizations. "Chong" is the 19th-most-common surname among Chinese Singaporeans; "Chang" is the 6th-most-common surname among Chinese Americans; and "Zhang" was the 7th-most-common particularly Chinese surname (i.e., excluding ethnically diverse surnames such as "Lee") found in a 2010 survey of Ontario's Registered Persons Database of Canadian health card recipients.


== History ==


=== Characters ===
張 combines the Chinese characters 弓 (gōng, "bow") and 長 (simp. 长, cháng, "long" or "wide"). It originally meant "to open up" or "to spread" as an arching bow, but as a common noun in modern use it is a measure word for flat objects such as paper and cloth, like the English "sheet of".
章 combines the characters 音 (yin, "sound", "(musical) note") and 十 (shi, "ten"). It originally meant "brilliant", "to display", "a distinctive mark" and was used as the name of a fief, but as a common noun in modern use it means an "article" in a newspaper or magazine or a "chapter" in a book or law.


=== Families ===
The traditional origin of the surname 張 (Old Chinese: *C. traŋ) is rooted in Chinese legend. The fifth son of the Yellow Emperor, Qing Yangshi (青陽氏/青阳氏, Qīng Yángshì), had a son Hui (揮/挥, Huī) who was inspired by the Heavenly Bow constellation (天弓星, Tiān Gōng Xīng) to invent the bow and arrow. Hui was then promoted to "First Bow" (弓正, Gōng Zhèng) and bestowed the surname 張, which – when broken into its constituent radicals – means "widening bow" or "archer". Its Middle Chinese pronunciation has been reconstructed as Trjang.
The surname 章 (Old Chinese: *taŋ) originated from the legendary Yan Emperor, whose personal surname was Jiang (姜). On the establishment of the state of Qi, Jiang Ziya apportioned the land among his many descendants, including a one known as Zhang (鄣国). Some of the people of this state took 章 as their surname, particularly after it was annexed by Qi. The Middle Chinese pronunciation of the name was Tsyang, the beginnings of what we now know to be the "Zhang" surname.


== List of persons with the surname ==


=== 張 / 张 ===


=== Historical Figure ===
Zhang Yan (201-238) Son of official during the Qin Dynasty
Zhang Yi (died 309 BC), strategist in the Warring States period.
Zhang Han (died 205 BC), Military General of the Qin Dynasty
Zhang Tang (died 116 BC), High Rank Official of the Western Han Dynasty under Emperor Wu
Zhang Anshi son of Zhang Tang official of the Han Dynasty
Zhang Liang (died 186 BC), adviser to Liu Bang (founding emperor of the Han dynasty).
Zhang Heng Chinese polymath who live during the Eastern Han dynasty
Zhang Liang (died 184),Rebel Leader of the Yellow Turban during the Late Han Dynasty
Zhang Bao (died 184),Rebel Leader of the Yellow Turban during the Late Han Dynasty
Zhang Jue Rebel Leader of the Yellow Turban during the Late Han Dynasty
Zhang Rang (died 189), Head of the Ten Regular Attendant in the late Han Dynasty
Zhang Fei (died 221), general of Shu Han in the Three Kingdoms period.
Zhang Bao son of Zhang Fei and general of Shu Han
Zhang Hong (153–212),", Official of Sun Quan in the Late Han Dynasty.
Zhang Yi (died 230), general of Shu Han in the Three Kingdoms period.
Empress Zhang (died 237), Empress of Shu Han in the Three Kingdoms period.
Zhang He (died 231), general of Cao Wei in the Three Kingdoms period.
Zhang Liao (169–222),", general of Cao Wei in the Three Kingdoms period.
Zhang Zhao (156–238),", Minister of Eastern Wu in the Three Kingdoms period.
Zhang Cheng (178–244),", general and Politician of Eastern Wu in the Three Kingdoms period.
Zhang Chunhua (died 247),", wife of Cao Wei General and Politician Sima Yi
Zhang Ni (died 254), general of Shu Han in the Three Kingdoms period
Zhang Yi (died 264), general of Shu Han in the Three Kingdoms period
Zhang Bu (died 264), general of Eastern Wu in the Three Kingdoms period
Zhang Hua (232–300), Western Jin dynasty official and poet.
Zhang Liang General Official and Chancellor of the Tang Dynasty
Zhang Yue (663–730), Tang dynasty chancellor and poet.
Zhang Jiuling (673–740), Tang dynasty chancellor and poet.
Zhang Jun (1086–1154), General of the Song Dynasty
Zhang Hongfan (1238–1280), Yuan dynasty general
Zhang Juzheng (1525–1582), Ming dynasty statesman.
Zhang

Tiong
Tiong Hiew King (born 1935), Chinese Malaysian businessman.
Tiong King Sing, Chinese Malaysian politician.


=== 章 ===
Zhang Binglin (1868–1936), Chinese philologist, textual critic, and anti-Manchu revolutionary.
Zhang Zhong (born 1978), Chinese chess grandmaster who now plays for Singapore.
Zhang Ziyi (born 1979), Chinese actress and model.


== See also ==
Chinese name
Chinese surname
List of common Chinese surnames


== References ==