Kaya toast is a well-known snack in Singapore and Malaysia. Kaya toast is prepared with kaya (coconut jam), a topping of sugar, coconut milk and eggs, pandan, and sometimes margarine or butter. It is generally served on toast, and also sometimes on crackers. It is considered a breakfast staple, and remains popular in Singapore. The dish is sometimes dipped into soft-boiled egg with a little dark soy sauce and white pepper.
Singaporeans and Malaysians often consume this food with a cup of tea or coffee, which has merited the snack's inclusion in many coffee houses. The Singaporean companies Ya Kun Kaya Toast and Killiney Kopitiam are franchises which have proliferated by this popular snack.


== History ==
This delicacy is credited to the Hainanese. Many Hainanese worked on British ships as kitchen hands. When they settled in the British colonies now constituting Singapore and Malaysia, they started selling the foods which they prepared for the British, including coffee, toast, and French toast, to the local populations. They replaced the Western jams and preserves favoured by the British with native coconut jams.


== References ==

Seetoh, KF (August 23, 2013). "Tong Ah is Back". Yahoo Entertainment (Singapore). Retrieved 2 December 2013. 


== Further reading ==
Lynch. Rene (July 22, 2009). "Sending out an SOS for the Kaya Toast at Susan Feniger's Street". Los Angeles Times. Retrieved 2 December 2013. 


== External links ==
Image of kaya toast with coconut jam. Chicago Tribune. June 22, 2012.