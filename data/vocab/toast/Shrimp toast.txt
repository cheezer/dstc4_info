Shrimp toast or prawn toast is a Chinese dim sum dish. It is made from small triangles of bread, brushed with egg and coated with minced shrimp and water chestnuts, then cooked by baking or deep frying. It is a common appetizer in Australian and American Chinese cuisine. A common variant in the United Kingdom and Australia is sesame prawn toast. This involves sprinkling sesame seeds before the baking or deep frying process.


== Gallery ==


== History ==
This dish has over 100 years of history, originating in the Guangzhou Canton, in China's Guangdong Province. It is called Hatosi 蝦多士 in Cantonese, Ha meaning shrimp, Tosi being a loan word from English meaning toast. The dish's range expanded along with foreign trade, making its way to Japan and Southeast Asian countries like Vietnam and Thailand.


== Hatoshi in Japan ==
The dish was introduced to Japan during the Meiji period through the port of Nagasaki, whose local Shippoku cuisine blended the cookery of China, Japan, and the West. In Japanese, shrimp toast is known as Hatoshi (Japanese: ハトシ), a loan word from Cantonese. Many Chinese restaurants and shops in Nagasaki's Chinatown still serve this dish. Some also serve a variant made with pork.


== See also ==
Krupuk