Tonga (Gitonga) is a Bantu language spoken along the southern coast of Mozambique. Often thought to be closest to Chopi to its south, the two languages have only a 44% lexical similarity.


== References ==


== External links ==
Christian hymns, together with some of the Psalms of David in the language of the Ba Tonga, as spoken in the district of Inhambane, east Africa (1901)
Ruthe. Samuele: Ruth, and I. Samuel, chapters I to IV, in the Gitonga language (1902)
Itestamente lipya nya pfumu yatu Jesu Kristu: kanga ku lobidwego ki gitonga (1905)