Kowloon Tong is a station on MTR's Kwun Tong Line and East Rail Line in New Kowloon, Hong Kong.
Kowloon Tong Station serves Kowloon Tong and its vicinity, including Yau Yat Tsuen, the Festival Walk shopping centre, City University of Hong Kong and Hong Kong Baptist University.
This station serves as one of the two interchange stations for the East Rail Line, leading up to the New Territories and entry point to mainland China. Therefore, it is one of the busiest stations in the system. Originally being the only such interchange station, its load was heavily reduced when a new interchange station was built at Tsim Sha Tsui/East Tsim Sha Tsui.
A new concourse for East Rail Line was opened on 15 April 2004 to increase its capacity.
On 28 September 2008, the Kwun Tong Line and East Rail Line interchange gates were removed.


== Timetable ==


== Station layout ==
There is a reserved space for a track connecting the East Rail Line and the Kwun Tong Line in the westbound tunnel just outside Kowloon Tong station towards Shek Kip Mei. The space was originally designed to allow Metro-Cammells to be transferred to the Kwun Tong Line when they were unloaded at Hung Hom, but the track was never built as the MTR decided to use lorries to carry all of its train carriages to the Kowloon Bay Depot.
Note that the platforms for both the East Rail Line and the Kwun Tong Line are called platforms 1 and 2.


== Entrances and exits ==
A pedestrian walkway connects the station with Festival Walk, a major shopping centre.
Kwun Tong Line concourse
A1: Suffolk Road
A2: Hong Kong Baptist Hospital
B: Suffolk Road
C1: Festival Walk
C2: City University of Hong Kong 
E: EDB Education Services Centre
East Rail Line southern concourse
D: Public Transport Interchange 
East Rail Line northern concourse
F: Kent Road 
East Rail Line platform 2 (ground level)
G1: To Fuk Road 
G2: Festival Walk
East Rail Line platform 1 (ground level)
H: Hong Kong Productivity Council 
Exit H is also accessible from the East Rail Line northern concourse. It is possible to walk between Exits A and E inside the vicinity MTR without entering the paid area. The same goes for Exits B, C, F and H.


== Transport connections ==
Minibus Routes
To Academic Community Hall/Broadcast Drive/Baptist University:
29A (Exit A2)
To Beacon Hill:
29B (Exit A2)
To Kowloon City:
25M (Exit B2)
To World Wide Gardens:
61M (Exit A2)
Kowloon Tong Railway Station Bus Terminus (Exits A1 or B2)
To Hin King Estate/ Sun Tin Wai Estate:
281M
To Sui Wo Court:
80M


== References ==