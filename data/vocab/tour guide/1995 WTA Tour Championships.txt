The 1995 WTA Tour Championships were held in New York, United States between November 13 and November 19.


== Champions ==


=== Singles ===

 Steffi Graf def.  Anke Huber, 6–1, 2–6, 6–1, 4–6, 6–3


=== Doubles ===

 Jana Novotná /  Arantxa Sánchez Vicario def.  Gigi Fernández /  Natasha Zvereva 6-2, 6-1


== References ==
ITF site