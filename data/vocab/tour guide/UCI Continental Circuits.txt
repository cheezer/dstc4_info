The UCI Continental Circuits are continental circuits for a series of road bicycle racing competitions since 2005. The five circuits (each representing Africa, the Americas, Asia, Europe and Oceania continents) are inferior to the UCI World Tour. Both the World Tour and the Continental Circuits comprise a series of races in which various cycling teams compete regularly. It is introduced by the Union Cycliste Internationale (UCI) to provide an adequate and realistic context in order to encourage the expansion of cycling, everywhere in the world.


== UCI Africa Tour ==


=== Winners ===
There is a competition for the individual ranking, team ranking and country ranking with the most points gained from winning or achieving a high place in the races. The rider in the first place of the individual ranking of the continental circuit at the end of the season will be the sacred winner of his circuit. The list shows the UCI Africa Tour champions according to the year.


=== Events ===


== UCI America Tour ==


=== Winners ===
There is a competition for the individual ranking, team ranking and country ranking with the most points gained from winning or achieving a high place in the races. The rider in the first place of the individual ranking of the continental circuit at the end of the season will be the sacred winner of his circuit. The list shows the UCI America Tour champions according to the year.


=== Events ===


== UCI Asia Tour ==


=== Winners ===
There is a competition for the individual ranking, team ranking and country ranking with the most points gained from winning or achieving a high place in the races. The rider in the first place of the individual ranking of the continental circuit at the end of the season will be the sacred winner of his circuit. The list shows the UCI Asia Tour champions according to the year.


=== Events ===


== UCI Europe Tour ==


=== Winners ===
There is a competition for the individual ranking, team ranking and country ranking with the most points gained from winning or achieving a high place in the races. The rider in the first place of the individual ranking of the continental circuit at the end of the season will be the sacred winner of his circuit. The list shows the UCI Europe Tour champions according to the year.


=== Events ===


== UCI Oceania Tour ==


=== Winners ===
There is a competition for the individual ranking, team ranking and country ranking with the most points gained from winning or achieving a high place in the races. The rider in the first place of the individual ranking of the continental circuit at the end of the season will be the sacred winner of his circuit. The list shows the UCI Oceania Tour champions according to the year.


=== Events ===


== Rankings ==
For more detailed explanations of the new-for-2015 points system and detail rankings see: 2015 UCI men's road cycling rankings


== External links ==
UCI Africa Tour
UCI America Tour
UCI Asia Tour
UCI Europe Tour
UCI Oceania Tour