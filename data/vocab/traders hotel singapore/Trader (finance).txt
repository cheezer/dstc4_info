A trader is person or entity, in finance, who buys and sells financial instruments such as stocks, bonds, commodities and derivatives, in the capacity of agent, hedger, arbitrageur, or speculator. According to the Wall Street Journal in 2004, a managing director convertible bond trader was earning between $700,000 and $900,000 on average.


== Duties and types ==
Traders are either professionals (institutional) working in a financial institution or a corporation, or individual (retail). They buy and sell financial instruments traded in the stock markets, derivatives markets and commodity markets, comprising the stock exchanges, derivatives exchanges and the commodities exchanges. Several categories and designations for diverse kinds of traders are found in finance, these may include:
Day trader
Floor trader
High-frequency trader
Pattern day trader
Rogue trader
Stock trader


== See also ==
Commodities exchange
Commodity market
Derivatives market
List of commodity traders
List of trading losses
Stock exchange
Stock market
Trading strategy


== References ==
^ Street's Weather: Bonus Showers - WSJ.com


== External links ==
 Trading at Wikibooks