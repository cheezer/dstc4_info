The Norwegian and Swedish Travellers (Norwegian: romanifolket, tatere, sigøynere; Swedish: resande, zigenare, tattare; Scandoromani: romanisæl, romanoar, rom(m)ani, tavringer/ar, tattare) are a group or branch of the Romani people who have been resident in Norway and Sweden for some 500 years, as distinct from other Romanies who arrived starting in the late 19th century. The estimated number of Romani Travellers is 65,000 while in Norway the number is probably less than 10,000.


== Origins ==
By history and culture, they are related to similar groups of Romani people in other countries, such as British Romani groups like Romanichals, Lowland Scottish Travellers and German Sinti.
Modern-day Travellers are the descendants of the first Romanies who arrived in Scandinavia during the 16th century. Some were deportees from Britain to Norway, but most came via Denmark. The Danish tatere first arrived in Denmark about 1410. They were met with hostility, and segregated themselves on the moorlands of Jutland, where some prospered in wool production and trade. They did not integrate in Denmark for nearly 400 years. According to their oral tradition, they were originally forced out of Rajasthan, India, when invaders conquered it. They do not consider themselves “gypsies”.
A related group are the Finnish Kale, descendants of early Scandinavian Romanies who were deported in the 17th century from Sweden proper to Finland. Over the years, the Finnish Kale developed separately from the other Scandinavian Travellers (particularly after Finland was lost to the Russian Empire in 1809), and their languages and customs now differ markedly. The Finnish Kale, however, maintain that their ancestors had originally come from Scotland, thereby supporting the idea that they and the Scandinavian Travellers are distantly related to present-day Romanichals.
Romani Travellers in Norway at times have been confused with the indigenous Norwegian Travellers, although they perceive the latter group to be non-Romanies by culture and origins.


== Names for the group ==

By the settled majority population, the Norwegian Travellers are known as tatere, and in Sweden they used to be called tattare. Both terms hint to the original misconception that these people were Tatars. Before the turn of the 20th century, the majority population made little distinction between tatere/tattare and "Gypsies" (Norwegian: sigøynere; Swedish: zigenare); this situation changed mainly due to the arrival of Kalderash Roma from Russia and Central Europe in the last decades of the 19th century, to whom the latter term came to be applied almost exclusively.
Skojare was a former name for Travellers in Sweden; in Norway skøyere was associated with indigenous Travellers. Fant was another term formerly applied to both Romani and non-Romani Travellers in southern Norway and some parts of Sweden. All these terms nowadays are considered pejorative due to their connotation of vagabondage and vagrancy.
In Sweden, tattare is now considered a disparaging term and has been completely abandoned in official use. Since 2000 Swedish Travellers are officially referred to as resande (Travellers), and counted as one of several groups within the "Roma" national minority. They often refer to themselves as resandefolket (Travelling people), or dinglare. Less common is the term tavringar. In recent years there has been an attempt to term Swedish Travellers as tschiwi, but this usage is contested.
For Norwegian Travellers, however, the name tatere does not carry the same stigma as in Sweden; some Traveller organizations maintain this term in their official names. In Norway the Travellers are categorized as a national minority group, officially referred to as tatere, reisende (Travellers) or romanifolk. Norwegian Travellers refer to themselves by various names, such as romany, romanoar, romanisæl, vandriar (Wanderers), etc. In contrast to Sweden, in Norway a distinction is made between romanifolket and rom (i.e., Roma groups that arrived since the 19th century) in the official legislation on national minorities.


== Language ==
The Travellers in Sweden and Norway speak a form of the Romani language known to academics as Scandoromani, sometimes referred to as Tavringer Romani. Many words of Scandoromani origin have survived in the Scandinavian languages, both in common speech and slang. Examples:
Tjej, meaning "Girl" in Swedish (Official)
Puffra, meaning "Gun" in Swedish (Common)
Hak, meaning "Place" (as in "Joint" or "Establishment") in Swedish (Common)
Vischan, meaning "The Bush" (as in boondocks or rural areas) in Swedish (Common)


== Organisations ==
Romani Travellers in Sweden and Norway have founded organisations for preserving their culture and lobbying for their collective rights. One example is Föreningen Resandefolkets Riksorganisation, based in Malmö, Sweden.


== Media ==
Romani Posten (also Romaniposten, The Romani Post; ISSN 0809-8379) was a news magazine for the Romani Traveller community in Norway. It had no political or religious affiliation, and published articles in Norwegian. At its most frequent, it came out eight times per year. On 6 September 2003, it was founded as an on-line publication; the first print edition October 2006. Jone Pedersen was the founding publisher and editor-in-chief. As of 2007, it had ceased publication.


== References ==
^ http://www.ne.se/romer
^ Eltzler. Zigenarna och deras avkomlingar i Sverige (Uppsala 1944) cited in: Angus. M. Fraser. The Gypsies (The Peoples of Europe) p120
^ Luton government website
^ Ethnologue website
^ Ethnologue website
^ http://www.spiritus-temporis.com/norwegian-and-swedish-travellers/
^ National Minorities of Finland, The Roma — Virtual Finland
^ Ethnologue website
^ Cf. Tater in Danish Wikipedia.
^ S.v. "Skojare", Gösta Bergman, Ord med historia (Stockholm: Prisma, 2003), pp. 536–7.
^ Cf. Skojare from Svenska Akademiens ordbok.
^ [1] Förföljda för sin dröm om frihet
^ Föreningen Resandefolkets Riksorganisation website
^ Romani Posten, no. 6-2006, p. 2


=== Sources, further reading ===
Lindell, Lenny; Thorbjörnsson-Djerf, Kenth (2008). Carling, Gerd, ed. Ordbok över svensk romani: Resandefolkets språk och sånger (in Swedish). Stockholm: Podium. ISBN 978-91-89196-43-8.  (This is a lexicon and grammatical overview of Swedish Scandoromani by Lenny Lindell; includes several Traveller song texts in extenso.)
Sundt, Eilert (1852). Beretning om Fante- eller Landstrygerfolket i Norge: Bidrag til Kundskab om de laveste Samfundsforholde (in Norwegian) (2nd ed.). Christiania: J. Chr. Abelsted. OCLC 7451358. 
Hazell, Bo (2002). Resandefolket: Från tattare till traveller (in Swedish). Stockholm: Ordfront. ISBN 91-7324-682-4. OCLC 185986575. 
[2]


== External links ==
http://www.romanifolket.info.se (Swedish)
Nasjonale minoritetar i Noreg - Om statleg politikk overfor jødar, kvener, rom, romanifolket og skogfinnar (Norwegian)