The keping was the currency of Trengganu until 1909 when it was replaced by the Straits dollar. It was subdivided into 10 pitis. Coins were issued in denominations of 1 pitis (tin), 1 keping (copper) and 10 keping (tin).


== See also ==
Kelantan keping