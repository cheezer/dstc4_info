Treasure Island (French: L'île au trésor) is a 1985 Chilean-French adventure film directed by Raúl Ruiz. It was screened in the Un Certain Regard section at the 1991 Cannes Film Festival.


== Cast ==
Melvil Poupaud as Jim Hawkins
Martin Landau as Old Captain
Vic Tayback as Long John Silver
Lou Castel as Doctor / Father
Jeffrey Kime as Timothy (The Squire)
Anna Karina as Mother
Sheila as Aunt
Jean-François Stévenin as Israel Hands (The Rat)
Charles Schmidt as The blind man
Jean-Pierre Léaud as Midas
Yves Afonso as French captain
Pedro Armendáriz Jr. as Mendoza
Tony Jessen as Ben Gunn
Michel Ferber as Crabb


== References ==


== External links ==
Treasure Island at the Internet Movie Database