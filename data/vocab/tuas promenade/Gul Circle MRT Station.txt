Gul Circle MRT Station (EW30) is a planned elevated above-ground Mass Rapid Transit station on the East West Line in Jurong, part of the Tuas West Extension (TWE) in Singapore.
This station is to serve people in the sub-zones of Gul Circle, Pioneer, Tuas West and Pioneer Sector.


== History ==
It was first announced on 11 January 2011 by Transport Minister Mr Raymond Lim in a speech while visiting Bedok when new platform screen doors opened there.
It is expected to be completed by mid-2016 and benefit an estimated 100,000 commuters daily. It is also expected to be the first elevated stacked island platform, as there will be an extension leading out from this station to Tuas South which is still under planning. The $190-million contract was awarded to Shanghai Tunnel Engineering, a major civil engineering company from China for this station.
This station ceiling will be 33 metres above ground - about the height of a 10-storey Housing Board block, the highest elevated station along the MRT network. There are two reasons for the height. First, the 7.5 km, $3.5 billion extension goes over the Ayer Rajah Expressway viaduct at the Pan-Island Expressway interchange. Second, a 4.8 km portion of the line is being integrated with a road viaduct, which runs below the rail line. 


== Station layout ==


== Transport connections ==


== References ==