Tuas Depot is a future train depot located off Tuas West Drive in Singapore. It will be constructed by Jurong Primewide Pte Ltd at a contract sum of S$237.1 million. The depot will provide maintenance services for the East West Line of the Mass Rapid Transit network, and is directly linked to the upcoming Tuas Link MRT station. Construction of the 10,000 square metres (110,000 sq ft) depot together with the four stations of the Tuas West Extension started in late 2011 and is expected to be completed by 2016. The depot will have space for 60 trains.


== References ==