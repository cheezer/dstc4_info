Tuas Link MRT Station (EW33) is an elevated Mass Rapid Transit station on the East West Line, part of the Tuas West Extension (TWE) in Singapore. It is the future western terminus of the East West Line which will replace Joo Koon. The station is to provide rail connection to the north-western part of Tuas. It will also be the first elevated station to have the platforms lower than the station control and the ticketing machines. It will also have a bus terminal, Tuas Bus Terminal. It will also have a shopping centre at Tuas Link, as it is also an integrated transport hub. Tenants include Uniqlo, H&M and New Look, which is called Tuas Link Mall.


== History ==
The groundbreaking ceremony was held at this station to mark the start of the construction of Tuas West Extension on 4 May 2012 by Transport Minister Lui Tuck Yew. It was first announced on 11 January 2011 by Transport Minister Mr Raymond Lim in a speech while visiting Bedok MRT Station when new platform screen doors opened there. To be completed by 2016 and benefit an estimated 100,000 commuters daily, there will be a complementary bus terminal facility built beside it, at the junction of Tuas West Drive and Tuas Link 4., which may be used to host cross-border bus services to Malaysia and beyond, via the Second Link.


=== Casualty incident ===
An Indian national died after falling into a 68 m (223 ft) deep bore hole at the Tuas West Extension worksite off Tuas West Road near Raffles Marina. The police was notified of the accident at around 2.50 pm on 25 August 2012 and subsequently dispatched Singapore Civil Defence Force (SCDF) rescue vehicles to the scene. Eyewitnesses said that the SCDF officers continued the rescue operation until the body of the worker was recovered the next day, almost 24 hours later. The Indian worker was pronounced dead by SCDF officers at 1.40 pm the next day. The Ministry of Manpower (MOM) said its preliminary findings revealed that the workers were in the process of lowering a rebar cage when it got dislodged from its lifting chains and dragged the 30-year-old worker into a 1.5 m (4 ft 11 in) wide bore hole.


== Station layout ==


== Transport connections ==


=== Rail ===


== References ==