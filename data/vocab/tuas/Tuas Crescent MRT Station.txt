Tuas Crescent MRT Station (EW31) is an elevated Mass Rapid Transit station on the East West Line, part of the Tuas West Extension (TWE) in Singapore. This station is to provide rail connection to Tuas South and the central part of Tuas from the rest of the island.


== History ==
It was first announced on 11 January 2011 by Transport Minister Mr Raymond Lim in a speech while visiting Bedok when new platform screen doors opened there. It is expected to be completed by 2016 and benefit an estimated 100,000 commuters daily.
According to Lianhe Zaobao, this station and the tracks linking it will be built in the middle of a vehicular bridge, the first ever in Singapore.


== Station layout ==


== Transport connections ==


== References ==