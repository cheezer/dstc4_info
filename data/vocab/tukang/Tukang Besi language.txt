Tukang Besi is an Austronesian language spoken in the Tukangbesi Islands in southeast Sulawesi in Indonesia by a quarter million speakers.


== Phonology ==

The northern dialect of Tukang Besi has 25 consonant phonemes and a basic 5-vowel system. It features stress which is usually on the second-to-last syllable. The language has two implosive consonants, which are uncommon in the world's languages. The coronal plosives and /s/ have prenasalized counterparts which act as separate phonemes.
/b/ only appears in loanwords, but it contrasts with /ɓ/. [d] and [z] are not phonemic and appear only as allophones of /dʒ/ which appears only in loanwords.


== References ==