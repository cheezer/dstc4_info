Javanese cuisine is the cuisine of Javanese people, a major ethnic group in Indonesia, more precisely the province of Central Java, Yogyakarta and East Java. Though the cuisine of Sumatra is known for its spiciness with notable Indian and Arabic influences, Javanese cuisine is more indigenously developed and noted for its simplicity. Nevertheless, some of Javanese dishes demonstrate foreign influences, most notably Chinese.
Some Indonesians perceive Javanese cuisine as rather sweet compared to other Indonesian dishes, because generous amount of gula jawa (palm sugar) or kecap manis (sweet soy sauce) are ingredients favored by the Javanese. Javanese food is categorized into Central and East Javanese food; both serve simple and non-spicy food, though Central Javanese food is tends to be sweeter.
In a wider sense, Javanese cuisine might also refer to the cuisine of the whole people of Java Island, Indonesia; which also include Sundanese in West Java, Betawi people in Jakarta and Madurese on Madura Island off East Java. These ethnic groups have their own distinctive cuisines.
Javanese cuisine is largely divided into three major groups
Central Javanese cuisine
East Javanese cuisine
Common Javanese dishes
There are similarities in the cuisines but the main differences lie in the flavors. Central Javanese cuisine is sweeter and less spicy, while East Javanese cuisine uses less sugar and more chili, possibly influenced by Madurese cuisine or Arab and Indian cuisine.


== Ingredients ==

Rice is an important food crop in Java, dating back to ancient times. The Javanese is known to revere Dewi Sri as the Rice Goddess. Steamed rice is the common staple food, and is served with every meal. Tumpeng, a cone-shaped yellow rice is essential in slametan, Javanese traditional ceremonies. Rice can be processed into lontong or ketupat, or cooked in coconut milk as nasi liwet or colored with turmeric as nasi kuning (yellow rice). Other sources of carbohydrate such as gaplek (dried cassava) is sometimes mixed into rice or replaces rice. Gaplek is usually consumed by poor commoners during hard times when rice is scarce. Tubers such as yam, taro and sweet potato are consumed as snacks in between meals. Bread and grains other than rice are uncommon, although noodles and potatoes are often served as accompaniments to rice. Potatoes are often fried and mashed to be rounded, spiced and fried again coated with battered eggs as perkedel. Wheat noodles, bihun (rice vermicelli) and kwetiau are influences of Chinese cuisine. The Javanese adopted these ingredients and made them their own by adding sweet soy sauce kecap manis and local spices to create bakmi Jawa, bakmi rebus, and bihun goreng. Vegetables feature heavily in Javanese cuisine, notably in vegetable-heavy dishes such as pecel, lotek, and urap.
Coconut milk, peanut sauce, gula jawa (palm sugar), asem jawa (tamarind), petis, terasi (shrimp paste), shallot, garlic, turmeric, galangal, ginger, and chili sambal are common ingredients and spices that can be found in Javanese cuisine. Freshwater fishes such as carp, tilapia, gourami and catfish are popular, while seafoods such as tuna, red snapper, wahoo, ray, anchovy, shrimp, squid and various salted fish are popular in coastal Javanese cities. Chicken, goat meat, mutton and beef are popular meats in Javanese cuisine. Next to common farmed chicken, the ayam kampung or free-range chicken, is popular and valued for its leaner, more natural flavors. Almost 90% of Javanese are Muslim, and consequently, much of Javanese cuisine omits pork. However, in small enclaves of Catholic Javanese population around Muntilan, Magelang, Yogyakarta and Klaten, pork might be consumed. Few ethnic groups in Indonesia use pork and other sources of protein considered haram under Muslim dietary laws in their cuisine, most prominently Balinese cuisine, Indonesian Chinese cuisine, and Manado cuisine.


== Influences ==

Compared to the spicy and curry-like cuisine of Sumatra that is heavily influenced by Indian and Arabic cooking, Javanese cuisine is relatively indigenously developed, with more influences from China. However, Javanese cuisine in different areas show different foreign influences. For example, Chinese influences in Semarang, Yogyakarta and Cirebon, Indian and Arabic influences in Surabaya, Lamongan, and Gresik, and European influences in Solo and Malang.
Javanese cuisine may also have contributed and influenced foreign cuisines. Javanese botok, for example, has influenced the South African bobotie dish. Because of the dispersion of Javanese people outside of Indonesia, the Javanese cuisine has influenced the cuisine of Johor in Malaysia and the cuisine of Suriname. The influence of Javanese cuisine has contributed significantly to the food of Johor. Malaysian-Javanese dishes such as nasi ambeng, soto ayam, pecel, tempe and sambal tempe are popular in Malaysia.


== Outlets ==

Javanese households usually purchase fresh ingredients from the local market every morning, cook and serve them in the late afternoon to be mainly consumed for lunch. The leftovers are stored to be heated again for family dinner. Other than homemade family dishes, Javanese cuisine are served from humble street side carts and warungs, to fancy restaurants in five-star hotels. Small family-run warungs are the budget options for street food, serving everything from family dishes for full meals, or snack foods. The popular simple Javanese cuisine establishments are the budget food of Warung Tegal mainly established by Javanese from Tegal city, and the Angkringan street side carts in Yogyakarta and Solo that sold cheap sego kucing and various wedang (hot beverages).


== Central Javanese cuisine ==
The food in Central Java is influenced by the two ancient kingdoms of Yogyakarta and Surakarta (also commonly known as Solo). Most of Central Javanese dishes are indigenously developed, however in coastal cities such as Semarang and Pekalongan, notable Chinese influences can be seen, such as lumpia spring roll and bakmi Jawa. While in royal court of Surakarta the European influences can be seen, such as bistik Jawa and selat Solo. Many of Central Java-specific dishes contain the names of the area where the food first became popular, for example:


=== Semarang ===

Lumpia Semarang: fried or steamed spring rolls. The filling varies, but consists mainly of meat and bamboo shoots. It is served with sweet fermented soybean sauce (tauco) or sweet garlic sauce. Another accompaniment is acar (Indonesian-style sweet and sour cucumber pickle) and chili.
Soto Bangkong: a chicken soup in a small personal serving; mixed with rice, perkedel, and satay of cockles, chicken intestines, and quail eggs. Named after Bangkong crossroad in Semarang.
Nasi ayam: a dish composed of rice, chicken, egg, tofu, and served with a sweet-salty coconut milk gravy.
Wingko babat: a cake made largely of glutinous rice and desiccated coconut, toasted and sold warm.
Bandeng Juwana: processed tender boned milkfish originated from the fishing town of Juwana east of Semarang. Although originated, produced and processed in Juwana, it is largely sold in Semarang.


=== Jepara ===
Soto Jepara: Soto is a common Indonesian soup usually infused with turmeric, and can be made with chicken, beef, or mutton. The version from Jepara, a Central Javanese town, is made of chicken.
Kuluban: Kuluban is traditional salad from Jepara Regency
Kelan Antep
Horok-Horok


=== Kudus ===
Soto Kudus: Soto is a common Indonesian soup usually infused with turmeric, and can be made with chicken, beef, or mutton. The version from Kudus, a Central Javanese town, is made of chicken.
Jenang Kudus: A sweetmeat made from rice flour, palm sugar and coconut milk.


=== Yogyakarta ===

Gudeg Yogya: a traditional food from Yogyakarta and Central Java which is made from young nangka (jack fruit) boiled for several hours with palm sugar, spices, and coconut milk. This is usually accompanied by opor ayam (chicken in coconut milk), telur pindang (hard boiled egg stew), and krechek a spicy beef inner skin and tofu stew. This dish has a unique sweet and savory flavor. The Yogyakarta gudeg is drier and have reddish coloring because the addition of Javanese teak leaf.
Ayam goreng Kalasan: chicken, stewed in spices consisting of coriander, garlic, candlenut, and coconut water, then deep-fried until crispy. Served with sambal and raw vegetable salad.
Bakpia: a sweet pastry filled with sugared mung bean paste. Influenced by Chinese pastry. The famous bakpia producing area is the Pathok area near Malioboro, thus the famous bakpia is bakpia Pathok.
Ronde (wedhang ronde): a hot Javanese dessert containing glutinous rice balls stuffed with peanut paste, floating in a hot and sweet ginger and lemongrass tea. Influence by the Chinese sweet dumpling tangyuan.
Angsle (wedhang angsle): a hot soupy dessert of sago pearls, pre-cooked glutinous rice and mung beans, putu mayang (brightly colored, noodle-shaped flour cakes), fried peanuts all drowned in hot, sweet coconut milk.
Kipo: the name derived from Javanese word iki opo? (what is this?), a small sweet snack from Kotagede. It is a dough made of glutinous rice flour and coconut milk, filled with grated coconut and palm sugar.


=== Solo ===

Nasi liwet: a rice dish cooked in coconut milk and chicken broth, served with meat and vegetable side dishes.
Tongseng: a strongly spiced curry of bone-in mutton, which is quickly stir-fried at the point of sale with vegetables added.
Tengkleng: goat ribs and offal in a curry-like soup, similar to gule kambing, but with a lighter and thinner soup.
Timlo Solo: a beef and vegetable soup. Some versions also have noodles.
Bakso Solo: bakso literally means meatballs, made of beef, and served in boiling hot soup with mung bean-thread noodles, green vegetables, shredded cabbage, and various sauces (chili, tomato). This version from Solo has super-sized meatballs, the size of tennis balls. Also known as Bakso Tenis. Bakso is a Chinese-influenced dish, but has become a popular snack throughout Indonesia.
Bistik Jawa: Javanese beef steak, a European-influenced dish from Solo
Selat solo: a salad consisting of stewed beef, lettuce, carrot, green bean, potato chips or French fries in sweet spiced dressing.
Sate buntel (lit: wrapped satay): Minced fatty beef or goat meat, encased in caul fat and wrapped around a bamboo skewer then grilled. The size of this satay is quite large, very similar to a Middle Eastern kebab. After being grilled on charcoal, the meat is separated from the skewer, cut into bite-size chunks, then served in sweet soy sauce and merica (pepper).
Srabi Solo: a pancake made of coconut milk, mixed with a little rice flour as thickener. Srabi can be served plain, or with toppings such as sliced banana, chopped jackfruit, chocolate sprinkle (muisjes), or cheese.


=== Banyumas ===

Refers to Javanese cultural region of Western Central Java bordering West Java, including Banyumas, Tegal, Brebes, Cilacap, Kebumen and Purwokerto regencies.
Nasi Bogana: Tegal: a steamed rice dish wrapped in banana leaves and served with a variety of side dishes.
Teh poci Tegal: tea brewed in a clay teapot, served with rock sugar. Tegal, a Central Java town, is a major producer of high-quality tea.
Sate Tegal or Sate Balibul: juvenile (five-month-old) goat satay from Tegal, noted for its tender meat.
Sate Ambal: a satay variant from Ambal, Kebumen, Central Java. This satay uses free-range chicken, ayam kampung. The sauce is not made from peanuts, but rather ground tempe, chilli and spices. The chicken is marinated for about two hours to make the meat tastier. This satay is served with ketupat.
Tempe mendoan: fried battered tempe from Banyumas.
Sroto Sokaraja: a variant of soto from Sokaraja, Banyumas.


=== Other Central Javanese cuisine ===
Sate Blora: Chicken satay from Blora area.
Swikee Purwodadi: Frogs' legs cooked in fermented soybean (tauco) soup.
Madu mongso: a sweetmeat made from fermented black glutinous rice, cooked in coconut milk and sugar. It is sticky and very sweet, and comes wrapped in corn husk.
Bakmoy: small cubes of fried tofu, chicken and boiled egg served with chicken broth and relish made from sweet soy sauce.
Mie koclok: a noodle soup from Cirebon.


== East Javanese cuisine ==
The East Javanese cuisine is largely influenced by Madurese cuisine - Madura being a major producer of salt, hence the omission of sugar in many dishes. Many of the East Javanese dishes are also typically Madurese, such as Soto Madura and Sate Madura, usually sold by Madurese settlers. Notable Arabic and Indian cuisine influence also can be found such in the coastal cities of Tuban, Gresik, Surabaya, Lamongan and Sidoarjo, due to the large number of Arabic descendants in these cities. Although there are many dishes with town names attached to them, local versions of these are available in every town. The most popular town-associated dishes are:


=== Madiun ===

Pecel Madiun: A salad of boiled vegetables, dressed in a peanut-based spicy sauce. It is usually served as an accompaniment to rice. A peanut or dried fish/shrimp cracker (rempeyek) is served on the side. Not to be confused with pecel lele, which is deep-fried local catfish served with sambal.
Brem Madiun: Fermented sugar and cassava cakes.


=== Lamongan ===
Soto Lamongan: Chicken soup originated from the town of Lamongan.
Ayam penyet: fried chicken (see ayam goreng), lightly smashed using a pestle in a mortar laced with sambal.
Bebek goreng: deep fried duck, similar to duck confit.
Pecel lele: deep fried catfish with sambal, vegetables and rice


=== Surabaya ===

Rawon: a dark beef soup, served with mung bean sprouts and the ubiquitous sambal. The dark (almost black) color comes from the kluwak (Pangium edule) nuts.
Rujak Cingur, a marinated cow snout or lips and noses (cingur), served with boiled vegetables and shrimp crackers. It is then dressed in a sauce made of caramelized fermented shrimp paste (petis), peanuts, chili, and spices. It is usually served with lontong, a boiled rice cake. Rujak Cingur is considered traditional food of Surabaya in East Java.
Lontong kupang: lontong with small cockles in petis sauce.
Semanggi: A salad made of boiled semanggi (M. crenata) leaves that grow in paddy fields. It is dressed in a spicy peanut sauce.


=== Madura ===
Soto Madura: a turmeric-based beef and offal soup, served with boiled egg slices, and sambal.
Sate Madura: originating on the island of Madura, near Java, is a famous variant among Indonesians. Most often made from mutton or chicken, the recipe's main characteristic is the black sauce made from Indonesian sweet soy sauce/kecap manis mixed with palm sugar (called gula jawa or "Javanese sugar" in Indonesia), garlic, deep fried shallots, peanut paste, petis (a kind of shrimp paste), candlenut, and salt. Chicken Madura satay is usually served in peanut sauce, while the mutton Madura satay is usually served in sweet soy sauce. Sate Madura uses smaller chunks of meat than other variants. It is eaten with rice or rice cakes wrapped in banana/coconut leaves (lontong/ketupat). Raw thinly sliced shallots and plain sambal are often served as condiments.


=== Malang ===

Bakso Malang : bakso literally means meatball. Bakso Malang has more accompaniments, beside the meatball (mostly beef) itself. For example, offal, siomay dumplings (fried or steamed), tahu (tofu, fried or steamed, filled with meat), soun (mung bean threads), and yellow egg noodles. All of these are served in hot beef stock.
Cwie mie: a Chinese-influenced noodle dish, containing boiled and seasoned noodles, topped with pre-cooked minced meat (usually pork or chicken) and boiled wonton. Similar to the Chinese zhajiang mian.


=== Other East Javanese cuisine ===
Sate Ponorogo: a variant of satay originating in Ponorogo, a town in East Java. It is made from sliced marinated chicken, served with a sauce made of peanuts and chilli sauce and garnished with shredded shallots, sambal, and lime juice. This variant is unique for the fact that each skewer contains one large piece of chicken, rather than several small cubes. The meat is marinated in spices and sweet soy sauce, in a process called "bacem" and is served with rice or lontong (rice cake). The grill is made from terracotta earthenware with a hole on one side to allow ventilation for the coals. After three months of use, the earthenware grill disintegrates, and must be replaced.
Lontong balap: literally means "racing rice cake", which is a dish of rice cakes, fried tofu, and beansprouts, doused in kecap manis and sambal sauce. In the past, lontong balap hawkers carried their wares in a large, heavy metal urn. The heaviness caused them to have to walk really quickly while carrying it, so they looked like they were "racing".
Tahu campur: a beef and offal soup, mixed with fresh vegetables, potatoes, rice cake, and tofu. The secret ingredient is the caramelized fermented shrimp paste (petis) which is mixed in just before serving.
Tahu tek-tek: a dish containing cut-up fried tofu, boiled vegetables (mostly beansprouts), potatoes, drenched in a peanut-based sauce. The sauce has caramelized fermented shrimp pasted (petis), chili, and garlic.


== Common Javanese dishes ==

These are the common Javanese dishes, which can be found throughout Java regardless of the location.
Sate: skewered grilled meat is a common dish in Java. The Javanese variants are Sate Tegal, Sate Ambal, Sate Solo, Sate Buntel, Sate Madura, Sate Ponorogo, etc.
Soto: this Indonesian soup dish is also a common dish in Java. The Javanese variants are common Soto ayam and Soto babat (tripe), Soto Kudus, Soto Madura, Soto Lamongan etc.
Tumpeng: a rice served in the shape of a conical volcano, usually with rice colored yellow using turmeric. It is an important part of ceremony in Java. Tumpeng served in important events such as birthday, moving house, or other ceremonies. Traditionally, Tumpeng is served alongside fried chicken, boiled egg, vegetables, goat meat on a round plate made from bamboo called besek.
Tempe: a meat substitute made from soy bean fermented with mold. It is a staple source of protein in Java and popular in the world as an excellent meat substitute for vegetarians.
Kripik tempe: tempe chips, made from thinly sliced, lightly battered, then deep fried tempeh (soybean cake).
Gorengan: assorted fritters such as tempeh, tofu, yam, sweet potato, cassava, and chopped vegetables.
Pecel: a type of peanut sauce with chili is a common ingredients in Javanese cuisine. It is used in various type of Rujak and Gado-gado. It can also be used as stand alone sauce with rice and prawn, egg and vegetables as Nasi Pecel (Pecel rice).
Urap sayur: Vegetables in spiced grated coconut dressing.
Lotek: Almost identical to Gado-gado, but sweeter. It is similar to pecel, but includes different vegetables as well as boiled egg slices and a garnish of fish or shrimp crackers and emping (Gnetum gnemon L. nut, flattened, dried, and fried into small thin crackers).
Sayur Lodeh: assorted vegetables, stewed in coconut milk.
Buntil: a traditional Javanese dish of scraped coconut meat mixed with teri (anchovies) and spices, wrapped in a papaya leaf, then boiled in coconut milk.
Botok: a dish made from shredded coconut flesh which has been squeezed of its coconut milk, often mixed with other ingredients such as vegetable or fish, and wrapped in banana leaf and steamed.
Nasi rames: Rice with accompaniments, usually some curried vegetable stew (sayur lodeh), a selection of cooked fish or chicken or meat and offal pieces, and a dollop of spicy sambal.
Nasi kuning is similar to nasi rames or nasi campur, but the rice is cooked in coconut milk and colored bright yellow using turmeric and scented with lemongrass and kaffir lime leaves.
Gule kambing: mutton cooked in a curry-like coconut milk soup.
Sop Buntut: Oxtail soup.
Tumis sayuran: Stir-fried vegetables, usually mixed with chili and a spice paste.
Klepon: A glutinous rice ball stuffed with palm sugar, colored green using pandanus leaf, and rolled in fresh grated coconut.
Jajan pasar: Several types of shaped and colored flour, rice flour, and glutinous rice flour cakes, sprinkled with desiccated coconut and drizzled with melted palm sugar. Jajan literally means snack, and pasar means market, as this snack is usually only found in traditional markets.


== Notes ==