Nasi Bogana or Nasi Begana, pronounced as Nah-see Boh-gâna, is an Indonesian style rice dish, originally from Tegal, Central Java. It is usually wrapped in banana leaves and served with side dishes.
This rice dish is a type of Nasi Rames or Nasi Campur — terms used for dishes that have rice and a variety of side dishes.
Nasi Bogana is very popular in Indonesia and is sold all over the streets of Jakarta, the capital city, for 12 to 20 Rupiah each. It is sold in almost all Sundanese or Javanese restaurants and sometimes in Warungs or Wartegs (Warung Tegal), a traditional outdoor restaurant or café. It is considered a convenient dish as it is wrapped in banana leaves and is usually ready to bring and eat anytime. It is a type of fast food that is brought to workplaces to eat.


== Preparation ==
Nasi Bogana is prepared by spreading a wide banana leaf and filling it with steamed rice. Then seasoning such as fried shallots are put on top of the rice. Over the rice, a smaller banana leaf is spread and the side dishes — opor ayam (white chicken curry), dendeng (shredded meat), fried chicken liver and gizzard in chili and coconut gravy, sambal of shredded red chili, telur pindang whole boiled eggs, serundeng (fried shredded spiced coconut with peanuts), sautéed tempeh or sautéed string beans — are decoratively placed. All ingredients are then wrapped and closed with the outer banana leaf that is placed over the rice. Plastic strings are used to tighten the pack together. It is put in a steamer to keep it warm and is ready to eat at anytime.
Other times they are prepared as a regular rice dish without the banana leaves because the dish has been too common and restaurants do not follow the traditions.


== Preparation of side dishes ==
The opor ayam is usually served as pieces without their bones. This is usually done because they can wrap it in banana leaves easier without taking too much space. There are two types of opor ayam: white gravy, commonly used in West Java, and yellow gravy, commonly used in Central Java. Both are sweet but yellow opor ayam tends to have curry spices in it.
The Dendeng is sometimes put in a stick and eaten the same way as a satay (meat in skewer). The telur pindang boiled eggs are most of the time cut in half and only half is served. This depends on the occasion. Serundeng, fried chicken liver and gizzard in chili and coconut gravy (suggested to use cow livers rather than chicken to avoid the smell), Sambal of shredded red chili and sautéed tempeh and sautéed string beans is served regularly, a spoon-full of each circling the steamed rice.


== Tradition and culture ==
In Java, Nasi Bogana is often used in special occasions, such as weddings and anniversaries, but is most commonly found in family gatherings and social gatherings (Arisan). At weddings, Nasi Bogana usually has its own booth where people can choose their own side dishes and sauces. Most people prefer Nasi Bogana to be eaten with kerupuk (Indonesian flour crackers) or emping (crushed bean crackers from melinjo), and as a result it becomes a part of the side dish. Some people like additional sauce like Kecap Manis (sweet soybean sauce) and sambal terasi (fish and shrimp chili sauce). The drink that they have while having this dish is most of the time hot or iced black tea.


== Nutrition ==
Nasi Bogana contains amounts of protein and carbohydrates from the meat and rice, but also contains a lot of fat and oil since it uses coconut and palm oils in the side dishes. Almost all of the side dishes are prepared by frying.
The dish ranges from 1000-1600 calories per serving. It is comparable with fast food.


== Other types of Nasi Uduk or Nasi Rames ==
Nasi campur
Nasi Megono
Nasi Langgi
Nasi Kapau
Nasi liwet
Nasi kuning
Nasi Campur Bali
Nasi Krawu
Nasi lemak
Nasi Gudeg
Nasi jamblang
Nasi kebuli
Nasi Padang
Nasi pecel
Nasi timbel
Nasi ulam
Nasi uduk


== References ==


== See also ==

Cuisine of Indonesia
List of Indonesian cuisine
Javanese cuisine
Tegal
Jawa Tengah
Nasi Campur
Nasi lemak