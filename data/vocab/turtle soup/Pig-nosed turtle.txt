The pig-nosed turtle (Carettochelys insculpta), also known as the pitted-shelled turtle or Fly River turtle, is a species of turtle native to northern Australia and southern New Guinea.


== Systematics ==
This species is the only living member of the genus Carettochelys, the subfamily Carettochelyinae and the family Carettochelyidae, though several extinct carettochelyid species have been described from around the world. Some literature claims two subspecies, but a recent paper rejects this.


== Description ==

The pig-nosed turtle is unlike any other species of freshwater turtle. The feet are flippers, resembling those of marine turtles. The nose looks like that of a pig, having the nostrils at the end of a fleshy snout, hence the common name. The carapace is typically grey or olive, with a leathery texture, while the plastron is cream-coloured. Males can be distinguished from females by their longer and narrower tails. Pig-nosed turtles can grow to about 70 cm (28 in) shell-length, with a weight of over 20 kg (44 lb).

Unlike the soft shelled turtles of the family Trionychidae, pig-nosed turtles retain a domed bony carapace beneath their leathery skin, rather than a flat plate. They also retain a solid plastron, connected to the carapace by a strong bony bridge, rather than the soft margin of the trionychids.


== Behavior ==
Pig-nosed turtles are not completely aquatic. Little is known about general behaviour, as there have been few studies in the wild. Their known extreme aggression in captivity suggests the species is markedly more territorial than most other turtles and tortoises. They seem to display a degree of social structure during the cooler dry season around the hydrothermal vents that line some river systems they inhabit.


=== Feeding ===
The species is omnivorous, eating a wide variety of plant and animal matter, including the fruit and leaves of figs, as well as crustaceans, molluscs and insects.


=== Breeding ===
Females reach maturity after 18 or more years old, and males around 16 years. They lay their eggs late in the dry season on sandy river banks. When the offspring are fully developed, they will stay inside the eggs in hibernation until conditions are suitable for emergence. Hatching may be triggered when the eggs have been flooded with water or by a sudden drop in air pressure signaling an approaching storm.
Using environmental triggers, along with vibrations created by other hatching turtles in the same clutch, gives a better chance for survival. Using a universal trigger rather than simply waiting for incubation to finish means they all hatch at the same time. This provides safety in numbers; also, the more turtles that hatch, the more help they have to dig through the sand to the surface.


== Distribution and habitat ==
The turtle is native to freshwater streams, lagoons and rivers of the Northern Territory of Australia, as well as to the island of New Guinea, where it is believed to occur in all the larger, and some smaller, southward-flowing rivers.


== Status and conservation ==
The species experienced a population decline of more than 50% in the thirty years between 1981 and 2011. Although the turtles are protected in Indonesia under Law No. 5/1990 on Natural Resources and Ecosystems Conservation, smuggling occurs. Some 11,000 turtles captured from smugglers were released into their habitats in the Wania River, Papua Province, Indonesia, on 30 December 2010. In March 2009, more than 10,000 turtles retrieved from smugglers were also released into the Otakwa River in Lorentz National Park. 687 pig-nosed turtles were seized at an Indonesian airport in March 2013. They were reportedly destined for Hong Kong.


== Captive care ==

Pig-nosed turtles have become available through the exotic pet trade, with a few instances of captive breeding. While juveniles are small and grow slowly, their high cost and large potential size makes them suitable only for experienced aquatic turtle keepers. They tend to be shy and prone to stress. They get sick easily, which can cause problems with their feeding, but they are known to eat commercially available processed turtle pellets or trout chow, as well as various fruits and vegetables. Breeding is rarely an option to the hobbyist, as adults are highly aggressive and will attack each other in all but the largest enclosures.


== References ==

Species Carettochelys insculpta at The Reptile Database
IUCN Red list of Threatened Species: Carettochelys insculpta


== External links ==
Gondwanan Turtle Information
Australian Dept of the Environment website on Pig-nosed Turtle Accessed 10 July 2007
Carettochelyidae (all species) at The Reptile Database
The Pig-nosed Turtle, Carettochelys insculpta at California Turtle & Tortoise Club (CTTC)