Germany had commissioned over 1,500 U-boats (German: Unterseeboot) into its various navies from 1906 to the present day. The submarines have usually been designated with a U followed by a number, although World War I coastal submarines and coastal minelaying submarines used the UB and UC prefixes, respectively. When Germany resumed building submarines in the 1930s, the numbering of the submarines was restarted at 1. The renumbering was restarted at 1 a third time when Germany resumed building submarines in the 1960s.


== World War I–era U-boats ==
There were some 380 U-boats commissioned into the Kaiserliche Marine in the years before and during World War I. Although the first four German U-boats—U-1, U-2, U-3, U-4—were commissioned before 1910, all four served in a training capacity during the war. German U-boats used during World War I were divided into three series. The U designation was generally reserved for ocean-going attack torpedo U-boats. The UB designation was used for coastal attack U-boats, while the UC designation was reserved for coastal minelaying U-boats.


=== U-boats ===
U-boats designed primarily for deep water service were designated with a U prefix and numbered up to 167.


==== U-1 to U-100 ====


==== U-101 to U-167 ====


=== UB coastal U-boats ===
Coastal attack torpedo U-boats were smaller craft intended for operation closer to land. They were designated with a UB prefix and numbered up to 154.


==== UB-1 to UB-100 ====


==== UB-101 to UB-154 ====


=== UC coastal minelaying U-boats ===
Coastal minelaying U-boats were smaller vessels intended to mine enemy harbors and approaches. They were designated with a UC prefix and numbered up to 105.


==== UC-1 to UC-105 ====


=== Foreign U-boats ===
At the outbreak of World war I Germany took charge of a number of submarines under construction in German shipyards for other countries.
SM UA (ex-Norwegian A class submarine A-5)
U-66 to U-70 (ex-Austro-Hungarian U-7 class U-7 to U-11)


== World War II U-boats ==
In the World War II era, Germany commissioned some 1,250 U-boats into the Kriegsmarine,


=== U-1 to U-100 ===


=== U-101 to U-200 ===


=== U-201 to U-300 ===


=== U-301 to U-400 ===


=== U-401 to U-500 ===


=== U-501 to U-600 ===


=== U-601 to U-700 ===


=== U-701 to U-800 ===


=== U-801 to U-900 ===


=== U-901 to U-1000 ===


=== U-1001 to U-2000 ===


=== U-2001 to U-3000 ===


=== U-3001 and up ===


=== Foreign U-boats ===

Germany captured and commissioned 14 submarines from six countries into the Kriegsmarine during World War II.


== Post–World War II U-boats ==


== See also ==
List of U-boats never deployed
List of German U-boat WW2 Raiding Careers
List of naval ships of Germany
Category:Type II U-boats
Category:Type IX U-boats
Category:Type VII U-boats
Category:Type XIV U-boats
Category:Type XXI U-boats


== External links ==
List of U-boats at U-boat.net
U-35, 1936–1939