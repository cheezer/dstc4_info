Ulu Island is the second largest island in the Duke of York island group. It is situated between Duke of York Island and Kabakon and Kerawara islands.
The island is on Papua New Guinea Time (PGT).  This works out as UTC/GMT +10:00 hours. They do not observe Daylight saving time.
The word Ulu means breadfruit.


== Other Names ==
Ulu is also known as Mauke Island or Pig Island


== References ==