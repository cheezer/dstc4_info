The Australian Underwater Federation (AUF) is the governing body for underwater sports in Australia.


== Mission ==
The mission of the AUF is to:

Bringing Sport, Conservation and Awareness to the Underwater World.


== Organisation ==
The AUF is a membership-based organisation whose day-to-day operations are overseen by a federal board and by a number of committees (known as commissions) for following activities - finswimming (commission known as Ozfin Inc.), scuba, snorkel, spearfishing and underwater hockey (commission known as Underwater Hockey Australia). It also currently has state branches in New South Wales (incorporated as the Underwater Skindivers & Fishermen's Association Inc) and Queensland, and state commissions for finswimming and underwater hockey in most states.


== Recognition ==
The AUF is recognised by the Australian Sports Commission as the national sporting organisation (NSO) for underwater sports in Australia. It is the Australian representative to Confédération Mondiale des Activités Subaquatiques (CMAS), with full voting rights to the Sports and Technical Committees and non-voting rights to the Scientific Committee. The AUF is a member of the World AquaChallenge Association (WAA) and Recfish Australia. It is also one of the organisations represented on the Standards Australia's Committee CS/83, Recreational Underwater Diving.


== Underwater sports ==
The AUF is the governing body for the following underwater sports within Australia: spearfishing, underwater hockey, finswimming and underwater rugby.


== Diver training ==
Historically, the AUF operated as a diver training organisation offering instructor training and certification, and recreational diver certification in both snorkel and scuba diving. It currently issues CMAS International Diving certificates in its capacity as a member of the CMAS Technical Committee in respect to its own training programs and those offered by the Federation of Australian Underwater Instructors (FAUI) and the now-defunct Australian branch of NAUI.
AUF currently offers training in snorkelling (including breath-hold technique) for open water and pool environments, and in coaching levels accredited with the Australian Government’s National Coaching Accreditation Scheme (NCAS) for three sports. Originally launched in 1985 under the name of the School Snorkelling Programme, the openwater training stream (known as Ocean) supports both recreational diving as well as the sports of spearfishing and photofishing (a breath-hold version of the sport of underwater photography offered by CMAS) while the pool stream is intended to develop proficiencies in finswimming and underwater hockey. The following instructional levels are currently offered - Finswimming Coach Level 1 and 2, Ocean Coach level 1 and 2, and Underwater Hockey Coach Level 1 and 2.


== See also ==
Ron and Valerie Taylor


== References ==


== External links ==
Official AUF website
Australian Underwater Federation - Queensland Inc. homepage
Underwater Skindivers & Fishermen's Association Inc. homepage
Underwater Hockey Australian Commission website
Welcome to Ozfin - Australian Fin Swimming
AUF Spearfishing homepage