Universal Republic Records was an American record label, that operated as a division of Universal Music Group. It was based on the then-defunct Republic Records label founded by brothers Monte and Avery Lipman. In the summer of 2011, changes were made at one of the three Universal Music Group umbrella labels, Universal Motown Republic Group, and Motown Records was separated from Universal Motown Records (causing it to shut down and transfer its artists to either Motown Records or Universal Republic Records) and the umbrella label and merged into The Island Def Jam Music Group, making Universal Republic Records a stand alone label and shutting down Universal Motown Republic Group. In mid-2012, the label reverted to its original Republic Records name, making this label defunct.


== Universal Records: 2000–2005 ==
Universal Music Group acquired the Lipman brothers' Republic Records as a fully owned subsidiary in 2000, and named Monte Lipman as President of the newly established Universal Records label, and Avery Lipman the COO; both reporting to Doug Morris (Chairman of Universal Music Group), and Mel Lewinter, (Chairman of Universal Records Group).


== Universal Republic Records: 2006–2012 ==
As of 2006, Universal Republic has adopted an A&R and partnership-based growth strategy. In an era of music industry transition they have proven effective, as Universal Republic has continued to grow in the industry's declining years. Universal Republic become a stand-alone label after Universal Motown Republic Group was shut down in summer 2011. One year later, it was renamed back to Republic Records.


=== Notable artists ===
In chronological order are current Republic artists:
2006: Hinder.
2007: Amy Winehouse and Colbie Caillat
2009: Steel Panther
2009: Owl City
2010: Kelly Rowland
2010: Veronica V
2010: Florence + the Machine & Enrique Iglesias
2011: Ariana Grande signed with Universal Republic in August 2011.
2012: The Weeknd signed with Universal Republic in September 2012 in a joint venture with his own imprint label XO.
2012: Austin Mahone
2012: Lisa Marie Presley Republic Nashville's The Band Perry, and James Blake.


== References ==


== External links ==
Universal Republic Records discography at Discogs