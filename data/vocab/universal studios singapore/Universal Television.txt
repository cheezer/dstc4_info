Universal Television is the television production subsidiary of the NBCUniversal Television Group and, by extension, the production arm of the NBC television network (since a majority of the company's shows air on NBC, and accounts for most of that network's prime time programming). It was formerly known as Revue Studios, MCA/Universal, NBC Studios, NBC Universal Television Studio, and Universal Media Studios. Both NBC Studios and Universal Network Television are predecessors of Universal Media Studios.


== Predecessors ==


=== Revue Studios ===
Revue Productions (also known as Revue Studios) was founded in 1943 by MCA to produce live radio shows and also produced "Stage Door Canteen" live events for the USO during World War II. Revue was re-launched as MCA's television production subsidiary in 1950. The partnership of NBC and Revue extends as far back as September 6, 1950, with the television broadcast of Armour Theatre, based on radio's Stars Over Hollywood. MCA bought the Universal Studios lot in 1958 and was renamed Revue Studios. Following the acquisition of Decca Records, the then-parent of Universal Pictures, the studio backlot name was changed back to Universal. In 1964, MCA formed Universal City Studios to merge the Motion Picture and Television arms of both Universal Pictures and Revue Productions, and Revue was officially renamed Universal Television in 1966.
During the early years of television, Revue was responsible for producing and/or distributing many television classics. The most noteworthy included Leave It to Beaver, which ran for only one season on CBS before going to ABC from 1958 until 1963. In addition, Revue also made Alan Hale, Jr.'s Biff Baker, U.S.A. (1952–1953) and all three of Rod Cameron's syndicated series, City Detective (1953–1955), State Trooper (1956–1959), and COronado 9 (1960–1961) and the Bill Williams western series, The Adventures of Kit Carson (1951–1955). It produced Bachelor Father (1957–1962), for "Bachelor Productions", Edmond O'Brien's syndicated crime drama Johnny Midnight, based on a fictitious New York City actor-turned-private detective. Another of its offerings was the 52-episode Crusader, the first Brian Keith series, which ran on CBS 1955–1956. Also McHale's Navy was produced by Revue from 1962 to 1966.
In December 1958 MCA/Revue purchased Universal Studios's 367 acre backlot to produce television series, then leased it back to Universal for a million dollars a year for a decade.
Revue also produced later seasons of The Jack Benny Program for CBS and NBC and in co-operation with Jack Benny's J and M productions Checkmate, General Electric Theater and Alfred Hitchcock Presents for CBS, Studio 57 for DuMont, and westerns such as Tales of Wells Fargo, The Restless Gun and Laramie for NBC, as well as Wagon Train for NBC and ABC, and the first two seasons of NBC's The Virginian, based on a film released originally by Paramount Pictures, whose pre-1950 theatrical sound feature film library was sold to MCA in 1957. Wagon Train was the only Revue-produced TV show ever to finish an American television season in first place.


=== Universal Television ===
The first incarnation of Universal Television was reincorporated from Revue Productions in 1966, 4 years after MCA bought Universal Pictures and its then-current parent Decca Records. Among their many contributions to television programming included production of the first motion picture made exclusively for television (See How They Run from 1964), the first series with revolving stars (Name of the Game from 1968), the first rotating series with an umbrella title (1969's The Bold Ones) and the first two-part television movie (Vanished from 1971). Uni TV (also commonly known as MCA/Universal) also co-produced many shows with Jack Webb's Mark VII Limited such as Adam-12 and a revival of the 1951 series Dragnet. During the 1970s and 1980s, Uni TV produced shows such as The Rockford Files, Murder, She Wrote, and Adam-12, which received critical acclaim and several TV movie spin-offs after their cancellations.
In 1990, MCA/Uni TV began the Law & Order franchise. In 1996, MCA was reincorporated as Universal Studios. The same time around, Universal was acquired by Joseph A. Seagram and Sons and later acquired the USA Networks and Multimedia Entertainment. In 1997, After the breakup of the United International Pictures TV arm. the company formed Universal Worldwide Television. In 1998, Universal sold off its USA Networks and Universal Television to Barry Diller and renamed it Studios USA.
In 1999, Seagram bought PolyGram, which included PolyGram Television and the post-1996 film library (plus some of the pre-1996 films). The deal closed in 2000 and quickly adapted PolyGram to the Universal name. Vivendi Universal acquired Studios USA and made Diller as CEO of VU Entertainment fully reforming Universal Television.
On August 2, 2004, GE formed NBC Universal Television.


=== NBC Productions ===
NBC Productions was founded in 1947 by RCA (NBC's former parent company). In 1996, the company was renamed NBC Studios. In 2004, NBC Studios was merged with Universal Network Television to form NBC Universal Television Studio.


=== MCA Television ===
MCA TV (also known as MCA Television Enterprises) was founded in 1951, several years before parent MCA's purchase of the US branch of Decca Records (in 1959) and Universal Pictures (in 1962). For more than four decades, it was one of the most active producers of television programming. MCA TV's other television divisions included Universal Television and MCA Television Entertainment.
In 1996 MCA TV was renamed to Universal Television Enterprises.
MCA Television Entertainment (commonly known as MTE) was formed in 1987. Like MCA TV, in 1996 it was renamed Universal Television Entertainment.
EMKA, Ltd. is the holding company responsible for a majority of the pre-1950 Paramount Pictures sound library. As an official part of the Universal Pictures library, they are part of the company's television unit, Universal Television.


=== Studios USA ===
Studios USA was formed by Barry Diller when he bought Universal Television and the USA Networks from Seagram's Universal Studios in 1998. It produced and distributed talk shows. It also produced shows formerly from Universal Television. The company also formed USA Films and USA Home Entertainment. In 2002 Vivendi Universal acquired Studios USA and the rest of USA's non-shopping (film and TV) assets, and reverted the remaining series produced and/or distributed by Studios USA to Universal Television.


== History ==
NBC Universal Television Studio was formed in 2004 from NBC Studios and Universal Network Television after NBC and Universal merged. On June 14, 2007, NBC Universal Television Studio was renamed Universal Media Studios (UMS) as the unit would be also developing entertainment for the web.
On July 21, 2009, Universal Cable Productions was split off from UMS and placed into NBCUniversal's NBCU Cable Entertainment division. On September 12, 2011, Universal Media Studios was renamed to Universal Television.


== Currently produced ==


== Future productions ==
The Carmichael Show
Mr. Robinson
Uncle Buck


== Formerly produced ==


== References ==


== External links ==
Universal TV at nbcuni.com
Universal TV at NBCU Media Village