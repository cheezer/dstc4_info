Upper Volta (now named Burkina Faso) may refer to:
French Upper Volta (1919–1932, 1947–1958)
a territory in French West Africa (1919–1932)
a territory of the French Union (1947–1958)

Republic of Upper Volta (1958–1984)
a self-governing republic of the French Community (1958–1960)
an independent republic (1960–1984)