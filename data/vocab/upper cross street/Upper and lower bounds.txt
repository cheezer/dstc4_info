In mathematics, especially in order theory, a upper bound of a subset S of some partially ordered set (K, ≤) is an element of K which is greater than or equal to every element of S. The term lower bound is defined dually as an element of K which is less than or equal to every element of S. A set with a upper bound is said to be bounded from above by that bound, a set with a lower bound is said to be bounded from below by that bound. The terms bounded above (bounded below) are also used in the mathematical literature for sets that have upper (respectively lower) bounds.


== Properties ==
A subset S of a partially ordered set P may fail to have any bounds or may have many different upper and lower bounds. By transitivity, any element greater than or equal to a upper bound of S is again a upper bound of S, and any element less than or equal to any lower bound of S is again a lower bound of S. This leads to the consideration of least upper bounds (or suprema) and greatest lower bounds (or infima).
The bounds of a subset S of a partially ordered set K may or may not be elements of S itself. If S contains a upper bound then that upper bound is unique and is called the greatest element of S. The greatest element of S (if it exists) is also the least upper bound of S. A special situation does occur when a subset is equal to the set of lower bounds of its own set of upper bounds. This observation leads to the definition of Dedekind cuts.
The empty subset ∅ of a partially ordered set K is conventionally considered to be both bounded from above and bounded from below with every element of P being both a upper and lower bound of ∅.


== Examples ==
5 is a lower bound for the set { 5, 10, 34, 13934 }, but 8 is not. 42 is both a upper and a lower bound for the set { 42 }; all other numbers are either a upper bound or a lower bound for that set.
Every subset of the natural numbers has a lower bound, since the natural numbers have a least element (0, or 1 depending on the exact definition of natural numbers). An infinite subset of the natural numbers cannot be bounded from above. An infinite subset of the integers may be bounded from below or bounded from above, but not both. An infinite subset of the rational numbers may or may not be bounded from below and may or may not be bounded from above.
Every finite subset of a non-empty totally ordered set has both upper and lower bounds.


== Bounds of functions ==
The definitions can be generalized to functions and even sets of functions.
Given a function f with domain D and a partially ordered set (K, ≤) as codomain, an element y of K is a upper bound of f if y ≥ f(x) for each x in D. The upper bound is called sharp if equality holds for at least one value of x.
Function g defined on domain D and having the same codomain (K, ≤) is a upper bound of f if g(x) ≥ f(x) for each x in D.
Function g is further said to be a upper bound of a set of functions if it is an upper bound of each function in that set.
The notion of lower bound for (sets of) functions is defined analogously, with ≤ replacing ≥.


== Tight bounds ==
A upper bound is said to be a tight upper bound, a least upper bound, or a supremum if no smaller value is an upper bound. Similarly a lower bound is said to be a tight lower bound, a greatest lower bound, or an infimum if no greater value is a lower bound.


== References ==