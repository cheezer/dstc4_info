Toome Upper is a barony in County Antrim, Northern Ireland. To its south lies Lough Neagh, and it is bordered by five other baronies: Toome Lower to the north; Antrim Lower to the north-east; Antrim Upper to the east; Massereene Lower to the south-east; and Loughinsholin to the south-east. Toome Upper also formed part of the medieval territories known as the Route and Clandeboye.


== History ==


== List of settlements ==
Below is a list of settlements in Toome Upper:


=== Towns ===
Ahoghill (also part of baronies of Toome Lower and Antrim Lower)
Antrim (also part of barony of Toome Lower)
Milltown
Randalstown


=== Population centres ===
Crosskeys
Toome
Newferry
Whiteside's Corner


== List of civil parishes ==
Below is a list of civil parishes in Toome Upper:
Antrim (split with barony of Antrim Upper)
Ballyscullion (split with barony of Loughinsholin)
Cranfield
Drummaul
Duneane
Grange of Ballyscullion
Grange of Shilvodan


== References ==