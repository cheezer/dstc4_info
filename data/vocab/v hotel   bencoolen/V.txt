V (named *vepe /ˈviː/,) is the 22nd letter in the modern English alphabet and the ISO basic Latin alphabet.


== History ==

The letter V comes from the Semitic letter Waw, as do the modern letters F, U, W, and Y. See F for details.
In Greek, the letter upsilon 'Υ' was adapted from waw to represent, at first, the vowel [u] as in "moon". This was later fronted to [y], the front rounded vowel spelled 'ü' in German.
In Latin, a stemless variant shape of the upsilon was borrowed in early times as V—either directly from the Western Greek alphabet or from the Etruscan alphabet as an intermediary—to represent the same /u/ sound, as well as the consonantal /w/. Thus, 'num' — originally spelled 'NVM' — was pronounced /num/ and 'via' was pronounced [ˈwia]. From the 1st century AD on, depending on Vulgar Latin dialect, consonantal /w/ developed into /β/ (kept in Spanish), then later to /v/.
In Roman numerals, the letter 'V' is used to represent the number 5. It was used because it resembled the convention of counting by notches carved in wood, with every fifth notch double-cut to form a 'V'.
During the Late Middle Ages, two forms of 'v' developed, which were both used for its ancestor /u/ and modern /v/. The pointed form 'v' was written at the beginning of a word, while a rounded form 'u' was used in the middle or end, regardless of sound. So whereas 'valour' and 'excuse' appeared as in modern printing, 'have' and 'upon' were printed as 'haue' and 'vpon'. The first distinction between the letters 'u' and 'v' is recorded in a Gothic script from 1386, where 'v' preceded 'u'. By the mid-16th century, the 'v' form was used to represent the consonant and 'u' the vowel sound, giving us the modern letter 'u'. Capital 'U' was not accepted as a distinct letter until many years later.


== Letter ==
In the International Phonetic Alphabet, /v/ represents the voiced labiodental fricative. See Help:IPA.
In English, V is unusual in that it has not traditionally been doubled to indicate a short vowel, the way for example P is doubled to indicate the difference between 'super' and 'supper'. However, that is changing with newly coined words, such as 'divvy up' and 'skivvies'. Like J, K, Q, X, and Z, V is not used very frequently in English. It is the 6th least common letter in the English language, with a frequency of about 1.03% in words. V is the only letter that cannot be used to form an English two-letter word in the Australian version of the game of Scrabble. C also cannot be used in the American version
The letter appears frequently in the Romance languages, where it is the first letter of the second person plural pronoun and (in Italian) the stem of the imperfect form of most verbs.
This letter, like Q and X, is not used in the Polish alphabet. /v/ is spelled with the letter ⟨w⟩ instead, following the convention of German.
Informal romanizations of Mandarin Chinese use V as a substitute for the close front rounded vowel /y/, properly written ü or ue in pinyin and ueh in Wade-Giles.


== Other names ==
Catalan: ve, pronounced [ˈve]; in dialects that lack contrast between /v/ and /b/, the letter is called ve baixa [ˈbe ˈbajʃə] "low B/V".
Czech: vé [vɛː]
French: vé [ve]
German: fau [ˈfaʊ]
Italian: vu [ˈvu] or vi [ˈvi]
Portuguese: vê [ˈve]
Spanish: uve [ˈuβe] is recommended, but ve [ˈbe] is traditional. If V is pronounced in the second way, it would have the same pronunciation as the letter B in Spanish (i.e. [ˈbe] after pause or nasal sound, otherwise [ˈβe]); thus further terms are needed to distinguish ve from be. In some countries it is called ve corta, ve baja, ve pequeña, ve chica or ve labiodental.
In Japanese, V is often called "bui" (ブイ). This name is an approximation of the English name which substitutes the voiced bilabial plosive for the voiced labiodental fricative (which does not exist in native Japanese phonology) and differentiates it from "bī" (ビー), the Japanese name of the letter B. The sound can be written with the relatively recently developed katakana character 「ヴ」 (vu) va, vi, vu, ve, vo (ヴァ, ヴィ, ヴ, ヴェ, ヴォ), though in practice the pronunciation is usually not the strictly labiodental fricative found in English. Moreover, some words are more often spelled with the b equivalent character instead of vu due to the long-time use of the word without it (e.g. "violin" is more often found as baiorin (バイオリン) than as vaiorin (ヴァイオリン) due partly to inertia, and to some extent due to the more native Japanese sound).


== Pronunciation ==
In most languages which use a Latin alphabet, ⟨v⟩ has a [v]-like sound (voiced labiodental fricative). In most dialects of Spanish, it is pronounced the same as ⟨b⟩, that is, [b] or [β̞]. In Corsican, it is pronounced [b], [v], [β] or [w], depending on the position in the word and the sentence. In German and Dutch it can be either [v] or [f].
In Native American languages of North America (mainly Iroquoian), ⟨v⟩ represents a nasalized central vowel, /ə̃/.
In Chinese pinyin, ⟨v⟩ is not used, as there is no sound [v] in Standard Mandarin; but the letter ⟨v⟩ is used by most input methods to enter letter ⟨ü⟩, which most keyboards lack. Romanised Chinese is a popular method to enter Chinese text phonetically.
In Irish, the letter ⟨v⟩ is mostly used in loanwords, such as veidhlín from English violin. However the sound [v] appears naturally in Irish when /b/ (or /m/) is lenited or "softened", represented in the orthography by ⟨bh⟩ (or "mh"), so that bhí is pronounced [vʲiː], an bhean (the woman) is pronounced [ən̪ˠ ˈvʲan̪ˠ], etc. For more information, see Irish phonology.
In the 19th century, ⟨v⟩ was sometimes used to transcribe a palatal click, [ǂ], a function since partly taken over by ⟨ç⟩.


== Related letters and other similar characters ==
𐤅: Phoenician letter waw, the ancestral glyph
Υ υ : Greek letter upsilon, from which V is descended
У у : Cyrillic letter u, descended from upsilon
Y y : Latin letter Y, which, like V, is also descended from upsilon
U u : Latin letter U, descended from V
W w : Latin letter W, also descended from V
Ѵ ѵ : Cyrillic letter izhitsa, a letter of the early Cyrillic alphabet
Ν ν : Greek letter nu, which appears like a "v" in lowercase
Վ վ : Վ from Armenian alphabet


== Computing codes ==
1 Also for encodings based on ASCII, including the DOS, Windows, ISO-8859 and Macintosh families of encodings.


== Other representations ==


== See also ==
Dominant, in music theory
Vee
∨, logical disjunction


== References ==


== External links ==
 Media related to V at Wikimedia Commons
 The dictionary definition of V at Wiktionary
 The dictionary definition of v at Wiktionary