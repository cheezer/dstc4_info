Value or values may refer to:


== Worth ==
Value (economics), a measure of the benefit that may be gained from goods or service.
Value (marketing), the difference between a customer's evaluation of benefits and costs.
Value investing, an investment paradigm.
Theory of value (economics), the study of the concept of economic value.

Value (ethics)
Value (personal and cultural)

Value (law), a legal concept similar to (but not necessarily identical with) the legal concept of consideration.
Value theory, the study of how the notion of value is used.


== Quantity ==
Value, also known as lightness or tone, a representation of variation in the perception of a color or color space's brightness.
Value (computer science), an expression that implies no (further) (mathematical) processing; a "normal form".
Value (mathematics), a property such as number assigned to or calculated for a variable, constant or expression.
Value (semiotics), the significance, purpose and/or meaning of a symbol as determined or affected by other symbols.
Note value, the relative duration of a musical note.