T. Veeraswamy is an Indian politician and former Member of the Legislative Assembly of Tamil Nadu. He was elected to the Tamil Nadu legislative assembly as an Indian National Congress (Indira) candidate from Orathanad constituency in 1980 election, and as an Anna Dravida Munnetra Kazhagam candidate in 1984 election.
T. Veerasamy, also called as Vellurar was born in Kallar Community at Vellur of Orathanadu taluk in Thanjavur district in Tamil Nadu as a third son and fifith Child of Late M.Thandavamurthi Athiyaman, Sornathammal. His father was the Nattamai of Vellur Nadu. His elder sisters was Ayikkannu and Pappammal. His Two elder Brothers Late T. Muthusamy Athiyaman and T. Ramasamy Athiyaman were the Panchayathars of Vellur Village.


== References ==