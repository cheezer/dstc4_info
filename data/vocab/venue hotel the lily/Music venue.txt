A music venue is any location used for a concert or musical performance. A music venue range in size and location, from an outdoor bandshell or bandstand or a concert hall to an indoor sports stadium. Typically, different types of venues host different genres of music. Opera houses, bandshells, and concert halls host classical music performances, whereas public houses, nightclubs, and discothèques offer music in contemporary genres, such as rock, dance, country and pop.


== Overview ==
Music venues may be either privately or publicly funded, and may charge for admission. An example of a publicly funded music venue is a park bandstand; such outdoor venues charge nothing for admission. A nightclub is a privately funded venue; venues like these often charge an entry fee to generate a profit. Music venues do not necessarily host live acts; disc jockeys at a discothèque or nightclub play recorded music through a PA system.
Depending on the type of venue, the opening hours, location and length of performance may differ, as well as the technology used to deliver the music in the venue. Other attractions, such as performance art or social activities, may also be available, either while music is playing or at other times. For example, at a bar or pub, the house band may be playing live songs while drinks are being served, and between songs, recorded music may be played. Some classes of venues may play live music in the background, such as a performance on a grand piano in a restaurant.


== Characteristics ==
Music venues can be categorised in a number of ways. Typically, the genre of music played at the venue, whether it is temporary and who owns the venue decide many of the other characteristics.


=== Permanent or temporary venues ===

The majority of music venues are permanent; however, there are temporary music venues. An example of a temporary venue would be one constructed for a music festival.


=== Ownership ===
Music venues may be the result of private or public enterprises.


=== Genre ===
Some venues only promote acts of one particular genre and example of this are opera houses.


=== Size and capacity ===
Music venues can be categorised by size and capacity; a small nightclub will often have a much smaller capacity than that of a stadium.


=== Indoor or outdoor ===

Music venues are either outdoor or indoor. Examples of outdoor venues include bandstands and bandshells; such outdoor venues provide minimal shelter for performing musicians and are usually located in parks. A temporary music festival is typically an outdoor venue. Examples of indoor venues include public houses, nightclubs, coffee bars and stadia.


=== Live or recorded music ===
Venues can play live music, recorded music, or a combination of the two, depending on the event or time of day. A characteristic of virtually every live music venue is that one or more stages are present.


=== Admissions policy and opening hours ===
Venues may be unticketed, casual entry available on the door, or advance tickets only. A dress code may or may not apply.


=== Centrality of performance ===


== History ==
Although music as an art form has existed since prehistoric times, permanent music venues began with the theatre of ancient Greece.


== Types ==


=== Opera houses ===

An opera house is a theatre constructed specifically for opera. The first opera house was the Teatro San Cassiano in Venice, Italy, which opened in 1637. An opera house generally has a spacious orchestra pit, where a large number of orchestra players may be seated at a level below the audience.


=== Amphitheater ===

Amphitheaters are round or oval shaped and usually unroofed. Permanent seating at amphitheaters is generally tiered.


=== Bandshell and bandstands ===

A bandshell is a large, outdoor performing venue typically used by concert bands and orchestras. The roof and the back half of the shell protect musicians from the elements and reflect sound through the open side and out towards the audience.


=== Jazz club ===

Jazz clubs are an example of a venue that is dedicated to a specific genre of music.


=== Live house ===

In Japan, small live music clubs are known as live houses, especially featuring rock, jazz, blues, and folk music, and have existed since the 1970s, now being found across the country.


=== Public houses and nightclubs ===


=== Concert hall ===

A concert hall is a performance venue constructed specifically for instrumental classical music. A concert hall may exist as part of a larger performing arts center.


=== Stadiums and arenas ===


== See also ==


== References ==


== External links ==