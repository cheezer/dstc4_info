A sport venue is a building, structure, or place in which a sporting competition is held.


== Types of sports venues ==
Arena
Baseball park
Billiard hall
Bullring
Gym
Ice hockey arena
Motorsport venues (autodrome)
Horse racing venues (hippodrome)
Shooting range
Speed skating rink
Stadium
Swimming pool
Velodrome


== See also ==
Lists of sports venues
List of indoor arenas
Multi-purpose stadium
Sports complex
Playing field
List of sporting venues with a highest attendance of 100,000 or more
List of stadiums by capacity


== External links ==
VisitingFan.com-Reviews of stadiums and arenas
www.SportVenueContruction.com - Overview of suppliers for Sport Venues