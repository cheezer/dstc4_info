Victoria Land is a region of Antarctica bounded on the east by the Ross Ice Shelf and the Ross Sea and on the west by Oates Land and Wilkes Land. It was discovered by Captain James Clark Ross in January 1841 and named after the UK's Queen Victoria. The rocky promontory of Minna Bluff is often regarded as the southernmost point of Victoria Land, and separates the Scott Coast to the north from the Hillary Coast of the Ross Dependency to the south.
The region includes ranges of the Transantarctic Mountains and the McMurdo Dry Valleys (the highest point being Mount Abbott in the Northern Foothills), and the flatlands known as the Labyrinth. Early explorers of Victoria Land include James Clark Ross and Douglas Mawson.


== References ==