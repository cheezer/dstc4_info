The Government of Victoria, under the Constitution of Australia, ceded certain legislative and judicial powers to the Commonwealth, but retained complete independence in all other areas. The Victorian Constitution says: "the Legislature of Victoria has full power and authority." In practice, however, the independence of the Australian states has been greatly eroded by the increasing financial domination of the Commonwealth.
Victoria is governed according to the principles of the Westminster system, a form of parliamentary government based on the model of the United Kingdom. Legislative power rests with the Parliament of Victoria, which consists of the Crown, represented by the Governor of Victoria, and the two Houses, the Victorian Legislative Council (the upper house) and the Victorian Legislative Assembly (the lower house).
Executive power rests formally with the Executive Council, which consists of the Governor and senior ministers. In practice executive power is exercised by the Premier of Victoria and the Cabinet, who are appointed by the Governor, but who hold office by virtue of their ability to command the support of a majority of members of the Legislative Assembly.
Judicial power is exercised by the Supreme Court of Victoria and a system of subordinate courts, but the High Court of Australia and other federal courts have overriding jurisdiction on matters which fall under the ambit of the Australian Constitution.


== §Public administration in Victoria ==
Victoria's public service has a reputation as one of the best in Australia. Areas in which Victoria is particularly strong include: road safety; water efficiency; the liveability of Melbourne; efforts to address family violence; and the efficiency and effectiveness of its service delivery.


== §See also ==
Victorian Ministry
List of Victorian government agencies
Local government areas of Victoria
Victorian Charter of Human Rights and Responsibilities (2006)


== §References ==


== §External links ==
Government of Victoria website