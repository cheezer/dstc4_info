Victoria may refer to:


== Given name ==
Victoria (name), commonly a female name Includes list of people with this given name
Queen Victoria (1819–1901), Queen of the United Kingdom (1837–1901), Empress of India (1876–1901)
Victoria, Crown Princess of Sweden (born 1977)
Victoria of Baden (1862–1930), queen-consort of Sweden as wife of King Gustaf V
Victoria (Gallic Empire) (died 271), 3rd century figure in the Gallic Empire
Victoria, Lady Welby (1837–1912), English philosopher of language, musician and artist


== Surname ==
Guadalupe Victoria (died 1843), first president of Mexico and hero of the Independence War
Manuel Victoria (died 1833), Mexican governor of Alta California in 1831
Tomás Luis de Victoria (c. 1548–1611), Renaissance-era composer


== Places ==


=== Africa ===
Lake Victoria
Victoria, Gauteng, South Africa
Victoria, Seychelles, the capital city of the Seychelles
Limbe, Cameroon, known as "Victoria" until 1982


=== Americas ===


==== Argentina ====
Victoria, Buenos Aires
Victoria, Entre Ríos
Victoria Department


==== Brazil ====
Former spelling of Vitória, Espírito Santo


==== Canada ====
Victoria, British Columbia, provincial capital
Greater Victoria, British Columbia, metropolitan around the provincial capital
Victoria Harbour (British Columbia)

Victoria Trail, Edmonton
Victoria, Manitoba
Victoria, New Brunswick in Glenelg Parish
Victoria, Newfoundland and Labrador
Victoria River (Newfoundland)

Victoria, Nova Scotia
Victoria Island (Canada), Nunavut/Northwest Territories
Victoria, Prince Edward Island


===== Canadian electoral districts =====
Victoria (electoral districts), a list of other provincial and former federal electoral districts
Victoria (electoral district), a federal electoral district in British Columbia
Victoria City (electoral district), historical federal electoral constituency in Canada
Victoria City (provincial electoral district), historical provincial electoral constituency
Victoria (Alberta electoral district)
Victoria (Alberta provincial electoral district)
Victoria (New Brunswick electoral district) (1867-1914)
Victoria (Nova Scotia electoral district)
Victoria (Ontario electoral district) (1903-1966)
Canadian Senate divisions named Victoria (in Quebec, Ontario, British Columbia, Nova Scotia, New Brunswick) and Victoria-Carelton (in New Brunswick)


==== Chile ====
Victoria, Chile, a city in Malleco Province, southern Chile


==== Colombia ====
La Victoria, Valle del Cauca, a town and municipality in the Department of Valle del Cauca
Victoria, Caldas, a town and municipality in the Department of Caldas


==== Grenada ====
Victoria, Grenada, a town in Saint Mark Parish


==== Guyana ====
Victoria, Guyana


==== Honduras ====
Victoria, Yoro


==== Mexico ====
Victoria, Guanajuato
Victoria Municipality, Guanajuato
Victoria Municipality, Tamaulipas


==== United States ====
Victoria, Arkansas
Victoria, Illinois
Victoria, Indiana
Victoria, Greene County, Indiana
Victoria, Kansas
Victoria, Louisiana
Victoria, Michigan
Victoria, Minnesota
Victoria, Mississippi
Victoria (Charlotte, North Carolina), listed on the National Register of Historic Places in Mecklenburg County, North Carolina
Victoria, Texas
Victoria, Virginia
Victoria, West Virginia


=== Asia ===


==== Hong Kong ====
Victoria, Hong Kong
Victoria Harbour
Victoria Peak


==== Malaysia ====
Victoria, Labuan, the capital of the Federal Territory of Labuan, Malaysia


==== Philippines ====
Victoria, Laguna
Victoria, Northern Samar
Victoria, Oriental Mindoro
Victoria, Tarlac
Victorias City, Negros Occidental
Victoria, Roxas, Oriental Mindoro, Philippines, a barangay


=== Antarctica ===
Victoria Land, Antarctica


=== Australia and New Zealand ===
Victoria (Australia), the state
Victoria, New Zealand, a suburb of Gisborne
Victoria Settlement or New Victoria, alternate names for Port Essington, Northern Territory, Australia


=== Europe ===


==== Malta ====
Victoria, Malta, capital of Gozo


==== Moldova ====
Victoria, Sărăteni, Leova district


==== Romania ====
Victoria, Braşov, a town in Braşov County
Victoria, Brăila, a commune in Brăila County
Victoria, Iași, a commune in Iaşi County
Victoria, a village in Hlipiceni Commune, Botoşani County
Victoria, a village in Stăuceni Commune, Botoşani County
Victoria, a village in Nufăru Commune, Tulcea County


==== United Kingdom ====
Victoria (Hackney ward)
Victoria (Sefton ward)
Victoria, ward of Newbury, Berkshire
Victoria, Newport, Wales
Victoria, London, a district named after Queen Victoria
Victoria, Roman name of Comrie, Scotland


== Transport ==


=== Aviation ===
Vickers Victoria, troop transport aircraft of the British Royal Air Force


=== Rail ===
Victoria station (disambiguation), several railway stations
London Victoria station, the second busiest rail terminus in London
GWR Victoria Class, type of steam locomotive


=== Road ===
Victoria (carriage), open carriage named after Queen Victoria
Victoria (body), designation for several automobile body styles and automobile model designations (e.g. Adams-Farwell, Packard One-Twenty, Packard Eight)
Victoria (motorcycle), a now defunct German bicycle and motorcycle manufacturer


=== Maritime ===
Victoria (ship) (also known as Nao Victoria and Vittoria), the first ship to circumnavigate the world
HMCS Victoria (SSK 876), a Canadian submarine
HMS Victoria, four ships of the British Royal Navy
HMVS Victoria, two ships of the Victorian Naval Force
Lake Victoria ferries of Kenya, Tanzania and Uganda
MV Princess Victoria, a ferry that sank on 31 January 1953
Queen Victoria (ship) (disambiguation)
RMS Victoria, a Lake Victoria ferry now called MV Victoria
Spanish frigate Victoria (F82), a Spanish frigate
Victoria, a ferry that sank 24 May 1881 in London, Ontario (see List of Canadian disasters by death toll)
Victoria (sternwheeler) a paddle steamer from the upper Fraser River
SS Victoria (Liberty), a Panamanian liberty ship in service 1947-50
SS Victoria (1870), a coastal passenger liner operated by the Alaska Steamship Company
SS Victoria (1907), a Cross-Channel and Isle of Man ferry
Victoria Class (disambiguation), various ship classes named Victoria
Victoria class submarine, a class of Canadian military submarines
Victoria-class battleship, a class of British battleships


== Sports ==
C.D. Victoria, Honduran football team
Northwich Victoria F.C., Cheshire, England
Victorian Bushrangers, Australian cricket team
Victoria Jaworzno, a Polish boxing and football team
Victoria Libertas Pesaro, Italian basketball team
Victoria National Golf Club, Indiana, U.S.
Victoria Rosport, Luxembourg football team
Victoria Vikes, the athletic program of the University of Victoria in Canada


== Titles, brands and popular culture ==
Victoria (3D figure), the articulated 3D figure by DAZ 3D
Victoria (1972 film), a 1972 Mexican film based on Washington Square (novel)
Victoria (1979 film), a 1979 Swedish film
Victoria (2008 film), a 2008 French-Canadian film
Victoria (2013 film), a 2013 Norwegian film
Victoria (2015 film), a 2015 German film
Victoria: An Empire Under the Sun, computer game by Paradox Interactive
Victoria II, the sequel from Paradox Interactive
Victoria (novel), an 1898 novel by Knut Hamsun
Victoria (Sami Michael novel), 1993, novel by Sami Michael
Victoria (Colombian telenovela), a Colombian soap opera
Victoria (theatre company), Belgian theatre company
Victoria (birthing simulator), a medical-education simulator by Gaumard Scientific


=== Music ===
"Victoria" (Magnus Uggla song), 1993 Magnus Uggla song
"Victoria" (song), a 1969 song about Queen Victoria by The Kinks
"Victoria" (Dance Exponents song) (1982)


=== Beverages ===
Victoria (soda) a fruit-flavored soda available in Querétaro (México) and owned by The Coca-Cola Company
Victoria Bitter, a bitter Australian lager
Victoria (Cervecería Centro Americana), a pale Guatemalan lager
Victoria (Grupo Modelo), a dark Mexican lager


=== Characters ===
Victoria Lord, the principal character in the long-running soap opera One Life to Live
Victoria the White Cat from Andrew Lloyd Webber's musical Cats
Victoria (Twilight), an antagonist from the Twilight series by Stephenie Meyer
Victoria Winters, the young governess in Dark Shadows
Victoria Regina Phibes (played by Caroline Munro) in The Abominable Dr. Phibes


== Species ==
Victoria (moth), a moth genus in the family Geometridae
Victoria (plant), a waterlily genus in the family Nymphaeaceae
Victoria perch (Lates niloticus), a fish species found in Africa
Victoria plum (disambiguation)


== Other uses ==
Province of Victoria, Anglican ecclesiastical province in Australia
Victoria (mythology), Roman goddess of Victory
Victoria (grape), another name for the German/Italian wine grape Trollinger
12 Victoria, asteroid
Victoria (crater), in the Meridiani Planum, Mars, named after one of Ferdinand Magellan's ships
The Victoria, Bayswater, London pub
The Victoria, Great Harwood, Lancashire pub


== See also ==