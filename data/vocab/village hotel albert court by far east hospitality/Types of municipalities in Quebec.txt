The following is a list of the types of local and supralocal territorial units in Quebec, including those used solely for statistical purposes, as defined by the Ministry of Municipal Affairs, Regions and Land Occupancy and compiled by the Institut de la statistique du Québec.
Not included are the urban agglomerations of Quebec, which, although they group together multiple municipalities, exercise only what are ordinarily local municipal powers.
A list of local municipal units in Quebec by regional county municipality can be found at List of municipalities in Quebec.


== Local municipalities ==
All municipalities (except cities), whether township, village, parish, or unspecified ones, are functionally and legally identical. The only difference is that the designation might serve to disambiguate between otherwise identically-named municipalities, often neighbouring ones. Many such cases have had their names changed, or merged with the identically-named nearby municipality since the 1950s, such as the former Township of Granby and City of Granby merging and becoming the Town of Granby in 2007.
Municipalities are governed primarily by the Code municipal du Québec (Municipal Code of Québec, R.S.Q. c. C-27.1), whereas cities and towns are governed by the Loi sur les cités et villes (Cities and Towns Act, R.S.Q. c. C-19) as well as (in the case of the older ones) various individual charters.
The very largest communities in Quebec are colloquially called cities; however there are currently no municipalities under the province's current legal system classified as cities. Quebec's government uses the English term town as the translation for the French term ville, and township for canton. The least-populous towns in Quebec (Barkmere, with a population of about 60, or L'Île-Dorval, with less than 10) are much smaller than the most-populous non-town municipalities (Saint-Charles-Borromée and Sainte-Sophie, each with populations of over 13,300).
The title city (French: cité code=C) still legally exists, with a few minor differences from that of ville. However it is moot since there are no longer any cities in existence. Dorval and Côte Saint-Luc had the status of city when they were amalgamated into Montreal on January 1, 2002 as part of the municipal reorganization in Quebec; however, when re-constituted as independent municipalities on January 1, 2006, it was with the status of town (French: ville) (although the municipal government of Dorval still uses the name Cité de Dorval).
Prior to January 1, 1995, the code for municipalité was not M but rather SD (sans désignation; that is, unqualified municipality).


== Aboriginal local municipal units ==
Prior to 2004, there was a single code, TR, to cover the modern-day TC and TK. When the distinction between TC and TK was introduced, it was made retroactive to 1984, date of the federal Cree-Naskapi (of Quebec) Act (S.C. 1984, c. 18).


== Territories equivalent to local municipalities ==


== Submunicipal units ==
There is also a different kind of submunicipal unit, which is defined and tracked not by the Quebec Ministry of Municipal Affairs but by Statistics Canada in the 2011 census: see List of unconstituted localities in Quebec.


== Supralocal units ==


== Notes ==


== See also ==
Administrative divisions of Quebec


== External links ==
Quebec provincial legislation
An Act respecting municipal territorial organization (also in French)
An Act respecting Northern villages and the Kativik Regional Government (also in French)
The Cree Villages and the Naskapi Village Act (also in French)
An Act respecting the Cree Regional Authority (also in French)
An Act respecting Cree, Inuit and Naskapi native persons (also in French)
An Act respecting the land regime in the James Bay and New Québec territories (also in French)
Federal legislation
Cree-Naskapi (of Quebec) Act (S.C. 1984, c. 18) (also in French)
Other
Répertoire des municipalités (look up the entry for any municipality)
Liste complète des types d'entités et leurs définitions (Commission de toponymie)