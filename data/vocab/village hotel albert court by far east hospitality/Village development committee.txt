A Village Development Committee (VDC) (Nepali: गाउँ विकास समिति; ‘’gāun bikās samiti’’) in Nepal is the lower administrative part of its Ministry of Federal Affairs and Local Development. Each district has several VDCs, similar to municipalities but with greater public-government interaction and administration. There are currently 3,276 village development committees in Nepal. Each VDC is further divided into several wards (Nepali: वडा) depending on the population of the district; the average is nine wards.


== Purpose ==
The purpose of village development committees is to organize village people structurally at a local level and to create a partnership between the community and the public sector for improved service delivery system. A VDC has a status as an autonomous institution and authority for interacting with the more centralized institutions of governance in Nepal. In doing so, the VDC gives village people an element of control and responsibility in development and ensures proper use and distribution of state funds and a greater interaction between government officials, NGOs and agencies. The village development committees, sanitation and income, and will monitor and record progress which is displayed in census data.


== Organization ==
In VDCs, there is one elected chief, usually elected with over an 80% majority. From each ward, there is a chief that is elected. With these, there are four members elected or nominated.
To keep data, records and to manage administrative works, there is one village secretary. The position is appointed by the government permanently, from whom they receive a salary. The ward members, ward chief, and VDC chiefs are not paid a salary, but they obtain money according to presence.
VDC is guided from the district development committee, headquarters, and the chief of DDC is a local development officer (LDO).
Population and housing details of VDCs in Nepal is provided by the National Population and Housing Census, in the 1991 Nepal census, the 2001 Nepal census and the 2011 Nepal census.


== References ==


== See also ==
Friends In Village Development Bangladesh
Land-use planning
List of village development committees of Nepal
Rural community council England
Village Development Committee (India)