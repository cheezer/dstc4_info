Voyage sans espoir (English: Voyage Without Hope) is a French drama film from 1943, directed by Christian-Jaque, written by Christian-Jaque, starring Simone Renant and Jean Marais.


== Cast ==
Simone Renant: Marie-Ange
Jean Marais: Alain Ginestier
Paul Bernard: Pierre Gohelle
Jean Brochard: Inspector Chapelin
Louis Salou: Inspector Sorbier
Ky Duyen: Li-Fang
Lucien Coëdel: Philippe Dejanin


== References ==


== External links ==
Voyage sans espoir at the Internet Movie Database