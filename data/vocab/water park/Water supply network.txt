A water supply system or water supply network is a system of engineered hydrologic and hydraulic components which provide water supply. A water supply system typically includes:
A drainage basin (see water purification - sources of drinking water).
A raw water collection point (above or below ground) where the water accumulates, such as a lake, a river, or groundwater from an underground aquifer. Raw water may be transferred using uncovered ground-level aqueducts, covered tunnels or underground water pipes to water purification facilities.
Water purification facilities. Treated water is transferred using water pipes (usually underground).
Water storage facilities such as reservoirs, water tanks, or water towers. Smaller water systems may store the water in cisterns or pressure vessels. Tall buildings may also need to store water locally in pressure vessels in order for the water to reach the upper floors.
Additional water pressurizing components such as pumping stations may need to be situated at the outlet of underground or above ground reservoirs or cisterns (if gravity flow is impractical).
A pipe network for distribution of water to the consumers (which may be private houses or industrial, commercial or institution establishments) and other usage points (such as fire hydrants).
Connections to the sewers (underground pipes, or aboveground ditches in some developing countries) are generally found downstream of the water consumers, but the sewer system is considered to be a separate system, rather than part of the water supply system.


== Water abstraction and raw water transfer ==

Raw water (untreated) is collected from a surface water source (such as an intake on a lake or a river) or from a groundwater source (such as a water well drawing from an underground aquifer) within the watershed that provides the water resource.
The raw water is transferred to the water purification facilities using uncovered aqueducts, covered tunnels or underground water pipes.


== Water treatment ==

Virtually all large systems must treat the water; a fact that is tightly regulated by global, state and federal agencies, such as the World Health Organization (WHO) or the United States Environmental Protection Agency (EPA). Water treatment must occur before the product reaches the consumer and afterwards (when it is discharged again). Water purification usually occurs close to the final delivery points to reduce pumping costs and the chances of the water becoming contaminated after treatment.
Traditional surface water treatment plants generally consists of three steps: clarification, filtration and disinfection. Clarification refers to the separation of particles (dirt, organic matter, etc.) from the water stream. Chemical addition (i.e. alum, ferric chloride) destabilizes the particle charges and prepares them for clarification either by settling or floating out of the water stream. Sand, anthracite or activated carbon filters refine the water stream, removing smaller particulate matter. While other methods of disinfection exist, the preferred method is via chlorine addition. Chlorine effectively kills bacteria and most viruses and maintains a residual to protect the water supply through the supply network.


== Water distribution network ==

The product, delivered to the point of consumption, is called potable water if it meets the water quality standards required for human consumption.
The water in the supply network is maintained at positive pressure to ensure that water reaches all parts of the network, that a sufficient flow is available at every take-off point and to ensure that untreated water in the ground cannot enter the network. The water is typically pressurised by pumps that pump water in to storage tanks constructed at the highest local point in the network. One network may have several such service reservoirs .
In small domestic systems, the water may be pressurised by a pressure vessel or even by an underground cistern (the latter however does need additional pressurizing). This eliminates the need of a water-tower or any other heightened water reserve to supply the water pressure.
These systems are usually owned and maintained by local governments, such as cities, or other public entities, but are occasionally operated by a commercial enterprise (see water privatization). Water supply networks are part of the master planning of communities, counties, and municipalities. Their planning and design requires the expertise of city planners and civil engineers, who must consider many factors, such as location, current demand, future growth, leakage, pressure, pipe size, pressure loss, fire fighting flows, etc. — using pipe network analysis and other tools.
As water passes through the distribution system, the water quality can degrade by chemical reactions and biological processes. Corrosion of metal pipe materials in the distribution system can cause the release of metals into the water with undesirable aesthetic and health effects. Release of iron from unlined iron pipes can result in customer reports of "red water" at the tap . Release of copper from copper pipes can result in customer reports of "blue water" and/or a metallic taste. Release of lead can occur from the solder used to join copper pipe together or from brass fixtures. Copper and lead levels at the consumer's tap are regulated to protect consumer health.
Utilities will often adjust the chemistry of the water before distribution to minimize its corrosiveness. The simplest adjustment involves control of pH and alkalinity to produce a water that tends to passivate corrosion by depositing a layer of calcium carbonate. Corrosion inhibitors are often added to reduce release of metals into the water. Common corrosion inhibitors added to the water are phosphates and silicates.
Maintenance of a biologically safe drinking water is another goal in water distribution. Typically, a chlorine based disinfectant, such as sodium hypochlorite or monochloramine is added to the water as it leaves the treatment plant. Booster stations can be placed within the distribution system to ensure that all areas of the distribution system have adequate sustained levels of disinfection.


== Topologies of water distribution networks ==
Like electric power lines, roads, and microwave radio networks, water systems may have a loop or branch network topology, or a combination of both. The piping networks are circular or rectangular. If any one section of water distribution main fails or needs repair, that section can be isolated without disrupting all users on the network.
Most systems are divided into zones. Factors determining the extent or size of a zone can include hydraulics, telemetry systems, history, and population density. Sometimes systems are designed for a specific area then are modified to accommodate development. Terrain affects hydraulics and some forms of telemetry. While each zone may operate as a stand-alone system, there is usually some arrangement to interconnect zones in order to manage equipment failures or system failures.


== Water network maintenance ==
Water supply networks usually represent the majority of assets of a water utility. Systematic documentation of maintenance works using a computerized maintenance management system (CMMS) is a key to a successful operation of a water utility.


== Sustainable urban water supply ==
A sustainable urban water supply network covers all the activities related to provision of potable water. Sustainable development is of increasing importance for the water supply to urban areas.
Water is an essential natural resource for human existence. It is needed in every industrial and natural process, for example, it is used for oil refining, for liquid-liquid extraction in hydro-metallurgical processes, for cooling, for scrubbing in the iron and the steel industry and for several operations in food processing facilities [1], etc. It is necessary to adopt a new approach to design urban water supply networks; water shortages are expected in the forthcoming decades and environmental regulations for water utilization and waste-water disposal are increasingly stringent.
To achieve a sustainable water supply network, new sources of water are needed to be developed, and to reduce environmental pollution.
The price of water is increasing, so less water must be wasted and actions must be taken to prevent pipeline leakage. Shutting down the supply service to fix leaks is less and less tolerated by consumers. A sustainable water supply network must monitor the freshwater consumption rate and the waste-water generation rate.
Many of the urban water supply networks in developing countries face problems related to population increase, water scarcity, and environmental pollution.


=== Population growth ===
In the year 1900 just 13% of the global population lived in cities. This percentage has been rising, and in 2005 49% of the global population lived in urban areas. In 2030 it is predicted, that this statistic will rise to 60% [2]. Attempts to expand water supply by governments are costly and often not sufficient. The building of new illegal settlements makes it hard to map, and make connections to, the water supply, and leads to inadequate water management [3]. In 2002, there were 158 million people with inadequate water supply.[4] An increasing number of people live in slums, in inadequate sanitary conditions, and are therefore at risk of disease.


=== Water scarcity ===
Potable water is not well distributed in the world. 1.8 million deaths are attributed to unsafe water supplies every year, according to the WHO [5]. Many people do not have any access, or do not have access to quality and quantity of potable water, though water itself is abundant. Poor people in developing countries can be close to major rivers, or be in high rainfall areas, yet not have access to potable water at all. There are also people living where lack of water creates millions of deaths every year.
Where the water supply system cannot reach the slums, people manage to use hand pumps, to reach the pit wells, rivers, canals, swamps and any other source of water. In most cases the water quality is unfit for human consumption. The principal cause of water scarcity is the growth in demand. Water is taken from remote areas to satisfy the needs of urban areas. Another reason for water scarcity is climate change: precipitation patterns have changed; rivers have decreased their flow; lakes are drying up; and aquifers are being emptied.


=== Governmental issues ===
In developing countries many governments are corrupt and poor and they respond to these problems with frequently changing policies. Water demand exceeds supply, and household and industrial water supplies are prioritised over other uses, which leads to water stress.[6] Potable water has a price in the market; water often becomes a business for private companies, which earn a profit by putting a higher price on water, which imposes a barrier for lower-income people. The Millennium Development Goals propose the changes required.
In advanced economies, the problems are about optimising existing supply networks. These economies have usually had continuing evolution, which allowed them to construct infrastructure to supply water to people. The European Union has developed a set of rules and policies to overcome expected future problems.
There are many international documents with interesting, but not very specific, ideas and therefore they are not put into practice [7]. Recommendations have been made by the United Nations, such as the Dublin Statement on Water and Sustainable Development.


== Optimizing the water supply network ==
The yield of a system can be measured by either its value or its net benefit. For a water supply system, the true value or the net benefit is a reliable water supply service having adequate quantity and good quality of the product. For example, if the existing water supply of a city needs to be extended to supply a new municipality, the impact of the new branch of the system must be designed to supply the new needs, while maintaining supply to the old system.


=== Single-objective optimization ===
The design of a system is governed by multiple criteria, one being cost. If the benefit is fixed, the least cost design results in maximum benefit. However, the least cost approach normally results in a minimum capacity for a water supply network. A minimum cost model usually searches for the least cost solution (in pipe sizes), while satisfying the hydraulic constraints such as: required output pressures, maximum pipe flow rate and pipe flow velocities. The cost is a function of pipe diameters; therefore the optimization problem consists of finding a minimum cost solution by optimising pipe sizes to provide the minimum acceptable capacity.


=== Multi-objective optimization ===
However, according to the authors of the paper entitled, “Method for optimizing design and rehabilitation of water distribution systems”, “the least capacity is not a desirable solution to a sustainable water supply network in a long term, due to the uncertainty of the future demand” [8]. It is preferable to provide extra pipe capacity to cope with unexpected demand growth and with water outages. The problem changes from a single objective optimization problem (minimizing cost), to a multi-objective optimization problem (minimizing cost and maximizing flow capacity).


=== Weighted sum method ===
To solve a multi-objective optimization problem, it is necessary to convert the problem into a single objective optimization problem, by using adjustments, such as a weighted sum of objectives, or an ε-constraint method. The weighted sum approach gives a certain weight to the different objectives, and then factors in all these weights to form a single objective function that can be solved by single factor optimization. This method is not entirely satisfactory, because the weights cannot be correctly chosen, so this approach cannot find the optimal solution for all the original objectives.


=== The constraint method ===
The second approach (the constraint method), chooses one of the objective functions as the single objective, and the other objective functions are treated as constraints with a limited value. However, the optimal solution depends on the pre-defined constraint limits.


=== Sensitivity analysis ===
The multiple objective optimization problems involve computing the tradeoff between the costs and benefits resulting in a set of solutions that can be used for sensitivity analysis and tested in different scenarios. But there is no single optimal solution that will satisfy the global optimality of both objectives. As both objectives are to some extent contradictory, it is not possible to improve one objective without sacrificing the other. It is necessary in some cases use a different approach,(e.g. Pareto Analysis), and choose the best combination.


=== Operational constraints ===
Returning to the cost objective function, it cannot violate any of the operational constraints. Generally this cost is dominated by the energy cost for pumping. “The operational constraints include the standards of customer service, such us: the minimum delivered pressure, in addition to the physical constraints such us the maximum and the minimum water levels in storage tanks to prevent overtopping and emptying respectively.” [9]
In order to optimize the operational performance of the water supply network, at the same time as minimizing the energy costs, it is necessary to predict the consequences of different pump and valve settings on the behavior of the network.
Apart from Linear and Non-linear Programming, there are other methods and approaches to design, to manage and operate a water supply network to achieve sustainability—for instance, the adoption of appropriate technology coupled with effective strategies for operation and maintenance. These strategies must include effective management models, technical support to the householders and industries, sustainable financing mechanisms, and development of reliable supply chains. All these measures must ensure the following: system working lifespan; maintenance cycle; continuity of functioning; down time for repairs; water yield and water quality.


== Sustainable development ==
In an unsustainable system there is insufficient maintenance of the water networks, especially in the major pipe lines in urban areas. The system deteriorates and then needs rehabilitation or renewal.

Householders and sewage treatment plants can both make the water supply networks more efficient and sustainable. Major improvements in eco-efficiency are gained through systematic separation of rainfall and wastewater. Membrane technology can be used for recycling wastewater.
The municipal government can develop a “Municipal Water Reuse System” which is a current approach to manage the rainwater. It applies a water reuse scheme for treated wastewater, on a municipal scale, to provide non-potable water for industry, household and municipal uses. This technology consists in separating the urine fraction of sanitary wastewater, and collecting it for recycling its nutrients.[10] The feces and graywater fraction is collected, together with organic wastes from the households, using a gravity sewer system, continuously flushed with non-potable water. The water is treated anaerobically and the biogas is used for energy production.
The sustainable water supply system is an integrated systemf including water intake, water utilization, wastewater discharge and treatment and water environmental protection. It requires reducing freshwater and groundwater usage in all sectors of consumption. Developing sustainable water supply systems is a growing trend, because it serves people’s long-term interests. [11]. There are several ways to re-use and recycle the water, in order to achieve long-term sustainability, such us:
Gray water re-use and treatment: gray water is wastewater coming from baths, showers, sinks and washbasins. If this water is treated it can be used as a source of water for uses other than drinking. Depending on the type of gray water and its level of treatment, it can be re-used for irrigation and toilet flushing. According to an investigation about the impacts of domestic grey water reuse on public health, carried out by the New South Wales Health Centre in Australia in the year 2000, grey water contains less nitrogen and fecal pathogenic organisms than sewage, and the organic content of grey water decomposes more rapidly.
Ecological treatment systems use little energy: there are many applications in gray water re-use, such us reed beds, soil treatment systems and plant filters. This process is ideal for gray water re-use, because of easier maintenance and higher removal rates of organic matter, ammonia, nitrogen and phosphorus.
Other possible approaches to scoping model’s for water supply, applicable to any urban area, include the following:
Sustainable Urban Drainage System.
Borehole extraction.
Intercluster groundwater flow.
Canal and river extraction.
Aquifer storage
A more user-friendly indoor water use.
The “Dublin Statement on Water and Sustainable Development”, mentioned above, is a good example of the new trend to overcome water supply problems. This statement, suggested by advanced economies, has come up with some principles that are of great significance to urban water supply. These are:
Fresh water is a finite and vulnerable resource, essential to sustain life, development and the environment.
Water development and management should be based on a participatory approach, involving users, planners and policy-makers at all levels.
Women play a central part in the provision, management and safeguarding of water. Institutional arrangements should reflect the role of women in water provision and protection
Water has an economic value in all its competing uses and should be recognized as an economic good. [12].
From these statements, developed in 1992, several policies have been created to give importance to water and to move urban water system management towards sustainable development. The Water Framework Directive by the European Commission is a good example of what has been created there out of former policies.


== Future approaches ==
There is great need for a more sustainable water supply systems. To achieve sustainability several factors must be tackled at the same time: climate change, rising energy cost, and rising populations. All of these factors provoke change and put pressure on management of available water resources.[13].
An obstacle to transforming conventional water supply systems to sustainability, is the amount of time needed to achieve the transformation. More specifically, transformation must be implemented by municipal legislation bodies, which always need short-term solutions too. Another obstacle to achieving sustainability in water supply systems is the insufficient practical experience with the technologies required, and the missing know-how about the organization and the transition process. One way to improve this situation is to implement pilot projects, learning from the costs involved and the benefits achieved.


== See also ==

Aqueduct
Civil Engineering
Conduit hydroelectricity
Domestic water system
Hardy Cross method
Hydrology
Infrastructure
Plumbing
River
Tap water
Water
Water pipes
Water meter
Water well
Automatic meter reading

Backflow prevention device
Fire hydrant
Strainers
Valve
Water tower
Water quality
Water resources
Water supply


== References ==

^ Riyanto, Eri. Chuei-TinChang. “A heuristic revamp strategy to improve operational flexibility of water networks based on active constraints”. (2009).
^ Ragot, José. Maquin, Didier. “Fault measurement detection in an urban water supply network”. (2006).
^ World Urbanization Prospects: the 2005 revision. Department of Economic and Social Affaires. Population Division.United Nations. (2005)
^ "Water and shared responsibility", Chapter 3 in Water and Human Settlements in an Urbanizing World. UN-HABITAT (United Nations Human Settlement Program) (2006) pp. 98–99.
^ WHO-Unicef joint monitoring program (2010) WHO/UNICEF Joint Monitoring Programme for Water Supply and Sanitation
^ Water sanitation and hygiene links to health. Water Health Organization (WHO) (2004)
^ K.Vairavamoorthy, S.D. Gorantiwar, A. Pathirana. “Managing urban water supplies in developing countries - Climate change and water scarcity scenarios” Elsevier. Physics and Chemistry of the Earth 33 (2008) 330-339. pp. 330–331.
^ P. Van der Steen. “Integrated Urban Water Management: towards sustainability:. Environmental Resources Department. UNESCO-IHE Institute for Water Education. SWITCH (2006).
^ Zheng, Y Wu. “Method for optimizing design and rehabilitation of water distribution systems”. Retrieved 2010-04-22 USA patent No. 7,593,839.
^ Martínez, Fernando. Hernández, Vicente. Alonso, José Miguel. Rao, Zhengfu. Alvisi, Stefano. “Optimizing the operation of the Valencia water distribution network”. (2007)
^ Craddock Consulting Engineering. "Recycling treated municipal wastewater for industrial water use".(2007).
^ Qiang, He. Li Zhai Jun, Huang. “Application of Sustainable Water System the Demonstration in Chengdu (China)”. (2008).
^ International Conference on Water and the Environment (1992) The Dublin Statement on Water and the Environment.Retrieved 2010-04-30.
^ Last, Ewan. Mackay,Rae. “Developing a New Scoping Model for Urban Sustainability”. (2007).


== External links ==
DCMMS: A web-based GIS application to record maintenance activities for water and wastewater networks.
An open-source hydraulic toolbox for water distribution systems
Water supply network schematic