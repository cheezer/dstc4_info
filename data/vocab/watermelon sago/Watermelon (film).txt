Watermelon is a 2003 television film directed by Kieron J. Walsh, and was released on 16 April 2003 on channel IPV. The screenplay is by Colin Bateman. The film is inspired by the novel the same name by Marian Keyes. The film is starring Anna Friel as Claire, Jamie Draven, Ciaran McMenamin, Sean McGinley, and Brenda Fricker. It is a lighthearted Irish drama following the troubles of a young couple when the man discovers that his beloved is carrying another man's baby.


== Plot ==
At twenty-nine, fun-loving, good-natured Claire has everything she ever wanted: a boyfriend she adores, a great apartment, a good job. Then, on the day she gives birth to her first baby, James visits her in the recovery room to inform her that he's leaving her. Claire is left with a beautiful newborn daughter, a broken heart, and a body that she can hardly bear to look at in the mirror. So, in the absence of any better offers, Claire decides to go home to her family in Dublin. To her gorgeous man-eating sister Anna, her soap-watching mother, her bewildered father. And there, sheltered by the love of an (albeit quirky) family, she gets better. A lot better. In fact, so much better that when James slithers back into her life, he's in for a bit of a surprise.


== Cast ==
Anna Friel ... Claire Ryan
Brenda Fricker ... Teresa Ryan
Ciarán McMenamin ... Adam Collins
Sean McGinley ... Joe Ryan
Jamie Draven ... James Wearing
Elaine Cassidy ... Anna Ryan
Helen Grace ... Judith
Siocha Quinn ... Baby Kate


== Awards ==


=== Irish Film and Television Awards ===


== References ==


== External links ==
Watermelon at the Internet Movie Database