Collider.com is a film website founded by Steve Weintraub in July 2005. It was purchased in January 2015 by Complex Media. In 2012, Weintraub was nominated for a Press Award by the International Cinematographers Guild for his work at Collider.


== References ==


== External links ==
Official website
Audio interview with Steve Weintraub about the site