West Midlands may refer to:


== In places ==
West Midlands (region), a region in England
West Midlands (county), the metropolitan county in the West Midlands region
West Midlands conurbation, the large conurbation in the West Midlands region


== In public sector ==
West Midlands (European Parliament constituency), the constituency of the European Parliament corresponding to the West Midlands region
West Midlands Police
West Midlands Regiment, the British Territorial Army regiment


== In other uses ==
BBC West Midlands, a local BBC region
West Midlands (Regional) League, a regional association football league in England
West Midlands English