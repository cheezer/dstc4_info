The Western Australian Legislative Assembly is elected from 59 single-member electoral districts. These districts are often referred to as electorates or seats.
The Electoral Distribution Act 1947 requires regular review of electoral boundaries, in order to keep the relative size of electorates within certain limits. Electoral boundaries are determined by the Western Australian Electoral Commission. The last electoral redistribution was completed in October 2007; it applied in the 2008 state election.


== List of electoral districts by electoral region ==
Agricultural electoral region
Central Wheatbelt
Geraldton
Moore
Wagin

East Metropolitan electoral region
Armadale
Bassendean
Belmont
Darling Range
Kalamunda
Gosnells
Forrestfield
Maylands
Midland
Morley
Mount Lawley
Nollamara
Swan Hills
West Swan

Mining and Pastoral electoral region
Eyre
Kalgoorlie
Kimberley
North West
Pilbara

North Metropolitan electoral region
Balcatta
Carine
Churchlands
Cottesloe
Girrawheen
Hillarys
Joondalup
Kingsley
Mindarie
Nedlands
Ocean Reef
Perth
Scarborough
Wanneroo

South Metropolitan electoral region
Alfred Cove
Bateman
Cannington
Cockburn
Fremantle
Kwinana
Jandakot
Riverton
Rockingham
South Perth
Southern River
Victoria Park
Warnbro
Willagee

South West electoral region
Albany
Blackwood-Stirling
Bunbury
Collie-Preston
Dawesville
Mandurah
Murray-Wellington
Vasse


== See also ==
Electoral regions of Western Australia


== References ==
^ 2007 Electoral Distribution


== External links ==
Western Australian Electoral Commission
Electoral Distribution Act 1947