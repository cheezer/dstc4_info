Picture This is the sixth studio album by Wet Wet Wet. It was released on 10 April 1995. Its six offspring singles were "Love Is All Around", "Julia Says", "Don't Want to Forgive Me Now", "Somewhere Somehow", "She's All on My Mind" and "Morning". The album reached #1 in the UK chart.


== Track listing ==
All tracks written by Wet Wet Wet (Clark/Cunningham/Mitchell/Pellow) except:
Track #6 She Might Never Know; written by Wet Wet Wet, lyrics by Chris Difford, and
Track #12 Love Is All Around; written by Reg Presley.
"Julia Says" - 4:09
"After the Love Goes" - 3:50
"Somewhere Somehow" - 3:51
"Gypsy Girl" - 2:11
"Don't Want to Forgive Me Now" - 2:55
"She Might Never Know" - 4:44
"Someone Like You" - 3:48
"Love is My Shepherd" - 3:23
"She's All on My Mind" - 3:55
"Morning" - 4:08
"Home Tonight" - 4:07
"Love Is All Around" (cover of The Troggs song) - 4:04


== References ==