Douglas Harry "Wheels" Wheelock (born May 5, 1960) is an American astronaut. He has flown in space twice, logging 178 days on the Space Shuttle, International Space Station, and Russian Soyuz. On July 12, 2011, Wheelock announced that he would be returning to duty with the active United States Army in support of Operation Enduring Freedom.


== Early life and education ==
Douglas Wheelock was born in Binghamton, New York to Olin and Margaret Wheelock. In a pre-flight interview, Wheelock stated that he was inspired to become an astronaut at an early age. He also stated that a major turning point in his life was the Apollo 11 moon landing in July 1969. In 1978 Wheelock graduated from Windsor Central High School in Windsor, New York before attending the United States Military Academy at West Point.


== Military career ==
Wheelock spent four years as a cadet at West Point, graduating in the class of 1983 with a Bachelor of Science degree in Applied Science and Engineering. After graduation he was commissioned a Second Lieutenant in the U.S. Army's Infantry Branch and entered flight school the next year. In September 1984, then Lieutenant Wheelock graduated at the top of his flight class and was designated an Army Aviator. He subsequently served in the Pacific as a combat aviation Section Leader, Platoon Leader, Company Executive Officer, Battalion Operations Officer, and Commander of an Air Cavalry Troop in the 9th Cavalry. He was later assigned to the Aviation Directorate of Combat Developments as an Advanced Weapons Research and Development Engineer.
Wheelock was selected as a member of Class 104 at the U.S. Naval Test Pilot School and upon completion was assigned as an Experimental Test Pilot with the Army Aviation Technical Test Center (ATTC). His flight testing was focused in the areas of tactical reconnaissance and surveillance systems in the OH-58D, UH-60, RU-21 and C-23 aircraft. He served as Division Chief for fixed-wing testing of airborne signal and imagery intelligence systems in support of the National Program Office for Intelligence and Electronic Warfare.
While on active duty, Wheelock received a Master of Science degree in Aerospace Engineering from Georgia Tech in 1992. In August 1996 Wheelock was assigned to the Johnson Space Center as a Space Shuttle integration test engineer. His technical duties involved engineering liaison for launch and landing operations of the Space Shuttle. He was selected as the vehicle integration test team lead for the joint Space Shuttle and Russian Space Station Mir mission STS-86, and lead engineer for International Space Station hardware fit checks.
Wheelock is a graduate of the Army Airborne and Air Assault Courses, the Infantry and Aviation Officer Advanced Courses, the Combined Arms Services Staff School, the Material Acquisition Management Course, and the U.S. Army Command and General Staff College. In July 2011, Wheelock left for a tour of duty in support of Operation Enduring Freedom. He is scheduled to return in fall 2011. A dual-rated Master Army Aviator Astronaut, he has logged over 2,500 flight hours in 43 different rotary and fixed-wing aircraft.


== NASA career ==

In August 1998 Wheelock reported for NASA Astronaut Candidate Training. Having completed the initial two years of intensive Space Shuttle and Space Station training, he was assigned to the Astronaut Office ISS Operations Branch as a Russian Liaison, participating in the testing and integration of Russian hardware and software products developed for the ISS. He worked extensively with the Energia Aerospace Company in Moscow, Russia, developing and verifying dual-language procedures for ISS crews. Wheelock led joint U.S./Russian teams to the Baikonur Cosmodrome in Kazakhstan to oversee bench reviews, inventory, loading and launch of the first four unmanned ISS resupply capsules.
In 2001, Wheelock assumed duties as the Crew Support Astronaut for the ISS Expedition 2 crew, which was on orbit for 147 days from March 2001 to August 2001, and for the ISS Expedition 4 crew, which was on orbit for 195 days from December 2001 to June 2002. He was the primary contact for all crew needs, coordination, planning and interactions, and was the primary representative of the crews while they were on orbit. In August 2002, Wheelock was assigned as a Spacecraft Communicator (CAPCOM) in the Mission Control Center in Houston, Texas. In this role, he was the primary communication link between crews on orbit and the ground support team in the Control Center. His work as a CAPCOM culminated in his assignment as the lead CAPCOM for the ISS Expedition 8 mission, which was 194 days in duration.
In July 2004, Wheelock served as an aquanaut during the NEEMO 6 mission aboard the Aquarius underwater laboratory, living and working underwater for ten days.
In January 2005, he was assigned as NASA's Director of Operations – Russia where he served at the Gagarin Cosmonaut Training Center in Star City, Russia. He was responsible for supporting Russia-based training, logistic, and administrative needs of NASA astronauts preparing for flight on the ISS. Wheelock was the primary liaison between Star City and NASA operations in Houston, including medical, training, science, contracting, public affairs, and administration departments. He was also responsible for liaison duties between NASA and the Russian Space Agency, as well as the Russian aerospace industry.
After his first spaceflight, Wheelock was assigned as the backup to T.J. Creamer for the Soyuz TMA-17/Expedition 22/Expedition 23 mission to the International Space Station. Creamer completed a successful 163 day mission from 2009–2010.


=== Spaceflight experience ===


==== STS-120 ====

On October 23, 2007 Wheelock launched on his first spaceflight aboard Space Shuttle Discovery. During the STS-120 mission, Wheelock was Mission Specialist 3 on a multinational crew whose mission was to deliver the Node 2 module to the International Space Station. Wheelock participated in three spacewalks with fellow astronaut Scott Parazynski in order to perform mission critical tasks on the exterior of the Station. The spacewalks involved outfitting the Node 2 module, storing an S-band antenna, work on the Integrated Truss Structure, and External Stowage Platform 2. During the mission a solar array on the P6 truss tore requiring an emergency repair by Wheelock and Parazynski. Parazynski attached cufflinks to the solar array allowing it to expand without additional damage. Wheelock orbited the Earth 238 times during the 15 day mission.


==== Soyuz TMA-19 ====
Wheelock launched to the International Space Station on June 15, 2010, aboard the Soyuz TMA-19 spacecraft with Fyodor Yurchikhin and Shannon Walker.


==== ISS Expedition 24 ====
Wheelock joined the Expedition 24 crew in progress. During the mission he participated in several scientific investigations. On July 31, 2010 a pump module on the exterior of the Space Station failed. Wheelock and Tracy Caldwell-Dyson made three spacewalks to replace the module. The failed pump module was later returned home on STS-135.


==== ISS Expedition 25 ====
On September 22, 2010, Wheelock took command of the International Space Station at the beginning of Expedition 25. He is the first U.S. Army officer to command the ISS. Wheelock and his crew continued science and maintenance of the space station. Wheelock is sharing pictures of the Earth, station and views of space via Twitter, as Soichi Noguchi did before him. In September 2010, Wheelock tweeted photos from space of Hurricane Earl. He also participated in a NASA Tweetup on March 16, 2011.
He became the first person to "check in" from space October 22, using the mobile social networking application Foursquare. Wheelock's check-in to the International Space Station launched a partnership between NASA and Foursquare to connect its users to the space agency, enabling them to explore the universe and discover Earth.
He returned to Earth on November 26, 2010 aboard the Soyuz.


=== Awards and decorations ===

Doug has also been awarded:
Order of St. Michael (Bronze Award) 2007 from the AAAA.
Two NASA Group Achievement Awards: Global Positioning System (1997) and Russian Liaison Support Team (2001)
NASA Superior Accomplishment Award (2002 and 2004)
Distinguished Graduate of the U.S. Army Flight Training Course (1984)
25th Infantry Division Flight Safety Award (1986 and 1989)
Shorty Awards Real-Time Photo of the Year


== Personal life ==
His hometown is Windsor, New York. Wheelock's parents, Olin and Margaret Wheelock, reside in upstate New York.
In 1989 he was selected by the U.S. Jaycees as one of "Ten Outstanding Young Men of America". In 1990 the Veterans of Foreign Wars selected Wheelock as an "Outstanding Spokesman for Freedom".


=== Organizations ===
Wheelock is a member of the Society of Experimental Test Pilots, the Society of American Military Engineers, the Association of the United States Army, and the Army Aviation Association of America.


== References ==
 This article incorporates public domain material from websites or documents of the National Aeronautics and Space Administration.


== External links ==
Douglas H. Wheelock on Twitter
Douglas H. Wheelock on Twitpic
Spacefacts biography of Douglas H. Wheelock