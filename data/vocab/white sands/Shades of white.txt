Shades of white are colors that differ only slightly from pure white. Variations of white include what are commonly termed off-white colors, which may be considered part of a neutral color scheme.
In color theory, a shade is a pure color mixed with black (or having a lower lightness). Strictly speaking, a “shade of white” would be a neutral gray. This article is also about off-white colors that vary from pure white in hue, and in chroma (also called saturation, or intensity).
Colors often considered "shades of white" include, cream, eggshell, ivory, Navajo white, and vanilla. Even the lighting of a room, however, can cause a pure white to be perceived as off-white.
Off-white colors were pervasively paired with beiges in the 1930s, and especially popular again from roughly 1955 to 1975.
Whiteness measures the degree to which a surface is white in colorimetry.


== Web colors ==
Below is a chart showing the computer web color shades of white. An achromatic white is a white color in which the red, green, and blue codes are exactly equal. The web colors white and white smoke are achromatic colors. A chromatic shade of white is a white color in which the red, green, and blue codes are not exactly equal, but are close to each other, which is what makes it a shade of white.


== White ==

White is a color, the perception of which is evoked by light that stimulates all three types of color sensitive cone cells in the human eye in equal amounts and with high brightness compared to the surroundings. A white visual stimulation will be void of hue and grayness. White is the lightest possible color.


== Variations of white (off-white colors) ==


=== Ghost white ===

The web color ghost white is a tint of white associated with what it is imagined the color of a ghost might be.
There is no evidence that this color name was in use before the X11 color names were formulated in 1987.


=== White smoke ===

The web color white smoke is displayed on the left.
There is no evidence that this color name was in use before the X11 color names were formulated in 1987.


=== Baby powder ===

The Crayola crayon color baby powder was introduced in 1994 as part of its specialty Magic Scent crayon collection.


=== Snow ===

The web color snow is displayed at left.
The first recorded use of snow as a color name in English was in 1000.
The color snow was included as one of the X11 colors when they were formulated in 1987.


=== Ivory ===

Ivory is an off-white color that resembles ivory, the material out of which the teeth and tusks of animals (such as the elephant and the walrus) are made. It has a very slight tint of yellow.
The first recorded use of ivory as a color name in English was in 1385.
The color ivory was included as one of the X11 colors when they were formulated in 1987.


=== Floral white ===

The web color floral white is displayed at left.
There is no evidence that this color name was in use before the X11 color names were formulated in 1987.


=== Seashell ===

Seashell is an off-white color that resembles some of the very pale pinkish tones that are common in many seashells.
The first recorded use of seashell as a color name in English was in 1926.
In 1987, seashell was included as one of the X11 colors.


=== Cornsilk ===

Cornsilk is a color that is a representation of the color of cornsilk.
The first recorded use of cornsilk as a color name in English was in 1927.
In 1987, cornsilk was included as one of the X11 colors.


=== Old lace ===

Old lace is a web color that is a very pale yellowish orange that resembles the color of an old lace tablecloth.
It is one of the original X11 colors.
Old lace is used as a color of a certain kind of Caucasian skin type in art.


=== Cream ===

Cream is a color that is a representation of the color of the cream produced from the milk of cattle.
The first recorded use of cream as a color name in English was in 1590.
In 1987, cream was included as one of the X11 colors.


=== Beige ===

The color beige is displayed at left.
The first recorded use of beige as a color name in English was in 1887.
The term originates from beige cloth, a cotton fabric left undyed in its natural color.
Items that are of beige color in real world applications are typically closer to yellow than they are to white.


=== Linen ===

Linen is a web color that is a very pale orange color that resembles the color of linen.
It is one of the original X11 colors.


=== Antique white ===

Antique white is a web color.
The color name antique white began to be used in 1987 when the X11 colors were first formulated.


=== Champagne ===

The color champagne is displayed at right.
The color's name is derived from the typical color of the beverage champagne.
The first recorded use of champagne as a color name in English was in 1915.


=== Eggshell ===

The color eggshell is displayed at right.
The color eggshell is a representation of the average color of chicken eggs.


=== Dutch white ===

Displayed at right is the color Dutch white.
Dutch white is one of the colors on the Resene Color List, a color list popular in Australia and New Zealand. The color Dutch white was formulated in 2000.


=== Bone ===

The color bone is displayed at left. This color is a representation of the color of bones.
The first recorded use of bone as a color name in English was in the first decade of the 19th century (exact year uncertain).
Bone colored paint is often used by landlords to paint vacant apartments that are for rent since it hides dirt and stains better than white.


=== Vanilla ===

The color vanilla is a rich tint of off-white as well as a medium pale tint of yellow.
The first recorded use of vanilla as a color name in English was in 1925.


=== Flax ===

The color flax is displayed at right.
The first recorded use of flax as a color name in English was in 1915.


=== Navajo white ===

Navajo white is a whitish orange color, and derives its name from its similarity to the background color of the Navajo Nation ethnic flag.
In 1987, Navajo white was included as one of the X11 colors.


=== Ecru ===

Ecru describes a shade of grayish-pale yellow or a light grayish-yellowish brown. It is often used to describe such fabrics as silk and linen in their unbleached state. Ecru comes from the French word écru, which means literally "raw" or "unbleached".


== References ==


== See also ==
Beige
List of colors
Shades of black
Variations of gray