The Major League Baseball Wild Card Game is a play-in game which was added to the Major League Baseball postseason in 2012. The addition keeps the playoff format similar to the three-tiered postseason format used from 1995 through 2011, but adds a second wild-card team. Two wild-card teams in each league play each other in a single-game playoff after the end of the regular season. The winner of the game advances to the Division Series. The home team for the wild-card game is the team with the better regular-season record.
If both teams have the same number of wins and losses, tie-breaking procedures are used, with no additional games being played. On the other hand, teams tied for the division title will now always play a one game playoff for the division title, even if both teams are already qualified for the postseason. This is in contrast to the earlier wild card format used, for example in the 2005 season when the New York Yankees and Boston Red Sox tied for first place in their division but did not play an additional game as both teams were qualified for the postseason in any event.
In the division series, the winner of the wild-card game will always face whichever division champion has the best record. All division winners receive a bye as they await the result of the game. Previously, a wild-card team could not face the champion of its own division. This change makes it possible for the two teams with the best record in the league to face each other before the League Championship Series for the first time since 1997 (from 1995 to 1997 the matchups for the division series were determined by annual rotation between the west, central and east divisions).


== Purpose ==
The addition of a second wild-card team to each league was completed for multiple reasons:
Added importance to division races. Before 1995, only division-winning teams advanced to the playoffs, creating excitement when teams within a division competed for the best record in that division. From 1995 to 2011, the urgency of a division race was reduced when the second place team also made the playoffs as a wild card. By adding a single-game playoff between the two wild-cards, teams are motivated to win the division in order to avoid the uncertainty of an elimination game. In addition, the winner of the wild-card game is at a disadvantage in the next series, due to having to make strategic decisions to avoid immediate elimination, such as play its best pitchers available, without regard for future playoff games.
Wild-card teams are penalized. In the four-team format from 1995 to 2011, the wild-card team had to win just as many postseason games as a division winner in order to reach the World Series. Now the winner of the wild-card game is at a disadvantage because it has to play an extra game.
Increases postseason excitement and revenue, with the tension of a sudden-death match at the start of the playoffs, similar to tie-breaker games held to resolve regular season ties. Recent examples of this kind of excitement were seen in tie-breaking games in 2007, 2008, and 2009, as well as the final day of the 2011 regular season.
With an additional playoff spot at stake, more teams are competing at the end of the regular season for a place in the playoffs.


== Implementation ==
With the adoption of MLB's new collective bargaining agreement in November 2011, baseball commissioner Bud Selig announced that a new playoff system would begin within the next two years; the change was ultimately put into place in 2012.


== Results ==


=== American League ===


=== National League ===


=== Win-Loss Records ===


==== By Team ====


==== By Manager ====


== References ==