The Colorado Parks and Wildlife division of the U.S. State of Colorado manages more than 300 state wildlife areas with a total area of more than 860 square miles (2,230 km2) in the state. The Colorado state wildlife areas are managed for hunting, fishing, observation, management, and preservation of wildlife.


== State wildlife areas ==
Adams State Wildlife Area
Adobe Creek Reservoir State Wildlife Area at Blue Lake
Alberta Park Reservoir State Wildlife Area
Alma State Wildlife Area
Almont Triangle State Wildlife Area
Andrews Lake State Wildlife Area
Andrick Ponds State Wildlife Area
Apishapa State Wildlife Area
Arkansas River (Holly) State Wildlife Area
Arkansas River/Big Bend State Wildlife Area
Atwood State Wildlife Area
Badger Basin State Wildlife Area
Banner Lakes State Wildlife Area
Basalt State Wildlife Area
Beaver Creek Reservoir State Wildlife Area
Beaver Creek State Wildlife Area
Beaver Lake State Wildlife Area
Bellaire Lake State Wildlife Area
Bellvue-Watson State Fish Unit (state fish hatchery)
Bergen Peak State Wildlife Area
Big Meadows Reservoir State Wildlife Area
Big Thompson Ponds State Wildlife Area
Billy Creek State Wildlife Area
Bitter Brush State Wildlife Area
Blacktail Conservation easement
Bliss State Wildlife Area
Blue River State Wildlife Area
Bob Terrell State Wildlife Area (formerly the Roaring Fork State Wildlife Area - Koziel Parcel)
Bodo State Wildlife Area
Boedecker Reservoir State Wildlife Area
Bosque Del Oso State Wildlife Area
Boyd Ponds State Wildlife Area
Bravo State Wildlife Area
Brower State Wildlife Area
Brown Lakes State Wildlife Area
Brown's Park State Wildlife Area - Beaver Creek Unit
Brown's Park State Wildlife Area - Calloway Unit
Brown's Park State Wildlife Area - Cold Springs
Brown's Park State Wildlife Area - Wiggins Unit
Brownlee State Wildlife Area (fishing lease on the North Platte River)
Brush Creek State Wildlife Area
Brush Hollow State Wildlife Area
Brush Prairie Ponds State Wildlife Area
Brush State Wildlife Area
Buena Vista State Wildlife Area
Burchfield State Wildlife Area
Cabin Creek State Wildlife Area
Centennial State Wildlife Area
Centennial Valley State Wildlife Area
Chalk Cliffs State Fish Unit (state fish hatchery)
Champion State Wildlife Area
Charlie Meyers State Wildlife Area
Cherokee State Wildlife Area - Lone Pine Unit
Cherokee State Wildlife Area - Rabbit Creek Unit
Cherokee State Wildlife Area - Middle Unit
Cherokee State Wildlife Area - Upper Unit
Cherokee State Wildlife Area - Lower Unit
Chipeta Lake State Wildlife Area
Christina State Wildlife Area
Chubb Park Ranch State Wildlife Area
Chuck Lewis State Wildlife Area
Cimarron State Wildlife Area
Clear Creek Reservoir State Wildlife Area
Cline Ranch State Wildlife Area
Coalbed Canyon State Wildlife Area
Cochetopa State Wildlife Area near Synder
Coke Oven State Wildlife Area
Coller State Wildlife Area
Colorado River Island State Wildlife Area
Columbine State Wildlife Area
Conejos River State Wildlife Area (fishing easements)
Cottonwood Creek State Wildlife Area (Marquard fishing easement)
Cottonwood State Wildlife Area
Cowdrey Lake State Wildlife Area
Creede State Wildlife Area
Crystal River State Fish Unit (state fish hatchery)
Dan Noble State Wildlife Area
Dawn Pond State Wildlife Area
Deadman State Wildlife Area
Delaney Butte Lakes State Wildlife Area
Devil Creek State Wildlife Area
DeWeese Reservoir State Wildlife Area
Diamond J State Wildlife Area
Dolores River State Wildlife Area
Dome Lakes State Wildlife Area
Dome Rock State Wildlife Area (formerly the Mueller State Wildlife Area)
Douglas Reservoir State Wildlife Area
Dowdy Lake State Wildlife Area
Dry Creek Basin State Wildlife Area
Duck Creek State Wildlife Area
Dune Ridge State Wildlife Area
Durango State Fish Unit (state fish hatchery)
Dutch Gulch State Wildlife Area
Eagle River State Wildlife Area (fishing leases)
Echo Canyon Lake State Wildlife Area
Elliott State Wildlife Area
Emerald Mountain State Wildlife Area
Escalante State Wildlife Area - Cap Smith Tract
Escalante State Wildlife Area - East Walker Tract
Escalante State Wildlife Area - Hamilton Tract
Escalante State Wildlife Area - Harrison Tract
Escalante State Wildlife Area - Lower Roubideau Tract
Escalante State Wildlife Area - Picket Corral Tract
Escalante State Wildlife Area - Upper Roubideau and Peach Orchard
Escalante State Wildlife Area - Waterwheel, Kelso, and Lower Gunnison River Tracts
Escalante State Wildlife Area - West Walker Tract
Finger Rock State Fish Unit (state fish hatchery)
Fish Creek State Wildlife Area
Flagler Res. State Wildlife Area
Forks State Wildlife Area
Fort Lyon State Wildlife Area
Four Mile State Wildlife Area
Frank State Wildlife Area
Franklin Island State Wildlife Area
Franz Lake State Wildlife Area
Frenchman Creek State Wildlife Area
Garfield Creek State Wildlife Area
Georgetown State Wildlife Area
Georgetown Watchable Wildlife Area
Glenwood Springs State Fish Unit (state fish hatchery)
Granada State Wildlife Area
Granby Ranch Conservation Easement
Granite State Wildlife Area
Grieve Ranch Conservation Easement State Wildlife Area
Groundhog Reservoir State Wildlife Area
Gunnison River State Wildlife Area
Gunnison State Wildlife Area
Gypsum Ponds State Wildlife Area
Hallenbeck Ranch State Wildlife Area (currently closed)
Hardeman (fishing easement)
Harmon State Wildlife Area (fishing easement)
Haviland Lake State Wildlife Area
Heckendorf State Wildlife Area
Higel State Wildlife Area
Hohnholz Lakes State Wildlife Area
Holbrook Reservoir State Wildlife Area
Holly State Wildlife Area
Holyoke State Wildlife Area
Home Lake State Wildlife Area
Horse Creek Reservoir State Wildlife Area at Timber Lake
Horsethief Canyon State Wildlife Area
Hot Creek State Wildlife Area
Hot Sulphur Springs State Wildlife Area - Byers Canyon Rifle Range
Hot Sulphur Springs State Wildlife Area - Jenny Williams
Hot Sulphur Springs State Wildlife Area - Joe Gerrans
Hot Sulphur Springs State Wildlife Area - Lone Buck
Hot Sulphur Springs State Wildlife Area - Parshall Divide Unit
Hot Sulphur Springs State Wildlife Area - Paul Gilbert Fishing Area
Hot Sulphur Springs State Wildlife Area - Pioneer Park
Huerfano State Wildlife Area
Hugo State Wildlife Area
Indian Run State Wildlife Area
Jackson Lake State Wildlife Area
James M. John State Wildlife Area
James Mark Jones State Wildlife Area
Jean K. Tool State Wildlife Area
Jensen State Wildlife Area
Jerry Creek Reservoirs State Wildlife Area Number 1
Jerry Creek Reservoirs State Wildlife Area Number 2
Jim Olterman/Lone Cone State Wildlife Area
Joe Moore Reservoir State Wildlife Area
John Martin Reservoir State Wildlife Area
Johnson Village State Wildlife Area
Julesburg State Wildlife Area (Julesburg and Ovid leases)
Jumbo Reservoir State Wildlife Area
Jumping Cow State Wildlife Area
Junction Butte State Wildlife Area
Karney Ranch State Wildlife Area
Karval Reservoir State Wildlife Area
Kemp - Breeze State Wildlife Area
Kinney Lake State Wildlife Area
Knight-Imler State Wildlife Area
Knudson State Wildlife Area
Kodak Watchable Wildlife Area
La Jara Reservoir State Wildlife Area
La Jara State Wildlife Area
Lake Beckwith State Wildlife Area
Lake Dorothey State Wildlife Area
Lake Fork Gunnison State Wildlife Area
Lake John State Wildlife Area
Las Animas State Fish Unit
Leaps Gulch State Wildlife Area
Leatha Jean Stassen State Wildlife Area
Little Snake State Wildlife Area
Loma Boat Launch State Wildlife Area
Lon Hagler State Wildlife Area
Lone Dome State Wildlife Area
Lonetree Reservoir State Wildlife Area
Love Meadow Watchable Wildlife Area
Lowell Ponds State Wildlife Area
Manville State Wildlife Area (fishing lease)
Mason Family State Wildlife Area (formerly Cebolla Creek State Wildlife Area)
McCluskey State Wildlife Area
Meeker Pasture State Wildlife Area
Melon Valley State Wildlife Area
Messex State Wildlife Area
Middle Taylor Creek State Wildlife Area
Mike Higbee State Wildlife Area
Miller Ranch State Wildlife Area
Mitani-Tokuyasu State Wildlife Area
Mogensen Ponds State Wildlife Area
Mount Evans State Wildlife Area
Mount Ouray State Wildlife Area
Mount Shavano State Wildlife Area and State Fish Unit (state fish hatchery)
Mount Werner State Wildlife Area (fishing easement)
Mountain Home Reservoir State Wildlife Area
Murphy State Wildlife Area (fishing lease)
Nakagawa State Wildlife Area
Narraguinnep Reservoir State Wildlife Area
Narrows State Wildlife Area
North Fork State Wildlife Area
North Lake State Wildlife Area
Oak Ridge State Wildlife Area
Odd Fellows State Wildlife Area (fishing lease)
Ogden-Treat State Wildlife Area
Orchard Mesa State Wildlife Area
Overland Trail State Wildlife Area
Owl Mountain State Wildlife Area
Oxbow State Wildlife Area
Paddock State Wildlife Area
Parachute Ponds State Wildlife Area
Parvin Lake State Wildlife Area
Pastorius Reservoir State Wildlife Area
Perins Peak State Wildlife Area
Piceance State Wildlife Area - Little Hills Unit
Piceance State Wildlife Area - North Ridge Unit
Piceance State Wildlife Area - Square S Ranch Unit
Piceance State Wildlife Area - Square S Summer Range
Piceance State Wildlife Area - Yellow Creek
Pikes Peak State Wildlife Area (bison reserve)
Pitkin State Fish Unit (state fish hatchery)
Plateau Creek State Wildlife Area
Playa Blanca State Wildlife Area
Pony Express State Wildlife Area
Poudre River State Fish Unit (state fish hatchery)
Poudre River State Wildlife Area
Prewitt Reservoir State Wildlife Area
Pueblo Reservoir State Wildlife Area
Pueblo State Fish Unit (Hatchery)
Puett Reservoir State Wildlife Area
Purgatoire River State Wildlife Area
Queens State Wildlife Area
Radium State Wildlife Area
Ralston Creek State Wildlife Area
Ramah State Wildlife Area
Red Dog State Wildlife Area
Red Lion State Wildlife Area
Red Mountain State Wildlife Area
Reddy State Wildlife Area
Richard State Wildlife Area
Rifle Falls State Fish Unit (state fish hatchery)
Rio Blanco Lake State Wildlife Area
Rio Grande River State Wildlife Area (Del Norte fishing easement)
Rio Grande State Wildlife Area
Rito Hondo Reservoir State Wildlife Area
Road Canyon Reservoir State Wildlife Area
Roaring Fork State Wildlife Area - Burry Parcel
Roaring Fork State Wildlife Area - Gianinetti Parcel
Roaring Fork State Wildlife Area - Wheeler/Dag Parcels
Roaring Judy State Fish Unit (state fish hatchery)
Rock Creek State Wildlife Area
Rocky Ford State Wildlife Area
Roeber State Wildlife Area
Rosemont Reservoir State Wildlife Area
Runyon / Fountain Lakes State Wildlife Area
Russell Lakes State Wildlife Area
San Luis Lakes State Wildlife Area
San Miguel State Wildlife Area
Sanchez Reservoir State Wildlife Area
Sand Draw State Wildlife Area
Sands Lake State Wildlife Area
Sandsage State Wildlife Area
Sapinero State Wildlife Area
Sarvis Creek State Wildlife Area
Sawhill Ponds State Wildlife Area
Sedgwick Bar State Wildlife Area
Sego Springs State Wildlife Area
Setchfield State Wildlife Area
Seymour Lake State Wildlife Area
Sharptail Ridge State Wildlife Area
Shriver-Wright State Wildlife Area
Simmons State Wildlife Area
Simpson Ponds State Wildlife Area
63 Ranch State Wildlife Area
Skaguay Reservoir State Wildlife Area
Smith Lake State Wildlife Area at Wellington Reservoir Number 4
Smith Reservoir State Wildlife Area
Smyth State Wildlife Area (fishing lease)
South Republican State Wildlife Area - Bonny Parcel
South Republican State Wildlife Area - Kleweno Parcel
Spanish Peaks State Wildlife Area
Spinney Mountain State Wildlife Area
Spring Creek Reservoir State Wildlife Area
Stalker Lake State Wildlife Area
Steamboat Springs State Wildlife Area (fishing easement)
Storm Mountain Access Road
Summit Reservoir State Wildlife Area
Tamarack Ranch State Wildlife Area
Tarryall Reservoir State Wildlife Area
Taylor River State Wildlife Area
Terrace Reservoir State Wildlife Area
Teter-Michigan Creek State Wildlife Area
Thurston Reservoir State Wildlife Area
Tilman Bishop State Wildlife Area
Timpas Creek State Wildlife Area
Tomahawk State Wildlife Area
Tomichi Creek State Wildlife Area
Totten Reservoir State Wildlife Area
Trujillo Meadows State Wildlife Area
Turks Pond State Wildlife Area
Twin Sisters State Wildlife Area
Two Buttes Reservoir State Wildlife Area
Vail Deer Underpass State Wildlife Area
Van Tuyl State Wildlife Area - Cabin Creek Parcel
Van Tuyl State Wildlife Area - Lost Canyon Parcel
Verner State Wildlife Area (fishing lease)
Wahatoya State Wildlife Area, includes Daigre Lake
Walker State Wildlife Area
Ward Road Pond State Wildlife Area
Watson Lake State Wildlife Area
Waunita Watchable Wildlife Area
Webster State Wildlife Area
Wellington State Wildlife Area
West Bank State Wildlife Area
West Lake State Wildlife Area (Larimer County, Colorado)
West Lake State Wildlife Area (Mesa County, Colorado)
West Rifle Creek State Wildlife Area
Wheeler State Wildlife Area
Whitehorse State Wildlife Area
Williams Creek Reservoir State Wildlife Area
Willow Creek State Wildlife Area
Windy Gap Watchable Wildlife Area
Woods Lake State Wildlife Area
Wray State Fish Unit (state fish hatchery)
Wright's Lake State Wildlife Area
Yampa River State Wildlife Area
Please see Colorado State Wildlife Areas for a current list of the state wildlife areas in Colorado.


== See also ==

State of Colorado
Colorado Department of Natural Resources
Colorado Parks and Wildlife
List of Colorado Natural Areas
List of Colorado state parks
List of Colorado state wildlife areas

List of Areas of Critical Environmental Concern in Colorado
List of Colorado scenic and historic byways
List of Colorado trails
List of federal lands in Colorado
List of National Register of Historic Places in Colorado


== References ==


== External links ==
State of Colorado
Colorado Department of Natural Resources
Colorado Parks and Wildlife
Colorado State Wildlife Areas