National Wildlife Refuge is a designation for certain protected areas of the United States managed by the United States Fish and Wildlife Service. The National Wildlife Refuge System is the system of public lands and waters set aside to conserve America's fish, wildlife and plants. Since President Theodore Roosevelt designated Florida's Pelican Island National Wildlife Refuge as the first wildlife refuge in 1903, the System has grown to over 560 national wildlife refuges and other units of the Refuge System, plus 38 wetland management districts encompassing more than 150,000,000 acres (607,028 km2).


== Background ==
The mission of the Refuge System is to manage a national network of lands and waters for the conservation, management, and where appropriate, restoration of fish, wildlife and plant resources and their habitat. The Refuge System maintains the biological integrity, diversity and environmental health of these natural resources for the benefit of present and future generations of Americans.
National Wildlife Refuges manage a full range of habitat types, including wetlands; prairies; coastal and marine areas; and temperate, tundra and boreal forests. The management of each habitat is a complex web of controlling or eradicating invasive species, using fire in a prescribed manner, assuring adequate water resources, and assessing external threats like development or contamination.
Among these hundreds of national refuges are home to some 700 species of birds, 220 species of mammals, 250 reptile and amphibian species and more than 1000 species of fish. Endangered species are a priority of National Wildlife Refuges in that nearly 60 refuges have been established with the primary purpose of conserving 280 threatened or endangered species.
National Wildlife Refuges are also places where visitors can participate in a wide variety of outdoor recreational activities. The National Wildlife Refuge System welcomes more than 45 million visitors each year, generating over $1.7 billion dollars and creating approximately 27,000 jobs for local economies. The Refuge System manages six wildlife-dependent recreational uses in accordance with the National Wildlife Refuge System Improvement Act of 1997, including hunting, fishing, birding, photography, environmental education, and interpretation. Hunters visit more than 350 hunting programs on refuges and on about 36,000 Waterfowl Production Areas. Opportunities for fresh or saltwater fishing are available at more than 340 refuges. There is at least one wildlife refuge in each of the fifty states.
The National Wildlife Refuge System has employees who have a passion for conserving America's natural heritage. They exercise a wide range of skills in planning, biological monitoring and habitat conservation, contaminants management, visitor services, outreach and environmental education, heavy equipment operation, law enforcement, fire management and more. Opportunities are available for graduate and undergraduate students, as well as individuals at every level of the career ladder
The National Wildlife Refuge System is dealing with such issues as urban intrusion/development, habitat fragmentation, degradation of water quantity and quality, climate change, invasive species, increasing demands for recreation, and increasing demands for energy development. The system has had numerous successes, including providing a habitat for endangered species, migratory birds, plants and numerous other valuable animals; implementation of the NWRS Improvement Act, acquisition and protection of key critical inholdings, and establishing leadership in habitat restoration and management.
The agency has created Comprehensive Conservation Plans (CCPs) for each refuge, developed through consultation with private and public stakeholders. These began a review process by stakeholders beginning in 2013. The CCCPs must be consistent with the Fish and Wildlife Service (FWS) goals for conservation and wildlife management.
The CCPs outline conservation goals for each refuge for fifteen years into the future, with the intent that they will be revised every fifteen years thereafter. The comprehensive conservation planning process requires several phases, including: a scoping phase, in which each refuge holds public meetings to identify the public’s main concerns; plan formulation, when refuge staff and FWS planners identify the key issues and refuge goals; writing the draft plan, in which wildlife and habitat alternatives are developed, and the plan is submitted for public review; revision of the draft plan, which takes into consideration the public’s input; and plan implementation.
Each CCP is required to comply with the National Environmental Policy Act (NEPA) and must contain several potential alternatives to habitat and wildlife management on the refuge, and identify their possible effects on the refuge. Additionally, NEPA requires FWS planners and refuge staff to engage the public in this planning process to assist them with identifying the most appropriate alternative.
Comprehensive Conservation Plans are important documents for guiding wildlife and habitat management on national wildlife refuges. The completed CCPs are available to the public and can be found on the FWS website.


== History ==


== Statistics (as of 2008) ==


=== Physical features ===
Area of land under management = 95,972,133 acres (388,385 km2)
Area of wetlands = 23,952,089 acres (96,931 km2)
Number of management units = 586 refuges
Number of Wilderness areas = 75
Area of Wilderness = 20,699,257 acres (83,767 km2)
Length of rivers within the National Wild & Scenic Rivers System = 1,086 miles (1,748 km)
Length of refuge boundary with Mexico = 120 miles (190 km)


=== Management ===
Area of wetlands restored in 2008 = 28,186 acres (114 km2)
Area burned in 2008 to reduce hazardous fuels = 436,422 acres (1,766 km2) (WUI 116,134 acres (470 km2) ; non-WUI 234,638 acres (950 km2); other 85,650 acres (347 km2))


=== Visitation ===
Total visitors in 2008 = 41,255,144
Total visits in 2008 = 75,237,594


=== Volunteers ===
Total volunteers in 2008 = 35,833
Total volunteer hours in 2008 = 1,389,886
Value of volunteer hours in 2008 = $27,116,675.86 (based on Independent Sector's current dollar value of $19.51)


=== Personnel ===
Total staff = (2004 total, awaiting 2008 figures) 3,809 FTE’s (full-time equivalents, thus two half-time employees count as one FTE)
Number of refuge enforcement officers = 402 (includes 259 full-time and 143 dual function employees) (source: Washington Office)
Number of firefighter = 538 FTE's (accounts for 800 firefighters—3000 additional non-fire-funded personnel with fire qualifications)(source: NIFC)


== Special Management Areas ==
In addition to refuge status, the "special" status of lands within individual refuges may be recognized by additional designations, either legislatively or administratively. Special designation may also occur through the actions of other legitimate agencies or organizations. The influence that special designations may have on the management of refuge lands and waters may vary considerably.
There is a wide variety of special land designations that currently overlay national wildlife refuges that total 175 refuges. Authority for designation of some special management area types (e.g., Research Natural Areas) on refuges lies solely with the Service. For most special management area types, responsibility is held by or shared with others.
Among the other varied special management area types found on refuges are Cultural Resource Sites, Historic Sites, Research Natural Areas, Wilderness, Wild and Scenic Rivers, National Natural Landmarks and National Trails. Some overlay designations may place refuges within international networks of protected lands, such as Western Hemisphere Shorebird Reserves and Wetlands of International Importance (Ramsar Convention).
Refuges may also be included within much larger special management areas designated by other agencies or organizations, such as Western Hemisphere Shorebird Reserves, National Marine Sanctuaries, Estuarine Sanctuaries and Biosphere Reserves. Management policy and procedural guidance for Service special management areas is currently found in the Refuge Manual. Revised guidance is in preparation for incorporation into the new Service Manual. Special management area training for administrators and refuge managers currently includes multi-agency sponsored and university correspondence courses.


== See also ==
List of National Wildlife Refuges
List of National Wildlife Refuges established for endangered species
List of largest National Wildlife Refuges
Timeline of environmental events
Pacific Island Wildlife Refuges
Congressional Wildlife Refuge Caucus
United States Fish and Wildlife Service
National Wildlife Refuge Association
State wildlife trails (United States)
Wilderness preservation systems in the United States


== References ==


== Further reading ==
Fischman, Robert (Fall 2005). "The Significance of National Wildlife Refuges in the Development of U.S. Conservation Policy". Journal of Land Use and Environmental Law 21:1–?. Indiana Legal Studies Research Paper No. 19. doi:10.2139/ssrn.699482.
Schroeder, R. (2008). "Comprehensive conservation planning and ecological sustainability within the United States National Wildlife Refuge System". Sustainability: Science, Practice, & Policy 4(1):38–44.


== External links ==
Official website of the National Wildlife Refuge System
Legislation
National Wildlife Refuge Association—A nonprofit entity supporting the National Wildlife Refuge System
The National Wildlife Refuge Association
NWRS - Refuge Planning
The Cooperative Alliance for Refuge Enhancement (CARE)
The short film National Wildlife Refuge System (2005) is available for free download at the Internet Archive
The short film Teddy Roosevelt - National Wildlife Refuge System (2005) is available for free download at the Internet Archive