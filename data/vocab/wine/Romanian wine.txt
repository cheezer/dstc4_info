Romania is one of the world's largest wine producers – in 2009 it produced around 610,000 tons of wine. In recent years, Romania has attracted many European business people and wine buyers, due to the affordable prices of both vineyards and wines compared to other wine producing nations such as France, Germany, and Italy.


== History ==

Romania has one of the oldest wine making traditions in the world, its viticulture dating back more than 6000 years. Due to the hot dry summers, the location proved to be successful and the grape vineyards thrived. Since the medieval times, wine has been the traditional alcoholic beverage of the Romanians.
Later on, during the medieval ages, Saxons emigrated to Romania, bringing along with them different variations of Germanic grape vines. However, by the 19th century, most of these grape vines were replaced by grapes from Western Europe.
In the 1880s, phylloxera (a pale yellow sap-sucking insect that attacks the roots of vines) arrived in Romania from North America. The phylloxera wiped out a majority of Europe's vineyards, including those in Romania. Eventually, many of the Romanian vines were replaced by those imported from France and other foreign nations, such as Merlot, Chardonnay, and Pinot noir.
In 2008, Romania was the twelfth largest wine producing country in the world, and, in 2009, the eleventh largest.


== Wine producing regions ==
Cotnari
Dealu Mare
Jidvei
Murfatlar
Minis - Maderat
Panciu
Odobeşti
Cotești
Recaș
Târnave
Vânju Mare
Bucium
Dragasani


== Wines ==

Whites
Grasă de Cotnari
Fetească albă and Fetească regală; Fetească is the most spread and known typical Romanian wine.
Galbenă de Odobeşti

Reds
Băbească Neagră
Fetească neagră
Negru de Dragasani

Aromatics
Tămâioasă Românească
Busuioacă de Bohotin

Others
Zaibăr is a wine originated from Romania. It is made in Oltenia, a region in the southwest of the country. The color of the wine is dark red.
Sânge de Taur is a wine originated from Vrancea County. The color of the wine is dark red.
Frâncușă


== See also ==
Cuisine of Romania
Old World wine


== References ==