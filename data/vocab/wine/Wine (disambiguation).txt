Wine is an alcoholic beverage.
Wine may also refer to:


== Entertainment ==
Wine (1913 film), a short comedy film starring Fatty Arbuckle
Wine (1924 film), an American comedy-drama film directed by Louis J. Gasnier
Red Wine (film), a 2013 Malayalam investigative film directed by Salam Bappu
WINE (AM), a radio station in the United States


== People ==
Wine (surname)
Wine (bishop) (died before 672), English bishop


== Technology ==
LG Wine Smart, an Android flip phone
Wine (software), a program that runs Microsoft Windows applications on Unix-like operating systems


== Other ==
Wine (color), or burgundy
Vanilla Wine and Super Wine, two varieties of shortbread biscuit common in New Zealand