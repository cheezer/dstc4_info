Wink-Loving Independent School District is a public school district based in Wink, Texas (USA) in Winkler County, Texas (USA). The district serves students in southern and western Winkler County along with all of Loving County.
In 2009, the school district was rated "academically acceptable" by the Texas Education Agency.


== Schools ==
Located in western Winkler County, the district has two campuses:
Wink High School (Grades 7-12) and
Wink Elementary (Grades PK-6).


== References ==


== External links ==
Official website