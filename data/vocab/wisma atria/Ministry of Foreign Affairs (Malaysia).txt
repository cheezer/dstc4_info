The Ministry of Foreign Affairs, Malaysia (Malay: Kementerian Luar Negeri), abbreviated KLN, is the Malaysian government ministry which oversees the foreign relations of Malaysia. The current ministry is based in Putrajaya with Datuk Seri Anifah Aman as Minister, Datuk Hamzah Zainuddin as the Deputy Minister. The ministry has its own building called Wisma Putra.


== Functions ==
The Ministry of Foreign Affairs is responsible for managing bilateral, regional and multilateral relations with foreign countries and international organisations. It is responsible for the formulation, planning, and implementation of the foreign policy of Malaysia. It is also responsible for managing external relations with regard to bilateral political and economic affairs, multilateral relations, ASEAN regional cooperation, human resources management, finance, administration and consular, protocol affairs and Malaysian mission abroad.


== History ==
The origin of the Ministry of Foreign Affairs began before Malaysia's independence in 1957. The groundwork for the establishment of the Ministry of External Affairs (MEA), as it was initially called, was initiated a year prior to Independence particularly with through the training of a batch of eleven diplomats to man the country's diplomatic missions overseas. This pioneering group was trained in the United Kingdom and Australia.
The Ministry of External Affairs was modeled after the British Foreign Office.
Initially, Malaysia had diplomatic missions in London, Washington, Canberra, New York, New Delhi, Jakarta and Bangkok. In 1963, there were fourteen Malaysian missions and twenty-five countries were represented in Malaysia (four by way of concurrent accreditation).
In 1965, the diplomatic machinery of Malaysia faced its first major reorganisation. In 1966, there was an accelerated growth pattern of the Foreign Ministry particularly with regard to the personnel and the financial allocation for its activities. That year also witnessed a change in the designation of MEA to the preferred terminology of "Ministry of Foreign Affairs" and also saw the physical relocation and consolidation of the Ministry. From its original premises at the Sultan Abdul Samad Building, the Ministry moved to Wisma Putra. The Wisma Putra Complex is based on a combination of both traditional and modern architecture.


== Former Foreign Ministers ==
The following are the former Foreign Ministers of Malaysia:
31 August 1957 – 2 February 1959: Tunku Abdul Rahman Putra Al-Haj
3 February 1959 – 31 August 1960: Tun Dr. Ismail Abdul Rahman
1 September 1960 – 22 September 1970: Tunku Abdul Rahman Putra Al-Haj
23 September 1970 – 12 August 1975: Tun Abdul Razak Hussein
13 August 1975 – 16 July 1981: Tengku Ahmad Rithauddeen Al-Haj Tengku Ismail
17 July 1981 – 16 July 1984: Tun Dr. Mohd. Ghazali Shafie
17 July 1984 – 10 August 1986: Tengku Ahmad Rithauddeen Al-Haj Tengku Ismail
20 May 1987 – 14 March 1991: Tan Sri Abu Hassan Omar
15 March 1991 – 8 January 1999: Tun Abdullah Ahmad Badawi
9 January 1999 – 18 March 2008: Dato' Seri Syed Hamid Albar
19 March 2008 – 8 April 2009: Dato' Seri Utama Dr. Rais Yatim
9 April 2009- present: Dato' Seri Dr. Anifah Aman


== Notes ==


== External links ==
Official website