Crab Rangoon are deep-fried dumpling appetizers served in American Chinese and, more recently, Thai restaurants, stuffed with a combination of cream cheese, lightly flaked crab meat (more commonly, canned crab meat or imitation crab meat), with scallions, and/or garlic. These fillings are then wrapped in Chinese wonton wrappers in a triangular or flower shape, then deep fried in vegetable oil.


== HistoryEdit ==
Crab Rangoon has been on the menu of the "Polynesian-style" restaurant Trader Vic's in San Francisco since at least 1956. Although the appetizer is allegedly derived from an authentic Burmese recipe, the dish was probably invented in the United States. A "Rangoon crab a la Jack" was mentioned as a dish at a Hawaiian-style party in 1952, but without further detail, and so may or may not be the same thing.
Though the history of crab Rangoon is unclear, cream cheese, like other cheese, is essentially nonexistent in Southeast Asian and Chinese cuisine, so it is unlikely that the dish is actually of east or southeast Asian origin. In North America, crab Rangoon is often served with soy sauce, plum sauce, duck sauce, sweet and sour sauce, or mustard for dipping.


== NamesEdit ==
In the Pacific Northwest states of America crab Rangoon are also known as crab puffs, although this primarily refers to versions that use puff pastry as a wrapper instead of wonton. They may also be referred to as crab pillows, crab cheese wontons, or cheese wontons.


== See alsoEdit ==
Jau gok
Curry beef triangle
Crab puff
List of crab dishes
List of deep fried foods
List of hors d'oeuvre
List of seafood dishes
 Food portal


== ReferencesEdit ==