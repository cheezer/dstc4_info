Woodlands is a rural municipality in the province of Manitoba in Western Canada.


== Communities ==
Erinview
Lake Francis
Marquette
Reaburn
Warren
Woodlands


== See also ==
Twin Lakes Beach, Manitoba


== External links ==
Official website


== References ==
Manitoba Historical Society - Rural Municipality of Woodlands
Map of Woodlands R.M. at Statcan