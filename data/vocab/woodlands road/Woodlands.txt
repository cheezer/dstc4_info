Woodlands may refer to:
Woodland, a low-density forest


== Geography ==


=== Australia ===
Woodlands, New South Wales
Woodlands, Ashgrove, one of two heritage-listed houses named Woodlands in Queensland, and associated with John Henry Pepper
Woodlands, Marburg, other of two heritage-listed houses named Woodlands in Queensland, and associated with Thomas Lorimer Smith
Woodlands, Western Australia


=== Canada ===
Woodlands, Calgary, a neighbourhood in Alberta, Canada
Woodlands County, a municipal district in Alberta, Canada
Woodlands, North Vancouver
Woodlands, Ontario
Woodlands, Manitoba
Rural Municipality of Woodlands, a rural municipality in Manitoba


=== United Kingdom ===
Woodlands, Dorset, England
Woodlands, Falkirk, Scotland
Woodlands, Glasgow, Scotland
Woodlands, Somerset, England
Woodlands, South Yorkshire, England
Woodlands, several other United Kingdom locations


=== United States ===
Woodlands (Gosport, Alabama)
Woodlands, California, a census-designated place
Barnsley Gardens, a plantation formerly known as Woodlands in Adairsville, Georgia
Woodlands and Blythewood, Clarkesville, Georgia, a National Register of Historic Places listing in Habersham County, Georgia
Woodlands Historic District, Lexington, Kentucky, a National Register of Historic Places listing in Fayette County, Kentucky
Woodlands (Perryville, Maryland)
The Woodlands (Philadelphia, Pennsylvania), a historic mansion and cemetery
Woodlands (Bamberg, South Carolina)
Woodlands (Columbia, South Carolina), a National Register of Historic Places listing in Richland County, South Carolina
The Woodlands, Texas
Woodlands (Charlottesville, Virginia), a National Register of Historic Places listing in Albemarle County, Virginia
Woodlands, West Virginia


=== Elsewhere ===
Woodlands, New Zealand
Woodlands, Singapore
Woodlands MRT Station
Woodlands Regional Bus Interchange

Woodlands, Gauteng, South Africa


== Other uses ==
Woodlands Christian Centre
Woodlands Historic Park near Melbourne Airport
Woodland period, indigenous cultures from ca. 1000 BCE—1000 CE in the eastern part of North America


== See also ==
Eastern Woodlands, a cultural area of indigenous North Americans
Woodland (disambiguation)
Woodlands School (disambiguation)