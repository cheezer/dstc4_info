Woodleigh is a village, parish and former manor located in the South Hams region of the county of Devon, England.


== Etymology ==
The name is derived from the Old English "leah" meaning a clearing in a forest.


== History ==
The manor is listed in the Domesday Book of 1086 as Odelie and was the 13th of the 17 holdings of Robert of Aumale (Latinised to Robertus de Albemarle). It then formed part of his demesne lands.


== Demographics ==
The population of Woodleigh is approximately 150.


== Amenities ==
Whilst there was once a small school in the village now this has been converted into a house so that the children of the village must travel to the much larger village of Loddiswell to receive their education. Woodleigh has no shops, pubs or sports facilities but it does have a postbox.


== Historic estates ==
The parish includes the following historic estates:


=== Wood ===
Wood was held by a junior branch of the Fortescue family whose earliest known seat in Devon was Wympstone in the parish of Modbury. Sir Henry Fortescue (fl. 1426), Lord Chief Justice of the Common Pleas in Ireland, married as his first wife Jane Bozun, daughter of Edmond Bozun of Wood, and Wood became the residence of his son and heir John Fortescue, and passed to his male descendants for three generations. The only daughter and heiress of John's great-grandson Anthony Fortescue of Wood was Joan Fortescue, who married her cousin John Fortescue (d.1587) of Preston, Devon. The Fortescue Baronetcy, "of Woodleigh in the County of Devon", was created in the Baronetage of England on 29 January 1667 for Peter Fortescue (1620-1685) of Wood in the parish of Woodleigh, 3rd son of Francis Fortescue (d.circa 1649) of Preston, Devon, descended from John Fortescue (d.1479) of Wympstone in the parish of Modbury, Devon, MP for Totnes, Tavistock and Plympton. The title became extinct on his death in 1685.


== References ==
^ Thorn, Caroline & Frank, (eds.) Domesday Book, (Morris, John, gen.ed.) Vol. 9, Devon, Parts 1 & 2, Phillimore Press, Chichester, 1985, Chap 28, 13
^ Risdon, Tristram (d.1640), Survey of Devon, 1811 edition, London, 1811, with 1810 Additions, p.181
^ Vivian, p.353
^ Vivian, Lt.Col. J.L., (Ed.) The Visitation of the County of Devon: Comprising the Heralds' Visitations of 1531, 1564 & 1620, Exeter, 1895, pp.353, 357