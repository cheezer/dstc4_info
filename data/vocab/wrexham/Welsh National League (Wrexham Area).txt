The Welsh National League (Wrexham Area) is a football league in Wales and forms level 3 of the Welsh football league system in Flintshire and Wrexham County Borough, but some teams are from Denbighshire (Corwen and Llangollen Town) and Gwynedd (Llanuwchllyn). For sponsorship purposes it is currently known as the Guy Walmsley & Co Welsh National League.


== History ==
The League grew out of the Wrexham and District League, which ran during the early years of the twentieth century. At that time the senior clubs in the Wrexham area played in English leagues such as The Combination and the Birmingham & District League. Their reserve sides, along with local amateur teams, contested the Wrexham and District League.
In the inter-war years, the new Welsh National League with its various sections was organised. Clubs from the Wrexham area, and a little beyond, joined the Northern Section.
The financial strain proved too severe for several of the clubs in the Wrexham district, and after World War II they re-organised as the Welsh National League (Wrexham Area).


== Member clubs 2014–15 ==


== Divisional Champions ==
The league has undergone various restructures in its history, with the number of divisions and their names changing.
For its first season the league had just one division, the Senior Division
In 1946 two regional divisions were introduced below the Senior Division
In 1949 the regionalised divisions were merged together and the resulting three divisions renamed Divisions One, Two and Three
In 1953–54 Division Three was not contested due to a lack of clubs
In 1954 Division Three was revived
In 1960 Division Three was discontinued once again
In 1962 Division Three was revived once again
In 1969 Division Three was split into two sections, A and B
In 1970 the two Division Three sections were reorganised into Divisions Three and Four
In 1974 the four divisions were reduced to just two
In 1975 Division Three was reintroduced
In 1976 Division Three was discontinued once again
In 1978 Divisions Three and Four were revived
In 1978 Division Four was discontinued once again
In 1980 Division Four was once again revived
In 1983 Division Four was split into two sections, North and South
In 1983 the two Division Four sections were merged back together and the four divisions renamed Premier, One, Two and Three
In 1990 Division Three was discontinued
In 1999 Division Three was revived
In 2008, all reserve and colts teams were split off, leaving two divisions for first teams and two divisions for lower teams


== External links ==
League website


== See also ==
Football in Wales
Welsh football league system
Welsh Cup
List of football clubs in Wales
List of stadiums in Wales by capacity