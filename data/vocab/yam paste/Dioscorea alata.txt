Dioscorea alata, known as purple yam and many other names, is a species of yam, a tuberous root vegetable. The tubers are usually bright lavender in color, hence the common name, but they may sometimes be white. It is sometimes confused with taro and the Okinawa sweet potato (Ipomoea batatas cv. Ayamurasaki), although D. alata is also grown in Okinawa where it is known as beniimo (紅芋). With its origins in the Asian tropics, D. alata has been known to humans since ancient times.


== Common names ==
Because it has become naturalized throughout tropical South America, Africa, Australia, the US southeast, D. alata has many different common names from these regions. In English alone, aside from purple yam, other common names include greater yam, Guyana arrowroot, ten-months yam, water yam, white yam, winged yam, or simply yam. In other cultures and languages it is known variously as ratalu or violet yam in India, rasa valli kilangu (இராசவள்ளிக்கிழங்கு) in Tamil, kondfal (कोंदफळ) in Marathi, kachil (കാച്ചില്‍) in Malayalam, and khoai mỡ in Vietnam. For the Igbo people of southern Nigeria, it is called ji or ji abana; while for the Yoruba people of the southwestern Nigeria, it is called isu ewura.
Malayo-Polynesian languages
*qube / *ʔube can be reconstructed as the Proto-Malayo-Polynesian word for D. alata, and words descended from it are found throughout the geographic range of this widespread language family, though in some daughter languages they are generalized or transferred to other root crops. Examples include Tagalog ube or ubi, Indonesian and Malay ubi**, Malagasy ovy**, Fijian uvi, Tongan ʻufi, Samoan ufi**, as well as Māori and Hawaiian uhi. D. alata was one of the canoe plants that the Polynesians brought with them when they settled new islands.
** Not specific to D.alata


== Uses ==


=== Culinary ===

Purple yam is used in a variety of desserts, as well as a flavor for ice cream, milk, Swiss rolls, tarts, cookies, cakes, and other pastries. In the Philippines, it is known as ube and is often eaten boiled or as a sweetened jam called ube halayá; the latter is a popular ingredient in the iced dessert called halo-halo. In Maharashtra, the stir-fried chips are eaten during religious fasting. Purple yam is an essential ingredient in Undhiyu. Purple yam is a popular desert in Jaffna, Sri Lanka.
D. alata is valued for the starch that can be processed from it.


=== Medicinal ===
In folk medicine, D. alata has been used as a laxative and vermifuge, and as a treatment for fever, gonorrhea, leprosy, tumors, and inflamed hemorrhoids.


=== Other uses ===
D. alata is sometimes grown in gardens for its ornamental value.


== Weed problems ==
Dioscorea alata is native to Southeast Asia (Indochina, Philippines, Indonesia, etc.) and surrounding areas (Taiwan, Ryukyu Islands, Assam, Nepal, New Guinea, Christmas Island). It has escaped into the wild in many other places, becoming naturalized in parts of China, Africa, Madagascar, the Western Hemisphere, and various islands in the Indian and Pacific Oceans. It persists in the wild in the United States in Louisiana, Georgia, Alabama, Florida, Puerto Rico, and the U.S. Virgin Islands. It is considered an invasive species, at least in Florida.


== References ==


== External links ==
"Dioscorea alata". Integrated Taxonomic Information System. Retrieved 18 February 2006. 
Sri Lanka Rasa Valli Pudding
Food Glossary, tarladalal.com
Images from Forestry Images (webpages from the University of Georgia’s Center for Invasive Species and Ecosystem Health)