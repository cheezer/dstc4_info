Yam khai dao (Thai: ยำไข่ดาว, pronounced [jām kʰàj dāːw], "fried-egg spicy salad") is a Thai dish made out of fried chicken or duck eggs. It is an easy-to-prepare food, but it cannot usually be purchased in restaurants. It is easy to find ingredients which are nutritious that are beneficial to health. It contains many vitamins, such as vitamin B2 (riboflavin), vitamin B12 and vitamin D.


== References ==