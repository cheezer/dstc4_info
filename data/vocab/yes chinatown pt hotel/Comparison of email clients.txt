The following tables compare general and technical features of a number of email client programs. Please see the individual products' articles for further information.


== General ==
Basic general information about the clients: creator/company, O/S, licence, & interface. Clients listed on a light purple background are no longer in active development.


== Release history ==
A brief digest of the release histories.


== Operating system support ==
The operating systems on which the clients can run natively (without emulation).


== Protocol support ==


=== Communication and access protocol support ===
What email and related protocols and standards are supported by each client.


=== Authentication support ===


=== SSL and TLS support ===


== Features ==
Information on what features each of the clients support.


=== General features ===
For all of these clients, the concept of "HTML support" does not mean that they can process the full range of HTML that a web browser can handle. Almost all email readers limit HTML features, either for security reasons, or because of the nature of the interface. CSS and JavaScript can be especially problematic. A number of articles describe these limitations per-email-client; see 1, 2, 3, and 4


=== Messages features ===


=== Database, folders and customization ===


=== Templates, scripts and programming languages ===


== Internationalization ==
The Bat! supports Email Address Internationalization (EAI).
As of April, 2014, there are no email clients that support SMTPUTF8.


== See also ==
Unicode and email
List of personal information managers
Comparison of layout engines
Webmail
Comparison of feed aggregators
Comparison of mail servers
Comparison of webmail providers


== References ==


== External links ==
T&B : Email : Clients (of historical interest—last updated in 1999)
Mail User Agents for Linux Based Platforms (includes many email clients not listed in the above tables)
Remail - research by The Collaborative User Experience group at IBM
a CSS implementation comparison of many well known mail clients