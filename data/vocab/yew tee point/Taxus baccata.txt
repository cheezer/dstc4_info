Taxus baccata is a conifer native to western, central and southern Europe, northwest Africa, northern Iran and southwest Asia. It is the tree originally known as yew, though with other related trees becoming known, it may now be known as English yew, or European yew.


== Taxonomy and namingEdit ==
The word yew is from Proto-Germanic *īwa-, possibly originally a loanword from Gaulish *ivos, compare Irish ēo, Welsh ywen, French if (see Eihwaz for a discussion). Baccata is Latin for bearing red berries. The word yew as it was originally used seems to refer to the color brown. The yew (μίλος) was known to Theophrastus, who noted its preference for mountain coolness and shade, its evergreen character and its slow growth.
Most romance languages, with the notable exception of French, kept a version of the Latin word taxus (Italian tasso, Corsican tassu, Occitan teis, Catalan teix, Gasconic tech, Spanish tejo, Portuguese teixo, Galician teixo and Romanian tisă) from the same root as toxic. In Slavic languages, the same root is preserved: Russian tiss (тис), Slovakian tis, Slovenian tisa, Bosnian tisa (тиса). In Albanian it is named tis.
The common yew was one of the many species first described by Linnaeus. It is one of around 30 other conifer species in seven genera in the family Taxaceae, which is placed in the order Pinales.


== DescriptionEdit ==

It is a small to medium-sized evergreen tree, growing 10–20 metres (33–66 ft) (exceptionally up to 28 metres (92 ft)) tall, with a trunk up to 2 metres (6 ft 7 in) (exceptionally 4 metres (13 ft)) diameter. The bark is thin, scaly brown, coming off in small flakes aligned with the stem. The leaves are flat, dark green, 1–4 centimetres (0.39–1.57 in) long and 2–3 millimetres (0.079–0.118 in) broad, arranged spirally on the stem, but with the leaf bases twisted to align the leaves in two flat rows either side of the stem, except on erect leading shoots where the spiral arrangement is more obvious. The leaves are poisonous.
The seed cones are modified, each cone containing a single seed, which is 4–7 millimetres (0.16–0.28 in) long, and partly surrounded by a modified scale which develops into a soft, bright red berry-like structure called an aril. The aril is 8–15 millimetres (0.31–0.59 in) long and wide and open at the end. The arils mature 6 to 9 months after pollination, and with the seed contained, are eaten by thrushes, waxwings and other birds, which disperse the hard seeds undamaged in their droppings. Maturation of the arils is spread over 2 to 3 months, increasing the chances of successful seed dispersal. The seeds themselves are poisonous and bitter, but are opened and eaten by some bird species including Hawfinches, Greenfinches and Great Tits. The aril is not poisonous, but is gelatinous and very sweet tasting. The male cones are globose, 3–6 millimetres (0.12–0.24 in) diameter, and shed their pollen in early spring. The yew is mostly dioecious, but occasional individuals can be variably monoecious, or change sex with time.


== LongevityEdit ==
Taxus baccata can reach 400 to 600 years of age. Some specimens live longer but the age of yews is often overestimated. Ten yews in Britain are believed to predate the 10th century. The potential age of yews is impossible to determine accurately and is subject to much dispute. There is rarely any wood as old as the entire tree, while the boughs themselves often become hollow with age, making ring counts impossible. There are claims as high as 5,000–9,500 years, but other evidence based on growth rates and archaeological work of surrounding structures suggests the oldest trees (such as the Fortingall Yew in Perthshire, Scotland) are more likely to be in the range of 2,000 years. Even with this lower estimate, Taxus baccata is one of the longest-living plants in Europe. One characteristic contributing to its longevity is that it is able to split under the weight of advanced growth without succumbing to disease in the fracture, as do most other trees. Another is its ability to give rise to new epicormic and basal shoots from cut surfaces and low on its trunk, even at an old age.


== Significant treesEdit ==

The Fortingall Yew in Perthshire, Scotland, has the largest recorded trunk girth in Britain and experts estimate it to be 2,000 to 3,000 years old, although it may be a remnant of a post-Roman Christian site and around 1,500 years old. The Llangernyw Yew in Clwyd, Wales, can be found at an early saint site and is about 1,500 years old. Other well known yews include the Ankerwycke Yew, the Balderschwang Yew, the Caesarsboom, the Florencecourt Yew, and the Borrowdale Fraternal Four, of which poet William Wordsworth wrote. The Kingley Vale National Nature Reserve in West Sussex has one of Europe's largest yew woodlands.

The oldest specimen in Spain is located in Bermiego, Asturias. It is known as Teixu l'Iglesia in the Asturian language. It stands 15 m (49 ft) tall with a trunk diameter of 6.82 m (22.4 ft) and a crown diameter of 15 m. It was declared a Natural Monument on April 27, 1995 by the Asturian Government and is protected by the Plan of Natural Resources.
A unique forest formed by Taxus baccata and European Box (Buxus sempervirens) lies within the city of Sochi, in the Western Caucasus.


== Allergenic potentialEdit ==
Yews in this genus are primarily separate-sexed, and males are extremely allergenic, with an OPALS allergy scale rating of 10 out of 10. Completely female yews have an OPALS rating of 1, and are considered "allergy-fighting". Male yews bloom and release abundant amounts of pollen in the spring; completely female yews only trap pollen while producing none.


== ToxicityEdit ==
All parts of a yew plant are toxic to humans with the exception of the yew berries (however, their seeds are toxic); additionally, male and monoecious yews in this genus release cytotoxic pollen, which can cause headaches, lethargy, aching joints, itching, and skin rashes; it is also a trigger for asthma. These pollen granules are extremely small, and can easily pass through window screens.
The foliage itself remains toxic even when wilted, and toxicity increases in potency when dried. Ingestion and subsequent excretion by birds whose beaks and digestive systems do not break down the seed's coating are the primary means of yew dispersal. The major toxin within the yew is the alkaloid taxine. Horses have a relatively low tolerance to taxine, with a lethal dose of 200–400 mg/kg body weight; cattle, pigs, and other livestock are only slightly less vulnerable. Several studies have found taxine LD50 values under 20 mg/kg in mice and rats.
Symptoms of yew poisoning include an accelerated heart rate, muscle tremors, convulsions, collapse, difficulty breathing, circulation impairment and eventually cardiac arrest. However, there may be no symptoms, and if poisoning remains undetected death may occur within hours. Fatal poisoning in humans is very rare, usually occurring after consuming yew foliage. The leaves are more toxic than the seed.


== Uses and traditionsEdit ==

One of the world's oldest surviving wooden artifacts is a Clactonian yew spear head, found in 1911 at Clacton-on-Sea, in Essex, UK. It is estimated to be about 450,000 years old.
In the ancient Celtic world, the yew tree (*eburos) had extraordinary importance; a passage by Caesar narrates that Catuvolcus, chief of the Eburones poisoned himself with yew rather than submit to Rome (Gallic Wars 6: 31). Similarly, Florus notes that when the Cantabrians were under siege by the legate Gaius Furnius in 22 BC, most of them took their lives either by the sword, by fire, or by a poison extracted ex arboribus taxeis, that is, from the yew tree (2: 33, 50–51). In a similar way, Orosius notes that when the Astures were besieged at Mons Medullius, they preferred to die by their own swords or by the yew tree poison rather than surrender (6, 21, 1).


=== ReligionEdit ===

The yew is often found in churchyards in England, Wales, Scotland, Ireland, France and northern areas of Spain. In France, the oldest yew trees are almost all located in church yards of Normandy and a chapel was very often laid out in the hollow trunk. Some examples can be found in La Haye-de-Routot or La Lande-Patry. It is said that up to 40 people could stand inside one of the La-Haye-de-Routot yew trees and the Le Ménil-Ciboult yew is probably the largest one (13 m diameter). Indeed some of these trees are exceptionally large (over 5 m diameter) and may be over 2,000 years old. Sometimes monks planted yews in the middle of their cloister, as at Muckross Abbey (Ireland) or abbaye de Jumièges (France). Some ancient yew trees are located at St Mary the Virgin Church, Overton-on-Dee in Wales.
In Asturian tradition and culture the yew tree has had a real link with the land, the people, the ancestors and the ancient religion. It was tradition on All Saints Day to bring a branch of a yew tree to the tombs of those who had died recently so they will find the guide in their return to the Land of Shadows. The yew tree has been found near chapels, churches and cemeteries since ancient times as a symbol of the transcendence of death, and is usually found in the main squares of the villages where people celebrated the open councils that served as a way of general assembly to rule the village affairs.
It has been suggested that the Sacred Tree at the Temple at Uppsala was an ancient yew tree. The Christian church commonly found it expedient to take over existing pre-Christian sacred sites for churches. It has also been suggested that yews were planted at religious sites as their long life was suggestive of eternity, or because being toxic they were seen as trees of death. Another suggested explanation is that yews were planted to discourage farmers and drovers from letting animals wander onto the burial grounds, the poisonous foliage being the disincentive. A further possible reason is that fronds and branches of yew were often used as a substitute for palms on Palm Sunday.
In traditional Germanic paganism, Yggdrasill was often seen as a giant ash tree. Many scholars now agree that in the past an error has been made in the interpretation of the ancient writings, and that the tree is most likely a European yew (Taxus baccata). This mistake would find its origin in an alternative word for the yew tree in the Old Norse, namely Needle Ash (barraskr). In addition, ancient sources, including the Eddas, speak about a vetgrønster vida which means "evergreen tree". An ash sheds its leaves in the winter, while yew trees retain their needles.
Conifers were in the past often seen as sacred, because they never lose their green. In addition, the tree of life was not only an object from the stories, but also believers often gathered around an existing tree. The yew releases gaseous toxins (taxine) on hot days. Taxine is in some instances capable of causing hallucinations. This has some similarities with the story that Odin had a revelation (the wisdom of the runes) after having been hanging from the tree for nine days.


=== MedicinesEdit ===
In 1021, Avicenna introduced the medicinal use of T. baccata for phytotherapy in The Canon of Medicine. He named this herbal drug "Zarnab" and used it as a cardiac remedy. This was the first known use of a calcium channel blocker drug, which were not in wide use in the Western world until the 1960s.
Certain compounds found in the bark of yew trees were discovered by Wall and Wani in 1967 to have efficacy as anti-cancer agents. The precursors of the chemotherapy drug paclitaxel (taxol) can be synthesized easily from the extracts of the leaves of European yew, which is a more renewable source than the bark of the Pacific yew (Taxus brevifolia). This ended a point of conflict in the early 1990s; many environmentalists, including Al Gore, had opposed the harvesting of yew for paclitaxel cancer treatments. Docetaxel can then be obtained by semi-synthetic conversion from the precursors.
In the Central Himalayas, the plant is used as a treatment for breast and ovarian cancer.


=== Woodworking and longbowsEdit ===

Wood from the yew is classified as a closed-pore softwood, similar to cedar and pine. Easy to work, yew is among the hardest of the softwoods; yet it possesses a remarkable elasticity, making it ideal for products that require springiness, such as bows.
Yew is also associated with Wales and England because of the longbow, an early weapon of war developed in northern Europe, and as the English longbow the basis for a medieval tactical system. The oldest surviving yew longbow was found at Rotten Bottom in Dumfries and Galloway, Scotland. It has been given a calibrated radiocarbon date of 4040 BC to 3640 BC and is on display in the National Museum of Scotland. Yew is the wood of choice for longbow making; the heartwood is always on the inside of the bow with the sapwood on the outside. This makes most efficient use of their properties as heartwood is best in compression whilst sapwood is superior in tension. However, much yew is knotty and twisted, and therefore unsuitable for bowmaking; most trunks do not give good staves and even in a good trunk much wood has to be discarded.
The trade of yew wood to England for longbows was so robust that it depleted the stocks of good-quality, mature yew over a vast area. The first documented import of yew bowstaves to England was in 1294. In 1350 there was a serious shortage, and Henry IV of England ordered his royal bowyer to enter private land and cut yew and other woods. In 1423 the Polish king commanded protection of yews in order to cut exports, facing nearly complete destruction of local yew stock. In 1470 compulsory archery practice was renewed, and hazel, ash, and laburnum were specifically allowed for practice bows. Supplies still proved insufficient, until by the Statute of Westminster in 1472, every ship coming to an English port had to bring four bowstaves for every tun. Richard III of England increased this to ten for every tun. This stimulated a vast network of extraction and supply, which formed part of royal monopolies in southern Germany and Austria. In 1483, the price of bowstaves rose from two to eight pounds per hundred, and in 1510 the Venetians would only sell a hundred for sixteen pounds. In 1507 the Holy Roman Emperor asked the Duke of Bavaria to stop cutting yew, but the trade was profitable, and in 1532 the royal monopoly was granted for the usual quantity "if there are that many." In 1562, the Bavarian government sent a long plea to the Holy Roman Emperor asking him to stop the cutting of yew, and outlining the damage done to the forests by its selective extraction, which broke the canopy and allowed wind to destroy neighbouring trees. In 1568, despite a request from Saxony, no royal monopoly was granted because there was no yew to cut, and the next year Bavaria and Austria similarly failed to produce enough yew to justify a royal monopoly. Forestry records in this area in the 17th century do not mention yew, and it seems that no mature trees were to be had. The English tried to obtain supplies from the Baltic, but at this period bows were being replaced by guns in any case.


=== HorticultureEdit ===

Today European yew is widely used in landscaping and ornamental horticulture. Due to its dense, dark green, mature foliage, and its tolerance of even very severe pruning, it is used especially for formal hedges and topiary. Its relatively slow growth rate means that in such situations it needs to be clipped only once per year (in late summer).
Well over 200 cultivars of T. baccata have been named. The most popular of these are the Irish yew (T. baccata 'Fastigiata'), a fastigiate cultivar of the European yew selected from two trees found growing in Ireland, and the several cultivars with yellow leaves, collectively known as "golden yew". In some locations, e.g. when hemmed in by buildings or other trees, an Irish yew can reach 20 feet in height without exceeding 2 feet in diameter at its thickest point, although with age many Irish yews assume a fat cigar shape rather than being truly columnar.
The following cultivars have gained the Royal Horticultural Society's Award of Garden Merit:-
European yew will tolerate growing in a wide range of soils and situations, including shallow chalk soils and shade, although in deep shade its foliage may be less dense. However it cannot tolerate waterlogging, and in poorly-draining situations is liable to succumb to the root-rotting pathogen Phytophthora cinnamomi.
In Europe, Taxus baccata grows naturally north to Molde in southern Norway, but it is used in gardens further north. It is also popular as a bonsai in many parts of Europe and makes a handsome small to large sized bonsai.


=== Musical instrumentsEdit ===
The late Robert Lundberg, a noted luthier who performed extensive research on historical lute-making methodology, states in his 2002 book Historical Lute Construction that yew was historically a prized wood for lute construction. European legislation establishing use limits and requirements for yew limited supplies available to luthiers, but it was apparently as prized among medieval, renaissance, and baroque lute builders as Brazilian Rosewood is among contemporary guitar-makers for its quality of sound and beauty.


== ConservationEdit ==
Clippings from ancient specimens in the UK, including the Fortingall Yew, were taken to the Royal Botanic Gardens in Edinburgh to form a mile-long hedge. The purpose of this "Yew Conservation Hedge Project" is to maintain the DNA of Taxus baccata. The species is threatened by felling, partly due to rising demand from pharmaceutical companies, and disease.


== See alsoEdit ==

List of plants poisonous to equines


== NotesEdit ==


== ReferencesEdit ==
Chetan, A. and Brueton, D. (1994) The Sacred Yew, London: Arkana, ISBN 0-14-019476-2
Conifer Specialist Group (1998) Taxus baccata, In: IUCN 2006/UCN Red List of Threatened Species, WWW page (Accessed 3 February 2007)
Hartzell, H. (1991) The yew tree: a thousand whispers: biography of a species, Eugene: Hulogosi, ISBN 0-938493-14-0
Simón, F. M. (2005) Religion and Religious Practices of the Ancient Celts of the Iberian Peninsula, e-Keltoi, v. 6, p. 287-345, ISSN 1540-4889 online


== External linksEdit ==
Notes on the Yew Druid Network
collect-if.net In France Switzerland and Belgium are yew leaves collected for pharmaceutical use.
Images and location details of ancient yews
Taxus baccata at the Encyclopedia of Life