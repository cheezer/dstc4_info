Yew Chung International School (YCIS) is a co-educational international school based in Hong Kong. The campus is situated in Kowloon Tong. It is operated by Yew Chung Education Foundation, YCIS provides a fully rounded education that shapes students into balanced and responsible citizens of a global community. It is operated by Yew Chung Education Foundation.


== Origins and history ==
The school was founded in 1932. Originally specialising in Early Childhood Education, Yew Chung International School (YCIS) now provides education from early childhood through primary and secondary culminating in the International General Certificate of Secondary Education (IGCSE) and International Baccalaureate (IB) Diploma Programme.


== Mission and philosophy ==
YCIS is committed to offering a global education characterized by: •A fully rounded and balanced education which nurtures the whole child: academically, socially and spiritually; •An emphasis on academic excellence, dual language acquisition and the tools for success in a globalised world; •A bilingual and multicultural learning environment which fosters respect for diversity; •A supportive and nurturing school life which seeks to produce confident, balanced and socially responsible individuals.


== YCIS education programme ==
YCIS’s international curriculum is based on the framework and schemes of work from The National Curriculum for England (NCE). It is a research based curriculum that allows high standards and ease of transition for international students who move from one country to another.


=== Chinese curriculum ===
YCIS places great emphasis on the acquisition of Chinese language (Mandarin or Cantonese for the Year 1's) which is an increasingly important world language. In order to foster a deeper understanding of the language, students also study Chinese culture.


== Fully rounded and balanced education ==
By recognizing the importance of multiple intelligence and individual learning styles, YCIS nurtures all aspects of student growth including the intellectual, artistic, social, spiritual and physical development of each student.


== World Classroom ==
The World Classroom is a unique programme which is a practical method of preparing our students to participate effectively in today’s global society. Students from Years 7 to 9(2013 newest update) learn outside the classroom by travelling abroad with experienced staff. Projects such as building a new school in Tanzania or re-building homes in Thailand give students meaningful encounters with diverse cultures.


== Network of schools ==
Yew Chung International School
Yew Chung International School of Beijing
Yew Chung International School of Shanghai
Yew Chung International School of Chongqing
Yew Chung International School of Qingdao
Yew Chung International School of Hong Kong
Yew Chung International School – Silicon Valley


== References ==


== External links ==
Yew Chung Education Foundation
Yew Chung International School
Yew Chung International School of Beijing
Yew Chung International School of Shanghai
Yew Chung International School of Chongqing
Yew Chung International School of Qingdao
YCIS Silicon Valley