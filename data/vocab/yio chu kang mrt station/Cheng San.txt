Cheng San is a precinct located at Ang Mo Kio, Singapore. It has precincts of Neighbourhood 5. The nearest MRT station is Ang Mo Kio and Yio Chu Kang.


== Politics ==
Cheng San is part of Ang Mo Kio Group Representation Constituency. The division is named Cheng San-Seletar from the 2011 elections onwards and the MP is Ang Hin Kee. Cheng San was formerly under the SMC of the same name, and the hotly contested GRC of the same name, dissolved in the 2001 elections. The division also bounds Seletar as well.


== References ==