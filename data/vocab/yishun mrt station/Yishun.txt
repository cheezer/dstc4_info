Yishun, or Nee Soon as it was initially named, is a suburban town in the northern part of Singapore, encompassing the Yishun Planning Area, in the North Region, which includes Yishun New Town and the Nee Soon private residential estate.
Under the 1996 Land Use and Urban Design Planning, the total land area of the Yishun Planning Area was 2108 ha.


== History ==
Yishun is the Mandarin romanisation of Nee Soon, named after the "pineapple king" and rubber magnate, Lim Nee Soon (b. 1879).
Lim Nee Soon was also a banker, contractor and general commission agent. He was the first general manager of the Bukit Sembawang Rubber Company Limited, formed in 1908. Nee Soon and Company was formed in 1911.
Nee Soon was one of the pioneers that opened up Sembawang. Nee Soon served on the Rural Board from 1913 to 1921 and was also appointed a Justice of Peace. In the field of education, he was one of the founders of Chinese High School and also a member of the Raffles College Committee. Nee Soon Road was officially named in 1950 by the Rural Board to facilitate postal services. Nee Soon also owned a large plot of land in the area and several roads in this area are named after his business concerns and family members. Nee Soon was a leading member of the Teochew clan association poit ip huay kwan, and was a close friend of Dr Sun Yat Sen.
The name Nee Soon was one of those changed at the height of the campaign to replace dialect names with Mandarin ones. While the government later revoked some of its decisions and reinstated names like Bukit Panjang (for Zhenghua), Yishun remained unchanged and is now the name attached to streets, roads, parks and a well-known cineplex within Yishun New Town, Golden Village Yishun.
Yishun has started the development of HDB flats since 1976, with the first HDB flats at Chong Pang. Yishun Neighbourhood 1 has been developed since 1981, followed by Neighbourhood 7 and Neighbourhood 2. Neighbourhood 6, 8 and 9 have been developed in 1987, together with the Town Centre. Neighbourhood 3 and 4 followed slightly later in 1992. Construction of the Neighbourhood 5 was started on 2009 after the Crossrail West was announced and will be completed by 2015.


== Amenities ==


=== Shopping/commercial areas ===
Northpoint Shopping Centre → The only shopping mall in Yishun, was located just beside the Yishun MRT Station. It underwent an expansion completed in 2010 which included a new building connected to the main shopping mall built on a plot of land next to it. The expansion increased the size of Northpoint Shopping Centre and hold more shops as well as a new library at its top floor. The shopping centre was opened in 1992 making it the first modern sub-urban mall in a major housing estate (new town). Currently, sub-urban malls are almost a standard feature in all housing estates.
Yishun 10 Cineplex → Adjacent to Northpoint Shopping Centre is the Yishun 10 Cineplex. Opened in May 1992, it was then the largest multiplex with the most screens (10 in all) in Asia. It is operated by the Golden Village group.
Chong Pang City → Chong Pang City is located in Neighbourhood 1. It has a collection of shophouses, a hawker centre and a market. There are the usual small, family-run businesses as well as a Giant supermarket, CK department store, MacDonald's, Watson's and Guardian pharmacies, 7-Elevens...etc. Chong Pang City was by far the biggest and most complete of the neighbourhood centres until the arrival of Northpoint Shopping Centre and Yishun 10.
Other neighbourhood shopping areas → There are various other neighbourhood centres at Nee Soon East (Neighbourhood 2), Khatib City (Neighbourhood 8) and Neighbourhood 4. A typical "heartland" neighbourhood centre consists of mum and pop shops, eateries (commonly called coffeeshops), supermarket chains, mini-marts, clinics, local banks, salons...etc. In the last decades, fast food outlets like MacDonald's, KFC as well as pharmacies are finding their way into these areas.
Yishun Community Library → The previous Yishun Community Library was located at Yishun Street 22. The library has since relocated to the new Northpoint Shopping Centre. The new library featured a bigger area which caters to the different needs of the Yishun population, and has books for both children and adults. It had been open to public on 14 November 2008 at 11am.


=== Neighbourhoods ===
Within the Yishun vicinity, There are 9 neighbourhoods.
Yishun Neighbourhood 1 or Yishun North
Yishun Neighbourhood 2 or Taman Yishun
Yishun Neighbourhoods 3, 4 and 5 or Yishun East
Yishun Neighbourhood 6 and 9 or Yishun Central
Yishun Neighbourhood 7 or Yishun West
Yishun Neighbourhood 8 or Yishun South
Yishun Neighbourhood Executive Condo [1]


=== Medical facilities ===
Khoo Teck Puat Hospital
Instead of Northern General Hospital, the new general hospital has been named Khoo Teck Puat Hospital after receiving a S$125 million donation from the late Mr Khoo's family. Spanning over 3.5 hectares in the Yishun Central Area, the 550-bed hospital will offer a full range of comprehensive healthcare services for residents living in the north. A new feature of the hospital includes overlooking the scenic Yishun Pond. This will be the first hospital in the north after the nearest one which is at Thomson.
Yishun Polyclinic
Yishun Polyclinic is located at 30 Yishun Central beside Khoo Teck Puat Hospital and is managed by National Healthcare Group Polyclinic (NHGP). It is accessible by Bus 853, 855 or 857 from Yishun Bus Interchange.
Other Private Clinics and Dental Clinics
Currently, the Yishun estate is well served by the Yishun Polyclinic as well as many private medical clinics and dental clinics.


=== Country clubs ===

SAFRA Yishun Country Club → SAFRA Yishun Country Club is a country club owned by SAFRA, which aims to build morale and camaraderie amongst NSmen in Singapore. The club is strategically located in Yishun Park to offer a feel of nature. It also has a wide range of amenities like bowling centre, gym, tennis court, wall climbing, Western restaurant and Party World KTV. To create various choices among NSmen, SAFRA Yishun Country Club is destined as an Adventure Sports Hub within the group of SAFRA club houses.
Orchid Country Club → Located near the scenic Seletar Reservoir. The club aims to improve the social status of its members. Activities there include Paintball. There is also a Chinese restaurant, bowling alley. Members facilities include a gym, swimming pool, golf driving range as well as tennis courts.


=== Community centres and clubs ===
Nee Soon East Community Club
Nee Soon South Community Centre
Chong Pang Community Club
Nee Soon Central Community Centre
Nee Soon East Toastmasters Club
Nee Soon East CC Youth Executive Committee (YEC)
Chong Pang Toastmasters Club [2]


=== Transport ===
Yishun MRT Station NS13, situated along Yishun Ave 2 and Yishun Ave 5, Yishun Central.
Yishun Bus Interchange Linked to the Yishun MRT station via the underpass at Northpoint Shopping Centre
Khatib MRT Station NS14, situated along Yishun Ave 2 and Yishun Ring Road
Yishun Temporary Bus Interchange, situated beside Golden Village and linked to Northpoint via a pedestrian crossing. It is situated a bit far from Yishun MRT station. (Open on 14 March 2015)


=== Parks, Garden and other Recreational Facilities ===
2 Major Parks:
Yishun Park (managed by National Parks Board) – a 13 ha park in the centre of the housing estate. Used to be a rubber estate, it is thickly covered with natural vegetation. Part of the land was developed into the SAFRA Yishun Country Club.
Lower Seletar Reservoir Park (managed by NParks and PUB) – this small 3 ha park at the southern edge of the housing estate bordering the northern edge of the reservoir. Under the PUB's Active, Beautiful, Clean Water for all (ABC) programme, there are plans to open up the reservoir to more water sports. Currently there is a smaller water sports rental facility there. Occasionally there are dragon boat competition. Viewing benches, riderside deck and possibly a stage will be in the works if the project take off.
Nee Soon East Park. It is located opposite Block 407 Yishun Avenue 6. It consist of a basketball court, fitness corner, and many more facilities.


=== Sports Facilities ===
Yishun Stadium and Sports Hall:
Located at the Southern end of the town, the Yishun Stadium was opened on 2 January 1992. In 1993, it played host to the Karate event during the 1993 SEA Games in Singapore. In 1996, it was the home ground for the Sembawang Rangers FC in the S-league. Eventually the team was dropped from the league in 2003. Later on, it hosted the Young Lions in 2005 season, Sporting Afrique FC in 2006 season and currently it is the home for Korean Super Reds FC for the 2007 season. Other than the main stadia with the soccer field and 8-lanes running track, it also houses a ClubFitt gym. The sport hall seats 800 people and it can be use for badminton, basketball or other indoor sports.
Yishun Swimming Complex
Opened in 1988, it has a competition pool (with 430 seating), a children pool and a training pool.


== Industrial activities ==
The Yishun Industrial Park is a small industrial park situated between Avenues 6 and 7 which comprises some well known companies such as Murata, Philips Lumileds, Agilent, Avago Technologies, ATS, ASM-Frontend, STATS, Inc. and many other smaller companies. It can be reached by Bus service 811 from the Yishun Bus Interchange.


== Educational institutions ==

There are 10 primary schools, 9 secondary schools and a Junior college.


== Housing activities ==
There are two housing executive condominiums in Yishun. The criterion executive condo and signature at yishun.


== References ==


== Sources ==
Victor R Savage, Brenda S A Yeoh (2003), Toponymics – A Study of Singapore Street Names, Eastern Universities Press, ISBN 981-210-205-1
SAFRA Online. (2006). About SAFRA. Retrieved on 5 April 2007. http://www.safra.sg/aboutus.aspx?sectionid=1