Chong Pang is a precinct located at Yishun, Singapore. It has precincts of Neighbourhood 1, part of Neighbourhood 7 and private residential areas along Sembawang Road (which has amenities like Sembawang Shopping Centre).


== Politics ==
Chong Pang is part of Nee Soon Group Representation Constituency (formerly Sembawang Group Representation Constituency until 2011). The MP of the area is Mr K Shanmugam.