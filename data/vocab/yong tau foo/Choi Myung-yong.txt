Choi Myung-Yong (Hangul: 최명용, born 21 July 1976) is a football referee in the South Korean K-League. He has been refereeing in the K-League since 2005. Choi was awarded a FIFA badge in 2007 and is now eligible to referee international matches.


== Honors ==
2010 K-League Best Referee Awards


== References ==