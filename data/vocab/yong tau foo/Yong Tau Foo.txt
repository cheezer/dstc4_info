Yong tau foo (also spelled yong tao foo, yong tau fu, yong tau hu or yong tofu; yentafo in Thailand) is a Hakka Chinese food consisting primarily of tofu that has been filled with either a ground meat mixture or fish paste (surimi). Variation of this food include vegetables and mushrooms stuff with ground meat or surimi. Yong tau foo is eaten in numerous ways, either dry with a sauce or served as a soup dish.
It is commonly found in China, Malaysia, Singapore and Thailand, and in cities where there are large Teochew and Hokkien populations.


== VariationsEdit ==


=== TraditionalEdit ===
Traditional Hakka versions of yong tau foo consists of tofu cubes stuffed and heaped with minced meat (usually lamb or pork) and herbs, then fried until golden brown, or sometimes braised. Variations include usage of various condiments, including eggplants, shiitake mushrooms, and bitter melon stuffed with the same meat paste. Traditionally, yong tau foo is served in a clear yellow bean stew along with the bitter melon and shiitake variants.


=== South East AsiaEdit ===
In South East Asia, the term "yong tau foo" is used to described a dish instead of the stuffed tofu item exclusively. The dish can contain a varied selection of food items, including young tau foo, fish balls, crab sticks, bitter melons, cuttlefish, lettuce, ladies fingers, as well as chilis, and various forms of fresh produce, seafood and meats common in Chinese cuisine. Vegetables such as bitter melon and chilis are usually filled with surimi. The foods are then sliced into bite-size pieces, cooked briefly in boiling broth and then served either in the broth as soup or with the broth in a separate bowl. The dish is eaten with chopsticks and a soup spoon and can be eaten by itself or served with a bowl of steamed rice, noodles, or Rice vermicelli . Another variation of this dish would be to serve it with laksa gravy or curry sauce. Essential accompaniments are a spicy, vinegary chili sauce, originally made with red fermented bean curd and distantly similar in taste to Sriracha sauce, and a distinctive brown sweet bean sauce or hoisin sauce for dipping.


== See alsoEdit ==

List of Chinese soups
List of soups
List of tofu dishes