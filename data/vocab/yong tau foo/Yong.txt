Yong may refer to:
Yong (Chinese: 永), Chinese character for "permanence", unique in that the character contains eight strokes common to Chinese characters; see Eight Principles of Yong
Yong (Chinese: 用), Chinese character for "use" or "function"; in Neo-Confucianism, often associated with Ti ("substance" or "body")
Yong (Chinese: 雍) or Yongcheng, capital of Qin (state), located in modern Fengxiang County, founded in 677 BCE and moved to Yueyang (櫟陽) in 383 BCE
Yong, a variant of Yang (surname) (楊/杨)
Yong (Korean name)
Korean dragon (yong)
Yong River in China, Zhejiang Province
Yong River in China, Guangxi Zhuang Autonomous Region
Yong, a community in Tamale Metropolitan District in the Northern Region of Ghana


== See also ==
Yung (disambiguation)