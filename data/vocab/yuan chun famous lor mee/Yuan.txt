Yuan may refer to:


== Currency ==
Chinese yuan, the basic unit of currency in China
Renminbi, the current currency used in the People's Republic of China, whose basic unit is yuan
New Taiwan Dollar, the current currency used in the Republic of China (Taiwan), whose basic unit is yuan
Manchukuo yuan, the unit of currency that was used in the Japanese puppet state of Manchukuo


== Governmental organ ==
"Court", the name for a kind of institution.
In the government of the Republic of China:
Control Yuan
Examination Yuan
Executive Yuan
Judicial Yuan
Legislative Yuan
Others:
Xuanzheng Yuan, or Bureau of Buddhist and Tibetan Affairs during the Yuan dynasty
Lifan Yuan during the Qing dynasty


== Dynasties ==
Yuan dynasty (元朝), also called the Mongol Dynasty, of China
Northern Yuan dynasty, the Yuan dynasty's successor state in Mongolia


== People and languages ==
Yuan (surname), the transliteration of a number of Chinese family names (e.g. 袁, 元, 苑, 原, 源, 爰)
Yuan Haowen (元好問), Chinese poet, author, and official

Thai Yuan, a people of Northern Thailand
Yuan language, commonly known as Northern Thai language, language of the Thai Yuan people


== Places ==
Yu'an District, in Anhui, China
Yuan River (沅江 or 沅水), one of the four largest rivers in Hunan, a tributary of the Yangtze River


== Buddhism ==
Yuanfen (缘分), Chinese or Buddhist concept that means the predetermined principle that dictates a person's relationships and encounters


== Submarines ==
Type 041 submarine, PLAN Yuan class submarines


== Traditional Chinese Medicine ==
Yuan Qi (TCM) Traditional Chinese Medicine. TCM Qi: Traditional Chinese Medicine four Qi, Yuan qi, Wei qi, Ying qi, Zong qi


== In fictional use ==
Yuan (Tales of Symphonia) (ユアン), a character from Tales of Symphonia
Tao Yúan (Tao En), a character in Shaman King


== See also ==
Yen, the currency of Japan
Korean won, the currency of Korea