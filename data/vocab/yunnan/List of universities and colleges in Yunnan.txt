This is a list of universities and colleges in Yunnan Province.
Note: Institutions without full-time bachelor programs are not listed.


== Notation ==
The following notation is used:
National (Direct): Directly administered by the Chinese Ministry of Education (MOE)
National (Other): Administered by other ministries
Ω (National Key Universities): Universities with high regards from the MOE and the Chinese government
Provincial: Public university administered by the province
Municipal: Public university administered by the municipality
Private: Privately owned and funded university


=== Provincial ===


==== Baoshan ====
Baoshan College of Chinese Medicine [1]
Baoshan Teachers College [2]


==== Chuxiong ====
Chuxiong Normal University (楚雄师范学院) [3]


==== Dali ====
Dali University (大理学院) [4]


==== Dehong ====
Dehong Teachers College [5]


==== Honghe ====
Honghe University [6]


==== Kunming ====
Kunming Medical College
Kunming Teachers College (昆明学院)
Kunming Metallurgy College [7]
Kunming Vocational and Technical College of Industry
Kunming Medical University (formerly Kunming Medical College)
Kunming Sailing University (昆明扬帆职业技术学院) [8]
Kunming University
Kunming University of Science and Technology (amalgamated with Yunnan Polytechnic University)
Southwest Forestry University (Southwest Forestry College)
Yunnan Arts University
Yunnan Agricultural University
Yunnan Nationalities University
Yunnan Normal University
Yunnan Normal University Business School [9]
Department of Physic and Electronic Information Optical Engineering

Yunnan UniversityΩ
Yunnan University of Finance and Economics
Yunnan University of Traditional Chinese Medicine


==== Lijiang ====
Lijiang Teachers Training College [10]
Lincang Teachers College [11]


==== Qujing ====
Qujing Normal University [12]


==== Simao ====
Simao Teachers College [13]


==== Wenshan ====
Wenshan Teachers College [14]


==== Xishuangbanna ====
Xishuangbanna Vocation & Technical Institute [15]


==== Yuxi ====
Yuxi Normal University (玉溪师范学院) [16]


==== Zhaotong ====
Zhaotong Teachers College [17]


==== Other / Unknown location ====
Yunnan Einsun Software College [18]
Yunnan Police College
Yunnan Medical College
Yunnan Vocational and Technical College of Energy Resources
Yunnan Vocational and Technical College of Science and Technology Information
Yunnan Vocational College of Land Resources
Yunnan Vocational and Technical College of Agriculture
Yunnan Vocational and Technical College of Communications
Yunnan Vocational College of Culture and Arts
Yunnan College of Business Management [19]
Yunnan Vocational College of Mechanical and Electrical Technology
Yunnan Forestry Technical College
Yunnan Vocational College of Tropical Crops
Yunnan Vocational College of Judicial Officer


== See also ==
Greater Mekong Sub-region Academic and Research Network (GMSARN)
Yunnan Provincial Library


== References ==
Baidu Baike
Hudong
Yunnan Institutions Admitting International Students (China Scholarship Council, December 4, 2008)