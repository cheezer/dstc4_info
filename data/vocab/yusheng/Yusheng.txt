Yusheng, yee sang or yuu sahng (Chinese: 鱼生; pinyin: yúshēng; Pe̍h-ōe-jī: hî-seⁿ or hû-siⁿ), or Prosperity Toss, also known as lo hei (Cantonese for 撈起 or 捞起) is a Teochew-style raw fish salad. It usually consists of strips of raw fish (most commonly salmon), mixed with shredded vegetables and a variety of sauces and condiments, among other ingredients. Yusheng literally means "raw fish" but since "fish (鱼)" is commonly conflated with its homophone "abundance (余)", Yúshēng (鱼生) is interpreted as a homophone for Yúshēng (余升) meaning an increase in abundance. Therefore, yusheng is considered a symbol of abundance, prosperity and vigor.
While versions of it are thought to have existed in China, the contemporary version was created and popularised in the 1960s amongst the ethnic Chinese community and its consumption has been associated with Chinese New Year festivities in Malaysia, Indonesia and Singapore.


== History ==
Fishermen along the coast of Guangzhou traditionally celebrated Renri, the seventh day of the Chinese New Year, by feasting on their catches. The practice of eating raw fish in thinly sliced strips can be traced back to ancient China through the raw fish or meat dish known as kuai (膾, kuài). However the present form of yusheng is believed to have started in Chaozhou and Shantou as far back as the Southern Song Dynasty.
There is also a legend regarding its originality. It was believed that in south China, a young man and his girlfriend found themselves stranded by bad weather at a temple with nothing to eat but they managed to catch a carp. Chancing upon a bottle of vinegar, they added this to the stripped carp and found it quite appetising.
In Malaya's colonial past, migrants imported this tradition; porridge stalls sold a raw fish dish which is believed to have originated in Jiangmen, Guangdong province that consisted of fish, turnip and carrot strips, which was served with condiments of oil, vinegar and sugar that were mixed in by customers.


=== Lohei Yusheng ===
Eating Yu Sheng during Chinese New Year is a cultural activity for Chinese living in Singapore and Malaysia, but not so much in other Chinese-populated countries such as Hong Kong, where the practice is almost unheard of.
There was a controversy between Singapore and Malaysia regarding the origins of this dish. It was said that a restaurant (陆祯记) in Seremban, Malaysia first refined this dish from a Cantonese dish and sold it during Lunar New Year around 1940s. However, the son of the chef acknowledged that it is hard to dispute the "ownership" of the dish. This dish has been declared a Malaysian heritage food by the Malaysian Department of National Heritage. One thing is certain though, that this dish has its roots deep in the Southern part of China. 
It was also said to be created by 4 master chefs in a Singapore restaurant kitchen way back in 1964, then still part of Malaysia. It made its debut during Lunar New Year of 1964 in Singapore's Lai Wah Restaurant (Established in Sept. 1963). [] The 4 master chefs were Than Mui Kai (Tham Yu Kai, co-head chef of Lai Wah Restaurant), Lau Yoke Pui (co-head chef of Lai Wah Restaurant), Hooi Kok Wai (Founder of Dragon-Phoenix Restaurant, established on 8 April 1963) and Sin Leong (Founder of Sin Leong Restaurant) who, together created that as a symbol of prosperity and good health amongst the Chinese []. All four Chefs were named as the "Four Heavenly Culinary Kings" of Singapore some 40 years ago for their culinary prowess and ingenuity.
In the 1970s, Lai Wah Restaurant started the modern-day method of serving Yu Sheng with a pre-mixed special sauce comprising plum sauce, rice vinegar, kumquat paste and sesame oil --- instead of customers mixing inconsistently-concocted sauce. []


== The dish (Modern Version) ==
The yusheng had fish served with daikon (white radish), carrots, red pepper (capsicum), turnips, red pickled ginger, sun-dried oranges, daun limau nipis (key lime leaves), Chinese parsley, chilli, jellyfish, chopped peanuts, toasted sesame seeds, Chinese shrimp crackers (or fried dried shrimp), five spice powder and other ingredients, laced with a sauce using plum sauce, rice vinegar, kumquat paste and sesame oil, for a total of 27 ingredients. Originally, the dish used raw wolf herring, although in deference to the popular wishes of customers, salmon was later offered as an alternative due to the growing popularity of Salmon.


== Serving ==

Yusheng is often served as part of a multi-dish dinner, usually as the appetizer due to its symbolism of "good luck" for the new year. Some would consume it on Renri, the seventh day of the Chinese New Year, although in practice it may be eaten on any convenient day during the Chinese New Year period (the first to the 15th day of the first lunar month).
The base ingredients are first served. The leader amongst the diners or the restaurant server proceeds to add ingredients such as the fish, the crackers and the sauces while saying "auspicious wishes" (吉祥话 jíxiáng huà) as each ingredient is added, typically related to the specific ingredient being added. For example, phrases such as niánnián yŏuyú (年年有余; "may there be abundance year after year") are uttered as the fish is added, as the Chinese word for "surplus" or "abundance" (余 yú) sounds the same as the Chinese word for "fish" (鱼 yú).
All diners at the table then stand up and proceed to toss the shredded ingredients into the air with chopsticks while saying various "auspicious wishes" out loud, or simply "lo hei, lo hei" (撈起, 撈起, in Mandarin lāoqǐ, lāoqǐ, meaning "scoop it up, scoop it up"). It is believed that the height of the toss reflects the height of the diners' growth in fortunes, thus diners are expected to toss enthusiastically.


== Meaning of the ingredients ==
When putting the yu sheng on the table offers New Year greetings.
恭喜发财 (Gong Xi Fa Cai) meaning “Congratulations for your wealth” 万事如意 (Wan Shi Ru Yi) meaning “May all your wishes be fulfilled”
The fish is added, symbolising abundance and excess through the year. 年年有余　(Nian Nian You Yu) meaning “Abundance through the year”, as the word "fish" in Mandarin also sounds like "Abundance".
The pomelo or lime is added to the fish, adding luck and auspicious value. 大吉大利　Da Ji Da Li meaning “Good luck and smooth sailing”
Pepper is then dashed over in the hope of attracting more money and valuables. 招财进宝 Zhao Cai Jin Bao meaning “Attract wealth and treasures”
Then oil is poured out, circling the ingredients and encouraging money to flow in from all directions. 一本万利 Yi Ben Wan Li meaning “Make 10,000 times of profit with your capital” 财源广进 Cai Yuan Guang Jin meaning “Numerous sources of wealth”
Carrots are added indicating blessings of good luck. 鸿运当头 Hong Yun Dang Tou meaning “Good luck is approaching”. Carrot (红萝卜) is used as the first character 鸿 also sound like the Chinese character for red.
Then the shredded green radish is placed symbolising eternal youth. 青春常驻 Qing Chun Chang Zhu meaning “Forever young”. Green radish is used as the first character 青 also sound like the Chinese character for green.
After which the shredded white radish is added - prosperity in business and promotion at work. 风生水起 Feng Sheng Shui Qi meaning “Progress at a fast pace” 步步高升 Bu Bu Gao Sheng meaning “Reaching higher level with each step”
The condiments are finally added.
First, peanut crumbs are dusted on the dish, symbolizing a household filled with gold and silver. 金银满屋 Jin Yin Man Wu meaning “Household filled with gold and silver”
Sesame seeds quickly follow symbolising a flourishing business. 生意兴隆 Sheng Yi Xing Long meaning “Prosperity for the business”
Yu Sheng sauce, usually plum sauce, is generously drizzled over everything. 甜甜蜜蜜 Tian Tian Mi Mi Meaning “May life always be sweet”
Deep-fried flour crisps in the shape of golden pillows is then added with wishes that literally the whole floor would be filled with gold. 满地黄金 Man Di Huang Jin meaning “Floor full of gold”


== See also ==

Ceviche
Hoe (dish)
Kuai (dish)
Namasu
Sashimi


== References ==


== External links ==
Interactive Virtual Reality – Process of Lo Hei
Jack Tsen-Ta Lee, A Dictionary of Singlish and Singapore English – lo hei
Jack Tsen-Ta Lee, A Dictionary of Singlish and Singapore English – yusheng
A recipe for yusheng
DIY Spring Toss Yusheng recipe
How to toss Yee Sang (video)