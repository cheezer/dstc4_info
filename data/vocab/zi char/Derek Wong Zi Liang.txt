Derek Wong Zi Liang (traditional Chinese: 黃梓良) (born 13 January 1989) is a Singaporean badminton player. He competed for Singapore at the 2012 Summer Olympics.


== 2011 World Championships ==
Derek Wong shocked Indonesia’s Taufik Hidayat, a former Olympic champion in the second round of the World Championships. He defeated the Indonesian star 21-17, 21-14 to earn a place in the third round against Hans-Kristian Vittinghus of Denmark.


== 2011 Southeast Asian Games ==
Derek Wong upset Vietnam’s Nguyen Tien Minh, then ranked in the world’s top 10, at the quarterfinals of the Sea Games. He also came agonizingly close to upsetting the eventual gold medalist, Simon Santoso, in the next round. He lead in the early stages of the semifinal match before succumbing to the Indonesian in straight sets.


== 2012 London Olympics ==
Derek Wong's Olympic debut ended after a 21–17, 21–14 loss to Jan Ø. Jørgensen in his final Group I match at London's Wembley Arena on 31 July to finish second in the three-man group. "Of course, I wanted to play more games instead of just two. But being in my first Olympics has been a huge experience, and one that I will use for my career." Derek Wong said afterwards. He had earlier beaten Israel's Misha Zilberman 21–9, 21–15 on 29 July.


== 2014 Commonwealth Games ==
Derek Wong advances to the men's singles final but loses to Parupalli Kashyap of India 14-21, 21-11, 21-19 thus winning him a silver medal.


== References ==