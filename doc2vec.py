__author__ = 'v-qizxie'
import gensim, os, argparse, dataset_walker, ontology_reader, time
from gensim.models.doc2vec import TaggedDocument
import sklearn.svm.libsvm as svm

def getWordList(transcript):
    t = transcript.lower()
    t = t.replace('-', ' ')
    t = t.split(' ')
    transcript = ''
    for _t in t:
        if len(_t)>0 and _t[0] == '%': continue
        transcript += _t + ' '
    transcript = transcript[:len(transcript)-1]
    ll = transcript.split()
    ans = []
    for i in ll:
        if i.isalpha() and i != "":
            ans += [i]
    return ans

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Sentence vectors on DSTC-4 by Qizhe Xie.')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
    parser.add_argument('--train',dest='train',action='store',metavar='JSON_FILE',required=False,help='Train/Test')
    parser.add_argument('--printFile',dest='printFile',action='store',metavar='PATH',required=False,help='doc2vec.obj')
    args = parser.parse_args()
    if args.train:
        start_time = time.time()
        #sp = os.pathsep
        sp = "/"
        path = os.getcwd() + sp + "data" + sp + "crawled_data" + sp
        documents = []
        ci = 0
        for filename in os.listdir(path):
            f = open(path + filename, "r")
            for line in f.readlines():
                documents += [TaggedDocument(getWordList(line), ['cra_%s' % ci])]
                ci += 1
        path = os.getcwd() + sp + "data" + sp + "vocab" + sp
        ci = 0
        for st in os.listdir(path):
            newP = path + st + sp
            for wikiTitle in os.listdir(newP):
                try:
                    f = open(newP + wikiTitle, "r")
                    lines = f.readlines()
                    doc = " ".join(lines)
                    documents += [TaggedDocument(getWordList(doc), ['wiki_%s' % ci])]
                    ci += 1
                except Exception:
                    pass
        tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
        for topic in tagsets:
            for slot in tagsets[topic]:
                for value in tagsets[topic][slot]:
                    documents += [TaggedDocument(getWordList(value), ['%s_%s_%s' % (topic, slot, value)])]
        for ds in ["dstc4_train", "dstc4_dev"]:
            dataset = dataset_walker.dataset_walker(ds,dataroot=args.dataroot,labels=False)
            tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
            for call in dataset:
                sessionID = call.log["session_id"]
                for (utter,_) in call:
                    utter_index = utter['utter_index']
                    index = ds + '_%s_%s' % (sessionID, utter_index)
                    documents += [TaggedDocument(getWordList(utter['transcript']), [index])]
        doc2Vec = gensim.models.doc2vec.Doc2Vec(documents, size=50)
        doc2Vec.save(args.printFile)
        end_time = time.time()
        print end_time - start_time
        print doc2Vec.estimate_memory()
