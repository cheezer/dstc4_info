import argparse, sys, ontology_reader, dataset_walker, time, json, copy, logging
from gensim.models.ldamodel import LdaModel
from topicModel import getWordList
import numpy as np
from gensim import corpora
multiValue = False
threshold = 0.99

class BaselineTracker(object):
    def __init__(self, tagsets, tmFile, dictFile):
        self.tagsets = tagsets
        self.frame = {}
        self.memory = {}
        self.tm = LdaModel.load(tmFile)
        self.dct = corpora.Dictionary.load(dictFile)
        #self.tm.print_topics(20)
        self.reset()
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    def inference(self, transcript):
        text = getWordList.getWordList(transcript)
        bow = self.dct.doc2bow(text)
        sv = self.tm[bow]
        numTopic = self.tm.num_topics
        ans = [0] * numTopic
        for i in sv:
            ans[i[0]] += i[1]
        return np.array(ans), sv

    def train(self, dataset):
        self.vec = {}
        self.cnt = {}
        for call in dataset:
            sessionID = call.log["session_id"]
            for (utter, label) in call:
                if utter['segment_info']['target_bio'] != 'O' and "INFO" in label["frame_label"]:
                    topic = utter['segment_info']['topic']
                    transcript = utter['transcript']
                    utter_index = utter["utter_index"]
                    if topic not in self.cnt:
                        self.cnt[topic] = {}
                        self.vec[topic] = {}
                    #index = "dstc4_train" + '_%s_%s' % (sessionID, utter_index)
                    for value in label["frame_label"]["INFO"]:
                        if value not in self.cnt[topic]:
                            self.cnt[topic][value] = 1
                            #self.vec[topic][value], sv = self.inference(transcript)
                            self.vec[topic][value] = transcript
                        else:
                            self.cnt[topic][value] += 1
                            #temp, sv = self.inference(transcript)
                            #self.vec[topic][value] += temp
                            self.vec[topic][value] += " " + transcript
                        '''print value, transcript, sv
                        for i in sv:
                            print self.tm.print_topic(i[0])'''
                    sys.stderr.write('training %d:%d\n' % (call.log['session_id'], utter['utter_index']))
        for topic in self.cnt:
            for value in self.cnt[topic]:
                #print topic, value, self.cnt[topic][value], self.vec[topic][value]
                #self.vec[topic][value] = np.array(self.vec[topic][value])
                #self.vec[topic][value] /= self.cnt[topic][value] * 1.0
                #self.vec[topic][value], _ = self.inference(self.vec[topic][value])
                self.vec[topic][value], _ = self.inference(value)

    def addUtter(self,  dataset, utter, sessionID):
        output = {'utter_index': utter['utter_index']}

        topic = utter['segment_info']['topic']
        transcript = utter['transcript'].replace('Singapore', '')

        if utter['segment_info']['target_bio'] == 'B':
            self.frame = {}
        #utter_index = utter["utter_index"]
        vec, _ = self.inference(transcript)
        if topic in self.tagsets:
            if topic in self.vec:
                for value in self.tagsets[topic]["INFO"]:
                    if value in self.vec[topic]:
                        #index = dataset + '_%s_%s' % (sessionID, utter_index)
                        #print vec, "\n", self.vec[topic][value]
                        sim = np.dot(vec, self.vec[topic][value])/np.linalg.norm(vec)/np.linalg.norm(self.vec[topic][value])
                        if sim > threshold:
                            if multiValue:
                                if "INFO" not in self.frame:
                                    self.frame["INFO"] = []
                                self.frame["INFO"].append(value)
                            else:
                                self.frame["INFO"] = [value]
            output['frame_label'] = copy.deepcopy(self.frame)
        return output

    def reset(self):
        self.frame = {}


def main(argv):
    parser = argparse.ArgumentParser(description='Simple info dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True,
                        help='The dataset to analyze')
    parser.add_argument('--dataroot', dest='dataroot', action='store', required=True, metavar='PATH',
                        help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--trackfile', dest='trackfile', action='store', required=True, metavar='JSON_FILE',
                        help='File to write with tracker output')
    parser.add_argument('--ontology', dest='ontology', action='store', metavar='JSON_FILE', required=True,
                        help='JSON Ontology file')
    parser.add_argument('--readtm', dest='readtm', action='store', metavar='JSON_FILE', required=True,
                        help='Topic model object file')
    parser.add_argument('--readDict', dest='readDict', action='store', metavar='JSON_FILE', required=True,
                        help='Dict file')
    args = parser.parse_args()
    trainset = dataset_walker.dataset_walker("dstc4_train", dataroot=args.dataroot, labels=True)
    dataset = dataset_walker.dataset_walker(args.dataset, dataroot=args.dataroot, labels=False)
    tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()

    track_file = open(args.trackfile, "wb")
    track = {"sessions": [], "dataset": args.dataset}
    start_time = time.time()
    tracker = BaselineTracker(tagsets, args.readtm, args.readDict)
    tracker.train(trainset)
    for call in dataset:
        this_session = {"session_id": call.log["session_id"], "utterances": []}
        tracker.reset()
        for (utter, _) in call:
            sys.stderr.write('%d:%d\n' % (call.log['session_id'], utter['utter_index']))
            tracker_result = tracker.addUtter(args.dataset, utter, call.log["session_id"]) #dstc4_train_1_84
            if tracker_result is not None:
                this_session["utterances"].append(copy.deepcopy(tracker_result))
        track["sessions"].append(copy.deepcopy(this_session))
    end_time = time.time()
    elapsed_time = end_time - start_time
    track['wall_time'] = elapsed_time
    json.dump(track, track_file, indent=4)
    track_file.close()

if __name__ == "__main__":
    main(sys.argv)
