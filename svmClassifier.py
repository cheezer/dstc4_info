import argparse, sys, ontology_reader, dataset_walker, time, json, copy
from gensim.models.doc2vec import Doc2Vec
import numpy as np
import sklearn.svm.libsvm as svm
multiValue = False

class BaselineTracker(object):
    def __init__(self, tagsets, doc2vecFile):
        self.tagsets = tagsets
        self.frame = {}
        self.memory = {}
        self.doc2vec = Doc2Vec.load(doc2vecFile)
        self.reset()

    def train(self, dataset):
        self.vec = {}
        self.cnt = {}
        for call in dataset:
            sessionID = call.log["session_id"]
            for (utter, label) in call:
                if utter['segment_info']['target_bio'] != 'O' and "INFO" in label["frame_label"]:
                    topic = utter['segment_info']['topic']
                    transcript = utter['transcript'].replace('Singapore', '')
                    utter_index = utter["utter_index"]
                    if topic not in self.cnt:
                        self.cnt[topic] = {}
                        self.vec[topic] = {}
                    index = "dstc4_train" + '_%s_%s' % (sessionID, utter_index)
                    for value in label["frame_label"]["INFO"]:
                        if value not in self.cnt[topic]:
                            self.cnt[topic][value] = 1
                            self.vec[topic][value] = {"x":[], "y":[]}
                        self.cnt[topic][value] += 1
                        self.vec[topic][value]["x"] += [self.doc2vec.docvecs[index]]
                        self.vec[topic][value]["y"] += [1]
                    #sys.stderr.write('training %d:%d\n' % (call.log['session_id'], utter['utter_index']))
        for topic in self.cnt:
            for value in self.cnt[topic]:
                self.vec[topic][value] /= self.cnt[topic][value] * 1.0
                self.svm = svm.

    def addUtter(self,  dataset, utter, sessionID):
        output = {'utter_index': utter['utter_index']}

        topic = utter['segment_info']['topic']
        transcript = utter['transcript'].replace('Singapore', '')

        if utter['segment_info']['target_bio'] == 'B':
            self.frame = {}
        utter_index = utter["utter_index"]
        if topic in self.tagsets:
            if topic in self.vec:
                for value in self.tagsets[topic]["INFO"]:
                    if value in self.vec[topic]:
                        index = dataset + '_%s_%s' % (sessionID, utter_index)
                        vec = self.doc2vec.docvecs[index]
                        sim = np.dot(vec, self.vec[topic][value])/np.linalg.norm(vec)/np.linalg.norm(self.vec[topic][value])
                        if sim > 0.3:
                            if multiValue:
                                if not "INFO" in self.frame:
                                    self.frame["INFO"] = []
                                self.frame["INFO"].append(value)
                            else:
                                self.frame["INFO"] = [value]
            output['frame_label'] = copy.deepcopy(self.frame)
        return output

    def reset(self):
        self.frame = {}


def main(argv):
    parser = argparse.ArgumentParser(description='Simple info dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True,
                        help='The dataset to analyze')
    parser.add_argument('--dataroot', dest='dataroot', action='store', required=True, metavar='PATH',
                        help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--trackfile', dest='trackfile', action='store', required=True, metavar='JSON_FILE',
                        help='File to write with tracker output')
    parser.add_argument('--ontology', dest='ontology', action='store', metavar='JSON_FILE', required=True,
                        help='JSON Ontology file')
    parser.add_argument('--doc2vec', dest='doc2vec', action='store', metavar='JSON_FILE', required=True,
                        help='Doc2vec object file')
    args = parser.parse_args()
    trainset = dataset_walker.dataset_walker("dstc4_train", dataroot=args.dataroot, labels=True)
    dataset = dataset_walker.dataset_walker(args.dataset, dataroot=args.dataroot, labels=False)
    tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()

    track_file = open(args.trackfile, "wb")
    track = {"sessions": [], "dataset": args.dataset}
    start_time = time.time()

    tracker = BaselineTracker(tagsets, args.doc2vec)
    tracker.train(trainset)
    for call in dataset:
        this_session = {"session_id": call.log["session_id"], "utterances": []}
        tracker.reset()
        for (utter, _) in call:
            sys.stderr.write('%d:%d\n' % (call.log['session_id'], utter['utter_index']))
            tracker_result = tracker.addUtter(args.dataset, utter, call.log["session_id"]) #dstc4_train_1_84
            if tracker_result is not None:
                this_session["utterances"].append(tracker_result)
        track["sessions"].append(this_session)
    end_time = time.time()
    elapsed_time = end_time - start_time
    track['wall_time'] = elapsed_time
    json.dump(track, track_file, indent=4)
    track_file.close()

if __name__ == "__main__":
    main(sys.argv)
