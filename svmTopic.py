import argparse, sys, ontology_reader, dataset_walker, time, json, copy, logging, pickle, math
#from gensim.models.ldamodel import LdaModel
#import sklearn.svm.libsvm as svm
from sklearn import svm
from topicModel import getWordList
import numpy as np
from gensim import corpora
from topicModel import *
import lda
multiValue = False
threshold = 0.5

#huibuhui bian, topic??

class BaselineTracker(object):
    def __init__(self, tagsets, tmFile, dictFile):
        self.tagsets = tagsets
        self.frame = {}
        self.memory = {}
        #self.tm = LdaModel.load(tmFile)
        self.tm = pickle.load(open(tmFile, "rb"))
        #self.dct = corpora.Dictionary.load(dictFile)
        #self.tm.print_topics(20)
        self.doc = ""
        self.reset()
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    def inference(self, transcript):
        ans = self.tm.infer(transcript)[0]
        for i in ans:
            if math.isnan(i):
                return np.array([0] * ans.shape[0]), []
        return ans, []
        text = getWordList.getWordList(transcript)
        bow = self.dct.doc2bow(text)
        sv = self.tm[bow]
        numTopic = self.tm.num_topics
        ans = [0] * numTopic
        for i in sv:
            ans[i[0]] += i[1]
        return np.array(ans), sv

    def train(self, dataset):
        self.vec = {}
        self.cnt = {}
        self.sentence = {}
        for call in dataset:
            sessionID = call.log["session_id"]
            index = None
            lastLabel = None
            doc = ""
            for (utter, label) in call:
                if utter['segment_info']['target_bio'] != 'I':
                    if index != None:
                        self.sentence[index], sv = self.inference(doc)
                        for value in lastLabel:
                            if value not in self.cnt[topic]:
                                self.cnt[topic][value] = 1
                                self.vec[topic][value] = [index]
                            else:
                                self.cnt[topic][value] += 1
                                self.vec[topic][value] += [index]
                        index = None
                        doc = ""
                        lastLabel = None

                if utter['segment_info']['target_bio'] != 'O': #and "INFO" in label["frame_label"]:
                    topic = utter['segment_info']['topic']
                    transcript = utter['transcript']
                    utter_index = utter["utter_index"]
                    if topic not in self.cnt:
                        self.cnt[topic] = {}
                        self.vec[topic] = {}
                    if index == None:
                        index = (sessionID, utter_index)
                        if "INFO" in label["frame_label"]:
                            lastLabel = copy.deepcopy(label["frame_label"]["INFO"])
                        else:
                            lastLabel = []
                        doc = transcript + " "
                    else:
                        doc = doc + transcript + " "
                    sys.stderr.write('training %d:%d\n' % (call.log['session_id'], utter['utter_index']))
            if index != None:
                #why so many 0.0333???
                self.sentence[index], sv = self.inference(doc)
                for value in lastLabel:
                    if value not in self.cnt[topic]:
                        self.cnt[topic][value] = 1
                        self.vec[topic][value] = [index]
                    else:
                        self.cnt[topic][value] += 1
                        self.vec[topic][value] += [index]
                index = None
                doc = ""
                lastLabel = None

    def reset(self):
        self.frame = {}
        self.doc = ""

    def merge(self, t1, t2):
        #print json.dump(t1, open("t1.json", "wb"), indent=4)
        #print json.dump(t2, open("t2.json", "wb"), indent=4)
        #print t1
        if t1 == []:
            t1 = copy.deepcopy(t2)
            return t1
        for i in range(len(t2)):
            ses = t2[i]
            for j in range(len(ses["utterances"])):
                turn = ses["utterances"][j]
                if "frame_label" in turn:
                    for slot in turn["frame_label"]:
                        for value in turn["frame_label"][slot]:
                            #print i, t1[i]["utterances"], j, t1[i]["utterances"][j]
                            if slot not in t1[i]["utterances"][j]["frame_label"]:
                                t1[i]["utterances"][j]["frame_label"][slot] = []
                            t1[i]["utterances"][j]["frame_label"][slot] += [value]
        return t1

    def run(self, dataset, ds):
        track = {"sessions": [], "dataset": dataset}
        start_time = time.time()
        sss = "what are your interests? I want to go shopping and go sightseeing"
        a, b = self.inference(sss)
        print a
        for topic in self.cnt:
            for value in self.cnt[topic]:
                sss = "what are your interests? I want to go shopping and go sightseeing"
                a, b = self.inference(sss)
                print a
                visited = {}
                X = []
                Y = []
                num1 = 0
                for i in self.vec[topic][value]:
                    visited[i] = 1
                    X += [copy.deepcopy(self.sentence[i])]
                    Y += [1]
                    num1 += 1
                    '''if value.lower() == "preference":
                        print i'''
                num0 = 0
                for i in self.sentence:
                    if i not in visited:
                        X += [copy.deepcopy(self.sentence[i])]
                        Y += [0]
                        num0 += 1
                '''if value.lower() == "preference":
                    print topic, Y'''
                #print Y
                for i in X:
                    for j in i:
                        if j > 1:
                            print j
                ss = svm.SVC(kernel='linear', class_weight={1:num0 / num1 / 32}).fit(np.array(X), np.array(Y))
                #ss = svm.SVC(kernel='linear').fit(np.array(X), np.array(Y))
                sessions = []
                sys.stderr.write('%s %s\n' % (topic, value))
                for call in ds:
                    this_session = {"session_id":call.log["session_id"], "utterances":[]}
                    self.reset()
                    for (utter,_) in call:
                        #sys.stderr.write('%s %s %d:%d\n' % (topic, value, call.log['session_id'], utter['utter_index']))
                        sessionID = call.log["session_id"]
                        utterNum = utter['utter_index']
                        transcript = utter['transcript']
                        output = {'utter_index': utter['utter_index']}
                        if utter['segment_info']['target_bio'] != 'I':
                            self.reset()
                        if utter['segment_info']['target_bio'] != 'O':
                            if topic == utter['segment_info']['topic']:
                                self.doc = self.doc + transcript + " "
                                sen, _ = self.inference(self.doc)
                                ans = ss.predict(sen)
                                ans = ans[0]
                                if ans > threshold:
                                    #print "haha"
                                    if "INFO" not in self.frame:
                                        self.frame["INFO"] = []
                                    self.frame["INFO"] = [value]
                            output['frame_label'] = self.frame
                        this_session["utterances"].append(copy.deepcopy(output))
                    sessions.append(copy.deepcopy(this_session))
                track["sessions"] = self.merge(track["sessions"], sessions)
        end_time = time.time()
        elapsed_time = end_time - start_time
        track['wall_time'] = elapsed_time
        return track

def main(argv):
    parser = argparse.ArgumentParser(description='Simple info dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True,
                        help='The dataset to analyze')
    parser.add_argument('--dataroot', dest='dataroot', action='store', required=True, metavar='PATH',
                        help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--trackfile', dest='trackfile', action='store', required=True, metavar='JSON_FILE',
                        help='File to write with tracker output')
    parser.add_argument('--ontology', dest='ontology', action='store', metavar='JSON_FILE', required=True,
                        help='JSON Ontology file')
    parser.add_argument('--readtm', dest='readtm', action='store', metavar='JSON_FILE', required=True,
                        help='Topic model object file')
    parser.add_argument('--readDict', dest='readDict', action='store', metavar='JSON_FILE', required=True,
                        help='Dict file')
    args = parser.parse_args()
    trainset = dataset_walker.dataset_walker("dstc4_train", dataroot=args.dataroot, labels=True)
    #trainset = dataset_walker.dataset_walker("dstc4_train1", dataroot=args.dataroot, labels=True)
    dataset = dataset_walker.dataset_walker(args.dataset, dataroot=args.dataroot, labels=False)
    tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
    track_file = open(args.trackfile, "wb")
    tracker = BaselineTracker(tagsets, args.readtm, args.readDict)
    tracker.train(trainset)
    track = tracker.run(args.dataset, dataset)
    json.dump(track, track_file, indent=4)
    track_file.close()

if __name__ == "__main__":
    main(sys.argv)
