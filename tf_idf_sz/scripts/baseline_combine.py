import argparse, sys, ontology_reader, dataset_walker, time, json
import string,re

from fuzzywuzzy import fuzz

class BaselineTracker(object):
    def __init__(self, tagsets, acts1, acts2, exist_in_train):
        self.tagsets = tagsets
        self.frame = {}
        self.memory = {}
        self.acts1 = acts1
        self.acts2 = acts2
        self.exist_in_train = exist_in_train
        self.reset()

    def addUtter(self, utter):
        output = {'utter_index': utter['utter_index']}

        topic = utter['segment_info']['topic']
        transcript = utter['transcript'].replace('Singapore', '')
        words = re.sub('[ ][ ]+',' ',transcript).split(' ')

        if utter['segment_info']['target_bio'] == 'B':
            self.frame = {}
        
        acts = []
        if utter['segment_info']['target_bio'] in ['B','I']:
            acts1 = self.acts1.readline().strip('\n\r').split(' <=> ')[1]
            acts2_line = self.acts2.readline().strip('\n\r')
            #print acts2_line
            acts2 = acts2_line.split(' <=> ')[1]
            acts0 = acts1
            if acts0 != "":
                for chunk in acts0[:-1].split(') '):
                    slot_value = chunk.split('(')[1]
                    if slot_value != "":
                        slot,value = slot_value.split('=')
                        if slot not in self.frame:
                            self.frame[slot] = []
                        if value not in self.frame[slot]:
                            self.frame[slot].append(value)
            acts0 = acts2
            if acts0 != "":
                for chunk in acts0[:-1].split(') '):
                    slot_value = chunk.split('(')[1]
                    if slot_value != "":
                        slot,value = slot_value.split('=')
                        #if slot in self.exist_in_train[topic] and value in self.exist_in_train[topic][slot]:
                        #    continue
                        #if slot in self.frame:
                        #    continue
                        if slot == "INFO":
                            continue
                        if slot not in self.frame:
                            self.frame[slot] = []
                        if value not in self.frame[slot]:
                            self.frame[slot].append(value)
            if 'PLACE' in self.frame and 'NEIGHBOURHOOD' in self.frame:
                self.frame['PLACE'] = list(set(self.frame['PLACE']) - set(self.frame['NEIGHBOURHOOD']))
                if self.frame['PLACE'] == []:
                    del self.frame['PLACE']

            output['frame_label'] = self.frame
            if acts == []:
                acts = ['o()']
            print transcript+' <=> '+' '.join(acts)
        return output

    def reset(self):
        self.frame = {}

def main(argv):
        parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
        parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
        parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
        parser.add_argument('--trackfile',dest='trackfile',action='store',required=True,metavar='JSON_FILE', help='File to write with tracker output')
        parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
        parser.add_argument('--acts1',dest='acts1',action='store',metavar='FILE',required=True,help='Acts file')
        parser.add_argument('--acts2',dest='acts2',action='store',metavar='FILE',required=True,help='Acts file')

        args = parser.parse_args()
        
        dataset_train = dataset_walker.dataset_walker('dstc4_train',dataroot=args.dataroot,labels=True)
        exist_in_train = {}
        for call in dataset_train:
            for (log_utter,labels_utter) in call:
                topic = log_utter['segment_info']['topic']
                if topic not in exist_in_train:
                    exist_in_train[topic] = {}
                target_bio = log_utter['segment_info']['target_bio']
                if log_utter['segment_info']['target_bio'] in ['B','I']:
                    frame_label = labels_utter["frame_label"]
                    goal_acts = []
                    for slot in frame_label:
                        for value in frame_label[slot]:
                            if slot not in exist_in_train[topic]:
                                exist_in_train[topic][slot] = [value]
                            else:
                                exist_in_train[topic][slot].append(value)
        
        dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=False)
        tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()

        track_file = open(args.trackfile, "wb")
        track = {"sessions":[]}
        track["dataset"]  = args.dataset
        start_time = time.time()

        tracker = BaselineTracker(tagsets,open(args.acts1,'rb'),open(args.acts2,'rb'),exist_in_train)
        for call in dataset:
                this_session = {"session_id":call.log["session_id"], "utterances":[]}
                tracker.reset()
                for (utter,_) in call:
                        sys.stderr.write('%d:%d\n'%(call.log['session_id'], utter['utter_index']))
                        tracker_result = tracker.addUtter(utter)
                        if tracker_result is not None:
                                this_session["utterances"].append(tracker_result)
                track["sessions"].append(this_session)
        end_time = time.time()
        elapsed_time = end_time - start_time
        track['wall_time'] = elapsed_time

        json.dump(track, track_file, indent=4)

        track_file.close()

if __name__ =="__main__":
        main(sys.argv)
