import argparse, sys, ontology_reader, dataset_walker, time, json
import string,re

from fuzzywuzzy import fuzz

class BaselineTracker(object):
    def __init__(self, tagsets, ratio):
        self.tagsets = tagsets
        self.frame = {}
        self.memory = {}
        self.ratio = ratio
        self.reset()

    def preprocessing(self,sentence):
        sentence = sentence.lower()
        sentence = sentence.replace('\"','')
        sentence = re.sub('[ ][ ]+',' ',sentence)
        sentence = sentence.strip()
        word_list = sentence.split(' ')
        new_word_list = []
        for word in word_list:
            word = word.strip("-~`")
            if word == "":
                continue
            elif word[-1] in ".,?!":
                new_word_list.append(word[:-1].strip(".,?!-~"))
                new_word_list.append(word[-1])
            else:
                new_word_list.append(word)
        for idx,word in enumerate(new_word_list):
            word = re.sub('%huh|%hmm|%hm|%eh|%um|%uh|%ah|%oh|%h','',word)
            word = word.replace("%","")
            new_word_list[idx] = word
        new_word_list = [key for key in new_word_list if key != ""]
        return ' '.join(new_word_list)

    def solve_value(self,value):
        number_map = {'1':'one','2':'two','3':'three','4':'four','5':'five','6':'six','7':'seven','8':'eight','9':'nine','0':'zero'}
        value = value.lower()
        new_words = []
        for word in value.split(' '):
            word = word.strip(",.?!+-@~"+"\'")
            if word == "":
                continue
            else:
                word = re.sub("[,.?!+\-@~]",' ',word)
                tmp_words = word.split(' ')
                new_tmp_words = []
                for tmp_word in tmp_words:
                    if tmp_word in number_map:
                        new_tmp_words.append(number_map[tmp_word])
                    else:
                        new_tmp_words.append(tmp_word)
                new_words += new_tmp_words
        return ' '.join(new_words)

    def addUtter(self, utter):
        output = {'utter_index': utter['utter_index']}

        topic = utter['segment_info']['topic']
        transcript = utter['transcript'].replace('Singapore', '')
        words = re.sub('[ ][ ]+',' ',transcript).split(' ')

        if utter['segment_info']['target_bio'] == 'B':
            self.frame = {}
        
        acts = []
        if topic in self.tagsets:
            for slot in self.tagsets[topic]:
                for value in self.tagsets[topic][slot]:
                    #ratio = fuzz.partial_ratio(value.lower(), transcript.lower())
                    ratio = fuzz.partial_ratio(' '+self.solve_value(value)+' ', ' '+self.preprocessing(transcript)+' ')
                    if ratio > self.ratio:#80:
                        act = 'inform('+slot+'='+value+')'
                        if act not in acts:
                            acts.append(act)
                        if slot not in self.frame:
                            self.frame[slot] = []
                        if value not in self.frame[slot]:
                            self.frame[slot].append(value)
            if topic == 'ATTRACTION' and 'PLACE' in self.frame and 'NEIGHBOURHOOD' in self.frame and self.frame['PLACE'] == self.frame['NEIGHBOURHOOD']:
                del self.frame['PLACE']

            output['frame_label'] = self.frame
            if acts == []:
                acts = ['o()']
            print transcript+' <=> '+self.preprocessing(transcript)+' <=> '+' '.join(acts)
        return output

    def reset(self):
        self.frame = {}

def main(argv):
        parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
        parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
        parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
        parser.add_argument('--trackfile',dest='trackfile',action='store',required=True,metavar='JSON_FILE', help='File to write with tracker output')
        parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
        parser.add_argument('--ratio',dest='ratio',action='store',metavar='NUMBER',required=True,help='ratio number')

        args = parser.parse_args()
        dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=False)
        tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()

        track_file = open(args.trackfile, "wb")
        track = {"sessions":[]}
        track["dataset"]  = args.dataset
        start_time = time.time()

        tracker = BaselineTracker(tagsets, int(args.ratio))
        for call in dataset:
                this_session = {"session_id":call.log["session_id"], "utterances":[]}
                tracker.reset()
                for (utter,_) in call:
                        sys.stderr.write('%d:%d\n'%(call.log['session_id'], utter['utter_index']))
                        tracker_result = tracker.addUtter(utter)
                        if tracker_result is not None:
                                this_session["utterances"].append(tracker_result)
                track["sessions"].append(this_session)
        end_time = time.time()
        elapsed_time = end_time - start_time
        track['wall_time'] = elapsed_time

        json.dump(track, track_file, indent=4)

        track_file.close()

if __name__ =="__main__":
        main(sys.argv)
