import argparse, sys, ontology_reader, dataset_walker, time, json
import string,re

from fuzzywuzzy import fuzz

usingbaseline = False #True
contextdep = False #True

class BaselineTracker(object):
    def __init__(self, tagsets, knowledge, decodeout):
        self.tagsets = tagsets
        self.frame = {}
        self.memory = {}
        self.memory['place'] = {}

        self.decodeout = decodeout
        self.decodeout_session_pointer = 0
        self.decodeout_turn_pointer = 0

        self.knowledge = knowledge
        self.ptypes = {}
        for slot in self.knowledge:
            for i in range(len(self.knowledge[slot])):
                if "TYPE_OF_PLACE" not in self.knowledge[slot][i]:
                    continue
                if "NAME" not in self.knowledge[slot][i]:
                    continue
                val = self.knowledge[slot][i]["NAME"].lower()
                for tp in self.knowledge[slot][i]["TYPE_OF_PLACE"]:
                    tp = tp.lower()
                    if val not in self.ptypes:
                        self.ptypes[val] = []
                    self.ptypes[val] += [tp]
        self.last_utter_idx = 0
        
        self.reset()

    def get_turn_from_decodeout(self):
        turn_acts = self.decodeout['sessions'][self.decodeout_session_pointer]['turns'][self.decodeout_turn_pointer]["slu-hyps"]
        if self.decodeout_turn_pointer < len(self.decodeout['sessions'][self.decodeout_session_pointer]['turns'])-1:
            self.decodeout_turn_pointer += 1
        else:
            self.decodeout_turn_pointer = 0
            self.decodeout_session_pointer += 1
        return turn_acts

    def preprocessing(self,sentence):
        sentence = sentence.lower()
        sentence = sentence.replace('\"','')
        sentence = re.sub('[ ][ ]+',' ',sentence)
        sentence = sentence.strip()
        word_list = sentence.split(' ')
        new_word_list = []
        for word in word_list:
            word = word.strip("-~`")
            if word == "":
                continue
            elif word[-1] in ".,?!":
                new_word_list.append(word[:-1].strip(".,?!-~"))
                new_word_list.append(word[-1])
            else:
                new_word_list.append(word)
        for idx,word in enumerate(new_word_list):
            word = re.sub('%huh|%hmm|%hm|%eh|%um|%uh|%ah|%oh|%h','',word)
            word = word.replace("%","")
            new_word_list[idx] = word
        new_word_list = [key for key in new_word_list if key != ""]
        return ' '.join(new_word_list)

    def solve_value(self,value):
        number_map = {'1':'one','2':'two','3':'three','4':'four','5':'five','6':'six','7':'seven','8':'eight','9':'nine','0':'zero'}
        value = value.lower()
        new_words = []
        for word in value.split(' '):
            word = word.strip(",.?!+-@~"+"\'")
            if word == "":
                continue
            else:
                word = re.sub("[,.?!+\-@~]",' ',word)
                tmp_words = word.split(' ')
                new_tmp_words = []
                for tmp_word in tmp_words:
                    if tmp_word in number_map:
                        new_tmp_words.append(number_map[tmp_word])
                    else:
                        new_tmp_words.append(tmp_word)
                new_words += new_tmp_words
        return ' '.join(new_words)
    
    def addUtter(self, utter):
        output = {'utter_index': utter['utter_index']}

        topic = utter['segment_info']['topic']
        #transcript
        transcript = self.preprocessing(utter['transcript'])
        #transcript = utter['transcript'].replace('Singapore', '')

        if utter['segment_info']['target_bio'] == 'B':
            self.frame = {}
            if utter['utter_index'] - self.last_utter_idx < 1:
                self.memory['place'] = {}
        if utter['segment_info']['target_bio'] in ['B','I']:
            self.last_utter_idx = utter['utter_index']

        acts = []
        if topic in self.tagsets:
            if contextdep:
                replaced_type_of_place = [] #['museum',]
                take_from_previous = []
                for type_of_place in self.memory['place']:
                    slot,value = self.memory['place'][type_of_place]
                    if slot in self.tagsets[topic] and value in self.tagsets[topic][slot]:
                        for pron in ['the '+type_of_place,'this '+type_of_place,'that '+type_of_place]:
                            if ' '+pron+' ' in ' '+transcript+' ':
                                #print '##',pron,transcript
                                transcript = (' '+transcript+' ').replace(' '+pron+' ',' ')
                                transcript = transcript.strip(' ')
                                acts.append('inform('+slot+'='+value+')')
                                if slot not in self.frame:
                                    self.frame[slot] = []
                                if value not in self.frame[slot]:
                                    self.frame[slot].append(value)
                                replaced_type_of_place.append(type_of_place)
                                take_from_previous.append(slot)
                                break
        
            decode_turn_acts = self.get_turn_from_decodeout()
            top_acts = decode_turn_acts[0]
            if top_acts["slu-hyp"] == []:
                pass
            else:
                for act in top_acts["slu-hyp"]:
                    if act['slots'] == []:
                        continue
                    else:
                        slot,value = act["slots"][0]
                        #if value == "Songs of the Sea":
                        #    continue
                        if value == 'Show' and ('show me' in transcript or 'show you' in transcript or 'you show' in transcript):
                            continue
                        if contextdep:
                            if slot == "TYPE_OF_PLACE" and value.lower() in replaced_type_of_place:
                                continue
                            #if slot in take_from_previous:
                            #    continue
                        acts.append('inform('+slot+'='+value+')')
                        if slot not in self.frame:
                            self.frame[slot] = []
                        if value not in self.frame[slot]:
                            self.frame[slot].append(value)
                        if contextdep:
                            _value = value.lower()
                            if _value in self.ptypes:
                                for _t in self.ptypes[_value]:
                                    self.memory['place'][_t] = (slot,value)
            
            if usingbaseline:
                #"""       
                #bf0
                for slot in self.tagsets[topic]:
                    if slot in self.frame: continue
                    bestv = 0
                    for value in self.tagsets[topic][slot]:
                        if len(transcript) < 6: continue
                        if len(value) < 6 and ' ' not in value and len(transcript) > 10: continue
                        #ratio = fuzz.partial_ratio(value.lower().replace('-', ' '), transcript)
                        ratio = fuzz.partial_ratio(self.solve_value(value), transcript)
                        if ratio >= 90 or (ratio >= 85 and (len(value) > 8 or ' ' in value)):
                            if ratio > bestv:
                                ok = True
                                for _s in self.frame:
                                    for _v in self.frame[_s]:
                                        #if value.lower().replace('-', ' ') in _v.lower().replace('-', ' '):
                                        if self.solve_value(value) in self.solve_value(_v):
                                            ok = False
                                            break
                                    if not ok:
                                        break
                                if ok:
                                    self.frame[slot] = [value]
                                    acts.append('inform('+slot+'='+value+')') #sz128
                                    bestv = ratio
                                    if contextdep:
                                        _value = value.lower()
                                        if _value in self.ptypes:
                                            for _t in self.ptypes[_value]:
                                                self.memory['place'][_t] = (slot,value)
                #"""
            #if topic == 'ATTRACTION' and 'PLACE' in self.frame and 'NEIGHBOURHOOD' in self.frame and self.frame['PLACE'] == self.frame['NEIGHBOURHOOD']:
            #    del self.frame['PLACE']
            if 'PLACE' in self.frame and 'NEIGHBOURHOOD' in self.frame:
                self.frame['PLACE'] = list(set(self.frame['PLACE']) - set(self.frame['NEIGHBOURHOOD']))
                if self.frame['PLACE'] == []:
                    del self.frame['PLACE']

            output['frame_label'] = self.frame
            print transcript+' <=> '+' '.join(acts)
        return output

    def reset(self):
        self.frame = {}
        self.memory['place'] = {}

def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--trackfile',dest='trackfile',action='store',required=True,metavar='JSON_FILE', help='File to write with tracker output')
    parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
    parser.add_argument('--decodeout',dest='decodeout',action='store',metavar='JSON_FILE',required=True,help='JSON decode output file')

    args = parser.parse_args()
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=False)
    tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
    knowledge = ontology_reader.OntologyReader(args.ontology).get_knowledge()

    decodeout = json.load(open(args.decodeout,'rb'))

    track_file = open(args.trackfile, "wb")
    track = {"sessions":[]}
    track["dataset"]  = args.dataset
    start_time = time.time()

    tracker = BaselineTracker(tagsets,knowledge,decodeout)
    for call in dataset:
        this_session = {"session_id":call.log["session_id"], "utterances":[]}
        tracker.reset()
        for (utter,_) in call:
            sys.stderr.write('%d:%d\n'%(call.log['session_id'], utter['utter_index']))
            tracker_result = tracker.addUtter(utter)
            if tracker_result is not None:
                this_session["utterances"].append(tracker_result)
        track["sessions"].append(this_session)
    end_time = time.time()
    elapsed_time = end_time - start_time
    track['wall_time'] = elapsed_time

    json.dump(track, track_file, indent=4)

    track_file.close()

if __name__ =="__main__":
        main(sys.argv)
