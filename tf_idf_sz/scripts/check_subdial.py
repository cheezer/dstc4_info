import argparse, sys, ontology_reader, dataset_walker, time, json

def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')

    args = parser.parse_args()
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=True)

    previous_frame_labels = {}
    last_turn_bio = None
    for call in dataset:
        session_id = call.log['session_id']
        for log_utter,labels_utter in call:
            turn_id = log_utter['utter_index']
            bio = log_utter['segment_info']['target_bio']
            if bio == 'B':
                for slot in previous_frame_labels:
                    for value in previous_frame_labels[slot]:
                        if last_turn_bio in ['B','I'] and slot in labels_utter['frame_label'] and value in labels_utter['frame_label'][slot]:
                            print str(session_id)+':'+str(turn_id)+' <=> '+slot+'='+value 
                previous_frame_labels = labels_utter['frame_label']
            last_turn_bio = bio



if __name__ =="__main__":
	main(sys.argv)
