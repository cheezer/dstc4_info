
import argparse, sys, ontology_reader, dataset_walker, time, json
import re


def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--output',dest='output',action='store',required=True,metavar='FILE', help='')
    parser.add_argument('--outofdial',dest='outofdial',action='store',required=False,metavar='FILE', help='')

    args = parser.parse_args()
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=True)
    
    output = open(args.output,'wb')
    if args.outofdial != None:
        outofdial = open(args.outofdial,'wb')
    for call in dataset:
        session_id = call.log["session_id"]
        for (log_utter,labels_utter) in call:
            topic = log_utter['segment_info']['topic']
            target_bio = log_utter['segment_info']['target_bio']
            speaker = log_utter["speaker"]
            utter_index = log_utter["utter_index"]
            transcription = log_utter["transcript"]
            if log_utter['segment_info']['target_bio'] not in ['B','I']:
                if args.outofdial != None:
                    semantic_tagged = labels_utter["semantic_tagged"]
                    null_sentences = []
                    for sub_sentence in semantic_tagged:
                        if sub_sentence == "":
                            continue
                        elif '<' not in sub_sentence:
                            null_sentences.append(sub_sentence)
                    if null_sentences != []:
                        output_line = str(session_id)+':'+str(utter_index)+':'+speaker+':'+topic+':'+target_bio+' <=> '+' '.join(null_sentences)+' <=> '
                        outofdial.write(output_line.encode('utf8')+'\n')
            else:
                frame_label = labels_utter["frame_label"]
                goal_acts = []
                for slot in frame_label:
                    for value in frame_label[slot]:
                        goal_acts.append('inform('+slot+'='+value+')')
                if goal_acts == []:
                    goal_acts.append('null()')
                output_line = str(session_id)+':'+str(utter_index)+':'+speaker+':'+topic+':'+target_bio+' <=> '+transcription+' <=> '+' '.join(goal_acts)
                output.write(output_line.encode('utf8')+'\n')


if __name__ =="__main__":
    main(sys.argv)
