
import argparse, sys, ontology_reader, dataset_walker, time, json
import re


def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--aligned',dest='aligned',action='store',required=True,metavar='FILE', help='')
    parser.add_argument('--unaligned',dest='unaligned',action='store',required=True,metavar='FILE', help='')
    #parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')

    args = parser.parse_args()
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=True)
    #tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
    
    unaligned = open(args.unaligned,'wb')
    aligned = open(args.aligned,'wb')
    for call in dataset:
        for (log_utter,labels_utter) in call:
            #if log_utter['segment_info']['target_bio'] in ['B','I']:
            semantic_tags = []
            semantic_tagged = labels_utter["semantic_tagged"]
            aligned_labelled_sentence = []
            for sub_sentence in semantic_tagged:
                if sub_sentence == "":
                    continue
                else:
                    chunks = re.split('</[^/<>]+>|<',sub_sentence)
                    new_sub_sentence = []
                    for chunk in chunks:
                        chunk = chunk.strip()
                        if chunk == "":
                            continue
                        elif '>' in chunk:
                            content = chunk
                            tags = content.split('>')[0]
                            value = content.split('>')[1].strip()
                            main = tags.split(' ')[0]
                            semantic_tags.append('<'+tags+'>'+value+'</'+main+'>')
                            
                            other_tags = tags.split(' ')[1:]
                            other_tags_dict = {'CAT':None,'REL':None,'FROM-TO':None}
                            for key in other_tags:
                                tag, val = key.split('=')
                                if val[0] == '\"':
                                    val = val[1:-1]
                                other_tags_dict[tag] = val
                            new_chunk = []
                            if other_tags_dict['CAT'] == 'MAIN':
                                new_chunk.append(main)
                            else:
                                new_chunk.append(other_tags_dict['CAT'])
                            if other_tags_dict['REL'] != None:
                                new_chunk.append(other_tags_dict['REL'])
                            if other_tags_dict['FROM-TO'] != None:
                                new_chunk.append(other_tags_dict['FROM-TO'])
                            new_chunk = '['+value+':'+':'.join(new_chunk)+']'
                            new_sub_sentence.append(new_chunk)
                        else:
                            new_sub_sentence.append(chunk)
                    if new_sub_sentence != []:
                        aligned_labelled_sentence.append(' '.join(new_sub_sentence))
            if aligned_labelled_sentence != []:
                aligned.write(' '.join(aligned_labelled_sentence).encode('utf8')+'\n')

            transcription = log_utter["transcript"]
            output_line = transcription+' <=> '+' '.join(semantic_tags)
            unaligned.write(output_line.encode('utf8')+'\n')


if __name__ =="__main__":
    main(sys.argv)
