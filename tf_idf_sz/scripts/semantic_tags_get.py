
import argparse, sys, ontology_reader, dataset_walker, time, json



def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    #parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')

    args = parser.parse_args()
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=True)
    #tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()

    for call in dataset:
        for (log_utter,labels_utter) in call:
            semantic_tagged = labels_utter["semantic_tagged"]
            for sub_sentence in semantic_tagged:
                if sub_sentence == "":
                    continue
                elif '<' in sub_sentence:
                    print sub_sentence.encode('utf8')

if __name__ =="__main__":
    main(sys.argv)
