
import argparse, sys, ontology_reader, dataset_walker, time, json, string

## extra feature set
### I: speech_act
speech_act = {}
category = ["QST","RES","INI","FOL"]
attribute = ["ACK","CANCEL","CLOSING","COMMIT","CONFIRM","ENOUGH","EXPLAIN","HOW_MUCH","HOW_TO","INFO","NEGATIVE","OPENING","POSITIVE","PREFERENCE","RECOMMEND","THANK","WHAT","WHEN","WHERE","WHICH","WHO"]
for cate in category:
    speech_act[len(speech_act)] = cate
    for attr in attribute:
        speech_act[len(speech_act)] = cate+'-'+attr
speech_act[len(speech_act)] = "None"
### II: segment_info
segment_info = {}
info = {"topic":["OPENING","CLOSING","ITINERARY","ACCOMMODATION","ATTRACTION","FOOD","SHOPPING","TRANSPORTATION"],
                "target_bio":['B','I','O'],
                "guide_act":["QST","ANS","REQ","REQ_ALT","EXPLAIN","RECOMMEND","ACK","NONE"],
                "tourist_act":["QST","ANS","REQ","REQ_ALT","EXPLAIN","RECOMMEND","ACK","NONE"],
                'initiativity':["GUIDE","TOURIST"]}
for key in info.keys():
    for value in info[key]:
        segment_info[len(segment_info)] = key+'-'+value


def get_extra(extra_label, last_out):
    extra = []
    for index in range(len(extra_label)):
        if extra_label[index] in last_out:
            extra.append('1')
        else:
            extra.append('0')
    return ' '.join(extra)


def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--speaker',dest='speaker',action='store',required=True,metavar='STRING', help='Guide/Tourist')
    parser.add_argument('--outsnt',dest='outsnt',action='store',required=True,metavar='FILE', help='')
    parser.add_argument('--outextra',dest='outextra',action='store',required=True,metavar='FILE', help='Will look for corpus in <destroot>/<dataset>/...')
    #parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')

    args = parser.parse_args()
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=True)
    speaker = args.speaker
    outsnt = open(args.outsnt,'wb')
    outextra = open(args.outextra,'wb')
    #tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()

    for index in range(len(speech_act)):
        outextra.write(str(index)+':'+speech_act[index]+' ')
    outextra.write("\n")
    
    for call in dataset:
        last_turn_that_speaker_cache = []
        last_speaker = None
        last_turn_this_speaker_cache = []
        for (log_utter,labels_utter) in call:
            utter_index = str(call.log['session_id']) + '-' + str(log_utter['utter_index'])
            # get label
            speech_act_tuple = []
            speech_acts = labels_utter["speech_act"]
            for act in speech_acts:
                acttype = act['act'].strip()
                if acttype == "":
                    acttype = "None"
                speech_act_tuple.append(acttype)
                for attribute in act["attributes"]:
                    attribute = attribute.strip()
                    if attribute != "":
                        speech_act_tuple.append(acttype+'-'+attribute)
            speech_act_tuple = list(set(speech_act_tuple)) # unique 
            if "None" in speech_act_tuple and len(speech_act_tuple) > 1:
                speech_act_tuple = [key for key in speech_act_tuple if key != "None"]
            # extra info : segment ingo
            this_segment_info = []
            for key in log_utter["segment_info"]:
                this_segment_info.append(key+'-'+log_utter["segment_info"][key])
            # fake speech_act context from true label
            that_speaker_last_out = []
            this_speaker_last_out = []
            if log_utter["speaker"] == last_speaker:
                last_speaker = log_utter["speaker"]
                this_speaker_last_out = last_turn_this_speaker_cache
                that_speaker_last_out = []
                last_turn_that_speaker_cache = []
                last_turn_this_speaker_cache = speech_act_tuple
            else:
                last_speaker = log_utter["speaker"]
                this_speaker_last_out = last_turn_that_speaker_cache
                that_speaker_last_out = last_turn_this_speaker_cache
                last_turn_that_speaker_cache = last_turn_this_speaker_cache
                last_turn_this_speaker_cache = speech_act_tuple
            
            if speaker == log_utter["speaker"]:
                transcription = log_utter["transcript"]
                output_line = utter_index+' <=> '+transcription+' <=> '+','.join(speech_act_tuple)
                outsnt.write(output_line.encode('utf8')+'\n')
                extra_fea = ''
                #extra_fea += ' '+get_extra(speech_act,that_speaker_last_out)
                #extra_fea += ' '+get_extra(speech_act,this_speaker_last_out)
                extra_fea += ' '+get_extra(segment_info,this_segment_info)
                outextra.write(extra_fea+'\n')
    outsnt.close()
    outextra.close()

if __name__ =="__main__":
    main(sys.argv)
