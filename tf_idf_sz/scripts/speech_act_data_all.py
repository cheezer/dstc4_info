
import argparse, sys, ontology_reader, dataset_walker, time, json



def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--outsnt',dest='outsnt',action='store',required=True,metavar='FILE', help='')
    parser.add_argument('--outextra',dest='outextra',action='store',required=True,metavar='FILE', help='Will look for corpus in <destroot>/<dataset>/...')
    #parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')

    args = parser.parse_args()
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=True)
    outsnt = open(args.outsnt,'wb')
    outextra = open(args.outextra,'wb')
    #tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
    
    outextra.write("speaker=Guide\n")

    for call in dataset:
        for (log_utter,labels_utter) in call:
            utter_index = str(call.log['session_id']) + '-' + str(log_utter['utter_index'])
            speech_act_tuple = []
            speech_acts = labels_utter["speech_act"]
            for act in speech_acts:
                acttype = act['act'].strip()
                if acttype == "":
                    acttype = "None"
                speech_act_tuple.append(acttype)
                for attribute in act["attributes"]:
                    attribute = attribute.strip()
                    if attribute != "":
                        speech_act_tuple.append(acttype+'-'+attribute)
            speech_act_tuple = list(set(speech_act_tuple)) # unique 
            if "None" in speech_act_tuple and len(speech_act_tuple) > 1:
                speech_act_tuple = [key for key in speech_act_tuple if key != "None"]
            
            transcription = log_utter["transcript"]
            output_line = utter_index+' <=> '+transcription+' <=> '+','.join(speech_act_tuple)
            outsnt.write(output_line.encode('utf8')+'\n')

            outextra.write(str(int("Guide"==log_utter["speaker"]))+'\n')
    outsnt.close()
    outextra.close()

if __name__ =="__main__":
    main(sys.argv)
