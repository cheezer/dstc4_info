import argparse,sys,string,re,os
import math,json

def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--knowledge',dest='knowledge',action='store',metavar='FILE',required=True,help='Class description path')
    #parser.add_argument('--word2vec',dest='word2vec',action='store',metavar='FILE',required=True,help='word embedding file')
    parser.add_argument('--features',dest='features',action='store',metavar='FILE',required=True,help='Class feature path')
    args = parser.parse_args()
    
    feature_space = {}
    words_idf = {}
    for topic in os.listdir(args.knowledge):
        knowledge_filename = os.path.join(args.knowledge,topic)
        if os.path.isdir(knowledge_filename):
            continue
        feature_space[topic] = {}
        words_idf[topic] = {}
        D = 0
        data_space = {}
        knowledge = open(knowledge_filename,'rb')
        for line in knowledge:
            line = line.strip()
            text, labels = line.split('<=>')
            
            words = text.split(' ')
            data_space[D] = (words,labels)
            for word in set(words):
                if word not in words_idf[topic]:
                    words_idf[topic][word] = [len(words_idf[topic]),1] #[idx,value]
                else:
                    words_idf[topic][word][1] += 1
                if word == "singapore" and topic == "FOOD":
                    print line

            D += 1
        
        for word in words_idf[topic]:
            words_idf[topic][word][1] = math.log(float(D)/words_idf[topic][word][1])

        #word_penalty_set = {'on':0.1,'in':0.1,'at':0.1,'by':0.1,'of':0.1,'the':0.1,'and':0.1, '&':0.01,'-':0.01,'@':0.01}
        for i in range(D):
            #feature = [0.0]*len(words_idf[topic])
            words_feats = {}
            words, labels = data_space[i]
            for word in words:
                if word not in words_feats:
                    words_feats[word] = 1
                else:
                    words_feats[word] += 1
            for word in words_feats:
                #tf = 1 + math.log(words_feats[word]/float(len(words)))
                tf = words_feats[word] #/float(len(words))
                #feature[words_idf[topic][word][0]] = tf*words_idf[topic][word][1]
                words_feats[word] *= words_idf[topic][word][1]
            feature_space[topic][' '.join(words)+'<=>'+labels] = json.dumps(words_feats) #feature
            
    
    for topic in feature_space:
        features = open(os.path.join(args.features,topic),'wb')
        for text_label in feature_space[topic]:
            feature = feature_space[topic][text_label]
            #feature = [str(val) for val in feature]
            features.write(text_label+'<=>'+feature+'\n')
        word_idf = open(os.path.join(args.features,topic+'.idf'),'wb')
        for word in words_idf[topic]:
            word_idf.write(' '.join([word,str(words_idf[topic][word][0]),str(words_idf[topic][word][1])])+'\n')


if __name__ =="__main__":
	main(sys.argv)
