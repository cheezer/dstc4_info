import argparse,sys,json,types,os
import re,string

def solve_unk_word(word):
    number_map = {'1':'one','2':'two','3':'three','4':'four','5':'five','6':'six','7':'seven','8':'eight','9':'nine','0':'zero'}
    word = word.strip(",.?!+-@~"+"\'")
    if word == "":
        return []
    else:
        word = re.sub("[,.?!+\-@~]",' ',word)
        tmp_words = word.split(' ')
        new_tmp_words = []
        for tmp_word in tmp_words:
            if tmp_word in number_map:
                new_tmp_words.append(number_map[tmp_word])
            else:
                if tmp_word in ['o\'clock']:
                    new_tmp_words.append(tmp_word)
                elif '\'' in tmp_word:
                    tmp_word = tmp_word.replace('\'',' \'').split(' ')
                    for key in tmp_word:
                        new_tmp_words.append(key)
                else:
                    new_tmp_words.append(tmp_word)
        return new_tmp_words
    
def preprocess(text):
    tmp_words = text.split(' ')
    words = []
    for word in tmp_words:
        new_sub_words = solve_unk_word(word)
        for sub_word in new_sub_words:
            words.append(sub_word)
    return ' '.join(words)

class OntologyReader():
    def __init__(self, ontology_file_name):
        self.ontology = json.load(open(ontology_file_name, 'r'))
        self.tagsets = self.ontology['tagsets']
        self.knowledge = self.ontology['knowledge']

        for topic in self.tagsets:
            for slot in self.tagsets[topic]:
                value_list = []
                for value in self.tagsets[topic][slot]:
                    if type(value) in types.StringTypes:
                        value_list.append(value)
                    elif type(value) == types.DictType:
                        source = value['source']
                        value_slot = value['slot']
                        for entry in self.knowledge[source]:
                            value_list.append(entry[value_slot])
                value_list = sorted(set(value_list))
                self.tagsets[topic][slot] = value_list
    
    def description(self, output_path):
        value_map = {'Historic site':'historical places','Cultural site':'cultural places',
                'Nightclub':'night club','Dresscode':'dress code','Thankyou':'thank you',
                'Parkroyal':'park royal','Northpoint':'north point','Fairprice':'fair price',
                'Riverview':'river view','Parkview':'park view','Waterpark':'water park','Artscinece':'art scinece',
                'Fishball':'fish ball','Southbridge':'south bridge','Pricerange':'price range','Westgate':'west gate',
                }
        value_map = {key.lower():value_map[key] for key in value_map}
        for topic in self.tagsets:
            output_filename = os.path.join(output_path,topic)
            output_file = open(output_filename,'wb')
            for slot in self.tagsets[topic]:
                if slot == 'LINE':
                    for item in self.knowledge['MRT_LINE']:
                        color = item['COLOR']
                        value = item['NAME']
                        output_file.write(preprocess(color.lower()+' line')+'<=>'+'inform('+slot+'='+value+')\n')
                prefix = ''
                if slot == "FROM":
                    prefix = 'from '
                elif slot == "TO":
                    prefix = 'to '
                for value in self.tagsets[topic][slot]:
                    value1 = value
                    if ' MRT Station' in value:
                        value1 = value.replace(' #MRT_STATION','')
                        value = value.replace(' #MRT_STATION',' MRT Station')
                    output_file.write(preprocess(prefix+value1.lower())+'<=>'+'inform('+slot+'='+value+')\n') 
                    if value.lower() in value_map:
                        value1 = value_map[value.lower()]
                        output_file.write(preprocess(prefix+value1.lower())+'<=>'+'inform('+slot+'='+value+')\n') 
            output_file.close()

def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
    parser.add_argument('--knowledge',dest='knowledge',action='store',metavar='FILE',required=True,help='Class description path')
    args = parser.parse_args()
    OntologyReader(args.ontology).description(args.knowledge)

if __name__ =="__main__":
    main(sys.argv)
