import math
import numpy as np

class tf_idf:
    def __init__(self, words_idf, knowledge):
        self.words_idf = words_idf
        self.knowledge = {}
        for topic in knowledge:
            if topic not in self.knowledge:
                self.knowledge[topic] = {}
            for sentence in knowledge[topic]:
                words = sentence.split(' ')
                words_feats = {}
                for word in words:
                    if word not in words_feats:
                        words_feats[word] = 1
                    else:
                        words_feats[word] += 1
                length = 0.0
                for word in words_feats:
                    tf = words_feats[word]
                    words_feats[word] = tf*self.words_idf[topic][word]
                    length += words_feats[word]**2
                length = length**0.5
                for act in knowledge[topic][sentence]:
                    if act not in self.knowledge[topic]:
                        self.knowledge[topic][act] = [(words_feats,length,sentence)]
                    else:
                        self.knowledge[topic][act] += [(words_feats,length,sentence)]
    
    def get_words_feats(self, topic, utter_words):
        feature = {}
        words_feats = {}
        for word in utter_words:
            if word not in words_feats:
                words_feats[word] = 1
            else:
                words_feats[word] += 1
        for word in words_feats:
            if word in self.words_idf[topic]:
                tf = words_feats[word]
                feature[word] = tf*self.words_idf[topic][word]
            else: ##??
                pass
        return feature
    
    def scoring_in_knowledge(self, topic, utter_words):
        if len(utter_words) == 0:
            return 0
        else:
            if topic == "TRANSPORTATION":
                from_words,to_words = [],[]
                from_flag,to_flag = False,False
                for word in utter_words:
                    if word in ['from']:
                        from_flag = True
                        from_words.append(word)
                    elif word in ['to','goto']:
                        to_flag = True
                        to_words.append(word)
                    else:
                        if from_flag:
                            from_words.append(word)
                        elif to_flag:
                            to_words.append(word)
                        else:
                            pass
                from_feature = self.get_words_feats(topic, from_words) 
                to_feature = self.get_words_feats(topic, to_words) 
            feature = self.get_words_feats(topic, utter_words)
            output = {}
            for act in self.knowledge[topic]:
                tmp_feature = feature
                '''if topic == "TRANSPORTATION":
                    if '(FROM=' in act:
                        tmp_feature = from_feature
                    elif '(TO=' in act:
                        tmp_feature = to_feature
                '''
                best_score = 0
                best_sentence = ''
                for act_feats,length2,sentence in self.knowledge[topic][act]:
                    length1 = 0.0
                    cosine_sim = 0.0
                    related_count = 0.0
                    for word in tmp_feature:
                        if word in act_feats:
                            related_count += 1
                            length1 += tmp_feature[word]**2
                            cosine_sim += act_feats[word]*tmp_feature[word]
                    length1 = length1**0.5
                    if length1 < length2:
                        cosine_sim = cosine_sim/(length2*length2)
                    else:
                        cosine_sim = cosine_sim/(length1*length2)
                    if cosine_sim > best_score:
                        best_score = cosine_sim
                        best_sentence = sentence
                output[act] = [best_score,best_sentence]
            return output

    def scoring_act(self, topic, utter_words, act):
        if len(utter_words) == 0:
            return 0
        else:
            feature = {}
            words_feats = {}
            for word in utter_words:
                if word not in words_feats:
                    words_feats[word] = 1
                else:
                    words_feats[word] += 1
            for word in words_feats:
                if word in self.words_idf[topic]:
                    tf = words_feats[word]
                    feature[word] = tf*self.words_idf[topic][word]
                else: ##??
                    pass
            best_score = 0
            for act_feats,length2,sentence in self.knowledge[topic][act]:
                length1 = 0.0
                cosine_sim = 0.0
                related_count = 0.0
                for word in feature:
                    if word in act_feats:
                        related_count += 1
                        length1 += feature[word]**2
                        cosine_sim += act_feats[word]*feature[word]
                length1 = length1**0.5
                if length1 < length2:
                    cosine_sim = cosine_sim/(length2*length2)
                else:
                    cosine_sim = cosine_sim/(length1*length2)
                if cosine_sim > best_score:
                    best_score = cosine_sim
            return best_score

class ngram_word2vec:
    def __init__(self, word2vec, vector_length, knowledge):
        self.word2vec = word2vec
        self.vector_length = vector_length
        self.knowledge = {}
        for topic in knowledge:
            if topic not in self.knowledge:
                self.knowledge[topic] = {}
            for sentence in knowledge[topic]:
                words = sentence.split(' ')
                filter_words = [key for key in words if key in self.word2vec]
                if len(filter_words) < len(words):
                    continue
                tmp = np.zeros((len(filter_words),vector_length))
                for i in range(len(filter_words)):
                    word = filter_words[i]
                    tmp[i] = self.word2vec[word]
                for act in knowledge[topic][sentence]:
                    if act not in self.knowledge[topic]:
                        self.knowledge[topic][act] = [tmp]
                    else:
                        self.knowledge[topic][act] += [tmp]

    def scoring_in_knowledge(self, topic, utter_words):
        filter_words = [key for key in utter_words if key in self.word2vec]
        if len(filter_words) == 0:
            return 0
        else:
            snt_matrix = np.zeros((len(filter_words),self.vector_length))
            for i in range(len(filter_words)):
                word = filter_words[i]
                snt_matrix[i] = self.word2vec[word]
            snt_matrix.transpose()
            output = {}
            for act in self.knowledge[topic]:
                best_score = 0
                for act_mat in self.knowledge[topic][act]:
                    result = np.dot(act_mat,snt_matrix)
                    rst = result.amax(axis=1)
                    score = 0.0
                    for val in rst:
                        if val > 0.5:
                            score += val
                    score = score/len(rst)
                    if score > best_score:
                        best_score = score
                output[act] = best_score
            return output

    def scoring_act(self, topic, utter_words, act):
        filter_words = [key for key in utter_words if key in self.word2vec]
        if len(filter_words) == 0:
            return 0
        else:
            snt_matrix = np.zeros((len(filter_words),self.vector_length))
            for i in range(len(filter_words)):
                word = filter_words[i]
                snt_matrix[i] = self.word2vec[word]
            snt_matrix.transpose()
            best_score = 0
            for act_mat in self.knowledge[topic][act]:
                result = np.dot(act_mat,snt_matrix)
                rst = result.amax(axis=1)
                score = 0.0
                for val in rst:
                    if val > 0.5:
                        score += val
                score = score/len(rst)
                if score > best_score:
                    best_score = score
            return best_score
