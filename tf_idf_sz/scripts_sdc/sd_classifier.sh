#!/bin/bash

stage=0
ontology=scripts/config/ontology_dstc4.json
word2vec=/slfs6/users/sz128/data/word2vec/pre-trained/glove.42B.300d.txt

exp_path=exp/sd_classifier
knowledge=$exp_path/knowledges    #path
features=$exp_path/features       #path

input_train=local/data/frame_slu_utter_train.txt
input_dev=local/data/frame_slu_utter_dev.txt

. utils/parse_options.sh || exit 1

if [ $# -eq 1 ] && [ $1 == '-h' ]; then
  echo "usage: " $0 " [options]"
  echo "e.g.: " $0 " --stage 0"
  echo "options: "
  echo "     --stage <number>          #0,1,2,3,4"
  exit 1;
fi

[ ! -d $exp_path ] && mkdir -p $exp_path

## data extract
#if [ $stage -le 0 ];then
#  python scripts/get_user_utterance.py --dataset dstc4_train --dataroot data --output $input_train --outofdial local/data/frame_slu_utter_train.o.txt
#  python scripts/get_user_utterance.py --dataset dstc4_dev --dataroot data --output $input_dev --outofdial local/data/frame_slu_utter_dev.o.txt
#fi

if [ $stage -le 1 ];then
  [ ! -d $knowledge ] && mkdir -p $knowledge
  [ ! -d $features ] && mkdir -p $features
  python scripts_sdc/describe_ontology.py --ontology $ontology --knowledge $knowledge
  python scripts_sdc/create_feature_space.py --knowledge $knowledge --features $features
fi

input=$input_dev
run_label=run_dev_snt
decodeout=$exp_path/$run_label.output.nbest.json
trackfile=$exp_path/$run_label.baseline_dev.json
scorefile=$exp_path/$run_label.baseline_dev.score.csv
report=$exp_path/$run_label.baseline_dev.score.csv.report
if [ $stage -le 2 ];then
  python scripts_sdc/test_feature.py --dataset dstc4_dev --dataroot data --trackfile $trackfile --ontology $ontology --features $features --word2vec $word2vec --actlog $exp_path/$run_label.output.acts > $exp_path/$run_label.fst.log
fi

#exit 1

if [ $stage -le 3 ];then 
#python scripts/baseline_zsp.py --dataset dstc4_dev --dataroot data --trackfile $trackfile --ontology $ontology --decodeout $decodeout > $exp_path/$run_label.output.acts
python scripts/check_track.py --dataset dstc4_dev --dataroot data --ontology $ontology --trackfile $trackfile
python scripts/score.py --dataset dstc4_dev --dataroot data --trackfile $trackfile --scorefile $scorefile --ontology $ontology
python scripts/report.py --scorefile $scorefile > $report
awk -F '|' '{if(NR>5&&NR<10){a=$2;gsub(/^ *| *$/,"",a);b=$3;gsub(/^ *| *$/,"",b);A=A","a;B=B","b;}}END{print A,B;}' $report
fi
