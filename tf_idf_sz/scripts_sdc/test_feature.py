import argparse,os,sys,string,math,re,time,json,dataset_walker,ontology_reader
import numpy as np
from nltk.tag import StanfordPOSTagger
from fuzzywuzzy import fuzz
import features, os, io

usingbaseline = False #True
contextdep = False #True
usingPOS = True

class BaselineTracker(object):
    def __init__(self, tagsets, knowledge, feature_maps, log):
        tagPath = "stanford-postagger-2014-08-27"
        sp = os.sep
        self.pos = StanfordPOSTagger(tagPath + sp + "models" + sp + "english-bidirectional-distsim.tagger", tagPath + sp + "stanford-postagger.jar")
        self.nounList = ["NN", "NNS", "NNP", "NNPS", "PRP", "PRP$", "WP", "WP$"]
        self.tagsets = tagsets
        self.frame = {}
        self.memory = {}
        self.memory['place'] = {}

        self.feature_maps = feature_maps
        self.log = log
        self.utter_count = 0

        self.knowledge = knowledge
        self.ptypes = {}
        for slot in self.knowledge:
            for i in range(len(self.knowledge[slot])):
                if "TYPE_OF_PLACE" not in self.knowledge[slot][i]:
                    continue
                if "NAME" not in self.knowledge[slot][i]:
                    continue
                val = self.knowledge[slot][i]["NAME"].lower()
                for tp in self.knowledge[slot][i]["TYPE_OF_PLACE"]:
                    tp = tp.lower()
                    if val not in self.ptypes:
                        self.ptypes[val] = []
                    self.ptypes[val] += [tp]
        
        self.reset()

    def preprocessing(self,sentence):
        sentence = sentence.lower()
        sentence = sentence.replace('\"','')
        sentence = re.sub('[ ][ ]+',' ',sentence)
        sentence = sentence.strip()
        word_list = sentence.split(' ')
        new_word_list = []
        for word in word_list:
            word = word.strip("-~`")
            if word == "":
                continue
            elif word[-1] in ".,?!":
                new_word_list.append(word[:-1].strip(".,?!-~"))
                new_word_list.append(word[-1])
            else:
                new_word_list.append(word)
        for idx,word in enumerate(new_word_list):
            word = re.sub('%huh|%hmm|%hm|%eh|%um|%uh|%ah|%oh|%h','',word)
            word = word.replace("%","")
            new_word_list[idx] = word
        new_word_list = [key for key in new_word_list if key != ""]
        return ' '.join(new_word_list)

    def solve_unk_word(self,word):
        number_map = {'1':'one','2':'two','3':'three','4':'four','5':'five','6':'six','7':'seven','8':'eight','9':'nine','0':'zero'}
        word = word.strip(",.?!+-@~"+"\'")
        if word == "":
            return []
        else:
            word = re.sub("[,.?!+\-@~]",' ',word)
            tmp_words = word.split(' ')
            new_tmp_words = []
            for tmp_word in tmp_words:
                if tmp_word in number_map:
                    new_tmp_words.append(number_map[tmp_word])
                else:
                    if '\'' in tmp_word:
                        tmp_word = tmp_word.replace('\'',' \'').split(' ')
                        for key in tmp_word:
                            new_tmp_words.append(key)
                    else:
                        new_tmp_words.append(tmp_word)
            return new_tmp_words

    def addUtter(self, utter):
        output = {'utter_index': utter['utter_index']}

        if utter['segment_info']['target_bio'] == 'O':
            return output 
        else:
            self.utter_count += 1
            #if self.utter_count != 4133:
            #    return output
            if utter['segment_info']['target_bio'] == 'B':
                self.frame = {}

        topic = utter['segment_info']['topic']
        speaker = utter['speaker']
        transcript = self.preprocessing(utter['transcript'])
        #transcript = utter['transcript'].replace('Singapore', '')
        text = transcript
        slot_values = []
        new_transcript = ''
        if text.strip() != "":
            words = []
            if usingPOS:
                sys.stderr.write(text.strip() + "\n")
                for word, pro in self.pos.tag(text.strip().split(" ")):
                    if word in ',.?!':
                        words.append(word)
                    elif pro in self.nounList:
                        sys.stderr.write(word + "\n")
                        sys.stderr.write(pro + "\n")
                        new_sub_words = self.solve_unk_word(word)
                        if new_sub_words != []:
                            words += new_sub_words
                    else:
                        #print word, pro
                        pass
            else:
                for idx,word in enumerate(text.strip().split(' ')):
                    if word in ',.?!':
                        words.append(word)
                    else:
                        new_sub_words = self.solve_unk_word(word)
                        if new_sub_words != []:
                            words += new_sub_words
            new_transcript = ' '.join(words)
            #print new_transcript
            #knn_points = self.knn('word2vec',feature_ngram_w2v,words,topic)
            slot_values = self.knn('tf-idf',self.feature_maps['tf-idf'],words,topic)

        acts = []
        if topic in self.tagsets:
            if contextdep:
                replaced_type_of_place = [] #['museum',]
                take_from_previous = []
                for type_of_place in self.memory['place']:
                    slot,value = self.memory['place'][type_of_place]
                    if slot in self.tagsets[topic] and value in self.tagsets[topic][slot]:
                        for pron in ['the '+type_of_place,'this '+type_of_place,'that '+type_of_place]:
                            if ' '+pron+' ' in ' '+transcript+' ':
                                #print '##',pron,transcript
                                transcript = (' '+transcript+' ').replace(' '+pron+' ',' ')
                                transcript = transcript.strip(' ')
                                acts.append('inform('+slot+'='+value+')')
                                if slot not in self.frame:
                                    self.frame[slot] = []
                                if value not in self.frame[slot]:
                                    self.frame[slot].append(value)
                                replaced_type_of_place.append(type_of_place)
                                take_from_previous.append(slot)
                                break
        
            if slot_values == []:
                pass
            else:
                for slot,value in slot_values:
                    if value == 'Show' and ('show me' in transcript or 'show you' in transcript or 'you show' in transcript):
                        continue
                    if contextdep:
                        if slot == "TYPE_OF_PLACE" and value.lower() in replaced_type_of_place:
                            continue
                    acts.append('inform('+slot+'='+value+')')
                    if slot not in self.frame:
                        self.frame[slot] = []
                    if value not in self.frame[slot]:
                        self.frame[slot].append(value)
                    if contextdep:
                        _value = value.lower()
                        if _value in self.ptypes:
                            for _t in self.ptypes[_value]:
                                self.memory['place'][_t] = (slot,value)
            
            if usingbaseline:
                #"""       
                #bf0
                for slot in self.tagsets[topic]:
                    if slot in self.frame: continue
                    bestv = 0
                    for value in self.tagsets[topic][slot]:
                        if len(transcript) < 6: continue
                        if len(value) < 6 and ' ' not in value and len(transcript) > 10: continue
                        ratio = fuzz.partial_ratio(self.solve_value(value), transcript)
                        if ratio >= 90 or (ratio >= 85 and (len(value) > 8 or ' ' in value)):
                            if ratio > bestv:
                                ok = True
                                for _s in self.frame:
                                    for _v in self.frame[_s]:
                                        #if value.lower().replace('-', ' ') in _v.lower().replace('-', ' '):
                                        if self.solve_value(value) in self.solve_value(_v):
                                            ok = False
                                            break
                                    if not ok:
                                        break
                                if ok:
                                    self.frame[slot] = [value]
                                    acts.append('inform('+slot+'='+value+')') #sz128
                                    bestv = ratio
                                    if contextdep:
                                        _value = value.lower()
                                        if _value in self.ptypes:
                                            for _t in self.ptypes[_value]:
                                                self.memory['place'][_t] = (slot,value)
                #"""
            #if topic == 'ATTRACTION' and 'PLACE' in self.frame and 'NEIGHBOURHOOD' in self.frame and self.frame['PLACE'] == self.frame['NEIGHBOURHOOD']:
            #    del self.frame['PLACE']
            if 'PLACE' in self.frame and 'NEIGHBOURHOOD' in self.frame:
                self.frame['PLACE'] = list(set(self.frame['PLACE']) - set(self.frame['NEIGHBOURHOOD']))
                if self.frame['PLACE'] == []:
                    del self.frame['PLACE']

            output['frame_label'] = self.frame
            self.log.write(new_transcript+' <=> '+' '.join(acts)+'\n')
        return output

    def reset(self):
        self.frame = {}
        self.memory['place'] = {}

    def knn(self,flag,feature,words,topic):
        if flag == 'word2vec':
            result = feature.scoring_in_knowledge(topic,words)
            if result == 0:
                print 'o()'
            else:
                for act in result:
                    print act,result[act]
        elif flag == 'tf-idf':
            result = feature.scoring_in_knowledge(topic,words)
            if result == 0:
                output = []
            else:
                output = []
                for act in result:
                    score,sentence = result[act]
                    if score > 0.8:
                        print act,result[act]
                        output.append([score,sentence,act])
                new_output = []
                for score,sentence,act in output:
                    unique = True
                    if sentence not in ['chinatown']:
                        for (score_1,sentence_1,act_1) in output:
                            if sentence != sentence_1 and ' '+sentence+' ' in ' '+sentence_1+' ':
                                unique = False
                                break
                    if unique == True:
                        new_output.append([score,sentence,act])
                output = new_output

        slot_values = []
        for score,sentence,act in output:
            acttype,slot_value = act[:-1].split('(')
            if slot_value != "":
                slot,value = slot_value.split('=')
                slot_values.append((slot,value))
                if slot == 'TICKET':
                    slot_values.append(('INFO','Ticketing'))
        return slot_values


def main(argv):
    parser = argparse.ArgumentParser(description='Simple hand-crafted dialog state tracker baseline.')
    parser.add_argument('--dataset', dest='dataset', action='store', metavar='DATASET', required=True, help='The dataset to analyze')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--features',dest='features',action='store',metavar='FILE',required=True,help='Class feature path')
    parser.add_argument('--word2vec',dest='word2vec',action='store',metavar='FILE',required=True,help='Word vector file')
    parser.add_argument('--trackfile',dest='trackfile',action='store',required=True,metavar='JSON_FILE', help='File to write with tracker output')
    parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
    parser.add_argument('--actlog',dest='actlog',action='store',metavar='FILE',required=True,help='act log file')
    args = parser.parse_args()
    
    dataset = dataset_walker.dataset_walker(args.dataset,dataroot=args.dataroot,labels=False)
    tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
    ontology_knowledge = ontology_reader.OntologyReader(args.ontology).get_knowledge()
    
    knowledge = {}
    words_idf = {}
    for topic in os.listdir(args.features):
        if topic.endswith('.idf'):
            continue
        feature_filename = os.path.join(args.features,topic)
        if os.path.isdir(feature_filename):
            continue
        feature_file = open(feature_filename,'rb')
        knowledge[topic] = {}
        for line in feature_file:
            line = line.strip()
            rule, labels, tfidf = line.split('<=>')
            if rule not in knowledge[topic]:
                knowledge[topic][rule] = [labels]
            else:
                knowledge[topic][rule] += [labels]
        feature_file.close()
        
        words_idf[topic] = {}
        word_idf_filename = os.path.join(args.features,topic+'.idf')
        word_idf_file = open(word_idf_filename,'rb')
        for line in word_idf_file:
            line = line.strip('\r\n')
            word, idx, idf = line.split(' ')
            idx = int(idx)
            idf = float(idf)
            words_idf[topic][word] = idf
        word_idf_file.close()
    
    if False:
        word2vec_file = open(args.word2vec,'rb')
        word2vec = {}
        vector_length = 0
        for line in word2vec_file:
            line = line.strip('\r\n')
            idx = line.index(' ')
            word = line[:idx]
            value = line[idx+1:]
            vector = np.fromstring(value, dtype=float, sep=' ')
            word2vec[word.lower()] = vector
            vector_length = len(vector)

    #feature_ngram_w2v = features.ngram_word2vec(word2vec,vector_length,knowledge)
    #feature_tf_idf = features.tf_idf(words_idf, knowledge) 
    feature_maps = {
            'tf-idf' : features.tf_idf(words_idf, knowledge)
            }

    track_file = open(args.trackfile, "wb")
    track = {"sessions":[]}
    track["dataset"]  = args.dataset
    start_time = time.time()
    print "clock0:%s" % time.clock()
    tracker = BaselineTracker(tagsets,ontology_knowledge,feature_maps,open(args.actlog,'wb'))
    for call in dataset:
        this_session = {"session_id":call.log["session_id"], "utterances":[]}
        tracker.reset()
        for (utter,_) in call:
            sys.stderr.write('%d:%d\n'%(call.log['session_id'], utter['utter_index']))
            tracker_result = tracker.addUtter(utter)
            if tracker_result is not None:
                this_session["utterances"].append(tracker_result)
        track["sessions"].append(this_session)
    end_time = time.time()
    elapsed_time = end_time - start_time
    track['wall_time'] = elapsed_time

    json.dump(track, track_file, indent=4)

    track_file.close()
    print "clock-end:%s" % time.clock()

if __name__ =="__main__":
	main(sys.argv)
