__author__ = 'v-qizxie'
import gensim, os, argparse, dataset_walker, ontology_reader, time, logging, copy, pickle
import gensim.corpora
import numpy as np
from nltk.corpus import stopwords
from collections import defaultdict
import lda

class getWordList:
    @staticmethod
    def getWordList(transcript):
        t = transcript.lower()
        t = t.replace('-', ' ')
        t = t.split(' ')
        transcript = ''
        stop = stopwords.words('english')
        for _t in t:
            if len(_t)>0 and _t[0] == '%': continue
            transcript += _t + ' '
        transcript = transcript[:len(transcript)-1]
        ll = transcript.split()
        ans = []
        for i in ll:
            if i != "" and i not in stop: #i.isalpha()
                ans += [i]
        return ans

class TM:
    def __init__(self, args):
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
        sp = os.pathsep
        path = os.getcwd() + sp + "data" + sp + "crawled_data" + sp
        documents = []
        #for ds in ["dstc4_train", "dstc4_dev"]:
        for ds in ["dstc4_train1"]:
            dataset = dataset_walker.dataset_walker(ds,dataroot=args.dataroot,labels=False)
            tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
            for call in dataset:
                sessionID = call.log["session_id"]
                doc = ""
                for (utter,_) in call:
                    utter_index = utter['utter_index']
                    index = ds + '_%s_%s' % (sessionID, utter_index)
                    bio = utter['segment_info']['target_bio']
                    if bio == "B":
                        doc = ""
                    elif bio == "O":
                        continue
                    doc = doc + utter['transcript'] + " "
                    documents += [getWordList.getWordList(doc)]
                '''if doc != "":
                    documents += [getWordList.getWordList(doc)]'''
        dictionary = gensim.corpora.Dictionary(documents)
        dictionary.save(args.printDict)
        ccc = []
        for text in documents:
            now = [0] * len(dictionary.token2id)
            for p, v in dictionary.doc2bow(text):
                now[p] = v
            ccc.append(copy.deepcopy(now))
        self.dict = dictionary
        self.model = lda.LDA(n_topics=100)
        ccc = np.array(ccc)
        self.model.fit(ccc)

    def infer(self, sentence):
        doc = getWordList.getWordList(sentence)
        now = [0] * len(self.dict.token2id)
        for p, v in self.dict.doc2bow(doc):
            now[p] = v
        Y = self.model.transform(now)
        return Y
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Sentence vectors on DSTC-4 by Qizhe Xie.')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
    parser.add_argument('--train',dest='train',action='store',metavar='JSON_FILE',required=False,help='Train/Test')
    parser.add_argument('--printFile',dest='printFile',action='store',metavar='PATH',required=False,help='doc2vec.obj')
    parser.add_argument('--printDict',dest='printDict',action='store',metavar='PATH',required=False,help='dict.obj')
    args = parser.parse_args()
    if args.train:
        tm = TM(args)
        print tm.infer("I want to go sightseeing and fuck you")
        pickle.dump(tm, open(args.printFile, "wb"))
        b = pickle.load(open(args.printFile, "rb"))

