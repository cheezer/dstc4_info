__author__ = 'v-qizxie'
import gensim, os, argparse, dataset_walker, ontology_reader, time, logging
from gensim.models.doc2vec import TaggedDocument
from gensim.models.ldamodel import LdaModel
from gensim import corpora, models, similarities
from nltk.corpus import stopwords
from collections import defaultdict

class getWordList:
    @staticmethod
    def getWordList(transcript):
        t = transcript.lower()
        t = t.replace('-', ' ')
        t = t.split(' ')
        transcript = ''
        stop = stopwords.words('english')
        for _t in t:
            if len(_t)>0 and _t[0] == '%': continue
            transcript += _t + ' '
        transcript = transcript[:len(transcript)-1]
        ll = transcript.split()
        ans = []
        for i in ll:
            if i != "" and i not in stop: #i.isalpha()
                ans += [i]
        return ans

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Sentence vectors on DSTC-4 by Qizhe Xie.')
    parser.add_argument('--dataroot',dest='dataroot',action='store',required=True,metavar='PATH', help='Will look for corpus in <destroot>/<dataset>/...')
    parser.add_argument('--ontology',dest='ontology',action='store',metavar='JSON_FILE',required=True,help='JSON Ontology file')
    parser.add_argument('--train',dest='train',action='store',metavar='JSON_FILE',required=False,help='Train/Test')
    parser.add_argument('--printFile',dest='printFile',action='store',metavar='PATH',required=False,help='doc2vec.obj')
    parser.add_argument('--printDict',dest='printDict',action='store',metavar='PATH',required=False,help='dict.obj')
    args = parser.parse_args()
    if args.train:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
        start_time = time.time()
        sp = os.pathsep
        path = os.getcwd() + sp + "data" + sp + "crawled_data" + sp
        documents = []
        for ds in ["dstc4_train", "dstc4_dev"]:
            dataset = dataset_walker.dataset_walker(ds,dataroot=args.dataroot,labels=False)
            tagsets = ontology_reader.OntologyReader(args.ontology).get_tagsets()
            for call in dataset:
                sessionID = call.log["session_id"]
                doc = ""
                for (utter,_) in call:
                    utter_index = utter['utter_index']
                    index = ds + '_%s_%s' % (sessionID, utter_index)
                    bio = utter['segment_info']['target_bio']
                    if bio == "B":
                        doc = ""
                    elif bio == "O":
                        '''if doc != "":
                            documents += [getWordList.getWordList(doc)]'''
                        continue
                    doc = doc + utter['transcript'] + " "
                    documents += [getWordList.getWordList(doc)]
                '''if doc != "":
                    documents += [getWordList.getWordList(doc)]'''
        frequency = defaultdict(int)
        for text in documents:
            for token in text:
                frequency[token] += 1
        texts = [[token for token in text if frequency[token] > 1] for text in documents]
        dictionary = corpora.Dictionary(texts)
        dictionary.save(args.printDict)
        corpus = [dictionary.doc2bow(text) for text in texts]
        lda = LdaModel(corpus, num_topics=100, id2word=dictionary)
        print lda.print_topics()
        lda.save(args.printFile)
        end_time = time.time()
        print end_time - start_time

